#pragma GCC optimize("O3")
#include <bits/stdc++.h>
using namespace std;

typedef int ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const int MOD = 1e9 + 7, MX = 3e5 + 4, VX = 1e6 + 4;

ll arr[MX], lazy[4 * MX], tree[4 * MX][10], n, q;
string str;

void build_tree(ll node, ll a, ll b) {
  	if(a > b) return; // Out of range
  	
  	if(a == b) 
  	{ // Leaf node
    		tree[node][str[a] - '0'] = 1; // Init value
		return;
	}
	
	build_tree(node*2, a, (a+b)/2); // Init left child
	build_tree(node*2+1, 1+(a+b)/2, b); // Init right child
	
	REP(i, 10) tree[node][i] = (tree[node*2][i] + tree[node*2+1][i]); // Init root value
}

/**
 * update elements within range [i, j] 
 */
void update_tree(ll node, ll a, ll b, ll i, ll j) {
  
  	if(lazy[node] != 0) 
  	{ 	
  		// This node needs to be updated
 		vector<ll> temp(10);
   		REP(i, 10) temp[(i + lazy[node])%10] = tree[node][i]; // Update it
   		REP(i, 10) tree[node][i] = temp[i];

		if(a != b) 
		{
			lazy[node*2] += lazy[node]; // Mark child as lazy
    			lazy[node*2+1] += lazy[node]; // Mark child as lazy
		}

   		lazy[node] = 0; // Reset it
  	}
  
	if(a > b || a > j || b < i) // Current segment is not within range [i, j]
		return;
    
  	if(a >= i && b <= j) 
  	{	
  		// Segment is fully within range
 		vector<ll> temp(10);
   		REP(i, 10) temp[(i + 1)%10] = tree[node][i]; // Update it
   		REP(i, 10) tree[node][i] = temp[i];

		if(a != b) 
		{	
			// Not leaf node
			lazy[node*2] += 1;
			lazy[node*2+1] += 1;
		}

    		return;
	}

	update_tree(node*2, a, (a+b)/2, i, j); // Updating left child
	update_tree(1+node*2, 1+(a+b)/2, b, i, j); // Updating right child

	REP(i, 10) tree[node][i] = (tree[node*2][i] + tree[node*2+1][i]); // Updating root with max value
}

/**
 * Query tree to get ans in range [i, j]
 */
ll query_tree(ll node, ll a, ll b, ll i, ll j) {
	
	if(a > b || a > j || b < i) return 0; // Out of range

	if(lazy[node] != 0) 
	{	
		// This node needs to be updated
 		vector<ll> temp(10);
   		REP(i, 10) temp[(i + lazy[node])%10] = tree[node][i]; // Update it
   		REP(i, 10) tree[node][i] = temp[i];

		if(a != b) 
		{
			lazy[node*2] += lazy[node]; // Mark child as lazy
			lazy[node*2+1] += lazy[node]; // Mark child as lazy
		}

		lazy[node] = 0; // Reset it
	}

	if(a >= i && b <= j) // Current segment is totally within range [i, j]
	{
		ll sum = 0;
		REP(i, 10) sum += i*tree[node][i];
		return sum;
	}

	ll q1 = query_tree(node*2, a, (a+b)/2, i, j); // Query left child
	ll q2 = query_tree(1+node*2, 1+(a+b)/2, b, i, j); // Query right child

	ll res = (q1 + q2); // Return final result
	
	return res;
}


int main()
{
	//{1, 4, 6, 2, 52, 11, 34}
	cin >> n >> q;

	str = " ";
	string k;
	cin >> k;
	str = str + k; 

	build_tree(1, 1, n);

	REP(i, q)
	{
		ll x, y;
		cin >> x >> y;
		cout << query_tree(1, 1, n, x, y) << endl;
	//	REP(i, 10) cout << tree[1][i] << " ";
	//	cout << endl;
		update_tree(1, 1, n, x, y);
	}

}