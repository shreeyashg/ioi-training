#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

struct node
{
	ll from, to, val, mn, lazy;
	node *left, *right;
	node()
	{
		from = 1;
		to = 1e9;
		val = 0, lazy = 0, mn = INF, left = NULL, right = NULL;
	}

	void extend()
	{
		if(left == NULL)
		{
			left = new node(), right = new node();
			left->from = from;
			left->to = (from + to) >> 1;
			right->from = ((from + to) >> 1) + 1;
			right->to = to;
		}
	}
};

node* root;

void update(node *curr, ll l, ll r, ll val)
{
	if(curr->lazy)
	{
		curr->val = (curr->to - curr->from + 1) * curr->lazy;
		curr->mn += curr->lazy;
		if(curr->from != curr->to)
		{
			curr->extend();
			curr->left->lazy += curr->lazy;
			curr->right->lazy += curr->lazy;
		}
		curr->lazy = 0;
	}

	if((curr->from) > r || (curr->to) < l || (curr->from) > (curr->to))
		return;
	if(curr->from >= l && curr->to <= r)
	{
		curr->val += (curr->to - curr->from + 1) * val;
		if(curr->mn != INF)
			curr->mn += val;
		else
			curr->mn = val;
		if(curr->from != curr->to)
		{
			curr->extend();
			curr->left->lazy += val;
			curr->right->lazy += val;
		}
		return;
	}

	curr->extend();
	update(curr->left, l, r, val);
	update(curr->right, l, r, val);
	curr->val = curr->left->val + curr->right->val;
	curr->mn = min(curr->left->mn, curr->right->mn);
}

ll query(node *curr, ll l, ll r)
{
	if(curr->from > curr->to || curr->from > r || curr->to < l)
		return 0;
	if(curr->lazy)
	{
		curr->val += (curr->to - curr->from + 1)*curr->lazy;
		curr->mn += curr->lazy;
		curr->extend();
		curr->left->lazy += curr->lazy;
		curr->right->lazy += curr->lazy;
		curr->lazy = 0;
	}

	if(curr->from >= l && curr->to <= r)
		return curr->val;

	ll q1, q2;
	q1 = query(curr->left, l, r), q2 = query(curr->right, l, r);
	return q1 + q2;
}

ll minq(node *curr, ll l, ll r)
{
	if(curr->from > curr->to || curr->from > r || curr->to < l)
		return 0;
	if(curr->lazy)
	{
		curr->val += (curr->to - curr->from + 1) * curr->lazy;
		curr->mn += curr->lazy;
		curr->extend();
		curr->left->lazy += curr->lazy;
		curr->right->lazy += curr->lazy;
		curr->lazy = 0;
	}

	if(curr->from >= l && curr->to <= r)
		return curr->mn;

	ll q1, q2;
	q1 = minq(curr->left, l, r), q2 = minq(curr->right, l, r);
	return min(q1, q2);

}

void vis()
{
	if(curr->from == curr->to)
		return;
	cout << curr->from << " " << curr->to << endl;
	cout << "sum = " << curr->val << ", min = " << 
}

int main()
{
	node *root = new node();
	ll n, q;
	cin >> n >> q;

	REP(i, n)
	{
		ll a;
		cin >> a;
		update(root, i + 1, i + 1, a);
	}

	FOR(i, 1, n) cout << query(root, i, i) << " " ;
	cout << endl;

	cout << endl;
	REP(i, q)
	{
		char ch;
		cin >> ch;

		if(ch == 'M')
		{
			ll x, y;
			cin >> x >> y;
			cout << minq(root, x, y) << endl;
		}
	
		if(ch == 'P')
		{
			ll x, y, z;
			cin >> x >> y >> z;
			update(root, x, y, z); // << endl; 

		}
	
		if(ch == 'S')	
		{
			ll x, y;
			cin >> x >> y;
			cout << query(root, x, y) << endl;
		}
	}
}