#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e3 + 4;

ll arr[MX], n;

ll find(ll x) { return arr[x] = (arr[x] != x) ? find(arr[x]) : x; }
void uni(ll x, ll y) {((x == y) ? : (rand() % 2) ? arr[find(x)] = find(y) : arr[find(y)] = find(x));}

int main()
{
	cin >> n;
	FOR(i, 1, n) arr[i] = i;

	uni(2, 5);
	uni(3, 7);
	uni(4, 9);
	uni(1, 2);
	uni(3, 3);

	FOR(i, 1, n) cout << i << " " << find(i) << endl;
}