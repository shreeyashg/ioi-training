#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

int main()
{
	ll x;
	cin >> x;
	string str;
	cin >> str;

	bool f = 0;

	ll cnt =0 ;
	REP(i, str.size())
	{
		if(str[i] == '?')
			cnt ++;
	}

	if(!cnt)
		cout << "No" << endl, exit(0);

	REP(i, str.size() - 1)
	{
		if(str[i] == str[i + 1] && str[i] != '?' && str[i + 1] != '?') f = 1;
	}

	if(f)
		cout << "No" << endl, exit(0);
	// single ? in the end => two ways

	REP(i, str.size()-1) // 
	{
		if(i == 0) continue;
		if(str[i] == '?')
		{
			 if(str[i + 1] != '?' && (str[i-1] != '?') && str[i + 1] != str[i - 1])
			 {
			 	f = 1;
			 }
			 else
			 {
			 	f = 0;
			 	break;
			 }
		}
	}

	if(str[str.size() - 1] == '?' || str[0] == '?') f = 0;

	if(f)
		cout << "No" << endl;
	else
		cout << "Yes" << endl;
}