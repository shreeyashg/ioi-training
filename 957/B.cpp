#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

vector<string> a;
map<ll, ll> ro, col;
ll r, c;

vector<ll> temp;

void colsearch()
{
	bool b = 1;
	REP(j, c)
	{
		cout << endl;
		REP(i, r)
		{
			REP(j, c)
			{
				cout << a[i][j] << " ";
			}
			cout << endl;
		}
		cout << endl << endl;
		bool b = 0;
		REP(ii, temp.size())
		{
			ll i = temp[ii];
			if(a[i][j] == '#')
			{
				b = 1;
				a[i][j] = '.';
			}
			else
				b = 0;
		}
		col[j] = 0;
	}

	if(!b)
		cout << "No" << endl;
}

int main()
{
	cin >> r >> c;

	REP(i, r)
	{
		string str;
		cin >> str;
		a.push_back(str);
	}

	REP(i, r)
	{
		REP(j, c)
		{
			if(a[i][j] == '#' && (ro[i] || col[j]))
			{
				DEBUG("2");
				cout << i << " " << j << " " << endl;
				DEBUG(col[j]);
				DEBUG(ro[i]);
				cout << "No" << endl, exit(0);
			}
			if(a[i][j] == '#')
			{
				REP(ii, r)
				{
					if(a[ii][j] == '#')
					{
						DEBUG(ii);
						temp.push_back(ii);
					}
				}

				colsearch();
				temp.clear();

			}

		}
	}

	REP(i, r)
	{
		REP(j, c)
		{
			if(a[i][j] == '#')
				cout << "No" << endl, exit(0);
		}
	}

	cout << "Yes" << endl;
}