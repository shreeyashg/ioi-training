//


#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n, arr[MX], memo[105][105];
ll sum(ll l, ll r);
/*

THIS DOESN'T WORK

std::pair<T,U> operator+(const std::pair<T,U> & l,const std::pair<T,U> & r) {   
    return {l.first+r.first,l.second+r.second};                                    
}                                                                                  

ll n, arr[MX];

pair<ll, ll> dp1(ll left, ll right)
{
	if(left == right)
		return {arr[left], 0};
	if(left + 1 == right)
		return {max(arr[left], arr[right]), min(arr[left], arr[right])};

	return max({arr[left], 0} + dp2(left + 1, right), {arr[right], 0} + dp2(left, right - 1));
}

pair<ll, ll> dp2(ll left, ll right)
{
	if(left == right)
		return {0, arr[left]};
	if(left + 1 == right)
		return {min(arr[left], arr[right]), max(arr[left], arr[right])};
	return min({0, arr[left]} + dp2(left + 1, right), {, arr[]} + dp2(left, right - 1));
}

*/

ll dp(ll l, ll r) // 
{
    if(memo[l][r] != -1)
        return memo[l][r];
    
    if(l == r)
    {
        return arr[l];
    }
    
    return memo[l][r] = max(arr[l] + sum(l+1, r) - dp(l+1, r), sum(l, r-1)-dp(l, r-1)+arr[r]);
    
    // What do I do here? Think about the transitions. What happens if I choose the left guy? What state do I go to?
    // You go to dp(L + 1, R)? I'm not sure. Yes. Be sure Look DP(L, R) =  max(arr[l] + sum(L+1, R) - DP(L+1, R), sum(l, r-1)-dp(l, r-1)+arr[r])
    // Justify if the above formula is right or wrong.
    // Okay. What is DP(L, R) in this formula? If it is my turn and I play optimally, what can be my maximum score. Note that I refers to the player whose turn it is
    // Okay.
    
    //Let me think for a moment.
    
    // Yeah looks right to me. This is exactly what you had written in fb messenger chat earlier. I just changed the dp2 thingy
    // Oh sweet. I was having problems handling the dp2 and the case of the second player. Let me code this thing now.
    // Wow
}

ll sum(ll l, ll r)
{
    ll x = 0;
    FOR(I, l, r)
    {
        x += arr[I];
    }
    return x;
}



int main()
{
    	memset(memo, -1, sizeof(memo));
	cin >> n;
	FOR(i, 1, n)
	{
		cin >> arr[i];
	}

	cout << dp(1, n) << " " << sum(1, n) - dp(1, n) << endl;
}