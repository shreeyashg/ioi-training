#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const long long MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n;
vector<long long> v;

int main()
{
	ifstream cin("angry.in");
	ofstream cout("angry.out");
	cin >> n;
	v.resize(n);

	REP(i, n)
	{
		cin >> v[i];
		v[i] *= 2;
	}

	sort(v.begin(), v.end());
	v.resize(unique(v.begin(), v.end()) - v.begin());

	vector<ll> DP[2];
	REP(it, 2)
	{
		int lstj = 0;
		DP[it].resize(n, INF);
		DP[it][0] = -2;
		FOR(i, 1, n - 1)
		{
			while(lstj + 1 < i && abs(v[i] - v[lstj + 1]) > DP[it][lstj + 1] + 2)
				lstj++;
			DP[it][i] = min(abs(v[i] - v[lstj]), DP[it][lstj + 1] + 2);
		}

		reverse(v.begin(), v.end());
	}
	reverse(DP[1].begin(), DP[1].end());

	ll i = 0, j = n - 1, result = INF;

	while(i < j)
	{
		result = min(result, max((v[j] - v[i])/2, 2 + max(DP[0][i], DP[1][j])));
		if(DP[0][i + 1] < DP[1][j - 1])
		{
			i++;
		}
		else
		{
			j--;
		}
	}

	cout << result / 2 << '.' << ((result % 2) ? 5 : 0);
	cout << endl;
}