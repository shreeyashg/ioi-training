#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e3 + 4;

ll arr[MX], n;
set<pair<ll, ll> > edges, bad;

ll find(ll x) { return arr[x] = (arr[x] != x) ? find(arr[x]) : x; }
void uni(ll x, ll y) {((x == y) ? : (rand() % 2) ? arr[find(x)] = find(y) : arr[find(y)] = find(x));}

int main()
{
	cin >> n;
	FOR(i ,1, n) arr[i] = i;
	REP(i, n - 1)
	{
		ll x, y;
		cin >> x >> y;
		if(find(x) == find(y))
			bad.insert({x, y});
		uni(x, y);
		edges.insert({x, y});
	}

	ll cnt = 0;
	cout << bad.size() << endl;
	while(bad.size())
	{
		auto it = bad.begin();
		bad.erase(it);
		auto x = *it;


		FOR(i, 2, n)
		{
			if(find(i) != find(1))
			{
				uni(1, i);
				cout << x.first << " " << x.second << " " << 1 << " " << i << endl;
				break;
			}
		}
	}
/*	while(true)
	{
		break;
	}
*/
//	cout << cnt << endl;
}