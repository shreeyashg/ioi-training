#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n, k, sz[MX], avl[MX], svd;
vector<pair<ll, ll> > v[MX];
ll cuts=  0;
//void f()

ll init(ll x, ll par)
{
	REP(i, v[x].size())
	{
		if(v[x][i].first != par)init(v[x][i].first, x);
	 	if(v[x][i].first != par)sz[x] += sz[v[x][i].first];
	}
}

ll f(ll x, ll par)
{
	REP(i, v[x].size())
	{
		if(sz[v[x][i]].second == 1 && (sz[1] - svd > k))
			cuts++, svd += sz[v[x][i]].first;

	}
}

bool cmp(pair<ll, ll>  x, pair<ll, ll >  y)
{
	return sz[x.first] > sz[y.first];
}

int main()
{
	cin >> n >> k;
	FOR(I,1,n) sz[I]=1;
	REP(i, n - 1)
	{
		ll a, b, c;
		cin >> a >> b >> c;
		v[a].push_back({b, c});
		v[b].push_back({a, c});
	}
	init(1, 0);
	FOR(i, 1, n)
	{
	//	cout << i << " " << sz[i] << endl;
		avl[i] = sz[i];
	}
	
	FOR(i, 1, n)
	{
		sort(v[i].begin(), v[i].end(), cmp);
	}
	f(1, 0);
}