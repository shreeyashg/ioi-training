#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

vector<ll> v;

int main()
{
	ll n;
	cin >> n;
	REP(i, n)
	{
		ll x;
		cin >> x;
		v.push_back(x);
	}

	ll pre = -1;
	ll cnt =0 ;
	REP(i, v.size())
	{
		if(v[i] > pre)
		{
			cnt ++;
			pre = v[i];
		}
	}
	cout << cnt << endl;
}