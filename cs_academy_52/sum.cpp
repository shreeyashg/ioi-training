#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n;
vector<ll> v;
map<ll, set<ll> >mp;

int main()
{


	cin >> n;
	REP(i, n)
	{
		ll x;
		cin >> x;
		v.push_back(x);
		mp[x].insert(i);
	}

	sort(v.begin(), v.end());
	reverse(v.begin(), v.end());
	ll cnt = 0;
	REP(i, v.size())
	{
		ll sum = v[i];
		FOR(j, i + 1, v.size() - 1)
		{
			ll first = v[j];
			ll req = sum - first;
			ll x = 0;
			if(j + 1 <= n - 1)x = upper_bound(v.begin() + j + 1, v.end(), req,[](ll a, ll b) { return a > b; } ) - lower_bound(v.begin() + j + 1, v.end(), req, [](ll a, ll b) { return a > b; });
			else x = 0; 
			 // Numbers equal to req

			cout << sum << " " << first << " " << req << " " << x << endl;

			cnt += x;
		}

	}

	cout << cnt << endl;
}