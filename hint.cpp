#include <bits/stdc++.h> // Howdy // Check this code Jeff, does this work? I mean, there's nowhere I have set the lazy flag. What is this for? Set [L, R] to K. See the update() function. THEIR IS ONLY UPDATE FUNCTION! 
using namespace std;

typedef int ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, MX = 3e4 + 4;

struct node
{
	ll from, to, val, lazy;
	node *left, *right;
	node()
	{
		from = 1;
		to = 1e7;
		val = 0, lazy = 0, left = NULL, right = NULL;
	}

	void extend()
	{
		if(left == NULL)
		{
			left = new node(), right = new node();
			left->from = from;
			left->to = (from + to) >> 1;
			right->from = ((from + to) >> 1) + 1;
			right->to = to;
		}
	}
};

node *root;

void update(node *curr, ll l, ll r, ll val) // THERE IS NO PLACE WHERE I SET curr->lazy = something
{
	if(curr->lazy)
	{
		curr->val = curr->lazy;//here? curr->lazy = x, not x = curr->lazy Ahhh. Gotcha. Give me a sec // I think this is not the original code you sent. Does not matter because on lines 62, 62, lazy should have been upgraded in the 'normal version'. But here, let me ask what does val store at every node? Oh crap. We can't do that update. Dude, we can't do anything. How will your query be? Like how will you ever know whether you are done or need to see children when the entire node contains one value. I meaan that root contains a certain value, then its children diferrent etc. Yeah, i got it. Need to read the official explanation on the blog.NONONONON. Why do you give up so easily? Think of something. ANYTHING. O kaTHink of the solution without seg tree if needed. I think here you don't even need prioriy queue. Just some good old set or something.
		
		// Hmm. Okay, wait. But I mean this problem was meant to be solved by reading the blog, right? Yes but you are unable to find that solution till now. I haven't read it yet, you mean? You have not read the blog? I decided to challenge myself and solve this without reading the blog. My man. Now, don't give up so fast. Think what does l, r tell you when you get a range. I don't understand your question. Like if posters are: 2-3, 4-5 and a new one comes along 1-5. It tells me the number of posters I cover or overlap?? Think more like in segtree terms. Idk what you're saying but does it mean number of affected nodes? Your original idea is good. You just need to modify and think through about what happens in case of overlapping ranges and all. Like if a range overlaps how do you know that while querying finally and if it covers everything what changes? Think why was this problem under the lazy section when you did not even use it? Hm. If there are different values in that node it means the range overlaps. We can just add 1 to [L, R] whenever we add a new poster. ??? WUT? add 1 to what? We create a segtree on [1, 1e7]. Then we want to add a poster covering [L, R]. We put update(L, R, 1). Now, when we want to find distinct, we just do that using a set??? What do you mean using a set to find distinct? Is this a mergesort tree thingy? No. Give me a minute. This obviously won't work. Why do you want to add 1? How do we take care of adding a new range?? You were setting the value of the node covering to that value right? Yeah but that isn't possible to do, right? Not withoug Lazy. Enough said . Figure it out.
		
		// Do we put a flag at a node when there are multiple values in that range? That was my original idea when I saw the problem but apparently their is even a more clever one. But I believe it should work if implemented correctly. Also, think about final query as well. Final query would just be s.insert(query(i, i))? then cout << s.size()?. Just take care not to put those values in the set which have been covered by their parents with a new value. Now, I don't wanna give away anything more. GTG. Okay bye
		
		
		
		
		
		
		
		
		
		
		

        
        // 2 - 3
        // 4 - 5
        

		if(curr->from != curr->to)
		{
			curr->extend();
			curr->left->lazy = curr->lazy;
			curr->right->lazy = curr->lazy;
		}
		curr->lazy = 0;
	}

	if((curr->from) > r || (curr->to) < l || (curr->from) > (curr->to))
		return;
	if(curr->from >= l && curr->to <= r)
	{
		curr->val = val;
		if(curr->from != curr->to)
		{
			curr->extend();
			curr->left->val = val;
			curr->right->val = val;
		}
		return;
	}
	curr->extend();
	update(curr->left, l, r, val);
	update(curr->right, l, r, val);
//	curr->val = curr->left->val + curr->right->val;
}


ll query(node *curr, ll l, ll r)
{
	if(curr->from > curr->to || curr->from > r || curr->to < l)
		return 0;
	if(curr->lazy)
	{
		curr->val += (curr->to - curr->from + 1)*curr->lazy;
		curr->extend();
		curr->left->lazy += curr->lazy;
		curr->right->lazy += curr->lazy;
		curr->lazy = 0;
	}

	if(curr->from >= l && curr->to <= r)
		return curr->val;

	ll q1, q2;
	q1 = query(curr->left, l, r), q2 = query(curr->right, l, r);
	return q1 + q2;
}


int main()
{
	ll t;
	cin >> t;
	while(t--)
	{
		
	}
}