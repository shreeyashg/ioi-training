/*
 ID: shreero1
 TASK: maze1
 LANG: C++11
*/
#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MW = 38 + 4, MH = 100 + 4;
vector<string> grid;
ll w, h, vis[2 * MH + 1][2 * MW + 1];
map<pair<ll, ll> , ll> dist, dist2;

void bfs(ll initx, ll inity)
{
	memset(vis, 0, sizeof(vis));
	queue<pair<ll, ll> >q;
	q.push({initx, inity});
	dist[{initx, inity}] = 1;
	while(!q.empty())
	{
		auto t = q.front();
		q.pop();
		ll x = t.first;
		ll y = t.second;

		//cout << x << " " << y << endl;

		if(vis[x][y])
			continue;
		vis[x][y] = 1;


		if(grid[x + 1][y] == ' ' && !vis[x + 2][y])
			dist[{x + 2, y}] = dist[t] + 1, q.push({x + 2, y});
		if(grid[x - 1][y] == ' ' && !vis[x - 2][y])
			dist[{x - 2, y}] = dist[t] + 1, q.push({x - 2, y});
		if(grid[x][y + 1] == ' ' && !vis[x][y + 2])
			dist[{x, y + 2}] = dist[t] + 1, q.push({x, y + 2});
		if(grid[x][y - 1] == ' ' && !vis[x][y - 2])
			dist[{x, y - 2}] = dist[t] + 1, q.push({x, y - 2});
	}
}

int main()
{
	ifstream cin("maze1.in");
	ofstream cout("maze1.out");
	cin >> w >> h;
	cin.ignore();
	REP(i, 2*h + 1)
	{
		string str;
		getline(cin, str);
		grid.push_back(str);	
	}

	vector<pair<ll, ll> > v;
	REP(i, 2*w + 1)
	{
		if(grid[0][i] == ' ')
			grid[0][i] = '*', v.push_back({0 + 1, i});
		if(grid[2*h][i] == ' ')
			grid[2*h][i] = '*', v.push_back({2 * h - 1 , i});
	}

	REP(i, 2*h + 1)
	{
		if(i == 0 || i == 2*h)
			continue;
		if(grid[i][0] == ' ')
			grid[i][0] = '*', v.push_back({i, 0 + 1});
		if(grid[i][2*w] == ' ')
			grid[i][2*w] = '*', v.push_back({i, 2*w - 1});
	}

	bfs(v[0].first, v[0].second);
	dist2 = dist;
	dist.clear();
	bfs(v[1].first, v[1].second);
	ll ans = 0;
	for(ll i = 1; i < 2 * h + 1; i += 2)
	{
		for(ll j = 1; j < 2 * w + 1; j += 2)
		{
			ans = max(ans, min(dist[{i, j}], dist2[{i, j}]));
		}

	}

	cout << ans << endl; 

}