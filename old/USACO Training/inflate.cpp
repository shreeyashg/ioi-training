/*
 ID: shreero1
 TASK: inflate
 LANG: C++11
*/

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;
vector<pair<ll, ll> > v;
ll dp[MX];

int main()
{
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout.tie(0);
	
	ifstream cin("inflate.in");
	ofstream cout("inflate.out");
	ll m, n;
	cin >> m >> n;

	REP(i, n)
	{
		ll x, y;
		cin >> x >> y;
		v.push_back({x, y});
	}

	sort(v.begin(), v.end());
	REP(i, m + 1)
	{
		REP(j, n)
		{
			if(i - v[j].second >= 0) dp[i] = max(dp[i], dp[i-v[j].second] + v[j].first);
		}
	}

	cout << dp[m] << endl;
}