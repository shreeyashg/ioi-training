/*
 ID: shreero1
 TASK: cowtour
 LANG: C++11
*/

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 150 + 4, X = 1e8;

ll n;
long double dist[MX][MX];
vector<string> adj;
vector<pair<long double, long double> >v;
vector<long double> maxes;

inline long double get(ll x, ll y)
{
	long double xx = v[y].first - v[x].first;
	long double yy = v[y].second - v[x].second;
	return sqrt((xx*xx) + (yy*yy));
}

int main()
{
	
	ifstream cin("cowtour.in");
	ofstream cout("cowtour.out");



	cin >> n;
	REP(i, n)
	{
		ll x, y;
		cin >> x >> y;
		v.push_back({x, y});	
	}


	REP(i, n)
	{
		REP(j, n)
		{
			dist[i][j] = 1e8;
		}
	}

	string str = "";
	REP(i, n)
	{
		// string str;
		cin >> str;
		adj.push_back(str);
	}

	REP(i, n)
	{
		REP(j, n)
		{
			dist[j][j] = 0;
			if(adj[i][j] == '1')
			{
				dist[i][j] = get(i, j);
			}	
		}
	}
/*
	REP(i, n)
	{
		REP(j, n)
		{
			cout << dist[i][j] << " ";
		}
		cout << endl;
	}
*/
	REP(k, n)
	{
		REP(i, n)
		{
			REP(j, n)
			{
				if(dist[i][k] + dist[k][j] < dist[i][j])
					dist[i][j] = dist[i][k] + dist[k][j];
			}
		}
	}


	REP(i, n)
	{
		long double ans = 0;
		REP(j, n)
		{
			if(i != j && dist[i][j] != X)
				ans = max(ans, dist[i][j]);
		}
		maxes.push_back(ans);
	}

//	REP(i, maxes.size()) cout << maxes[i] << endl;

	long double ans = X;
	REP(i, n)
	{
		REP(j, n)
		{
			if(dist[i][j] == X)
			{
				double x = get(i, j);
				ans = min(ans, x + maxes[i] + maxes[j]);
			}
		}
	}

	//cout << ans << endl;

	REP(i, n)
	{
		if(maxes[i] > ans)
			ans = maxes[i];
	}

	cout << fixed << setprecision(6) << ans << endl;
}