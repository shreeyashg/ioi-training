/*
 ID: shreero1
 TASK: comehome
 LANG: C++11
*/
#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 150 + 4;

vector<pair<ll, ll> > v[MX];
ll cost[MX][MX], n, vis[MX], dist[MX];

int main()
{
	ifstream cin("comehome.in");
	ofstream cout("comehome.out");
	REP(i, MX) dist[i] = MOD;
	cin >> n;
	REP(i, n)
	{
		char a, b;
		ll c;
		cin >> a >> b >> c;
		if(cost[a][b] == 0)
			cost[a][b] = c;
		else 
			cost[a][b] = min(cost[a][b], c);
	}

	//cout << cost['d']['Z'] << endl;

	REP(i, MX)
	{
		REP(j, MX)
		{
			if(cost[i][j] != 0 && i!=j)
				v[i].push_back({j, cost[i][j]}), v[j].push_back({i, cost[i][j]});
		}
	}

	//cout << v[90].size() << endl;

	priority_queue< pair<ll, ll> , vector<pair<ll, ll> >, greater<pair<ll, ll> > > pq;
	pq.push({0, 'Z'});
	dist[0] = 0;


	while(!pq.empty())
	{
		auto x = pq.top();
		pq.pop();
		ll id = x.second;
		ll di = x.first;

		if(vis[id])
			continue;
		vis[id] = 1;

		//cout << id << endl;


		REP(i, v[id].size())
		{
			ll nid = v[id][i].first;
			ll ndi = di + v[id][i].second;

			if(!vis[nid] && ndi < dist[nid])
			{
				dist[nid] = ndi;
				pq.push({ndi, nid});
			}
		}

	}

	ll ans = INF;
	char ch = ' ';
	REP(i, MX)
	{
		if(dist[i] != MOD)
		{
			if(i <= 90 && i >= 65)
			{
				if(dist[i] < ans) ch = (char)i, ans = dist[i];
			}
		}
	}

	cout << ch << " " << ans << endl;

}