/*
 ID: shreero1
 TASK: ttwo
 LANG: C++11
*/
#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 11 + 4;

vector< pair<ll, ll> > v = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};
char grid[MX][MX];
ll vis[MX][MX][4][MX][MX][4];
ll fans = 0;

bool valid(pair<ll, ll> p)
{
	if(p.first < 0 || p.first > 9 || p.second < 0 || p.second > 9 || grid[p.first][p.second] == '*')
		return 0;
	return 1;
}

void go(pair<ll, ll> C, ll c, pair<ll, ll> F, ll f, ll cnt)
{
	//cout << C.first << " " << C.second << " | " << F.first << " " << F.second << endl;  

	if(C == F)
	{
		fans = cnt;
		return;
	}
	if(vis[C.first][C.second][c][F.first][F.second][f])
	{
		return;
	}
	vis[C.first][C.second][c][F.first][F.second][f] = 1;
	if(!valid({C.first + v[c].first, C.second + v[c].second}))
		c = (c + 1) % 4;
	else
		C = {C.first + v[c].first, C.second + v[c].second};

	if(!valid({F.first + v[f].first, F.second + v[f].second}))
		f = (f + 1) % 4;
	else
		F = {F.first + v[f].first, F.second + v[f].second};

	go(C, c, F, f, cnt + 1);
}

int main()
{
	ifstream cin("ttwo.in");
	ofstream cout("ttwo.out");
	pair<ll, ll> C, F;
	REP(i, 10)
	{
		REP(j, 10)
		{
			cin >> grid[i][j];
			if(grid[i][j] == 'C')
				C = {i, j};
			if(grid[i][j] == 'F')
				F = {i, j};
		}
	}

	go(C, 0, F, 0, 0);
	cout << fans << endl;
}