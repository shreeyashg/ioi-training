/*
 ID: shreero1
 TASK: humble
 LANG: C++11
*/

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, IMX = 2147483647;

map<ll, ll> mp;
ll k, n;

int main()
{
	cin >> k >> n;
	REP(f, k)
	{
		ll x;
		cin >> x;
		for(ll i = x; ; i *= x)
		{
			if(i > IMX || i/x > n)
				break;
			mp.insert({i, 1});
		}
	}

	ll cnt = 0;
	for(auto it = mp.begin(); it != mp.end(); it++)
	{
		auto x = *it;
		cnt ++;
		
		cout << cnt << " " << x.first << endl;
	}
}