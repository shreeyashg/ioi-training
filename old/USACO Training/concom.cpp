/*
 ID: shreero1
 TASK: concom
 LANG: C++11
*/

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 100 + 4;
ll n;
ll sum[MX], vis[MX];
ll g[MX][MX];

void dfs(ll idx)
{
	if(vis[idx])
		return;

	vis[idx] = 1;

	REP(i, MX)
	{
		sum[i] += g[idx][i];
		if(sum[i] > 50)
			dfs(i);
	}

}

int main()
{
	double d = 17.0/31.0;
	cout << d << endl;
	ifstream cin("concom.in");
	ofstream cout("concom.out"); 
	cin >> n;
	REP(i, n)
	{
		ll x, y, z;
		cin >> x >> y >> z;
	
		g[x][y] = z;
	}


	REP(i, MX)
	{
		memset(vis, 0, sizeof(vis));
		memset(sum, 0, sizeof(sum));
		dfs(i);
		REP(j, MX)
		{
			if(sum[j] > 50 && j!=i)
				cout << i << " " << j << endl;
		}
	}
}