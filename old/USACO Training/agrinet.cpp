/*
 ID: shreero1
 TASK: agrinet
 LANG: C++11
*/

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 100 + 4;
vector<pair<ll, ll> > v;
ll n;
ll arr[MX], rnk[MX];

ll finds(ll x)
{
	if(x != arr[x])
		arr[x] = finds(arr[x]);
	else
		return arr[x];
}

void init()
{
	FOR(i, 1, MX - 1)
	{
		arr[i] = i;
	}
}

void merge(ll x, ll y)
{
	ll rx = finds(x);
	ll ry = finds(y);

	if(rx == ry)
		return;

	if(rnk[rx] > rnk[ry])
		arr[ry] = rx;
	else if(rnk[rx] < rnk[ry])
		arr[rx] = ry;
	else
		arr[ry] = rx, rnk[rx] += 1;
}


int main()
{
	init();
	merge(1, 3);
	merge(2, 4);
	merge(1, 4);
	REP(i, 5) cout << finds(i) << endl;
	cin >> n;
	REP(i, n)
	{
		REP(j, n)
		{
			ll x, y;
			cin >> x >> y;
			v.push_back({x, y});
		}
	}


}