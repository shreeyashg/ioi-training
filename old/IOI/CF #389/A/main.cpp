#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstdio>
#include <cstring>
#include <set>
#include <algorithm>
#include <cmath>
#include <bitset>

using namespace std;

typedef long long ll;

#define FOR(i, a, b) for(ll i=a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define REP(i, n) for(ll i=0; i<n; i++)
#define DEBUG(x) cout<<#x<<" : "<<x<<endl
#define lb(x, s) lower_bound(x.begin(), x.end(), s)
#define pb push_back
#define pi 3.1415926536

const ll MX = 104;

ll n, arr[MX];

int main()
{
        ll mx = 0;
        cin>>n;
        REP(i, n)
        {
                cin>>arr[i];
                mx = max(mx, arr[i]);
        }
        ll ans=0;
        REP(i, n)
        {
                ans += mx-arr[i];
        }
        cout<<ans<<endl;
}