#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstdio>
#include <cstring>
#include <set>
#include <algorithm>
#include <iomanip>
#include <cmath>
#include <bitset>

using namespace std;

typedef long long ll;

#define FOR(i, a, b) for(ll i=a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define REP(i, n) for(ll i=0; i<n; i++)
#define DEBUG(x) cout<<#x<<" : "<<x<<endl
#define lb(x, s) lower_bound(x.begin(), x.end(), s)
#define pb push_back
#define pi 3.1415926536

double n, m, k, x, y;

int main()
{
        cin>>n>>m>>k>>x>>y;
        double pupils = n*(m);
        ll q = k-((n-1)*(m))/pupils;
        if(n==1)
        {
                ll times = k/m;
                cout<<times+((((ll)k%(ll)m)>0))<<" "<<times;
                ll md = (ll)k%(ll)m;
                if(y<=md)
                        cout<<" "<<times+1<<endl;
                else
                        cout<<times<<endl;
                exit(0);
        }
        
        double times=k/((n-1)*m);
        cout<<fixed<<times<<endl;
        cout<<fixed<<floor(times)<<" "<<ceil(k/(2*(n-1)*m));
}