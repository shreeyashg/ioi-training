#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstdio>
#include <cstring>
#include <set>
#include <algorithm>
#include <cmath>
#include <bitset>

using namespace std;

typedef long long ll;

#define FOR(i, a, b) for(ll i=a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define REP(i, n) for(ll i=0; i<n; i++)
#define DEBUG(x) cout<<#x<<" : "<<x<<endl
#define lb(x, s) lower_bound(x.begin(), x.end(), s)
#define pb push_back
#define pi 3.1415926536

string str;

double r, b, y, g;
double mr, mb, my, mg;

int main()
{
        cin>>str;
        REP(i, str.size())
        {
                if(str[i]=='R')
                        mr = i%4;
                if(str[i]=='B')
                        mb = i%4;
                if(str[i]=='G')
                        mg = i%4;
                if(str[i]=='Y')
                        my = i%4;
        }
        
        //cout<<mr<<" "<<mb<<" "<<my<<" "<<mg<<endl;

        REP(i, str.size())
        {
                if(i%4==mr && str[i]=='!')
                        r++;
                if(i%4==mb && str[i]=='!')
                        b++;
                if(i%4==my && str[i]=='!')
                        y++;
                if(i%4==mg && str[i]=='!')
                        g++;
        }
        
        cout<<r<<" "<<b<<" "<<y<<" "<<g<<endl;
}