#include <bits/stdc++.h>
#include <ext/pb_ds/detail/standard_policies.hpp>
using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i=a; i<=b; i++)
#define DEBUG(x) cout<<#x<<" = "<<x<<endl

typedef long long ll;
typedef pair<ll, ll> pll;

const ll MX = 1e5 + 5, MOD = 1e9 + 7;
ll arr[MX], black[MX], n, vis[MX], dp[MX][2], old[2];
vector<ll> v[MX];

// dp[v][k] = No. of ways vertex v has k black vertices in subtree

void dfs(ll idx)
{
        dp[idx][0] = !black[idx];
        dp[idx][1] = black[idx];
        vis[idx]=1;
        REP(i, v[idx].size())
        {
                ll u = v[idx][i];
                if(!vis[u])
                {
                        dfs(u);
                        old[0] = dp[idx][0];
                        old[1] = dp[idx][1];
                        dp[idx][0] = dp[idx][1] = 0;
                        
                        
                        
                        // do not include
                        dp[idx][0] += (dp[u][1]*old[0])%MOD;
                        dp[idx][1] += (dp[u][1]*old[1])%MOD;
                        
                        // include
                        dp[idx][1] += (dp[u][0]*old[1])%MOD;
                        dp[idx][1] += (dp[u][1]*old[0])%MOD;
                        
                        dp[idx][0] += (dp[u][0]*old[0])%MOD;
                        
                        dp[idx][1]%=MOD;
                        dp[idx][0]%=MOD;
                }
        }
}

int main()
{
        cin>>n;
        REP(i, n-1)
        {
                ll k;
                cin>>k;
                v[k].push_back(i+1);
                v[i+1].push_back(k);
        }
        REP(i, n) cin>>black[i];
        
        dfs(0);
        cout<<dp[0][1]%MOD<<endl;
}