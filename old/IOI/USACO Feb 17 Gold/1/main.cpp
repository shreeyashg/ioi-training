#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <sstream>
#include <fstream>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

typedef long long ll;

ll field[102][102];
ll n, t;

const ll INF = 1e9 + 3;

ll rec(ll x, ll y, ll cnt, vector<vector<bool> > vis)
{
       // cout<<x<<" "<<y<<" "<<cnt<<endl;
        if(x>n || x<1 || y>n || y<1 || vis[x][y])
                return INF;
        
        if(x==y && x==n)
        {
                return ((cnt==3)*field[n][n]);
        }
        
        vis[x][y]=1;
        ll ans=0,newcnt=0;
        if(cnt==3)
                ans+=field[x][y], newcnt=1;
        else
                newcnt = cnt+1;
        ans+=t;
        
        ll ff=INF;
        ff = min(ff, ans + rec(x+1, y, newcnt, vis));
        ff = min(ff, ans + rec(x, y+1, newcnt, vis));
        ff = min(ff, ans + rec(x-1, y, newcnt, vis));
        ff = min(ff, ans + rec(x, y-1, newcnt, vis));
        
        return ff;
}

int main()
{
        ifstream cin("visitfj.in");
        ofstream cout("visitfj.out");
        cin>>n>>t;
        REP(i, n)
        {
                REP(j, n)
                {
                        cin>>field[i+1][j+1];
                }
        }
        
        vector<vector<bool> >v;
        FOR(i, 1, n+1)
        {
                vector<bool> x;
                x.push_back(0);
                FOR(j, 1, n)
                {
                        x.push_back(0);
                }
                v.push_back(x);
        }
        cout<<rec(1, 1, 0, v)<<endl;
}