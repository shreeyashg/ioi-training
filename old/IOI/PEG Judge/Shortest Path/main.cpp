#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

ll n;
vector<string> v;
vector<char> ops;

void p(string str, ll x)
{
        FOR(i, x, str.size()-1)
        {
                ops.push_back(str[i]);
        }
        ops.push_back('P');
}

bool cmp(string x, string y)
{
        return x.size()<y.size();
}

int main()
{
        cin>>n;
        REP(i, n)
        {
                string str;
                cin>>str;
                v.push_back(str);
        }
        sort(v.begin(), v.end());
        stable_sort(v.begin(), v.end(), cmp);
    //    REP(i, v.size()) cout<<v[i]<<endl;
        p(v[0], 0);
        ll curr = v[0].size()-1;
        FOR(i, 1, v.size()-1)
        {
                while(curr>=0 && (curr>v[i].size()-1 || v[i].substr(0,curr+1)!=v[i-1].substr(0,curr+1)))
                        curr--,ops.push_back('-');
                p(v[i], curr+1);
                curr=v[i].size()-1;
        }
        cout<<ops.size()<<endl;
        REP(i, ops.size()) cout<<ops[i]<<endl;
}