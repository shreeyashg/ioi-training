#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 101 //10^5 + 6
#define MOD 2000000007 // 10^9 + 7
#define INF MOD // 10^18 + 5

#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))

ll cost, coins[33], dp[MX];
ll n;

int main()
{
        //cost=0;
        //while(cost<=5280)
        {
                //cin>>cost;
                cin>>cost;
                cin>>n;
                ll mx=0;
                REP(i,n)
                {
                        /*coins[i]=rand()%101;
                        if(coins[i]==0) coins[i]++;*/
                        cin>>coins[i];
                        mx = max(mx, coins[i]);
                }
                DEBUG(mx);
                REP(i, MX)
                {
                        dp[i]=MOD;
                }
                
                dp[0]=0;
                FOR(i, 1, cost)
                {
                        dp[i%mx]=INF;
                        REP(j, n)
                        {
                                if(coins[j]<=i)
                                {
                                        if(dp[(i-coins[j])%mx]!=MOD)dp[i%mx]=min(dp[i%mx], dp[(i-coins[j])%mx]+1);
                                        
                                }
                        }
                        cout<<i<<" "<<dp[(i)%mx]<<endl;
                }
                
                
                
                //cost++;
        }
}