#pragma GCC optimize("O3")
#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

ll x = 100;

vector<ll> dp(101, 19999999);
void mincoins(vector<ll> coins)
{
        
        FOR(ii, 1, 100) dp[ii]=55555;
        dp[0]=0;
        FOR(i, 1, x)
        {
                REP(j, coins.size())
                {
                        if(i-coins[j]>=0)
                                dp[i] = min(dp[i], 1+dp[i-coins[j]]);
                }
                //  cout<<i<<" "<<dp[i]<<endl;
        }
}

int main()
{
        double d=20;
        FOR(i, 1, 1)
        {
                FOR(j, i+1, 5)
                {
                        FOR(k, j+1, 10)
                        {
                                FOR(l, k+1, 19)
                                {
                                        FOR(m, l+1, 25)
                                        {
                                                FOR(n, m+1, 33)
                                                {
                                                        FOR(o, n+1, 50)
                                                        {
                                                                FOR(p, o+1, 50)
                                                                {
                                                                        vector<ll> x = {i, j, k, l, m, n, o, p};
                                                                        mincoins(x);
                                                                        double sum=0;
                                                                        FOR(ii, 1, 100) sum+=dp[ii];
                                                                        if(sum/100.0 < d)
                                                                        {
                                                                                cout<<i<<" "<<j<<" "<<k<<" "<<l<<" "<<m<<" "<<n<<" "<<o<<" "<<o<<" "<<sum/100.0<<endl;
                                                                                d = sum/100.0;
                                                                        }

                                                                }
                                                        }
                                                }
                                        }
                                }
                        }
                }
        }
}