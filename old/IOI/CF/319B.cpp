#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n;

int main()
{
	vector<ll> v;
	cin >> n;
	REP(i, n)
	{
		ll k;
		cin >> k;
		v.push_back(k);
	}

	vector<ll> pre = v;
	ll cnt = 0;

	if(is_sorted(v.begin(), v.end()))
		cout << 0 << endl, exit(0);

	while(true)
	{
		vector<ll> x;
		ll first = pre[0]; 
		x.push_back(first);
		REP(i, pre.size() - 1)
		{
			if(pre[i] < pre[i + 1])
				first = pre[i + 1], x.push_back(first);
		}
	/*	REP(i, x.size())
		{
			cout << x[i] << " ";
		}
		cout << endl; */
		if(x!=pre)cnt++;
		if(x == pre)
			cout << cnt << endl, exit(0);
		pre = x;
	}
}