#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n, a, b, c, d;

int main()
{
	cin >> n >> a >> b >> c >> d;
	// Iterate over x1
	// Multiply by n

	ll ans = 0;

	FOR(i, 1, n)
	{
		ll tl = a + b + i;
		ll x4 = a + i -d;
		ll x2 = b + i -c;
		ll x5 = a + b + i - c - d;
		if(x4 > 0 && x4 <= n && x2 > 0 && x2 <= n && x5 > 0 && x5 <= n)
			ans ++;
	}

	cout << ans * n << endl;
}