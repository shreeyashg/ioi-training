#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll h, ext;

int main()
{
	cin >> h >> ext;
	bool nxt = 0;
	ll l = 1LL, r = (1LL << h), ans = 0;
	while(h >= 0)
	{
		if(l == r) break;
		//cout << ans << " " << l << " " << r << endl;
		if(ext >= l && ext <= (l+r)/2 && !nxt)
			ans ++, r = (l + r)/2;
		else if(ext >= (l + r)/2 + 1 && ext <= r && nxt)
			ans ++, l = (l + r)/2 + 1;
		else
		{
			ans += (1LL << h) - 1;
			if(!nxt)
				l = (l + r)/2 + 1;
			else
				r = (l + r)/2;
			ans ++;
			nxt = !nxt;
		}
		nxt = !nxt;
		h--;
	}
	//cout << ans << " " << l << " " << r << endl;

	cout << ans << endl;
}