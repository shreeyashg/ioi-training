#pragma GCC optimize("O3")
#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

vector<vector<ll> > v{ {0, 1, 2} ,{1,0,2}, {1,2,0}, {2,1,0}, {2, 0, 1}};

int main()
{
        ll n,k;
        cin>>n>>k;
        cout<<v[n%6][k]<<endl;
}