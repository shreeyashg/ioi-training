#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MX = 3e5 + 4;
const ll MOD = 1e9 + 7, INF = 1e18;
set<ll> s;
ll n, arr[MX];
map<pair<ll, ll>, ll> mp;
int main()
{
	cin >> n;

	s.insert(-INF);
	s.insert(INF);
	mp[{-INF, INF}] = 1;
	REP(i, n)
	{
		ll x;
		cin >> x;
		if(i == 0)
			continue;
		ll f, l;
		auto it = s.upper_bound(x);
		it--;
		f = *it;
		it++;
		l = *it;
		cout << mp[{f, l}]/2 << " ";
		s.insert(x);
		mp[{f, x}] = (2*mp[{f, l}]) % MOD;
		mp[{x, l}] = (2*mp[{f, l}] + 1) % MOD;
	}
	cout << endl;
}