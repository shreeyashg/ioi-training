#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

vector<pair<ll, pair<ll, ll> > > v;
ll n, m;



int main()
{
	cin >> n >> m;
	REP(i, m)
	{
		ll w, a, b;
		cin >> a >> b >> w;
		v.push_back({w, {a, b}});
	}

	sort(v.begin(), v.end());
	REP(i, v.size())
	{
		cout << v[i].second.first << " " << v[i].second.second << " " << v[i].first << endl;
	}

	
}