#pragma GCC optimize("O3")
#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

ll n, m;
ll ps[100005];
vector<vector<ll> > v, pp,pps;

int main()
{
        memset(ps, -1, sizeof(ps));
        cin>>n>>m;
        v.resize(n+1, vector<ll>(m+1));
        pp=v;
        pps=v;
        ll k;
        
        FOR(i ,1, n)
        {
                FOR(j, 1 ,m)
                {
                        cin>>v[i][j];
                }
        }
        
        FOR(i, 1, m)
        {
                FOR(j, 1 ,n)
                {
                        if(v[j][i]>=v[j-1][i])
                                pp[j][i]=1;
                        else
                                ps[i+1] = 0;
                }
                if(ps[i+1]==-1)
                        ps[i+1]=1;
        }
        
        FOR(c, 1, m)
        {
                FOR(x, 1, n)
                {
                        pps[x][c] += pps[x-1][c] + pp[x][c];
                        
                }
        }
        
        FOR(i, 1, n)
        {
                FOR(j, 1, m)
                {
                        cout<<pps[i][j]<<" ";
                }
                cout<<endl;
        }
        
        ll d;
        cin>>d;
        REP(i, d)
        {
                ll x, y;
                cin>>x>>y;
                bool ff=0;
                if(x==y)
                {
                        cout<<"Yes"<<endl;
                        continue;
                }
                FOR(i, 1, m)
                {
                 //       DEBUG(y);
                   //     DEBUG(x);
                    //    DEBUG(i);
                    //    DEBUG(pps[y][i]);
                      //  DEBUG(pps[x-1][i]);
                        if(pps[y][i]-pps[x][i]==y-x)
                        {
                                cout<<"Yes"<<endl;
                                ff=1;
                                break;
                        }
                        else
                                continue;
                }
                if(!ff)
                        cout<<"No"<<endl;
        }
}