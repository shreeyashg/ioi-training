#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n, m, k;
vector<pair<ll, ll> > v[MX];
ll exitp[MX];

int main()
{
	cin >> n >> m >> k;
	REP(i, m)
	{
		ll x, y, z;
		cin >> x >> y >> z;
		v[x].push_back({y, z});
		v[y].push_back({x, z});
	}
	REP(i, k)
	{
		ll x;
		cin >> x;
		exitp[x] = 1;
	}



}