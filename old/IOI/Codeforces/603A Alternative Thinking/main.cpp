#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 1000001
#define MOD 1000000007
#define INF 1000009*1e9

ll n;
string str;
vector<ll> v;

// index , state , prev
/*
ll rec(ll idx, ll state, ll prev)
{
        if(idx == n && (state == 2))
                return 0;
        else if(idx == n)
                return -INF;
        
        if(idx == 0)
        {
                
        }
}*/

int main()
{
        cin>>n>>str;
        REP(i, n)
        {
                v.push_back(str[i]-'0');
        }
        ll ans = 1;
        ll prev = str[0];
        REP(i, str.size())
        {
                if(str[i]!=prev && i!=0)
                {
                        ans++;
                        prev = str[i];
                }
        }
        ll curr = ans;
        //DEBUG(ans);
        FOR(i, 2, str.size()-1)
        {
                if(((str[i] == str[i-1] )&& ((str[i-1] == str[i-2])||(str[i] == str[i+1]))))
                {
                        ans+=2;
                        break;
                }
        }
        
        cout<<ans<<endl;
        //cout<<max(rec(0, 0, 0), rec(0, 1, 0))<<endl;
}