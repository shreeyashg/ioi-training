#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 1000001
#define MOD 1000000007
#define INF 1000009*1e9

ll n;
vector<pair<ll, ll> > v1, v2;
vector<ll> lefts, rights;

bool cmp(pair<ll, ll> a, pair<ll, ll> b)
{
        if(a.first == b.first)
                return a.second > b.second;
        else
                return a.first < b.first;
}

int main()
{
        ll mx = -INF;
        cin>>n;
        REP(i, n)
        {
                ll k, kj;
                cin>>k>>kj;
                v1.push_back({k, kj});
        }
        
        sort(v1.begin(), v1.end());
        //sort(v2.begin(), v2.end());//,
        
        REP(i, v1.size())
        {
                lefts.push_back(v1[i].first);
                rights.push_back(v1[i].second);//
        }
        
        if(is_sorted(rights.begin(), rights.end()))
        {
                cout<<rights[n-1]<<endl;
        }
        else
        {
                cout<<lefts[n-1]<<endl;
        }
}