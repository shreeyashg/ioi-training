#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 1001
#define MOD 1000000007
#define INF 1000009*1e9

ll n;
ll arr[MX];
multiset<ll> s;

int main()
{
        cin>>n;
        REP(i, n) cin>>arr[i], s.insert(arr[i]);
        ll ans=0;
        while(!s.empty() && s.size()>1 &&*s.rbegin()-*s.begin()>17)
        {
                ll b = *s.rbegin();
                ll a = *s.begin();
                ll curr = *s.rbegin()-*s.begin();
                ans+=((curr-17)/2)*((curr-17)/2);
                ans+=pow(((curr-17)/2) + (curr-17)%2, 2);
                s.erase(s.find(b));
                s.erase(s.find(a));
        }
        cout<<ans<<endl;
}