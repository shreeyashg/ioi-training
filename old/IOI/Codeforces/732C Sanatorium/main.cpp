#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

typedef long long ll;

int main()
{
        ll x, y, z;
        cin>>x>>y>>z;
        vector<ll> v, a;
        v.push_back(x);
        v.push_back(y);
        v.push_back(z);
        
        a.assign(v.begin(), v.end());
        ll sum=0;
        sort(a.begin(), a.end());
        sort(v.begin(), v.end());
        while(abs(v[0]-v[v.size()-1])>1)
        {
                v[0]+=(v[v.size()-1]-v[0]-1);
                //sum+=v[v.size()-1]-v[0]-1;
                sort(v.begin(), v.end());
        }
        
        for(ll i=0; i<v.size(); i++)
        {
                sum+=abs(v[i]-a[i]);
        }
        
        cout<<sum<<endl;
}