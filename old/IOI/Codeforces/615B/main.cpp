#include <iostream>
#include <vector>
#include <map>
#include <cstring>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i,a,b) for(ll i=a; i<=b; i++)

const ll MX = 1e5 + 4;
ll n, m;
//cin>>n>>m;

vector<ll> v[MX];
//map<ll, map<ll, ll > >mp;

ll dp[MX], size[MX], mark[MX];

ll rec(ll curr, ll sz)
{
        /*if(curr>n)
                return sz*v[last].size();*/
        if(dp[curr]!=-1)
                return dp[curr];
        
        ll ans=sz*v[curr].size();
        REP(i, v[curr].size())
        {
                if(v[curr][i]>curr)
                {
                        ans = max(rec(v[curr][i], sz+1), ans);
                }
        }
        
        return dp[curr] = max(dp[curr],ans);
}

ll getsz(ll x)
{
        if(size[x])
                return size[x];
        ll ans=0;
        mark[x]=1;
        REP(i, v[x].size())
        {
                if(v[x][i]<x)
                {
                        ans = max(ans, 1+getsz(v[x][i]));
                }
        }
        
        return size[x] = ans;
}

int main()
{
        memset(dp, -1, sizeof(dp));
        
        cin>>n>>m;
        REP(i, m)
        {
                ll a, b;
                cin>>a>>b;
                v[a].push_back(b);
                v[b].push_back(a);
        }
        
        ll ans=0;
        for(ll i=n; i>=1; i--)
        {
                if(!size[i])getsz(i);
        }
        
        FOR(i, 1, n)
        {
                size[i]++;
        }
        
        REP(i, n+1)
        {
                if(i!=0)
                {
                        ans = max((ll)size[i]*(ll)v[i].size(), ans);
                }
        }
        
        cout<<ans<<endl;
}