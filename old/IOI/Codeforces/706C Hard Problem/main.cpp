#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 100006 //10^5 + 6
#define MOD 1000000007 // 10^9 + 7
#define INF 1e18+5 // 10^18 + 5

vector<string> v, rev;
ll cost[MX];
ll dp[MX][2];
ll n;

int main()
{
        ios::sync_with_stdio(false);
        cin.tie(0);
        
        cin>>n;
        REP(i, n)
        {
                cin>>cost[i+1];
        }
        
        v.push_back(" ");
        rev.push_back(" ");
        
        REP(i, n)
        {
                string str;
                cin>>str;
                v.push_back(str);
                reverse(str.begin(), str.end());
                rev.push_back(str);
        }
        
        REP(i, n+1)
        {
                REP(j, 2)
                {
                        dp[i][j] = INF;
                }
        }
        
        dp[0][0] = 0;
        dp[0][1] = 0;
        dp[1][0] = 0;
        dp[1][1] = cost[1];
        FOR(i, 2, n)
        {
                if(v[i]>=v[i-1])
                {
                        dp[i][0] = min(dp[i][0], dp[i-1][0]);
                }
                if(v[i]>=rev[i-1])
                {
                        dp[i][0] = min(dp[i][0], dp[i-1][1]);
                }
                if(rev[i]>=v[i-1])
                {
                        dp[i][1] = min(dp[i][1], dp[i-1][0]+cost[i]);
                }
                if(rev[i]>=rev[i-1])
                {
                        dp[i][1] = min(dp[i][1], cost[i]+dp[i-1][1]);
                }
        }
        
        if(min(dp[n][0], dp[n][1]) == INF)
        {
                cout<<-1<<endl;
        }
        else
        {
                cout<<min(dp[n][0], dp[n][1])<<endl;
        }
}