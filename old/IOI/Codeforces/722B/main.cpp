#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <unordered_map>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 5008
#define MOD 1000000007

ll n, k;
vector<ll> a, b;

int main()
{
        cin>>n>>k;
        REP(i, n)
        {
                ll f;
                cin>>f;
                a.push_back(f);
                b.push_back(f);
        }
        
        ll ans=0;
        FOR(i, 1, n-1)
        {
                ll sum  = a[i]+a[i-1];
                if(sum<k) a[i]+=k-sum, ans+=k-sum;
        }
        
        if(n!=1)cout<<ans<<endl;
        else
                cout<<0<<endl;
        
        REP(i, a.size())
        {
                cout<<a[i]<<" ";
        }
        cout<<'\n';
}