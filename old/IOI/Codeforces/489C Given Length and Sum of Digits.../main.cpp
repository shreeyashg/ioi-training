#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 1000001
#define MOD 1000000007
#define INF 1000009*1e9

ll d,s;

bool can(ll digits, ll sum)
{
        return sum>=0 && sum<=digits*9;
}

int main()
{
        cin>>d>>s;
        if((s==0 && d>1) || s>d*9)
        {
                cout<<"-1 -1"<<endl;
                return 0;
        }
        
        
        
        ll sum=s, sum2=s;
        string str, st;
        str.resize(d, '0');
        st.resize(d, '0');
        REP(i, d)
        {
                str[i]='0';
                st[i]='0';
        }
        
        for(ll i=0; i<d; i++)
        {
                for(ll j=0; j<=9; j++)
                {
                        if(i==0 && j==0) continue;
                        if(can(d-i-1, sum2-j))
                        {
                                st[i] = char('0' + j);
                                sum2-=j;
                                break;
                        }
                }
        }
        
        for(ll i=0; i<d; i++)
        {
                for(ll j=9; j>=0; j--)
                {
                        if(i==0 && j==0)
                                continue;
                        if(can(d-i-1, sum-j))
                        {
                                str[i]=char('0' + j);
                                sum-=j;
                                break;
                        }
                }
        }
        
        cout<<st<<" "<<str<<endl;
}