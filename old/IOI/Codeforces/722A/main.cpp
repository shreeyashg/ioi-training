#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 100006
#define MOD 1000000007
#define INF 1000009*1e9

vector<pair<ll, pair<ll, ll> > >v;

int main()
{
        ll n;
        cin>>n;
        REP(i, n)
        {
                ll a, b, c;
                cin>>a>>b>>c;
                v.push_back({a, {b, c}});
        }
        sort(v.begin(), v.end());
        reverse(v.begin(), v.end());
        
        REP(i, v.size())
        {
                cout<<v[i].first<<" "<<v[i].second.first<<" "<<v[i].second.second<<endl;
        }
}