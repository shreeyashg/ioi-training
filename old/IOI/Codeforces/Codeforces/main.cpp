#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define ll long long
#define endl '\n'
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 100005
#define MOD 1000000007

ll n;
vector<ll> v, nv;
vector<pair<ll, ll> >a;
map<vector<ll>, ll> choice;

ll rec(vector<ll> v)
{
        if(v==nv)
                return 0;
        REP(i, v.size())
        {
                if(v[i]>nv[i])
                        return MOD;
        }
        ll ans=MOD;
        REP(i, v.size())
        {
                if(v[i]>v[i+1] && i!=v.size())
                {
                        vector<ll> q = v;
                        q[i]+=q[i+1];
                        q.erase(q.begin()+i+1);
                        ans = min(ans,1+rec(q));
                }
                if(v[i]<v[i+1] && i!=v.size())
                {
                        //a.push_back({i+2, 1});
                        //cout<<i+2<<" L"<<endl;
                        vector<ll> q = v;
                        q[i+1]+=q[i];
                        q.erase(q.begin()+i);//, <#const_iterator __last#>)
                        ans = min(ans,1+rec(q));
                }
                if(v[i]>v[i-1])
                {
                        //a.push_back({i+1, 1});
                        //cout<<i+1<<" L"<<endl;
                        vector<ll> q = v;
                        
                        q[i]+=q[i-1];
                        q.erase(q.begin()+i-1);
                        ans = min(ans,1+rec(q));
                }
                if(v[i]<v[i-1])
                {
                        vector<ll> q = v;
                        //a.push_back({i, 2});
                        //cout<<i<<" R"<<endl;
                        q[i-1]+=q[i];
                        q.erase(q.begin()+i);//, <#const_iterator __last#>)
                        ans = min(ans,1+rec(q));
                }
                
                
        }
        return choice[v] = ans;
}

int main()
{
        set<ll> s;
        ll sum=0;
        cin>>n;
        REP(i, n)
        {
                ll k;
                cin>>k;
                v.push_back(k);
                sum+=k;
        }
        ll nl;
        cin>>nl;
        REP(i, nl)
        {
                ll k;
                cin>>k;
                sum-=k;
                nv.push_back(k);
        }
        if(v!=nv && s.size()==1)
        {
                cout<<"NO"<<endl;
                exit(0);
        }
        
        
        //cout<<rec(v)<<endl;
        ll k=0;
        while(v!=nv)
        {
                k++;
                if(k>1000)
                {
                        cout<<"NO"<<endl;
                        exit(0);
                }
                if(sum!=0)
                {
                        //DEBUG("FSASF");
                        cout<<"NO"<<endl;
                        exit(0);
                }
                k++;
                REP(i, v.size())
                {
                        FOR(j, 1, 2)
                        {
                                if(v[i]!=nv[i])
                                {
                                        if(j==1 && i!=v.size()-1)
                                        {
                                                if(v[i]>v[i+1])
                                                {
                                                        //cout<<i+1<<" "<<'R'<<endl;
                                                        a.push_back({i+1, 2});
                                                        v[i]+=v[i+1];
                                                        v.erase(v.begin()+i+1);//, <#const_iterator __last#>)
                                                }
                                                if(v[i]<v[i+1])
                                                {
                                                        a.push_back({i+2, 1});
                                                        //cout<<i+2<<" L"<<endl;
                                                        v[i+1]+=v[i];
                                                        v.erase(v.begin()+i);//, <#const_iterator __last#>)
                                                }
                                        }
                                        
                                        if(j==2 && i!=0)
                                        {
                                                if(v[i]>v[i-1])
                                                {
                                                        a.push_back({i+1, 1});
                                                        //cout<<i+1<<" L"<<endl;
                                                        v[i]+=v[i-1];
                                                        v.erase(v.begin()+i-1);
                                                }
                                                if(v[i]<v[i-1])
                                                {
                                                        a.push_back({i, 2});
                                                        //cout<<i<<" R"<<endl;
                                                        v[i-1]+=v[i];
                                                        v.erase(v.begin()+i);//, <#const_iterator __last#>)
                                                }
                                        }
                                        
                                        if(v[i]!=nv[i] && v.size()==nv.size() && i<nv.size())
                                        {
                                               // DEBUG(i);
                                               // DEBUG(v[i]);
                                               // DEBUG(nv[i]);
                                                cout<<"NO"<<endl;
                                                exit(0);
                                        }
                                }
                        }
                }
        }
        cout<<"YES"<<endl;
        REP(i, a.size())
        {
                cout<<a[i].first<<" ";
                if(a[i].second==2)
                        cout<<"R"<<endl;
                else
                        cout<<"L"<<endl;
        }
}