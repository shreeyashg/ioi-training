#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <tuple>
#include <unordered_map>
#include <iomanip>
#include <functional>
using namespace std;

typedef long long ll;
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 100002
#define MOD 1000000007
#define INF 1000009*1e9

string d[] = {"S","M", "L", "XL", "XXL", "XXXL"};
vector<vector<string> >s;
ll val[7], n;

ll getidx(string str)
{
        REP(i, 6)
        {
                if(d[i]==str)
                        return i;
        }
        return INF;
}

int main()
{
        ll tot=0;
        REP(i, 6)
        {
                cin>>val[i];
                tot+=val[i];
        }
        cin>>n;
        if(tot<n)
        {
                cout<<"NO"<<endl;
                return 0;
        }
        
        vector<vector<string>>in;
        vector<string> res(n);
        REP(i, n)
        {
                string sr;
                cin>>sr;
                vector<string> x;
                bool c=0;
                REP(i, sr.size())
                {
                        if(sr[i]==',')
                        {
                                c=1;
                                x.push_back(sr.substr(0, i));
                                x.push_back(sr.substr(i+1, sr.size()-i));
                        }
                }
                if(!c) x.push_back(sr);
                in.push_back(x);
        }
        
        while(n--)
        {
                auto i=0;;
                for(i = 0; i<in.size(); i++)
                {
                        if(in[i].empty())
                        {
                                continue;
                        }
                        else
                                break;
                }
                if(i == in.size()) break;
                
                REP(j, 6)
                {
                        if(val[j]<0)
                        {
                                cout<<"NO"<<endl;
                                exit(0);
                        }
                }
                
                REP(i, in.size())
                {
                        if(in[i].size()==1)
                        {
                                val[getidx(in[i][0])]--;
                                res[i] = in[i][0];
                                in[i].clear();
                        }
                }
                
                REP(i, in.size())
                {
                        if(in[i].size()==2)
                        {
                                if(val[getidx(in[i][0])])
                                {
                                        val[getidx(in[i][0])]--;
                                        res[i] = in[i][0];
                                }
                                else
                                {
                                        val[getidx(in[i][1])]--;
                                        res[i] = in[i][1];
                                }
                        }
                }
        }
        
        REP(j, 6)
        {
                if(val[j]<0)
                {
                        cout<<"NO"<<endl;
                        exit(0);
                }
        }
        
        cout<<"YES"<<endl;
        REP(i, res.size())
        {
                cout<<res[i]<<endl;
        }
}