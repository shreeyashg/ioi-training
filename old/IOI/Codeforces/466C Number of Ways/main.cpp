#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 1000006
#define MOD 1000000007
#define INF 1000009*1e9

ll n;
ll arr[MX];
ll cnt[MX];
ll ps[MX];
ll sums[MX];

int main()
{
        ios::sync_with_stdio(false);
        cin.tie(0);
        ll s=0;
        ps[0]=0;
        cin>>n;
        REP(i, n)
        {
                cin>>arr[i+1];
                ps[i+1]=arr[i+1]+ps[i];
                s+=arr[i+1];
        }
        
        if(s%3!=0)
        {
                cout<<0<<endl;
                exit(0);
        }
        
        
        FOR(i, 1, n)
        {
         //       DEBUG(ps[n]-ps[i-1]);
           //     DEBUG(s/3);
                if(ps[n]-ps[i-1]==(s/3))
                {
                        //DEBUG("YEAH");
                        cnt[i]=1;
                }
                else
                {
                        //DEBUG("NO");
                        cnt[i]=0;
                }
        }
        
        sums[n] = cnt[n];
        
        FORD(i, n-1, 1)
        {
                sums[i]=sums[i+1]+cnt[i];
        }
        
        ll ans=0;
        
        FOR(i, 1, n-2)
        {
                if(ps[i]==s/3)
                {
                        ans+=(sums[i+2]);
                }
        }
        
        cout<<ans<<endl;
}