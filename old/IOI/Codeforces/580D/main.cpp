#include <iostream>
#include <cstring>
using namespace std;

#define ll long long
#define REP(i, n) for(int i=0; i<n; i++)
#define FOR(i, a, b) for(int i = a; i<=b; i++)
#define FORD(i, a, b) for(int i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MOD 1000000007 // 10^9 + 7
#define INF 1e18+5 // 10^18 + 5

const ll MX = 22;
const ll FF = 1<<20;

int s[MX];
int boost[MX][MX];
int n, m, k;

ll dp[FF][MX];

ll rec(int x, int last)
{
        
        if(dp[x][last]!=-1)
                return dp[x][last];
        
        if(__builtin_popcount(x)>=m)
        {
                return 0;
        }
        
        ll ans=0;
        
        FOR(i, 1, n)
        {
                if(!(x&(1<<i)))
                {
                        ans = max(ans, s[i]+boost[last][i]+rec(x|(1<<i), i));
                }
        }
        return dp[x][last] = ans;
}

int main()
{
        //ll f = 1<<20;
        //DEBUG(f);
        memset(dp, -1, sizeof(dp));
        cin>>n>>m>>k;
        REP(i, n)
        {
                cin>>s[i+1];
        }
        REP(i, k)
        {
                ll a, b, c;
                cin>>a>>b>>c;
                boost[a][b]=c;
        }
        
        ll ans=0;
        FOR(i, 1, n)
        {
                ans = max(ans, s[i]+rec(1<<i, i));
        }
        cout<<ans<<endl;
}