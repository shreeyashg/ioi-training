#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 1000001
#define MOD 1000000007
#define INF 1000009*1e9

ll n;
vector<pair<ll, ll> >v;

bool cmp(pair<ll, ll> a, pair<ll, ll> b)
{
        if(a.second==b.second)
        {
                return a.first>b.first;
        }
        return a.second<b.second;
}

int main()
{
        cin>>n;
        REP(i , n)
        {
                ll a, b;
                cin>>a>>b;
                v.push_back({a,b});
        }
        
        sort(v.begin(), v.end(), cmp);
        
        ll ans=1;
        ll last = v[0].second;
        REP(i, v.size())
        {
                if(i!=0)
                {
                        if(v[i].first>last)
                        {
                                last = v[i].second;
                                ans++;
                        }
                }
        }
        
        cout<<ans<<endl;
}