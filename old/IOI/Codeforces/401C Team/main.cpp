#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 1000001
#define MOD 1000000007
#define INF 1000009*1e9

ll z, o;

int main()
{
        cin>>z>>o;
        if(z-o>1 || o>(2*(z+1)))
        {
                cout<<-1<<endl;
                exit(0);
        }
        
        string str;
        str.resize(z+o);
        if(o>z)
                str[0] = '1', o--;
        else
                str[0] = '0', z--;
        
        if(o>z || str[0]== '0')
                str[1] = '1', o--;
        else
                str[1] = '0', z--;
        
        //DEBUG(str);
        
        //DEBUG(o);
        //DEBUG(z);
        
        FOR(i, 2, str.size()-1)
        {
                if(str[i-1] == '1' && str[i-2] == '1')
                {
                        if(z)
                        {
                                str[i] = '0', z--;
                        }
                        else
                                cout<<"SHIT"<<endl;
                        continue;
                        
                }
                if(o>z && o)
                {
                        str[i] = '1';
                        o--;
                        continue;
                }
                else if(z)
                {
                        if(str[i-1]=='0')
                        {
                                str[i] = '1';
                                o--;
                        }
                        else
                        {
                                str[i] = '0';
                                z--;
                        }
                        continue;
                }
        }
        //cout<<o<<" "<<z<<endl;
        cout<<str<<endl;
}