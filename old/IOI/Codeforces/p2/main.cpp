#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <stack>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 10001
#define MOD 1000000007
#define INF 1000009*1e9

ll v[MX], b[MX];


ll n, k;
ll mp[1000][1000];

ll max(ll a, ll b, ll c, ll d)
{
        return max(max(max(a, b), c), d);
}
ll max(ll a, ll b, ll c)
{
        return max(max(a, b), c);
}

ll rec(ll i, ll j)
{
        if(i>=j)
                return 0;
        if(mp[i][j]!=-MOD)
                return mp[i][j];
        if(b[j] == b[i] + k)
        {
                return mp[i][j] = max(v[i]+v[j]+rec(i+1, j-1)+rec(j+1, n-1), (rec(i+1, j)), rec(i, j-1));
        }
        else
                return mp[i][j] = max(mp[i][j], rec(i+1, j))
                ;
}

int main()
{
        REP(i, 1000)
        {
                REP(j, 1000)
                {
                        mp[i][j] = -MOD;
                }
        }
        cin>>n>>k;
        REP(i, n)
        {
                cin>>v[i];
        }
        REP(i, n)
        {
                cin>>b[i];
        }
        ll ans = -INF;
        REP(i, n)
        {
                REP(j, n)
                {
                        if(b[j] == b[i] + k)
                                ans = max(ans, v[i]+v[j]);
                }
        }
        stack<ll> s;
        if(rec(0, n-1)==0)
                cout<<ans<<endl;
        else
                cout<<rec(0, n-1)<<endl;
}