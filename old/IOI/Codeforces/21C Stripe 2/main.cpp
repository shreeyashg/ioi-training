#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 1000006
#define MOD 1000000007
#define INF 1000009*1e9

ll n;
ll arr[MX/2];
ll cnt[MX/2];
ll ps[MX/2];

int main()
{
        ios::sync_with_stdio(false);
        cin.tie(0);
        cin>>n;
        ps[0]=0;
        REP(i, n)
        {
                cin>>arr[i+1];
                ps[i+1]=ps[i]+arr[i+1];
        }
        
        if(ps[n]%3!=0)
        {
                cout<<0<<'\n';
                exit(0);
        }
        
        if(arr[n]==(ps[n]/3)) cnt[n]=1;
        else cnt[n]=0;
        FORD(i, n-1, 1)
        {
                if(ps[n]-ps[i-1]==(ps[n]/3))
                {
                        cnt[i]=cnt[i+1]+1;
                }
                else
                {
                        cnt[i]=cnt[i+1];
                }
        }
        
        ll ans=0;
        FOR(i, 1, n)
        {
                if(ps[i]==(ps[n]/3))
                {
                        ans+=cnt[i+2];
                }
        }
        
        cout<<ans<<endl;
}