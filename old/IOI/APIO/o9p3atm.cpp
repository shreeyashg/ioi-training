#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 5e5 + 4;

ll m, n, start, k, cc, vis[MX], cost[MX], spc[MX], comp[MX], memo[MX];
vector<ll> v[MX], rev[MX], adj[MX];
stack<ll> s;
vector<ll> vec;
vector<vector<ll> > components;
bool sp[MX];
ll connscc[MX];

void DFS(ll idx)
{
	if(vis[idx])
		return;
	vis[idx] = 1;
	REP(i, v[idx].size())
	{
		DFS(v[idx][i]);
	}
	s.push(idx);
}

void DFS2(ll idx)
{
	//cout << idx << endl;
	if(vis[idx])
		return;
	vis[idx] = 1;
	vec.push_back(idx);
	REP(i, rev[idx].size())
	{
		DFS2(rev[idx][i]);
	}
}

ll dp(ll idx)
{
	// Longest path that ends in a special vertex
	if(memo[idx] != -1)
		return memo[idx];

	ll ans = 0;

	if(sp[idx])
		ans = spc[idx];
	else
		ans = -INF;

	REP(i, adj[idx].size())
	{
		ans = max(ans, spc[idx] + dp(adj[idx][i]));
	}

	return memo[idx] = ans;
}

int main()
{
	cin >> n >> m;
	REP(i, m)
	{
		ll x, y;
		cin >> x >> y;
		v[x].push_back(y);
		rev[y].push_back(x);
	}

	FOR(i, 1, n) cin >> cost[i];
	cin >> start >> k;
	REP(i, k)
	{
		ll x;
		cin >> x;
		sp[x] = 1;
	}

	for(ll i = 1; i <= n; i ++)
	{
		if(!vis[i])
			DFS(i);
	//	cout << s.size() << endl;
	}

	memset(vis, 0, sizeof(vis));
	while(!s.empty())
	{
		cc ++;
		ll x = s.top();
		s.pop();
		if(!vis[x])
		{
			DFS2(x);
			components.push_back(vec);
			REP(i, vec.size())
			{
				comp[vec[i]] = components.size() - 1;
				spc[components.size() - 1] += cost[vec[i]];
				sp[components.size() - 1] |= sp[vec[i]]; 
			}
		}
		vec.clear();

	}


	FOR(i, 1, n)
	{
		REP(j, v[i].size())
		{
			if(comp[i] != comp[v[i][j]])
				adj[comp[i]].push_back(comp[v[i][j]]);
		}
	}

	memset(memo, -1, sizeof(memo));
	cout << dp(comp[start]) << endl;
}