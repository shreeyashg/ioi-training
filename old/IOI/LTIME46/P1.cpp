#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18;

int main()
{
	ll q;
	cin>>q;
	REP(i, q)
	{
		ll a, b;
		cin>>a>>b;
		if(a==1 && (b==2 || b==3))
		{
			cout<<"YES"<<endl;
			continue;
		}
		if(a==2 && (b==1 || b==4))
		{
			cout<<"YES"<<endl;
			continue;
		}
		if(a%2)
		{
			if(b==a-2 || b==a+1 || b==a+2)
				cout<<"YES"<<endl;
			else
				cout<<"NO"<<endl;
		}
		else
		{
			if(b==a-2 || b==a+2 || b==a-1)
				cout<<"YES"<<endl;
			else
				cout<<"NO"<<endl;
		}
	}	
}