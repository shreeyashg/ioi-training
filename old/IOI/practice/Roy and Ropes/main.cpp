#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <cmath>
#include <cstring>
#include <cctype>
#include <string>
#include <climits>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <queue>
#include <stack>
#include <algorithm>
#include <functional>
using namespace std;
#define DEBUG(x) cout << '>' << #x << ':' << x << endl;
#define REP(i,n) for(long long i=0;i<(n);i++)
#define FOR(i,a,b) for(long long i=(a);i<=(b);i++)
#define FORD(i,a,b) for(long long i=(a);i>=(b);i--)
#define endl '\n'
#define NMAX 3010
#define MX 20000001
#define mp make_pair
char grid[101][101];
int r, c;
string str;
set<pair<int, int> >s;
set<pair<int, int> >vs;

string x = "ALLIZZWELL";
int nxt = 0;

bool exp(int i, int j)
{
    if(nxt==x.size()-1)
        return true;
    
    if(i<0 || i>=r || j<0 || j>=c)
        return false;
    
    if(vs.count(mp(i, j))!=0)
        return false;
    
    DEBUG(grid[i][j]);
    DEBUG(nxt);
    
    vs.insert(mp(i, j));
    
    if(grid[i][j] == x[nxt])
        nxt++;
    
    return (exp(i+1, j) || exp(i+1, j+1)|| exp(i+1, j-1)||exp(i, j-1)||exp(i, j+1)||exp(i-1, j+1)|| exp(i-1, j)||exp(i-1, j-1));
    
}

int main()
{
    int t;
    cin>>t;
    while(t--)
    {
        cin>>r>>c;
        REP(i, r)
        {
            REP(j, c)
            {
                cin>>grid[i][j];
                //DEBUG(i);
                //DEBUG(j);
                if(grid[i][j]=='A')
                    s.insert(mp(i, j));
            }
        }
    
        for(auto it = s.begin(); it!=s.end(); it++)
        {
            if(exp((*it).first, (*it).second))
            {
                cout<<"YES"<<endl;
                exit(0);
            }
        }
    
        cout<<"NO"<<endl;
    }
}



