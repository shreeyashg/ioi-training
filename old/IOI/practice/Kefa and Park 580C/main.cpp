#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <algorithm>
#include <cmath>
#include <bitset>

using namespace std;

typedef long long ll;

#define FOR(i, a, b) for(ll i=a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define REP(i, n) for(ll i=0; i<n; i++)
#define DEBUG(x) cout<<#x<<" : "<<x<<endl
#define lb(x, s) lower_bound(x.begin(), x.end(), s)
#define pb push_back
#define pi 3.1415926536

const ll MV = 304, MP = 34, INF = 1e18 + 7;

ll p, v;
ll villages[MV], res[MP];
pair<ll,ll> choice[MV];
set<pair<ll, ll> >s;

ll dp[MV][MP];

ll rec(ll idx, ll off)
{
        if(dp[idx][off]!=-1)
                return dp[idx][off];
        if(off>=p)
        {
                choice[idx] = {-1ll,-1ll};
                ll sum=0;
                FOR(i, idx+1, v)
                {
                        sum+=abs(villages[i]-villages[idx]);
                }
                return dp[idx][off] = sum;
        }
        ll ans=INF;
        if(idx==0)
        {
                FOR(i, 1, v)
                {
                        ll sum=0;
                        FOR(j, idx+1, i-1)
                        {
                                sum+=abs(villages[i]-villages[j]);
                        }
                        if(sum+rec(i, off+1)<ans)
                        {
                                ans = sum+rec(i, off+1);
                                choice[idx] = {off+1,i};
                        }
                }
                return dp[idx][off] = ans;
        }
        ans = INF;
        FOR(i, idx+1, v)
        {
                ll sum=0;
                FOR(j, idx+1, i-1)
                {
                        sum += min(abs(villages[j]-villages[idx]), abs(villages[j]-villages[i]));
                }
                //DEBUG(sum);
                if(sum+rec(i, off+1)<ans)
                {
                        choice[idx] = {off+1,i};
                        ans = sum+rec(i, off+1);
                }
        }
       // DEBUG(ans);
        return dp[idx][off] = ans;
}

int main()
{
        memset(dp, -1, sizeof(dp));
        cin>>v>>p;
        REP(i, v)
        {
                cin>>villages[i+1];
        }
        cout<<rec(0, 0)<<endl;
        ll i = 0;
        while(choice[i]!=make_pair(-1ll,-1ll))
        {
                if(!res[choice[i].first])
                        res[choice[i].first] = villages[choice[i].second];
              //  cout<<villages[choice[i].second]<<" "<<choice[i].first<<endl;
                //s.insert({villages[choice[i].second],choice[i].first});
                i = choice[i].second;
        }
        FOR(i, 1, p)
        {
                cout<<res[i]<<" ";
        }
        cout<<endl;
}