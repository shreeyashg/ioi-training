/*
 ID: shreero1
 LANG: C++11
 TASK: crypt1
 */

#include <iostream>
#include <map>
#include <fstream>
#include <algorithm>
#include <vector>
using namespace std;
#define FOR(i,a,b) for(long long i=(a);i<=(b);i++)
#define DEBUG(x) cout << '>' << #x << ':' << x << endl;
#define REP(i,n) for(long long i=0;i<(n);i++)
#define bs(v, val) binary_search((v).begin(), (v).end(), (val))
vector<pair<int, int> > lim = {{9, 1}, {8, 1}, {7, 1}, {6, 1}, {5, 1}, {4, 2}, {3, 3}, {2, 4}, {1, 9}, {0, 0} };
vector<int> smp(5);
vector<int> v;// = {2, 3, 4, 6, 8};

bool check(int abc, int de)
{
    int prod = abc*de;
    //cout<<prod%10;
    if(prod >= 10000) return false;
    
    int cprod = prod;
    //DEBUG(cprod);
    //DEBUG(!bs(v, prod%10));
    if(!bs(v, prod%10))
        return false;
    //DEBUG(cprod);
    cprod/=10;
    if(!bs(v, cprod%10))
        return false;
    //DEBUG(cprod);
    cprod/=10;
    if(!bs(v, cprod%10))
        return false;
    //DEBUG(cprod);
    cprod/=10;
    if(!bs(v, cprod))
        return false;
    //DEBUG(cprod);
    
    int cde = de;
    int d = cde/10;
    int e = de%10;
    
    if(e*abc > 1000) return false;
    if(d*abc > 1000) return false;
    
    int p1 = e*abc;
    int p2 = d*abc;
    
    if(!bs(v, p1%10))
        return false;
    p1/=10;
    if(!bs(v, p1%10))
        return false;
    p1/=10;
    if(!bs(v, p1))
        return false;
    
    
    if(!bs(v, p2%10))
        return false;
    p2/=10;
    if(!bs(v, p2%10))
        return false;
    p2/=10;
    if(!bs(v, p2))
        return false;
    //DEBUG(d);
  //  DEBUG(e);
  //  DEBUG(cde);
    //DEBUG(de)
   // DEBUG(e*abc);
   // DEBUG(10*d*abc);
   // DEBUG(prod);
    
    if(e*abc + 10*d*abc == prod) return true;
    return false;
}

int main()
{
    //cout<<check(222, 38);
    ifstream cin("crypt1.in");
    ofstream cout("crypt1.out");
    
    int n;
    cin>>n;
    REP(i, n)
    {
        int x;
        cin>>x;
        v.push_back(x);
    }
    sort(v.begin(), v.end());
    int a, b, c, d, e;
    int p1, p2;
    int cnt = 0;
    
    REP(i, v.size())
    {
        REP(j, v.size())
        {
            REP(k, v.size())
            {
                REP(l, v.size())
                {
                    REP(m, v.size())
                    {
                        if(v[i]*100+v[j]*10+v[k]>1000)
                            continue;
                        if(check(v[i]*100+v[j]*10+v[k], v[l]*10+v[m]))
                        {
                            cnt++;
                        }
                    }
                }
            }
        }
    }
    
    cout<<cnt<<endl;
}
