#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <set>
#include <cmath>
using namespace std;

#define mp make_pair
#define ll long long
#define FOR(i, a, b) for(ll i=a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define REP(i, n) for(ll i=0; i<n; i++)
#define INF 2000000007
#define MX 1005
#define DEBUG(x) cout<<#x<<" : "<<x<<endl
#define lb(x, s) lower_bound(x.begin(), x.end(), s)
#define pb push_back
#define pi 3.1415926536
ll n, k;
vector<ll> nos(10);
vector<ll> ps;
string str="";

ll calc()
{
        vector<ll> f1(3);
        vector<ll> f2(3);
        ll cnt=0;
        FOR(i, 1, str.size())
        {
                f1[((str[i-1]-'0')%3)%3]=f2[0]+1;
                f1[((str[i-1]-'0')%3+1)%3]=f2[1];
                f1[((str[i-1]-'0')%3+2)%3]=f2[2];
                cnt += f1[0];
                vector<ll>t=f2;
                f2=f1;
                f1=t;
        }
        return cnt;
}

int main()
{
        ll t;
        cin>>t;
        while(t--)
        {
                str.clear();
                ll n;
                cin>>n;
                nos.clear();
                ps.clear();
                ps.resize(n);
                REP(i, n)
                {
                        ll a, b;
                        cin>>a>>b;
                        str.append(a, '0'+b);
                }
                cout<<calc()<<endl;
        }
}