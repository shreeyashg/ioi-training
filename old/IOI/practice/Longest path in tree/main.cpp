
#include <iostream>
#include <vector>
#include <cmath>
#include <set>

using namespace std;

#define pb push_back
#define fast_cin() ios_base::sync_with_stdio(false)

typedef long long ll;
typedef pair <int, int> pii;
typedef pair <ll, ll> pll;

const int N = 1e5 + 5;

set <ll> divisors;

string str;

int main()
{
        
        string str1="12454367"; //original string;
        string str2;
        
        for(int i=str1.size()-1; i>=0; i--)
        {
                str2+=str1[i];
        }
        cout<<str2<<endl;
}