#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <tuple>
#include <unordered_map>
#include <iomanip>
#include <functional>
using namespace std;

typedef long long ll;
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 1002
#define MOD 1000000007
#define INF 1000009*1e9

int main()
{
        ll t;
        cin>>t;
        while(t--)
        {
                ll n, d;
                cin>>n>>d;
                
                REP(i, n-2)
                {
                        cout<<1<<" ";
                }
                cout<<2<<" "<<n+d<<'\n';
        }
}