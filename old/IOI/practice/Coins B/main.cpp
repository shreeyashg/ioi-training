#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <unordered_map>
#include <set>
#include <cmath>
using namespace std;

#define ll long long
#define FOR(i, a, b) for(ll i=a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define REP(i, n) for(ll i=0; i<n; i++)
#define INF 2000000007
#define MOD 1000000007
#define MX 1005
#define DEBUG(x) cout<<#x<<" : "<<x<<endl
#define lb(x, s) lower_bound(x.begin(), x.end(), s)
#define pb push_back
#define pi 3.1415926536

#define MAXN 1000001

ll dp[MAXN + 5][4];

int main()
{
        //0 = '0'
        //1 = '1'
        
        //dp[i][j] is the number of strings of length i with last char j
        
        dp[0][0]=0;
        dp[0][1]=0;
        dp[2][0]=1;
        dp[2][1]=0;
        
        FOR(i, 3, MAXN)
        {
                REP(j, 2)
                {
                        if(j==0)dp[i][j]=dp[i-1][j]+1;
                        else dp[i][j]=dp[i-1][j];
                }
        }
        
        
        ll t;
        cin>>t;
        while(t--)
        {
                ll n;
                cin>>n;
                
                cout<<(dp[n][0]%MOD+dp[n][1]%MOD)%MOD<<endl;
        }
}