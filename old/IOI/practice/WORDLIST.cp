#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <cmath>
#include <cstring>
#include <cctype>
#include <string>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <queue>
#include <stack>
#include <algorithm>
#include <functional>
using namespace std;
#define DEBUG(x) cout << '>' << #x << ':' << x << endl;
#define REP(i,n) for(long long i=0;i<(n);i++)
#define FOR(i,a,b) for(long long i=(a);i<=(b);i++)
#define FORD(i,a,b) for(long long i=(a);i>=(b);i--)
#define endl '\n'
int graph[1001][1001];
int V, E;
vector<string> words;

int main()
{
    int l;
    cin>>l;
    while((l--)+1)
    {
        string line, buf;
        getline(cin, line);
        transform(line.begin(), line.end(), line.begin(), ::tolower);
        stringstream ss(line);
        
        while(ss>>buf)
        {
            words.push_back(buf);
        }
        
        
    }
    sort(words.begin(), words.end());
    string x;
    
    set<string> st(words.begin(), words.end());
    vector<string> v;
    v.assign(st.begin(), st.end());
    
    sort(v.begin(), v.end());
    
    cout<<v.size()<<endl;
    REP(i, v.size())
    {
        REP(j, v[i].length())
        {
            if(v[i][j]>=97&&v[i][j]<=122)
                cout<<v[i][j];
                
        }
        cout<<endl;
    }
}