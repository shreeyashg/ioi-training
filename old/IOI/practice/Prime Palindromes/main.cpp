/*
 ID: shreero1
 LANG: C++11
 TASK: pprime
 */

#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <cmath>
using namespace std;

#define ll long long
#define REP(i, n) for(int i=0; i<n; i++)
#define DEBUG(x) cout << '>' << #x << ':' << x << endl;
#define FOR(i, a, b) for(int i=a; i<=b; i++)

vector<int> palindromes;
vector<int> pralins;
//bool primes[10000001];
//bool palins[10000001];

bool is(int x)
{
    for(int i=2; i<=sqrt(x); i++)
    {
        if(x%i==0)
            return false;
    }
    return true;
}

int main()
{
    ifstream cin("pprime.in");
    ofstream cout("pprime.out");
    
    palindromes.push_back(5);
    palindromes.push_back(7);
    palindromes.push_back(11);
    
    for(int d1 = 1; d1<10; d1+=2)
    {
        for(int d2=0; d2<10; d2++)
        {
            palindromes.push_back(d1*100 + d2*10 + d1);
        }
    }
    
    for(int d1 = 1; d1<10; d1+=2)
    {
        for(int d2=0; d2<10; d2++)
        {
            for(int d3 = 0; d3<10; d3++)
            {
                palindromes.push_back(d1*10000 + d2*1000 + d3*100 + d2*10 + d1);
            }
        }
    }
    
    for(int d1 = 1; d1<10; d1+=2)
    {
        for(int d2=0; d2<10; d2++)
        {
            for(int d3 = 0; d3<10; d3++)
            {
                for(int d4 =0; d4<10; d4++)
                {
                    palindromes.push_back(d1*1000000+ d2*100000 + d3*10000 + d4*1000 + d3*100 + d2*10 + d1);
                }
            }
        }
    }
    
    int a, b;
    cin>>a>>b;
    
    REP(i, palindromes.size())
    {
        if(is(palindromes[i]))
            pralins.push_back(palindromes[i]);
    }
    
    REP(i, pralins.size())
    {
        if(pralins[i]>=a && pralins[i]<=b)
            cout<<pralins[i]<<endl;
    }
}