/*
 ID: shreero1
 LANG: C++11
 TASK: ariprog
 */

#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
using namespace std;
#define FOR(i, a, b) for(int i = a; i<=b; i++)
vector<int> bisquares(125001);

int main()
{
    ifstream cin("ariprog.in");
    ofstream cout("ariprog.out");
    
    int N, M;
    cin>>N>>M;
    FOR(i, 0, M)
    {
        FOR(j, 0, M)
        {
            bisquares[(i*i)+(j*j)] = 1;
        }
    }
    
    //sort(bisquares.begin(), bisquares.end());
    
    int cnt = 0;
    int k=0;
    FOR(d, 1, M*M)
    {
        FOR(a, 0, M*M)
        {
            if(a+((N-1)*d)>125001) break;
            int ft = a;
            cnt = 0;
            FOR(i, 0, N-1)
            {
                if(bisquares[a+(i*d)])
                {
                    cnt++;
                }
                else
                {
                    cnt = 0;
                    break;
                }
            }
            if(cnt==N)
            {
                cout<<a<<" "<<d<<endl;
                k++;
            }
        }
    }
    if(!k)
    {
        cout<<"NONE"<<endl;
    }
}