/*
 ID: shreero1
 LANG: C++11
 TASK: barn1
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <cmath>
#include <cstring>
#include <cctype>
#include <string>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <queue>
#include <stack>
#include <algorithm>
#include <functional>
using namespace std;
#define DEBUG(x) cout << '>' << #x << ':' << x << endl;
#define REP(i,n) for(long long i=0;i<(n);i++)
#define FOR(i,a,b) for(long long i=(a);i<=(b);i++)
#define FORD(i,a,b) for(long long i=(a);i>=(b);i--)
int gb=0, ge=0, gc=0;
vector<int> v(1000, 0);
int fi=23523523, la = -1;

void calc(int S)
{
    int cnt=0, be=0, en=0, mbe=0, men=0, mcnt=0;
    REP(i, v.size())
    {
        if(!v[i])
        {
            cnt++;
        }
        else
        {
            if(cnt>mcnt) mcnt = cnt;
            
            en = i;
            be = i-cnt;
            if((men - mbe)<en-be)
                men = en, mbe = be;
            cnt = 0;
        }
    }
    if(abs(en-be)>abs(men-mbe))
        men = en, mbe = be;
    if(cnt>mcnt)
    {
        mcnt = cnt;
        men = S;
        mbe = S-cnt;
    }
    
    gb = mbe, ge = men, gc = mcnt;
}

int main()
{
    ifstream cin("barn1.in");
    ofstream cout("barn1.out");
    int M, S, C;
    cin>>M>>S>>C;
    
    REP(i, C)
    {
        int f;
        cin>>f;
        v[f] = f;
        
        if(f<fi) fi = f;
        if(f>la) la = f;
    }
    v.assign(v.begin()+fi, v.begin()+la+1);

    int pre = 0;
    REP(i, M-1)
    {
        calc(S);
        FOR(j, gb, ge-1)
        {
            v[j] = 1;
        }
        pre+=gc;
    }
    cout<<((v.size()))-pre<<endl;
    
}