#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <set>
#include <cmath>
using namespace std;

#define ll long long
#define FOR(i, a, b) for(ll i=a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define REP(i, n) for(ll i=0; i<n; i++)
#define INF 2000000007
#define MX 1005
#define DEBUG(x) cout<<#x<<" : "<<x<<endl
#define lb(x, s) lower_bound(x.begin(), x.end(), s)
#define mp make_pair
#define pb push_back
#define pi 3.1415926536

vector<ll> v;

int main()
{
        ll n;
        cin>>n;
        while(n--)
        {
                ll k;
                cin>>k;
                REP(i, k)
                {
                        ll f;
                        cin>>f;
                        v.push_back(f);
                }
                ll mans = 0;
                ll ans=0;
                REP(i, v.size()-1)
                {
                        while(v[i+1]<v[i])
                        {
                                //DEBUG(ans);
                                ans+=(v[i]-v[i+1]);
                                mans = max(ans, mans);
                                i++;
                        }
                        ans=0;
                }
                cout<<mans<<endl;
                v.clear();
        }
}