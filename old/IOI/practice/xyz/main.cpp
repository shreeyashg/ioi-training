#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstdio>
#include <cstring>
#include <set>
#include <algorithm>
#include <cmath>
#include <bitset>

using namespace std;

typedef long long ll;

#define FOR(i, a, b) for(ll i=a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define REP(i, n) for(ll i=0; i<n; i++)
#define DEBUG(x) cout<<#x<<" : "<<x<<endl
#define lb(x, s) lower_bound(x.begin(), x.end(), s)
#define pb push_back
#define pi 3.1415926536

ll w, h, n;

const ll MX = 605;

ll mp[MX][MX], dp[MX][MX];

ll rec(ll x, ll y)
{
        if(mp[x][y])
                return 0;
        if(dp[x][y]!=-1)
                return dp[x][y];
        ll ans = x*y;
        FOR(i, 2, x-1)
        {
                ans = min(ans, rec(i, y) + rec(x-i, y));
        }
        FOR(i, 2, y-1)
        {
                ans = min(ans, rec(x, i) + rec(x, y-i));
        }
        
        return dp[x][y] = ans;
}

int main()
{
        ios::sync_with_stdio(false);
        cin.tie(0);
        REP(i, MX)
        {
                REP(j, MX)
                {
                        if(0)
                        {
                                dp[i][j]=0;
                        }
                        else
                                dp[i][j]=i*j;
                }
        }
        cin>>w>>h;
        cin>>n;
        REP(i, n)
        {
                ll a, b;
                cin>>a>>b;
                mp[a][b]=1;
        }
        
        FOR(i, 1, w)
        {
                FOR(j, 1, h)
                {
                        FOR(ii, 1, i-1)
                        {
                                dp[i][j] = min(dp[i][j], dp[ii][j] + dp[i-ii][j]);
                                if(mp[i][j])
                                        dp[i][j]=0;
                        }
                        
                        FOR(ii, 1, j-1)
                        {
                                dp[i][j] = min(dp[i][j], dp[i][ii] + dp[i][j-ii]);
                                if(mp[i][j])
                                        dp[i][j]=0;
                        }
                }
        }
        cout<<dp[w][h]<<endl;
}