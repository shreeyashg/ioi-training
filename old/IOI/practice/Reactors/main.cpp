/*
 ID: shreero1
 LANG: C++11
 TASK: castle
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <cmath>
#include <cstring>
#include <cctype>
#include <string>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <queue>
#include <stack>
#include <algorithm>
#include <functional>
using namespace std;
#define DEBUG(x) cout << '>' << #x << ':' << x << endl;
#define REP(i,n) for(long long i=1;i<=(n);i++)
#define GREP(i,n) for(long long i=1;i<=(n);i++)
#define FOR(i,a,b) for(long long i=(a);i<=(b);i++)
#define FORD(i,a,b) for(long long i=(a);i>=(b);i--)
#define endl '\n'
int r, c;
int cas[51][51], p[51][51];
vector<int> ai, aj;
vector<char> ch;
map<long long, long long> mp;
int k=1;
int cnt=0;

struct  state
{
    int vi, vj;
    char ch;
    
    bool operator<(const state &rhs)
    {
        return (vi < rhs.vi || (vi == rhs.vi && (vj < rhs.vj || (vj == rhs.vj && ch < rhs.ch))));
    }
};

vector<state> v;

void paint(int i, int j)
{
    if(i<1 || i>r)
        return;
    if(j<1 || j>c)
        return;
    if(p[i][j]) return;
    else
    {
        p[i][j] = k;
        mp[k]++;
    }
    
    int temp = cas[i][j];
    bool n=0, s=0, e=0, w=0;
    if(temp>=8)
    {
        s = 1;
        temp-=8;
    }
    if(temp>=4)
    {
        e = 1;
        temp-=4;
    }
    if(temp>=2)
    {
        n = 1;
        temp-=2;
    }
    if(temp>=1)
    {
        w = 1;
        temp-=1;
    }
    
    if(!n)
        paint(i-1, j);
    if(!s)
        paint(i+1, j);
    if(!e)
        paint(i, j+1);
    if(!w)
        paint(i, j-1);
    
}

void phelp()
{
    REP(i, r)
    {
        REP(j, c)
        {
            if(!p[i][j])
            {
                paint(i, j);
                k++;
            }
            //cout<<p[i][j]<<" ";
        }
        //cout<<endl;
    }
}

long long getmax()
{
    long long ans=0;
    REP(i, mp.size())
    {
        ans = max(ans, mp[i]);
    }
    return ans;
}

void clearp()
{
    REP(i, r)
    {
        REP(j, c)
        {
            p[i][j] = 0;
        }
    }
}

int del()
{
    long long ans = 0;
    FOR(i, 1, r)
    {
        REP(j, c)
        {
            if(i!=1 && p[i][j]!=p[i-1][j])
            {
                cnt++;
                k=1;
                cas[i][j]-=2;
                mp.clear();
                clearp();
                phelp();
                if(getmax()>=ans)
                {
                    ans = getmax();
                    if(getmax()>ans)
                    {
                        v.clear();
                    }
                    state s;
                    s.vi = i;
                    s.vj = j;
                    s.ch = 'N';
                    v.push_back(s);
                    ai.push_back(i);
                    aj.push_back(j);
                    ch.push_back('E');
                }
                cas[i][j]+=2;
            }
            
            if(j!=c && p[i][j]!=p[i][j+1])
            {
                cnt++;
                k=1;
                mp.clear();
                clearp();
                cas[i][j]-=4;
                phelp();
                if(getmax()>=ans)
                {
                    ans = getmax();
                    if(getmax()>ans)
                    {
                        v.clear();
                    }
                    state s;
                    s.vi = i;
                    s.vj = j;
                    s.ch = 'E';
                    v.push_back(s);
                    ai.push_back(i);
                    aj.push_back(j);
                    ch.push_back('E');
                }
                cas[i][j]+=4;
            }

        }
    }
    return ans;
}

int main()
{
    //ifstream cin("castle.in");
    //ofstream cout("castle.out");
    cin>>c>>r;
    REP(i, r)
    {
        REP(j, c)
        {
            cin>>cas[i][j];
        }
    }
    
    phelp();
    int jmin = 123456789;
    int imax = 0;
    cout<<k-1<<endl;
    long long maxs = 0;
    REP(i, mp.size()) maxs=max(maxs, mp[i]);

    cout<<maxs<<endl;
    cout<<del()<<endl;
    
    cout<<v[0].vi<<" "<<v[0].vj<<" "<<v[0].ch<<endl;
    
}