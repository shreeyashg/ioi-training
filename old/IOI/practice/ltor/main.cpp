#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <cmath>
#include <cstring>
#include <cctype>
#include <string>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <queue>
#include <stack>
#include <algorithm>
#include <functional>
using namespace std;
#define ll long long
#define mp make_pair
#define DEBUG(x) cout << '>' << #x << ':' << x << endl;
#define REP(i,n) for(long long i=0;i<(n);i++)
#define FOR(i,a,b) for(long long i=(a);i<=(b);i++)
#define FORD(i,a,b) for(long long i=(a);i>=(b);i--)
#define INF 1000000000000l
#define NX 20001
#define endl '\n'
//i

int main()
{
        ll t;
        cin>>t;
        while(t--)
        {
                ll n;
                cin>>n;
                ll m = n+1;
                cout<<(2*2*n)/__gcd(2*2*n, m)<<endl;
        }
}