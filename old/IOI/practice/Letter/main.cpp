#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <queue>
#include <set>
#include <map>
#include <sstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cctype>
using namespace std;

#define PB push_back
#define MP make_pair

const double pi		=	acos(-1.0);
const double eps	=	1e-8;

typedef long long LL;
typedef unsigned long long ULL;
typedef vector<int> VI;
typedef vector<string> VS;

int n,a[1000000];

int main()
{
        LL t;
        cin>>t;
        while(t--)
        {
                LL n;
                cin>>n;
                LL y;
                cin>>y;
                LL s=0;
                LL x;
                for(int i=1; i<n; i++)
                {
                        cin>>x;
                        if(x<y) s+=y-x;
                        y = x;
                }
                
                cout<<s<<endl;
        }
}