/*
 ID: shreero1
 LANG: C++11
 TASK: combo
 */

#include <iostream>
#include <fstream>
#include <vector>
#define FOR(i, a, b) for(int i=a; i<=b; i++)
using namespace std;

int n;
struct comb
{
    int a, b, c;
}f, m, flb, fub, mlb, mub;

vector<comb> ff;
vector<comb> mm;



int check(comb c, comb x)
{
    if(abs(c.a-x.a) <= 2 || abs(c.a-x.a) >= n-2)
        if(abs(c.b-x.b)<= 2 || abs(c.b-x.b) >= n-2)
            if(abs(c.c - x.c)<= 2 || abs(c.c-x.c) >= n-2)
                return true;
    return false;
}

int main()
{
    //int n;
    ifstream cin("combo.in");
    ofstream cout("combo.out");
    cin>>n;
    
    //comb f, m, flb, fub, mlb, mub;
    cin>>f.a>>f.b>>f.c;
    cin>>m.a>>m.b>>m.c;
    
    flb.a = f.a-2;
    if(flb.a<=0) flb.a = n+flb.a;
    flb.b = f.b-2;
    if(flb.b<=0) flb.b = n+flb.b;
    flb.c = f.c-2;
    if(flb.c<=0) flb.c = n+flb.c;
    //)%n;
    fub.a = (f.a+2)%n;
    fub.b = (f.b+2)%n;
    fub.c = (f.c+2)%n;
    
    mlb.a = m.a-2;
    if(mlb.a<=0) mlb.a = n+mlb.a;
    mlb.b = m.b-2;
    if(mlb.b<=0) mlb.b = n+mlb.b;
    mlb.c = m.c-2;
    if(mlb.c<=0) mlb.c = n+mlb.c;
    
    mub.a = (m.a+2)%n;
    mub.b = (m.b+2)%n;
    mub.c = (m.c+2)%n;
    
    
    
    int cnt=0;
    
    FOR(i, 1, n)
    {
        FOR(j, 1, n)
        {
            FOR(k, 1, n)
            {
                comb x;
                x.a = i;
                x.b = j;
                x.c = k;
                
                if((check(x, f) || check(x, m)))
                {
                    cnt++;
                    //cout<<i<<" "<<j<<" "<<k<<endl;
                }
            }
        }
    }
    cout<<cnt<<endl;
}