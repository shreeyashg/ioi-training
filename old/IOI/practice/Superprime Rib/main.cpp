/*
ID: shreero1
LANG: C++11
TASK: sprime
*/

#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>
#include <algorithm>
using namespace std;

#define DEBUG(x) cout<<#x<<" "<<x<<endl;

vector<int> primes;
vector<int> v;
vector<int> ribs = {2,3,5,7,23,29,31,37,53,59,71,73,79,233,239,293,311,313,317,373,379,593,599,719,733,739,797,2333,2339,2393,2399,2939,3119,3137,3733,3739,3793,3797,5939,7193,7331,7333,7393,23333,23339,23399,23993,29399,31193,31379,37337,37339,37397,59393,59399,71933,73331,73939,233993,239933,293999,373379,373393,593933,593993,719333,739391,739393,739397,739399,2339933,2399333,2939999,3733799,5939333,7393913,7393931,7393933, 23399339, 29399999, 37337999, 59393339, 73939133};

int num(int x, int dg)
{
    string s = to_string(x);
    string f = s.substr(0, dg);
    return stoi(f);
}

bool chk(int x)
{
    if(x==1)
        return 0;
    for(int i=2; i<=sqrt(x); i++)
    {
        if(x%i==0)
            return 0;
    }
    return 1;
}

int main()
{
    ifstream cin("sprime.in");
    ofstream cout("sprime.out");
    
    //cout<<ribs.size()<<endl;
    
    int s;
    cin>>s;
    
    if(s<=8)
    {
        
        for(int i=0; i<ribs.size(); i++)
        {
            if(ribs[i]>=pow(10, s-1) && ribs[i]<=pow(10, s))
                cout<<ribs[i]<<endl;
        }
    }
    //if(s==8)
    //{
        /*for(int i=70; i<ribs.size(); i++)
        {
            int nums=0;
            bool flag = true;
            for(int j=1; j<=9; j++)
            {
                nums = ribs[i] + (j*10000000);
                if(chk(j))
                {
                    for(int k=8; k>=1; k--)
                    {
                        DEBUG(nums);
                        DEBUG(k);
                        DEBUG(num(nums, k));
                        DEBUG(chk(num(nums, k)));

                        if(!chk(num(nums, k)))
                        {
                            flag = false;
                            break;
                        }
                    }
                }
            }
            
        }*/
        
        /*for(int i=2; i<=pow(10, 8); i++)
        {
            if(i%2 && (i/10)%2 && (i/100)%2 && (i/1000)%2 && (i/10000)%2 && (i/100000)%2)
            {
                int cnt=0;
                if(chk(i))
                {
                    for(int j=to_string(i).size(); j>=1; j--)
                    {
                        if(chk(num(i, j)))
                        {
                            cnt++;
                        }
                        else
                        {
                            cnt=0;
                            break;
                        }
                    }
                    if(cnt==to_string(i).size())
                    {
                        cout<<i<<endl;
                    }
                }
            }
        }*/
        
        
    
}

