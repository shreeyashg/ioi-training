/*
 ID: shreero1
 LANG: C++11
 TASK: numtri
 */

#include<iostream>
#include<fstream>

using namespace std;
#define FOR(i, a, b) for(int i=a; i<=b; i++)

int triangle[1001][1001];
int r;

int cache[1001][1001];

int rec(int i, int j)
{
    if(i==r || j==r)
        return triangle[i][j];
    if(cache[i][j]!=-1) return cache[i][j];
    
    cache[i][j] = triangle[i][j] + max(rec(i+1, j), rec(i+1, j+1));
    return cache[i][j];
}

int main()
{
    ifstream cin("numtri.in");
    ofstream cout("numtri.out");
    
    FOR(i,0, 1000)
    {
        FOR(j,0, 1000)
        {
            cache[i][j] = -1;
        }
    }
    
    cin>>r;
    
    FOR(i, 1, r)
    {
        FOR(j, 1, i)
        {
            cin>>triangle[i][j];
        }
    }
    
    cout<<rec(1, 1)<<endl;
}