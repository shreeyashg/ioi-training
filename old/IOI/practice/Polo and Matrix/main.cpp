#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
using namespace std;

#define ll long long
#define FOR(i, a, b) for(ll i=a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define REP(i, n) for(ll i=0; i<n; i++)
#define MOD 1000000007
#define INF 1000000007

#define MX 1005
#define DEBUG(x) cout<<#x<<" : "<<x<<endl
#define lb(x, s) lower_bound(x.begin(), x.end(), s)
#define pb push_back
#define pi 3.1415926536

ll arr[MX][MX];
ll n;
pair<ll,ll> dp[MX][MX];

ll lcm(ll x, ll y)
{
        return (x*y)/__gcd(x,y);
}

ll fives(ll x)
{
        if(x==0)
                return -INF;
        ll cnt=0;
        while(x%5==0 && x>0)
        {
                cnt++;
                x/=5;
        }
        return cnt;
}

ll twos(ll x)
{
        ll cnt=0;
        if(x==0)
                return -INF;
        while(x%2==0 && x>0)
        {
                cnt++;
                x/=2;
        }
        return cnt;
}


/*
pair<ll,ll> rec(ll x, ll y)
{
        if(x>n-1 || y>n-1)
                return {0,0};
        
        if(dp[x][y].first!=-1)
        {
                return dp[x][y];
        }
        
        pair<ll, ll> a = rec(x+1, y);
        pair<ll, ll> b = rec(x, y+1);
        
        
        dp[x][y].first = twos(arr[x][y])+a.first;
        dp[x][y].second = fives(arr[x][y])+a.second;
        
        if((a.first*a.second)<(b.second*b.first))
        {
                dp[x][y].first = twos(arr[x][y])+a.first;
                dp[x][y].second = fives(arr[x][y])+a.second;
        }
        else
        {
                dp[x][y].first = twos(arr[x][y])+b.first;
                dp[x][y].second = fives(arr[x][y])+b.second;
        }
        
        return dp[x][y];
}
 */

int main()
{
        ll t;
        cin>>t;
        while(t--)
        {
                memset(dp, 0, sizeof(dp));
                memset(arr, 0, sizeof(arr));
                cin>>n;
                REP(i, n)
                {
                        REP(j, n)
                        {
                                cin>>arr[i][j];
                        }
                }
                
                dp[0][0].first=twos(arr[0][0]);
                dp[0][0].second=fives(arr[0][0]);
                
                FOR(i, 1, n-1)
                {
                        dp[i][0].first = dp[i-1][0].first+twos(arr[i][0]);
                        dp[i][0].second = dp[i-1][0].second+fives(arr[i][0]);
                        
                        dp[0][i].first = dp[0][i-1].first+twos(arr[0][i]);
                        dp[0][i].second = dp[0][i-1].second+fives(arr[0][i]);
                }
                //dp[0][1].first=twos(arr[0][1])+dp[0][0].first;
                //dp[1][0].second=twos(arr[1][0])+dp[0][0].second;
                FOR(i, 1, n-1)
                {
                        FOR(j, 1, n-1)
                        {
                                //dp[i][j] = min(arr[i][j]+dp[i][j-1], arr[i][j]+dp[i][j-1]);
                                dp[i][j].first = min(twos(arr[i][j])+dp[i][j-1].first, twos(arr[i][j])+dp[i-1][j].first);
                                dp[i][j].second = min(fives(arr[i][j])+dp[i][j-1].second, fives(arr[i][j])+dp[i-1][j].second);
                        }
                }
                
                //DEBUG(fives(100));
                
           /*     REP(i, n)
                {
                        REP(j, n)
                        {
                                cout<<dp[i][j].first<<" "<<dp[i][j].second<<"             ";
                        }
                        cout<<'\n';
                }*/
               
               /* REP(i, n)
                {
                        REP(j, n)
                        {
                                cout<<dp[i][j].first<<" "<<dp[i][j].second<<"            ";
                        }
                        cout<<'\n';
                } */
                
                if(min(dp[n-1][n-1].first, dp[n-1][n-1].second)<=0)
                {
                        cout<<0<<endl;
                }
                else
                {
                        cout<<min(dp[n-1][n-1].first, dp[n-1][n-1].second)<<endl;
                }
        }
}