#include <iostream>
#include <cmath>
#include <algorithm>
#include <queue>
#include <set>
#include <cstring>
#include <utility>
typedef long long ll;
#define FOR(i, a, b) for(ll i=a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define REP(i, n) for(ll i=0; i<n; i++)
#define INF 2000000007
#define MX 100005
#define DEBUG(x) cout<<#x<<" : "<<x<<endl
#define lb(x, s) lower_bound(x.begin(), x.end(), s)
#define pb push_back
#define mp make_pair
#define pi 3.1415926536
using namespace std;
ll n;
vector<ll> sums;
ll arr[501][501];
pair<ll, ll> z;

ll rowsum(ll r)
{
    ll a=0;
    REP(i, n)
    {
        a+=arr[r][i];
    }
    return a;
}

ll colsum(ll c)
{
    ll a=0;
    REP(i, n)
    {
        a+=arr[i][c];
    }
    return a;
}

ll diag1sum()
{
    ll ans=0;
    REP(i, n)
    {
        ans+=arr[i][i];
    }
    return ans;
}

ll diag2sum()
{
    ll ans=0;
    REP(i, n)
    {
        ans+=arr[i][n-i-1];
        //DEBUG(n-i);
    }
    return ans;
}

int main()
{
    // ll n;
    cin>>n;
    
    REP(i, n)
    {
        REP(j, n)
        {
            cin>>arr[i][j];
            if(arr[i][j]==0)
                z=mp(i, j);
        }
    }
    if(n==1)
    {
        cout<<5<<endl;
        exit(0);
    }
    
    
    ll d1s=0;
    ll x=0;
    if(z.first==z.second)
    {
        d1s = diag1sum();
        x=1;
        if(!(d1s==rowsum(z.first)&&rowsum(z.first)==colsum(z.second)&&d1s==colsum(z.second)))
        {
            cout<<-1<<endl;
            exit(0);
        }
    }
    ll d2s=0;
    if(z.first+z.second+1==n)
    {
        if(x==1) x=3;
        if(x!=3) x=2;
        d2s = diag2sum();
        //  DEBUG(d2s);
        //   DEBUG(rowsum(z.first));
        //  DEBUG(colsum(z.second));
        if((d2s==rowsum(z.first)&&rowsum(z.first)==colsum(z.second)&&d2s==colsum(z.second)))
        {
            // DEBUG("YEAH");
        }
        else
        {
            cout<<-1<<endl;
            exit(0);
        }
    }
    
    
    
    ll cnt=0;
    ll ra=0;
    set<ll> s;
    REP(i, n)
    {
        if(i!=z.first)
        {
            ll rs = rowsum(i);
            s.insert(rs);
            if(rs!=ra&& (cnt!=0))
            {
                cout<<-1<<endl;
                exit(0);
            }
            ra = rs;
            cnt++;
        }
    }
    
    ra=0;
    
    cnt=0;
    REP(i, n)
    {
        if(i!=z.second)
        {
            ll rs = colsum(i);
            s.insert(rs);
            if(rs!=ra &&(cnt!=0))
            {
                cout<<-1<<endl;
                exit(0);
            }
            ra = rs;
            cnt++;
        }
    }
    set<ll> zero;
    zero.insert(colsum(z.second));
    zero.insert(rowsum(z.first));

    if(!x)
    {
        s.insert(diag1sum());
        s.insert(diag2sum());
    }
    
    if(x==1)
    {
        s.insert(diag2sum());
        zero.insert(diag1sum());
    }
    
    if(x==2)
    {
        s.insert(diag1sum());
        zero.insert(diag2sum());
    }
    
    
    if(x==3)
    {
        zero.insert(diag1sum());
        zero.insert(diag2sum());
    }
    
    //DEBUG(s.size());
    //DEBUG(zero.size());
    
    if((s.size()+zero.size())>2) cout<<-1<<endl;
    else if(*(s.begin())-*(zero.begin())<=0) cout<<-1<<endl;
    else cout<<ra-rowsum(z.first)<<endl;
}