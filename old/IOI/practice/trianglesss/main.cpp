#include <cmath>
#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <map>
#include <list>
#include <set>
#include <cmath>
#include <utility>
using namespace std;

#define ll long long
#define FOR(i, a, b) for(ll i=a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define REP(i, n) for(ll i=0; i<n; i++)
#define INF 2000000007
#define MX 100500
#define DEBUG(x) cout<<#x<<" : "<<x<<endl
#define lb(x, s) lower_bound(x.begin(), x.end(), s)
#define pb push_back
#define pi 3.1415926536

ll n, b, x[MX], y[MX];

int main()
{
        cin>>n>>b;
        REP(i, b)
        {
                cin>>x[i];
        }
        REP(i, b)
        {
                cin>>y[i];
        }
        ll ans=0;
        REP(i, b)
        {
                ans+=min(min(abs(x[i]-1)+abs(y[i]-1), abs(x[i]-1)+abs(y[i]-n)), min(abs(x[i]-n)+abs(y[i]-1), abs(x[i]-n)+abs(y[i]-n)));
        }
        cout<<ans<<endl;
}