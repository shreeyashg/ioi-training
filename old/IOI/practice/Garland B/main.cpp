#include <iostream>
#include <map>
#include <vector>
#include <utility>
#include <string>
#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i=a; i<=b; i++)
#define DEBUG(x) cout << '>' << #x << ':' << x << endl;

using namespace std;
map<string, pair<ll, ll> >mp;

int main()
{
    ll t;
    cin>>t;
    while(t--)
    {
        ll d;
        cin>>d;
        REP(i, d)
        {
            string s;
            
            cin>>s;
            ll l, h;
            cin>>l>>h;
            mp[s] = make_pair(l, h);
        }
        
        ll q;
        cin>>q;
        REP(i,q)
        {
            ll x;
            cin>>x;
            ll cnt=0;
            string s="";
            for(auto it = mp.begin(); it!=mp.end(); it++)
            {
                if(x>=it->second.first && x<=it->second.second)
                {
                    s=it->first;
                    cnt++;
                }
            }
            
            if(cnt==1)
                cout<<s<<endl;
            else
                cout<<"UNDETERMINED"<<endl;
        }
        if(t) cout<<endl;
        mp.clear();
    }
}