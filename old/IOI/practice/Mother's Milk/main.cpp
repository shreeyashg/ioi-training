/*
 ID: shreero1
 LANG: C++11
 TASK: milk3
 */


#include <iostream>
#include <set>
#include <fstream>
#include <algorithm>
#include <map>
#include <stack>
#include <vector>
using namespace std;
#define REP(i, n) for(int i=0; i<n; i++)
#define DEBUG(x) cout << '>' << #x << ':' << x << endl;

int sum;
int x, y;


int maxarr[3];
set<int>sets;

struct node
{
    int arr[3];
    
};

node as;


vector<node> visited;
bool search(node x)
{
    REP(i, visited.size())
    {
        node t = visited[i];
        if(t.arr[0] == x.arr[0])
            if(t.arr[1] == x.arr[1])
                if(t.arr[2] == x.arr[2])
                    return true;
    }
    return false;
}

node pour(int s, int d, node src)
{
    if(s==d)
        return src;
    if(src.arr[s]==0)
        return src;

    node x;
    
    if(src.arr[d] == maxarr[d])
        return src;
    //int ogs = src.arr[s];
    if(src.arr[d]<maxarr[d])
    {
        x.arr[d]=min(maxarr[d], src.arr[d]+src.arr[s]);
    }
    //int ogd = src.arr[d];
    x.arr[s] = src.arr[s]-(x.arr[d]-src.arr[d]);
    //int newd = x.arr[d];
    //DEBUG(ogd);
    //DEBUG(newd);
    
    src.arr[d] = x.arr[d];
    src.arr[s] = x.arr[s];
    
    return src;
}

void helper(node n)
{
    if(search(n))
        return;
    
    visited.push_back(n);
    
    if(!n.arr[0])
        sets.insert(n.arr[2]);
    
    REP(i, 3)
    {
      //  DEBUG(n.arr[0]);
      //  DEBUG(n.arr[1]);
      //  DEBUG(n.arr[2]);
    }
    
    helper(pour(2, 1, n));
    helper(pour(2, 0, n));
    helper(pour(1, 0, n));
    helper(pour(0, 1, n));
    helper(pour(0, 2, n));
    helper(pour(1, 2, n));
}

int main()
{
    ifstream cin("milk3.in");
    ofstream cout("milk3.out");
    
    cin>>maxarr[0]>>maxarr[1]>>maxarr[2];
    sum=0+maxarr[2];
    node s;
    s.arr[0] = 0;
    s.arr[1] = 0;
    s.arr[2] = maxarr[2];
    
    helper(s);
    
    vector<int> v;
    
    set<int>::iterator it;
    for(it = sets.begin(); it!= sets.end(); it++)
        v.push_back(*it);
    
    REP(i, v.size())
    {
        if(i!=v.size()-1)
            cout<<v[i]<<" ";
        else
            cout<<v[i];
    }
    cout<<endl;
}