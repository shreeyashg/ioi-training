/*
ID: shreero1
LANG: C++11
TASK: skidesign
*/

#include <iostream>
#include <vector>
#include <deque>
#include <fstream>
#include <queue>
#include <algorithm>
#define REP(i, n) for(int i=0; i<n; i++)

using namespace std;
int N;
priority_queue<int> maxq;
pair<int, int> interval;
priority_queue<int, vector<int>, greater<int> > minq;
vector<int> v;
int cost;
int mincost = 12000000;
int cnt;

#define N_MAX 1000

void solve()
{
    if(interval.second == 100) return;
    REP(i, N)
    {
        if(v[i]<interval.first)
            cost+=((interval.first-v[i])*(interval.first-v[i]));
        if(v[i]>interval.second)
            cost+=((v[i]-interval.second)*(v[i]-interval.second));
    }
    interval.first++;
    interval.second++;
    mincost = min(cost, mincost);
    cost = 0;
    solve();
    cnt++;
}

int main()
{
    interval = make_pair(1, 18);
    ifstream cin("skidesign.in");
    ofstream cout("skidesign.out");
    cin>>N;
    REP(i, N)
    {
        int f; cin>>f;
        v.push_back(f);
    }
    
    sort(v.begin(), v.end());
    solve();
    cout<<mincost<<endl;
}