#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <cmath>
#include <cstring>
#include <cctype>
#include <string>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <queue>
#include <stack>
#include <algorithm>
#include <functional>
using namespace std;
#define DEBUG(x) cout << '>' << #x << ':' << x << endl;
#define REP(i,n) for(long long i=0;i<(n);i++)
#define FOR(i,a,b) for(long long i=(a);i<=(b);i++)
#define FORD(i,a,b) for(long long i=(a);i>=(b);i--)
#define endl '\n'
vector<string> words;

int main()
{
    int l;
    cin>>l;
    string str;
    while((l--)+1)
    {
        string line, buf;
        getline(cin, line);
        
        REP(i, line.size())
        {
            if(line[i]>=65&&line[i]<=90)
            {
                line[i]+=32;
            }
            
            if(!(line[i]>=65&&line[i]<=90) && !(line[i]>=97&&line[i]<=122) && line[i]!=32)
            {
                line.erase(line.begin()+i);
            }
        }
        stringstream ss(line);
        
        while(ss>>buf)
        {
            words.push_back(buf);
        }
        
        
    }
    
    
    sort(words.begin(), words.end());
    string x;
    
    set<string> st(words.begin(), words.end());
    vector<string> v;
    v.assign(st.begin(), st.end());
    
    sort(v.begin(), v.end());
    
    cout<<v.size()<<endl;
    REP(i, v.size())
    {
        REP(j, v[i].length())
        {
            if(v[i][j]>=97&&v[i][j]<=122)
                cout<<v[i][j];
                
        }
        cout<<endl;
    }
}