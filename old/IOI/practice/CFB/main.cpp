#include <climits>
#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <cmath>
#include <map>
#include <string>
#include <deque>

#define REP(i,n) for(long long i=0;i<(n);i++)
#define DEBUG(x) cout << '>' << #x << ':' << x << endl;
#define ll long long
#define bs(x) binary_search(x)
using namespace std;
#define MOD (-1000000007l)

ll n, sm;
map<ll, map<ll, ll> > mp1, mp2;

string max(string s1, string s2)
{
    REP(i, s1.size())
    {
        if(s1[i]=='!')
        {
            s1.erase(s1.begin()+i);
        }
    }
    
    REP(i, s2.size())
    {
        if(s2[i]=='!')
        {
            s2.erase(s2.begin()+i);
        }
    }
    
    if(s1.empty())
        return s2;
    if(s2.empty())
        return s1;
    
    return "";
    
}

ll rec1(ll s, ll sz)
{
    if(mp1[s][sz]>0)
        return mp1[s][sz];
    
    if(sz == n && s==0)
        return 0;
    
    if(sz == n)
        return MOD;
    
    if(s==0)
        return MOD;
    
    if(s<0)
        return MOD;
    
    ll ans = MOD;
    REP(i, 10)
    {
        ans = max(ans, ((ll)pow(10, sz)*i)+rec1(s-i, sz+1));
    }
    return mp1[s][sz] = ans;
}

ll rec2(ll s, ll sz)
{
    if(mp2[s][sz]>0)
        return mp2[s][sz];
    
    if(sz == n && s==0)
        return 0;
    
    if(sz == n)
        return -MOD;
    
    if(s==0)
        return -MOD;
    
    if(s<0)
        return -MOD;
    
    ll ans = -MOD;
    REP(i, 10)
    {
        ans = min(ans, ((ll)pow(10, sz)*i)+rec2(s-i, sz+1));
    }
    return mp2[s][sz] = ans;
}

int main()
{
    cin>>n>>sm;
    ll a1 = rec1(sm, 0);
    ll a2 = rec2(sm, 0);
    if(a1==MOD && a2==-MOD)
        cout<<"-1 -1"<<endl;
    else
        cout<<a2<<" "<<a1<<endl;
}