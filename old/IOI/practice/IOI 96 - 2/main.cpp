/*
 ID: shreero1
 TASK: sort3
 LANG: C++11
 */


#include <iostream>
#include <fstream>
#define REP(i, n) for(int i=0; i<n; i++)
#define FOR(i, a, b) for(int i=a; i<=b; i++)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
using namespace std;

int arr[1001];
int n, a, b, c;


int main()
{
    

    //ifstream cin("sort3.in");
    //ofstream cout("sort3.out");
    
    cin>>n;
    REP(i, n)
    {
        cin>>arr[i];
        if(arr[i]==1)
            a++;
        if(arr[i]==2)
            b++;
        if(arr[i]==3)
            c++;
    }
    
    int cnt=0;
    
    REP(i, a)
    {
        if(arr[i]!=1)
        {
            for(int j=a; j<n; j++)
            {
                if(arr[j]==1 && arr[i]!=arr[j])
                {
                    swap(arr[j], arr[i]);
                    cnt++;
                    //DEBUG(arr[i]);
                    //DEBUG(arr[j]);
                    //DEBUG(cnt);
                }
            }
        }
    }
    

    FOR(i, a, a+b-1)
    {
        if(arr[i]!=2)
        {
            FOR(j, a+b, n-1)
            {
                if(arr[j]==2 && arr[i]!=arr[j])
                {
                    swap(arr[j], arr[i]);
                    cnt++;
                    //DEBUG(arr[i]);
                    //DEBUG(arr[j]);
                    //DEBUG(cnt);
                }
            }
        }
    }
    


    cout<<cnt<<endl;
}