/*
 ID: shreero1
 LANG: C++11
 TASK: wormhole
 */

#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
using namespace std;
#define REP(i, n) for(int i=0; i<n; i++)
#define FOR(i, a, b) for((i)=a; (i)<=b; (i)++)

int X[13], Y[13], N;
int partner[13], rights[13];


bool cycle_exists()
{
    
    for(int i=1; i<=N; i++)
    {
        int pos = i;
        for(int cnt=0; cnt<N; cnt++)
        {
            pos = rights[partner[pos]];
        }
        if(pos) return true;
    }
    
    //else
    return false;
}

int solve()
{
    int i, total = 0;
    
    for(i=1; i<=N; i++)
    {
        if(partner[i] == 0) break;
    }
    
    if(i>N)
    {
        if(cycle_exists()) return 1;
        else return 0;
    }
    
    
    
    for(int j=i+1; j<=N; j++)
    {
        if(partner[j] == 0)
        {
            partner[i] = j;
            partner[j] = i;
            total += solve();
            partner[i] = 0;
            partner[j] = 0;
        }
        
    }
    return total;
    
}

int main()
{
    ifstream cin("wormhole.in");
    ofstream cout("wormhole.out");
    cin>>N;
    
    REP(i, N)
    {
        cin>>X[i+1]>>Y[i+1];
    }
    
    for(int i=1; i<=N; i++)
    {
        for(int j=1; j<=N; j++)
        {
            if(Y[i]==Y[j] && X[j] > X[i])
                if(rights[i]==0 || X[j]-X[i]<X[rights[i]] - X[i])
                    rights[i] = j;
        }
    }
    
    cout<<solve()<<endl;
}

















