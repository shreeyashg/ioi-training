#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <iomanip>
using namespace std;

typedef long long ll;
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

vector<ll> v;

int main()
{
        ll n, l, r;
        ios::sync_with_stdio(false);
        cin.tie(0);
        cin>>n>>l>>r;
        REP(i, n)
        {
                ll k;
                cin>>k;
                v.push_back(k);
        }
        sort(v.begin(), v.end());
        cout<<abs((v[n-1]+v[n-2])-abs(v[n-1]-v[0]))<<endl;
}