#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18;

ll n;
vector<ll> v1;
vector<ll> v2;
map<ll, ll> mp;

ll msb(ll i) {
    i |= (i >>  1);
    i |= (i >>  2);
    i |= (i >>  4);
    i |= (i >>  8);
    i |= (i >> 16);
    return i - ((unsigned)i >> 1);
}


int main()
{
	//cout<<(1 << 1)<<endl;
	ios::sync_with_stdio(false);
	//cin.tie(0);
	//cout<<"HI";
	scanf("%lld", &n);

	REP(i, 1 << n)
	{
		ll k;
		scanf("%lld", &k);
		v1.push_back(k);
		v2.push_back(k);
	}
	sort(v2.begin(), v2.end());
	REP(i, v1.size())
	{
		auto x = upper_bound(v2.begin(), v2.end(), v1[i]);
		//cout<<x-v2.begin()+1<<" "<<v1[i]<<endl;
		x--;
		ll pos = x - v2.begin();
		pos++;
	//	double xx;
	//	if(x-v2.begin()) xx = log2(pos);
		printf("%lld ", n-(ll)(log2(pos)));//<<" ";
	}
	printf("\n");
}