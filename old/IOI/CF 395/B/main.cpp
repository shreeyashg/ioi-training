#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <fstream>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

typedef long long ll;

ll n;
const ll MX = 1e5  +3;
ll arr[2*MX], narr[2*MX];

int main()
{
        cin>>n;
        REP(i, n)
        {
                cin>>arr[i+1];
        }
        
        if(n%2)
        {
                ll k=1;
                narr[n/2 + 1] = arr[n/2 + 1];
                ll l = 1;
                ll r = n;
                while(l!=n/2+1)
                {
                        if(k%2)
                                narr[l] = arr[r], narr[r] = arr[l];
                        else
                                narr[l] = arr[l], narr[r] = arr[r];
                        k++, l++, r--;
                }
        }
        else
        {
                ll k=1;
                ll l = 1;
                ll r = n;
                while(l<=r)
                {
                        if(k%2)
                                narr[l] = arr[r], narr[r] = arr[l];
                        else
                                narr[l] = arr[l], narr[r] = arr[r];
                        k++, l++, r--;
                }
        }
        REP(i, n)
        {
                cout<<narr[i+1]<<" ";
        }
}