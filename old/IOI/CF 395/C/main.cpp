#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <fstream>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

typedef long long ll;
const ll MX = 1e5 + 4;
ll arr[MX], cnt[MX], n, kx, dp[MX];

int main()
{
        cin>>n;
        REP(i, n)
        {
                cin>>arr[i];
                kx = max(arr[i], kx);
                cnt[arr[i]]++;
        }
        dp[0]=0;
        dp[1]=cnt[1];
        FOR(i, 2, kx)
        {
                dp[i] = max(dp[i-1], dp[i-2]+(cnt[i]*i));
        }
        cout<<dp[kx]<<endl;
}