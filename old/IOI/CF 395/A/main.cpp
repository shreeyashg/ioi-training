#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <fstream>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

typedef long long ll;

const ll MX = 1e5;

int main()
{
        ll n, m, z;
        cin>>n>>m>>z;
        ll cnt=0;
        for(ll i=1; i*n<=z; i++)
        {
                if((n*i)%m==0)
                        cnt++;
        }
        cout<<cnt<<endl;
}