#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <fstream>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <iomanip>
#include <functional>
using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

typedef long long ll;

const ll NX = 1e5 + 4;
const ll KX = 24;

ll dp[NX][KX][3], n, k, arr[NX];
map<char, char> mp;

ll rec(ll idx, ll kx, ll curr)
{
        if(dp[idx][kx][curr]!=-1)
                return dp[idx][kx][curr];
        if(idx>n)
                return 0;
        if(kx>k)
                return 0;
        if(curr==1)
        {
                return dp[idx][kx][curr] = (arr[idx]==curr) + max(rec(idx+1, kx+1, 0), max(rec(idx+1, kx+1, 2), rec(idx+1, kx, 1)));
        }
        if(curr==2)
        {
                return dp[idx][kx][curr] = (arr[idx]==curr) + max(rec(idx+1, kx+1, 0), max(rec(idx+1, kx, 2), rec(idx+1, kx+1, 1)));
        }
        if(curr==0)
                return dp[idx][kx][curr] = (arr[idx]==curr) + max(rec(idx+1, kx, 0), max(rec(idx+1, kx+1, 2), rec(idx+1, kx+1, 1)));
        
        return 0;
}

int main()
{
        ifstream cin("hps.in");
        ofstream cout("hps.out");
        memset(dp, -1, sizeof(dp));
        mp['P'] = 0;
        mp['H'] = 1;
        mp['S'] = 2;
        cin>>n>>k;
        arr[0] = 'C';
        REP(i, n)
        {
                char ch;
                cin>>ch;
                arr[i+1] = mp[ch];
        }
        cout<<max(rec(0, 0, 0), max(rec(0, 0, 1), rec(0, 0, 2)))<<endl;
}