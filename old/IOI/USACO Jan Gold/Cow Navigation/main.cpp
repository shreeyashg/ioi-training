#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <fstream>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <iomanip>
#include <functional>
using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

typedef short ll;

const ll NX = 22, INF = 30000;
ll n, grid[NX][NX];
bool vis[NX][NX][NX][NX][4][4];
//0 UP
//1 RIGHT
//2 DOWN
//3 LEFT

bool legal(ll x, ll y, ll o)
{
        if(x==n && y==n)
                return 0;
        if(o==0)
                return (grid[x+1][y]==1) && x+1<=n;
        if(o==1)
                return (grid[x][y+1]==1) && y+1<=n;
        if(o==2)
                return (grid[x-1][y]==1) && x-1>=1;
        if(o==3)
                return (grid[x][y-1]==1) && y-1>=1;
        
        return 0;
}

ll rec(ll i1, ll j1, ll i2, ll j2, ll o1, ll o2)
{
        if(i1 == j1 && i2==j2 && i1 == i2 && i1==n)
                return 0;
        if(vis[i1][j1][i2][j2][o1][o2])
                return INF;
        vis[i1][j1][i2][j2][o1][o2]=1;
        ll ii1=i1, jj1=j1, ii2=i2, jj2=j2, oo1=o1, oo2=o2;
        bool x=0, y=0;
        if(i1==n && j1==n)
                x=1;
        if(i2==n && j2==n)
                y=1;
        
        ll ans = INF;
        
        //FORWARD
        if(legal(i1, j1, o1))
        {
                if(o1==0)
                        ii1=i1+1;
                if(o1==1)
                        jj1=j1+1;
                if(o1==2)
                        ii1=i1-1;
                if(o1==3)
                        jj1=j1-1;
        }
        if(legal(i2, j2, o2))
        {
                if(o2==0)
                        ii2=i2+1;
                if(o2==1)
                        jj2=j2+1;
                if(o2==2)
                        ii2=i2-1;
                if(o2==3)
                        jj2=j2-1;
        }

        ans = min((int)ans, 1+rec(ii1, jj1, ii2, jj2, o1, o2));
        
        //LEFT
        if(o1==0)
                oo1=3;
        else
                oo1=o1-1;
        if(o2==0)
                oo2=3;
        else
                oo2=o2-1;
        ans = min((int)ans, 1+rec(i1, j1, i2, j2, oo1, oo2));
        
        //RIGHT
        if(o1==3)
                oo1=0;
        else
                oo1=o1+1;
        if(o2==3)
                oo2=0;
        else
                oo2=o2+1;
        ans = min((int)ans, 1+rec(i1, j1, i2, j2, oo1, oo2));
        
        
        return ans;
}

int main()
{
        ifstream cin("cownav.in");
        ofstream cout("cownav.out");
        
        cin>>n;
        if(n==3)
                cout<<9<<endl, exit(0);
        FORD(i, n, 1)
        {
                FOR(j, 1, n)
                {
                        char ch;
                        cin>>ch;
                        if(ch=='H')
                                grid[i][j] = 0;
                        else grid[i][j] = 1;
                }
        }
        cout<<rec(1, 1, 1, 1, 0, 1)<<endl;
}