#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <fstream>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

typedef long long ll;

const ll MX = 1e5 + 4;

ll n;
vector<ll> sorted, v;
map<ll, ll> mp;

int main()
{
        ifstream cin("bphoto.in");
        ofstream cout("bphoto.out");
        cin>>n;
        REP(i, n)
        {
                ll k;
                cin>>k;
                v.push_back(k);
        }
        sorted = v;
        sort(sorted.begin(), sorted.end());
        ll cnt=0;
        REP(i, v.size())
        {
                ll left = 0, right=0;
                REP(j, i)
                {
                        if(v[j]>v[i])
                                left++;
                }
                FOR(j, i+1, v.size()-1)
                {
                        if(v[j]>v[i])
                                right++;
                }
                if(max(left, right)>2*min(left, right))
                        cnt++;
        }
        cout<<cnt<<endl;
}