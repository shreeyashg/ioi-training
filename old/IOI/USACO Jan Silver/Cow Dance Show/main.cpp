#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <fstream>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MX = 1e4 + 3;

ll n, hi, lim, lo, arr[MX], mid;

bool chk(ll x)
{
        priority_queue<ll, vector<ll>, greater<ll> >pq;
        ll curr=0, nxt=x, elapsed=0;
        REP(i, x)
        {
                pq.push(arr[i]);
        }
        while(!pq.empty())
        {
                elapsed+=(pq.top()-elapsed);
                pq.pop();
                if(x<n)pq.push(arr[x]+elapsed), x++;
        }
       // cout<<elapsed<<endl;
        if(elapsed<=lim)
                return 1;
        return 0;
}

int main()
{
        ifstream cin("cowdance.in");
        ofstream cout("cowdance.out");
        cin>>n>>lim;
        REP(i, n)
        {
                cin>>arr[i];
        }
        lo = 1;
        ll ans=n;
        hi = n;
        REP(i, 20)
        {
              //  DEBUG(mid);
                mid = (lo+hi)/2;
                if(chk(mid))
                        hi = mid-1, ans = min(ans, mid);//, cout<<mid<<endl;
                else
                        lo = mid+1;
        }
        
        cout<<ans<<endl;
}