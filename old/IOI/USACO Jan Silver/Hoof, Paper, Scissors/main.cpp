#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <fstream>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MX = 1e5 + 4;

vector<char> v;
ll n;
ll P[MX], H[MX], S[MX];

int main()
{
        ifstream cin("hps.in");
        ofstream cout("hps.out");
        cin>>n;
        REP(i, n)
        {
                char c;
                cin>>c;
                v.push_back(c);
        }
        REP(i, v.size())
        {
                if(i!=0)P[i] = P[i-1], H[i] = H[i-1], S[i] = S[i-1];
                if(v[i]=='P') P[i]++;
                if(v[i]=='H') H[i]++;
                if(v[i]=='S') S[i]++;
        }
        
        ll ans=max(H[n-1], max(P[n-1], S[n-1]));
        FOR(i, 1, n-1)
        {
                ans = max(ans, P[i]+(H[n-1]-H[i-1]));
                ans = max(ans, P[i]+(S[n-1]-S[i-1]));
                ans = max(ans, H[i]+(P[n-1]-P[i-1]));
                ans = max(ans, H[i]+(S[n-1]-S[i-1]));
                ans = max(ans, S[i]+(H[n-1]-H[i-1]));
                ans = max(ans, S[i]+(P[n-1]-P[i-1]));
        }
        
        cout<<ans<<endl;
}