#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <fstream>
#include <set>
#include <algorithm>
#include <cmath>
#include <deque>
using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define NMAX 1000009
#define INF 1000009*1e9

typedef long long ll;

ll n, k;

ll rec(ll x, ll idx, ll cnt)
{
        if(x==1)
                return 1;
        
        if(idx==(x/2)+1)
        {
                return n-cnt;
        }
        
        if(idx>x/2+1)
        {
                return rec(x/2, idx-x/2-1, cnt+1);
        }
        else
                return rec(x/2, idx, cnt+1);
}

int main()
{
        cin>>n>>k;
       
                ll f = (ll)pow(2, n)-1;
                cout<<rec((ll)pow(2, n)-1, k, 0)<<endl;
        
}