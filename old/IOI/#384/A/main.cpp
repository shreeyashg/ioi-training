#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <fstream>
#include <set>
#include <stack>
#include <unordered_map>
#include <algorithm>
#include <deque>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define NMAX 1000009

const ll INF = 1e18 + 5;

typedef long long ll;

ll n, a, b;
string str;
int main()
{
        cin>>n>>a>>b;
        cin>>str;
        if(str[a-1]==str[b-1])
        {
                cout<<0<<endl;
                exit(0);
        }
        else
        {
                ll mind = abs(b-a);
                FOR(i, a, n-1)
                {
                        if(str[i]==str[b-1])
                        {
                                mind = min(mind, abs(i-a+1));
                        }
                }
                
                FORD(i, a-2, 0)
                {
                        if(str[i]==str[b-1])
                        {
                                mind = min(mind, abs(i-a+1));
                        }
                }
                
                cout<<mind<<endl;
        }
}