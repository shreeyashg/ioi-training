#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <fstream>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

typedef long long ll;

const ll MX = 1e5 + 4, INF = 1e9 + 5;

ll orr[MX*4], xorr[MX*4], n, q, arr[MX*4], type, qs, qe, lazy[4*MX];

void build(ll s, ll e, ll idx)
{
        if(s==e)
        {
                orr[idx] = arr[s];
                xorr[idx] = arr[s];
                // gcd[idx] = arr[s];
                return;
        }
        
        build(s, (s+e)/2, 2*idx);
        build((s+e)/2 + 1, e, 2*idx + 1);
        
        xorr[idx]=orr[2*idx]^orr[2*idx+1];
        orr[idx]=xorr[2*idx]|xorr[2*idx+1];
        // gcd[idx] = __gcd(gcd[2*idx], gcd[2*idx+1]);
}

void update(ll s, ll e, ll idx, ll arridx, ll val)
{
        if(s>arridx || e<arridx)
                return;
        if(s==e && s==arridx)
        {
                // cout<<s<<" "<<e<<endl;
                xorr[idx] = arr[arridx];
                orr[idx] = arr[arridx];
                // if(val)gcd[idx]=val;
                return;
        }
        ll mid = (s+e)/2;
        if(arridx>=s && arridx<=mid)
                update(s, mid, idx*2, arridx, val);
        else
                update(mid+1, e, idx*2+1, arridx, val);
        xorr[idx]=orr[2*idx]^orr[2*idx+1];
        orr[idx]=xorr[2*idx]|xorr[2*idx+1];
        
}

int main()
{
        ios::sync_with_stdio(false);
        cin>>n>>q;
        REP(i, pow(2,n)) cin>>arr[i+1];
        build(1, pow(2,n), 1);
        ll y, z;
        //cin>>q;
        REP(i, q)
        {
                // cin>>x;
                cin>>y>>z;
                arr[y]=z;
                update(1, pow(2,n), 1, y, z);
                if(n%2)
                        cout<<orr[1]<<'\n';
                else
                        cout<<xorr[1]<<'\n';
        }
}