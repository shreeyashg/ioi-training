#include <iostream>
#include <vector>
#include <queue>
#include <iomanip>
#include <map>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <set>
#include <climits>
#include <algorithm>
#include <deque>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)

const ll MX = 1e5 + 4;
const ll INF = 1e18;
ll dist[MX], n, m, s, e, vis[MX];;

void dij(vector<pair<ll,ll> >v[MX])
{
        memset(vis, 0, sizeof(vis));
        REP(I, MX) dist[I] = INF;
        dist[s] = 0;
        priority_queue<pair<ll, ll>, vector<pair<ll, ll> >, greater<pair<ll, ll> > > q;
        q.push({0, s});
        while(!q.empty())
        {
                auto t = q.top();
                q.pop();
                
               // cout<<" > "<<t.second<<endl;
                ll id = t.second;
                ll di = t.first;
                
                if(!vis[id])
                        vis[id] = 1;
                else
                        continue;
                
                REP(i, v[id].size())
                {
                        ll nid = v[id][i].first;
                        ll ndist = di + v[id][i].second;
                        if(!vis[nid] && ndist<dist[nid])
                        {
                                dist[nid] = ndist;
                                q.push({ndist, nid});
                                //if(!vis[nid])vis[nid] = 1;
                        }
                }
        }
}

int main()
{
        ios::sync_with_stdio(false);
        cin.tie(0);
        cout.tie(0);
        ll t;
        scanf("%lld", &t);
        while(t--)
        {
                vector<pair<ll, ll> >v[MX];
                scanf("%lld %lld", &n, &m);
                REP(i, m)
                {
                        ll a, b, c;
                        scanf("%lld %lld %lld", &a, &b, &c);
                        v[a].push_back({b, c});
                }
                scanf("%lld %lld", &s, &e);
                dij(v);
                if(dist[e]>=INF)
                {
                        printf("NO\n");
                }
                else
                        printf("%lld\n", dist[e]);
        }
}