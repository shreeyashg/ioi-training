// See SPOJ SEQ/SPP

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

typedef vector<vector<ll> > mx;

const ll MOD = 1e9 + 7;
mx I;
ll m, n;
ll K;


mx mul(mx a, mx b)
{
        mx c(m+1, vector<ll>(m+1));
        FOR(i,1,m) FOR(j,1,m) FOR(k,1,m)
        {
                c[i][j] = (c[i][j] + ((a[i][k] * b[k][j])%MOD)) % MOD;
        }
        return c;
}

mx pow(mx a, ll x)
{
        if(x==0)
                return I;
        if(x==1)
                return a;
        if(x%2)
                return mul(a, pow(a, (x-1)));//, pow(a, (x-1)/2)));
        mx X = pow(a,x/2);
        return mul(X, X);
}

// if on odd row, f(i, j) = f(i-1, j) + f(i-1, j-1) + f(i-1, j+1)
// else f(i, j) = f(i-1, j-1) + f(i-1, j+1)

int main()
{
        I.resize(m+1, vector<ll>(m+1));
        
        FOR(i, 1, m)
        {
                I[i][i]=1;
        }
        
        ll t;
        cin>>t;
        while(t--)
        {
                mx M_odd, M_even, M, T;
         //       ll n, m;
                cin>>n>>m;
                if(n==1){cout<<m<<endl;continue;}
                M_odd.resize(m+1, vector<ll>(m+1));
                M_even.resize(m+1, vector<ll>(m+1));
                
                
                FOR(i, 1, m)
                {
                        if(i-1>=1)M_even[i][i-1]=1, M_odd[i][i-1]=1;//, M_even[i-1][i]=0;
                        if(i+1<=m)M_even[i][i+1]=1, M_odd[i][i+1]=1;//, M_even[i+1][i]=0;
                        M_even[i][i]=1;
                        
                }
                
                K = m;
                M = mul(M_odd, M_even);
                n--;
                if(n==1)
                {
                        ll ann=0;
                        FOR(i ,1, m)
                        {
                                FOR(j, 1, m)
                                {
                                        ann+=M_odd[i][j];
                                }
                                //cout<<endl;
                        }
                        cout<<ann<<endl;
                        continue;
                }
                T = pow(M, n/2);
                //M = mul(M_odd, M_even);
                if(n%2)
                        T = mul(T, M_odd);
                ll ans=0;
                
                FOR(i, 1, m)
                {
                        FOR(j, 1, m)
                        {
                                ans = (ans + T[i][j]) % MOD;
                        }
                }
                cout<<ans % MOD<<endl;
                //M_odd.clear(), M_even.clear(), M.clear(), T.clear();
        }
}