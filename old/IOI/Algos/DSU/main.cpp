#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <fstream>
#include <set>
#include <cmath>
#include <unordered_map>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

typedef int ll;

const ll MX = 2e5 + 4;

ll n, m;
unordered_map<ll, ll> id;//arr, sz;
ll arr[MX], sz[MX];
set<ll> s;

inline ll finds(ll x)
{
        if(x!=arr[x])
                return arr[x] = finds(arr[x]);
        return arr[x];
}

inline bool find(ll x, ll y)
{
        return finds(x)==finds(y);
}

void uni(ll x, ll y)
{
        x = finds(x);
        y = finds(y);
        if(x==y)
                return;
        if(sz[x]<sz[y]) arr[x] = arr[y], sz[y]+=sz[x];
        else arr[y] = arr[x], sz[x]+=sz[y];
}

int main()
{
        vector<pair<ll, ll> >v;
        scanf("%d %d", &n, &m);
        REP(i, m)
        {
                ll x, y;
                scanf("%d", &x);
                scanf("%d", &y);
                v.push_back({x,y});
                s.insert(x), s.insert(y);
        }
        vector<ll> q(s.begin(), s.end());
        REP(i, q.size()) id[q[i]] = i, arr[i]=i;
        REP(i, v.size())
        {
                uni(id[v[i].first], id[v[i].second]);
        }
        set<ll> f;
        REP(i, q.size())
        {
                f.insert(finds(q[i]));
        }
        printf("%lu\n", n-s.size()+f.size());
}

