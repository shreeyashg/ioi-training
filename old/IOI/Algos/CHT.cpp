#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n;
ll h[MX], b[MX], dp[MX];

int main()
{
	cin >> n;
	REP(i, n) cin >> h[i + 1];
	REP(i, n) cin >> b[i + 1];

	FOR(i, 1, n) dp[i] = INF;

	dp[1] = 0;
	FOR(i, 2, n)
	{
		FOR(j, 1, i)
		{
			dp[i] = min(dp[i], dp[j] + b[j] * h[i]);
		}
	}

	cout << dp[n] << endl;

}