#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

const ll MX = 1e5 + 4;

vector<ll> v[4 * MX];
ll arr[MX], n;

void build(ll ii, ll jj, ll idx)
{
	cout<<ii<<" "<<jj<<" "<<idx<<endl;
	if(ii == jj)
	{
		vector<ll> x = {arr[ii]};
		v[idx] = x;
		return;
	}
	build(ii, (ii+jj)/2, 2*idx);
	build((ii+jj)/2 + 1, jj, 2*idx+1);
	vector<ll> left = v[2*idx];
	vector<ll> right = v[2*idx + 1];
	vector<ll> vv;
	vv.resize(left.size() + right.size());
	ll l = 0, r = 0, nxt = 0;
	while(true)
	{
		if(l==left.size() && r == right.size())
			break;

		if(l < left.size() && left[l] <= right[l])
			vv[nxt] = left[l], l++, nxt++;
		else if(r < right.size())
			vv[nxt] = right[r], r++, nxt++;
		else
			break;
	}
	v[idx] = vv;
}

int main()
{
	cin>>n;
	for(int i = 1; i <= n; i ++) cin >> arr[i];

	build(1, n, 1);
	cout<<v[2].size()<<endl;
	for(int i =0; i<v[2].size(); i++) cout<<v[2][i]<<" ";
	cout<<endl;
}