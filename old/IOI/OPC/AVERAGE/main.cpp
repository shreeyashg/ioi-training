#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <utility>
#include <set>
#include <map>
#include <cmath>
#include <algorithm>

using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MX = 1e4 + 2;
const ll NX = 154;

vector<double> arr;
ll n;
map<ll,ll>mp;
set<ll> s;

inline double avg(ll x, ll y)
{
        return (x+y)/2.0;
}


int main()
{
        ll ans=0;
        cin>>n;
        arr.resize(n);
        REP(i, n) {cin>>arr[i], s.insert(arr[i]), mp[arr[i]]++;}
        
        sort(arr.begin(), arr.end());
        for(set<ll>::iterator it = s.begin(); it!=s.end(); it++)
        {
                if(mp[*it]>1)ans += mp[*it];
        }
        ll l = 0;
        ll u = n-1;
        
       /* REP(i, arr.size())
        {
                cout<<arr[i]<<endl;
        } */
        
        
        FOR(i, 1, arr.size()-2)
        {
                l = i-1;
                u = i+1;
                
                while(l>=0 && u<=arr.size()-1)
                {
                        //cout<<arr[i]<<endl;
                        //cout<<arr[l]<<" "<<arr[u]<<endl;
                        if(arr[l]!=arr[u] && avg(arr[l],arr[u]) == arr[i])
                        {
                                ans += mp[arr[i]];
                                break;
                        }
                        else if(avg(arr[l],arr[u]) < arr[i])
                        {
                                u++;
                        }
                        else
                                l--;
                }
                
        }
        
        
        cout<<ans<<endl;
}