#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstdio>
#include <cstring>
#include <set>
#include <algorithm>
#include <cmath>
#include <bitset>

using namespace std;

typedef long long ll;

#define FOR(i, a, b) for(ll i=a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define REP(i, n) for(ll i=0; i<n; i++)
#define DEBUG(x) cout<<#x<<" : "<<x<<endl
#define lb(x, s) lower_bound(x.begin(), x.end(), s)
#define pb push_back
#define pi 3.1415926536

const ll INF = 1e13+ 5;
const ll MX = 905;

ll n;
ll a[MX], b[MX];
ll dp[MX][MX];
map<ll, ll> mp2;
vector<ll> c1, c2;

map<ll, map<ll, pair<ll,ll> > >choice;

ll rec(ll x, ll y)
{
        if(x>n || y>n)
                return 0;
        
        if(dp[x][y]!=-1)
                return dp[x][y];
        
        ll ans=0;
        choice[x][y] = {-1,-1};
        FOR(i, x+1, n)
        {
                FOR(j, y+1, n)
                {
                        if(b[j]-b[y]==a[i]-a[x] && 1+rec(i, j)>ans)
                        {
                                choice[x][y] = {i,j};
                                ans = 1+rec(i, j);
                        }
                }
        }
        
        return dp[x][y] = ans;
}


int main()
{
        srand(time(NULL));
        REP(i, MX)
        {
                REP(j, MX)
                {
                        dp[i][j]=-1;
                }
        }
        //cin>>n;
        n = rand()%300 + 1;
        REP(i, n)
        {
                //cin>>a[i+1];
                a[i+1] = rand()%10000;
                //DEBUG(a[i+1]);
        }
        REP(i, n)
        {
                //cin>>b[i+1];
                b[i+1] = rand()%10000;
             //   mp2[b[i+1]]=i+1;
        }
        
        
        ll ans=0;
        pair<ll, ll> in;
        FOR(i, 1, n)
        {
                FOR(j, 1, n)
                {
                        if(1+rec(i, j)>ans)
                        {
                                in = {i, j};
                                ans = 1+rec(i, j);
                        }
                }
        }
        
        cout<<ans<<endl;
        
        ll aa = in.first, bb = in.second;
        vector<ll> v1, v2;
        while(aa!=-1 && bb!=-1)
        {
                v1.push_back(a[aa]), v2.push_back(b[bb]);
                ll k1 = aa, k2 = bb;
                aa = choice[k1][k2].first, bb = choice[k1][k2].second;
        }
        
        REP(i, v1.size())
        {
                cout<<v1[i]<<" ";
        }
        cout<<'\n';
        REP(i, v2.size())
        {
                cout<<v2[i]<<" ";
        }
        cout<<'\n';
}