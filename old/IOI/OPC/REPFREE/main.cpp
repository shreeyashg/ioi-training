#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <string>
#include <cstring>
#include <utility>
#include <set>
#include <map>
#include <cmath>
#include <algorithm>
#include <sstream>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

bool is(ll x)
{
        stringstream ss;
        ss << x;
        string str = ss.str();
        set<ll> s;
        REP(i, str.size())
        {
                if(str[i]=='0')
                        return 0;
                s.insert(str[i]);
        }
        if(s.size()==str.size())
                return 1;
        else
                return 0;
}

ll n;

int main()
{
        cin>>n;
        if(n>=987654321) {
                cout<<0<<endl;
                exit(0);
        }
        
        FOR(i, n+1, 1e10-1)
        {
                if(is(i)) {
                        cout<<i<<endl;
                        exit(0);
                }
        }
        cout<<0<<endl;
        exit(0);
}