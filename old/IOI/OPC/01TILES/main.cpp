#include <iostream>
using namespace std;

typedef long long ll;

ll dp[1000001];
#define MOD 15746

int main()
{
        ll n;
        cin>>n;
        dp[0]=1;
        dp[1]=2;
        for(ll i=2; i<n; i++)
        {
                dp[i] = (dp[i-1]%MOD + dp[i-2]%MOD)%MOD;
        }
        
        cout<<dp[n-1]<<endl;
}