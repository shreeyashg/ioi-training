#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>

using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 1000001
#define MOD 1000000007
#define INF 1000009*1e9

ll n;
string str;

bool is(ll i, ll j)
{
        if(i>=j)
                return 1;
        if(str[i]!=str[j])
                return 0;
        return is(i+1, j-1);
}

int main()
{
        //str = "mandnamasf";
        cin>>n;
        cin>>str;
        
        ll ans=0;
        ll aj=0, ai=0;
        
        FOR(i, 0, str.size()-1)
        {
                FOR(j, i+1, str.size()-1)
                {
                        if(is(i, j))
                        {
                                if(j-i+1>ans)
                                {
                                        ans = j-i+1;
                                        aj = j;
                                        ai = i;
                                }
                        }
                }
        }
        
        cout<<ans<<endl;
        FOR(i, ai, aj)
        {
                cout<<str[i];
        }
        cout<<'\n';
}