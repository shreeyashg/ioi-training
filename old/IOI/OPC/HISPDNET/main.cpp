#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <cstdio>
using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MOD 1000000007

typedef long long ll;

const ll NX = 2004, INF = 1e18;
ll adj[NX][NX], in[NX];

int main()
{
        ios::sync_with_stdio(false);
        cin.tie(0);
        ll n;
        cin>>n;
        REP(i, n)
        {
                REP(j, n)
                {
                        cin>>adj[i+1][j+1];
                }
        }
        
        in[1] = n-1;
        FOR(i, 2, n) in[i] = 1;
        ll ans=0;
        FOR(i, 2, n)
        {
                if(in[i]<2)
                {
                        ll mn = INF, p1=0, p2=0;
                        FOR(j, 2, n)
                        {
                                if(adj[i][j]<mn && j!=i)
                                {
                                        mn = adj[i][j];
                                        p1 = i, p2 = j;
                                }
                        }
                      //  cout<<p1<<" "<<p2<<endl;
                        ans+=mn;
                        in[p1]++;
                        in[p2]++;
                        adj[p1][p2] = INF;
                        adj[p2][p1] = INF;
                }
        }
        
        cout<<ans<<endl;
}