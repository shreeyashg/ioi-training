#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>

using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 200001
#define MOD 1000000007
#define INF 1000009*1e9

ll n, m, c;
ll arr[1001][1001];
ll ps[1002][1002];

int main()
{
        ios::sync_with_stdio(false);
        cin.tie(0);
        
        cin>>n>>m;
        REP(i, n)
        {
                REP(j, m)
                {
                        cin>>arr[i][j];
                }
        }
        
        FOR(i, 1, n)
        {
                FOR(j, 1, m)
                {
                        ps[i][j] = arr[i-1][j-1]+ps[i-1][j]+ps[i][j-1]-ps[i-1][j-1];
                }
        }
        
     /*   FOR(i, 1 ,n)
        {
                FOR(j, 1, m)
                {
                        cout<<ps[i][j]<<" ";
                }
                cout<<endl;
        }
        
        FOR(i, 1, n)
        {
                FOR(j, 1, m)
                {
                        cout<<ps[i][j]<<" ("<<i<<','<<j<<')'<<endl;
                }
        }
        */
        cin>>c;
        REP(i, c)
        {
                ll x1, y1, x2, y2;
                //if(x1==x2 && y1==y2)
                  //      cout<<ps[x1][y1]<<endl;
                cin>>x1>>y1>>x2>>y2;
                cout<<ps[x2][y2]-ps[x1-1][y2]-ps[x2][y1-1]+(ps[x1-1][y1-1])<<'\n';
        }
}