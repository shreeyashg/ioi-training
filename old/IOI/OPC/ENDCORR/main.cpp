#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <string>
#include <cstring>
#include <utility>
#include <set>
#include <map>
#include <cmath>
#include <algorithm>
#include <sstream>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

ll n, k;

int main()
{
        priority_queue<ll> pq;
        cin>>n>>k;
        REP(i, n+k)
        {
                ll k;
                cin>>k;
                if(k==-1)
                        cout<<pq.top()<<endl, pq.pop();
                else
                        pq.push(k);
        }
}