#include <iostream>
#include <cstring>
#include <algorithm>
using namespace std;

typedef long long ll;

#define FOR(i,a ,b) for(ll i=a; i<=b; i++)
#define FORD(i, a, b) for(ll i=a; i>=b; i--)
#define REP(i, n) for(ll i=0; i<n; i++)
#define DEBUG(x) cout<<#x<<" --> "<<x<<endl;

const ll MX = 1e4 + 1;

int n, arr[MX];
int dp[MX];

ll rec(ll x)
{
        if(dp[x]!=-1)
                return dp[x];
        
        ll ans=1;
        FORD(i, x-1, 0)
        {
                if(arr[x]%arr[i]==0)
                {
                        ans = max(ans, 1+rec(i));
                }
        }
        
        return dp[x] = ans;
}

int main()
{
        ios::sync_with_stdio(false);
        cin.tie(0);
        memset(dp, -1, sizeof(dp));
        cin>>n;
        REP(i, n)
        {
                cin>>arr[i];
        }
        
        //REP(i, n) cout<<arr[i]<<endl;
        
        ll ans=0;
        FOR(i, 0, n-1)
        {
                ans = max(ans, rec(i));
        }
        cout<<ans<<endl;
}