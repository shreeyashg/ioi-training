#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <utility>
#include <set>
#include <map>
#include <cmath>
#include <algorithm>

using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MX = 1e5 + 1e5;
const ll NX = MX;
const ll INF = 1e17 + 11;

ll a, b, c;
map<ll, ll> mp;
ll arr[MX];
vector<ll> v;

int main()
{
        cin>>a>>b>>c;
        REP(i, a+b+c)
        {
                cin>>arr[i];
                mp[arr[i]]++;
        }
        ll ans=0;
        REP(i, a+b+c)
        {
                if(mp[arr[i]]>=2)
                {
                        ans++;
                        mp[arr[i]]=0;
                        v.push_back(arr[i]);
                }
        }
        sort(v.begin(), v.end());
        cout<<ans<<endl;
        REP(i, v.size())
        {
                cout<<v[i]<<'\n';
        }
}