#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>

using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 200001
#define MOD 1000000007
#define INF 1000009*1e9

vector<ll> v;
ll n;

ll dp[MX];

ll lis(ll idx)
{
        if(idx>n-1)
                return 0;
        if(dp[idx] && idx!=-1)
                return dp[idx];
        ll ans=0;
        FOR(i, idx+1, n-1)
        {
                if(v[i]>v[idx]||idx==-1)
                {
                        ans = max(ans, 1+lis(i));
                }
        }
        return dp[idx]=ans;
}

int CeilIndex(std::vector<int> &v, int l, int r, int key) {
        while (r-l > 1) {
                int m = l + (r-l)/2;
                if (v[m] >= key)
                        r = m;
                else
                        l = m;
        }
        
        return r;
}

int LongestIncreasingSubsequenceLength() {
        if (v.size() == 0)
                return 0;
        
        std::vector<int> tail(v.size(), 0);
        int length = 1; // always points empty slot in tail
        
        tail[0] = v[0];
        for (size_t i = 1; i < v.size(); i++) {
                if (v[i] < tail[0])
                        // new smallest value
                        tail[0] = v[i];
                else if (v[i] > tail[length-1])
                        // v[i] extends largest subsequence
                        tail[length++] = v[i];
                else
                        // v[i] will become end candidate of an existing subsequence or
                        // Throw away larger elements in all LIS, to make room for upcoming grater elements than v[i]
                        // (and also, v[i] would have already appeared in one of LIS, identify the location and replace it)
                        tail[CeilIndex(tail, -1, length-1, v[i])] = v[i];
        }
        
        return length;
}

int main()
{
        cin>>n;
        REP(i, n)
        {
                ll k;
                cin>>k;
                v.push_back(k);
        }
        
        ll x=0;
        FOR(i, 1, v.size()-1)
        {
                if(v[i]<v[i-1])
                        x++;
        }
        
        ll ans = 0;
        //cout<<n-lis(-1)<<endl;
        cout<<n-LongestIncreasingSubsequenceLength()<<endl;
}