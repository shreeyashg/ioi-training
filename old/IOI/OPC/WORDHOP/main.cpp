#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <string>
#include <cstring>
#include <utility>
#include <set>
#include <map>
#include <cmath>
#include <algorithm>
#include <sstream>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

ll n;
vector<string> v;
ll choice[101];

bool can(ll x, ll y)
{
        if(v[x].size()==v[y].size())
        {
                ll cnt=0, pos=-1;
                REP(i ,v[x].size())
                {
                        if(v[x][i]!=v[y][i])
                        {
                                cnt++;
                                pos = i;
                        }
                }
                
                if(cnt==1 && v[y][pos]>v[x][pos])
                {
                        if(v[x].find_last_of(v[y][pos])!=string::npos && v[x].find_last_of(v[y][pos])>pos)
                        {
                                return 1;
                        }
                }
        }
        else if(v[x].size()-v[y].size()==1)
        {
                ll px = 0, py = 0, err = 0;
                while(px<v[x].size() && py<v[y].size())
                {
                        if(v[x][px] == v[y][py])
                                px++, py++;
                        else
                                err++,px++;
                }
                if(err<=1)
                        return true;
                
        }
        
        return 0;
}

ll dp[105];


ll rec(ll idx)
{
        if(dp[idx]!=-1)
                return dp[idx];
        
        ll ans=1;
        
        FOR(i, idx+1, v.size()-1)
        {
                if(can(idx, i))
                {
                        if(1+rec(i)>ans)
                        {
                                ans = 1+rec(i);
                                choice[idx] = i;
                        }
                }
        }
        
        return dp[idx] = ans;
}

bool cmp(string a, string b)
{
        if(a.size()!=b.size()) return a.size()>b.size();
        else return a<b;
}

int main()
{
        memset(dp, -1, sizeof(dp));
        cin>>n;
        REP(i, n)
        {
                string str;
                cin>>str;
                v.push_back(str);
        }
        sort(v.begin(), v.end(), cmp);

       // DEBUG(can(1, 2));
        
        ll ans=0;
        
      /*  REP(i, v.size())
        {
                cout<<v[i]<<endl;
        }
         */
        
        REP(i, n)
        {
                if(rec(i)>ans)
                {
                        choice[-1] = i;
                        ans  = rec(i);
                }
        }
        
        cout<<ans<<endl;
      /*  ll x = choice[-1];
        while(choice[x]!=0)
        {
                cout<<v[x]<<" ";
                x = choice[x];
        }
        cout<<v[x]<<" ";
      */
}