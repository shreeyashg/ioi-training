#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstdio>
#include <cstring>
#include <set>
#include <algorithm>
#include <cmath>
#include <bitset>

using namespace std;

typedef long long ll;

#define FOR(i, a, b) for(ll i=a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define REP(i, n) for(ll i=0; i<n; i++)
#define DEBUG(x) cout<<#x<<" : "<<x<<endl
#define lb(x, s) lower_bound(x.begin(), x.end(), s)
#define pb push_back
#define pi 3.1415926536

const ll INF = 1e13+ 5;
const ll MX = 1e6 + 3;

int n;
int v[MX];

int main()
{
        cin>>n;
        REP(i, n)
        {
                int k, q;
                scanf("%d %d", &k, &q);
                v[i]=min(k,q);
        }
        sort(v, v+n);
        int next=1;
        int ans=0;
        
        REP(i, n)
        {
                if(v[i]>=next)
                {
                        next++;
                        ans++;
                }
        }
        
        cout<<ans<<endl;
        
}