#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstdio>
#include <cstring>
#include <set>
#include <algorithm>
#include <cmath>
#include <bitset>

using namespace std;

typedef long long ll;

#define FOR(i, a, b) for(ll i=a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define REP(i, n) for(ll i=0; i<n; i++)
#define MX 2501
#define DEBUG(x) cout<<#x<<" : "<<x<<endl
#define lb(x, s) lower_bound(x.begin(), x.end(), s)
#define pb push_back
#define pi 3.1415926536

const ll INF = 1e13+ 5;

typedef pair<ll,ll> pii;

ll n,k;
vector<ll> v;
vector<pair<ll, pii> > ps;

bool cmp(pair<ll,pii>a, pair<ll,pii> b)
{
        return a.first<b.first;
}

int main()
{
        cin>>n;
        REP(i, n)
        {
                ll f;
                cin>>f;
                v.push_back(f);
        }
        
        REP(i, v.size())
        {
                if(!i)
                        ps.push_back(make_pair(v[i], make_pair(1, i+1)));
                else ps.push_back(make_pair(ps[i-1].first+v[i], make_pair(1, i+1)));
        }
        
        sort(ps.begin(), ps.end(), cmp);
        ll minans= INF;
        ll mx = 0, my = 0;

      /*  REP(i, ps.size())
        {
                cout<<ps[i].first<<" "<<ps[i].second.first<<" "<<ps[i].second.second<<endl;
        } */
        
        REP(i, ps.size()-1)
        {
                if(abs(ps[i+1].first-ps[i].first)<abs(minans))
                {
                        minans = ps[i+1].first-ps[i].first;
                        //my-mx<max(ps[i+1].second.second, ps[i].second.second)-min(ps[i+1].second.second, ps[i].second.second))
                        
                        mx = min(ps[i+1].second.second, ps[i].second.second)+1;
                        my = max(ps[i+1].second.second, ps[i].second.second);
                        
                }
        }
        
        cout<<-minans<<endl;
        cout<<mx<<" "<<my<<endl;
}