#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <cstdio>
using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MOD 1000000007

typedef int ll;

const ll MX = 1e6 + 3;
const ll NX = 1e4 * 4 + 2;
const ll INF = 1e9;

ll arr[MX], m, n, q[NX];

int main()
{
        cin>>m;
        REP(i, m)
        {
                cin>>arr[i];
        }
        
        cin>>n;
        REP(i, n)
        {
                cin>>q[i];
        }
        set<ll> s;
        
        REP(i, n)
        {
                for(set<ll>::iterator it = s.begin(); it!=s.end(); it++)
                {
                        if(q[i]>=*it)
                                q[i]++;
                }
                s.insert(q[i]);
                cout<<arr[q[i]-1]<<endl;
        }
        
        
}