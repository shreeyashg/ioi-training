#include <bits/stdc++.h>
using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

typedef long long ll;

const ll MX = 1e5 + 5, INF = 1e12;

vector<ll> v[MX];
ll n, h[MX], dp1[MX], dp2[MX];

void dfs(ll idx, ll par)
{
        ll a=0, b=0;
        REP(i, v[idx].size())
        {
                if(v[idx][i]==par) continue;
                dfs(v[idx][i], idx);
                a += dp2[v[idx][i]];
                b += max(dp1[v[idx][i]], dp2[v[idx][i]]);
        }
        
        dp1[idx] = h[idx]+a;
        dp2[idx] = b;
}

int main()
{
        ios::sync_with_stdio(false);
        cin.tie(0);
        cin>>n;
        REP(i, n)
        {
                cin>>h[i+1];
        }
        REP(i, n-1)
        {
                ll x ,y;
                cin>>x>>y;
                v[x].push_back(y);
                v[y].push_back(x);
        }
        dfs(1, 0);
        cout<<max(dp1[1], dp2[1])<<endl;
}
