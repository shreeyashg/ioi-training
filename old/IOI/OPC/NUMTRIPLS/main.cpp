#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <cstdio>
using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

typedef long long ll;

const ll MX = 4e6 + 4;
const ll NX = 2e3 + 4;
const ll INF = 1e18 + 4;

ll n, s, e;
set<ll> ss[NX];
bool vis[NX]; ll dist[NX];
ll wt[NX][NX];

int main()
{
        REP(i, NX)
        {
                REP(j, NX)
                {
                        wt[i][j] = 2005;
                }
        }
        ios::sync_with_stdio(false);
        cin.tie(0);
        cin>>n>>s>>e;
        REP(i, n)
        {
                ll a, b, c;
                cin>>a>>b>>c;
                ss[a].insert(c);
                ss[c].insert(a);
               // v[a].push_back(make_pair(c,b));
               // v[c].push_back(make_pair(a,b));
                wt[a][c] = min(wt[a][c], b);
                wt[c][a] = wt[a][c];
        }
        
        REP(i, NX) dist[i] = INF;
        priority_queue<pair<ll,ll>, vector<pair<ll,ll> >, greater<pair<ll,ll> > >q;
        q.push(make_pair(0, s));
        dist[s] = 0;
        while(!q.empty())
        {
                pair<ll,ll> t = q.top();
                q.pop();
                
             //   DEBUG(t.second);
                
                ll id = t.second, di = t.first;
                
                if(id==e)
                        break;
                
          /*      if(vis[id]==1)
                        continue;
                vis[id]=1; */
                
                for(set<ll>::iterator it = ss[id].begin(); it!=ss[id].end(); it++)
                {
                        ll i = *it;
                        ll nid = i;
                        ll ndist = wt[id][i] + di;
                        if(!vis[nid] && ndist<dist[nid])
                        {
                                dist[nid] = ndist;
                                q.push(make_pair(ndist, nid));
                              //  par[nid] = id;
                        }
                }
        }
        
        if(dist[e]<INF)
                cout<<"YES"<<endl<<dist[e]<<endl;
        else
                cout<<"NO"<<endl;
        
       // cout<<par[e]<<endl;
}

