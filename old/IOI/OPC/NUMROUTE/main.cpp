#include <bits/stdc++.h>
using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

typedef long long ll;
typedef vector<vector<ll> > mx;

ll n, ii, jj, kk;
mx a;

const ll MOD = 42373;

mx mul(mx a, mx b)
{
        mx c(n+1, vector<ll> (n+1));
        FOR(i, 1, n) FOR(j, 1, i) FOR(k, 1, n)
        {
                c[i][j] = (c[i][j] + (a[i][k] * b[k][j])%MOD) % MOD;
        }
        FOR(i, 1, n)
        {
                FOR(j, 1, i-1)
                {
                        c[j][i] = c[i][j];
                }
        }
        return c;
}

mx pow(mx mat, ll x)
{
        if(x==1)
                return mat;
        if(x%2)
                return mul(mat,pow(mat, (x-1)));
        mx X = pow(mat, x/2);
        return mul(X, X);
}

int main()
{
        ios::sync_with_stdio(false);
        cin.tie(0);
        cin>>n;
        a.resize(n+1, vector<ll>(n+1));
        REP(i,n) REP(j,n)
        {
                cin>>a[i+1][j+1];
        }
        cin>>ii>>jj>>kk;
        mx aa = pow(a, kk);
        cout<<aa[ii][jj]%MOD<<endl;
}