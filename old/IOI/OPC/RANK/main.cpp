#include <iostream>
#include <set>
#include <vector>
#include <map>
#include <algorithm>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MX = 45*1e3 + 5;

ll bit[MX];
vector<ll> a, b;
map<ll,ll> mp;
ll n;

ll query(ll aa)
{
        ll ans=0;
        while(aa>0)
        {
                ans+=bit[aa];
                aa-= (aa&-aa);
        }
        return ans;
}

void update(ll aa)
{
        while(aa<MX)
        {
                bit[aa]+=1;
                aa+= (aa&-aa);
        }
}

int main()
{
        cin>>n;
        REP(i, n)
        {
                ll k;
                cin>>k;
                a.push_back(k);
        }
        b=a;
        sort(b.begin(), b.end());
        REP(i, b.size())
        {
                mp[b[i]]=i+1;
        }
        
        REP(i, a.size())
        {
                ll idx = mp[a[i]];
                //DEBUG(idx);
                cout<<i-query(idx-1)+1<<endl;
                update(idx);
        }
}