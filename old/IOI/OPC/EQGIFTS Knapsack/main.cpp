#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <utility>
#include <set>
#include <cmath>
#include <algorithm>

using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MX = 304;
const ll NX = 154;

ll n;
ll ans;
ll arr[NX];

ll rec(ll x)
{
        if(x==n)
                return 0;
        
        return min(abs(arr[x]+rec(x+1)), abs(-arr[x]+rec(x+1)));
}

int main()
{
        cin>>n;
        REP(i, n)
        {
                ll a, b;
                cin>>a>>b;
                arr[i] = abs(a-b);
        }
        
        cout<<rec(0)<<endl;
}
