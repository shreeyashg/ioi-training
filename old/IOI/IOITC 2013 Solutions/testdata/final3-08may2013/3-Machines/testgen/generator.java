import java.io.*;
import java.util.*;

class generator
{
	BufferedReader keyin = new BufferedReader(new InputStreamReader(System.in));
	Random r = new Random();
	int N, K;
	int scheme, subtask;
	
	int maxn, maxk;
	
	int [][] M;
	
	int maxwt = 200000;
	
	int myrand(int a, int b)
	{
		int range = b-a+1;
		int x = r.nextInt() % range;
		if(x < 0) x += range;
		return a+x;
	}
	
	public static void main(String[] args) throws IOException
	{
		new generator().exec();
	}
	
	void input() throws IOException
	{
//		N = Integer.parseInt(keyin.readLine());
//		K = Integer.parseInt(keyin.readLine());
		
		/* schemes
		 * 0: Random
		 * 1: pos 0
		 * 2: pos 1
		 * 3: skewed random
		 * 4: K = 1
		 * 5: maxNK
		 */
		String[] sp = keyin.readLine().split(" ");
		subtask = Integer.parseInt(sp[0]);
		scheme = Integer.parseInt(sp[1]);
	}
	
	void exec() throws IOException
	{
		input();
		generate();
		output();
	}
	
	void generate()
	{
		switch (subtask) {
			case 1:
				maxn = 20;
				maxk = 10;
				break;
			case 2:
				maxn = 1000;
				maxk = 10;
				break;
			case 3:
				maxn = 10000;
				maxk = 100;
				break;
			case 4:
				maxn = 300000;
				maxk = 100000;
				break;
			default:
				break;
		}
		
		switch (scheme) {
			case 0:
				gen0();
				break;
			case 1:
				gen1();
				break;
			case 2:
				gen2();
				break;
			case 3:
				gen3();
				break;
			case 4:
				gen4();
				break;
			case 5:
				gen5();
				break;
			default:
				break;
		}
	}
	
	void gen0()
	{
		K = myrand(2, maxk);
		N = myrand(K, maxn);
		M = new int[N][2];

		for(int i = 0; i < N; i++)
		{
			M[i][0] = myrand(1, maxwt);
			M[i][1] = myrand(1, maxwt);
		}
	}
	
	void gen1()
	{
		K = myrand(2, maxk);
		N = myrand(K, maxn);
		M = new int[N][2];
		
		for(int i = 0; i < N; i++)
		{
			M[i][0] = myrand(1, maxwt-1);
			M[i][1] = myrand(M[i][0]+1, maxwt);
		}
	}
	
	void gen2()
	{
		K = myrand(2, maxk);
		N = myrand(K, maxn);
		M = new int[N][2];
		
		for(int i = 0; i < N; i++)
		{
			M[i][0] = myrand(2, maxwt);
			M[i][1] = myrand(1, M[i][0]-1);
		}
	}
	
	void gen3()
	{
		K = myrand(2, maxk);
		N = myrand(K, maxn);
		M = new int[N][2];
		
		int left = N;
		int ch = myrand(0, 1);
		while(left > K)
		{
			int l = myrand(K+1, Math.min(left, K+K+K));
			for(int i = 0; i < l; i++)
			{
				if(ch == 1)
				{
					M[N-left][0] = myrand(1, maxwt-1);
					M[N-left][1] = myrand(M[N-left][0]+1, maxwt);
				}
				else 
				{
					M[N-left][0] = myrand(2, maxwt);
					M[N-left][1] = myrand(1, M[N-left][0]-1);
				}
				left--;
			}
			ch = 1-ch;
		}
		for(; left > 0; left--)
		{
			M[N-left][0] = myrand(1, maxwt);
			M[N-left][1] = myrand(1, maxwt);
		}
	}
	
	void gen4()
	{
		K = 1;
		N = myrand(2, maxn);
		M = new int[N][2];
		for(int i = 0; i < N; i++)
		{	
			M[i][0] = myrand(1, maxwt);
			M[i][1] = myrand(1, maxwt);
		}
	}
	
	void gen5()
	{
		K = maxk;
		N = maxn;
		M = new int[N][2];
		for(int i = 0; i < N; i++)
		{
			M[i][0] = myrand(1, maxwt);
			M[i][1] = myrand(1, maxwt);
		}
	}
	
	void output()
	{
		System.out.println(N + " " + K);
		System.err.println("Subtask " + subtask + " scheme " + scheme + " (N, K) = " + N + " " + K);
		
		for(int i = 0; i < N; i++)
			System.out.print(String.format("%d%c", M[i][0], (i == N-1 ? '\n' : ' ')));
		for(int i = 0; i < N; i++)
			System.out.print(String.format("%d%c", M[i][1], (i == N-1 ? '\n' : ' ')));
	}
}
