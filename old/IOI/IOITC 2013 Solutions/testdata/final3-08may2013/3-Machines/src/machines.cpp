#include"stdio.h"
#include"vector"
#include"set"
#include"map"
#include"assert.h"
#include"algorithm"
#include"math.h"
#include"stdlib.h"
#include"string.h"
#include"string"
#include"queue"
using namespace std;
typedef unsigned int ui;
typedef long long ll;
typedef unsigned long long ull;
typedef const int& ci;
typedef const unsigned int& cui;
typedef const long long& cll;
typedef const unsigned long long& cull;
#define REP(i,n) for(unsigned int i=0;i<(n);i++)
#define LOOP(i,x,n) for(int i=(x);i<(n);i++)
#define ALL(v) v.begin(),v.end()
#define FOREACH(it,v)   for(typeof((v).begin()) it=(v).begin();it != (v).end();it++)
#define i(x) scanf("%d",&x)
#define u(x) scanf("%u",&x)
#define l(x) scanf("%lld",&x)
#define ul(x) scanf("%llu",&x)
#define N 300001
ll A[N], B[N], bestA[N], bestB[N];
int n, k;
void input() {
	i(n);
	i(k);
	LOOP(i, 1, n+1) {
		l(A[i]);
		A[i] += A[i-1];
	}
	LOOP(i, 1, n+1) {
		l(B[i]);
		B[i]+=B[i-1];
	}
	bestA[0] = 0;
	bestB[0] = 0;
}
void compute() {
	priority_queue<pair<ll,int> > a, b;
	a.push(make_pair(A[0]-bestB[0], 0));
	b.push(make_pair(B[0]-bestA[0], 0));
	for(int i = 1; i <= n; i++) {
		while(a.top().second < i-k) a.pop();
		while(b.top().second < i-k) b.pop();
		bestA[i] = A[i] - a.top().first;
		bestB[i] = B[i] - b.top().first;
		a.push(make_pair(A[i]-bestB[i],i));
		b.push(make_pair(B[i]-bestA[i],i));
	}
	printf("%lld\n", min(bestA[n], bestB[n]));
}
void solve() {
	input();
	compute();
}
int main() {
	ui T = 1;
//	u(T);
	while(T--) {
		solve();
	}
}
