/*
   2 Arrays A[i], B[i]
   For each i, choose A[i] or B[i],
   minimize total
   You can take at most K consecutively

Solution:
	dpA[i] = A[i] + min(i-K <= j <= i - 1)(dpB[j] - A[j]) 
   */

#include<iostream>
#include<cstdio>
#include<queue>
#define MAXN 1123456
using namespace std;
int N, K;
long long A[2][MAXN], dp[2][MAXN];
int main() {
    int i, j;
    scanf("%d %d", &N, &K);
    deque<long long> d[2], di[2];
    for(j = 0; j < 2; j++) {
	for(i = 1; i <= N; i++) {
	    scanf("%lld", A[j]+i); A[j][i] += A[j][i-1];
	}
	// d stores (dpB[j] - A[j]). At i, you only have up to i - K
	d[j].push_back(0); di[j].push_back(0);
    }

    long long curr;
    int k;
    for(i = 1; i <= N; i++) {
	for(j = 0; j < 2; j++) {
	    dp[j][i] = d[1-j].front() + A[j][i];
	}
	for(j = 0; j < 2; j++) {
	    if(di[j].front() == i - K) {
		di[j].pop_front(); d[j].pop_front();
	    }
	}
	// Now add the current one
	for(j = 0; j < 2; j++) {
	    // Remove ones from the top which are no better
	    curr = dp[j][i] - A[1-j][i];
	    while(!d[j].empty() && curr <= d[j].back()) { d[j].pop_back(); di[j].pop_back();}
	    d[j].push_back(curr);
	    di[j].push_back(i);
	}
    }
    printf("%lld\n", min(dp[0][N], dp[1][N]));
    return 0;
}
