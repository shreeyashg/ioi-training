#include"stdio.h"
#include"vector"
#include"set"
#include"map"
#include"assert.h"
#include"algorithm"
#include"math.h"
#include"stdlib.h"
#include"string.h"
#include"string"
using namespace std;
typedef unsigned int ui;
typedef long long ll;
typedef unsigned long long ull;
typedef const int& ci;
typedef const unsigned int& cui;
typedef const long long& cll;
typedef const unsigned long long& cull;
#define REP(i,n) for(unsigned int i=0;i<(n);i++)
#define LOOP(i,x,n) for(int i=(x);i<(n);i++)
#define ALL(v) v.begin(),v.end()
#define FOREACH(it,v)   for(typeof((v).begin()) it=(v).begin();it != (v).end();it++)
#define i(x) scanf("%d",&x)
#define u(x) scanf("%u",&x)
#define l(x) scanf("%lld",&x)
#define ul(x) scanf("%llu",&x)
#define N 300001
int sign(int x) { return x<0 ? -1: (x>0?1:0); }
ll A[N], B[N], bestA[N], bestB[N];
int n, k;
void input() {
	i(n);
	i(k);
	LOOP(i, 1, n+1) {
		l(A[i]);
	}
	LOOP(i, 1, n+1) {
		l(B[i]);
	}	
	bestA[0] = 0;
	bestB[0] = 0;
}
void compute() {
	long long ans = 0;
	int st = 0;
	while(1) {
		while(st<n and A[st]==B[st]) ans += A[st++];
		if(st+k+1>=n) {
			while(st<n) {
				ans += min(A[st], B[st]);
				st++;
			}
			break;
		}
		int sgn = sign(A[st]-B[st]);
		long long mini = 1000000000;
		bool quit = 0;
		for(int i = st; i<st+k+1; i++)
			if(sign(A[i]-B[i]) != sgn) quit = 1;
			else mini = min(mini, (A[i]-B[i])*sign(A[i]-B[i]));
		if(quit) {
			for(; sign(A[st]-B[st]) == sgn; st++)
				ans += min(A[st], B[st]);
			continue;
		}
		for(; mini!=(A[st]-B[st])*sign(A[st]-B[st]); st++)
			ans += min(A[st], B[st]);
		ans += max(A[st], B[st]);
		st++;
	}
	printf("%lld\n", ans);
}
void solve() {
	input();
	compute();
}
int main()
{
	ui T = 1;
//	u(T);
	while(T--) {
		solve();
	}
}
