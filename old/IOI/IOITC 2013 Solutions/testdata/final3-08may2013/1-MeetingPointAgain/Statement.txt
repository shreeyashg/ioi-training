Meeting Point Again

Hyperland is a K-dimensional city. There are N people located at grid points there, and they wish to meet at a particular location. They choose a location such that the sum of their Manhattan distances to that point is minimized. Find the number of candidate points which they can choose to meet at.
Since the answer may be large, output the value modulo 1000000007.

Note: It is possible that multiple people are at the same point.

Input Format:
The first line consists of two integers N and K.
The next N lines consist of K integers  each, describing the coordinates of the point at which the i'th person is at initially.

Output Format:
A single integer, describing the number of points they can choose to meet at. Remember to output your answer modulo 1000000007.

Test Data:
Subtask 1: 10 points
N <= 100, K <= 2
0 <= Each coordinate <= 500

Subtask 2: 30 points
N <= 1000, K <= 3
0 <= Each coordinate <= 5000

Subtask 3: 60 points
N <= 100000, K <= 3
Absolute value of Each Coordinate <= 10^18


Sample Input:
4 2
0 0
3 3
2 1
1 2

Sample Output:
4

Explanation:
(1, 1), (1, 2), (2, 1) and (2, 2) are candidate points, each amounting to a total sum of distance 8.