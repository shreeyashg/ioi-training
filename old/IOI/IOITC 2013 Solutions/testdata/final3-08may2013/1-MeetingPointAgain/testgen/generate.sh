
echo "100 1 500 0" | ./generator >Inputs/sub1_1.in
./a.out <Inputs/sub1_1.in >Outputs/sub1_1.out

echo "100 2 500 0" | ./generator >Inputs/sub1_2.in
./a.out <Inputs/sub1_2.in >Outputs/sub1_2.out

echo "99 1 500 0" | ./generator >Inputs/sub1_3.in
./a.out <Inputs/sub1_3.in >Outputs/sub1_3.out

echo "99 1 500 0" | ./generator >Inputs/sub1_4.in
./a.out <Inputs/sub1_4.in >Outputs/sub1_4.out

echo "98 2 500 0" | ./generator >Inputs/sub1_5.in
./a.out <Inputs/sub1_5.in >Outputs/sub1_5.out

echo "100 2 500 0" | ./generator >Inputs/sub1_6.in
./a.out <Inputs/sub1_6.in >Outputs/sub1_6.out

echo "1000 3 5000 0" | ./generator >Inputs/sub2_1.in
./a.out <Inputs/sub2_1.in >Outputs/sub2_1.out

echo "999 3 5000 0" | ./generator >Inputs/sub2_2.in
./a.out <Inputs/sub2_2.in >Outputs/sub2_2.out

echo "998 3 5000 0" | ./generator >Inputs/sub2_3.in
./a.out <Inputs/sub2_3.in >Outputs/sub2_3.out

echo "996 3 5000 1" | ./generator >Inputs/sub2_4.in
./a.out <Inputs/sub2_4.in >Outputs/sub2_4.out

echo "100000 3 1000000000000000000 1" | ./generator >Inputs/sub3_1.in
./a.out <Inputs/sub3_1.in >Outputs/sub3_1.out

echo "99999 3 999999999999999999 1" | ./generator >Inputs/sub3_2.in
./a.out <Inputs/sub3_2.in >Outputs/sub3_2.out

echo "99998 3 888888888888888888 1" | ./generator >Inputs/sub3_3.in
./a.out <Inputs/sub3_3.in >Outputs/sub3_3.out

echo "99600 3 666666666666666666 1" | ./generator >Inputs/sub3_4.in
./a.out <Inputs/sub3_4.in >Outputs/sub3_4.out


