#include<iostream>
#include<cstdio>
#include<algorithm>

using namespace std;

long long myrand() {
    return rand() * (long long)rand() + rand();
}
int main() {
    long long N, K, range;
    int bad;
    scanf("%lld %lld %lld", &N, &K, &range);
    scanf("%d", &bad);
    printf("%lld %lld\n", N, K);
    int i, j, x;
    if (bad) {
	for(i = 0; i < N; i++) {
	    for(j = 0; j < K; j++) {
		printf("%lld ", i < N/2?myrand() % 10: myrand() % 10 + range - 10);
	    }
	    puts("");
	}
    }
    else {
	for(i = 0; i < N; i++) {
	    for(j = 0; j < K; j++) {
		printf("%lld ", myrand() % (range+1));
	    }
	    puts("");
	}
    }

    return 0;
}
