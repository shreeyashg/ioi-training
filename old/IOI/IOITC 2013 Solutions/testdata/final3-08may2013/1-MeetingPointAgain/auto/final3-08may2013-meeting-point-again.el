(TeX-add-style-hook "final3-08may2013-meeting-point-again"
 (lambda ()
    (TeX-add-symbols
     '("Red" 1)
     '("Code" 1)
     '("Mybox" 1)
     "True"
     "False")
    (TeX-run-style-hooks
     "amssymb"
     "amsfonts"
     "amsmath"
     "latexsym"
     "multicol"
     "xcolor"
     "graphicx"
     "fullpage"
     "latex2e"
     "art11"
     "article"
     "11pt")))

