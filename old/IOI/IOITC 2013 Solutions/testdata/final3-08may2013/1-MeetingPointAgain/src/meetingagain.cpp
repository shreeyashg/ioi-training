#include<iostream>
#include<algorithm>
#include<cstdio>
#define MAXN 100001
#define MAXK 3
using namespace std;

long long X[MAXK][MAXN];
const long long mod = 1000000007;
int main() {
    int N, K;
    scanf("%d %d", &N, &K);
    int i, j;
    for(i = 0; i < N; i++) {
	for(j = 0; j < K; j++) {
	    scanf("%lld", &X[j][i]);
	}
    }
    if(N & 1) {
	printf("1\n");
    }
    else {
	long long ans = 1;
	for(j = 0; j < K; j++) {
	    sort(X[j], X[j]+ N);
	    ans *= (X[j][N/2] - X[j][N/2-1] + 1) % mod;
//	    cout << ans << endl;
	    ans %= mod;
	}
	cout << ans << endl;
    }
    return 0;
}
