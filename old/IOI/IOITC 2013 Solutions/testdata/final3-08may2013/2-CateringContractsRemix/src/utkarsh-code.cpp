#include"stdio.h"
#include"vector"
#include"set"
#include"map"
#include"assert.h"
#include"algorithm"
#include"math.h"
#include"stdlib.h"
#include"string.h"
#include"string"
using namespace std;
typedef unsigned int ui;
typedef long long ll;
typedef unsigned long long ull;
typedef const int& ci;
typedef const unsigned int& cui;
typedef const long long& cll;
typedef const unsigned long long& cull;
#define REP(i,n) for(unsigned int i=0;i<(n);i++)
#define LOOP(i,x,n) for(int i=(x);i<(n);i++)
#define ALL(v) v.begin(),v.end()
#define FOREACH(it,v)   for(typeof((v).begin()) it=(v).begin();it != (v).end();it++)
#define i(x) scanf("%d",&x)
#define u(x) scanf("%u",&x)
#define l(x) scanf("%lld",&x)
#define ul(x) scanf("%l64u",&x)
#define adjust(x) x--;
#define N 100000
#define SIGMA 100
#define INF 1000000000
struct edge {	int v, w; };
int color[N];
int f[N][SIGMA];
int g[N][SIGMA];
vector<edge> G[N];
void read() {
	int n;
	i(n);
	REP(i,n-1) {
		edge e1, e2;
		i(e2.v);
		i(e1.v);
		i(e1.w);
		adjust(e1.v)
		adjust(e2.v)
		e2.w = e1.w;
		G[e1.v].push_back(e2);
		G[e2.v].push_back(e1);
	}
	REP(i,n)
		i(color[i]);
}
void recurse(int u, int p) {
	FOREACH(v, G[u]) {
		if(v->v==p) continue;
	       	recurse(v->v, u);
		REP(i, SIGMA)
			f[u][i] += min(g[v->v][i] + v->w, f[v->v][i]);
	}
	if(color[u] >= 0) {
		REP(i, SIGMA) {
			if(i!=color[u])	f[u][i] = INF, g[u][i]=f[u][color[u]];
			else g[u][i] = INF;
		}
	}
	else {
		int min1 = INF, min1pos = -1, min2 = INF, min2pos = -1;
		REP(i, SIGMA) {
			if(min1>f[u][i]) {
				min2 = min1, min2pos = min1pos;
				min1 = f[u][i], min1pos = i;
			}
			else if (min2>f[u][i]) {
				min2 = f[u][i], min2pos = i;
			}
		}
		REP(i, SIGMA)
			if(i==min1pos) g[u][i] = min2; else g[u][i] = min1;
	}
}
void solve() {
	read();
	recurse(0,-1);
	printf("%d\n", min(f[0][0], g[0][0]));
}
int main() {
	ui T = 1;
//	u(T);
	while(T--) {
		solve();
	}
}
