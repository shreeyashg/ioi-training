#include"stdio.h"
#include"vector"
#include"set"
#include"map"
#include"assert.h"
#include"algorithm"
#include"math.h"
#include"stdlib.h"
#include"string.h"
#include"string"
using namespace std;
typedef unsigned int ui;
typedef long long ll;
typedef unsigned long long ull;
typedef const int& ci;
typedef const unsigned int& cui;
typedef const long long& cll;
typedef const unsigned long long& cull;
#define REP(i,n) for(unsigned int i=0;i<(n);i++)
#define LOOP(i,x,n) for(int i=(x);i<(n);i++)
#define ALL(v) v.begin(),v.end()
#define FOREACH(it,v)   for(typeof((v).begin()) it=(v).begin();it != (v).end();it++)
#define i(x) scanf("%d",&x)
#define u(x) scanf("%u",&x)
#define l(x) scanf("%l64d",&x)
#define ul(x) scanf("%l64u",&x)
#define N 100000
#define M 200000
#define logN 17
#define adjust(x) x--;
struct union_find {
	int count[N], component[N];
	union_find() {
		REP(i,N) count[i] = 1, component[i]=i;
	}
	bool unite(int u, int v) {
		find(u);
	        find(v);
		if(u==v)return false;
		if(count[u] < count[v]) swap(u,v);
		component[v] = u;
		count[u] += count[v];
		return true;
	}
	void find(int& u) const {
		while(component[u]!=u) u = component[u];
	}
};
struct edge {
	int u, v, wt;
	bool operator<(const edge& e) const {
		return (wt<e.wt);
	}
	edge reverse() const {
		edge e;
		e.v = u, e.u = v, e.wt = wt;
		return e;
	}
};
vector<edge> G[N];
int hts[N];
int parent[logN][N];
int wts[logN][N];
edge edges[M];
void dfs(int u, int p, int ht) {
	hts[u] = ht;
	FOREACH(e, G[u])
		if(e->v != p)
			parent[0][e->v] = u, wts[0][e->v] = e->wt, dfs(e->v, u, ht+1);
}
void solve() {
	int n, m;
	i(n), i(m);
	assert((unsigned)n<=N and (unsigned)m<=M);
	REP(i, m) {
		edge& e = edges[i];
		i(e.u), i(e.v), i(e.wt);
		adjust(e.u) adjust(e.v)
	}
	sort(edges, edges+m);
	union_find uf;
	for(int i=0; i<m; i++) {
		if(uf.unite(edges[i].u, edges[i].v))
			G[edges[i].u].push_back(edges[i]),
			G[edges[i].v].push_back(edges[i].reverse());
	}
	dfs(0, -1, 0);
	parent[0][0] = 0;
	wts[0][0] = 0;
	for(int lvl = 1; lvl < logN; lvl++) {
		for(int i = 0;i < n; i++) {
			int lastp = parent[lvl-1][i];
			parent[lvl][i] = parent[lvl-1][lastp];
			wts[lvl][i] = max(wts[lvl-1][i] , wts[lvl-1][lastp]);
		}
	}
	int Q;
	scanf("%d", &Q);
	while(Q--) {
		int u, v;
		i(u), i(v);
		adjust(u) adjust(v)
		if(hts[u]<hts[v]) swap(u,v);
		int worst = 0;
		int l = 0;
		while((1<<l)<=(hts[u]-hts[v])) l++;
		l--;
		while(hts[u]!=hts[v]) {
			assert(l>=0);
			if((1<<l)<=(hts[u]-hts[v]))
				worst = max(worst, wts[l][u]),
				u = parent[l][u];
			l--;
		}
		l = logN -1;
		while(u!=v and l>=0) {
			if(parent[l][u]!=parent[l][v]) {
				worst = max(worst, max(wts[l][u], wts[l][v])),
				u = parent[l][u],
				v = parent[l][v];
			}
			l--;
		}
		assert(u==v or (parent[0][u] == parent[0][v] and ((worst=max(worst, max(wts[0][u], wts[0][v]))) or true)));
		printf("%d\n", worst);
	}
}
int main() {
	ui T=1;
//	u(T);
	while(T--) {
		solve();
	}
}
