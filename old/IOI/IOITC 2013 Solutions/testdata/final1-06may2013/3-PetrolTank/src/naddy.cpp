#include<iostream>
#include<cassert>
#include<cstring>
#include<vector>
#include<algorithm>
#include<cstdio>
using namespace std;

int V, E;
struct edge {
    int u, v, w;
    edge(int _u, int _v, int _w) : u(_u), v(_v), w(_w) {}
};

bool operator< (const edge &a, const edge &b) {
    return a.w < b.w;
}
vector<edge> edges;
#define MAXV 100000
int union_set[MAXV + 5];
int parent[MAXV + 5];
int find_set(int u) {
    return union_set[u] < 0? u:(union_set[u] = find_set(union_set[u]));
}
vector<int> tree[MAXV + 5], tweight[MAXV + 5];
vector<int> jump[MAXV + 5];
vector<int> jweight[MAXV + 5];
int height[MAXV + 5];
void dfs(int u, int p, int wt) {
//    printf("DFS from %d (parent = %d)\n", u, p);
    if(p != -1) {
	height[u] = height[p] + 1;
	parent[u] = p;
	// Do the processing for this node
	// First is jump of length 2^0
	jump[u].push_back(p);
	jweight[u].push_back(wt);
	// Now for each i, you need to jump 2^i
	// Jump 2^i-1 from the 2^i-1
	int halfway;
	for(int i = 1; ; i++) {
	    halfway = jump[u].back();
//	    printf("halfway through 2^%d is %d which is of size %u\n", i, halfway, jump[halfway].size());
	    if(jump[halfway].size() >= i) {
		jweight[u].push_back(max(jweight[halfway][i-1], jweight[u].back()));
		jump[u].push_back( jump[halfway][i-1] );

	    }
	    else break;
	}
	/*
	printf("For %d, the jumps are : ", u);
	for(int i = 0; i < jump[u].size(); i++) printf("%d ", jump[u][i]);
	printf("\n");
	*/
    }
    for(int i = 0; i < tree[u].size(); i++) {
	if(tree[u][i] != p) {
	    dfs(tree[u][i], u, tweight[u][i]);
	}
    }
}
int solve(int u, int v) {
    int ans = -1;
    // If they are not of the same height, make them of the same height
    if(height[u] != height[v]) {
//	printf("heights %d %d\n", height[u], height[v]);
	if(height[u] < height[v]) swap(u, v);
	int diff = height[u] - height[v];
	// Now we have height[u] > height[v]. So jump u
	for(int i = 0; diff; i++) {
	    if(diff & (1 << i)) {
		diff ^= 1 << i;
//		printf("%d\n", i);
		ans = max(ans, jweight[u][i]);
		u = jump[u][i];
	    }
	}
    }
    if(u == v) return ans;
    assert(jump[u].size() == jump[v].size());
    // Now we have u and v at same height
    for(int i = jump[u].size(); i--;) {
	
	if(i < jump[u].size() && jump[u][i] != jump[v][i]) {
	    ans = max( ans, jweight[u][i]);
	    ans = max( ans, jweight[v][i]);
	    u = jump[u][i];
	    v = jump[v][i];
	}
    }
    // Now jump  one more
    ans = max( ans, jweight[u][0]);
    ans = max( ans, jweight[v][0]);
    u = jump[u][0];
    v = jump[v][0];
    assert(u == v);
    
    return ans;
}
int main() {
    scanf("%d %d", &V, &E);
    int i;
    int u, v, w;
    for(i = 0; i < E; i++) {
	scanf("%d %d %d", &u, &v, &w);
	edges.push_back(edge(u,v,w));
    }
    sort(edges.begin(), edges.end());
    memset(union_set, -1, sizeof(union_set));
    //printf("Sorted\n");
    int pu, pv;
    for(i = 0; i < E; i++) {
	u = edges[i].u; v = edges[i].v;
	pu = find_set(u);
	pv = find_set(v);
//	printf(" (%d,%d) -> (%d, %d) \n", u, v, pu, pv);

	if(pv != pu) {
//	    printf("%d %d\n", u, v);
	    union_set[pv] = pu;
	    // Add (u, v, w) to the tree
	    tree[u].push_back(v);
	    w = edges[i].w;
	    tweight[u].push_back(w);
	    tree[v].push_back(u);
	    tweight[v].push_back(w);
	}
    }

    height[1] = 0;
    dfs(1, -1, -1);
    // Got the tree with parent information
    int Q;
    scanf("%d", &Q);
    for(; Q--;) {
	scanf("%d %d", &u, &v);
//	printf("Solving %d %d\n", u, v);
	printf("%d\n", solve(u, v));
    }
    return 0;
}
