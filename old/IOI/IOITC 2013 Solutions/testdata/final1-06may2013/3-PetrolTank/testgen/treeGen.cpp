#include"vector"
#include"cstdio"
#include"string.h"
#include"stdlib.h"
#include"math.h"
#include"assert.h"
using namespace std;
int uf[N],size[N];
bool unite(int u,int v){/*{{{*/
  int t=u,l=v;
  while(uf[t]!=-1)t=uf[t];
  while(uf[l]!=-1)l=uf[l];
  if(l==t)											{
    while(uf[u]>=0)t=uf[u],uf[u]=l,u=t;
    while(uf[v]>=0)t=uf[v],uf[v]=l,v=t;
    return 0;  											}
  if(size[l]<size[t])swap(l,t);
  size[l]+=size[t];uf[t]=l;
  while(uf[u]>=0)t=uf[u],uf[u]=l,u=t;
  while(uf[v]>=0)t=uf[v],uf[v]=l,v=t;
  return 1;
}/*}}}*/
vector<int> adj[N];
void randomMST(vector<int> G[],unsigned int n){/*{{{*/
    unsigned int connected=1;
    for(unsigned int x=0;x<n;x++)uf[x]=-1,size[x]=1,adj[x].clear();
    while(connected!=n){
      int u=rand()%n,v=G[u][rand()%G[u].size()];
      if(unite(u,v))
	connected++,
	adj[u].push_back(v),
	adj[v].push_back(u);
    }
    //spanning tree generated
}/*}}}*/
void uniformST(vector<int> G[],unsigned int n){/*{{{*/
    for(unsigned int x=0;x<n;x++)uf[x]=-1,size[x]=1,adj[x].clear();
	    unsigned int current=0;
	    unsigned int visited=1;
	    while(visited!=n)
	    {
		unsigned int next=G[current][(rand()%G[current].size())];
	        if(unite(next,current))
	        	visited++,
			adj[next].push_back(current),
			adj[current].push_back(next);
        	current=next;
	    }
    //spanning tree generated
}/*}}}*/
int pow(int a,int b){/*{{{*/
	if(b==0)return 1;
	return a*pow(a,b-1);
}/*}}}*/
int randomCylinderMST(int n,int k){/*{{{*/
	int m=n/k;
	n=m*k;
	vector<int> G[n];
	for(int i=0;i<m;i++){
		for(int j=0;j<k;j++){
			int u=i*k+j;
			if(u>=k)
				G[u-k].push_back(u),
				G[u].push_back(u-k);
			for(int l=0;l<k;l++){
				int v=i*k+l;
				if(v!=u)	G[v].push_back(u);
			}
		}
	}
	randomMST(G,n);
	return n;
}/*}}}*/
int randomCylinderST(int n,int k){/*{{{*/
	int m=n/k;
	n=m*k;
	vector<int> G[n];
	for(int i=0;i<m;i++){
		for(int j=0;j<k;j++){
			int u=i*k+j;
			if(u>=k)
				G[u-k].push_back(u),
				G[u].push_back(u-k);
			for(int l=0;l<k;l++){
				int v=i*k+l;
				if(v!=u)	G[v].push_back(u);
			}
		}
	}
	uniformST(G,n);
	return n;
}/*}}}*/
int genLineGraph(int n){/*{{{*/
//	cerr<<"Generate a graph that is line "<<n<<"\n";
	for(int i=0;i<n;i++)
		adj[i].clear();
	for(int i=1;i<n;i++)
		adj[i].push_back(i-1),adj[i-1].push_back(i);
	return n;
}/*}}}*/
/*int buildCombGraph (int n) {
	n=n - (n&1);
	for(int i=0;2*i+1<tc.n;i++,ct++)
		adj[2*i+1].push_back(2*i);
		adj[2*i].push_back(2*i+1);
	for(int i=0;2*i+2<tc.n;i++,ct++)
		adj[2*i+2].push_back(2*i);
		adj[2*i].push_back(2*i+2);
	return n;
}*/
int genCombGraph(int n){/*{{{*/
	n=2*(n/2);
	for(int i=0;i<n;i++)
		adj[i].clear();
	for(int i=0;2*i+1<n;i++)
		adj[2*i].push_back(2*i+1),
		adj[2*i+1].push_back(2*i);
	for(int i=1;2*i<n;i++)
		adj[2*i].push_back(2*i-2),
		adj[2*i-2].push_back(2*i);
	return n;
}/*}}}*/
int genStar(int n,int c){/*{{{*/
	int m=(n-1)/c;
	n=m*c+1;
	vector<int> G[n];
	for(int i=0;i<n;i++)
		G[i].clear();
	for(int i=0;i<m;i++){
		G[i*c].push_back(n-1),G[n-1].push_back(i*c);
		for(int j=0;j<c;j++)
			for(int k=0;k<c;k++)
				if(k!=j)G[i*c+j].push_back(i*c+k);
	}
	uniformST(G,n);
	return n;
}/*}}}*/
int genkarytree(int n,int k,int startindex){/*{{{*/
	assert(n>0);
	if(n<=k)return startindex+1;
	int idx=startindex+1;
	for(int i=0;i<k;i++){
		adj[startindex].push_back(idx);
		adj[idx].push_back(startindex);
		idx=genkarytree((n-1)/k,k,idx);
	}
	return idx;
}/*}}}*/
int genkAryTree(int n,int k){/*{{{*/
	for(int i=0;i<n;i++)adj[i].clear();
	return genkarytree(n,k,0);
}/*}}}*/
