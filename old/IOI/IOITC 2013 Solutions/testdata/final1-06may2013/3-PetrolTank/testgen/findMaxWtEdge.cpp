namespace Oracle {
struct union_find {
	int count[N], component[N];
	union_find() {
		REP(i,N) count[i] = 1, component[i]=i;
	}
	bool unite(int u, int v) {
		find(u);
	        find(v);
		if(u==v)return false;
		if(count[u] < count[v]) swap(u,v);
		component[v] = u;
		count[u] += count[v];
		return true;
	}
	void find(int& u) const {
		while(component[u]!=u) u = component[u];
	}
};
struct edge {
	int u, v, wt;
	bool operator<(const edge& e) const {
		return (wt<e.wt);
	}
	edge reverse() const {
		edge e;
		e.v = u, e.u = v, e.wt = wt;
		return e;
	}
};
vector<edge>* G;
int hts[N] = {-1};
int parent[logN][N];
int wts[logN][N];
edge edges[M];
void dfs(int u, int p, int ht) {
	//cerr<<" dfs:: u="<<u<<" p="<<p<<" ht="<<ht<<"\n";
	hts[u] = ht;
	FOREACH(e, G[u])
		if(e->v != p)
			parent[0][e->v] = u, wts[0][e->v] = e->wt, dfs(e->v, u, ht+1);
}
bool pre = 0;
void preprocess(int n, vector<edge>* g_) {
	G = g_;
	dfs(0, -1, 0);
	parent[0][0] = 0;
	wts[0][0] = 0;
	for(int lvl = 1; lvl < logN; lvl++) {
		for(int i = 0;i < n; i++) {
			int lastp = parent[lvl-1][i];
			parent[lvl][i] = parent[lvl-1][lastp];
			wts[lvl][i] = max(wts[lvl-1][i] , wts[lvl-1][lastp]);
		}
	}
	pre = 1;
}
int dist (int u, int v) {
	assert(pre);
	int ans = 0;
	if(hts[u]<hts[v]) swap(u,v);
	int l = 0;
	while((1<<l)<=(hts[u]-hts[v])) l++;
	l--;
	while(hts[u]!=hts[v]) {
		assert(l>=0);
		if((1<<l)<=(hts[u]-hts[v]))
			ans += (1<<l),
			u = parent[l][u];
		l--;
	}
	l = logN -1;
	while(u!=v and l>=0) {
		if(parent[l][u]!=parent[l][v]) {
			u = parent[l][u],
			v = parent[l][v];
			ans += 2<<l;
		}
		l--;
	}
	if(u!=v)ans+=2;
	assert(u==v or parent[0][u] == parent[0][v]);
	return ans;
}
int query(int u, int v) {
	assert(pre);
	if(hts[u]<hts[v]) swap(u,v);
	int worst = 0;
	int l = 0;
	while((1<<l)<=(hts[u]-hts[v])) l++;
	l--;
	while(hts[u]!=hts[v]) {
		assert(l>=0);
		if((1<<l)<=(hts[u]-hts[v]))
			worst = max(worst, wts[l][u]),
			u = parent[l][u];
		l--;
	}
	l = logN -1;
	while(u!=v and l>=0) {
		if(parent[l][u]!=parent[l][v]) {
			worst = max(worst, max(wts[l][u], wts[l][v])),
			u = parent[l][u],
			v = parent[l][v];
		}
		l--;
	}
	assert(u==v or (parent[0][u] == parent[0][v] and ((worst=max(worst, max(wts[0][u], wts[0][v]))) or not worst)));
	return worst;
}
void solve() {
	int n, m;
	i(n), i(m);
	G = new vector<edge>[n];
	assert((unsigned)n<=N and (unsigned)m<=M);
	REP(i, m) {
		edge& e = edges[i];
		i(e.u), i(e.v), i(e.wt);
	}
	sort(edges, edges+m);
	union_find uf;
	for(int i=0; i<m; i++) {
		if(uf.unite(edges[i].u, edges[i].v))
			G[edges[i].u].push_back(edges[i]),
			G[edges[i].v].push_back(edges[i].reverse());
	}
	preprocess(n, G);
	int q;
	scanf("%d", &q);
	while(q--) {
		int u, v;
		i(u), i(v);
		printf("%d\n", query(u,v));
	}
}
}
