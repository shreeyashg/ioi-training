#include"stdio.h"
#include"vector"
#include"assert.h"
#include"algorithm"
#include"math.h"
#include"stdlib.h"
#include"string"
#include"iostream"
#include"map"
#include"string.h"
using namespace std;
typedef unsigned int ui;
typedef long long ll;
typedef unsigned long long ull;
typedef const int& ci;
typedef const unsigned int& cui;
typedef const long long& cll;
typedef const unsigned long long& cull;
#define REP(i,n) for(unsigned int i=0;i<(n);i++)
#define LOOP(i,x,n) for(int i=(x);i<(n);i++)
#define ALL(v) v.begin(),v.end()
#define FOREACH(it,v)   for(typeof((v).begin()) it=(v).begin();it != (v).end();it++)
#define i(x) scanf("%d",&x)
#define u(x) scanf("%u",&x)
#define l(x) scanf("%Ld",&x)
#define ul(x) scanf("%Lu",&x)
#include"findMaxWtEdge.cpp"
typedef Oracle::edge edge;
#include"findDia.cpp"
#include"treeGen.cpp"
map<pair<int,int>,int> edgelookup;
vector<edge> G;
int nnn_;
vector<edge> edge_adj[N];
int diameter = -1;
void buildG(int n) { // tree of my lking and inconsequential edges
	edgelookup.clear();
	for(int u=0;u<n;u++)
		edge_adj[u].clear();
	for(int u=0;u<n;u++) {
		for(int i=0;i<adj[u].size();i++)
			if(adj[u][i]>u) {
				int v=adj[u][i];
				edgelookup[make_pair(u,v)] = edgelookup[make_pair(v,u)] = G.size();
				edge e;
				e.u=u,e.v=v;
				G.push_back(e);
			}
	}
	for(int i=0;i<n;i++)
		uf[i]=-1,size[i]=1;
	int count[N] = {0};
	for(int i=0;i<n-1;i++) {
		count[G[i].u]++;
		count[G[i].v]++;
		assert((unsigned)G[i].u<n &&(unsigned)G[i].v<n);
		assert(unite(G[i].u,G[i].v));
	}
	assert(G.size() == n-1);
}
struct testcase{ int n;	};
//code::[0=> random, 1=> increasing as you go to root, 2=> increase decrease, 3=>decreasing]
//code2::[0=> random, 1=>small dist, 2=> large dist]
void  outputTestCase(testcase t, int code=0, int code2=0) {
	cerr << " output ::"<<code<<" "<<code2<<"\n";
	for(int i = 0;i<t.n-1; i++) {
		int u = G[i].u, v = G[i].v, wt;
		if(code==0) wt = rand()%MAX_WT+1;
		if(code==1) wt = sqrt((u+v)*(MAX_WT-1.0)*(MAX_WT-1.0)/(2.0*t.n));
		if(code==3) wt = MAX_WT - sqrt((u+v)*(MAX_WT-1.0)*(MAX_WT-1.0)/(2.0*t.n));
		if(code==2) wt = MAX_WT - ((u+v)/2.0 + (t.n*2.0)/(u+v)) * (MAX_WT/(2*t.n+1.0));
		assert(code <=3 and code>=0);
		if(wt <= 1 or wt > MAX_WT)
			cerr<< " Correcting wt between "<< u <<" and "<<v<< " was: "<<wt<<" code: "<<code << " max :: " << MAX_WT <<"\n";
		wt = max(1, min(wt, MAX_WT));
		edge e;
		e.u=u, e.v=v, e.wt=wt;
		edge_adj[u].push_back(e);
		swap(e.u, e.v);
		edge_adj[v].push_back(e);
		G[i].wt = wt;
	}
	int n = t.n;
/*	for(int i = 0; i<n; i++)
		cerr<<i<<" "<<edge_adj[i].size()<<"\n";*/
	if(nnn_!=9)
		diameter = dia::dia(edge_adj, n);
	else diameter = n-1;
/*	for(int i = 0; i<n; i++)
		cerr<<i<<" "<<edge_adj[i].size()<<"\n";*/
	Oracle::preprocess(n, edge_adj);
	cerr <<"diameter :: "<<diameter<<"\n";
	for(int i=N-1;i<M;i++) {
		int u = rand()%n, v = rand()%n;
		if(u==v or edgelookup.count(make_pair(u,v))) {i--; continue;}
		edge e;
		e.u = u; e.v = v;
		e.wt = Oracle::query(u,v)+1;
		if(e.wt == 1) {
			cerr << " Oracle::query("<<u<<","<<v<<") = "<<Oracle::query(u,v)<<"\n";
		}
		edgelookup[make_pair(u,v)] = edgelookup[make_pair(v,u)] = G.size();
		G.push_back(e);
	}
	assert((unsigned)t.n<=N&&t.n>=2);
	cout<<t.n<<" "<<G.size() <<"\n";
	random_shuffle(G.begin(), G.end());
	int labels[t.n];
	for(int i=0;i<t.n;i++)
		labels[i] = i+1;
	random_shuffle(labels, labels+t.n);
	for(int i=0;i<G.size();i++)
		cout<<labels[G[i].u]<<" "<<labels[G[i].v]<<" "<<G[i].wt<<"\n";
	int q = Q;
	cout<<q<<"\n";
	for(int i =0;i<q; i++) {
		int u, v;
		if(code2==0) {
			do {
				u = rand()%t.n;
				v = rand()%t.n;
			}while(u==v);
		}
		if(code2==1) {
			do {
				u = rand()%t.n;
				v = rand()%t.n;
			}while(u==v or Oracle::dist(u,v)>max(diameter>>2, 4));
		}
		if(code2==2) {
			do {
				u = rand()%t.n;
				v = rand()%t.n;
			} while(u==v or Oracle::dist(u,v) < max(1,diameter>>1));
		}
		assert((unsigned)u<n and (unsigned)v<n and u!=v);
		cout<<labels[u]<<" "<<labels[v]<<"\n";
	}
}
bool done=false;
int tholdepth,minsize;
string put;
void gen(int numT) {
	nnn_ = numT;
	testcase tc;
			int c=3;
			int k=rand()%3+2;
	srand(numT);
	switch(numT)
  	{
//cylinder => 2-3 random
		case 0:
			//Cylinder, R a) => random path has been labelled
			c = rand()%3+3;
			tc.n=(rand()&1)?randomCylinderMST(N,c):randomCylinderST(N,c);
			buildG(tc.n);
			outputTestCase(tc, rand()%4, rand()%3);
			break;
		case 1:
			//Cylinder, R c) => short pattern, random characters;
			c = 7+rand()%4;
			tc.n=(rand()&1)?randomCylinderMST(N,c):randomCylinderST(N,c);
			buildG(tc.n);
			outputTestCase(tc, rand()%4, rand()%3);
			break;
		case 2:
			//Cylinder, R a) => random path has been labelled
			c = rand()%3+3;
			tc.n=(rand()&1)?randomCylinderMST(N,c):randomCylinderST(N,c);
			buildG(tc.n);
			outputTestCase(tc, rand()%4, rand()%3);
			break;
		case 3:
			//star 1	
			tc.n=genStar(N,1);
			buildG(tc.n);
			outputTestCase(tc);	
			break;
		case 4:
			//star 2, R c)	
			tc.n=genStar(N,3+rand()%7);
			buildG(tc.n);
			outputTestCase(tc);	
			break;
		case 5:
			//k-ary, (0,x) = s1, otherwise 0
			tc.n=genkAryTree(N,k);
			buildG(tc.n);
			outputTestCase(tc);	
			break;
//star, k-ary => 0
//line, comb => 0,1,2 *  1,2      
		case 6:
		case 7:
		case 8:
		case 9:
		case 10:
		case 11:
			if(rand()&1)
				tc.n=genLineGraph(N);
			else
				tc.n=genCombGraph(N);
			//line, F(a)	
			buildG(tc.n);
			outputTestCase(tc, numT%3, numT%2+1);
			break;
		default:
			assert(0);
	}
}
