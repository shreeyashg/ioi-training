namespace dia {
	vector<edge>* g;
	pair<int,int> diameter(const int& root,const int& parent) {
		int deepest=0,secondDeepest=0,ans=0;
		FOREACH(it,g[root]){
			if(it->v!=parent){
				pair<int,int> p=diameter(it->v,root);
				if(p.first>=deepest){secondDeepest=deepest, deepest=1+p.first;}
				else if(p.first>=secondDeepest)secondDeepest=1+p.first;
				ans=max(ans,p.second);
			}
		}
		return make_pair<int,int>(deepest,max(ans,deepest+secondDeepest));
	}
	int n;
	int dia(vector<edge>* G, int n_){
		g = G;
		n = n_;
		return diameter(rand()%n,-1).second;
	}
}
