(TeX-add-style-hook "final1-06may2013-happy-journey"
 (lambda ()
    (TeX-add-symbols
     '("Code" 1)
     '("Mybox" 1))
    (TeX-run-style-hooks
     "amssymb"
     "amsfonts"
     "amsmath"
     "latexsym"
     "multicol"
     "fullpage"
     "latex2e"
     "art11"
     "article"
     "11pt")))

