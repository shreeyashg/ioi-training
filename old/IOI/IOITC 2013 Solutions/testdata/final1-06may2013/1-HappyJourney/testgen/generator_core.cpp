#include"stdio.h"
#include"iostream"
#include"vector"
#include"cstdlib"
#include"cmath"
#include"algorithm"
#include"cassert"
#include"set"
using namespace std;
#define FOREACH(it, v) for(typeof((v).begin()) it = (v).begin(); it!=(v).end(); it++)
struct edge {
	int u, v;
	char a;
	edge(int u_, int v_, char c_) {
		u = u_;
		v = v_;
		a = c_;
	}
};
vector<edge> G;
set<pair<int,int> > edges;
int uf[N],size[N];
bool unite(int u,int v){
  int t=u,l=v;
  while(uf[t]!=-1)t=uf[t];
  while(uf[l]!=-1)l=uf[l];
  if(l==t)											{
    while(uf[u]>=0)t=uf[u],uf[u]=l,u=t;
    while(uf[v]>=0)t=uf[v],uf[v]=l,v=t;
    return 0;  											}
  if(size[l]<size[t])swap(l,t);
  size[l]+=size[t];uf[t]=l;
  while(uf[u]>=0)t=uf[u],uf[u]=l,u=t;
  while(uf[v]>=0)t=uf[v],uf[v]=l,v=t;
  return 1;
}
int n, s, t;
set<pair<int, int> > removed;
void gena(int l) {
	if(l==0)
		l = min((int)sqrt(N/3), rand()%100+100);
	int m = min((M-2-l)/(2*l), (N-2)/l);
	n = l * m + 2;
	s = 0, t = n-1;
	assert(m>=2);
	for(int i = 1; i<=l;i++) {
		edge e(s, i, 'a'); // top edge
		edge e2((m-1)*l+i ,t, 'a'); // bottom edge
		G.push_back(e);
		G.push_back(e2);
	}
	for(int i = 0; i < m-1; i++) {
		char c = rand()%26 + 'a';
		for(int j =1; j<=l; j++) {
			edge e(i*l+j, i*l+j+l, c);
			if(i>=m-5 and rand() % l <= 3) {
				e.a = max('a', (char)(e.a-rand()%5));
				if(i==m-5 and (rand()%2))
					removed.insert(make_pair((m-1)*l+j,t));
			}
			G.push_back(e);
		}
	}
	for(int i = 1; i < l; i++)
		for(int j = 0; j<m; j++) {
			edge e(i+j*l, i+j*l+1, 'a');
		}
	for(int i =0; i<G.size(); i++) {
		if(removed.count(make_pair(G[i].u, G[i].v)))
			G.erase(G.begin()+i), i--;
	}
}
void dfs_killer_a(int n_) {
	n = n_-(n&1);
	assert(n>2);
	s = 0, t = n-1;
	edge e11(s, 1, 'a');
	edge e12(s, 2, 'a');
	edge e21(n-2, t, 'a');
	edge e22(n-3, t, 'a');
	G.push_back(e11);
	G.push_back(e12);
	G.push_back(e21);
	G.push_back(e22);
	for(int i = 1; 2*i+2<t; i++) {
		char s = rand()%26 + 'a';
		edge e1(2*i, 2*i+1, s);
		edge e2(2*i-1, 2*i+1, s);
		edge e3(2*i-1, 2*i+2, s);
		if(i>(n-10)/2 and i<(n+10)/2 and rand()%5==1)
			e1.a = rand()%26+'a';
		G.push_back(e1);
		G.push_back(e2);
		G.push_back(e3);
	}
}
void dfs_killer_b(int n_, int l = 10) {
	int k = (n_-1)/(l+1);
	n = k*(l+1)+1;
	assert(n>2);
	s = 0, t = n-1;
	for(int i = 0; i<t; i+=1+l) {
		char s = rand()%26 + 'a';
		char s2 = rand()%26 + 'a';
		for(int j = 1; j <= l; j++){
			edge e1(i, i+j, s);
			G.push_back(e1);
			edge e2(i+j, i+l+1, s2);
			if(i>=(n/2-2*l) and i<=(n/2+2*l) and rand()%l == 0)
				e2.a = rand()%26+'a';
			G.push_back(e2);
		}
	}
}
void gen_random(int n_) {
	s = 0, t = n_-1;
	n = n_;
	int m = min(n*5, M);
	for(unsigned int x=0;x<n;x++) uf[x]=-1,size[x]=1;
	for(int i = 1;i < n; i++) {
		int u, v;
		do {
			u = rand()%n, v= rand()%n;
		}while(!unite(u,v));
		edges.insert(make_pair(u,v));
		edges.insert(make_pair(v,u));
		edge e(u, v, 'a'+rand()%2);
		G.push_back(e);
	}
	for(int i = n; i<= m; i++) {
		int u,v;
		do {
			u = rand()%n, v= rand()%n;
		}while(u==v or edges.count(make_pair(u,v)));
		edge e(u, v, 'a'+rand()%2);
		edges.insert(make_pair(v,u));
		edges.insert(make_pair(u,v));
		G.push_back(e);
	}
}
void print() {
	int order[n];
	for(int i=0;i<n;i++)
		order[i]=i+1;
//	random_shuffle(order, order+n);
	assert(s>=0 and s<n and t>=0 and t<n and s!=t and n >=0 and n<=N and G.size() <=M);
	cout << n << " " << G.size() << "\n";
	cout << order[s] << " " << order[t] << "\n";
	FOREACH(e, G) {
		assert(e->u >=0 and e->u <n and e->v>=0 and e->v <n and e->u!=e->v);
		cout << order[e->u] << " " << e->a << " " << order[e->v] << "\n";
	}
}
void gen(int t) {
	srand(t);
	assert(t>=0 and t<=10);
	switch(t) {
		case 0:
			gena(0);
			break;
		case 1:
			gena(10);
			break;
		case 2:
			gena(50);
			break;
		case 3:
			gena(min(N/10,1000));
			break;
		case 4:
		case 5:
			dfs_killer_a(N-rand()%100);
			break;
		case 6:
			dfs_killer_b(N-rand()%100, 13);
			break;
		case 7:
			dfs_killer_b(max(20,N-rand()%100), rand()%5+3);
			break;
		case 8:
		case 9:
		case 10:
			gen_random(2*N/(2+rand()%5));
			break;
		default:
			assert(0);
	}
	print();
}
/*
void gena(int l) {
void dfs_killer_a(int n_) {
void dfs_killer_b(int n_) {
void gen_random(int n_) {
*/
