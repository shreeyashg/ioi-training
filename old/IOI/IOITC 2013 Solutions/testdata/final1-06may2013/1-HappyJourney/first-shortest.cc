// Given a graph and s and t, among all shortest paths s to t, find the lexicographically first.

// Idea: First find distances to t from every vertex. (BFS)
//       Next, starting with s (at distance D say), find the lexicographically
//       first character that leads to something at distance D-1, merging all
//       vertices that share that character.

#include <iostream>
#include <cstdio>
#include <vector>
#include <set>
#include <cassert>
using namespace std;

#define FCO(i, a, b) for(int i = a, _b = b; i < _b; ++i)
#define FOR(i, n) FCO(i, 0, n)
#define FOZ(i, v) FOR(i, signed(v.size()))

int main() {
  int n, m;
  scanf("%d %d", &n, &m);
  int s, t;
  scanf("%d %d", &s, &t);
  s--, t--;
  vector<int> neigh[n];
  vector<char> label[n];	// label[u][i] = label on edge from u to neigh[u][i]
  FOR(i, m) {
    int u, v; char c;
    scanf("%d %c %d", &u, &c, &v);
    u--, v--;
    neigh[u].push_back(v);
    label[u].push_back(c);
    neigh[v].push_back(u);
    label[v].push_back(c);
  }

#define INFINITY (n+1)
  int d[n]; FOR(i,n) d[i] = INFINITY;
  int que[n+1], head = 0, tail = 0;
  d[t] = 0;
  que[tail++] = t;
  while (head < tail) {
    int u = que[head++];
    if (u == s) break;
    FOZ(i, neigh[u]) {
      int v = neigh[u][i];
      if (d[v] == INFINITY) {
	d[v] = d[u] + 1;
	que[tail++] = v;
      }
    }
  }

  // FOR(i, n) cerr << "Vertex " << i << " is at distance " << d[i] << endl;

  string best_path;
  vector<int> frontier;
  frontier.push_back(s);
  while(!frontier.empty()) {
    char best_out_char = 'z' + 1;
    bool done = false;
    FOZ(i, frontier) {
      int u = frontier[i];
      if (u == t) {
	done = true;
	break;
      }
      FOZ(j, neigh[u]) {
	if (d[neigh[u][j]] == d[u] - 1 && label[u][j] < best_out_char) {
	  best_out_char = label[u][j];
	}
      }
    }
    if (best_out_char == 'z' + 1) {
      assert(done);
      break;
    }
    best_path += best_out_char;
    set<int> new_frontier_vertices;
    FOZ(i, frontier) {
      int u = frontier[i];
      FOZ(j, neigh[u]) {
	if (d[neigh[u][j]] == d[u] - 1 && label[u][j] == best_out_char) {
	  new_frontier_vertices.insert(neigh[u][j]);
	}
      }
    }
    frontier.clear();
    frontier = vector<int>(new_frontier_vertices.begin(), new_frontier_vertices.end());
    assert(!frontier.empty());
  }
  printf("%s\n", best_path.c_str());
  return 0;
}
