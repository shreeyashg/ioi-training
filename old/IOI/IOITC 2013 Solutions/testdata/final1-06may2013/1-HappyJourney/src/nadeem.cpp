#include<iostream>
#include<cstring>
#include<cstdio>
#include<vector>
#include<queue>

#define MAXN 200000
using namespace std;
vector<int> g[MAXN+5];
vector<char> gc[MAXN+5];
int vis[MAXN+5];
int V, E;
int dist[MAXN+5];
int main() {
    //printf("Inputting\n");
    scanf("%d %d", &V, &E);
    int i, p, q;
    int source, dest;
    scanf("%d %d", &source, &dest);
    char c;
    for(i = 0; i < E; i++) {
	scanf("%d %c %d", &p, &c, &q);
//	printf("%d %d\n", p, q);
	g[p].push_back(q);
	gc[p].push_back(c);
	g[q].push_back(p);
	gc[q].push_back(c);
    }
 //   printf("%d %d\n", source, dest);

    // First do a bfs from the destination so that we know the shortest distance of each node to the destination
    queue<int> Q;
    memset(dist, -1, sizeof(dist));
    Q.push(dest); dist[dest] = 0;
    int t, v;
 //   printf("starting bfs\n");
    while(!Q.empty())  {
	t = Q.front(); Q.pop();

	for(i = 0; i < g[t].size(); i++) {
	    v = g[t][i];
	    if(dist[v] == -1) {
		dist[v] = dist[t] + 1;
		Q.push(v);
	    }
	}
    }
    /*
    for(i = 0; i < V; i++) {
	printf("%d %d\n", i, dist[i]);
    }
    */
    
    vector<int> layer[MAXN];
    layer[0].push_back(source);
    int curr = 0, k, j;
    for(i = 1; layer[i-1].size(); i++) {
//	printf("Layer : %d \n", i);
	char minchar = 'z' + 1;
	for(j = 0; j < layer[i-1].size(); j++) {
	    t = layer[i-1][j];
//	    printf("Checking out vertex: %d\n Found neighbours :", t);
	    for(k = 0; k < g[t].size(); k++) {
		v = g[t][k];
		if(dist[v] == dist[t] - 1) {
//		    printf("(%d,%c) ", v, gc[t][k]);
		    // We have a valid edge
		    minchar = min(minchar, gc[t][k]);
		}
	    }
//	    puts("");
	}
	for(j = 0; j < layer[i-1].size(); j++) {
	    t = layer[i-1][j];
	    for(k = 0; k < g[t].size(); k++) {
		v = g[t][k];
		if(!vis[v] && dist[v] == dist[t] - 1 && gc[t][k] == minchar) {
		    vis[v] = true;
		    layer[i].push_back(v);
		}
	    }
	}
	if(layer[i].size())
	    printf("%c", minchar);
    }
    puts("");
    return 0;
}
