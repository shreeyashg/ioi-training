import java.io.*;
import java.util.*;

class generator
{
	BufferedReader keyin = new BufferedReader(new InputStreamReader(System.in));
	Random r = new Random();
	int T;
	int MAXN, MAXM;
	
	int myrand(int a, int b)
	{
		int range = b-a+1;
		int x = r.nextInt() % range;
		if(x < 0) x += range;
		return a+x;
	}
	
	public static void main(String[] args) throws IOException
	{
		new generator().exec();
	}
	
	void input() throws IOException
	{
		int [] arr = new int[MAXM * MAXN];
		for(int i = 0; i < arr.length; i++)
			arr[i] = i;
		
		for(int i = 0; i < T; i++)
		{
			int j = i;
			for(int iter = 0; iter < 3; iter++)
				j = myrand(j, arr.length-1);
			
			int t = arr[i];
			arr[i] = arr[j];
			arr[j] = t;
		}
		
		int m, n;
		for(int i = 0; i < T; i++)
		{
			m = arr[i]/MAXN; m++;
			n = arr[i]%MAXN; n++;
			System.out.println(m + " " + n);
		}
	}
	
	void exec() throws IOException
	{
		System.err.println("Enter the number of testcases:");
		T = Integer.parseInt(keyin.readLine());
		System.out.println(T);

		System.err.println("Enter the max values of M and N");
		MAXM = Integer.parseInt(keyin.readLine());

		MAXN = Integer.parseInt(keyin.readLine());
		input();
	}
}
