/*
 	C++ Template
	Pradeep George Mathias
 */


#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <cassert>
#include <string>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <stack>
#include <queue>
#include <cstdlib>
using namespace std;

#define s(T) scanf("%d", &T)
#define sl(T) scanf("%lld", &T)
#define fill(a, val) memset(a, val, sizeof(a))
#define mp make_pair
#define pb push_back
#define all(x) x.begin(), x.end()
#define DREP(x) sort(all(x)); x.erase(unique(all(x)), x.end())
#define INDEX(a, val) (lower_bound(all(a), val) - a.begin())

const int maxm = 5000;
const int maxn = 5000;
const int MOD = 1000000007;

int totalCases, testNum;
int F[maxm+1][maxn+1];
int M, N;

void preprocess()
{
	/* F[m][n] = (summation k=0ton-1 (k+1)*F[m-1][n-k]) + 1
	 * simplifies to
	 * F[m][n] = F[m][n-1] + (summation (nn=1ton) F[m-1][nn])
	 */
	
	for(int n = 1; n <= maxn; n++)
		F[1][n] = 1;
	int g;		//stores summation nn1ton F[m-1][nn]
	for(int m = 2; m <= maxm; m++)
	{
		F[m][1] = m;
		g = m-1;
		for(int n = 2; n <= maxn; n++)
		{
			g = (g + F[m-1][n]) % MOD;
			F[m][n] = (F[m][n-1] + g) % MOD;
		}
	}
}

bool input()
{
	s(M); s(N);
	assert(M >= 1 && M <= maxm && N >= 1 && N <= maxn);
	return true;
}

void solve()
{
	printf("%d\n", F[M][N]);
}

int main()
{
	preprocess();
	s(totalCases); assert (totalCases <= 200000);
	for(testNum = 1; testNum <= totalCases; testNum++)
	{
		if( !input())
			break;
		solve();
	}
}
