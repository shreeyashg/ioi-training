/* 
   Input, N, M
   */
#include<iostream>
#include<cstdio>
#define N 5005
#define M 5005
#define MOD 1000000007
#define red(x) if(x >= MOD) x -= MOD 
using namespace std;

int f[N+1][M+1];
int main() {
    int i, j;
    i = 0;
    for(j = 1; j <= M; j++) {
	f[i][j] = 1;
    }
    i = 1;
    for(j = 1; j <= M; j++) {
	f[i][j] = j;
    }
    long long g;
    for(i = 2; i <= N; i++) {
	f[i][0] = f[i-1][0] * 2;
	red(f[i][0]);
	f[i][0] += -f[i-2][0] + MOD;
	red(f[i][0]);

	for(j = 1; j <= M; j++) {
	    g = f[i-1][j] << 1; 
	    g -= f[i - 2][j]; 
	    g += f[i][j - 1] + MOD;
	    g %= MOD;
	    f[i][j] = g;
	}
    }
    int n, m, q;
    for(scanf("%d", &q);q--; printf("%d\n", f[n][m])) scanf("%d%d", &m, &n);
    return 0;
}
