In the King's court, there is a bitter rivalry between the Mathematician and the Jester.
The King gives both of them an array A, and then the Mathematician begins asking the jester: "What is the maximum value of the elements of the array between position i and j?" The jester, being lazy, doesn't compute the maximum of all the elements, instead he just computes the maximum between the value A[i] and A[j] and tells it to the Mathematician.
The King supports the Jester, since his wit is what keeps the court destressed and calm. Hence, he wishes to give an array A such that no matter what values of i and j the Mathematician chooses, the Jester gives the correct answer. How many possible arrays are there of this sort, where the length of the array is N and each element of the array is between 1 and M.

Since the answer may be large, output the value modulo 1000000007.

Input:
The first line consists of the number of queries, Q.
The next Q lines each contain two integers: M and N.

Output:
For each test-case, output the answer on a single line.

Constraints:
SubTask 1
Q <= 2*10^5
1 <= N, M <= 20

SubTask 2
Q <= 2*10^5
1 <= N, M <= 200

SubTask 3
Q <= 2*10^5
1 <= N, M <= 5000

Sample Input:
3
2 2
3 3
1 5

Sample Output:
4
22
1
