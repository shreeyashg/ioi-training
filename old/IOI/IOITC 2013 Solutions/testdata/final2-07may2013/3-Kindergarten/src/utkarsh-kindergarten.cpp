#include"map"
#include"vector"
#include"stdio.h"
#include"cassert"
#include"algorithm"
using namespace std;
#define MOD 1000000007
typedef int bigint;
char max_components = 0;
#define NN 30
int N;
//#define DEBUG 0
map<vector<char>, bigint > m[NN];
void init(){
	for(char i=0;i<N;i++)m[i].clear();
}
char get_pos(vector<char>& v, char l, char begin=0) {
	while(begin<v.size() && v[begin]<l)begin++;
	return begin;
}
bigint solve(vector<char>& v, char sum);
bigint put(vector<char> v, char i, char j, char sum) {
#ifdef DEBUG
//	printf("put :: v[i]=%d, i=%d, j=%d\n", v[i], i, j);
#endif
	char other = v[i] - j;
	if(v[i]==0){
		v.erase(v.begin()+i);
		return solve(v, sum);
	}
	if(j<other) swap(other, j);
	v[i] = j;
	while(i>0 && v[i]<v[i-1])
		swap(v[i], v[i-1]), i--;
	if(other==0) {
		return solve(v, sum);
	}
	char l = get_pos(v, other);
	v.insert(v.begin()+l, other);
	return solve(v, sum);
}
int num_call = 0;
bigint solve(vector<char>& v,char sum) {
#ifdef DEBUG
	char ss = 0;
	for(int i=0; i<v.size(); i++)assert(v[i]), ss += v[i];
	for(int i=1; i<v.size(); i++)assert(v[i-1]<=v[i]);
	int saved_num = num_call++;
//	printf("%d | needed ans for (", saved_num);
//	for(int i=0;i<v.size();i++)printf("%d,", v[i]);
//	printf("):: %d\n", sum);
	assert(ss==sum);
#endif
	{
		bigint ans = -1;
		char components = v.size();
		if(components > max_components) ans = 0;
		else if(sum==0)ans = 1;
		else if(m[sum].count(v)) ans = m[sum][v];
		if(ans>=0) {
#ifdef DEBUG
//			printf("%d | returning ans %d\n", saved_num, ans);
#endif
			return ans;
		}
	}
	char n=v.size();
	sum--;
	bigint ans=0;
	for(char i=0; i<n; i++) {
		assert(v[i]!=0);
		v[i]--;
		char new_val = v[i];
		for(char j=0;j<=new_val-j; j++) {
			bigint inc = put(v, i, j, sum);
			assert(inc < MOD);
			if(j!=new_val-j) inc <<= 1;
			ans = (ans + (long long)inc) % MOD;
		}
		v[i]++;
	}
#ifdef DEBUG
//	printf("%d | returning ans %d\n",saved_num, ans);
#endif
	m[sum+1][v] = ans;
	return ans;
}
#include"iostream"
int main() {
	int T=1;
//	scanf("%d", &T);
	while(T--) {
		int max_comp;
		scanf("%d%d", &N, &max_comp);
		max_components = max_comp;
		assert(N<=NN and N>0 and max_comp > 0);
		init();
		vector<char> v;
		v.push_back(N-1);
		bigint ans = (solve(v, N-1) * (long long)N) % MOD;
		printf("%d\n", ans);
	}
}
