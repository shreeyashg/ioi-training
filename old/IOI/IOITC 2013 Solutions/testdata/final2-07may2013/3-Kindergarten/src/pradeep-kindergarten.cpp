/*
 	C++ Template
	Pradeep George Mathias
 */


#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <cassert>
#include <string>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <stack>
#include <queue>
#include <cstdlib>
using namespace std;

#define s(T) scanf("%d", &T)
#define sl(T) scanf("%lld", &T)
#define fill(a, val) memset(a, val, sizeof(a))
#define mp make_pair
#define pb push_back
#define all(x) x.begin(), x.end()
#define DREP(x) sort(all(x)); x.erase(unique(all(x)), x.end())
#define INDEX(a, val) (lower_bound(all(a), val) - a.begin())

int totalCases, testNum;
int N, K;

long long F[1<<20];
//map <int, long long> F;

void preprocess()
{
	fill(F, -1);
}

long long f(int bm)
{
	if(bm == (1<<(N-1))-1)
		return 1;
	
	//if(F.count(bm) > 0)
	if(F[bm] >= 0)
		return F[bm];
	
	int prev = 1;
	int comps = 0;
	for(int i = 0; i < (N-1); i++)
	{
		int cur = (bm >> i)&1;
		if(prev == 1 && cur == 0)
			comps++;
		prev = cur;
	}
	
	if(comps > K)
		return F[bm] = 0;
	
	long long ret = 0;
	for(int i = 0; i < N-1; i++)
		if((bm & (1<<i)) == 0)
			ret += f(bm | (1<<i));
	return F[bm] = ret % 1000000007;
}

bool input()
{
	s(N); //assert(N <= 18);
	s(K);
	return true;
}

int fac(int n)
{
	int prod=1;
	for(int i = n; i >= 1; i--)
		prod = (prod * 1ll * i) % 1000000007;
	return prod;
}

void solve()
{
	if(2 * K >= N)
		printf("%d\n", fac(N));
	else 
		printf("%lld\n", (N * f(0)) % 1000000007);
}

int main()
{
	preprocess();
	input();
	solve();
}
