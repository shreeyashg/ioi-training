//You are free to include standard headers

bool query(const bool *); //Do not change this line


//You are free to have variables and functions at global scope


//For example,
const int max_N = 10000;
bool q[max_N];


 //Do not change the following function signature, though you may change the names of the variables if you wish
void solve(int N, bool *ans)
{
	//Your code here
	
	
	q[0] = true;
	q[1] = false;
	
	
	//Make queries:
	bool a = query(q);
	
	//When you're done, write to ans: ans[i] should be true iff variable i is included
	for(int i=0; i<N; ++i)
		ans[i] = true;
}
