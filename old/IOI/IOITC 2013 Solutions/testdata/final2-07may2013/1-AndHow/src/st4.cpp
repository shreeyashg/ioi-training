#include"cmath"
//You are free to include standard headers
void work( int u, int v);
extern bool query(const bool *); //Do not change this line
#define FILL(u,v,x) for(int tttt=u;tttt<v;tttt++) vals[tttt]=x;
//You are free to have variables and functions at global scope
#define NN 25000
#define K 25
bool vals[NN] = {0};
bool *my_ans;
int n;
int left = 0;
//Do not change the following function signature, though you may change the names of the variables if you wish
void solve(int N, bool* ANS)
{
	n = N;
       	my_ans = ANS;
	for(int i=0; i<N; i++)
		my_ans[i] = 0;
/*	int segments = sqrt(N) + 0.0001;
	for(int i = 0; i < segments; i++) {
		work((n*i)/segments, (n*(i+1))/segments);
	}*/
	work(0, N);
}
void work( int u, int v) {
	if(left >= K) return;
	FILL(0,n,1);
	FILL(u,v,0);
	if(query(vals))return;
	FILL(u,v,1);
	int lo = u, hi = v;
	while(lo<hi-1) {
		int mid = (lo+hi)>>1;
		FILL(lo, mid, 0);
		bool q = !query(vals);
		FILL(lo, mid, 1);
		if(q) hi = mid;
		else lo = mid;
	}
	my_ans[lo] = 1;
	work(hi, v);
}
