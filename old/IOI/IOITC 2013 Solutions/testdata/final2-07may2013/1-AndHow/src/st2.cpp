#include"cmath"
//You are free to include standard headers
extern bool query(const bool *); //Do not change this line

void work ( int u, int v);
//You are free to have variables and functions at global scope
#define NN 25000
#define K 25
bool vals[NN] = {0};
bool *my_ans;
int n;
int left = 0;
//Do not change the following function signature, though you may change the names of the variables if you wish
void solve(int N, bool* ANS)
{
	n = N;
       	my_ans = ANS;
	int segments = sqrt(N) + 0.0001;
	for(int i = 0; i < segments; i++) {
		work((n*i)/segments, (n*(i+1))/segments);
	}
}
void work ( int u, int v) {
	if(left >= K) return;
	for(int i = 0; i < n; i++) vals[i] = 1;
	for(int x = u; x < v; x++) vals[x] = 0;
	if(query(vals))return;
	for(int x = u; x < v; x++) vals[x] = 1;
	for(;u<v and left<K; u++) {
		vals[u] = 0;
		if(!query(vals))
			my_ans[u] = 1, left++;
		else my_ans[u] = 0;
		vals[u] = 1;
	}
}
