//You are free to include standard headers
extern bool query(const bool *); //Do not change this line

//You are free to have variables and functions at global scope
#define NN 25000
#define K 25
bool vals[NN] = {0};
//Do not change the following function signature, though you may change the names of the variables if you wish
void solve(int N, bool* ans)
{
	for(int i = 0; i < N; i++)
		vals[i] = 1;
	//Your code here
	int left = 0;
	for(int i = 0; i < N and left < K; i++) {
		vals[i] = 0;
		if(!query(vals)) {	ans[i] = 1, left++; /*printf("%d\n",i);*/}
		else ans[i] = 0;
		vals[i] = 1;
	}
}
