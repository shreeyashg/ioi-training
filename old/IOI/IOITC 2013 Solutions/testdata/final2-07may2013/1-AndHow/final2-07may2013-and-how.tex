\documentclass[11pt]{article}
\usepackage{fullpage}
%\addtolength{\textheight}{2cm}
%\addtolength{\topmargin}{-1cm}
\usepackage{multicol}
\usepackage{latexsym}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
%\pagestyle{empty}
\newcommand{\Mybox}[1]{\parbox{1.5cm}{\centerline{#1}}}
%\setlength{\parindent}{0mm}
%\setlength{\parskip}{3mm}
\newcommand{\Code}[1]{\texttt{#1}}
\usepackage{xcolor}
\newcommand{\True}{\Code{tt}}
\newcommand{\False}{\Code{ff}}

\begin{document}
\begin{center}
\textbf{\large IOI Training Camp 2013 -- Final 2}\\[.3cm]
\end{center}

\setcounter{section}{0}
% \renewcommand{\thesection}{Problem \arabic{section}}
\section{And how!}

A boolean variable $x$ can take two values, \emph{True} and
\emph{False}, which we write as \True\ and \False, respectively.  The
function \emph{and}, written $\land$, takes two boolean variables $x$
and $y$ as input and evaluates to \True\ if both inputs are \True\ and
evaluates to \False\ otherwise.  This is summarized by the following
\emph{truth table} that describes the behaviour of $x \land y$ for all
possible combinations of $x$ and $y$.

$$
  \begin{array}{|c|c|c|}\hline
    x & y  & x \land y \\\hline
    \False & \False & \False \\
    \False & \True & \False \\
    \True & \False & \False \\
    \True & \True & \True \\\hline
  \end{array}
$$

\noindent
It is easy to see $(x \land y) \land z = x \land (y \land z) = x \land
y \land z$ evaluates to \True\ if and only if all of $x$, $y$ and $z$
are \True.  In general, $x_0 \land x_1 \land \cdots \land x_n$
evaluates to \True\ if and only if all of $x_0,x_1,\ldots,x_n$ are
\True.

You are given a hidden function $f(x_0,x_1,x_2,\ldots,x_{N{-1}})$ that
computes the \emph{and} of some subset of $N$ boolean variables $x_0,
x_1,\ldots,x_{N{-1}}$.  In other words, $f(x_0,x_1,\ldots,x_{N{-1}}) =
x_{j_1} \land x_{j_2} \land \cdots \land x_{j_K}$ for some $0 \leq j_1
< j_2 < \cdots < j_K \leq N{-1}$.  Your goal is to identify the set of
variables $x_{j_1}, x_{j_2}, \ldots, x_{j_K}$ that define $f$.

For this, you have access to a function \emph{query()}.  To use this
function, you fix the value of each $x_i$ as \True\ or \False.  Let
$v_i$ denote the value assigned to $x_i$.  The function
$\emph{query}(v_0,v_1,\ldots,v_{N{-1}})$ then reports whether
$f(x_0,x_1,\ldots,x_{N{-1}})$ evaluates to \True\ or \False\ for the given
assignment of values to the variables.

For example, suppose $N = 4$ and $f(x_0,x_1,x_2,x_3) = x_1 \land x_3$.
Here are some possible calls to \emph{query} with the corresponding
answers.

\begin{itemize}
\addtolength{\itemsep}{-1ex}
\item $\emph{query}(\True,\False,\True,\True)$ = \False, because $v_1
  \neq \True$.
\item $\emph{query}(\True,\False,\True,\False)$ = \False, because $v_1
  \neq \True$ and $v_3 \neq \True$.
\item $\emph{query}(\True,\True,\False,\True)$ = \True, because $v_1
  = v_3 = \True$.
\item $\emph{query}(\False,\True,\False,\True)$ = \True, because $v_1
  = v_3 = \True$.
\end{itemize}


\subsubsection*{Your task}

You have to write a function 

\Code{void solve(int N, bool *ans)} 

\noindent 
that calls a function 

\Code{bool query(const bool *q)} 

\noindent
multiple times to compute the exact subset of variables that define
the hidden function $f$.

\begin{itemize}

\item Your solution is returned by \Code{solve()} through the array
  \Code{ans[0..N-1]}, where \Code{ans[i]} should be \Code{true} if
  your computation reports variable $x_i$ to be included in the
  definition of $f$, and \Code{false} otherwise.

\item You call \Code{query} by passing a boolean array
  \Code{q[0..N-1]}, where \Code{q[0]}, \Code{q[1]}, \ldots,
  \Code{q[N-1]} are the values you choose for $x_0$, $x_1$, \ldots,
  $x_{N{-1}}$.  The call to \Code{query} returns the result of
  evaluating $f$ with these values.

\item Note that \Code{ans} and \Code{q} are declared as global arrays
  in the code provided to you (see below).
\end{itemize}

\noindent
You will be provided two files.

\begin{itemize}

\item \Code{grader.cpp}:  You should not edit this file.  This files
  reads a description of $f$ from the input, calls
  \Code{solve()} once and compares the array \Code{ans} returned by
  \Code{solve()} with $f$.

  \Code{grader.cpp} also implements the function \Code{query()} that
  reports the value of $f$ for a given choice of inputs.

\item \verb|dummy_solution.cpp|: This is the file in which you write
  your code for \Code{solve()}.  There are some function headers,
  etc., that you should not modify or delete.  The place where you
  have to insert your code is clearly marked.

\end{itemize}

\subsubsection*{Compiling and testing your code}

To compile your code use the following command.

\medskip

\verb|g++ grader.cpp dummy_solution.cpp -o grader|

\medskip

\noindent
To provide test inputs to the code, see the section \textbf{Input
  format} below.

\subsubsection*{Submissions}

You should submit your modified version of \verb|dummy_solution.cpp|
to the online judge, which will compile it with
\Code{grader.cpp}\footnote{%
  Note: The exact implementation of \Code{grader.cpp} in the online
  judge may vary from the code given to you, but the two will be
  functionally equivalent.}  
and evaluate your implementation of \Code{solve()}. You get full marks
if your code is able to exactly identify the correct subset
$x_{i_1},x_{i_2},\ldots,x_{i_K}$ for each candidate function $f$
in less than or equal to \textcolor{red}{300 queries}.

\subsubsection*{Input format}

All input is done by \Code{grader.cpp} so you do not need to write any
code for input.  However, to test your program, you can supply
\Code{grader.cpp} with the definition of a function $f$ in the
following format.

\begin{itemize}
\addtolength{\itemsep}{-1ex}

\item The first contains two integers $N$ and $K$, the total number of
  variables and the number of variables used in the definition of $f$,
  respectively. 

\item The second line contains $K$ distinct integers in the range
  $0..N{-1}$, corresponding to the $K$ variables from
  $x_0,x_1,\ldots,x_{N{-1}}$ that define $f$.

\end{itemize}


\subsubsection*{Output format}

All output is done by \Code{grader.cpp} so you do not need to write any
code for output.  Your implementation of \Code{solve()} has to compute
and return the array \Code{ans[0..N-1]} as described above.

\subsubsection*{Test Data}

\begin{itemize}
\addtolength{\itemsep}{-1ex}
\item Subtask 1 (10 marks) : $N = 300$, $1 \leq K \leq 16$
\item Subtask 2 (15 marks) : $N = 900$, $1 \leq K \leq 9$
\item Subtask 3 (20 marks) : $N \le 10^4$, $1 \leq K \leq 10$
\item Subtask 4 (30 marks) : $N \le 10^4$, $1 \leq K \leq 20$
\item Subtask 5 (25 marks) : $N \le 25,000$, $1 \leq K \leq 25$

\end{itemize}

\bigskip

\noindent
\begin{minipage}[t]{0.3\textwidth}
\subsubsection*{Sample Input}
$f(x_0,x_1,x_2,x_3) = x_1 \land x_3$
\begin{verbatim}
4 2
1 3
\end{verbatim}
\subsubsection*{Sample Solution}
\begin{verbatim}
ans[0] = false
ans[1] = true
ans[2] = false
ans[3] = true
\end{verbatim}
\end{minipage}
\hfill
\begin{minipage}[t]{0.6\textwidth}
\subsubsection*{Sample Interaction}
\begin{verbatim}
q[0] = true; q[1] = false; q[2] = true; q[3] = true;
query(q) returns false

q[0] = true; q[1] = false; q[2] = true; q[3] = false;
query(q) returns false

q[0] = true; q[1] = true; q[2] = false; q[3] = true;
query(q) returns true

q[0] = false; q[1] = true; q[2] = false; q[3] = true;
query(q) returns true
\end{verbatim}
\end{minipage}
\hfill
\begin{minipage}[t]{0.3\textwidth}
\end{minipage}

\subsubsection*{Limits}

\begin{itemize}
\addtolength{\itemsep}{-1ex}
\item \emph{Time limit:} 3 s
\item \emph{Memory limit:} 64 MB
\end{itemize}

\end{document}

