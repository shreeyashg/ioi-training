#include <cstdio>
#include"dummy_solution.cpp"
using namespace std;
#define max_N 100000
#define max_K 1000
void solve(int N, bool* ans);

bool ans[max_N];

class Input {
	int Q;
	bool included[max_N];
	public:
	int status;
	int N;
	Input(bool nothing = 0) {
		Q = 300;
		if(nothing)return;
		int K;
		scanf("%d %d", &N, &K);
		if(K <= 0 or K > N or N <= 0 or N > max_N or K > max_K) {
			printf("Invalid input\n");
			status = 1;
			return;
		}
		for(int i=0;i<N; i++)
			included[i] = 0;
		for(int i=0; i<K; ++i) {
			int x;
			scanf("%d", &x);
			if(x < 0 || x >= N or included[x])
			{
				printf("Invalid input : %d is not in the range [0, %d).\n", x, N);
				status = 1;
				return;
			}
			included[x] = true; //Indices are 0-based!
		}
		status=0;
	}
	public:
	bool query(const bool* q) {
		if( --Q < 0)
		{
			fprintf(stderr, "Too many queries.\n");
			printf("NO");
		}
		bool res = true;
		for(int i=0; i<N; ++i)
			if(included[i])
				res = res && q[i];
		return res;
	}
	bool check(bool* ans) {
		for(int i = 0; i<N; ++i)
			if(ans[i] != included[i]) {
				fprintf(stderr, "Answer is wrong for variable %d.\n", i);
			return 0;
		}
		return 1;
	}
};
namespace {
	Input* I1;
	Input* I;
	Input* I2;
}

int main()
{
	I = new Input();
	I1 = new Input(1);
	I2 = new Input(1);
	if(I->status)return 0;
	solve(I->N, ans);
	if(I->check(ans))
		printf("YES\n");
	else 
		printf("NO\n");
}

bool query(const bool *q)
{
	return I->query(q);
}
/*void solve(int N, bool *ans) {
included[0]=0;
}*/
