(TeX-add-style-hook "final2-07may2013-and-library"
 (lambda ()
    (TeX-add-symbols
     '("Code" 1)
     '("Mybox" 1)
     "True"
     "False")
    (TeX-run-style-hooks
     "xcolor"
     "amssymb"
     "amsfonts"
     "amsmath"
     "latexsym"
     "multicol"
     "fullpage"
     "latex2e"
     "art11"
     "article"
     "11pt")))

