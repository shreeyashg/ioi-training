//You are free to include standard headers here

bool query(const bool *); //Do not change this line

const int max_N = 25000;  //Do not change this line
bool q[max_N];            //Do not change this line

//You are free to add variables and functions at global scope here



//Do not change the following function signature

void solve(int N, bool *ans)
{
        //Your code here.  For example
        //  Set up q
        //    q[0] = true; q[1] = false;
        //  Make a query
        //    bool a = query(q);
        //  When you're done, write to ans: 
        //    ans[i] should be true iff variable i is included in f
        //    for(int i=0; i<N; ++i)
        //      ans[i] = true;
}
