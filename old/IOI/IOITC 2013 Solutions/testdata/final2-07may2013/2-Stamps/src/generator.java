import java.io.*;
import java.util.*;

class generator
{
	BufferedReader keyin = new BufferedReader(new InputStreamReader(System.in));
	Random r = new Random();
	
	int K;
	int N;
	int alph;
	
	char [] S;
	String [] T;
	int [] M;
	
	int maxlen;
	int sumlen;
	
	int impossible;
	int maxtst;
	
	int myrand(int a, int b)
	{
		int range = b-a+1;
		int x = r.nextInt() % range;
		if(x < 0) x += range;
		return a+x;
	}
	
	public static void main(String[] args) throws IOException
	{
		new generator().exec();
	}
	
	void exec() throws IOException
	{
		//System.err.println("Enter the alphabet size:");
		alph = Integer.parseInt(keyin.readLine());
		
		input();
		if(maxtst == 0)
			generate();
		else 
			generate1();
		output();
	}
	
	void generate() throws IOException
	{
		M = new int[K];
		T = new String[K];
		for(int i = 0; i < K; i++)
		{
			M[i] = myrand(1, Math.min(maxlen, sumlen - (K-i-1)));
			sumlen -= M[i];
		}
		
		for(int i = 0; i < K; i++)
		{
			int [] prob = new int[alph];
			int tot = 0;
			for(int j = 0; j < alph; j++)
			{	
				prob[j] = myrand(1, 100);
				tot += prob[j];
			}
			
			String cur = "";
			for(int j = 0; j < M[i]/2; j++)
			{
				int tmp = myrand(0, tot-1);
				for(int k = 0; k < alph; k++)
					if(tmp < prob[k])
					{
						cur = cur + (char)(k + 97);
						break;
					}
					else 
						tmp -= prob[k];
			}
			
			for(int j = 0; j < alph; j++)
				prob[j] = tot - prob[j];
			tot *= alph-1;
			for(int j = M[i]/2; j < M[i]; j++)
			{
				int tmp = myrand(0, tot-1);
				for(int k = 0; k < alph; k++)
					if(tmp < prob[k])
					{
						cur = cur + (char)(k + 97);
						break;
					}
					else 
						tmp -= prob[k];
			}
			T[i] = cur;
		}
		
		int maxmin = myrand(0, 1);
		S = new char[N];
		for(int rem = N; rem > 0; )
		{
			int k = myrand(0, K-1);
			int len = myrand(1, Math.min(M[k], rem));
			for(int i = 0; i < len; i++)
			{
				int pos = 0;
				if(maxmin == 0)
					pos = M[k]-1;
				
				for(int iter = 0; iter < 3; iter++)
					if(maxmin == 1)
						pos = myrand(pos, M[k]-1);
					else 
						pos = myrand(0, pos);
				S[N-rem] = T[k].charAt(pos);
				rem--;
			}
		}
		
		if(impossible == 1)
			S[myrand(0, N-1)] = (char)(97 + alph);
	}
	
	void generate1() throws IOException
	{
		int [] forb = new int[K];
		for(int i = 0; i< K;i++)
			forb[i] = myrand(alph, 25);
		
		M = new int[K];
		T = new String[K];
		for(int i = 0; i < K; i++)
			M[i] = Math.min(maxlen, sumlen/K);
		
		for(int i = 0; i < K; i++)
		{
			T[i] = "";
			for(int j = 0; j < M[i]-1; j++)
			{
				T[i] += (char)(97 + myrand(0, alph));
			}
			T[i] += (char)(97 + forb[i]);
		}
		
		S = new char[N];
		for(int i = 0; i < N; i++)
			S[i] = (char)(97 + forb[myrand(0, K-1)]);
	}
	
	void input() throws IOException
	{
		//System.err.println("Enter the value of K:");
		K = Integer.parseInt(keyin.readLine());
		
		//System.err.println("Enter the value of N:");
		N = Integer.parseInt(keyin.readLine());
		
		//System.err.println("Enter the maximum length of a particular stamp:");
		maxlen = Integer.parseInt(keyin.readLine());
		
		//System.err.println("Enter the bound on the maximum sum of lengths:");
		sumlen = Integer.parseInt(keyin.readLine());
		
		//System.err.println("Enter whether it is impossible to finish or not:");
		impossible = Integer.parseInt(keyin.readLine());
		
		//System.err.println("Enter whether you wish it to be a maxtest or not:");
		maxtst = Integer.parseInt(keyin.readLine());
	}
	
	void output()
	{
		System.out.println(K);

		System.out.print(N + " ");
		for(int i = 0; i < N; i++)
			System.out.print(S[i]);
		System.out.println();
		
		for(int i = 0; i < K; i++)
		{
			System.out.println(M[i] + " " + T[i]);
		}
	}
}
