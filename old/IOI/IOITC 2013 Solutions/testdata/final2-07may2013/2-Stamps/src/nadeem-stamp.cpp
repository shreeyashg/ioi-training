/*
Problem: Given a string s1 and a list of strings s2, 
	 find the minimum number of strings you need to use from s2 such that 
	 s1 is a subsequence of that string
   */
#include<cstdio>
#include<algorithm>
#include<iostream>
#include<vector>
using namespace std;

#define MAX1 100005
#define MAXL 11
#define MAX2 10005
char s1[MAX1], s2[MAXL][MAX2];
vector<int> v[MAXL][26];
int main() {
    int s1size;
    int l, i, j;
    scanf("%d", &l);
    scanf("%d", &s1size);
    scanf("%s", s1);
    int s2size;
    for(i = 0; i < l; i++) {
	scanf("%d %s",&s2size,  s2[i]);
	for(j = 0; s2[i][j]; j++) {
	    v[i][s2[i][j]-'a'].push_back(j);
	}
    }
    int start = 0;
    int farthest; // Stores how far we reached in each string
    int curr; // Stores where we are in the original string
    int pos; // Stores where we are in the s2 string
    char c; // Character in question now
    vector<int> :: iterator it;
    int farthesti;
    int answer = 0;
    while(start < s1size) {
//	printf("Start = %d\n", start);
	answer ++;
	farthest = start;
	for(i = 0; i < l; i++) {
	    // See how far we can reach from start in this string
	    curr = start;
	    pos = 0;
//	    printf("Curr = %d\n", curr);
	    while(curr < s1size) {
//	    printf("curr = %d\n", curr);
		c = s1[curr] - 'a';
		it = lower_bound(v[i][c].begin(), v[i][c].end(), pos);
		if(it == v[i][c].end()) {
//		    printf("Stopping search in %s\n", s2[i]);
		    break;
		}
		else {
		    pos = *it + 1;
//		    printf("Found %c at %d\n", c + 'a', *it);
		}
		curr++;
	    }
	    if(curr > farthest) {
		farthest = curr;
		farthesti = i;
	    }
	}
	if(farthest == start) {
	    break;
	}
	start = farthest;
    }
    if(start < s1size) {
	printf("-1\n");
    }
    else {
	printf("%d\n", answer);
    }


    return 0;
}

