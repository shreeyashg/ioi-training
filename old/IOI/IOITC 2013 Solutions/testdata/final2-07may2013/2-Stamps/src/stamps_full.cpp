/*
 	C++ Template
	Pradeep George Mathias
 */


#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <cassert>
#include <string>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <stack>
#include <queue>
#include <cstdlib>
using namespace std;

#define s(T) scanf("%d", &T)
#define sl(T) scanf("%lld", &T)
#define fill(a, val) memset(a, val, sizeof(a))
#define mp make_pair
#define pb push_back
#define all(x) x.begin(), x.end()
#define DREP(x) sort(all(x)); x.erase(unique(all(x)), x.end())
#define INDEX(a, val) (lower_bound(all(a), val) - a.begin())

int totalCases, testNum;
int K;
int N;
vector <int> M;

vector <vector <int> > posns[26];
vector <string> T;
string S;

void preprocess()
{
	for(int i = 0; i < 26; i++)
		posns[i].resize(K);
	for(int i = 0; i < K; i++)
	{	
		for(int p = 0; p < M[i]; p++)
			posns[T[i][p]-'a'][i].push_back(p);
		for(int c = 0; c < 26; c++)
			posns[c][i].push_back(M[i]);
	}
}

int nxt(int spos, int ti)
{
	for(int pos = 0; spos < N; spos++, pos++)
	{
		char c = S[spos];
		int t = INDEX(posns[c-'a'][ti], pos);
		if(posns[c-'a'][ti][t] == M[ti])
			break;
		pos = posns[c-'a'][ti][t];
	}
	return spos;
}

bool input()
{
	s(K);
	s(N);
	cin >> S;
	assert(S.length() == N);
	for(int i = 0; i < K; i++)
	{
		int m;
		s(m);
		M.push_back(m);
		string x;
		cin >> x;
		assert(x.length() == m);
		T.push_back(x);
	}
	return true;
}

void solve()
{
	preprocess();
	int i, j;
	int ans = 0;
	for(i = 0, j = 0; i < N; i=j, ans++)
	{
		for(int k = 0; k < K; k++)
			j = max(j, nxt(i, k));
		if(j == i)
		{
			printf("-1\n");
			return;
		}
	}
	printf("%d\n", ans);
}

int main()
{
	input();
	solve();
}
