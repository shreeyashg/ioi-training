/*
 	C++ Template
	Pradeep George Mathias
 */


#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <cassert>
#include <string>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <stack>
#include <queue>
#include <cstdlib>
using namespace std;

#define s(T) scanf("%d", &T)
#define sl(T) scanf("%lld", &T)
#define fill(a, val) memset(a, val, sizeof(a))
#define mp make_pair
#define pb push_back
#define all(x) x.begin(), x.end()
#define DREP(x) sort(all(x)); x.erase(unique(all(x)), x.end())
#define INDEX(a, val) (lower_bound(all(a), val) - a.begin())

int totalCases, testNum;
int subtaskno;
int maxn;
int maxk;
int maxlen;
int sumlen;

int type;

void preprocess()
{
	
}

bool input()
{
	s(subtaskno);
	s(type);
	switch (subtaskno) {
		case 1:
			maxk = 1;
			maxn = 2000;
			maxlen = sumlen = 1000;
			break;
		case 2:
			maxk = 10;
			maxn = 2000;
			maxlen = sumlen = 2000;
			break;
		case 3:
			maxk = 1;
			maxn = 100000;
			maxlen = sumlen = 10000;
			break;
		case 4:
			maxk = 10;
			maxn = 100000;
			maxlen = 10000;
			sumlen = 100000;
			break;

		default:
			break;
	}
	return true;
}

void solve()
{
	int K;
	int N;
	int impossible;
	int maxtst;
	int alph;
	
	switch (type) {
		case 1:
			alph = 26;
		case 4:
			if(type != 1)
				alph = 1 + (rand()%24);
			K = maxk;
			N = maxn;
			impossible = 0;
			maxtst = 0;
			break;
		
		case 2:
			if(rand() & 1)
				alph = 1 + (rand() % 24);
			else 
				alph = 26;
			K = 1 + (rand() % maxk);
			N = 1 + (rand() % maxn);
			impossible = 0;
			maxtst = 0;
			break;
			
		case 3:
			alph = 26;
			K = maxk;
			N = 1;
			impossible = 0;
			maxtst = 0;
			break;
		
		case 5:
			if(rand() & 1)
				alph = 1 + (rand() % 24);
			else 
				alph = 25;			
			K = maxk;
			N = maxn;
			impossible = 1;
			maxtst = 0;
			break;
		
		case 6:
			alph = 1 + (rand() % 13);
			K = maxk;
			N = maxn;
			impossible = 0;
			maxtst = 1;
			break;

		default:
			break;
	}
	
	printf("%d\n", alph);
	printf("%d\n", K);
	printf("%d\n", N);
	printf("%d\n", maxlen);
	printf("%d\n", sumlen);
	printf("%d\n", impossible);
	printf("%d\n", maxtst);
	
}

int main()
{
	preprocess();
	input();
	solve();
	
}
