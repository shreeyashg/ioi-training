(TeX-add-style-hook "final2-07may2013-stamps"
 (lambda ()
    (TeX-add-symbols
     '("Red" 1)
     '("Code" 1)
     '("Mybox" 1)
     "True"
     "False")
    (TeX-run-style-hooks
     "amssymb"
     "amsfonts"
     "amsmath"
     "latexsym"
     "multicol"
     "xcolor"
     "fullpage"
     "latex2e"
     "art11"
     "article"
     "11pt")))

