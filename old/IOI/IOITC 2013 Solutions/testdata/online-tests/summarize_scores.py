import os 
import re
import sys
from collections import defaultdict

dir_name = os.path.dirname(os.path.abspath(__file__))
tests = [os.path.join(dir_name, d) for d in os.listdir(dir_name) \
				if os.path.isdir(os.path.join(dir_name, d))]
tests.sort()

files = []
dntp_files = []

for test_dir in tests:
	scores_dir = os.path.join(test_dir, "scores")
	if not os.path.exists(scores_dir):
		sys.stderr.write("Dir '%s' does not have a scores dir\n" % test_dir)
	for f in os.listdir(scores_dir):
		if f.endswith("-scores.txt"): files.append(os.path.join(scores_dir, f))
		if f == "did-not-take-part.txt": dntp_files.append(os.path.join(scores_dir, f))

tasks = []
users = []
scores = defaultdict(dict)
dntp = defaultdict(list)

for dntp_file in dntp_files: 
	f = open(dntp_file)
	for line in f: 
		m = re.match(r"(?P<user>\w+).*", line)
		dntp[m.group("user")].append(os.path.dirname(dntp_file))

for scores_file in files: 
	f = open(scores_file)
	for line in f:
		m = re.match(r"User: (?P<user>\w+), Task: (?P<task>[a-zA-Z ]*), Score: (?P<score>\d+).*", line)
		if m.group("task") not in tasks: tasks.append(m.group("task"))
		if m.group("user") not in users: users.append(m.group("user")) 
		scores[m.group("user")][m.group("task")] = int(m.group("score"))

	for user in users:
		if os.path.dirname(scores_file) in dntp[user]:
			scores[user][tasks[-1]] = -1

sys.stdout.write("                %s\n" % "\t".join(t[:7] for t in tasks))
for user in users:
	sys.stdout.write("%14s" % user)
	for task in tasks:
		if task in scores[user]: sys.stdout.write("%8d" % scores[user][task])
		else: sys.stdout.write("--\t")
	sys.stdout.write("\n")




