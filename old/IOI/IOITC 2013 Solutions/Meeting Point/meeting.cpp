#include<iostream>
#include<vector>
#include<queue>
#include<cstring>
#include<cstdio>

#define MAXN 1000010

using namespace std;

int N, M;
vector<int> g[MAXN];
int color[MAXN];
long long paint(int u) {
    long long total, s[2] = {};
    total = 0;
    queue<int> q;
    q.push(u);
    color[u] = 0;
    int t, v;
    bool bipartite = true;
    while(!q.empty()) {
	t = q.front();
	total ++;
	s[color[t]]++;
	q.pop();
	int i;
	for(i = 0; i < g[t].size(); i++) {
	    v = g[t][i];
	    if(color[v] == -1) {
		color[v] = 1 ^ color[t];
		q.push(v);
	    }
	    else if(color[v] != 1 ^ color[t]) {
		bipartite = false;
	    }
	}
    }
//    printf("%d\n", bipartite);
    return bipartite ? s[1] * s[1] + s[0] * s[0] : total * total;
}
int main() {
    int N, M;
    scanf("%d%d", &N, &M);
    int p, q, i;
    for(i = 0; i < M; i++) {
		scanf("%d %d", &p, &q); 
	g[p].push_back(q);
	g[q].push_back(p);
    }
    memset(color, -1, sizeof(color));
    long long ans = 0;
    for(i = 0; i < N; i++) {
	if(color[i] == -1) {
	    ans += paint(i);
	}
    }
    cout << ans << endl;
    return 0;
}
