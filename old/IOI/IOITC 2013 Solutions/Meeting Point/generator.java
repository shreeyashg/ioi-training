import java.io.*;
import java.util.*;

class generator
{
	BufferedReader keyin = new BufferedReader(new InputStreamReader(System.in));
	Random r = new Random();
	
	int N, M;
	int C;
	int g;
	
	int [] vert;
	int [][] edges = new int[1000005][2];
	int E;
	
	HashSet <Long> added = new HashSet<Long>();
	
	int myrand(int a, int b)
	{
		int range = b-a+1;
		int x = r.nextInt() % range;
		if(x < 0) x += range;
		return a+x;
	}
	
	public static void main(String[] args) throws IOException
	{
		new generator().exec();
	}
	
	void exec() throws IOException
	{
		System.err.println("Enter the number of (connected) graphs:");
		g = Integer.parseInt(keyin.readLine());
		
		int start = 0;
		M = N = 0;
		for(int i = 0; i < g; i++)
		{
			System.err.println("Enter the number of vertices for graph " + i);
			int n = Integer.parseInt(keyin.readLine());
			System.err.println("Enter the number of edges here (<= n(n-1)/2):");
			int m = Integer.parseInt(keyin.readLine());
			
			N += n;
			vert = new int[n];
			for(int j = 0; j < n; j++)
				vert[j] = start + j;
			gen(myrand(0, 1), n, m);
			E = M;
			start += n;
		}
		
		System.out.println(N + " " + M);
		for(int i = 0; i < M; i++)
			System.out.println(edges[i][0] + " " + edges[i][1]);
	}
	
	boolean clean(int u, int v)
	{
		if(u == v) return false;
		long tmp = u * 1000005l + v;
		if(added.contains(new Long(tmp)))
			return false;
		added.add(new Long(tmp));
		return true;
	}
	
	void gen(int bp, int n, int m)
	{
		if(bp == 1)
		{
			int n1 = myrand(0, n-1);
			int n2 = n - n1;
			m = (int)Math.min((long)m, (long)n1 * n2);
			
			for(int j = 0; j < m; j++)
			{
				int u, v;
				do 
				{
					u = myrand(0, n1-1);
					v = myrand(n1, n-1);
				}while(!clean(vert[u], vert[v]));
				edges[M][0]= vert[u];
				edges[M][1] = vert[v];
				M++;
			}
		}
		else 
		{
			for(int j = 0; j < m; j++)
			{
				int u, v;
				do 
				{
					u = myrand(0, n-2);
					v = myrand(u+1, n-1);
				}while(!clean(vert[u], vert[v]));
				edges[M][0] = vert[u];
				edges[M][1] = vert[v];
				M++;
			}
		}
	}
}
