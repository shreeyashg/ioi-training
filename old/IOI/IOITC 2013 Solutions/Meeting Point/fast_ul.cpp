#include"stdio.h"
#include"queue"
#include"vector"
using namespace std;
#define N 1000000
#define FOREACH(it, v) for (typeof((v).begin()) it = (v).begin(); it!=(v).end(); it++)
vector<int> G[N];
char color[N];
bool colorable(int v, int& sz1, int& sz2) {
	sz1 = 0, sz2 = 0;
	bool bad = 0;
	queue<int> q;
	q.push(v);
	color[v] = 3;
	sz1++;
	while(!q.empty()) {
		v = q.front();
		q.pop();
		FOREACH(it, G[v]) {
			if(color[*it]==0) {
				color[*it] = 1^color[v];
//				printf("Found %d, coloring it %d, sizes are %d, %d\n", *it, (int)color[*it], sz1, sz2);
				if(color[*it] == 2) sz2++; else sz1++;
				q.push(*it);
			}
			else bad |= (((color[*it] ^ color[v]) & 1) == 0);
		}
	}
//	printf("END %d %d bad ? %d\n", sz1, sz2, bad);
	return !bad;
}
void solve() {
	int n,m;
	scanf("%d%d", &n, &m);
	for(int i=0;i<n;i++) G[i].clear();
	while(m--) {
		int u, v;
		scanf("%d%d", &u, &v);
		G[u].push_back(v);
		G[v].push_back(u);
	}
	for(int i=0;i<n; i++) color [i] =0;
	long long ans  = 0;
	for (int i = 0; i < n; i++) {
		if(color[i]) continue;
		int u, v;
		if(colorable(i, u, v)) ans += (u*1ll*u) + (v*1ll*v);
		else ans += (u+v) * (0ll+u+v);
	}
	printf("%lld\n", ans);
}
int main() {
	int T=1;
//	scanf("%d", &T);
	while(T--) {
		solve();
	}
}
