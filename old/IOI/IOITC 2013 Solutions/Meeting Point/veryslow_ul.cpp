#include"stdio.h"
#include"vector"
#include"cassert"
using namespace std;
#define N 1000
char reach[N][N];
vector<int> G[N];
#define FOREACH(it, v) for(typeof((v).begin()) it = (v).begin(); it!=(v).end();it++)
char find(int i, int j) {
	char& x = reach[i][j];
//	printf("finiding F[%d][%d] : %d\n", i, j, x);
	if(x<2)return x;
	if(x==3) return 0;
	x=3;
	FOREACH(it1, G[i])
		FOREACH(it2, G[j]) {
			if(find(*it1, *it2)) {
//				printf("%d %d is good\n", i, j);
			       	return x = 1;
			}
		}
//	printf("%d %d is bad\n", i, j);
	return x = 0;
}
void solve() {
	int n, m;
	scanf("%d%d", &n, &m);
assert(n<=N);
	for(int i =0; i<n; i++) G[i].clear();
	while(m--) {
		int u, v;
		scanf("%d %d", &u, &v);
		G[--u].push_back(--v);
		G[v].push_back(u);
	}
	int cnt = 0;
	for(int i=0;i<n;i++) for(int j=0; j<n; j++) reach[i][j]=(i==j)?1:2;
	for(int i=0;i<n;i++) for(int j=0; j<n; j++){ if(reach[i][j]==2) find(i,j); cnt += reach[i][j];}
	printf("%d\n", cnt);
}
int main() {
	int T=1;
//	scanf("%d", &T);
	while(T--) {
		solve();
	}
}
