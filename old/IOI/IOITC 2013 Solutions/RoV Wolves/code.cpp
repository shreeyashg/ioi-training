#include"stdio.h"
#include"cmath"
#include"algorithm"
#include"cassert"
using namespace std;
int main(){
	int T;
	scanf("%d", &T);
	while(T--) {
		int type;
		scanf("%d", &type);
		if(type==0) {
		long long m;
		scanf("%Ld", &m);
		long long n = (sqrt(1+m)-1)/2;
		long long t = m - 4*n*(n-1);
//		printf("I got n = %Ld t = %Ld\n", n, t);
		while(t>8*n) n++, t = m - 4*n*(n-1);
//		printf("I got n = %Ld t = %Ld\n", n, t);
		int x = n, y = -n+t;
//		printf("%d, %d ::  %Ld \n", x,y,t);
		if(y>n) {
//			printf("%d, %d ::  %Ld \n", x,y,t);
			t-=2*n;
			y = n, x = n-t;
		}
//			printf("%d, %d ::  %Ld \n", x,y,t);
		if(x<-n) {
			t-=2*n;
			x = -n, y = n-t;
		}
//			printf("%d, %d ::  %Ld \n", x,y,t);
		if(y<-n) {
			t-=2*n;
			y = -n, x = -n + t;
		}
//		printf("%d, %d ::  %Ld \n", x,y,t);
		assert(max(abs(x), abs(y)) == n);
		printf("%d %d\n", x, y);
		}
		else {
			long long x, y;
			scanf("%Ld%Ld", &x, &y);
			long long n = max(abs(x), abs(y));
			long long idx = 4*n*(n-1);
			if(y == -n) idx += 6*n + n + x;
			else if(x == n) idx += y + n;
			else if(y == n) idx += 2*n + n - x;
			else if(x == -n) idx += 4*n + n - y;
			else assert(0);
			printf("%Ld\n", idx);
		}
	}
}
