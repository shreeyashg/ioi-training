/*
 	C++ Template
	Pradeep George Mathias
 */


#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <cassert>
#include <string>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <stack>
#include <queue>
#include <cstdlib>
using namespace std;

#define s(T) scanf("%d", &T)
#define sl(T) scanf("%lld", &T)
#define fill(a, val) memset(a, val, sizeof(a))
#define mp make_pair
#define pb push_back
#define all(x) x.begin(), x.end()
#define DREP(x) sort(all(x)); x.erase(unique(all(x)), x.end())
#define INDEX(a, val) (lower_bound(all(a), val) - a.begin())

int totalCases, testNum;

long long sqrs[1000005];
map<long long, pair<pair<long long, long long>, pair <int, int> > > corners;
int q;
long long x, y, t;

void preprocess()
{
	for(long long i = 1; i <= 1000000; i+=2)
	{
		corners[i * i] = mp(mp((i+1)/2, -(i+1)/2 + 1), mp(0, 1));
		corners[i * i + i] = mp(mp((i+1)/2, (i+1)/2), mp(-1, 0));
		corners[(i+1) * (i+1)] = mp(mp(-(i+1)/2, (i+1)/2), mp(0, -1));
		corners[(i+1) * (i+1) + i+1] = mp(mp(-(i+1)/2, -(i+1)/2), mp(1, 0));
	}
//	for(int i = 0; i <= 1000000; i++)
//		sqrs[i] = i * 1ll * i;
}

bool input()
{
	s(q);
	if(q == 0)
	{	
		sl(t);
		
	}
	else 
	{
		sl(x);
		sl(y);
	}
	return true;
}

void solve0()
{
	map<long long, pair<pair<long long, long long>, pair <int, int> > >::iterator it;
	it = corners.lower_bound(t);
	
	long long t0 = (*it).first;
	long long diff = t - t0;
	x = (*it).second.first.first + diff * (*it).second.second.first;
	y = (*it).second.first.second + diff * (*it).second.second.second;
	
	printf("%lld %lld\n", x, y);
}

void solve1()
{
	long long Linf = max(abs(x), abs(y));
	long long k = 2 * Linf - 1;
	long long tmpt = k * k;
	
	if(x == Linf && y < Linf)
		t = tmpt + y - (-Linf + 1);
	else if(y == Linf && x > -Linf)
		t = tmpt + k + (Linf - x);
	else if(x == -Linf && y > -Linf)
		t = tmpt + k + k+1 + (Linf - y);
	else 
		t = tmpt + k + k+1 + k+1 + x - (-Linf);
}

void solve()
{
	if(q == 0)
		solve0();
	else 
		solve1();
}

int main()
{
	preprocess();
	s(totalCases);
	for(testNum = 1; testNum <= totalCases; testNum++)
	{
		if( !input())
			break;
		solve();
	}
}
