/*
 	C++ Template
	Pradeep George Mathias
 */


#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <cassert>
#include <string>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <stack>
#include <queue>
#include <cstdlib>
using namespace std;

#define s(T) scanf("%d", &T)
#define sl(T) scanf("%lld", &T)
#define fill(a, val) memset(a, val, sizeof(a))
#define mp make_pair
#define pb push_back
#define all(x) x.begin(), x.end()
#define DREP(x) sort(all(x)); x.erase(unique(all(x)), x.end())
#define INDEX(a, val) (lower_bound(all(a), val) - a.begin())

int totalCases, testNum;

map<long long, pair<pair<long long, long long>, pair <int, int> > > corners;			// map from TIME to Pair< Corner, Direction of traversal>
int q;
long long x, y, t;

void preprocess()
{
	for(long long i = 1; i <= 1000000; i+=2)
	{
		//Odd corners are "starting points" of squares, and then ...
		
		corners[i * i] = mp(mp((i+1)/2, -(i+1)/2 + 1), mp(0, 1));		// NOTE: "#define mp make_pair" in the template earlier
		corners[i * i + i] = mp(mp((i+1)/2, (i+1)/2), mp(-1, 0));
		corners[(i+1) * (i+1)] = mp(mp(-(i+1)/2, (i+1)/2), mp(0, -1));
		corners[(i+1) * (i+1) + i+1] = mp(mp(-(i+1)/2, -(i+1)/2), mp(1, 0));
	}
}

bool input()
{
	s(q);		// NOTE: "#define s(T) ..." in the template, also sl(T)
	if(q == 0)
	{	
		sl(t);
		
	}
	else 
	{
		sl(x);
		sl(y);
	}
	return true;
}

void solve0()		// query type 0 t
{
	map<long long, pair<pair<long long, long long>, pair <int, int> > >::iterator it;
	it = corners.upper_bound(t); it--;		// find last corner where time <= t : recall definition of upper_bound and lower_bound. Check if you can use lower_bound instead
	
	long long t0 = (*it).first;
	long long diff = t - t0;
	x = (*it).second.first.first + diff * (*it).second.second.first;		// x = x-point of the corner + time_diff * direction of traversal...... PS: Dirty code here: sorry :P
	y = (*it).second.first.second + diff * (*it).second.second.second;
	
	printf("%lld %lld\n", x, y);
}

void solve1()		//query type 1 x y
{
	assert(x != 0 || y != 0);
	long long Linf = max(abs(x), abs(y));									// L-inf is the "square number" of the coordinate.
	long long k = 2 * Linf - 1;												// k is the "length" of the side of the square
	long long tmpt = k * k;													// tmpt is the starting-time of the square
	
	if(x == Linf && abs(y) < Linf)											// edge1: distance of y from (1-Linf)
		t = tmpt + y - (-Linf + 1);
	else if(y == Linf && x > -Linf)											// edge2: distance of x from Linf
		t = tmpt + k + (Linf - x);
	else if(x == -Linf && y > -Linf)										// edge3: distance of y from Linf
		t = tmpt + k + k+1 + (Linf - y);
	else																	// edge4: distance of x from -Linf
		t = tmpt + k + k+1 + k+1 + x - (-Linf);
	printf("%lld\n", t);
}

void solve()
{
	if(q == 0)
		solve0();
	else 
		solve1();
}

int main()
{
	preprocess();
	s(totalCases);
	for(testNum = 1; testNum <= totalCases; testNum++)
	{
		if( !input())
			break;
		solve();
	}
}
