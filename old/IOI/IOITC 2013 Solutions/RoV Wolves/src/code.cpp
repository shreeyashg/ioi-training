#include"stdio.h"
#include"cmath"
#include"algorithm"
#include"cassert"
#include <iostream>
using namespace std;
int main(){
	int T;
	scanf("%d", &T);
	while(T--) {
		int type;
		scanf("%d", &type);
		if(type==0) {
			long long m;
			scanf("%lld", &m);
			
			long long n = (sqrt(1+m)-1)/2;		// lying on square of radius n : n = max(|x|, |y|)
			long long t = m - 4*n*(n-1);		// t-th element on the square of radius n
			
			while(t>8*n) {
				n++;
				t = m - 4*n*(n-1);
			}
			
			long long x = n, y = -n+t;
			
			if(y>n) {			//crosses first corner, so "turn"
				t-=2*n;
				y = n, x = n-t;
			}
			
			if(x<-n) {			//further crosses second corner, so "turn" again
				t-=2*n;
				x = -n, y = n-t;
			}
			
			if(y<-n) {			//and again
				t-=2*n;
				y = -n, x = -n + t;
			}
			
			assert(max(abs(x), abs(y)) == n);		//code breaks if something is amiss
			printf("%lld %lld\n", x, y);
		}
		else {
			long long x, y;
			scanf("%lld%lld", &x, &y);
			
			long long n = max(abs(x), abs(y));		//get the radius
			long long idx = 4*n*(n-1);				//this is the 0-th element of the square : 0, 4, 8, 24, ... (check the position to confirm)
			
			if(y == -n) idx += 6*n + n + x;			//in the 4th edge
			else if(x == n) idx += y + n;			//in the 1st edge
			else if(y == n) idx += 2*n + n - x;		//in the 2nd edge
			else if(x == -n) idx += 4*n + n - y;	//in the 3rd edge
			else assert(0);
			
			printf("%lld\n", idx);
		}
	}
}
