import java.io.*;
import java.util.*;

class generator
{
	BufferedReader keyin = new BufferedReader(new InputStreamReader(System.in));
	Random r = new Random();
	
	int Q;
	long maxN;
	long root;
	long N;
	
	long myrand(long a, long b)
	{
		long range = b-a+1;
		long x = r.nextLong() % range;
		if(x < 0) x += range;
		return a+x;
	}
	
	public static void main(String[] args) throws IOException
	{
		new generator().exec();
	}
	
	void exec() throws IOException
	{
		System.err.println("Enter the number of queries:");
		Q = Integer.parseInt(keyin.readLine());
		System.err. println("Enter the maximum value of N:");
		maxN = Long.parseLong(keyin.readLine());
		root = (long)Math.sqrt(maxN) - 1;
		
		System.out.println(Q);
		for(int i = 0; i < Q; i++)
		{
			long corner = myrand(0, 5);
			long qtype = myrand(0, 1);
			if(corner == 0)
			{
				if(qtype == 1)
				{
					long k = myrand(-root/2, root/2);
					System.out.println("1 " + k + " " + (k + myrand(-1, 1)));
					continue;
				}
				long k = myrand(3, root);
				N = k*k;
				N += myrand(0, 1) * k;
				N += myrand(-1, 1);
				
				if(N > maxN)
					System.err.println("BAD INPUT!");
				System.out.println("0 " + N);
			}
			else if(qtype == 0)
				System.out.println("0 " + myrand(0, maxN));
			else 
				System.out.println("1 " + myrand(-root/2, root/2) + " " + myrand(-root/2, root/2));
		}
	}
}
