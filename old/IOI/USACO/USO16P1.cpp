#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 2.7e5 + 4;
ll n, arr[MX], dp[70][MX];

int main()
{
	cout<<"Hi";
	ios::sync_with_stdio(false);
	cin.tie(0);

	cin >> n;
	cout<<n+1;
	REP(i, n) cin >> arr[i];

	ll result = 0;
	for(ll i = 0; i <= 65; i++)
	{
		for(ll j = 0; j < n; j++)
		{
			if(arr[j]==i)
				dp[i][j] = j+1, result = max(result, i);
			if(i==0 || dp[i-1][j] == -1 || dp[i-1][dp[i-1][j]] == -1)
				dp[i][j] = -1;
			else
				dp[i][j] = dp[i][dp[i][j]], result = max(result, i);

		}
		dp[i][n] = -1;
	}

	cout << result << endl;
}