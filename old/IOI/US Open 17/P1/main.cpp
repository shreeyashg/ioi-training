#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 504;

vector<string> spotty, plain;
ll n, m;
ll ps[2][MX][MX];
unordered_map<ll, ll> mp[MX][MX], mpp[MX][MX];

inline string sub(string str, ll l, ll r)
{
        return str.substr(l, r-l+1);
}

int main()
{
        ios::sync_with_stdio(false);
        cin.tie(0);
        ifstream cin("cownomics.in");
        ofstream cout("cownomics.out");
        cin>>n>>m;
        REP(i, n)
        {
                string str;
                cin>>str;
                spotty.push_back(str);
        }
        
        REP(i, n)
        {
                string str;
                cin>>str;
                plain.push_back(str);
        }

        ll ans = INF;
        REP(i, m)
        {
                FOR(j, i, m-1)
                {
                        set<string> s;
                        REP(ii, spotty.size())
                        {
                                s.insert(sub(spotty[ii], i, j));
                        }
                      /*  cout<<"- ";
                        for(auto it = s.begin(); it!=s.end(); it++)
                        {
                                cout<<*it<<" ";
                        }
                        cout<<endl; */
                        
                        bool x = 0;
                        REP(ii, plain.size())
                        {
                                if(s.count((sub(plain[ii], i, j))))
                                {
                                       // cout<<ps[1][ii][j+1]-ps[1][ii][i]<<" "<<i+1<<" "<<j+1<<endl;
                                        //cout<<i<<" "<<j+1<<endl;
                                        x=1;
                                        break;
                                }
                        }
                        if(!x)
                                ans = min(ans, j-i+1);
                }
        }
        
        cout<<ans<<endl;
        
}