/*
 ID: shreero1
 LANG: C++11
 TASK: runround
 */
#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <cmath>
#include <fstream>
#include <set>
#include <algorithm>
#include <deque>

using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

typedef long long ll;

const ll INF = 1e18;

int main()
{
        ifstream cin("runround.in");
        ofstream cout("runround.out");
        ll n;
        cin>>n;
        FOR(i, n+1, INF)
        {
                bool x = 1;
                set<ll> s, s2;
                string str = to_string(i);
                ll idx = (str[0]-'0')%str.size();
                REP(i, str.size())
                {
                        if(str[i]=='0')
                                x=0;
                }
                s.insert(0);
                s2.insert(str[0]);
                while(s.size()!=str.size() && x)
                {
                       // cout<<idx<<" ";
                        if(s.count(idx))
                        {
                                x=0;
                                break;
                        }
                        s.insert(idx);
                        s2.insert(str[idx]);
                        idx = idx+(str[idx]-'0');
                        idx %= str.size();
                }
                
                if(x && s.size()==str.size() && s2.size()==str.size() && idx==0)
                {
                        cout<<str<<endl;
                        break;
                }
        }
}