/*
 ID: shreero1
 LANG: C++11
 TASK: preface
 */
#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <cmath>
#include <fstream>
#include <set>
#include <algorithm>
#include <deque>

using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

typedef long long ll;

const ll MX = 3505;

vector<char> v = {'I', 'V', 'X', 'L', 'C', 'D', 'M'};
ll cnt[3508][7];


int main()
{
        ifstream cin("preface.in");
        ofstream cout("preface.out");
        cnt[1][0] = 1;
        
        FOR(i, 2, 3500)
        {
                // I
                if(i%5)
                {
                        if(i%10==9 || i%10==4)
                        {
                                cnt[i][0] = cnt[i-1][0] + 1;
                        }
                        else
                                cnt[i][0] = cnt[i-1][0] + i%5;
                        
                }
                else
                        cnt[i][0] = cnt[i-1][0];
                
                // V
                if(i%10>=4 && i%10<=8)
                {
                        cnt[i][1] = cnt[i-1][1] + 1;
                }
                else
                        cnt[i][1] = cnt[i-1][1];
                
                // X
                if(i%50>=10 && i%50<40)
                {
                        cnt[i][2] = cnt[i-1][2] + ((i%50)/10);
                        if(i%10==9)
                                cnt[i][2]++;
                }
                else if(i%50>=40)
                {
                        cnt[i][2] = cnt[i-1][2] + 1;
                        if(i%10==9)
                                cnt[i][2]++;
                }
                else if(i%50==9)
                {
                        cnt[i][2] = cnt[i-1][2] + 1;
                }
                else
                        cnt[i][2] = cnt[i-1][2];
                
                // L
                if(i%100>=40 && i%100<=89)
                {
                        cnt[i][3] = cnt[i-1][3] + 1;
                }
                else
                        cnt[i][3] = cnt[i-1][3];
                
                // C
                if(i%500>=90 && i%500<=99)
                {
                        cnt[i][4] = cnt[i-1][4] + 1;
                }
                else if(i%500>=100 && i%500<400)
                {
                        cnt[i][4] = cnt[i-1][4] + ((i%500)/100);
                        if(i%100>=90 && i%100<=99)
                                cnt[i][4]++;
                }
                else if(i%500>=400)
                {
                        cnt[i][4] = cnt[i-1][4] + 1;
                        if(i%100>=90 && i%100<=99)
                                cnt[i][4]++;
                }
                else
                {
                        cnt[i][4] = cnt[i-1][4];
                        if(i%100>=90 && i%100<=99)
                                cnt[i][4]++;
                }
                
                // D
                if(i%1000>=400 && i%1000<=899)
                {
                        cnt[i][5] = cnt[i-1][5] + 1;
                }
                else
                        cnt[i][5] = cnt[i-1][5];
                
                // M
                if(i%5000>=900 && i%5000<=999)
                {
                        cnt[i][6] = cnt[i-1][6] + 1;
                }
                else if(i%5000>=1000 && i%5000<4000)
                {
                        cnt[i][6] = cnt[i-1][6] + ((i%5000)/1000);
                        if(i%1000>=900 & i%1000<=999)
                                cnt[i][6]++;
                }
                else
                        cnt[i][6] = cnt[i-1][6];
        }
        
        ll n;
        cin>>n;
        
        
        REP(i, v.size())
        {
                if(cnt[n][i])cout<<v[i]<<" "<<cnt[n][i]<<endl;
        }
}