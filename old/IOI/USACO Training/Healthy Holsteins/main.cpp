/*
 ID: shreero1
 LANG: C++11
 TASK: holstein
 */
#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <cmath>
#include <fstream>
#include <set>
#include <algorithm>
#include <deque>

using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

typedef long long ll;

const ll VX = 30, GX = 20;
ll v, g, vit[VX], data[GX][VX], cur[VX], ansarr[VX], cursar[VX];
vector<ll> an, cu;

int main()
{
        ifstream cin("holstein.in");
        ofstream cout("holstein.out");
        cin>>v;
        REP(i, v) cin>>vit[i+1];
        cin>>g;
        REP(i, g)
        {
                REP(j, v) cin>>data[i+1][j+1];
        }
        
        ll ans = 1e15, curr=0;
        REP(i, (ll)pow(2, g)-1)
        {
                curr=0;
                ll idx = i+1;
                REP(j, g)
                {
                        if(idx&(1<<(j)))
                        {
                                curr++;
                                cu.push_back(j+1);
                                REP(f, v)
                                {
                                        cur[f+1]+=data[j+1][f+1];
                                }
                        }
                }
                
            /*    cout<<"--------------------"<<endl;
                REP(i, cu.size())
                {
                        cout<<cu[i]<<" ";
                }
                cout<<endl;
                REP(i, v)
                {
                        cout<<cur[i+1]<<endl;
                } */
                bool f=1;
                REP(j, v)
                {
                        if(cur[j+1]<vit[j+1]){
                                f=0; break;
                               // DEBUG(cu.size())
                        }
                }
                if(f)
                {
                        if(curr<ans)
                        {
                              //  DEBUG(ans);
                                ans = curr;
                                an = cu;
                        }
                }
                memset(cur, 0, sizeof(cur));
                cu.clear();
        }
        sort(an.begin(), an.end());
        cout<<an.size()<<" ";
        REP(i, an.size())
        {
                if(i!=an.size()-1)cout<<an[i]<<" ";
                else cout<<an[i];
        }
        cout<<endl;
}


















