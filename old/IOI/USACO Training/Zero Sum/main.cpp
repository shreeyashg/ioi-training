/*
 ID: shreero1
 LANG: C++11
 TASK: zerosum
 */
#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <string>
#include <cmath>
#include <fstream>
#include <set>
#include <algorithm>
#include <deque>
#include <bitset>

using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

typedef long long ll;

set<string> ans;
string temp;
ll n;

bool chk(string str)
{
        string temp="";
        ll sum=0;
        char pre = '+';
        REP(i, str.size())
        {
                if(!(i%2))
                {
                        temp=temp+(str[i]);
                }
                else
                {
                        if(str[i]==' ')
                        {
                                continue;
                        }
                        
                        if(pre=='+')
                                sum+=stoi(temp), pre=str[i], temp="";
                        else if(pre=='-')
                                sum-=stoi(temp), pre=str[i], temp="";
                }
                
                if(temp[temp.size()-1]==' ')
                        temp.pop_back();
        }
        if(temp!="")
        {
                if(pre=='+')
                        sum+=stoi(temp);
                else
                        sum-=stoi(temp);
        }
     //   cout<<str<<" = "<<sum<<endl;
        return sum==0;
        
}

void rec(ll idx, string str)
{
        if(idx==n)
        {
                if(chk(str))
                        ans.insert(str);
                return;
        }
        
        rec(idx+1, str+'+'+to_string(idx+1));
        rec(idx+1, str+'-'+to_string(idx+1));
        rec(idx+1, str+' '+to_string(idx+1));
}

int main()
{
        ifstream cin("zerosum.in");
        ofstream cout("zerosum.out");
        string x = to_string(1)+to_string(2);
        ll k = stoi(x);
        //DEBUG(k);
        
       // chk("1 2+3 4-5 6 7");
        
        cin>>n;
        
        rec(1, "1");
        for(auto it = ans.begin(); it!=ans.end(); it++)
        {
                cout<<*it<<endl;
        }
}