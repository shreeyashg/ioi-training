/*
 ID: shreero1
 LANG: C++11
 TASK: money
 */
#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <string>
#include <cmath>
#include <fstream>
#include <set>
#include <algorithm>
#include <deque>
#include <bitset>

using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

typedef long long ll;

ll V, N;//, coins[30];
const ll NX = 1e4 + 35;
ll ways[NX];
vector<ll> coins;
int main()
{
        ifstream cin("money.in");
        ofstream cout("money.out");
        cin>>V>>N;
        REP(I, V)
        {
                ll k;
                cin>>k;
                coins.push_back(k);
                //cin>>coins[I];
        }
        sort(coins.begin(), coins.end());
        ways[0]=1;
        for(ll j=0; j<V; j++)
        {
                for(ll i=coins[j]; i<=N; i++)
                {
                        ways[i] += ways[i-coins[j]];
                }
        }
        cout<<ways[N]<<endl;
}