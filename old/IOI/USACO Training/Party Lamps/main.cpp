/*
 ID: shreero1
 LANG: C++11
 TASK: lamps
 */
#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <cmath>
#include <fstream>
#include <set>
#include <algorithm>
#include <deque>
#include <bitset>

using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

typedef long long ll;
ll n, c;

const ll NX = 1e2 + 3;
bool arr[NX];
ll on[NX];
set<vector<bool> > ans;
map<ll, map<vector<bool> , bool > > vis;

void rec(ll cnt, vector<bool> x)
{
        if(vis[cnt][x])
                return;
        vis[cnt][x]=1;
        if(cnt>c)
                return;
        if(cnt==c)
        {
                REP(i, n)
                {
                        if(on[i]!=-1 && on[i]!=x[i])
                        {
                                return;
                        }
                }
                ans.insert(x);
                return;
        }
        
        vector<bool> x2(n);
        REP(i, n)
        {
                x2[i] = !x[i];
        }
        rec(cnt+1, x2);
        REP(i, n)
        {
                if(i%2)
                        x2[i]=!x[i];
                else
                        x2[i]=x[i];
        }
        rec(cnt+1, x2);
        REP(i, n)
        {
                if(i%2)
                        x2[i]=x[i];
                else
                        x2[i]=!x[i];
        }
        rec(cnt+1, x2);
        REP(i, n)
        {
                if(i%3==0)
                {
                        x2[i]=!x[i];
                }
                else
                        x2[i]=x[i];
        }
        rec(cnt+1, x2);
}

int main()
{
     //   ifstream cin("lamps.in");
       // ofstream cout("lamps.out");
        memset(on, -1, sizeof(on));
        cin>>n>>c;
        ll k;
        cin>>k;
        while(k!=-1)
        {
                //cout<<k<<endl;
                on[k-1]=1;
                cin>>k;
        }
        cin>>k;
        while(k!=-1)
        {
                //DEBUG(k);
                on[k-1]=0;
                cin>>k;
        }
        
        vector<bool> temp(NX, 1);
        //memset(temp, 0, sizeof(temp));
        rec(0, temp);
        for(auto it = ans.begin(); it!=ans.end(); it++)
        {
                REP(i, n)
                {
                        cout<<(*it)[i];
                }
                cout<<endl;
        }
        if(ans.empty())
                cout<<"IMPOSSIBLE"<<endl;
}