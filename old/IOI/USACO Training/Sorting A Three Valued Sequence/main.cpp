/*
 ID: shreero1
 LANG: C++11
 TASK: sort3
 */
#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <fstream>
#include <set>
#include <algorithm>
#include <deque>
using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define NMAX 1000009
#define INF 1000009*1e9

typedef long long ll;

const ll MX = 1004;
ll ones, twos, threes;

vector<ll> v;
vector<pair<ll,ll> >swaps;

int main()
{
        ifstream cin("sort3.in");
        ofstream cout("sort3.out");
        ll n;
        cin>>n;
        v.push_back(0);
        REP(i, n)
        {
                ll k;
                cin>>k;
                if(k==1)
                        ones++;
                if(k==2)
                        twos++;
                else
                        threes++;
                v.push_back(k);
        }
        FOR(i, 1, v.size()-1)
        {
                if(v[i]==1 && i>ones)
                {
                        bool x =0;
                        FOR(j, 1, ones)
                        {
                                if(v[j]==2)
                                {
                                        swaps.push_back({i, j});
                                        swap(v[i], v[j]);
                                        x=1;
                                        break;
                                }
                        }
                        if(!x)
                        {
                                FOR(j, 1, ones)
                                {
                                        if(v[j]==3)
                                        {
                                                swaps.push_back({i,j});
                                                swap(v[i], v[j]);
                                                break;
                                        }
                                }
                        }
                }
        }
        FOR(i, ones+1, v.size()-1)
        {
                if(v[i]==2 && i>ones+twos)
                {
                        FOR(j, ones+1, v.size()-1)
                        {
                                if(v[j]!=2)
                                {
                                        swaps.push_back({i,j});
                                        swap(v[i],v[j]);
                                        break;
                                }
                        }
                }
        }
        
        cout<<swaps.size()<<endl;
       /* REP(i, swaps.size())
        {
                cout<<swaps[i].first<<" "<<swaps[i].second<<endl;
        }*/
}