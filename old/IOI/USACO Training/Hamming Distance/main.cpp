/*
 ID: shreero1
 LANG: C++11
 TASK: hamming
 */
#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <cmath>
#include <fstream>
#include <set>
#include <algorithm>
#include <deque>

using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

typedef long long ll;

ll n, b, d;
vector<ll> nums;

ll distd(ll x, ll y)
{
        ll k=0, dd=0;
        while(max(x>>k, y>>k)>0)
        {
                if(((x>>k)&1)^(((y>>k)&1)))
                        dd++;
                k++;
        }
        return dd;
}

int main()
{
        ifstream cin("hamming.in");
        ofstream cout("hamming.out");
        nums.push_back(0);
        cin>>n>>b>>d;
        FOR(i, 1, 1<<b)
        {
                bool x = 1;
                REP(j, nums.size())
                {
                        if(distd(nums[j], i)<d)
                        {
                                x=0;
                                break;
                        }
                }
                if(x)
                        nums.push_back(i);
        }
        sort(nums.begin(), nums.end());
        ll cnt=0;
        REP(i, nums.size())
        {
                if(cnt==n) break;
                if(i%10==9 && i>0)cout<<nums[i]<<endl;
                else if(i!=nums.size()-1 && cnt!=n-1) cout<<nums[i]<<" ";
                else cout<<nums[i]<<endl;
                cnt++;
        }
}