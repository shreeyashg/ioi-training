/*
 ID: shreero1
 LANG: C++11
 TASK: frac1
*/
#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <fstream>
#include <set>
#include <algorithm>
#include <deque>
using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define NMAX 1000009
#define INF 1000009*1e9

typedef long long ll;

const ll MX = 165;


struct C
{
        bool operator() (const pair<ll, ll> &a, const pair<ll,ll> &b) const
        {
                return (double)a.first/(double)a.second < (double)b.first/(double)b.second;
        }
};
set<pair<ll,ll>, C>s;


int main()
{
        ifstream cin("frac1.in");
        ofstream cout("frac1.out");
        ll n;
        cin>>n;
        FOR(i, 0.0, n)
        {
                FOR(j, 1.0, n)
                {
                        if((double)i/(double)j>1)
                                continue;
                        ll f = __gcd(i, j);
                        s.insert({i/__gcd(i,j),j/__gcd(i,j)});
                }
        }
        
        
        for(set<pair<ll,ll>>::iterator it = s.begin(); it!=s.end(); it++)
        {
                cout<<(*it).first<<"/"<<(*it).second<<endl;
        }
}
