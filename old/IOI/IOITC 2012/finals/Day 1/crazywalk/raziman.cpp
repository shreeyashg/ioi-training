# include <cstdio>
# include <algorithm>

using namespace std;

const int NMAX=10000,LMAX=1000;
const long long INF=1ll<<50;

long long best[NMAX][LMAX+1],bestint[NMAX];
char isbad[NMAX];

int main()
{
	int N,L,Q,M,temp;
	scanf("%d%d%d%d",&N,&L,&Q,&M);
	
	for(int i=0;i<M;i++)
		scanf("%d",&temp),isbad[temp]=1;
	
	for(int i=0;i<N;i++)
		bestint[i]=((i+L<N)||isbad[i])?INF:0;
	
	for(int i=N-1;i>=0;i--)
		for(int j=L;j>0;j--)
		{
			if(isbad[i]||i<j)
			{
				best[i][j]=INF;
				continue;
			}
			best[i][j]=min(bestint[i]+Q,((i+j<N)?best[i+j][j]:0)+1);
			bestint[i-j]=min(bestint[i-j],best[i][j]);
		}
	printf("%Ld\n",(bestint[0]>=INF)?-1:bestint[0]+Q);
	
	return 0;
}
