#include<iostream>
#include<cstdio>
#include<sstream>
#include "MersenneTwister.h"

using namespace std;

const int max_n=10000;
bool visited[max_n+1]={0};

int main(int argc, char **argv)
{
	if(argc!=7)
	{
	  fprintf(stderr, "%s : Wrong format for command-line arguments.",argv[0]);
	  return 1;
	}
	
	int n,refl,refq,pnum,pden,seed;
	istringstream is(string(argv[1]) + " " + string(argv[2]) + " " + string(argv[3]) +  " " + string(argv[4]) +  " " + string(argv[5]) + " " + string(argv[6]));
	is>>n>>refl>>refq>>pnum>>pden>>seed;
	MTRand R(seed);
	
	int l=R.randInt(refl-1)+1;
	int q=R.randInt(refq-2)+2;
	
	printf("%d %d %d\n",n,l,q);
	int m=0;
	for(int i=1; i<n; ++i)
	{
		if(R.randInt(pden-1)<pnum)
		{
			visited[i]=true;
			++m;
		}
	}
	
	printf("%d\n",m);
	
	for(int i=1; i<n; ++i)
	{
		if(visited[i])
			printf("%d ",i);
	}
	printf("\n");
	
	return 0;
}
