#include<iostream>
#include<cstdio>
#include<limits>

using namespace std;

const int max_n=10000;
const int max_l=1000;

const int inv=numeric_limits<int>::max();

int n,l,q;
int m;
bool lineat[max_n+max_l+1]={0};
int best[max_n+max_l][max_l+1];
int lbest[max_n+max_l];
int res;

int spadd(int a, int b)
{
	if(a==inv or b==inv)
		return inv;
	else
		return a+b;
}

int main()
{
	scanf("%d %d %d",&n,&l,&q);
	
	scanf("%d",&m);
	int cline;
	for(int i=0; i<m; ++i)
	{
		scanf("%d",&cline);
		lineat[cline]=true;
	}
	
	res=inv;
	for(int i=1; i<n+l; ++i)
	{
		if(lineat[i])
		{
			for(int j=1; j<=l; ++j)
				best[i][j]=inv;
			lbest[i]=inv;
		}
		else
		{
			lbest[i]=inv;
			for(int j=1; j<=l; ++j)
			{
				if((i-j)==0)
				{
					best[i][j]=q;
				}
				else if(i-j>0)
				{
					best[i][j]=min( spadd(lbest[i-j],q) , spadd(best[i-j][j],1) );
				}
				else
					best[i][j]=inv;
					
				lbest[i]=min(lbest[i],best[i][j]);
			}
			if(i>=n)
				res=min(res,lbest[i]);
		}
	}
	
	/*for(int i=1; i<n+l; ++i)
	{
		cout<<i<<" : "<<lbest[i]<<endl;
		for(int j=1; j<=l; ++j)
		{
			cout<<i<<" "<<j<<" : "<<best[i][j]<<"    ";
		}
		cout<<endl;
	}*/
	
	//cout<<n<<" "<<q<<" "<<l<<endl;
	
	if(res==inv)
		printf("-1\n");
	else
		printf("%d\n",res);
	
	return 0;
}

