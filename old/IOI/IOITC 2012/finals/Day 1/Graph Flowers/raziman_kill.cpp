# include <cstdio>
# include <iostream>
# include <algorithm>
# include <vector>
# include <cstring>
# include <cctype>
# include <set>
# include <map>
# include <cmath>
# include <queue>
# include <string>

using namespace std;

const int NMAX=10000;

map<int,int> bestmap;
vector<int> adjlist[NMAX];
multiset<int> bestset[NMAX];

int work(int u,int v)
{
	if(bestmap.count(u*NMAX+v))return bestmap[u*NMAX+v];
	
	int ret=0;
	for(int i=0;i<adjlist[v].size();i++)
		if(adjlist[v][i]!=u)ret=max(ret,work(v,adjlist[v][i]));
	bestmap[u*NMAX+v] = ret+1;
	return ret+1;
}

int main()
{
	int N,K;
	scanf("%d%d",&N,&K);
	
	for(int i=1;i<N;i++)
	{
		int a,b;
		scanf("%d%d",&a,&b);
		adjlist[a].push_back(b);
		adjlist[b].push_back(a);
	}
	
	for(int i=0;i<N;i++)
		for(int j=0;j<adjlist[i].size();j++)
			bestset[i].insert(work(i,adjlist[i][j]));
	
	int soln=0;
	for(int i=0;i<N;i++)
	{
		int cur=1,j=0;
		for(multiset<int>::iterator sit=bestset[i].begin();sit!=bestset[i].end()&&j<K;sit++,j++)
			cur+=*sit;
		soln=max(soln,cur);
	}
	
	printf("%d\n",soln);
	return 0;
}
