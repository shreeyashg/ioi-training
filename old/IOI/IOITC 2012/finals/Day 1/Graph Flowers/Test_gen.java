import java.io.*;
import java.util.*;

class Test_gen
{
	BufferedReader keyin = new BufferedReader(new InputStreamReader(System.in));
	Random rand = new Random();
	
	int N, K;
	int heuristic;
	
	void exec() throws IOException
	{
		System.err.println("Task Number: ");
		int taskno = Integer.parseInt(keyin.readLine());
		
		System.err.println("Enter heuristic: (1) Star (2) Near Star (3) Path (4) Near Path (5) Complete Sol (6) Regular K-tree (7) Strands");
		heuristic = Integer.parseInt(keyin.readLine());
		
		switch (taskno) {
			case 1:
				N = 50000;	if(heuristic % 2 == 0) N -= rand.nextInt(150);
				K = 2;
				break;
				
//			case 2:
//				N = 100;	if(heuristic % 2 == 0) N -= rand.nextInt(15);
//				K = 100;
//				break;

			case 2:
				N = 1000;	if(heuristic % 2 == 0) N -= rand.nextInt(50);
				K = 100;
				break;

			case 3:
				N = 50000;	if(heuristic % 2 == 0) N -= rand.nextInt(150);
				K = 100;
				break;
				
			case 4:
				N = 50000;	if(heuristic % 2 == 0) N -= rand.nextInt(150);
				K = 2 + rand.nextInt(49999);

			default:
				break;
		}
		
		generate();
	}
	
	void generate()
	{
		System.out.println(N + " " + K);
		int u, v, deg;
		int root = (int)Math.sqrt(N);
		int r;
		switch (heuristic) {
			case 1:
				for(int i = 1; i < N; i++)
					System.out.println("0 " + i);
				break;
				
			case 2:
				for(u = 0, v = 1; v < N; u++)
				{
					deg = N - rand.nextInt(root);
					for(; deg > 0 && v < N; deg--, v++)
						System.out.println(u + " " + v);
				}
				break;
				
			case 3:
				for(u = 0, v = 1; v < N; u++,v++)
					System.out.println(u + " " + v);
				break;
				
			case 4:
				v = 1;
				for(int head = 0; v < N; head++)
				{
					deg = N/(1 + rand.nextInt(root));
					for(u = head; deg > 0 && v < N; deg--, u = v, v++)
						System.out.println(u + " " + v);
				}
				break;

			case 5:
				deg = K+1;
				for(v = 1; deg > 0 && v < N; deg--, v++)
					System.out.println("0 " + v);
				for(u = 1; v < N; u++, v++)
					System.out.println(u + " " + v);
				break;

			case 6:
				for(u = 0, v = 1, deg = K; v < N; u++, deg= 5 + rand.nextInt(root/2))
					for(; v < N && deg>0; deg--, v++)
						System.out.println(u + " " + v);
				break;

			case 7:
				for(u = 0, v = 1; v < N; u++)
				{
					r = rand.nextInt(2*root);
					if(r < 1) deg = root;
					else deg = 1;
					for(; v < N & deg > 0; deg--, v++)
						System.out.println(u + " " + v);
				}
				break;

			default:
				break;
		}
	}
	
	public static void main(String[] args) throws IOException
	{
		new Test_gen().exec();
	}
}
