# include <cstdio>
# include <iostream>
# include <algorithm>
# include <vector>
# include <cstring>
# include <cctype>
# include <set>
# include <map>
# include <cmath>
# include <queue>
# include <string>

using namespace std;

const int NMAX=50000;

vector<int> adjlist[NMAX];
multiset<int> bestset[NMAX];
int degree[NMAX];

int work1(int u,int v)
{
	int ret=0;
	for(int i=0;i<adjlist[v].size();i++)
		if(adjlist[v][i]!=u)ret=max(ret,work1(v,adjlist[v][i]));
	bestset[u].insert(ret+1);
	return ret+1;
}

void work2(int u,int v)
{
	int ret=0;
	int thisdir=(bestset[v].empty()?0:*(bestset[v].rbegin()))+1,ubest=*(bestset[u].rbegin());
	set<int>::reverse_iterator rit=bestset[u].rbegin();rit++;
	
	if(thisdir==ubest)ret=(bestset[u].size()==1?0:*rit)+1;
	else ret=ubest+1;
	
	bestset[v].insert(ret);
	
	for(int i=0;i<adjlist[v].size();i++)
		if(adjlist[v][i]!=u)work2(v,adjlist[v][i]);
}

int main()
{
	int N,K;
	scanf("%d%d",&N,&K);
	
	for(int i=1;i<N;i++)
	{
		int a,b;
		scanf("%d%d",&a,&b);
		adjlist[a].push_back(b);
		adjlist[b].push_back(a);
		degree[a]++,degree[b]++;
	}
	
	if(N == 1)
	{
		printf("1\n");
		return 0;
	}
	
	int V=-1;
	for(int i=0;i<N;i++)
		if(degree[i]==1)
		{
			V=i;
			break;
		}
	
	work1(V,adjlist[V][0]);
	work2(V,adjlist[V][0]);
	
	int soln=0;
	for(int i=0;i<N;i++)
	{
		int cur=1,j=0;
		for(multiset<int>::reverse_iterator sit=bestset[i].rbegin();sit!=bestset[i].rend()&&j<K;sit++,j++)
		//for(multiset<int>::iterator sit = bestset[i].begin(); sit!=bestset[i].end() && j < K; sit++,j++)
			cur+=*sit;
		soln=max(soln,cur);
	}
	
	printf("%d\n",soln);
	return 0;
}
