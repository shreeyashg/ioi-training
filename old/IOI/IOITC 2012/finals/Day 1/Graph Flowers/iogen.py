import random
rint = random.randint

def printSpanningTree(L):	
  edges = []
  dest = range(L)
  random.shuffle(dest)
  src = [-1] * L
  src[0] = dest[0]
  for i in range(1,L):
    a = random.choice(src[0:i])
    b = dest[i]
    src[i] = b
    print a, b
    
def main():
  N = 50000
  K = 100
  import sys
  FILES = 3
  
  #subtask1
  #for f in xrange(FILES):
    #fname = '1_%d.in' %f
    #sys.stdout = open(fname, 'w')
    #N, K = rint(1, 50000), 2
    #print N, K
    #printSpanningTree(N)

  #subtask2
  for f in xrange(FILES):
    fname = '2_%d.in' %f
    sys.stdout = open(fname, 'w')
    N, K = rint(1, 1000), rint(2, 50000)
    print N, K
    printSpanningTree(N)    
    
  ##subtask3
  #for f in xrange(FILES):
    #fname = '3_%d.in' %f
    #sys.stdout = open(fname, 'w')
    #N, K = rint(1, 1000), rint(1, 100)
    #print N, K
    #printSpanningTree(N)    

    #subtask4
  for f in xrange(FILES):
    fname = '4_%d.in' %f
    sys.stdout = open(fname, 'w')
    N, K = rint(1, 50000), rint(2, 50000)
    print N, K
    printSpanningTree(N)
    
main()    