#include<iostream>
#include<cstdio>
#include<vector>
#include<algorithm>

using namespace std;

const int max_n=50000;
const int max_k=50000;

int n,k;
int res=0;

vector<int> adjl[max_n];
vector<int> chl[max_n];
bool visited[max_n]={0};

int LPP[max_n];
int LP[max_n];

int C[max_n];
int Csz;

void DFS1(int vt)
{
	visited[vt]=true;
	LP[vt]=1;
	
	int vtnbr;
	for(int i=0; i<adjl[vt].size(); ++i)
	{
		vtnbr=adjl[vt][i];
		
		if(not visited[vtnbr])
		{
			chl[vt].push_back(vtnbr);
			DFS1(vtnbr);
			LP[vt]=max(LP[vt],1+LP[vtnbr]);
		}
	}
}

void DFS2(int vt)
{
	int ch;
	
	C[0]=LPP[vt];
	Csz=1;
	for(int i=0; i<chl[vt].size(); ++i)
	{
		ch=chl[vt][i];
		C[Csz]=LP[ch];
		++Csz;
	}
	sort(C,C+Csz);
	reverse(C,C+Csz);
	
	int cflower=1;
	for(int i=0; i<min(Csz,k); ++i)
		cflower+=C[i];
	
	res=max(res,cflower);
	
	int m1=0,m1id=-1,m2=0,m2id=-1;
	for(int i=0; i<chl[vt].size(); ++i)
	{
		ch=chl[vt][i];
		if(LP[ch]>m1)
		{
			m2=m1;
			m2id=m1id;
			m1=LP[ch];
			m1id=ch;
		}
		else if(LP[ch]>m2)
		{
			m2=LP[ch];
			m2id=ch;
		}
	}
		
	for(int i=0; i<chl[vt].size(); ++i)
	{
		ch=chl[vt][i];
		
		if(ch==m1id)
			LPP[ch]=max(LPP[vt]+1,m2+1);
		else
			LPP[ch]=max(LPP[vt]+1,m1+1);
		
		DFS2(ch);
	}
}

int main()
{
	scanf("%d %d",&n,&k);
	
	int tu,tv;
	for(int i=0; i<n-1; ++i)
	{
		scanf("%d %d",&tu,&tv);
		adjl[tu].push_back(tv);
		adjl[tv].push_back(tu);
	}
	
	DFS1(0);
	
	LPP[0]=0;
	DFS2(0);
	
	printf("%d\n",res);
	
	return 0;
}

