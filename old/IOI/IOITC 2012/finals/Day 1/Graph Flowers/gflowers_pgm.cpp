/*
 *  gflowers.cpp
 *
 */

#include <cstdio>
#include <vector>
#include <algorithm>
#include <cstring>
#include <iostream>
using namespace std;

const int max_n = 50005;
const int max_k = 50005;

int N, K;
vector <int> adjl[max_n];
int deg[max_n];
int parent[max_n];
int height[max_n];

int DP1[max_n][2];			// dp(i,j) maximum size of a "flower" where bud is at node i, tree considered is only SUBTREE rooted at i, and having atmost j-stems emerging Excluding node i
int DP2[max_n];					// dp(i) maximum length of a path from i's PARENT which goes AWAY from node i

vector <pair<int, int> > childheights[max_n];

void dfs(int u, int par)
{
	parent[u] = par;
	int maxHeight = 0;
	for(int i = 0; i < deg[u]; i++)
	{
		int v = adjl[u][i];
		if(v == par) continue;
		dfs(v, u);
		maxHeight = max(maxHeight, height[v]);
		childheights[u].push_back(make_pair(height[v], v));
	}
	sort(childheights[u].rbegin(), childheights[u].rend());
	height[u] = 1 + maxHeight;
}

int f(int u, int k)
{
	if(k == K)
		k = 1;
	else 
		k = 0;
	
	if(DP1[u][k] != -1)
		return DP1[u][k];
	
	int sum = 0;
	for(int i = 0; i < min(K-1, (int)childheights[u].size()); i++)
		sum += childheights[u][i].first;
	
	DP1[u][0] = sum;
	if(childheights[u].size() >= K)
		sum += childheights[u][K-1].first;
	DP1[u][1] = sum;
	
	return DP1[u][k];
}

int g(int u)
{
	int p = parent[u];
	if(p < 0)
		return 0;			//attempting to find a path that doesnt exist.
	
	if(DP2[u] != -1)
		return DP2[u];
	
	int maxPath = g(p);
	if(u == childheights[p][0].second)
	{	
		if(childheights[p].size() > 1)
			maxPath = max(maxPath, childheights[p][1].first);
	}
	else 
		maxPath = max(maxPath, childheights[p][0].first);
	
	return DP2[u] = 1 + maxPath;
}

int flower_size(int _N, int _K, int _from[], int _to[])
{
//construct adjacency list	
	N = _N;
	K = _K;
	for(int i = 0; i < N-1; i++)
	{
		adjl[_from[i]].push_back(_to[i]); deg[_from[i]]++;
		adjl[_to[i]].push_back(_from[i]); deg[_to[i]]++;
	}

//root the tree
	
	dfs(0, -1);

	memset(DP1, -1, sizeof DP1);
	memset(DP2, -1, sizeof DP2);

//compute answer
	int ans = 0; int rot = -1, mdeg = -1;
	for(int u = 0; u < N; u++)
	{	
		int cons = 1 + max(f(u, K-1) + g(u), f(u, K));
		if(cons > ans || (cons == ans && deg[u] > deg[rot]))
		{
			ans = cons;
			rot = u;
		}
//		ans = max(ans, 1 + max(f(u, K-1) + g(u), f(u, K)));		
		mdeg = max(mdeg, deg[u]);
	}
	cerr << rot << " " << mdeg  << " " << deg[rot] << endl;
	
	return ans;
}


//TESTING
int from[max_n], to[max_n];

int main()
{
	int n, k;
	scanf("%d %d", &n, &k);
	for(int i = 0; i < n-1; i++)
		scanf("%d %d", &from[i], &to[i]);
	printf("%d\n", flower_size(n, k, from, to));
}
