#include <stdio.h>

char grid[201][201];

main() {
	int m, n, b, sx, sy, tx, ty;
	scanf("%d %d %d", &m, &n, &b);
	scanf("%d %d %d %d", &sx, &sy, &tx, &ty);
	int i, j, k, bulx, buly, blrx, blry;
	
	for(i=0; i<m; i++)
		for(j=0; j<n; j++)
			grid[i][j] = '.';
	
	grid[sx][sy] = 's';
	grid[tx][ty] = 't';
	for(i=0; i<b; i++) {
		scanf("%d %d %d %d", &bulx, &buly, &blrx, &blry);
		for(j=bulx; j<=blrx; j++)
			for(k=buly; k<= blry; k++)
				grid[j][k] = 'X';
	}
	
	for(i=0; i<m; i++) {
		for(j=0; j<n; j++)
			printf("%c", grid[i][j]);
		printf("\n");
	}
	
	return 0;
}
