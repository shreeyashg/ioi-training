#include<vector>
#include<stack>
#include<set>
#include<map>
#include<queue>
#include<deque>
#include<string>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<cassert>
#include<cstdlib>
#include<cstdio>
#include<cmath>
#include<string>

using namespace std;

#define s(n)					scanf("%d",&n)
#define sl(n) 					scanf("%lld",&n)
#define sf(n) 					scanf("%lf",&n)
#define ss(n) 					scanf("%s",n)
#define INF						(int)1e9
#define LINF					(long long)1e18
#define EPS						1e-9
#define maX(a,b)				((a)>(b)?(a):(b))
#define miN(a,b)				((a)<(b)?(a):(b))
#define abS(x)					((x)<0?-(x):(x))
#define FOR(i,a,b)				for(int i=a;i<b;i++)
#define REP(i,n)				FOR(i,0,n)
#define foreach(v,c)            for( typeof((c).begin()) v = (c).begin();  v != (c).end(); ++v)
#define mp						make_pair
#define FF						first
#define SS						second
#define tri(a,b,c)				mp(a,mp(b,c))
#define XX						first
#define YY						second.first
#define ZZ						second.second
#define pb						push_back
#define fill(a,v) 				memset(a,v,sizeof a)
#define all(x)					x.begin(),x.end()
#define SZ(v)					((int)(v.size()))
#define DREP(a)					sort(all(a)); a.erase(unique(all(a)),a.end())
#define INDEX(arr,ind)			(lower_bound(all(arr),ind)-arr.begin())
#define debug(args...)			{dbg,args; cerr<<endl;}
#define dline					cerr<<endl	

void sc(char &c){
	char temp[4];	ss(temp);	
	c=temp[0];
}

struct debugger
{
	template<typename T> debugger& operator , (const T& v)
	{	
		cerr<<v<<" ";	
		return *this;	
	}
} dbg;

void debugarr(int * arr,int n)
{
	cout<<"[";
	for(int i=0;i<n;i++)
		cout<<arr[i]<<" ";
	cout<<"]"<<endl;
}





typedef long long LL;
typedef pair<int,int> PII;
typedef pair<LL,LL> PLL;
typedef pair<LL,PII> TRI;

typedef vector<int> VI;
typedef vector<LL> VL;
typedef vector<PII> VII;
typedef vector<PLL> VLL;
typedef vector<TRI> VT;

typedef vector<VI> VVI;
typedef vector<VL> VVL;
typedef vector<VII> VVII;
typedef vector<VLL> VVLL;
typedef vector<VT> VVT;


/*Main code begins now */

int testnum;
int R,C,B;
int sx,sy,dx,dy;
int r1[500],r2[500],c1[500],c2[500];
VI rall,call;

int occ[1500][1500];
LL dist[1500][1500];
set<TRI> q;

int delx[] = {-1,1,0,0};
int dely[] = {0,0,-1,1};

void solve()
{
	fill(occ,0);
	for(int i=0;i<B;i++)
	{
		occ[ r1[i] ][ c1[i] ]++;
		occ[ r1[i] ][ c2[i] ]--;
		occ[ r2[i] ][ c1[i] ]--;
		occ[ r2[i] ][ c2[i] ]++;
	}
	
	for(int i=0;i<R;i++)
	{
		for(int j=0;j<C;j++)
		{
			if(i>0)	occ[i][j] += occ[i-1][j];
			if(j>0)	occ[i][j] += occ[i][j-1];
			if(i>0 && j>0)	occ[i][j] -= occ[i-1][j-1];
		}
	}
		
	for(int i=0;i<R;i++)
		for(int j=0;j<C;j++)
			dist[i][j] = (LL)1e18;
			
	dist[sx][sy]=0;
	q.insert(tri(0ll,sx,sy));
	
	while(!q.empty())
	{
		TRI temp = *(q.begin());
		q.erase(q.begin());
		
		LL d = temp.XX;
		int x = temp.YY;
		int y = temp.ZZ;
		if(d > dist[x][y]) continue;
		
		for(int k=0;k<4;k++)
		{
			int nx = x+delx[k];
			int ny = y+dely[k];
			if(nx<0 || nx>=R || ny<0 || ny>=C) continue;
			if(occ[nx][ny]) continue;
			
			LL nd = d;
			nd += abs(rall[nx] - rall[x]);
			nd += abs(call[ny] - call[y]);
			
			if(nd < dist[nx][ny])
			{
				dist[nx][ny] = nd;
				q.insert(tri(nd,nx,ny));
			}
		}
	}
	
	if(dist[dx][dy] == (LL)1e18) dist[dx][dy]=-1;
	printf("%lld\n",dist[dx][dy]);
}

void input()
{
	s(R);	s(C);	s(B);
	rall.pb(0);	rall.pb(R);
	call.pb(0);	call.pb(C);
	
	s(sx);	s(sy);
	s(dx);	s(dy);	
	rall.pb(sx);	call.pb(sy);
	rall.pb(dx);	call.pb(dy);
	
	for(int i=0;i<B;i++)
	{
		s(r1[i]);	s(c1[i]);
		s(r2[i]);	s(c2[i]);
		r2[i]++; 	c2[i]++;
		if(r1[i]>0)	rall.pb(r1[i]-1);	
		if(c1[i]>0)	call.pb(c1[i]-1);
		rall.pb(r1[i]);	call.pb(c1[i]);
		rall.pb(r2[i]);	call.pb(c2[i]);
	}
	
	DREP(rall);
	DREP(call);
	
	sx = INDEX(rall,sx);
	sy = INDEX(call,sy);
	dx = INDEX(rall,dx);
	dy = INDEX(call,dy);
	for(int i=0;i<B;i++)
	{
		r1[i] = INDEX(rall,r1[i]);
		c1[i] = INDEX(call,c1[i]);
		r2[i] = INDEX(rall,r2[i]);
		c2[i] = INDEX(call,c2[i]);
	}
	
	R = INDEX(rall,R);
	C = INDEX(call,C);
}


int main()
{
	input();
	solve();
}
