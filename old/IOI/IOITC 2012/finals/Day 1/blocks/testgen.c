#include <stdio.h>
#include <stdlib.h>
#include <time.h>

# define BMAX 400
# define NMAX 1e9

/*
 * sub1: 1<=M,N<=1e3, 0<=B<=20
 * sub2: 1<=M,N<=1e9, 0<=B<=20
 * sub3: 1<=M,N<=1e9, 0<=B<=400
 */

int
min(int a, int b) {
	if(a<b)
		return a;
	return b;
}

int inside (int px, int py, int ulx, int uly, int lrx, int lry) {
	if(ulx <= px && px <= lrx && uly <= py && py <= lry)
		return 1;
	
	return 0;
}

void
swap(int *a, int *b) {
	int t = *a;
	*a = *b;
	*b = t;
}

main() {
	unsigned int iseed = (unsigned int)time(NULL);
	srand (iseed);
	int m, n, b;
	int i;
	for(m = 1; m<=6; m++) {
		char name[10];
		sprintf(name, "sub3_%d.in", m);
		FILE *fin = fopen(name, "w");
		n=NMAX;
		b = BMAX;

		fprintf(fin, "%d %d %d\n", n, n-1, b);
		
		int bulx, buly, blrx, blry;
		int sx, sy, tx, ty;
		
		sx = rand()%(n/4);
		sy = rand()%(n/4);
		
		tx = 3*(n/4)+rand()%(n/4);
		ty = 3*(n/4)+rand()%(n/4);
		
		fprintf(fin, "%d %d\n%d %d\n", sx, sy, tx, ty);
		
		for(i=0; i<b;) {
			
			bulx = rand()%n;
			blrx = rand()%n;
			if(bulx>blrx) swap(&bulx, &blrx);
			
			buly = rand()%n;
			blry = rand()%n;
			if(buly>blry) swap(&buly, &blry);
			
			if((long long)((blrx-bulx+1)*(blry-buly+1))>(long long)(n*m))
				continue;
			if(inside(sx, sy, bulx, buly, blrx, blry)) {
				continue;
			}
			if(inside(tx, ty, bulx, buly, blrx, blry)) {
				continue;
			}
			fprintf(fin, "%d %d %d %d\n", bulx, buly, blrx,  blry);
			i++;
		}
		fclose(fin);
	}			
	return 0;
}
