#!/usr/bin/python3

M = 10**9 #rows
N = 10**9 #columns

B = 400

print(M, N, B)
print(0,0) #s
print(M-1, N-1) #t

block_height = ((M-2)//B) - 1

cur_height = 1

ca = (0, N-2)
cb = (1, N-1)

for b in range(B):
	print(cur_height, ca[0], cur_height + block_height - 1, ca[1])
	(ca, cb) = (cb, ca)
	cur_height += block_height + 1
