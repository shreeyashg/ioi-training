/*
Phase 1 : Analyse blocks, find barrier information
Phase 2 : Using this, build the graph.
Phase 3 : Find the shortest path.
*/

//#define SLOW_PR_QUEUE

#include <cstdio>
#include <map>
#include <algorithm>
#include <cassert>
#include <vector>
#include <limits>
#include <set>

using namespace std;

struct block
{
	int r1, r2, c1, c2;
	block(){}
	block(int r1_, int r2_, int c1_, int c2_) : r1(r1_), r2(r2_), c1(c1_), c2(c2_) {}
};

const int max_B = 500;
const int max_RC = 2*max_B+2;
const int max_V = max_RC*max_RC;

block blocks[max_B];

//from a given cell, is it prohibited to go in a given direction?
bool barrier[max_RC][max_RC][4];

inline bool in_range(int a, int A)
{
	return(0 <= a && a < A);
}

inline bool in_range(int r, int c, int R, int C)
{
	return in_range(r,R) && in_range(c,C);
}

struct edge
{
	int v, wt;
	edge(){}
	edge(int v_, int wt_) : v(v_), wt(wt_) {}
};

vector<edge> adjl[max_V];

long long d[max_V];

const long long inv = numeric_limits<long long>::max();

//initially everything is in the priority queue, then they are removed one by one
class prqueue
{
	int V; //elements [0,V)
	long long *pr;
	bool included[max_V];
	public:
	bool exists(int x) const {return included[x];}
	private:
#ifdef SLOW_PR_QUEUE
	int size;
	
	
	public:
	bool empty() const {return size==0;}
	prqueue(int V_, long long *pr_) : V(V_), pr(pr_), size(V) {fill(included, included+V, true);}
	
	int popmin()
	{
		assert(!empty());
		int ans = inv;
		for(int i=0; i<V; ++i)
			if(included[i] && (ans == inv || pr[i] < pr[ans]) )
				ans = i;
		included[ans] = false;
		--size;
		return ans;
	}
	
	void reduce_pr(int x, long long newpr) {assert(exists(x)); pr[x] = min(pr[x],newpr);}
	
	
	
#else
	struct less_pr : public binary_function<int, int, bool>
	{
		const long long * const pr;
		less_pr(const long long *pr_) : pr(pr_) {}
		bool operator()(int x, int y){return (pr[x] < pr[y] || (pr[x] == pr[y] && x < y));}
	};
	
	set<int, less_pr> S;
	
	public:
	
	prqueue(int V_, long long *pr_) : V(V_), pr(pr_), S(less_pr(pr))
	{
		for(int i=0; i<V; ++i)
		{
			S.insert(i);
			included[i] = true;
		}
	}
	bool empty() const {return S.empty();}
	
	int popmin()
	{
		assert(!empty());
		const set<int, less_pr>::iterator it = S.begin();
		const int ans = *it;
		S.erase(it);
		included[ans]=false;
		return ans;
	}
	
	void reduce_pr(int x, long long newpr)
	{
		if(newpr < pr[x])
		{
			set<int, less_pr>::iterator it = S.find(x);
			assert(it != S.end());
			S.erase(it);
			pr[x] = newpr;
			S.insert(x);
		}
	}
	
	
#endif
};

//bool burnt[max_V]={0};

int main()
{
	//Phase 1 :
	int M, N, B;
	scanf("%d %d %d", &M, &N, &B);
	
	int sr, sc, tr, tc;
	scanf("%d %d %d %d", &sr, &sc, &tr, &tc);
	
	for(int i=0; i<B; ++i)
	{
		scanf("%d %d %d %d", &blocks[i].r1, &blocks[i].c1, &blocks[i].r2, &blocks[i].c2);
		assert(!(blocks[i].r1 <= sr && sr <= blocks[i].r2 && blocks[i].c1 <= sc && sc <= blocks[i].c2));
		assert(!(blocks[i].r1 <= tr && tr <= blocks[i].r2 && blocks[i].c1 <= tc && tc <= blocks[i].c2));
	}
	
	
	//We will restrict attention to only those cells whose row is an "interesting row" and whose column is an "interesting column"
	//Note that some such cells may appear inside blocks
	int Rs[2*max_B]; //interesting rows
	int Cs[2*max_B]; //interesting columns
	
	int R=0, C=0; //number of interesting rows/columns
	Rs[R++] = sr;
	Rs[R++] = tr;
	Cs[C++] = sc;
	Cs[C++] = tc;
	for(int i=0; i<B; ++i)
	{
		if (blocks[i].r1 != 0) Rs[R++] = blocks[i].r1 - 1;
		if(blocks[i].r2 != M-1) Rs[R++] = blocks[i].r2+1;
		if(blocks[i].c1 != 0) Cs[C++] = blocks[i].c1 - 1;
		if(blocks[i].c2 != N-1) Cs[C++] = blocks[i].c2 + 1;
	}
	sort(Rs, Rs+R);
	R = unique(Rs, Rs+R) - Rs;
	sort(Cs, Cs+C);
	C = unique(Cs, Cs+C) - Cs;
	//Rs and Cs now have the correspondence between the original coordinates and the new (compressed) coordinates
	assert(R <= 2*B+2);
	assert(C <= 2*B+2);
	//map<int, int> Rsinv, Csinv; //inverse of the above correspondence
	//for(int i=0; i<R; ++i) Rsinv[Rs[i]] = i;
	//for(int i=0; i<C; ++i) Csinv[Cs[i]] = i;
	
	//Compress s,r:
	sr = lower_bound(Rs, Rs+R, sr) - Rs;
	sc = lower_bound(Cs, Cs+C, sc) - Cs;
	tr = lower_bound(Rs, Rs+R, tr) - Rs;
	tc = lower_bound(Cs, Cs+C, tc) - Cs;
	//The 4 directions:
	const int dr[4] = {1, -1, 0, 0};
	const int dc[4] = {0, 0, 1, -1};
	
	const int oppdir[4] = {1,0,3,2};
	
	//barriers are induced by the borders of the grid, as well as blocks. Let us process the borders first
	for(int r=0; r<R; ++r) //all rows
		for(int c=0; c<C; c+= (r==0 || r== R-1 ? 1 : C-1)) //all edge cells in row r
			for(int z=0; z<4; ++z)
			{
				int rr = r + dr[z], cc = c + dc[z];
				if(!in_range(rr,cc,R,C))
					barrier[r][c][z] = true;
			}
	//Now the blocks:
	
	//Do not write code 4 times, instead parametrise everything by direction and write a loop over the 4 directions!
	//From inside a block, going outward in direction z affects coordinate coord[z]
	const int block::*coord[4] = { &block::r2, &block::r1, &block::c2, &block::c1} ;
	const int block::*othercoord1[4] = { &block::c1, &block::c1, &block::r1, &block::r1};
	const int block::*othercoord2[4] = { &block::c2, &block::c2, &block::r2, &block::r2};
	const int RC[4] = {R, R, C, C};
	const int otherRC[4] = {C, C, R, R};
	const int MN[4] = {M, M, N, N};
	const int otherMN[4] = {N, N, M, M};
	
	const int * const RsCs[4] = {Rs, Rs, Cs, Cs};
	const int * const otherRsCs[4] = {Cs, Cs, Rs, Rs};
	
	for(int i=0; i<B; ++i) //this block
	{
		for(int z=0; z<4; ++z) //this direction
		{
			const block b = blocks[i];
			//Look at all the cells in direction z of block b, and mark the barrier in direction oppdir[z]
			if(in_range(b.*coord[z] + dr[z] + dc[z], MN[z])) //these cells exist
			{
				const int thisidx = b.*coord[z] + dr[z] + dc[z];
				const int othidx1= b.*othercoord1[z], othidx2 = b.*othercoord2[z];
				assert(in_range(othidx1, otherMN[z]));
				assert(in_range(othidx2, otherMN[z]));
				const int comp_thisidx = lower_bound(RsCs[z], RsCs[z] + RC[z], thisidx) - RsCs[z];
				assert(comp_thisidx < RC[z] && RsCs[z][comp_thisidx] == thisidx);
				const int comp_othidx1 = lower_bound(otherRsCs[z], otherRsCs[z] + otherRC[z], othidx1) - otherRsCs[z];
				const int comp_othidx2 = upper_bound(otherRsCs[z], otherRsCs[z] + otherRC[z], othidx2) - otherRsCs[z]-1;
				for(int y = comp_othidx1; y<= comp_othidx2; ++y)
					((z==0 || z==1) ?  barrier[comp_thisidx][y] : barrier[y][comp_thisidx])[oppdir[z]] = true;
			}
		}
	}
// 	for(int i=0; i<R; ++i)
// 	{
// 		for(int j=0; j<C; ++j)
// 			printf(barrier[i][j][0]?"*":".");
// 		printf("\n");
// 	}
	
	//*************************************************
	//Phase 2:
	
	//map (r,c) to C*r + c so that vertices are represented by i;
	const int V = R*C;
	for(int r=0; r<R; ++r)
	for(int c=0; c<C; ++c)
	{
		for(int z=0; z<4; ++z)
			if(!barrier[r][c][z] && !barrier[r+dr[z]][c+dc[z]][oppdir[z]])
				adjl[C*r+c].push_back(edge(C*(r+dr[z]) + c+dc[z], abs(Rs[r] - Rs[r+dr[z]]) + abs(Cs[c] - Cs[c+dc[z]])));
	}
	
	const int s = C*sr + sc, t = C*tr + tc;
	
	
	/*fprintf(stderr, "s = %d, t = %d\n", s, t);
	for(int i=0; i<V; ++i)
	{
		fprintf(stderr, "Vertex %d\n", i);
		for(int t=0; t<adjl[i].size(); ++t)
			fprintf(stderr, "    %d %d\n", adjl[i][t].v, adjl[i][t].wt);
	}*/
	
	//*************************************************
	//Phase 3:
	
	fill(d, d+V, inv);
	d[s] = 0;
	prqueue q(V, d);
	while(!q.empty())
	{
		const int minv = q.popmin();
		if(d[minv] == inv)
			break;
		for(int t=0; t<adjl[minv].size(); ++t)
		{
			const edge nbr = adjl[minv][t];
			if(q.exists(nbr.v))
				q.reduce_pr(nbr.v, d[minv] + nbr.wt);
		}
	}
	
	/*for(int i=0; i<V; ++i)
		fprintf(stderr, "d[%d] = %lld\n", i, d[i]);*/
	
	if(d[t] == inv) d[t] = -1;
	printf("%lld\n", d[t]);
}
