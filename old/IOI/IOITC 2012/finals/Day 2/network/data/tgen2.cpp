#include<iostream>
#include<cstdio>
#include<sstream>
#include "MersenneTwister.h"

using namespace std;

bool visited[2001][2001]={0};

int main(int argc, char **argv)
{
	if(argc!=5)
	{
	  fprintf(stderr, "%s : Wrong format for command-line arguments.",argv[0]);
	  return 1;
	}
	
	int n,xylim,rlim,seed;
	istringstream is(string(argv[1]) + " " + string(argv[2]) + " " + string(argv[3]) +  " " + string(argv[4]));
	is>>n>>xylim>>rlim>>seed;
	MTRand R(seed);
	
	printf("%d\n",n);
	for(int i=0; i<n; ++i)
	{
		int p=R.randInt(1000)+4000;
		int r=R.randInt(rlim-1)+1;
		int x=R.randInt(2*xylim)-xylim;
		int y=R.randInt(2*xylim)-xylim;
		while(visited[x+xylim][y+xylim])
		{
			x=R.randInt(2*xylim)-xylim;
			y=R.randInt(2*xylim)-xylim;
		}
		printf("%d %d %d %d\n",p,r,x,y);
		visited[x+xylim][y+xylim]=true;
	}
	
	for(int z=0; z<2; ++z)
	{
		int x=R.randInt(2*xylim)-xylim;
		int y=R.randInt(2*xylim)-xylim;
		while(visited[x+xylim][y+xylim])
		{
			x=R.randInt(2*xylim)-xylim;
			y=R.randInt(2*xylim)-xylim;
		}
		printf("%d %d\n",x,y);
		visited[x+xylim][y+xylim]=true;
	}
	
	return 0;
}
