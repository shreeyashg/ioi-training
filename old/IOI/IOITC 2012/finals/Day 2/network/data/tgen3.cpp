#include<iostream>
#include<cstdio>
#include<sstream>
#include<string>
#include "MersenneTwister.h"

using namespace std;

bool visited[2001][2001]={0};

int main(int argc, char **argv)
{
	if(argc!=2)
	{
	  fprintf(stderr, "%s : Wrong format for command-line arguments.",argv[0]);
	  return 1;
	}
	
	int seed;
	istringstream is(  (string(argv[1]) ));
	is>>seed;
	MTRand R(seed);
	
	int n=1000, xylim=1000;
	printf("%d\n",n);
	
	visited[0][0]=true; visited[2000][2000]=true;
	for(int i=0; i<n; ++i)
	{
		int p,r,x,y;
		int dec=R.randInt(1);
		if(dec)
		{
			p=R.randInt(100)+400;
			r=1;
			x=R.randInt(200)+800;
			y=R.randInt(200)+800;
			while(visited[x+xylim][y+xylim])
			{
				x=R.randInt(200)+800;
				y=R.randInt(200)+800;
			}
			printf("%d %d %d %d\n",p,r,x,y);
			visited[x+xylim][y+xylim]=true;
		}
		else
		{
			p=R.randInt(100)+400;
			r=1;
			x=R.randInt(200)-1000;
			y=R.randInt(200)-1000;
			printf("%d %d %d %d\n",p,r,x,y);
			visited[x+xylim][y+xylim]=true;
		}
	}
	
	printf("-1000 -1000\n");
	printf("1000 1000\n");
	
	return 0;
}
