# include <cstdio>
# include <iostream>
# include <algorithm>
# include <vector>
# include <cstring>
# include <cctype>
# include <set>
# include <map>
# include <cmath>
# include <queue>
# include <string>

using namespace std;

const int NMAX=1000;
const long long PMAX=5000;

struct two
{
	int v;
	long long w;
	
	two(int a=0,long long b=0)
	{
		v=a,w=b;
	}
};

bool operator<(const two& t1,const two& t2)
{
	if(t1.w!=t2.w)return t1.w>t2.w;
	return t1.v>t2.v;
}

vector<two> adjlist[NMAX+2];
set<two> djset;

long long x[NMAX+2],y[NMAX+2],p[NMAX+2],r[NMAX+2];
long long bestpow[NMAX+2];
char seen[NMAX+2];

inline long long sq(long long sth)
{
	return sth*sth;
}

int main()
{
	int N;
	scanf("%d",&N);
	
	for(int i=1;i<=N;i++)
		scanf("%Ld%Ld%Ld%Ld",p+i,r+i,x+i,y+i);
	p[0]=p[N+1]=0;
	r[0]=r[N+1]=1;
	
	scanf("%Ld%Ld%Ld%Ld",x,y,x+N+1,y+N+1);
	
	for(int i=0;i<N+2;i++)
		for(int j=i+1;j<N+2;j++)
		{
			int u=i,v=j;
			if(p[u]<p[v])swap(u,v);
			const long long dsq=sq(x[u]-x[v])+sq(y[u]-y[v]);
			
			if(sq(p[u]*r[v]+p[v]*r[u])<sq(r[u]*r[v])*dsq)continue;
			
			long long start=0,end=p[u]+1,mid;
			
			while(start<end-1)
			{
				mid=(start+end)>>1;
				if(sq(r[v]*max(p[u]-mid,0ll)+r[u]*max(p[v]-mid,0ll))>=sq(r[u]*r[v])*dsq)start=mid;
				else	end=mid;
			}
			if(start==0)continue;
			adjlist[u].push_back(two(v,start));
			adjlist[v].push_back(two(u,start));
		}
	
	bestpow[0]=PMAX;
	for(int i=0;i<N+2;i++)
		djset.insert(two(i,bestpow[i]));
	
	while(!djset.empty())
	{
		int u=djset.begin()->v;
		long long w=djset.begin()->w;
		djset.erase(djset.begin());
		seen[u]=1;
		
		for(int i=0;i<adjlist[u].size();i++)
		{
			int v=adjlist[u][i].v;
			long long z=adjlist[u][i].w;
			if(seen[v]||bestpow[v]>=min(w,z))continue;
			djset.erase(two(v,bestpow[v]));
			bestpow[v]=min(w,z);
			djset.insert(two(v,min(w,z)));
		}
	}
	
	printf("%Ld\n",bestpow[N+1]);
	
	return 0;
}
