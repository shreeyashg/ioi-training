#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<queue>
#include<algorithm>
#include<cmath>

using namespace std;

typedef long long LL;

const int max_n=1000;
const int max_pi=5000;

int n;
int p[max_n+2];
int r[max_n+2];
int x[max_n+2];
int y[max_n+2];

//int rec[max_n+2][max_n+2];

struct edge
{
	int eu,ev,rec;
	edge(){};
	edge(int cu, int cv, int crec){eu=cu; ev=cv; rec=crec;}
};

edge edges[(max_n+2)*(max_n+1)/2];
int nedges=0;

struct more_edge_rec : public binary_function <edge,edge,bool>
{
	bool operator()(const edge &a, const edge &b){return a.rec>b.rec;}
};

void findrec(int u, int v)
{
	//cerr<<u<<" "<<v<<endl;
	long double pu=((long double)(p[u])), ru=((long double)(r[u])), xu=((long double)(x[u])), yu=((long double)(y[u]));
	long double pv=((long double)(p[v])), rv=((long double)(r[v])), xv=((long double)(x[v])), yv=((long double)(y[v]));
	long double duv=sqrt((xu-xv)*(xu-xv)+(yu-yv)*(yu-yv));
	
	if(pu-ru*duv>pv)
	{
		edges[nedges]=edge(u,v, (int)(floor(pu-ru*duv)) );
		++nedges;
	}
	else if(pv-rv*duv>pu)
	{
		edges[nedges]=edge(u,v, (int)(floor(pv-rv*duv)) );
		++nedges;
	}
	else
	{
		long double x=((pu+rv*duv-pv)/(ru+rv));
		if(pu-ru*x>0)
		{
			edges[nedges]=edge(u,v, (int)(floor(pu-ru*x)) );
			++nedges;
		}
		else
		{
			edges[nedges]=edge(u,v, 0 );
			++nedges;
		}
	}
	
	return;
}

int head[max_n+2];
int rank[max_n+2];

int findhead(int vt)
{
	if(head[vt]!=vt)
		head[vt]=findhead(head[vt]);
	return head[vt];
}

void unify(int h1, int h2)
{
	if(rank[h1]>rank[h2])
	{
		head[h2]=h1;
	}
	else if(rank[h1]<rank[h2])
	{
		head[h1]=h2;
	}
	else
	{
		head[h2]=h1;
		++rank[h1];
	}
}

int main()
{
	scanf("%d",&n);
	
	for(int i=2; i<n+2; ++i)
	{
		scanf("%d %d %d %d",&p[i],&r[i],&x[i],&y[i]);
	}
	
	p[0]=0; r[0]=1;
	scanf("%d %d",&x[0],&y[0]);
	
	p[1]=0; r[1]=1;
	scanf("%d %d",&x[1],&y[1]);
	
	for(int i=0; i<n+2; ++i)
	{
		for(int j=0; j<i; ++j)
		{
			if(i!=j)
				findrec(i,j);
		}
	}
	
	sort(edges,edges+nedges,more_edge_rec());
	
	for(int i=0; i<n+2; ++i)
	{
		head[i]=i;
		rank[i]=0;
	}
	
	for(int i=0; i<nedges; ++i)
	{
		int u=edges[i].eu, v=edges[i].ev;
		if(findhead(u)!=findhead(v))
		{
			unify(head[u],head[v]);
		}
		if(findhead(0)==findhead(1))
		{
			printf("%d\n",edges[i].rec);
			return 0;
		}
	}
}

