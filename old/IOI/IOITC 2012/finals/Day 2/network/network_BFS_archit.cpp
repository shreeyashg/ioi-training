#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<queue>
#include<algorithm>

using namespace std;

typedef long long LL;

const int max_n=1000;
const int max_pi=5000;

int n;
int p[max_n+2];
int r[max_n+2];
int x[max_n+2];
int y[max_n+2];

long long sq(int a)
{
	if(a<0)
		return 0;
	else
		return ((LL)(a))*((LL)(a));
}

long long sqr(int a)
{
	return ((LL)(a))*((LL)(a));
}

long long f(int a)
{
	if(a<0)
		return 0;
	else
		return ((LL)(a));
}

bool route(int i, int j, int rec)
{
	if(rec==0)
		return true;
	
	//cout<<"@ : "<<sq(p[j]-rec)*sqr(r[i])<<" "<<(sqr(x[i]-x[j])+sqr(y[i]-y[j]))*sqr(r[i])*sqr(r[j])<<endl;
		
	return ( ( sq(p[i]-rec)*sqr(r[j]) + ((LL)(2))*f(p[i]-rec)*f(p[j]-rec)*f(r[i])*f(r[j]) + sq(p[j]-rec)*sqr(r[i]) ) >= ( (sqr(x[i]-x[j])+sqr(y[i]-y[j]))*sqr(r[i])*sqr(r[j]) ) ); 
}

bool visited[max_n+2];
queue<int> Q;
bool possible(int rec)
{
	fill(visited,visited+n+2,false);
	
	visited[0]=true;
	Q.push(0);
	
	int vt;
	while(!Q.empty())
	{
		vt=Q.front(); Q.pop();
		
		for(int i=0; i<n+2; ++i)
		{
			if((not visited[i]) and route(vt,i,rec))
			{
				visited[i]=true;
				Q.push(i);
			} 
		}
	}
	
	return visited[1];
}

int binsrc(int l, int r) //[l,r)
{	
	if(l+1==r)
		return l;
	else
	{
		int mid=(l+r)/2;
		if(possible(mid))
			return binsrc(mid,r);
		else
			return binsrc(l,mid);
	}
}

int main()
{
	scanf("%d",&n);
	
	for(int i=2; i<n+2; ++i)
	{
		scanf("%d %d %d %d",&p[i],&r[i],&x[i],&y[i]);
	}
	
	p[0]=0; r[0]=1;
	scanf("%d %d",&x[0],&y[0]);
	
	p[1]=0; r[1]=1;
	scanf("%d %d",&x[1],&y[1]);
	
	printf("%d\n",binsrc(0,max_pi+1));
	
	//cout<<route(1,2,54)<<" "<<route(1,2,55)<<endl;
	/*for(int i=0; i<=100; ++i)
	{
		cout<<i<<" "<<route(0,1,i)<<" "<<route(0,2,i)<<" "<<route(1,2,i)<<endl;
	}*/
	
	return 0;
}

