//Written by Prateek

#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>
#include <cctype>

const bool logging=true;


void solve(long long R, long long C, long long br, long long bc, 
    int &ans_N, char *ans_digits, long long &ans_r, long long &ans_c); //This function will be written by the user

namespace{

#include "digits-common.h"

using namespace std;
typedef long long ll;

const int max_N = 100;
const int max_read = 120;

char digits_char[max_read];
int digits[max_N];

const int BAD_INPUT = 10;
const int CORRECT_ANSWER = 0;
const int INVALID_QUERY = 0;
const int TOO_MANY_QUERIES = 0;

FILE *flog;

const ll maxR=1000LL*1000*1000*1000*1000*1000;
const ll maxC=1000LL*1000*1000*1000*1000*1000;
const ll minR=5;
const ll minC=3;
const ll minfont=1;
const ll maxfont=1000LL*1000*1000*1000*1000*1000;

int N;
ll R, C; //size of the grid
ll font;
ll tlr, tlc; //top-left corner of display rectangle
ll dispR, dispC; //number of rows and columns in the display rectangle
ll br, bc;

const int maxQ=5000;
int Q=0;

void check_range(const char *name, ll var, ll minvar, ll maxvar)
{
	if(!(minvar <= var && var <= maxvar)) {
		fprintf(stderr, "Bad input: %s = %lld is not in the range [%lld, %lld].\n", name, var, minvar, maxvar);
		exit(BAD_INPUT);
	}
}

bool is_black(ll r, ll c)
{
	//Is this cell in the display rectangle?
	if(!(tlr <= r && r < tlr + dispR))
		return false;
	if(!(tlc <= c && c < tlc + dispC))
		return false;
	
	//Find which-th digit this pixel could possibly belong to
	const ll i = (c - tlc)/(font*(gap+CD));
	
	//Within the area occupied by the digit and the gap after it, where is this pixel?
	const ll zr = (r - tlr)/font, zc = ((c - tlc)/font)%(gap+CD);
	
	//Is it actually in the digit area (columns [0,CD) ), or the gap area (columns [CD, CD+gap) )?
	if(zc >= CD)
		return false;
	
	assert(0 <= zr && zr < RD && 0 <= zc && zc < CD);
	return dbit[digits[i]][zr][zc];
}

//Want to check a*b > c, avoiding overflow, with a,b,c > 0
inline bool check_product(ll a, ll b, ll c)
{
	return( (a > c/b + 1) || (b > c/a + 1) || (a*b > c));
}

void read_and_validate_input()
{
	fgets(digits_char, max_read, stdin);
	N = 0;
	while(digits_char[N] != 0 && digits_char[N] != '\n') //string terminated by null or newline
		++N;
	
	check_range("number of digits", N, 1, max_N);
	if(logging) fprintf(flog, "Read a sequence of %d digits.\n", N);
	
	for(int i=0; i<N; ++i) {
		if(!isxdigit(digits_char[i])) {
			fprintf(stderr, "Bad input: The first line of input has a non-hex-digit.\n");
			exit(BAD_INPUT);
		}
		digits[i] = val[digits_char[i]];
	}
	
	scanf("%lld %lld", &R, &C);
	check_range("R", R, minR, maxR);
	check_range("C", C, minC, maxC);
	if(logging) fprintf(flog, "Read number of rows, %lld, and number of columns, %lld.\n", R, C);
	
	scanf("%lld %lld", &tlr, &tlc);
	check_range("leftmost row of display rectangle", tlr, 0, R-1);
	check_range("topmost column of display rectangle", tlc, 0, C-1);
	if(logging) fprintf(flog, "Read coordinates of top-left corner of display rectangle, (%lld, %lld).\n", tlr, tlc);
	
	scanf("%lld", &font);
	check_range("font size", font, minfont, maxfont);
	if(logging) fprintf(flog, "Read font size, %lld.\n", font);
	
	scanf("%lld %lld", &br, &bc);
	check_range("row coordinate of black pixel", br, 0, R-1);
	check_range("column coordinate of black pixel", bc, 0, C-1);
	
	//In the comments, we have taken CD = 3 and gap = 3
	//want to check if tlc + (6*N*font - 3*font) <= C - but 6*N*font might overflow long long
	//equivalent to 6*N*font <= C + 3*font - tlc
	if(  check_product((gap+CD)*N, font, C + gap*font -tlc) || (tlr + font*RD > R)) {
		fprintf(stderr, "Bad input: Display rectangle does not fit on the screen.\n");
		exit(BAD_INPUT);
	}
	if(logging) fprintf(flog, "Read coordinates of black pixel, (%lld, %lld).\n", br, bc);
	
	dispR = font*RD;
	dispC = (gap+CD)*N*font - gap*font;
	
	if(!is_black(br, bc)) {
		fprintf(stderr, "Bad input: Alleged black pixel (%lld, %lld) is not actually black.\n", br, bc);
		exit(BAD_INPUT);
	}
	
}

void call_solve_and_check_answer()
{
	int ans_N;
	char ans_digits[max_N];
	ll ans_r;
	ll ans_c;
	if(logging) fprintf(flog, "Calling solve.\n");
	solve(R, C, br, bc, ans_N, ans_digits, ans_r, ans_c);
	if(logging) fprintf(flog, "solve returned.\n");
	if(N != ans_N) {
		printf("Wrong answer: The number of digits reported is %d, while the actual number of digits is %d.\n", ans_N, N);
		exit(0);
	}
	
	for(int i=0; i<N; ++i)
		if(tolower(digits_char[i]) != tolower(ans_digits[i])) {
			printf("Wrong answer: Digit %d is reported as %c, while the actual digit is %c.\n", i, tolower(ans_digits[i]), tolower(digits_char[i]));
			exit(0);
		}
	
	if(ans_r != tlr || ans_c != tlc) {
		printf("Wrong answer: The top-left corner of the display rectangle is reported as (%lld, %lld), while the actual top-left corner is (%lld, %lld).\n", ans_r, ans_c, tlr, tlc);
		exit(0);
	}
	//fprintf(stderr, "%d queries made\n", Q);
	if(logging) fprintf(flog, "Correct answer received from solve, program is terminating.\n");
	printf("CORRECT!\n");
}


} //end unnamed namespace

bool pixel(ll r, ll c)
{
	++Q;
	if(logging) fprintf(flog, "Received query #%d, (%lld, %lld).\n", Q, r, c);
	if(Q > maxQ) {
		printf("The limit of %d queries has been exceeded.\n", maxQ);
		exit(TOO_MANY_QUERIES);
	}
	
	if( r < 0 || r >= R || c < 0 || c >= C) {
		printf("Bad query : The pixel (%lld, %lld) lies outside the screen, [0,%lld) x [0,%lld).\n", r, c, R, C);
		exit(INVALID_QUERY);
	}
	const bool ret_ans = is_black(r,c);
	if(logging) fprintf(flog, "Returning answer to query #%d, (%lld, %lld), \"%s\".\n", Q, r, c, ret_ans?"true":"false");
	return ret_ans;
}


int main()
{
	if(logging)
	{
		flog = fopen("digits.log", "w");
		fprintf(flog, "Started digits.\n");
	}

	init();
	read_and_validate_input();
	call_solve_and_check_answer();
}
