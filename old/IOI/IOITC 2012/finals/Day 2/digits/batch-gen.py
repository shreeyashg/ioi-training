#!/usr/bin/python3

# Reads test case descriptions, and calls the C++ generator
# The description of each test case begins with a line '### filename'
# This is followed by 4 or 6 or 7 lines:
#
# Sequence of digits, or the word "random" followed by the number of digits and a seed
# R C //The word 'largest' means 10**18, 'smallest' means the size of the display rectangle
# top-left corner of display rectangle. Keywords 'top-left', 'top-right', 'bottom-left', 'bottom-right' are recognised
# Font size
# Index of the digit with the initial black cell , must be omitted for 'random'. "find x" can be used to mean the first index where digit x occurs. x must occur in the sequence of digits.
# Location of the block within the digit, with the initial black cell, must be omitted for 'random'
# Location of the black cell within the block. Can be omitted if the font size is 1. Keywords 'top-left', 'top-right', 'bottom-left', and 'bottom-right' are recognised.


import sys, subprocess

lines = sys.stdin.readlines()

BIG = 10**18

# Strip out comments:

def stripcomment(s):
	idx = s.find('//')
	if idx == -1:
		return s
	return s[0:idx]

lines = [stripcomment(l).strip() for l in lines]
lines = [l for l in lines if l != '']

assert(lines[0][:3] == '###')

# Each test case begins with a line beginning with ###

cases = []

for l in lines:
	if l[:3] == '###': #Begin new testcase
		cases.append([l])
	else:
		cases[-1].append(l) #Append to existing testcase

for c in cases:
	filename = c[0].split()[1]
	
	
	firstsplit = c[1].split()
	if not( (len(firstsplit) == 1) or (len(firstsplit) == 3 and firstsplit[0] == "random")):
		print("First line of test case description for " + filename + " is incorrectly formatted.", file=sys.stderr)
		sys.exit(1)
	
	israndom = firstsplit[0] == "random"
	if not ((israndom and len(c) == 5) or ( (not israndom) and len(c) in [7,8])):
		print("Test case description for " + filename + " does not have the right number of lines.", file=sys.stderr)
		sys.exit(1)
		
	if(len(firstsplit) == 1):
		N = len(firstsplit[0])
	else:
		N = int(firstsplit[1])
	
	font = int(c[4])
	dispR = 5*font
	dispC = 6*font*N - 3*font
	
	RCsplit = c[2].split()
	if len(RCsplit) == 1:
		RCsplit.append(RCsplit[0])
	if RCsplit[0] == 'largest':
		RCsplit[0] = str(BIG)
	if RCsplit[0] == 'smallest':
		RCsplit[0] = str(dispR)
	if RCsplit[1] == 'largest':
		RCsplit[1] = str(BIG)
	if RCsplit[1] == 'smallest':
		RCsplit[1] = str(dispC)
	c[2] = ' '.join(RCsplit)
	
	(R,C) = (int(RCsplit[0]), int(RCsplit[1]))
	assert(dispR <= R and dispC <= C) #Sanity check only, does not ensure display rectangle will fit!
	if c[3] == 'top-left':
		c[3] = '0 0'
	elif c[3] == 'top-right':
		c[3] = '0 ' + str(C - dispC)
	elif c[3] == 'bottom-left':
		c[3] = str(R - dispR) + ' 0'
	elif c[3] == 'bottom-right':
		c[3] = str(R - dispR) + ' ' + str(C - dispC)
	
	if not israndom:
		if c[5][:4] == 'find':
			c[5] = str(c[1].index(c[5][5:].strip()))
	
	if len(c) == 7:
		if font == 1:
			c.append('0 0')
		else:
			print("Test case description for " + filename + " does not have the right number of lines.", file=sys.stderr)
	
	fullcase = '\n'.join(c[1:])
	
	outfile = open(filename, 'w')
	subp = subprocess.Popen('./digits-gen', stdin = subprocess.PIPE, stdout = outfile)
	subp.communicate(input = fullcase.encode())
	if subp.wait() == 0:
		print('File ' + filename + ' generated')
	else:
		sys.exit(1)
	outfile.close()
