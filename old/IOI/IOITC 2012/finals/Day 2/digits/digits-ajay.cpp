#include<iostream>
#include<cmath>
#include<string>
using namespace std;
long long R1,C1;
bool pixel(long long i , long long j);
bool pixel2(long long i, long long j)
{
    if (i<0 || j<0 || i>=R1 || j>=C1) return false;
    else return pixel(i,j);
}
//bool display[1000][1000];
long long k;
long long t;
//long long R,C;
/*bool pixel(long long i , long long j)
{
    if (i<0 || j<0 || i>=R || j>=C) return false;
    return display[i][j];
}*/
long long rightc;
long long vspan(int dx, long long br, long long bc,
            long long R)
{
    long long g=0;
    long long nr=br;
    while(true)
    {
        nr+=dx*(1<<g);
        if (0<=nr && nr<R && pixel2(nr,bc))
        {
            g++;
        }
        else
        {
            break;
        }
    }
    if (nr<0)
    {
        nr=0;
        if (pixel2(nr,bc)) return (br+1);
    }
    else if (nr>=R)
    {
        nr=R-1;
        if (pixel2(nr,bc)) return (R-br);
    }
    long long lo=br;
    long long hi=nr;
    while(lo!=hi)
    {
        long long mid=(lo+hi)/2;
        if (lo==mid || hi==mid) hi=lo;
        else if (pixel2(mid,bc)) lo=mid;
        else hi=mid;
    }
    return (max(lo-br,br-lo)+1);

}
long long hspan(int dy, long long br, long long bc,
            long long C)
{
    long long g=0;
    long long nc=bc;
    while(true)
    {
        nc+=dy*(1<<g);
        if (0<=nc && nc<C && pixel2(br,nc))
        {
            g++;
        }
        else
        {
            break;
        }
    }
    if (nc<0)
    {
        nc=0;
        if (pixel2(br,nc)) return (bc+1);
    }
    else if (nc>=C)
    {
        nc=C-1;
        if (pixel2(br,nc)) return (C-bc);
    }
    long long lo=bc;
    long long hi=nc;
    while(lo!=hi)
    {
        long long mid=(lo+hi)/2;
        if (lo==mid || hi==mid) hi=lo;
        else if (pixel2(br,mid)) lo=mid;
        else hi=mid;
    }
    return (max(lo-bc,bc-lo)+1);

}
char digit(long long, long long, bool);
char fdigit(long long R, long long C, long long br, long long bc)
{
    long long lspan=hspan(-1,br,bc,C);
    long long rspan=hspan(1,br,bc,C);
    long long uspan=vspan(-1,br,bc,R);
    long long dspan=vspan(1,br,bc,R);
    long long w=lspan+rspan-1;
    long long h=uspan+dspan-1;
   // cout << w << " " << h << endl;
    if (h==5*w)
    {
        t=br-uspan+1;
        k=w;
        bc=bc+rspan-1;
        if (bc+2*k<C && (pixel2(t,bc+2*k) || pixel2(t+5*k-1,bc+2*k)))
        {
            rightc=bc+2*k;
            return digit(t+5*k-1,rightc,false);
        }
        else
        {
            rightc=bc;
            return digit(t+5*k-1,rightc,false);
        }
    }
    if (w==3*h)
    {
        k=h;
        rightc=bc+rspan-1;
        br=br-uspan+1;
        if (br-4*k>=0 && (pixel2(br-4*k,rightc-2*k) || pixel2(br-4*k,rightc)))
        {
            t=br-4*k;
            return digit(t+5*k-1,rightc,false);
        }
        else if (br+5*k-1<R &&(pixel2(br+5*k-1,rightc-2*k) || pixel2(br+5*k-1,rightc)))
        {
            t=br;
            return digit(t+5*k-1,rightc,false);
        }
        else
        {
            t=br-2*k;
            return digit(t+5*k-1,rightc,false);
        }

    }
    if (5*w==3*h)
    {
        t=br-uspan+1;
        k=w/3;
        rightc=bc+rspan-1;
        return digit(t+5*k-1,rightc, false);
    }
    if (w==h)
    {
        k=w/3;
        rightc=bc+rspan-1;
        br=br-uspan+1;
        if (br-4*k>=0 && (pixel2(br-4*k,rightc-2*k) || pixel2(br-4*k,rightc)))
        {
            t=br-4*k;
            return digit(t+5*k-1,rightc,false);
        }
        else if (br+5*k-1<R &&(pixel2(br+5*k-1,rightc-2*k) || pixel2(br+5*k-1,rightc)))
        {
            t=br;
            return digit(t+5*k-1,rightc,false);
        }
        else
        {
            t=br-2*k;
            return digit(t+5*k-1,rightc,false);
        }

    }
    if (h==3*w)
    {
        k=w;
        br=br-uspan+1;
        bc=bc+rspan-1;
        if (pixel2(br+5*k-1,bc-2*k))
        {
            t=br;
            rightc-bc;
            return digit(t+5*k-1,rightc,false);

        }
        else if (pixel2(br+5*k-1,bc+2*k))
        {
            t=br;
            rightc=bc+2*k;
            return digit(t+5*k-1,rightc,true);
        }
        else if (pixel2(br+3*k-1,bc-2*k))
        {
            rightc=bc;
            t=br=2*k;
            return digit(t+5*k-1,rightc,true);
        }
        else
        {
            rightc=bc+2*k;
            t=br-2*k;
            return digit(t+5*k-1,rightc,true);
        }
    }
    //rightc = 5;
   // t=0;
   // return 'a';

}
char digit(long long br, long long bc, bool checked)
{
    if (!checked)
    {
        if (!pixel2(br,bc)) return 'f';
    }
    long long r0=br-4*k;
    long long c0=bc-2*k;
    if (pixel2(r0+3*k,c0))
    {

        if (pixel2(r0+k,c0+2*k))
        {
            if (pixel2(r0+k,c0))
            {
                if (pixel2(r0+4*k,c0+k))
                {
                    if (pixel2(r0+2*k,c0+k)) return '8';
                    else return '0';
                }
                else
                {
                    return 'a';
                }
            }
            else
            {
                if(pixel2(r0,c0)) return '2';
                else return 'd';
            }
        }
        else
        {
            if (pixel2(r0+3*k,c0+2*k))
            {
                if (pixel2(r0,c0+2*k)) return '6' ;
                else return 'b';
            }
            else
            {
                if(pixel2(r0+2*k,c0+2*k)) return 'e' ;
                else return 'c';
            }
        }
    }
    else
    {
        if (pixel2(r0+k,c0))
        {
            if (pixel2(r0,c0+k))
            {
                if(pixel2(r0+k,c0+2*k)) return '9' ;
                else return '5';
            }
            else
            {
                return '4';
            }
        }
        else
        {
            if (pixel2(r0,c0))
            {
                if(pixel2(r0+2*k,c0)) return '3' ;
                else return '7';
            }
            else
            {
                return '1';
            }
        }
    }
}
void solve(long long R, long long C, long long br, long long bc,
            int &ans_N, char *ans_digits, long long &ans_r, long long &ans_c)
{
    R1=R;
    C1=C;
    int curr=0;
    char right[10000];
    char left[10000];
   // assert(pixel2(br,bc)==true);
    while(true)
    {
        if (curr==0) right[curr]=fdigit(R,C,br,bc);
        else
        {
            long long nrc=rightc+6*k;
            if (nrc>=C) break;
            else if (pixel2(t+5*k-1,nrc))
            {
                right[curr]=digit(t+5*k-1,nrc,true);
                rightc+=6*k;
            }
            else if (pixel2(t+5*k-1,rightc+4*k))
            {
                right[curr]='f';
                rightc+=6*k;
            }

            else break;
        }


        curr++;
    }
    int curr2=0;
    rightc-=(curr-1)*6*k;
    while(true)
    {
        if (curr2==0) left[curr2]=right[0];
        else
        {
            long long nrc=rightc-6*k;
            if (nrc-3*k+1<0) break;
            else if (pixel2(t+5*k-1,nrc))
            {
                left[curr2]=digit(t+5*k-1,nrc,true);
                rightc-=6*k;
            }
            else if (pixel2(t+5*k-1,rightc-9*k+1))
            {
                left[curr2]='f';
                rightc-=6*k;
            }

            else break;
        }

        curr2++;

    }
   // cout << curr << " " << curr2 << endl;
    ans_N=curr+curr2-1;
    ans_r=t;
    ans_c=rightc-3*k+1;
    for (int i=curr2-1;i>=0;i--)
    {


        ans_digits[curr2-1-i]=left[i];
      //  cout << left[i];
    }


    for (int i=1;i<curr;i++)
    {


        ans_digits[curr2-1+i]=right[i];
     //   cout << right[i];
    }
    //cout << ans_N << " " << ans_r << " " << ans_c;
    return;
}
/*int main()
{
        //long long R,C;
        R=11;
        C=54;
        string ch;
        int ans_N;
        char *ans_digits=(char *)malloc(1000*sizeof(char));
        long long ans_r, ans_c;

        for (int i=0;i<R;i++)
        {
            cin >> ch;
           // cout << ch << endl;
            for (int j=0;j<C;j++)
            {

                if (ch[j]=='1') display[i][j]=true;
                else display[i][j]=false;
            }
        }
        solve(R,C,1,17,ans_N,ans_digits,ans_r,ans_c);
}*/
