#include <cassert>
#include <cstdio>
#include <utility>
#include "digits-common.h"

using namespace std;

bool pixel(long long r, long long c);

const int max_N = 100;

int temp_ans[2*max_N + 1];

typedef long long ll;
long long R, C;

long long font = -1;


int queries = 0;

inline bool query(ll r, ll c) {
	if(r < 0 || r >= R || c < 0 || c >= C)
		return false;
	++queries;
	return(pixel(r,c));
}



pair<ll, ll> search(ll r, ll c, ll dr, ll dc) { // (r,c) must be black, and dr^2 + dc^2 == 1 Returns the last black cell from (r,c) in the direction (dr,dc) 
	ll z=1;
	while(query(r + z*dr, c + z*dc))
		z*=2;
	
	ll L = 0, U = z; //Invariant : the last black cell in the direction (dr,dc) is (r,c) + p*(dr,dc), for p in [L,U)
	while(U - L > 1)
	{
		const ll mid = (L+U)/2;
		if(query(r + mid*dr, c + mid*dc))
			L = mid;
		else
			U = mid;
	}
	return make_pair(r + L*dr, c + L*dc);
}

ll find_font_size(ll hspan, ll vspan) {
	if(hspan == vspan) {
		assert(hspan%3 == 0);
		return hspan/3;
	}
	if (hspan == 3*vspan)
		return vspan;
	if (vspan == 3*hspan)
		return hspan;
	if (vspan == 5*hspan)
		return hspan;
	if (hspan%3 == 0 && vspan%5 == 0 && hspan/3 == vspan/5)
		return hspan/3;
	return -1; //This should not happen!
}

int get_digit(const bool g[5][3]) { //get the digit this represents, or -1 if none
	for(int d=0; d<16; ++d) {
		for(int r=0; r<5; ++r)
		for(int c=0; c<3; ++c)
			if(dbit[d][r][c] != g[r][c])
				goto TRY_NEXT_DIGIT;
		return d;
		TRY_NEXT_DIGIT:
		;
	}
	return -1;
}

inline int query_for_digit(ll tlr, ll tlc) { //query and try to find the digit with this top-left corner
	bool test[5][3];
	for(int r=0; r<5; ++r)
	for(int c=0; c<3; ++c)
		test[r][c] = query(tlr + font*r, tlc + font*c);
	return get_digit(test);
}

void solve(long long R_, long long C_, long long br, long long bc, int &ans_N, char *ans_digits, long long &ans_r, long long &ans_c) {
	
	init();
	R = R_;
	C = C_;
	//First find the horizontal and vertical span from the given black pixel.
	const ll go_right = search(br, bc, 0, 1).second, go_left = search(br, bc, 0, -1).second;
	const ll go_down = search(br, bc, 1, 0).first, go_up = search(br, bc, -1, 0).first;
	const ll hspan = go_right - go_left + 1;
	const ll vspan = go_down - go_up + 1;
	//From hspan and vspan, we can find the font size:
	font = find_font_size(hspan, vspan);
	
	
	// A block is a (font x font) square of black pixels, correctly aligned
	
	//Replace (br, bc) by the top-left corner of the block in which (br,bc) occurs:
	br -= (br - go_up) % font;
	bc -= (bc - go_left) % font;
	
	//From the given black block, go 4 blocks up, 4 blocks down, 2 blocks left, and 2 blocks right
	
	bool tmp1__[9][5];
	bool *tmp2__[9];
	for(int i=0; i<9; ++i)
		tmp2__[i] = tmp1__[i] + 2;
	bool * const * const g = tmp2__ + 4;
	//Effect : we have indexing g[-4][-2] to g[4][2]
	
	int nblack=0;
	for(ll off_r = -4; off_r <= 4; ++off_r)
	for(ll off_c = -2; off_c <= 2; ++off_c)
		if(g[off_r][off_c] = query(br + off_r*font, bc + off_c*font))
			++nblack;
	
	bool test[5][3];
	int first_digit = -1; //First to be detected
	ll tlr = -1, tlc = -1; //top-left corner of this digit
	for(ll off_r = -4; off_r +5 -1 <= 4; ++off_r)
	for(ll off_c = -2; off_c +3 -1 <= 2; ++off_c) //Try all offsets
	{
		//fprintf(stderr, "off_r = %lld, off_c = %lld\n", off_r, off_c);
		int localblack=0;
		for(ll r=0; r<5; ++r)
		for(ll c=0; c<3; ++c) //Copy all digits
			if(test[r][c] = g[off_r + r][off_c + c])
				++localblack;
		
		if(nblack != localblack) //We have missed some pixels, should not detect digit based on incomplete pixels
			continue;
		if((first_digit = get_digit(test)) != -1) {
			tlr = br + off_r*font;
			tlc = bc + off_c*font;
			goto FOUND_FIRST_DIGIT;
		}
	}
	FOUND_FIRST_DIGIT:
	assert(first_digit != -1);
	temp_ans[max_N] = first_digit;
	
	int endidx = max_N; //scan to the right
	ll tlrz = tlr, tlcz = tlc;
	while(temp_ans[endidx] != -1)
		temp_ans[++endidx] = query_for_digit(tlrz, (tlcz += 6*font)); //6 = 3 for font + 3 for gap
	--endidx;
	
	int begidx = max_N; //scan to the left
	tlrz = tlr, tlcz = tlc;
	while(temp_ans[begidx] != -1)
		temp_ans[--begidx] = query_for_digit(tlrz, (tlcz -= 6*font));
	
	tlcz += 6*font;
	++begidx;
	
	const char chars[16] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
	//report answers
	ans_N = endidx - begidx + 1;
	for(int i = begidx; i <= endidx; ++i)
		ans_digits[i - begidx] = chars[temp_ans[i]];
	ans_r = tlrz;
	ans_c = tlcz;
}
