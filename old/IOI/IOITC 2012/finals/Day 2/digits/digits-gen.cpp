// Meant to be called by batch-gen.py

#include <iostream>
#include <vector>
#include <fstream>
#include "MersenneTwister.h"

using namespace std;

#include "digits-common.h"

const int max_N=100;

typedef long long ll;

const ll maxR=1000LL*1000*1000*1000*1000*1000;
const ll maxC=1000LL*1000*1000*1000*1000*1000;
const ll minR=5;
const ll minC=3;
const ll minfont=1;
const ll maxfont=1000LL*1000*1000*1000*1000*1000;


MTRand Rgen;

ll rand_ll(ll n) //[0,n]
{
	return (((static_cast<unsigned long long>(Rgen.randInt()))<<32) + Rgen.randInt())%(n+1);
}


int main()
{
	init();
	string s;
	cin >> s; //should be a sequence of hex digits, or "random"
	int digits[max_N];
	int N;
	MTRand Rgen;
	if(s == "random") {
		int seed;
		cin >> N >> seed;
		assert(1 <= N && N <= max_N);
		Rgen.seed(seed);
		for(int i=0; i<N; ++i)
			digits[i] = Rgen.randInt(DIGITS-1);
		
	}
	else {
		N = s.size();
		assert(1 <= N && N <= max_N);
		for(int i=0; i<N; ++i)
			assert((digits[i] = val[s[i]]) != -1);
	}
	ll R, C;
	cin >> R >> C;
	assert(minR <= R && R <= maxR && minC <= C && C <= maxC);
	ll tlr, tlc;
	cin >> tlr >> tlc;
	assert(0 <= tlr && tlr < R && 0 <= tlc && tlc < C);
	ll font;
	cin >> font;
	assert(1 <= font && font <= maxfont);
	
	ll dispR = font*RD;
	ll dispC = (gap+CD)*N*font - gap*font;
	assert(tlr + dispR <= R && tlc + dispC <= C);
	
	int idx; //Index of the digit with the initial black cell;
	int dr, dc; //To identify the block in the digit
	ll pr, pc; //To identify the pixel within the block
	if(s == "random") {
		idx = Rgen.randInt(N-1);
		vector<pair<int, int> > choices;
		for(int r=0; r<RD; ++r)
		for(int c=0; c<CD; ++c)
			if(dbit[digits[idx]][r][c])
				choices.push_back(make_pair(r,c));
		const int cidx = Rgen.randInt(choices.size()-1);
		dr = choices[cidx].first;
		dc = choices[cidx].second;
		pr = rand_ll(font-1);
		pc = rand_ll(font-1);
	}
	else {
		cin >> idx;
		assert(0 <= idx && idx < N);
		cin >> dr >> dc;
		assert(0 <= dr && dr <= RD && 0 <= dc && dc <= CD);
		assert(dbit[digits[idx]][dr][dc]);
		cin >> pr >> pc;
		assert(0 <= pr && pr < font && 0 <= pc && pc < font);
	}
	const char chars[16] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
	for(int i=0; i<N; ++i)
		cout<<chars[digits[i]];
	cout<<'\n'<<R<<' '<<C<<'\n';
	cout<<tlr<<' '<<tlc<<'\n';
	cout<<font<<'\n';
	cout<<(tlr + font*dr + pr)<<' '<<(tlc + font*(CD+gap)*idx + font*dc + pc)<<'\n';
}
