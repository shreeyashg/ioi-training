\documentclass[11pt]{article}
\usepackage{fullpage}
\addtolength{\textheight}{4cm}
\addtolength{\topmargin}{-1cm}

\usepackage{latexsym}
\usepackage{amssymb}
\usepackage{gastex}
\pagestyle{empty}
\usepackage{colortbl}
\usepackage{xcolor}
\usepackage[normalem]{ulem}



\newcommand{\CHead}[1]{\multicolumn{1}{c}{\emph{#1}}}
\newcommand{\RHead}[1]{\multicolumn{1}{c||}{\emph{#1}}}

\renewcommand{\thesubsection}{Problem \arabic{subsection}}

\newcommand{\Code}[1]{\texttt{#1}}
\setlength{\parindent}{0mm}

\begin{document}

\begin{center}
\textbf{\large IOI Training Camp 2012 -- Final 2}
\end{center}
\setcounter{section}{2}
\section{Digits}

Orange has released their latest new shiny electronic device, equipped with their cutting-edge Cornea display with zillions of tiny subatomic pixels, a carbon fibre body, and hardware support for Facebook integration. Crazyman has won one such gadget in a programming contest, but he is using it simply to display a number in hexadecimal, even though the 7-segment display on a calculator would have sufficed for this purpose. As a challenge, you have decided to find the number written on the screen by querying a few pixels of the screen.

We first describe how a sequence of digits is represented as a rectangular grid of \emph{blocks}, each of which can be white (the background), or black (the foreground). Each digit occupies a grid of blocks with 3 columns and 5 rows. Two consecutive digits have \emph{three} empty columns of blocks between them.  The following two grids show ``01234567'' and ``89abcdef'' respectively, \emph{except that due to space constraints, only one empty column between two digits instead of three is shown}.

\vspace{2mm}
\begin{minipage}[t]{0.2\textwidth}
There are only $16$ possible digits, and they are all shown here. 
\end{minipage}
\hfill
\begin{minipage}[t]{0.78\textwidth}
\setlength{\unitlength}{0.9mm}
\begin{center} \input{line1.tex} \end{center}
\vspace{-7mm}
\begin{center} \input{line2.tex} \end{center}
\end{minipage}
\vspace{2mm}


Finally, to display a given sequence of digits on the screen using font size $k$ ($k \geq 1$), we first translate the sequence of digits into a grid of blocks as described above, and then translate this to a grid of pixels by using a $k \times k$ square of pixels for every block. For example, the following shows ``21b74'' displayed with font size $k=2$. Note that this correctly has three columns of blocks between two consecutive digits.

\vspace{-3mm}
\begin{center} \input{ex.tex} \end{center}
\vspace{-3mm}

As you can calculate, a sequence of $m$ digits displayed with font size $k$ will require a rectangle (called its \emph{display rectangle}) occupying $5k$ rows and $6mk-3k$ columns of pixels on the Cornea display. 

Your task is the following : a sequence of digits is displayed somewhere on the screen, in some font size. You will be given the location of one black pixel on the screen. By querying the black/white status of various pixels on the screen, you have to determine the sequence of digits and the top-left corner of the display rectangle used (note: in some cases, the top-left corner may be a white pixel). You may make at most 5000 queries.

\vspace{-3mm}
\subsection*{Library interaction}

For this problem, you must \emph{not} write a \Code{main} function and must \emph{not} perform any input or output. Instead you should write a function \Code{solve} which plays the role of your main function, with the following signature:

\begin{verbatim}
void solve(long long R, long long C, long long br, long long bc, 
    int &ans_N, char *ans_digits, long long &ans_r, long long &ans_c);
\end{verbatim}

The grader will call this function with appropriate arguments. \Code{R} and \Code{C} describe the screen resolution: the number of rows and columns of pixels respectively. The rows are numbered from 0 to $\Code{R} - 1$ from top to bottom, and the columns are numbered from 0 to $\Code{C} - 1$. The pixel \Code{(br, bc)} is black. It is guaranteed that $0 \leq \Code{br} < R$ and $0 \leq \Code{bc} < C$.

Your code can call the function \Code{pixel} to find out the colour of a particular pixel:

\verb+bool pixel(long long r, long long c);+

\Code{pixel(r, c)} returns \Code{true} if the pixel \Code{(r,c)} is black, and \Code{false} otherwise, for $0 \leq r < R$ and $0 \leq c < C$. If $(r,c)$ is not within range, your program will be terminated.

Finally when the function \Code{solve} is ready to report the answers, it should write the answers to the following variables:
\begin{itemize}
\addtolength{\itemsep}{-1ex}
\item \Code{ans\_N} : The number of digits displayed.
\item \Code{ans\_digits[0]}, \ldots, \Code{ans\_digits[N-1]} : The digits displayed. Each element must be one of \Code{'0'} \ldots \Code{'9'}, \Code{'a'} - \Code{'f'}, or \Code{'A'} - \Code{'F'}. Both lowercase and uppercase letters will be accepted and can be used interchangeably.
\item \Code{ans\_r}, \Code{ans\_c} : The row and column coordinate of the top-left corner of the display rectangle. Note that this may be a white pixel or a black pixel.
\end{itemize}

You are given a template file with a \Code{solve} function in which you can fill in your code.

\vspace{-3mm}
\subsection*{Compiling your program}

In the beginning of your solution, add the following line:\\
\verb+bool pixel(long long, long long);+

The template provided includes this. If your file is called \Code{digits.cpp}, you may compile as follows: \\
\verb#g++ digits.cpp digitslib.o -W -Wall -O2 -o digits#

\vspace{-3mm}
\subsection*{Test data}

In all subtasks, $5 \leq R \leq 10^{18}$ and $3 \leq C \leq 10^{18}$, where $R$ and $C$ are the number of rows and columns of pixels respectively. $1 \leq N \leq 100$, where $N$ is the number of digits displayed. You can make at most 5000 calls to \Code{pixel}.

\begin{itemize}
\addtolength{\itemsep}{-1ex}
\item Subtask 1 (15 marks) : The font size is $1$.
\item Subtask 2 (25 marks) : The font size is between $1$ and $16$ inclusive.
\item Subtask 3 (15 marks) : $N=1$, and the digit displayed is \Code{6}.
\item Subtask 4 (20 marks) : $N=1$.
\item Subtask 5 (25 marks) : No further constraints.
\end{itemize}

\vspace{-5mm}
\subsection*{Experimentation}

This section describes how the library works, so that you can experiment with your solution yourself. The information in this section is \emph{not} needed to write your solution or to submit your solution on the grader. The library reads data from the standard input and then calls your \Code{solve} function. The input format is as follows:

\begin{itemize}
\addtolength{\itemsep}{-1ex}
\item Line 1 : A sequence of hexadecimal digits, of length between $1$ and $100$. Both lowercase and uppercase letters are accepted, and they are equivalent.
\item Line 2 : Two integers, the number of rows and columns of pixels that the Cornea display has.
\item Line 3 : Two integers, the row and column coordinates of the top-left corner of the display rectangle respectively.
\item Line 4 : A single integer, the font size.
\item Line 5 : Two integers, the row and column coordinates of a black pixel, to be passed to \Code{solve}.
\end{itemize}
Finally, the library checks the correctness of your answers and reports it on standard output. The library writes a log of the interaction to {\Code{digits.log}}. This is for your information only and you are not required to examine this file.

\vspace{-3mm}
\subsection*{Limits}

\begin{minipage}[t]{0.49\textwidth}
\begin{itemize}
\addtolength{\itemsep}{-1ex}
\item \emph{Memory limit} : 128 MB 
\end{itemize}
\end{minipage}
\hfill
\begin{minipage}[t]{0.49\textwidth}
\begin{itemize}
\addtolength{\itemsep}{-1ex}
\item \emph{Time limit} : 1s
\end{itemize}
\end{minipage}

\end{document}
