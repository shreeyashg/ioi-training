#include <cassert>
const int DIGITS=16;
const int RD = 5; //rows for a digit
const int CD = 3; //columns for a digit
const int gap = 3;

char d_visual[DIGITS][RD][CD+1]={ //+1 only for null-terminator which we don't care for
	"***",
	"*.*",
	"*.*",
	"*.*",
	"***",
	
	"..*",
	"..*",
	"..*",
	"..*",
	"..*",
	
	"***",
	"..*",
	"***",
	"*..",
	"***",
	
	"***",
	"..*",
	"***",
	"..*",
	"***",
	
	"*.*",
	"*.*",
	"***",
	"..*",
	"..*",
	
	"***",
	"*..",
	"***",
	"..*",
	"***",
	
	"***",
	"*..",
	"***",
	"*.*",
	"***",
	
	"***",
	"..*",
	"..*",
	"..*",
	"..*",
	
	"***",
	"*.*",
	"***",
	"*.*",
	"***",
	
	"***",
	"*.*",
	"***",
	"..*",
	"..*",
	
	"***",
	"*.*",
	"***",
	"*.*",
	"*.*",
	
	"*..",
	"*..",
	"***",
	"*.*",
	"***",
	
	"***",
	"*..",
	"*..",
	"*..",
	"***",
	
	"..*",
	"..*",
	"***",
	"*.*",
	"***",
	
	"***",
	"*..",
	"***",
	"*..",
	"***",
	
	"***",
	"*..",
	"***",
	"*..",
	"*.."
};

bool dbit[DIGITS][RD][CD]={0};

const int NCHARS=256;
int val[NCHARS];

void init()
{
	for(int d=0; d<DIGITS; ++d)
	for(int r=0; r<RD; ++r)
	for(int c=0; c<CD; ++c)
	{
		assert(d_visual[d][r][c] == '.' || d_visual[d][r][c] == '*');
		dbit[d][r][c] = (d_visual[d][r][c] == '*');
	}
	for(int i=0; i<NCHARS; ++i)
		val[i] = -1;
	val['0'] = 0;
	val['1'] = 1;
	val['2'] = 2;
	val['3'] = 3;
	val['4'] = 4;
	val['5'] = 5;
	val['6'] = 6;
	val['7'] = 7;
	val['8'] = 8;
	val['9'] = 9;
	val['a'] = val['A'] = 10;
	val['b'] = val['B'] = 11;
	val['c'] = val['C'] = 12;
	val['d'] = val['D'] = 13;
	val['e'] = val['E'] = 14;
	val['f'] = val['F'] = 15;
}
