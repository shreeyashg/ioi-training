# include <algorithm>
# include <cstdio>
using namespace std;

bool pixel(long long, long long);
long long R,C;

struct point
{
	long long r,c;
	
	point(long long a=0,long long b=0)
	{
		r=a,c=b;
	}
};

point operator+(point a,point b)
{
	return point(a.r+b.r,a.c+b.c);
}

point operator*(point a,long long x)
{
	return point(a.r*x,a.c*x);
}

bool mypixel(point a)
{
	if(a.r<0||a.c<0||a.r>=R||a.c>=C)return false;
	else return pixel(a.r,a.c);
}

point search(point cur,point dir)
{
	long long start=0,end=1,mid;
	while(mypixel(cur+dir*end))start=end,end<<=1;
	
	while(start<end-1)
	{
		mid=(start+end)>>1;
		(mypixel(cur+dir*mid)?start:end)=mid;
	}
	return cur+dir*start;
}

void solve(long long R, long long C, long long br, long long bc, int &N, char *num, long long &r0, long long &c0)
{
	::R=R,::C=C;
	
	point cur(br,bc);
	
	long long r1=search(cur,point(1,0)).r,r2=search(cur,point(-1,0)).r,dr=r1-r2+1;
	long long c1=search(cur,point(0,1)).c,c2=search(cur,point(0,-1)).c,dc=c1-c2+1;
	
	long long F;
	if(dr==dc)F=dr/3;
	else if(min(dr,dc)*5==max(dr,dc)*3)F=min(dr,dc)/3;
	else F=min(dr,dc);
	
	char box[9][5],numgood=0;
	for(int i=0;i<9;i++)
		for(int j=0;j<5;j++)
		{
			box[i][j]=mypixel(cur+point(i-4,j-2)*F);
			numgood+=box[i][j];
		}
	
	int tlbr,tlbc;
	for(int i=0;i<5;i++)
		for(int j=0;j<3;j++)
		{
			int cnt=0;
			for(int k=i;k<i+5;k++)
				for(int l=j;l<j+3;l++)
					cnt+=box[k][l];
			
			if(cnt==numgood)
			{
				tlbr=i;tlbc=j;
				goto BPP;
			}
		}
	BPP:
	
	point tl=cur+point(tlbr-4,tlbc-2)*F;
	int fwd[]={119,18,93,91,58,107,111,82,127,122,126,47,101,31,109,108},bwd[128];
	int pos[][2]={{0,1},{1,0},{1,2},{2,1},{3,0},{3,2},{4,1}};
	
	for(int i=0;i<16;i++)
		bwd[fwd[i]]=i;
	
	while(1)
	{
		for(int i=0;i<7;i++)
			if(mypixel(tl+point(pos[i][0],pos[i][1]-6)*F))
			{
				tl=tl+point(0,-6)*F;
				goto BPP2;
			}
		break;
		BPP2:;
	}
	
	N=0;
	while(1)
	{
		int val=0;
		for(int i=0;i<7;i++)
		{
			val<<=1;
			if(mypixel(tl+point(pos[i][0],pos[i][1])*F))val|=1;
		}
		
		if(val==0)break;
		num[N]=bwd[val];
		if(num[N]>9)num[N]+='A'-10;
		else num[N]+='0';
		N++;
		tl=tl+point(0,6)*F;
	}
	
	tl=tl+point(0,-6)*F*N;
	
	if(mypixel(tl))
	{
		tl=search(tl,point(-1,0));
		tl=search(tl,point(0,-1));
	}
	else
	{
		tl=tl+point(0,2)*F;
		tl=search(tl,point(-1,0));
		tl=search(tl,point(0,-1));
		tl=tl+point(0,-2)*F;
	}
	
	r0=tl.r;c0=tl.c;
	return;
}

