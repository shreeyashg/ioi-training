bool pixel(long long r, long long c);

//You are free to define other functions, but you must not have a function called main.
//For debugging you may use I/O, but your final submission must not do any input/output.

void solve(long long R, long long C, long long br, long long bc, int &ans_N, char *ans_digits, long long &ans_r, long long &ans_c)
{
	/*
	 * R is the number of rows of pixels on the screen, numbered 0 to R-1 from top to bottom.
	 * C is the number of columns of pixels on the screen, numbered 0 to C-1 from left to right.
	 * The pixel at row br, column bc is black.
	 */
	
	//Your solution here:
	
	if(pixel(0,0)) //Just an example
	{
		/*...*/ ;
	}
	
	
	//Finally, when you are done, write your answers to the provided variables, for example:
	ans_N = 3;
	ans_digits[0] = '0';
	ans_digits[1] = '1';
	ans_digits[2] = '2';
	//You must not write to ans_digits[i] for i < 0 or i >= 100.
	ans_r = 0;
	ans_c = 0;
}
