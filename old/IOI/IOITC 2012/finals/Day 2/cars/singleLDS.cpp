/*
 	Team Proof
	IIT Delhi
 
	C++ Template
 */


#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <cassert>
#include <string>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <stack>
#include <queue>
#include <cstdlib>
using namespace std;

#define s(T) scanf("%d", &T)
#define sl(T) scanf("%lld", &T)
#define fill(a, val) memset(a, val, sizeof(a))
#define mp make_pair

const int INF = 1000000009;
const int maxn = 5005;

int totalCases, testNum;

int N;
vector <int> ldsarr;
vector <int> arr;
int dp[maxn];
int parent[maxn];

void preprocess()
{
	
}

bool input()
{
	s(N);
	arr.resize(N);
	for(int i = 0; i < N; i++)
	{	
		s(arr[i]);
		ldsarr.push_back(arr[i]);
	}
	
//	int spd = INF;
//	for(int i = 0; i < N; i++)
//		if(arr[i] < spd)
//			spd = arr[i];
//		else 
//			ldsarr.push_back(arr[i]);
	
	return true;
}

void solve()
{
	//int minima = N - ldsarr.size();
	fill(parent, -1);
	int lastans = -1;
	int ans = 0;
	for(int i = 0; i < ldsarr.size();i++)
	{
		int maxlen = 0;
		for(int j = 0; j < i;j++)
			if(ldsarr[j] > ldsarr[i])
				if(maxlen < dp[j])
				{
					maxlen = dp[j];
					parent[i] = j;
				}
		dp[i] = 1 + maxlen;
		if(dp[i] > ans)
		{
			ans = dp[i];
			lastans = i;
		}
	}
	
	bool mark[maxn];
	fill(mark, false);
	for(int i = lastans; i >= 0; i = parent[i])
		mark[i] = true;
	
	ldsarr.clear();
	int spd = INF;
	for(int i= 0; i < N; i++)
		if(!mark[i] && arr[i] < spd)
			ans++, spd = arr[i];
	
//	int ans = 0;
//	for(int i = 0; i < ldsarr.size(); i++)
//	{	
//		int maxlen = 0;
//		for(int j = 0; j < i; j++)
//			if(ldsarr[j] > ldsarr[i])
//				maxlen = max(maxlen, dp[j]);
//		dp[i] = 1 + maxlen;
//		ans = max(ans, dp[i]);
//	}
//	printf("%d\n", minima + ans);
	printf("%d\n", ans);
}

int main()
{
	preprocess();
	//s(totalCases);
	totalCases = 1;
	for(testNum = 1; testNum <= totalCases; testNum++)
	{
		if( !input())
			break;
		solve();
	}
}
