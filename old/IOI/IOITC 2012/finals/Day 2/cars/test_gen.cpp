/*
 *  test_gen.cpp
 */

#include <iostream>
#include <cstdio>
#include <set>
#include <algorithm>
#include <cstdlib>
#include <cassert>
using namespace std;

#define s(T) scanf("%d", &T)

int N;
int len;

int arr[5000];

int main()
{
	s(N);
	s(len);
	
	set <int> found;
	srand(time(NULL));
	for(int i = 0; i < N; i++)
	{
		int r = (rand()%100000)*10000 + (rand()%10000) + 1;
		if(found.find(r) == found.end())
			found.insert(r);
		else 
			i--;
	}
	
	assert(found.size() == N);
	int k = 0;
	for(set<int>::reverse_iterator it = found.rbegin(); it != found.rend(); it++)
		arr[k++] = *it;
	
	
//asc sorting
	reverse(arr, arr+N);
/*	
	
	if(len < N)
	{
		for(int iter = 0; iter < (N-len)*(N-len); iter++)
		{
			int i = rand() % (N-len);
			int j = rand() % (N-len);
			swap(arr[i], arr[j]);
		}
	}
	
	int tmp = ((rand() % (k+1)) + (rand() % (k+1)))/2;
	swap(arr[N-1], arr[tmp]);
	
	k = N-len;
	int prev = -1;
	while(k < N)
	{
		int range = min( (N - prev - 1 +N-k-1)/(N-k), k-prev);
		int cur = (rand() % range) + prev+1;
		swap(arr[cur], arr[k]);
		prev = cur;
		k++;
	}
*/
	
	
	
	printf("%d\n", N);
	for(int i = 0; i < N; i++)
		printf("%d\n", arr[i]);
}

