/*
 	Team Proof
	IIT Delhi
 
	C++ Template
 */


#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <cassert>
#include <string>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <stack>
#include <queue>
#include <cstdlib>
using namespace std;

#define s(T) scanf("%d", &T)
#define sl(T) scanf("%lld", &T)
#define fill(a, val) memset(a, val, sizeof(a))
#define mp make_pair

const int INF = 1000000009;
const int maxn = 5005;

int totalCases, testNum;

int N;
vector <int> ldsarr;
vector <int> arr;
int dp[maxn];

void preprocess()
{
	
}

bool input()
{
	s(N);
	arr.resize(N);
	for(int i = 0; i < N; i++)
		s(arr[i]);
	
	int spd = INF;
	for(int i = 0; i < N; i++)
		if(arr[i] < spd)
			spd = arr[i];
		else 
			ldsarr.push_back(arr[i]);
	
	return true;
}

void solve()
{
	int minima = N - ldsarr.size();
	int ans = 0;
	for(int i = 0; i < ldsarr.size(); i++)
	{	
		int maxlen = 0;
		for(int j = 0; j < i; j++)
			if(ldsarr[j] > ldsarr[i])
				maxlen = max(maxlen, dp[j]);
		dp[i] = 1 + maxlen;
		ans = max(ans, dp[i]);
	}
	printf("%d\n", minima + ans);
}

int main()
{
	preprocess();
	//s(totalCases);
	totalCases = 1;
	for(testNum = 1; testNum <= totalCases; testNum++)
	{
		if( !input())
			break;
		solve();
	}
}
