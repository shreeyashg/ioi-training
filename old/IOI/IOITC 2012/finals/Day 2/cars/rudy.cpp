#include<vector>
#include<stack>
#include<set>
#include<map>
#include<queue>
#include<deque>
#include<string>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<cassert>
#include<cstdlib>
#include<cstdio>
#include<cmath>
#include<string>

using namespace std;

#define s(n)					scanf("%d",&n)
#define sl(n) 					scanf("%lld",&n)
#define sf(n) 					scanf("%lf",&n)
#define ss(n) 					scanf("%s",n)
#define INF						(int)(1e9+9)
#define LINF					(long long)1e18
#define EPS						1e-9
#define maX(a,b)				((a)>(b)?(a):(b))
#define miN(a,b)				((a)<(b)?(a):(b))
#define abS(x)					((x)<0?-(x):(x))
#define FOR(i,a,b)				for(int i=a;i<b;i++)
#define REP(i,n)				FOR(i,0,n)
#define foreach(v,c)            for( typeof((c).begin()) v = (c).begin();  v != (c).end(); ++v)
#define mp						make_pair
#define FF						first
#define SS						second
#define tri(a,b,c)				mp(a,mp(b,c))
#define XX						first
#define YY						second.first
#define ZZ						second.second
#define pb						push_back
#define fill(a,v) 				memset(a,v,sizeof a)
#define all(x)					x.begin(),x.end()
#define SZ(v)					((int)(v.size()))
#define DREP(a)					sort(all(a)); a.erase(unique(all(a)),a.end())
#define INDEX(arr,ind)			(lower_bound(all(arr),ind)-arr.begin())
#define debug(args...)			{dbg,args; cerr<<endl;}
#define dline					cerr<<endl	

void sc(char &c){
	char temp[4];	ss(temp);	
	c=temp[0];
}

struct debugger
{
	template<typename T> debugger& operator , (const T& v)
	{	
		cerr<<v<<" ";	
		return *this;	
	}
} dbg;

void debugarr(int * arr,int n)
{
	cout<<"[";
	for(int i=0;i<n;i++)
		cout<<arr[i]<<" ";
	cout<<"]"<<endl;
}





typedef long long LL;
typedef pair<int,int> PII;
typedef pair<LL,LL> PLL;
typedef pair<int,PII> TRI;

typedef vector<int> VI;
typedef vector<LL> VL;
typedef vector<PII> VII;
typedef vector<PLL> VLL;
typedef vector<TRI> VT;

typedef vector<VI> VVI;
typedef vector<VL> VVL;
typedef vector<VII> VVII;
typedef vector<VLL> VVLL;
typedef vector<VT> VVT;


/*Main code begins now */

int testnum;
int N;
int spd[5005];
int ind[5005];		// ind[i] denotes index of the slowest car in 0...i
int dp[2][5005];	// dp[i%2][j] denotes max ans when 0...i cars are inspected, min in one lane is car j, min in other lane is ind[i]


void solve()
{
	ind[0]=0;
	for(int i=1;i<=N;i++)
		ind[i] = (spd[i] < spd[ind[i-1]]) ? i : ind[i-1];
	
	fill(dp,-1);
	dp[0][0]=0;			// Initially both lanes are filled with dummy car number 0, with INF speed, but score is still zero
	for(int i=0;i<N;i++)
	{
		int cur = i&1;
		int nxt = 1-cur;
		fill(dp[nxt],-1);
		
		for(int j=0;j<=i;j++)
		{
			if(dp[cur][j] < 0) continue; 
			int speed1 = spd[ind[i]];
			int speed2 = spd[j];
			int curspeed = spd[i+1];
			
			// Put in lane 1
			if(ind[i+1]==i+1)			// Case 1.1 : i+1 is the new slowest
				dp[nxt][j] = max(dp[nxt][j], dp[cur][j]+1);
			else						// Case 1.2 : Nothing extra happens;
				dp[nxt][j] = max(dp[nxt][j], dp[cur][j]);
			
			// Put in lane 2
			if(ind[i+1]==i+1)			// Case 2.1 : i+1 is the slowest so far, so put it in lane 2 and swap lanes, so ind[i] becomes last leader of lane 2 now
				dp[nxt][ind[i]] = max(dp[nxt][ind[i]], dp[cur][j]+1);
			else if(curspeed<speed2)	// Case 2.2 : i+1 becomes new last leader of lane 2
				dp[nxt][i+1] = max(dp[nxt][i+1], dp[cur][j]+1);
			else						// Case 2.3 : Nothing extra happens
				dp[nxt][j] = max(dp[nxt][j], dp[cur][j]);
		}
	}
				
	int ans=0;
	int cur = N&1;
	for(int j=0;j<=N;j++)
		ans = max(ans, dp[cur][j]);
	printf("%d\n",ans);
}

bool input()
{
	s(N);
	spd[0] = INF;	// Dummy
	for(int i=1;i<=N;i++)
		s(spd[i]);
	return true;
}


int main()
{
	input();
	solve();
	
}
