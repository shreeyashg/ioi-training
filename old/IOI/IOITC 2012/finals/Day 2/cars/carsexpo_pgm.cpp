/*
 	Team Proof
	IIT Delhi
 
	C++ Template
 */


#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <cassert>
#include <string>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <stack>
#include <queue>
#include <cstdlib>
using namespace std;

#define s(T) scanf("%d", &T)
#define sl(T) scanf("%lld", &T)
#define fill(a, val) memset(a, val, sizeof(a))
#define mp make_pair

const int INF = 1000000007;

int totalCases, testNum;
int N;
int arr[25];

int onelane(vector <int> cur)
{
	int ans = 0;
	int spd = INF;
	for(int i = 0; i < cur.size(); i++)
	{
		if(cur[i] < spd)
		{
			ans++;
			spd = cur[i];
		}
	}
	return ans;
}

void preprocess()
{
	
}

bool input()
{
	s(N);
	if(N > 20)
		return false;
	for(int i = 0; i < N; i++)
		s(arr[i]);
	return true;
}

void solve()
{
	vector <int> lane1, lane2;
	int sol = 0;
	for(int bm = 0; bm < (1<<N); bm+=2)
	{
		lane1.clear(); lane2.clear();
		for(int i = 0; i < N; i++)
			if(bm & (1<<i))
				lane1.push_back(arr[i]);
			else 
				lane2.push_back(arr[i]);
		sol = max(sol, onelane(lane1) + onelane(lane2));
	}
	printf("%d\n", sol);
}

int main()
{
	preprocess();
	//s(totalCases);
	totalCases = 1;
	for(testNum = 1; testNum <= totalCases; testNum++)
	{
		if( !input())
			break;
		solve();
	}
}
