// Written by Rudradev Basak
// Note that increasing (decreasing) beamWidth will result in better (worse) results at the cost of higher (lower) execution time.

//Data Structure includes
#include<vector>
#include<stack>
#include<set>
#include<map>
#include<queue>
#include<deque>
#include<string>
#include<list>


//Other Includes
#include<iostream>
#include<fstream>
#include<algorithm>
#include<cstring>
#include<cassert>
#include<cstdlib>
#include<cstdio>
#include<cmath>
#include <sys/time.h>

using namespace std;

#define FOR(i,a,b)					for(int i=a;i<b;i++)
#define REP(i,n)					FOR(i,0,n)
#define pb						 	push_back
#define mp						 	make_pair
#define s(n)						scanf("%d",&n)
#define sl(n) 						scanf("%lld",&n)
#define sf(n) 						scanf("%lf",&n)
#define ss(n) 						scanf("%s",n)
#define fill(a,v) 					memset(a, v, sizeof a)
#define sz							size()
#define INF							(int)1e9
#define EPS							1e-9
#define bitcount					__builtin_popcount
#define all(x)						x.begin(), x.end()
#define gcd							__gcd
#define maX(a,b)					(a>b?a:b)
#define miN(a,b)					(a<b?a:b)

typedef vector<int> VI;
typedef vector<vector<int> > VVI;
typedef long long LL;
typedef pair<int, int > PII;

/*Main code begins now */

const int beamWidth = 50;
// Increasing (decreasing) beamWidth will result in better (worse) results at the cost of higher (lower) execution time.
// Time taken varies linearly with beamWidth, while results will not :)


int testnum;
int R,C;
VVI grid;
int dx[]={-1,1,0,0};
int dy[]={0,0,-1,1};
int dist[55][55];
list<pair<int,PII> > dists;

map<char,int> dict;
char rdict[15];
int rval[]={237,96,243,220,70,126};

string ans;

void display(const VVI &grid)
{
	printf("\n");
	for(int i=0;i<R;i++)
	{
		for(int j=0;j<C;j++)
			printf("%d",grid[j][i]+1);
		printf("\n");
	}
	printf("\n");
}

int getH(const VVI &grid)
{
	for(int i=0;i<R;i++)
		for(int j=0;j<C;j++)
			dist[i][j] = INF;
			
	while(!dists.empty())
		dists.pop_front();
	dists.push_back(mp(0,mp(0,0)));
	
	while(!dists.empty())
	{
		pair<int,PII> temp = *(dists.begin());
		int d=temp.first;
		int x=temp.second.first;
		int y=temp.second.second;
		
		dists.erase(dists.begin());
		if(d>dist[x][y]) continue;
		
		for(int i=0;i<4;i++)
		{
			int xx = x+dx[i];
			int yy = y+dy[i];
			if(xx<0 || xx>=R || yy<0 || yy>=C) continue;
			
			int dd = d + (grid[xx][yy]==grid[x][y] ? 0 : 1);
			if(dd>=dist[xx][yy]) continue;
			
			dist[xx][yy]=dd;
			if(dd==d)
				dists.push_front(mp(dist[xx][yy],mp(xx,yy)));
			else
				dists.push_back(mp(dist[xx][yy],mp(xx,yy)));
		}
	}
	
	int ans=0;
	for(int i=0;i<R;i++)
		for(int j=0;j<C;j++)
			ans=max(ans,dist[i][j]);
		
	int fmax=0;
	for(int i=0;i<R;i++)
		for(int j=0;j<C;j++)
			if(dist[i][j]==ans) fmax++;
			
	bool occ[6];
	fill(occ,false);
	for(int i=0;i<R;i++)
		for(int j=0;j<C;j++)
			if(dist[i][j]>0)
				occ[grid[i][j]]=true;
			
	int cnt=0;
	for(int i=0;i<6;i++)
		if(occ[i]) cnt++;
		
	int cnt1=0;
	for(int i=0;i<R;i++)
		for(int j=0;j<C;j++)
			cnt1++;
	
	return  cnt*1000000 + ans*10000 + fmax*100 + cnt1;
}

VVI temp;

void dfs(int r,int c,int pre,int post)
{
	temp[r][c]=post;	//Enough to also serve as done array
	
	for(int i=0;i<4;i++)
	{
		int x=r+dx[i];
		int y=c+dy[i];
		if(x<0 || x>=R || y<0 || y>=C) continue;
		if(temp[x][y]!=pre) continue;
		dfs(x,y,pre,post);
	}
}
	

VVI apply(int col)
{
	temp=grid;
	dfs(0,0,temp[0][0],col);
	return temp;
}

VVI apply(const VVI &grid,int col)
{
	temp=grid;
	dfs(0,0,temp[0][0],col);
	return temp;
}

bool isSolved(const VVI &grid)
{
	bool occ[6];
	fill(occ,false);
	for(int i=0;i<R;i++)
		for(int j=0;j<C;j++)
			occ[grid[i][j]]=true;
	
	int cnt=0;
	for(int i=0;i<6;i++)
		if(occ[i])
			cnt++;
			
	return cnt==1;
}

bool canComplete(int col)
{
	int cnt=0;
	for(int i=0;i<R;i++)
		for(int j=0;j<C;j++)
			if(grid[i][j]==col)
				cnt++;
	if(cnt==0) return false;
	
	VVI oldg = grid;
	grid = apply(col);
	grid = apply(7-col);
	
	cnt=0;
	for(int i=0;i<R;i++)
		for(int j=0;j<C;j++)
			if(grid[i][j]==col)
				cnt++;
				
	grid = oldg;
	
	return cnt==0;
}
	
struct node
{
	VVI grid;
	int h;
	int par,chng;
	
	node(){}
	
	node(const VVI &_grid,int _h,int _par,int _chng)
	{
		grid=_grid;
		h=_h;
		par=_par;
		chng=_chng;
	}	
};

bool operator < (node a,node b)
{
	return a.h < b.h;
}



vector<node> all;
int curbeg,curend;
int nxtbeg,nxtend;

int reconstruct(int ind)
{
	if(all[ind].par==-1) return 0;
	int z=reconstruct(all[ind].par);
	ans += (char)(rdict[all[ind].chng]);
	return z+1;
}

void solve()
{
	ans="";	
	all.clear();
	all.resize(beamWidth*5 + beamWidth*30 + 100);
	all[0]=node(grid,getH(grid),-1,-1);
	
	curbeg=0;
	curend=1;	
	int solvedInd=-1;
	
	while(true)
	{
		nxtbeg=curend;
		nxtend=nxtbeg;
		
		for(int i=curbeg;i<curend;i++)
		{
			if(isSolved(all[i].grid))
			{
				solvedInd=i;
				break;
			}
			
			for(int j=0;j<6;j++)
			{
				if(j==all[i].grid[0][0]) continue;
				VVI temp = apply(all[i].grid,j);
				all[nxtend++] = node(temp, getH(temp), i, j);
			}
		}		
		if(solvedInd >= 0)
			break;					
			
		sort(&all[nxtbeg],&all[nxtend]);		
		if(nxtend-nxtbeg > beamWidth)
			nxtend = nxtbeg + beamWidth;
		
		curbeg=nxtbeg;
		curend=nxtend;
	}	
	
	int len = reconstruct(solvedInd);
	printf("%d\n",ans.size());
	printf("%s\n",ans.c_str());
}

bool randomInput()
{
	R=14;
	C=14;
	grid.resize(R);
	for(int i=0;i<R;i++)
		grid[i].resize(C);
		
	for(int i=0;i<6;i++)
		rdict[i]=(char)('1'+i);
	
	for(int i=0;i<R;i++)
		for(int j=0;j<C;j++)
			grid[i][j] = (rand()>>((i+j)%14)) % 6;
	
	display(grid);
	return true;
}	


void input()
{
	s(R); //s(C);
	C=R;
	
	char temp[105];
	grid.resize(R);
	for(int i=0;i<R;i++)
	{
		ss(temp);
		grid[i].resize(C);
		for(int j=0;j<C;j++)
		{
			char c=temp[j];
			int val;
			if(dict.count(c)>0)
				val=dict[c];
			else
			{
				int z = dict.size();
				dict[c] = z;
				rdict[z] = c;
				val = z;
			}
			grid[i][j]=val;
		}
	}
}

int main()
{
	input();		// change to randomInput() if desired
	solve();
}
