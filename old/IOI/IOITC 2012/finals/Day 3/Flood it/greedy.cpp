# include <cstdio>
# include <set>
# include <algorithm>
# include <queue>
# include <cstring>
using namespace std;

typedef vector<char> VC;
typedef vector<VC> VVC;

int N;
char initboard[50][51];
int neigh[4][2]={{1,0},{0,1},{-1,0},{0,-1}};
char seen[50][50];
char clrs[]="ABCDEF";
const int LIM=1;

struct three
{
	VC seq;
	VVC board;
	int firstcomp;
};

bool operator<(const three& t1,const three& t2)
{
	if(t1.firstcomp!=t2.firstcomp)return t1.firstcomp<t2.firstcomp;
	return t1.seq<t2.seq;
}

set<three> boards[2];

bool isvalid(int i,int j)
{
	return (i>=0)&&(j>=0)&&(i<N)&&(j<N);
}

int findcomp(VVC& v)
{
	char clr=v[0][0];
	memset(seen,0,2500);
	seen[0][0]=1;
	
	int ret=0;
	queue<int>bfsqueue;
	bfsqueue.push(0);
	
	while(!bfsqueue.empty())
	{
		ret++;
		int i=bfsqueue.front()/N,j=bfsqueue.front()%N;
		bfsqueue.pop();
		
		for(int n=0;n<4;n++)
		{
			int ii=i+neigh[n][0],jj=j+neigh[n][1];
			if((!isvalid(ii,jj))||(seen[ii][jj])||(v[ii][jj]!=clr))continue;
			bfsqueue.push(ii*N+jj);
			seen[ii][jj]=1;
		}
	}
	return ret;
}

void flood(VVC& v,char c)
{
	char clr=v[0][0];
	memset(seen,0,2500);
	seen[0][0]=1;
	
	queue<int>bfsqueue;
	bfsqueue.push(0);
	
	while(!bfsqueue.empty())
	{
		int i=bfsqueue.front()/N,j=bfsqueue.front()%N;
		bfsqueue.pop();
		v[i][j]=c;
		
		for(int n=0;n<4;n++)
		{
			int ii=i+neigh[n][0],jj=j+neigh[n][1];
			if((!isvalid(ii,jj))||(seen[ii][jj])||(v[ii][jj]!=clr))continue;
			bfsqueue.push(ii*N+jj);
			seen[ii][jj]=1;
		}
	}
}

int main()
{
	scanf("%d",&N);
	for(int i=0;i<N;i++)
		scanf("%s",initboard[i]);
	
	three temp;
	temp.board.resize(N);
	for(int i=0;i<N;i++)
		for(int j=0;j<N;j++)
			temp.board[i].push_back(initboard[i][j]);
	temp.firstcomp=findcomp(temp.board);
	
	int cur=0,prev=1;
	boards[cur].insert(temp);
	
	while(1)
	{
		swap(cur,prev);
		boards[cur].clear();
		
		set<three>::reverse_iterator sit;
		for(sit=boards[prev].rbegin();sit!=boards[prev].rend();sit++)
		{
			for(int i=0;i<6;i++)
			{
				if(clrs[i]==sit->board[0][0])continue;
				temp=*sit;
				temp.seq.push_back(clrs[i]);
				flood(temp.board,clrs[i]);
				temp.firstcomp=findcomp(temp.board);
				if(temp.firstcomp==N*N)
				{
					for(int i=0;i<temp.seq.size();i++)
						printf("%c",temp.seq[i]);
					printf("\n");
					printf("%d\n",(int)temp.seq.size());
					return 0;
				}
				boards[cur].insert(temp);
				if(boards[cur].size()>LIM)boards[cur].erase(boards[cur].begin());
			}
		}
	}
}
