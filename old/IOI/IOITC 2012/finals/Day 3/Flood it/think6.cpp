# include <cstdio>
# include <iostream>
# include <algorithm>
# include <vector>
# include <cstring>
# include <cctype>
# include <set>
# include <map>
# include <cmath>
# include <queue>
# include <string>

using namespace std;

int R,C;
char board[100][100];
int parent[100000];
int neigh[4][2]={{0,1},{0,-1},{1,0},{-1,0}};
set<int> adjlist[10000];

int findparent(int u)
{
	if(parent[u]==u)return u;
	return parent[u]=findparent(parent[u]);
}

void merge(int u,int v)
{
	if(findparent(u)<findparent(v))swap(u,v);
	parent[findparent(u)]=findparent(v);
}

bool isvalid(int x,int y)
{
	return(x>=0&&y>=0&&x<R&&y<C);
}

set<int> edges;
set<int> colours;

char clr[]="ABCDEF";

int work(int n,int *test)
{
	set<int> component;
	component.insert(0);
	
	for(int i=0;i<n;i++)
	{
		set<int>additions;
		for(set<int>::iterator sit=component.begin();sit!=component.end();sit++)
		{
			int u=*sit;
			for(set<int>::iterator sit2=adjlist[u].begin();sit2!=adjlist[u].end();sit2++)
			{
				int v=*sit2;
				if((findparent(v)!=0)&&(board[v/C][v%C]==clr[test[i]]))additions.insert(v);
			}
		}
		for(set<int>::iterator sit=additions.begin();sit!=additions.end();sit++)component.insert(*sit);
	}
	return component.size();
}

int main()
{
	scanf("%d",&R);C=R;
	for(int i=0;i<R;i++)
		scanf("%s",board[i]);
	
	for(int i=0;i<R*C;i++)
		parent[i]=i;
	
	for(int i=0;i<R;i++)
		for(int j=0;j<C;j++)
		{
			for(int n=0;n<4;n++)
			{
				int ii=i+neigh[n][0],jj=j+neigh[n][1];
				if(!isvalid(ii,jj))continue;
				if(board[i][j]==board[ii][jj])merge(ii*C+jj,i*C+j);
			}
		}
	
	for(int i=0;i<R;i++)
		for(int j=0;j<C;j++)
		{
			colours.insert(findparent(i*C+j)*128+board[i][j]);
			for(int n=0;n<5;n++)
			{
				int ii=i+neigh[n][0],jj=j+neigh[n][1];
				if(!isvalid(ii,jj))continue;
				if(board[i][j]!=board[ii][jj])
				{
					int u=findparent(i*C+j),v=findparent(ii*C+jj);
					adjlist[u].insert(v);
					adjlist[v].insert(u);
				}
			}
		}
	
	char cur=board[0][0];
	while(!adjlist[0].empty())
	{
		int best=0,bestlen=0,bestar[10],test[10];
		for(int n=1,lim=6;n<=4;n++,lim*=6)
		{
			for(int i=0;i<lim;i++)
			{
				//printf("%d %d\n",i,lim);
				int j=i,thisval;
				for(int k=0;k<n;k++)
				{
					test[k]=j%6;
					j/=6;
					if(k>0&&test[k]==test[k-1])goto BPP;
				}
				
				thisval=work(n,test);
				if(thisval>best)
				{
					bestlen=n;
					best=thisval;
					for(int r=0;r<n;r++)bestar[r]=test[r];
				}
				BPP:;
			}
		}
		
		char bestcol=clr[bestar[0]];
		printf("%c",bestcol);
		set<int> tempset=adjlist[0];
		adjlist[0].clear();
		for(set<int>::iterator sit=tempset.begin();sit!=tempset.end();sit++)
		{
			int u=*sit;
			if(board[u/C][u%C]!=bestcol)adjlist[0].insert(u);
			else
			{
				parent[findparent(u)]=0;
				for(set<int>::iterator sit2=adjlist[u].begin();sit2!=adjlist[u].end();sit2++)
				{
					int v=*sit2;
					if(findparent(v)!=0)adjlist[0].insert(v);
				}
			}
		}
		
		cur=bestcol;
	}
	printf("\n");
	return 0;
}
