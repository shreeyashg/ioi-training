# include <stdio.h>
# include <string.h>

# define R	51
# define C	51

# define DEPTH	6
# define MARKED	'.'
# define XMARK	'x'
# define INF	1e9

int r, c, gmm;
char board[R][C];
char rang[6] = "ABCDEF";

int
solved(char board[R][C]) {
	int i, j;
	for(i=0; i<r; i++)
		for(j=0; j<c; j++)
			if(board[i][j]!=MARKED)
				return 0;
	return 1;
}

int
validcell(int x, int y) {
	return (x>=0 && x<r && y>=0 && y<c);
}

int
unexposed(char brd[R][C]) {
	int i, j, count = 0;
	for(i=0; i<r; i++)
		for(j=0; j<c; j++) {
	
			if(brd[i][j] == MARKED)
				continue;
				
			if(validcell(i-1,j))
				if(brd[i-1][j] == MARKED)
					continue;
				
			if(validcell(i+1,j))
				if(brd[i+1][j]==MARKED)
					continue;

			if(validcell(i,j-1))
				if(brd[i][j-1]==MARKED)
					continue;

			if(validcell(i,j+1))
				if(brd[i][j+1]==MARKED)
					continue;
			
			count++;
		}
	return count;
}

int
mark_color(char brd[R][C], char color) {
	int i, j, found=0, colored = 0;
	do {
		found = 0;
		for(i=0; i<r; i++)
			for(j=0; j<c; j++) {
				
				if(brd[i][j] != color)
					continue;
					
				if(validcell(i-1,j)) {
					if(brd[i-1][j]==MARKED) {
						found = 1;
						brd[i][j] = MARKED;
					}
				}

				if(validcell(i+1,j)) {
					if(brd[i+1][j]==MARKED) {
						brd[i][j] = MARKED;
						found = 1;
					}
				}

				if(validcell(i,j-1)) {
					if(brd[i][j-1]==MARKED) {
						brd[i][j] = MARKED;
						found = 1;
					}
				}

				if(validcell(i,j+1)) {
					if(brd[i][j+1]==MARKED) {
						brd[i][j] = MARKED;
						found = 1;
					}
				}
			}
			if(found)	colored = 1;
	}while(found);
	return colored;
}

int
marked_count(char brd[R][C]) {
	int i, j, count = 0;
	for(i=0; i<r; i++)
		for(j=0; j<c; j++)
			if(brd[i][j] == MARKED)
				count++;
	return count ;
}

int search(char brd[R][C], char color, int depth) {

	if((mark_color(brd, color)) == 0)
		return INF;

	int unexp = unexposed(brd);
		
	if(unexp == 0)
		return -depth;
	if(depth == 0) {
		int mc = marked_count(brd);
		gmm = mc>gmm?mc:gmm;
		return unexp;
	}
		
	int i, tmp, minval = INF;
	char brdt[R][C];
	
	for(i=0; i<6; i++)
		if(rang[i] != color) {
			memcpy(brdt, brd, R*C);
			tmp = search(brdt, rang[i], depth-1);
			if(tmp<minval)
				minval = tmp;
		}
	return minval;
}
	

int
main() {
	
	int i;
	scanf("%d", &r);
	c = r;
	for(i=0; i<r; i++)
		scanf("%s", board[i]);
	
	char color = board[0][0];
	
	board[0][0] = MARKED;
	mark_color(board, color);

	int count = 0, cc, tmp, bestval=INF, mm = INF;
	char brd[R][C], maxpclr;
	
	while(!solved(board)) {

		bestval = INF;	gmm = -INF;
		
		for(cc=0; cc<6; cc++) {

			if(rang[cc] == color) continue;
			
			memcpy(brd, board, R*C);
			tmp = search(brd, rang[cc], DEPTH);
			
			if(tmp == bestval) {
				if(mm<gmm)
					maxpclr = rang[cc];
				mm = gmm;
			}
			if(tmp<bestval) {
				bestval = tmp;
				maxpclr = rang[cc];
				mm = gmm;
			}
		}
		mark_color(board, maxpclr);
		printf("%c", maxpclr);fflush(stdout);

		count++;
		color = maxpclr;
	}
	printf("\n%d\n", count);
	return 0;
}
