/*
 Team Proof
 IIT Delhi
 
 C++ Template
 */


#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <cassert>
#include <string>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <stack>
#include <queue>
#include <cstdlib>
#include <fstream>
using namespace std;

#define s(T) scanf("%d", &T)
#define sl(T) scanf("%lld", &T)
#define fill(a, val) memset(a, val, sizeof(a))
#define mp make_pair

const int max_moves = 2505;

int totalCases, testNum;
int N, M, top;
vector <vector<int> > V[max_moves];
char chosen[max_moves];

int dx[] = {
	-1, 1, 0, 0
};

int dy[] = {
	0, 0, -1, 1
};

int mapping[256];
char revmapping[] = {
	'u', 'A', 'B', 'C', 'D', 'E', 'F'
};

vector <vector <int> > dfschg;
vector <vector <int> > cur;

bool isVert(int i, int j)
{
	return (i >= 0 && i < N && j >= 0 && j < M);
}

void dfs(int i, int j, int orig, int nxt)
{
	if(!isVert(i, j) || dfschg[i][j] != orig)
		return;
	dfschg[i][j] = nxt;
	for(int dir = 0; dir < 4; dir++)
	{
		int ni, nj;
		ni = i + dx[dir]; nj = j + dy[dir];
		dfs(ni, nj, orig, nxt);	
	}
}

vector <vector<int> > move(int chg, vector <vector <int> > attempt)
{
	if(chg >= 65) chg -= 64;
	if(chg >= '0') chg -= '0';
	
	dfschg = attempt;
	dfs(0, 0, dfschg[0][0], chg);
	return dfschg;
}

void display(vector <vector<int> > v)
{
	printf("\n");
	for(int i = 0; i < N; i++)
	{
		for(int j = 0; j < M; j++)
			printf("%c", revmapping[ v[i][j] ]);
		printf("\n");
	}
	printf("\n");
}


void preprocess()
{
	mapping['A'] = 1; mapping['B'] = 2; mapping['C'] = 3; mapping['D'] = 4; mapping['E'] = 5; mapping['F'] = 6;
}

bool input()
{
	cin >> N; M = N;
	string tmp;
	cur.clear();
	cur.resize(N);
	
	for(int i = 0; i < N; i++)
	{
		cur[i].resize(M);
		cin >> tmp;
		for(int j = 0; j < M; j++)
			cur[i][j] = mapping[tmp[j]];
	}	
	return true;
}

string m;
bool input(char* fname, char *ofname)
{
	ifstream fin(fname);
	fin >> N; M = N;
	string tmp;
	cur.clear();
	cur.resize(N);
	
	for(int i = 0; i < N; i++)
	{
		cur[i].resize(M);
		fin >> tmp;
		for(int j = 0; j < M; j++)
			cur[i][j] = mapping[tmp[j]];
	}	
	fin.close();
	
	ifstream fout(ofname);
	fout >> m;
	fout.close();
	return true;
}

bool done()
{
	for(int i = 0; i < N; i++)
		for(int j = 0; j < M; j++)
			if(cur[i][j] != cur[0][0])
				return false;
	return true;
}

bool valid(char ch)
{
	return (ch >= 'A' && ch <= 'F');
}

bool solve()
{
	top = 0;
	V[top++] = cur;
	
	int cptr = 0;
	int len = m.length();
	char consider;
	
	while(!done())
	{
		//display(cur);
		
		if(cptr == len)
		{
			cerr << ("Board incomplete after performing moves :(\n");
			return false;
		}
		
		consider = m[cptr++];
		
		if(!valid(consider))
		{
			cerr << ("Invalid character in Output file\n");
			return false;
		}
		
		if(consider == 'U')
		{
			assert(false);
			if(top == 1)
			{
				cerr << ("Already at initial position. Cannot undo!\n");
				continue;
			}
			top--;
			cur = V[top-1];
		}
		else 
		{
			if(top >= 2501)
			{
				cerr << ("Too many moves taken.\n");
				return false;
			}
			if(consider - 64 != cur[0][0])
				cur = move(consider, cur);
			chosen[top-1] = consider;
			V[top++] = cur;
		}
	}
	
	for(; cptr < len; cptr++)
		if(!valid(m[cptr]))
		{
			cerr << ("Invalid character in Output file\n");
			return false;
		}
	
	cout << len << endl;
	return true;
}

int main(int c, char* argv[])
{
	preprocess();
	//s(totalCases);
	totalCases = 1;
	for(testNum = 1; testNum <= totalCases; testNum++)
	{
		if(c != 3)
		{
			cerr << ("Incorrect number of arguments. Format ./checker infile youroutfile\n");
			return 0;
		}
		input(argv[1], argv[2]);
		if(!solve())
			cout << "0\n";
	}
}
