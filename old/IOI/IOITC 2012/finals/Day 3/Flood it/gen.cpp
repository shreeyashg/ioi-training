# include <cstdio>
# include <cstdlib>
# include <ctime>

using namespace std;

char str[]="ABCDEF";
char puzzle[55][55];

void
gen_puzzle(int R) {
	unsigned int iseed = (unsigned int) time(NULL);
	srand(iseed);
	int i, j;
	for(i=0;i<R;i++)
		for(j=0;j<R;j++) {
			puzzle[i][j] = str[rand()%6];
		puzzle[i][R] = '\0';
	}
}

void
bloat_component(int R, int k) {
	int l, m, i, j;
	for(i = 0; i<R+k; i+=k) {
		for(j = 0; j<R+k; j+=k) {
			char color = puzzle[i][j];
			for(l=0; l<k && i+l<R; l++)
				for(m=0; m<k && j+m<R; m++)
					if(0<(rand()%2))
						puzzle[i+l][j+m] = color;
					
		}
	}
}

void
print_puzzle(int R) {
	for(int i=0;i<R;i++)
			printf("%s\n", puzzle[i]);
}

int main()
{
	int R;
	
	scanf("%d", &R);
	printf("%d\n",R);
	
	gen_puzzle(R);
	bloat_component(R, 10);
	print_puzzle(R);
	
	return 0;
}
