/*
 Team Proof
 IIT Delhi
 
 C++ Template
 */


#include <iostream>
#include <cstdio>
#include <cassert>
using namespace std;

#define s(T) scanf("%d", &T)
#define sl(T) scanf("%lld", &T)
#define fill(a, val) memset(a, val, sizeof(a))
#define mp make_pair

const int MOD = 1000000007;
const int max_n = 5005;

int totalCases, testNum;
long long DP[max_n][max_n];
long long expected;

int N, prob;
int arr[max_n];
int push_pop_seq[2*max_n];

void preprocess()
{
	prob = 0;
	for(int j = 0; j < max_n; j++)
		DP[0][j] = 1;
	
	for(int i = 1; i < max_n; i++)
		for(int j = i; j < max_n; j++)
			DP[i][j] = (DP[i-1][j] + DP[i][j-1]);
}

bool input()
{
	if(prob == -1)
	{
//		printf("%d\n", N);
		return true;
	}
//	cerr << "Enter the value of N: ";
//	s(N); 
//	printf("%d\n", N);
	
	cerr << "Enter the probabilitycoeff that you will give a specific answer:";
	s(prob);
//	for(int i = 0; i < N; i++)
//		s(arr[i]);
	return true;
}

void solve()
{
	if(prob == -1)
	{
		//any random stack permutation
		int st[max_n]; int top = 0;
		int nextpush = 1;
		int k = 0;
		while(nextpush <= N || top > 0)
		{
			int r = rand() % 2;
			if(top == 0 || (r == 1) && nextpush <= N)
				st[top++] = nextpush++;
			else 
				arr[k++] = st[--top];
		}
	}
	else if(prob == 0 || (rand() % prob > 0))
	{
		//make a good answer
		cerr << "Enter expected answer: ";
		sl(expected);
		for(int i = 0; i < N - 32; i++)
			arr[i] = i+1;
		
		int l = 0;
		
		int pushLeft = 32, popLeft = 32;
		while(popLeft > 0)
		{
			if(DP[pushLeft][popLeft-1] > expected)
			{
				push_pop_seq[l++] = 0;
				popLeft--;
			}
			else 
			{
				expected -= DP[pushLeft][popLeft-1];
				push_pop_seq[l++] = 1;
				pushLeft--;
			}
		}
		
		int st[35]; int top = 0;
		st[top++] = 0;
		int k = N-32; l = 0;
		int nextpush = N-31;
		while(l < 64)
			if(push_pop_seq[l++] == 0)
				arr[k++] = st[--top];
			else 
				st[top++] = nextpush++;
		
	}
	else 
	{
		for(int i = 0; i < N; i++)
			arr[i] = i+1;
		for(int iter = 0; iter < N*N*5; iter++)
		{
			int i = rand() % N, j = rand()%N;
			swap(arr[i], arr[j]);
		}
	}
	
/*	if(!stackperm())
	{
		printf("-1\n");
		return;
	}
	
	int ans = 0;
	int pushLeft = N, popLeft = N;
	for(int i = 0; i < 2*N; i++)
	{
		if(push_pop_seq[i] == 1)
		{
			//see how many you have skipped over had you instead popped
			ans = (ans + DP[pushLeft][popLeft-1]) % MOD;
			pushLeft--;
		}
		else 
			popLeft--;
	}
	printf("%d\n", ans);*/
	
	for(int i = 0; i < N; i++)
		printf("%d%c", arr[i], (i == N-1 ? '\n' : ' '));
}

int main()
{
	srand(time(NULL));
	preprocess();

	cerr << "Enter the value of N: ";
	s(N);
	
	cerr << "Enter the number of queries Q: ";
	s(totalCases);
	printf("%d %d\n", N, totalCases);
	for(testNum = 1; testNum <= totalCases; testNum++)
	{
		if( !input())
			break;
		solve();
	}
}
