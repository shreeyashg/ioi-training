/*
 *  shift.cpp
 */
#include <cstdio>
#define s(T) scanf("%d", &T)
int main()
{
	int N = 5000, Q = 2000;
	printf("%d %d\n", N, Q);
	s(Q);
	for(int i = 0; i < Q; i++)
	{
		s(N);
		int x;
		for(int j = 0; j < N; j++)
		{	
			s(x);
			printf("%d%c", x, (j == N-1? '\n' : ' '));
		}
	}
}
