/*
 	Team Proof
	IIT Delhi
 
	C++ Template
 */


#include <iostream>
#include <cstdio>
#include <cassert>
using namespace std;

#define s(T) scanf("%d", &T)
#define sl(T) scanf("%lld", &T)
#define fill(a, val) memset(a, val, sizeof(a))
#define mp make_pair

const int MOD = 1000000007;
const int max_n = 5005;

int totalCases, testNum;
int DP[max_n][max_n];

int N;
int arr[max_n];
int push_pop_seq[2*max_n];

void preprocess()
{
	for(int j = 0; j < max_n; j++)
		DP[0][j] = 1;
	
	for(int i = 1; i < max_n; i++)
		for(int j = i; j < max_n; j++)
			DP[i][j] = (DP[i-1][j] + DP[i][j-1]) % MOD;
}

bool input()
{
	//s(N);
	for(int i = 0; i < N; i++)
		s(arr[i]);
	return true;
}

bool stackperm()
{
	int st[max_n]; int top=0;
	st[top++] = 0;				//insert 0 as a dummy value
	int k = 0, l = 0;
	int nextpush = 1;
	while(k < N)
	{
		int cur = arr[k++];
		while(nextpush <= cur)
			st[top++] = nextpush++, push_pop_seq[l++] = 1;
		
		if(cur != st[top-1])
			return false;
		top--,	push_pop_seq[l++] = 0;
		
	}
	assert(top == 1);
	assert(l == 2*N);
	return true;
}

void solve()
{
	bool found[5005];
	fill(found, false);
	for(int i = 0; i < N; i++)
	{
		assert(!found[arr[i]]);
		found[arr[i]] = true;
		assert (arr[i] >= 1 && arr[i] <= N);
	}
	
	
	if(!stackperm())
	{
		printf("-1\n");
		return;
	}
	
	int ans = 0;
	int pushLeft = N, popLeft = N;
	for(int i = 0; i < 2*N; i++)
	{
		if(push_pop_seq[i] == 1)
		{
			//see how many you have skipped over had you instead popped
			ans = (ans + DP[pushLeft][popLeft-1]) % MOD;
			pushLeft--;
		}
		else 
			popLeft--;
	}
	printf("%d\n", ans);
}

int main()
{
	preprocess();
	s(N);
	s(totalCases);
	
	assert(N <= 5000 && totalCases <= 2000);
	
	for(testNum = 1; testNum <= totalCases; testNum++)
	{
		if( !input())
			break;
		solve();
	}
}
