#include<iostream>
#include<cstdio>
#include<stack>
#include<cassert>

using namespace std;

typedef long long LL;

const int max_n=5000;
const LL modref=1000000007;

int n,q;
int qry[max_n]; int qryct;
int brc[2*max_n]; int brcsz;
LL dp[max_n+1][max_n+1]={0};

int ct[max_n+1];
bool isqryperm()
{
	for(int i=1; i<=n; ++i)
		ct[i]=0;
	for(int i=0; i<n; ++i)
		++ct[qry[i]];
	for(int i=1; i<=n; ++i)
		if(ct[i]!=1)
			return false;
	return true;
}

int main()
{
	scanf("%d %d",&n,&q);
	assert(1<=n and n<=5000 and 1<=q and q<=2000);
	
	for(int j=0; j<n; ++j)
		dp[n][j]=1;
	//for(int i=1; i<n; ++i)
		//dp[i][n]=0;
	
	for(int i=n-1; i>=0; --i)
	{
		for(int j=i; j>=0; --j)
		{
			dp[i][j]+=dp[i+1][j]; dp[i][j]%=modref;
			dp[i][j]+=dp[i][j+1]; dp[i][j]%=modref;
		}
	}
	
	int l[max_n],lct=0;
	stack<int> s;
	for(int z=0; z<q; ++z)
	{
		while(!s.empty())
			s.pop();
		
		qryct=0;
		for(int i=0; i<n; ++i)
			scanf("%d",&qry[i]);
		
		assert(isqryperm());
		
		lct=0;
		for(int i=0; i<n; ++i)
			l[i]=((LL)(i+1));
		
		brcsz=0;
		bool stperm;
		while(true)
		{
			if((not s.empty()) and qry[qryct]==s.top())
			{
				s.pop();
				++qryct;
				brc[brcsz]=-1;
				++brcsz;
				if(qryct==n)
				{
					stperm=true;
					break;
				}
			}
			else
			{
				if(lct==n)
				{
					stperm=false;
					break;
				}
				s.push(l[lct]);
				++lct;
				brc[brcsz]=1;
				++brcsz;
			}
		}
		
		if(not stperm)
		{
			printf("-1\n");
			continue;
		}
		
		//for(int i=0; i<brcsz; ++i)
			//cout<<brc[i]<<" ";
		//cout<<endl;
		
		//cout<<"$"<<dp[0][1]<<" "<<dp[1][1]<<" "<<dp[2][1]<<endl;
		
		assert(brcsz==2*n);
		long long res=0;
		int oct=0, cct=0;
		for(int i=0; i<2*n; ++i)
		{
			if(brc[i]==1)
			{
				res+=dp[oct][cct+1];
				res%=modref;
				++oct;
			}
			else
				++cct;
		}
		
		printf("%lld\n",res);
	}
	
	return 0;
}
