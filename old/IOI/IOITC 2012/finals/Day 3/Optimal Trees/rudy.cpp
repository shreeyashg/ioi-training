#include<vector>
#include<stack>
#include<set>
#include<map>
#include<queue>
#include<deque>
#include<string>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<cassert>
#include<cstdlib>
#include<cstdio>
#include<cmath>
#include<string>

using namespace std;

#define s(n)					scanf("%d",&n)
#define sl(n) 					scanf("%lld",&n)
#define sf(n) 					scanf("%lf",&n)
#define ss(n) 					scanf("%s",n)
#define INF						(int)1e9
#define LINF					(long long)1e18
#define EPS						1e-9
#define maX(a,b)				((a)>(b)?(a):(b))
#define miN(a,b)				((a)<(b)?(a):(b))
#define abS(x)					((x)<0?-(x):(x))
#define FOR(i,a,b)				for(int i=a;i<b;i++)
#define REP(i,n)				FOR(i,0,n)
#define foreach(v,c)            for( typeof((c).begin()) v = (c).begin();  v != (c).end(); ++v)
#define mp						make_pair
#define FF						first
#define SS						second
#define tri(a,b,c)				mp(a,mp(b,c))
#define XX						first
#define YY						second.first
#define ZZ						second.second
#define pb						push_back
#define fill(a,v) 				memset(a,v,sizeof a)
#define all(x)					x.begin(),x.end()
#define SZ(v)					((int)(v.size()))
#define DREP(a)					sort(all(a)); a.erase(unique(all(a)),a.end())
#define INDEX(arr,ind)			(lower_bound(all(arr),ind)-arr.begin())
#define debug(args...)			{dbg,args; cerr<<endl;}
#define dline					cerr<<endl	

void sc(char &c){
	char temp[4];	ss(temp);	
	c=temp[0];
}

struct debugger
{
	template<typename T> debugger& operator , (const T& v)
	{	
		cerr<<v<<" ";	
		return *this;	
	}
} dbg;

void debugarr(int * arr,int n)
{
	cout<<"[";
	for(int i=0;i<n;i++)
		cout<<arr[i]<<" ";
	cout<<"]"<<endl;
}





typedef long long LL;
typedef pair<int,int> PII;
typedef pair<LL,int> PLI;
typedef pair<LL,LL> PLL;
typedef pair<int,PII> TRI;

typedef vector<int> VI;
typedef vector<LL> VL;
typedef vector<PII> VII;
typedef vector<PLL> VLL;
typedef vector<TRI> VT;

typedef vector<VI> VVI;
typedef vector<VL> VVL;
typedef vector<VII> VVII;
typedef vector<VLL> VVLL;
typedef vector<VT> VVT;


/*Main code begins now */

int N,M;
VII adjl[100005];

const LL mod = 1000000007;
LL ans;
LL dist[100005];
int cnt[100005];
set<PLI> q;




int main()
{
	s(N); s(M);
	int a,b,d;
	for(int i=0;i<M;i++)
	{
		s(a); s(b); s(d);
		adjl[a].pb(mp(b,d));
		adjl[b].pb(mp(a,d));
	}
	
	for(int i=0;i<N;i++)
	{
		dist[i]=LINF;
		cnt[i]=0;
	}
	
	ans=1;
	q.insert(mp(0,0));
	dist[0]=0;
	cnt[0]=1;
	
	while(!q.empty())
	{
		PLI head = *q.begin();
		q.erase(q.begin());
		int u=head.SS;
		LL d=head.FF;
		if(d>dist[u]) continue;
		
		ans = (ans*cnt[u])%mod;
		
		int len=adjl[u].size();
		for(int i=0;i<len;i++)
		{
			int v=adjl[u][i].FF;
			LL dnew = d + adjl[u][i].SS;
			
			if(dnew<dist[v])
			{
				q.insert(mp(dnew,v));
				dist[v]=dnew;
				cnt[v]=1;
			}
			else if(dnew==dist[v])
				cnt[v]++;
		}
	}
	
	for(int i=0;i<N;i++)
		if(dist[i]>=LINF/2)
			ans=0;
			
	printf("%lld\n",ans);
}
