import java.io.*;
import java.util.*;
import java.math.*;

public class Gen1
{
	public static void main(String[] args) throws IOException
	{
		Random rand=new Random();
		int A=3;
		int B=50000;
		int N=1+A+B;
		int M=A+A*B;
		
		System.out.printf("%d %d\n",N,M);
		for(int i=0;i<A;i++)
			System.out.printf("%d %d %d\n",0,1+i,1000);
		for(int i=0;i<A;i++)
			for(int j=0;j<B;j++)
				System.out.printf("%d %d %d\n",1+i,1+A+j,3000+rand.nextInt(10));
	}
	
	static void debug(Object...os)
	{
		System.out.println(Arrays.deepToString(os));
	}
}
