import java.io.*;
import java.util.*;
import java.math.*;

public class Gen
{
	public static void main(String[] args) throws IOException
	{
		Random rand=new Random();
		HashSet<Long> edges = new HashSet<Long>();
		int N=100000;
		int M=0;
		int low=1;
		
		while(low!=N)
		{
			int ind = rand.nextInt(low);
			edges.add(((long)ind<<20) + low);
			M++;
			low++;
		}
		
		int target=N*2;
		while(M<target)
		{
			int u=rand.nextInt(N);
			int v=rand.nextInt(N);
			if(u==v || edges.contains(((long)u<<20)+v) || edges.contains(((long)v<<20)+u)) continue;
			edges.add(((long)u<<20)+v);
			M++;
		}
		
		int[] perm=new int[N];
		for(int i=0;i<N;i++)
			perm[i]=i;
		
			
		for(int i=N-1;i>=0;i--)
		{
			int ind = rand.nextInt(i+1);
			int temp = perm[i];
			perm[i] = perm[ind];
			perm[ind] = temp;
		}
		
		System.out.printf("%d %d\n",N,M);
		int mask = (1<<20)-1;
		for(Long z:edges)
		{
			System.out.printf("%d %d %d\n",perm[(int)(z>>20)],perm[(int)(z&mask)],rand.nextInt(2)+1000);
		}
	}
	
	static void debug(Object...os)
	{
		System.out.println(Arrays.deepToString(os));
	}
}
