/*
 	Team Proof
	IIT Delhi
 
	C++ Template
 */


#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <cassert>
#include <string>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <stack>
#include <queue>
#include <cstdlib>
using namespace std;

#define s(T) scanf("%d", &T)
#define sl(T) scanf("%lld", &T)
#define fill(a, val) memset(a, val, sizeof(a))
#define mp make_pair

const int max_n = 100005;
const long long INF = 1000000000000000ll;
const int MOD = 1000000007;

int totalCases, testNum;
int N, M;

vector <pair<int, int> >adjl[max_n];
long long dist[max_n];
int parents[max_n];

set <pair <int, int> > edges;

void preprocess()
{
	
}

bool input()
{
	s(N);
	s(M);
	
	assert(N <= 100000 && M <= 200000);
	for(int i = 0; i < M; i++)
	{
		int a, b, c;
		s(a);
		s(b);
		
		assert(a != b);
		if(a > b) swap(a, b);
		assert(edges.find(mp(a,b)) == edges.end());
		edges.insert(mp(a,b));
		
		assert(a >= 0 && a < N);
		assert(b >= 0 && b < N);
		
		s(c);
		
		assert(c > 0 && c <= 1000000000);
		adjl[a].push_back(mp(b, c));
		adjl[b].push_back(mp(a, c));
		
		
	}
	return true;
}

void solve()
{
	for(int i = 0; i < N; i++)
		dist[i] = INF;
	fill(parents, 0);
	
	multiset <pair<long long, int> > q;
	q.insert(make_pair(0, 0));
	parents[0] = 1;
	
	while(!q.empty())
	{
		pair <long long, int> cur = *q.begin();
		q.erase(q.begin());
		
		int u = cur.second;
		long long cons = cur.first;
		if(cons < dist[u])
		{
			parents[u] = 1;
			dist[u] = cons;
			
			for(int i = 0; i < adjl[u].size(); i++)
			{
				int v = adjl[u][i].first; int d = adjl[u][i].second;
				q.insert(mp(cons + d, v));
			}
		}
		else if(cons == dist[u])
			parents[u]++;
	}
	
	long long ans = 1;
	for(int i = 0;i < N; i++)
		ans = (ans * parents[i]) % MOD;
	
	printf("%lld\n", ans);
}

int main()
{
	preprocess();
	//s(totalCases);
	totalCases = 1;
	for(testNum = 1; testNum <= totalCases; testNum++)
	{
		if( !input())
			break;
		solve();
	}
}
