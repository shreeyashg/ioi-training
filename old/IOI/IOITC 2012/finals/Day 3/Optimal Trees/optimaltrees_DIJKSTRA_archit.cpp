#include<iostream>
#include<cstdio>
#include<set>
#include<vector>
#include<limits>

using namespace std;

typedef long long LL;
const LL inv=numeric_limits<LL>::max();
const LL modref=1000000007;
const int max_n=100000;

struct edge
{
	int nb;
	LL wt;
	edge(){}
	edge(int nb_, LL wt_){nb=nb_; wt=wt_;}
};

struct entry
{
	int vx;
	LL bt;
	entry(){}
	entry(int vx_, LL bt_){ vx=vx_; bt=bt_; }
};

struct less_entry : public binary_function <entry,entry,bool>
{
	bool operator()(const entry &a, const entry &b){ return a.bt<b.bt or (a.bt==b.bt and a.vx<b.vx); }
};

set<entry,less_entry> S;
set<entry,less_entry>::iterator it;

int n,m;
vector<edge> adjl[max_n];
bool burnt[max_n]={0};
int burntsz=0;
LL ebt[max_n];
LL npar[max_n];
LL res;

int main()
{
	scanf("%d %d",&n,&m);
	
	int u,v; long long w;
	for(int i=0; i<m; ++i)
	{
		scanf("%d %d %lld",&u,&v,&w);
		adjl[u].push_back(edge(v,w));
		
		//scanf("%d %d %lld",&u,&v,&w);
		adjl[v].push_back(edge(u,w));
	}
	
	fill(ebt,ebt+n,inv);
	ebt[0]=0;
	npar[0]=1;
	for(int i=0; i<n; ++i)
		S.insert(entry(i,ebt[i]));
	//cerr<<"@ "<<S.size()<<" n:"<<n<<endl;
	
	//for (it=S.begin(); it!=S.end(); it++)
    	//cerr << "! " << (*it).vx << " " << (*it).bt <<endl;
	
	int vt,vtnbr; LL vtnbrwt;
	LL vtbt;
	while(!S.empty())
	{
		it=S.begin();
		vt=(*it).vx;
		vtbt=(*it).bt;
		S.erase(it);
		
		//cerr<<vt<<" "<<npar[vt]<<endl;
		
		if(vtbt==inv)
			break;
		
		burnt[vt]=true;
		++burntsz;
		
		for(int i=0; i<adjl[vt].size(); ++i)
		{
			vtnbr=adjl[vt][i].nb;
			vtnbrwt=adjl[vt][i].wt;
			
			//if(vt==0 and vtnbr==2)
			if(ebt[vtnbr]==vtbt+vtnbrwt)
			{
				//cout<<vt<<" "<<vtnbr<<" "<<vtbt<<" "<<vtnbrwt<<" "<<ebt[vtnbr]<<endl;
				++npar[vtnbr];
			}
			else if(ebt[vtnbr]>vtbt+vtnbrwt)
			{
				it=S.find(entry(vtnbr,ebt[vtnbr]));
				S.erase(it);
				ebt[vtnbr]=vtbt+vtnbrwt;
				npar[vtnbr]=1;
				S.insert(entry(vtnbr,ebt[vtnbr]));
			}
		}
	}
	
	if(burntsz!=n)
		printf("0\n");
	else
	{
		res=1;
		
		for(int i=0; i<n; ++i)
		{
			res*=npar[i];
			res%=modref;
		}
		
		printf("%lld\n",res);
	}
	
	return 0;
}
