#include<vector>
#include<stack>
#include<set>
#include<map>
#include<queue>
#include<deque>
#include<string>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<cassert>
#include<cstdlib>
#include<cstdio>
#include<cmath>
#include<string>

using namespace std;

#define s(n)					scanf("%d",&n)
#define sl(n) 					scanf("%lld",&n)
#define sf(n) 					scanf("%lf",&n)
#define ss(n) 					scanf("%s",n)
#define fill(a,v) 				memset(a,v,sizeof a)
#define all(x)					x.begin(),x.end()

typedef long long LL;

/*Main code begins now */

int testnum;
int R,C,K,D;
int posx[305],posy[305];
LL dp[305][305];

LL memo(int ind,int cnt)
{
	LL & ans = dp[ind][cnt];
	if(ans>=0) return ans;
	
	if(cnt==1)
	{
		ans = posx[ind]+posy[ind];
		return ans;
	}
	
	ans = (LL)1e18;
	for(int i=0;i<D;i++)
	{
		if(posx[i] >= posx[ind]) continue;
		LL d = memo(i,cnt-1) + abs(posx[ind]-posx[i]) + abs(posy[ind]-posy[i]);
		ans = min(ans, d);
	}
	return ans;
}

void solve()
{
	fill(dp,-1);
	LL ans = (LL)1e18;
	for(int i=0;i<D;i++)
		ans = min(ans, memo(i,K));
	printf("%lld\n",ans);
}

bool input()
{
	s(R); s(C); s(K); s(D);
	assert(1<=K);
	assert(K<=D);
	assert(D<=R);
	assert(R<=1000000000);
	assert(C<=1000000000);
	for(int i=0;i<D;i++)
	{
		s(posx[i]);
		s(posy[i]);
		assert(!(posx[i]==0 && posy[i]==0));
		assert(posx[i]>=0 && posx[i]<R);
		assert(posy[i]>=0 && posy[i]<C);
	}
	
	for(int i=0;i<D;i++)
		for(int j=i+1;j<D;j++)
			if(posx[i]==posx[j])
				assert(false);
	return true;
}

int main()
{
	input();
	solve();
}
