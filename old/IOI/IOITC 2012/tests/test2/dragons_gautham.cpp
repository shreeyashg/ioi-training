#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<utility>
#include<iostream>
#include<functional>

using namespace std;
#define MAXDRAG 302
#define MAXINT 1e18

pair <long long int, long long int> dragons[MAXDRAG];
long long int min(long long int a, long long int b)
{
	if (a <= b)
		return a;
	return b;
}

long long int ptdistance(const pair <long long int,long long int> &a, const pair<long long int,long long int> &b)
{
	return abs(a.first - b.first) + abs(a.second - b.second);
}

long long int mydistance (long long int i, long long int k)
{
	return ptdistance(dragons[i], dragons[k]);
}

int main()
{
	long long int R, C, K, D;
	long long int i, tempx, tempy, j, k;
	long long int dp[MAXDRAG][MAXDRAG], min_dist = MAXINT;
	pair <int, int> start = make_pair(0,0);

	cin >> R >> C >> K >> D;
	for (i = 0; i < D; i++) {
		cin >> tempx >> tempy;
		dragons[i] = make_pair(tempx, tempy);
	}

	sort(dragons, dragons+D);

	for (i = 0; i < D; i++) {
		dp[i][1] = 0;
	}

	for (j = 2; j <= K ;j++) {
		for (i = 0;i < D; i++){
			dp[i][j] = MAXINT;
			for (k = i+1; k < D; k++){
				dp[i][j] = min(dp[i][j], (dp[k][j-1] + mydistance(i,k)));
			}
	        }
	}

	for (i = 0; i < D; i++) {
		min_dist = min(min_dist, ptdistance(start,dragons[i])+dp[i][K]);
	}
	cout << min_dist << "\n";
	return 0;
}
