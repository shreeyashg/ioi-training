/*
 *  Solution for test 2 - Here there be Dragons
 * Code by Pradeep
 */

#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <cstring>
using namespace std;

#define s(T) scanf("%d", &T)

const int D_Max = 305;
const long long INF = (long long)1000000000000ll;			//10^12, greater than any possible path that would kill K dragons

int R, C, K, D;
long long F[D_Max][D_Max];
vector <pair <int, int> > posns;	//positions of the D dragons, sorted by rows

long long f(int i, int k)			// return the minimum amount of moves starting from the position of i'th dragon, assuming you have killed 'k' dragons so far till the end
{
	if(F[i][k] >= 0)
		return F[i][k];
	
	if(k >= K)		// we have killed all that we need
		return F[i][k] = 0;

	long long ans = INF;
	int currow, nxtrow, curcol, nxtcol;

	currow = posns[i].first, curcol = posns[i].second;
	for(int j = i+1; j < D; j++)
	{	
		nxtrow = posns[j].first;
		nxtcol = posns[j].second;
		ans = min(ans, abs(currow-nxtrow) + abs(curcol-nxtcol) + f(j, k+1));
	}
	return F[i][k] = ans;
}

void input()
{
	s(R);
	s(C);
	s(K);
	s(D);
	
	for(int i = 0; i < D; i++)
	{
		int x, y;
		s(x); s(y);
		posns.push_back(make_pair(x, y));
	}
}

void solve()
{
	sort(posns.begin(), posns.end());
	for(int i = 0; i < D_Max; i++)
		for(int j = 0; j < D_Max; j++)
			F[i][j] = -1;
	//find which is the first dragon you've killed optimally and use that
	long long ans = INF;
	for(int i = 0; i < D; i++)
		ans = min(ans, posns[i].first + posns[i].second + f(i, 1));				//starting from (0,0), going to (xi, yi) and killing dragon i first
	printf("%lld\n", ans);
}

int main()
{
	input();
	solve();
}
