#include <stdio.h>
#include <stdint.h>

#define INF	1e16

int R, C, K, D;
int dposx[300], dposy[300];
uint64_t kkill[300][300];

uint64_t
min(uint64_t a, uint64_t b) {
	if(a > b)
		return b;
	return a;
}

int
absol(int a) {
	if(a < 0)
		return -a;
	return a;
}

int
dist2dragon(int u, int v) {
	return absol(dposx[u] - dposx[v]) + absol(dposy[u] - dposy[v]);
}

void bubbleSort()
{
  int i, j, temp, tempy;
 
  for (i = (D - 1); i > 0; i--)
  {
    for (j = 1; j <= i; j++)
    {
      if (dposx[j-1] > dposx[j])
      {
        temp = dposx[j-1];
        dposx[j-1] = dposx[j];
        dposx[j] = temp;
        
        tempy = dposy[j-1];
        dposy[j-1] = dposy[j];
        dposy[j] = tempy;
      }
    }
  }
}

int
main() {
	scanf("%d %d %d %d", &R, &C, &K, &D);
	
	int i;
	for(i=0; i<D; i++)
		scanf("%d %d", &dposx[i], &dposy[i]);
	
	bubbleSort();
	/*kkill[d][n] = min_{p<d} (kkill[p][n-1]+wt[p to d] */
	
	int n, p, d;
	
	for(d=0; d<D; d++)
		for(n=0; n<K; n++)
			kkill[d][n] = (uint64_t) INF;
	
	for(p=0; p<D; p++)
		kkill[p][0] = dposx[p]+dposy[p];
	
	for(n=1; n<K; n++)
		for(d=n; d<D; d++)
			for(p=n-1; p<d; p++)
				kkill[d][n] =
					min(kkill[d][n], kkill[p][n-1]+dist2dragon(d, p));
	
	uint64_t dmin = (uint64_t) INF;
	for(i=0; i<D; i++)
		dmin = min(dmin, kkill[i][K-1]);
	
	printf("%llu\n", dmin);
	
	return 0;
}
