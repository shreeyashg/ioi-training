import java.io.*;
import java.util.*;
import java.math.*;

public class Gen
{
	public static void main(String[] args) throws IOException
	{
		Random rand=new Random();
		int R,C,K,D;
		R = 1000000000;
		C = 1000000000;
		D = 300;
		K = 275;
		
		int[] posx=new int[D];
		int[] posy=new int[D];
		
		for(int i=0;i<D;i++)
		{
			int x,y;
			
			while(true)
			{
				x = rand.nextInt(R);
				boolean ok=true;
				for(int j=0;j<i;j++)
					if(posx[j]==x)
						ok=false;
				if(ok) break;
			}			
			while(true)
			{
				y=rand.nextInt(C);
				if(!(x==0 && y==0)) break;
			}
			
			posx[i]=x;
			posy[i]=y;
		}

		
		System.out.printf("%d %d %d %d\n",R,C,K,D);
		for(int i=0;i<D;i++)
			System.out.printf("%d %d\n",posx[i],posy[i]);
	}
}


