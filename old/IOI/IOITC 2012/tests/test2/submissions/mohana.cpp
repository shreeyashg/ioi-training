#include<iostream>
#include<algorithm>
using namespace std;
#define fs first
#define sc second
pair <long int,long int>a[310];
int r,c,k,d;
long long int dp[310][310];
long long int dis;

int main(){
	ios::sync_with_stdio(false);
    int i,j,k;
    cin>>r>>c>>k>>d;
    for(i=0;i<d;i++){
	cin>>a[i].fs>>a[i].sc;
    }
    
    sort(a,a+d);
    dis=0;
    
    for(i=0;i<d;i++){
	dp[1][i]=a[i].fs+a[i].sc;
    }
    
    
   for(i=2;i<=k;i++){
       
	for(j=i-1;j<d;j++){
		dp[i][j]=dp[i-1][i-2]+abs(a[j].fs-a[i-2].fs)+abs(a[j].sc-a[i-2].sc);
		for(k=i-1;k<j;k++){
		    dp[i][j]=min(dp[i][j],dp[i-1][k]+abs(a[j].fs-a[k].fs)+abs(a[j].sc-a[k].sc));
		}
	}
   }
  dis=dp[k][k-1]; 
  for(i=k;i<d;i++){
	dis=min(dis,dp[k][i]);
  }
  
  cout<<dis;
  
  return 0;
}
