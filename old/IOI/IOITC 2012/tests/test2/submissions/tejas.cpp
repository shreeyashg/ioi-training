#include <iostream>
#include <vector>
#include <cstdlib>
#include <algorithm>
using namespace std;

class dragon
{
	public:
	long long int row, column;
};

bool comp (dragon aa, dragon bb)
{
	if (aa.row < bb.row)
	{
		return true;
	}
	else return false;
}

long long int distances[300][300];

dragon dragons[300];

long long int dist_dragon (int a, int b)
{
	long long int sum = abs(dragons[a].row - dragons[b].row) + abs(dragons[a].column - dragons[b].column);
	return sum;
	
}

int main ()
{
	long long int dist = 2000000001;
	long long int first_check;
	long long int r,c;
	int k,d;
	cin >> r >> c >> k >> d;
	
	for (int i = 0; i < d; ++i)
	{
		cin >> dragons[i].row >> dragons[i].column;
	}
	
	sort (dragons, dragons+d, comp);
	
	if (k == d)
	{
		dist = dragons[0].row + dragons[0].column;
		for (int i = 0; i < d-1; ++i)
		{
			dist += dist_dragon(i, i+1);
		}
		cout << dist << endl;
		return 0;
	}
	
	/*for (int i = 0; i < d; ++i)
	{
		cout << dragons[i].row << " " << dragons[i].column << endl;
	}*/
	
	for (int i = 0; i < d; ++i) //initialise to max dist
	{
		for (int j = 0; j < k; ++j)
		{
			distances[i][j] = 2000000000;
		}
	}
	
	distances[0][0] = dragons[0].row + dragons[0].column;
	
	for (int i = 1; i < d; ++i)
	{
		distances[i][0] = dragons[i].row + dragons[i].column;
		if (i < k)
		{
			for (int j = 1; j < k-i+1; ++j)
			{
				if (d-k-1 < 0)
				{
					first_check = 0;
				}
				else
				{
					first_check = d-k-1;
				}
				for (int c = first_check; c < i; ++c)
				{
					if ((dist_dragon(c, i) + distances[c][j-1]) < distances[i][j])
					{
						distances[i][j] = dist_dragon(c, i) + distances[c][j-1];
					}
				}
			}
		}
		else
		{
			for (int j = 1; j < k+1; ++j)
			{
				if (d-k-1 < 0)
				{
					first_check = 0;
				}
				else
				{
					first_check = d-k-1;
				}
				for (int c = first_check; c < i; ++c)
				{
					if ((dist_dragon(c, i) + distances[c][j-1]) < distances[i][j])
					{
						distances[i][j] = dist_dragon(c, i) + distances[c][j-1];
					}
				}
			}
		}
	}
	
	/*for (int i = 0; i < d; ++i)
	{
		for (int j = 0; j < k+1; ++j)
		{
			cout << distances[i][j] << " ";
		}
		cout << endl;
	}*/
	
	for (int i = 0; i < d; ++i)
	{
		if (distances[i][k-1] < dist)
		{
			dist = distances[i][k-1];
		}
	}
	cout << dist << endl;
	return 0;
}
		


