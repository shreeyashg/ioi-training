#define intl long long

#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

intl R, C;
int K, D;
vector<pair<intl, intl> > dragons;
intl **costs;

intl solve(int index, pair<intl, intl>pos, int left) {
	
	if(left == 0)
		return 0;
	
	intl a = left < D - index ? solve(index + 1, pos, left) : -1;
	intl b = costs[index][left] == 0 ? solve(index + 1, dragons[index], left - 1) : costs[index][left];
	costs[index][left] = b;
	b += dragons[index].first - pos.first + abs(pos.second - dragons[index].second);
	
	return a < 0 ? b : min(a, b);
}

int main() {
	
	scanf("%lld %lld %d %d", &R, &C, &K, &D);
	dragons = vector<pair<intl, intl> >(D);	
	for(int i = 0; i < D; i++)
		scanf("%lld %lld", &dragons[i].first, &dragons[i].second);
		
	costs = new intl*[D];
	for(int i = 0; i < D; i++)
		costs[i] = new intl[K+1];
		
	sort(dragons.begin(), dragons.end());
		
	printf("%lld\n", solve(0, pair<intl, intl>(0, 0), K));
}
