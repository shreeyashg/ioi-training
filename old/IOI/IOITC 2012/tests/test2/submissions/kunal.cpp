#include<cstdlib>
#include<iostream>
#include<algorithm>
#define infinity 1e10
using namespace std;

long long **arr;
struct str{long long x,y;}*pos;
int r,c,k,d;
long long minimum=infinity;
long long mod(long long a)
{
	if(a>=0)return a;
	return -1*a;
}
int compare(str a,str b)
{
	return a.x>b.x;
}
int main()
{
	cin>>r>>c>>k>>d;
	arr=new long long*[d+2];
	pos=new str[d+2];
	for(int i=0;i<=d;i++)
	{
		arr[i]=new long long[k+1];
	}
	pos[d+1].x=pos[d+1].y=0;
	for(int i=d;i>=1;i--)
	{
		cin>>pos[i].x>>pos[i].y;
	}
	sort(pos+1,pos+d+1,compare);
	long long dis;
	for(int i=1;i<=d;i++)
	{
		for(int j=max(i-d+k,1);j<=k;j++)
		{
			if(j>i)
			{
				arr[i][j]=infinity;
			}
			else if(j==1)arr[i][j]=0;
			else
			{
				minimum=infinity;
				for(int z=i-1;z>=1;z--)
				{
					dis=mod(pos[i].x-pos[z].x)+mod(pos[i].y-pos[z].y);
					if(arr[z][j-1]+dis<minimum)minimum=arr[z][j-1]+dis;
				}
				arr[i][j]=minimum;
			}
			//	cout<<i<<" "<<j<<" "<<arr[i][j]<<endl;
		}
	}
	minimum=infinity;
	for(int i=1;i<=d;i++)
	{
		dis=pos[i].x+pos[i].y;
		if(minimum>arr[i][k]+dis)minimum=arr[i][k]+dis;
	}
	cout<<minimum<<endl;
	return 0;
}
