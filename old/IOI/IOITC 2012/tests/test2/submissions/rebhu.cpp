#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#include<stack>
#include<vector>
#define gi(a) gar=scanf("%d",&a)
#define gl(a) gar=scanf("%lld",&a)
#define For(i,lb,ub) for(int i=lb;i<ub;i++)
#define Rev(i,ub,lb) for(int i=ub;i>lb;i--)
#define pi(a) printf("%d ",a)
#define pb(a) push_back(a)
#define mod(a) ((a<0)?-a:a)
#define li long long int
using namespace std;
int gar; //garbage value returned by scanf
li tim=115292150500000000;
li R,C;
int K,D;
vector< pair<li,li> > pos;
li findDis(int a,int b)
{
  return mod((mod((pos[a].first-pos[b].first)) + mod((pos[a].second-pos[b].second))));
}
void recurse(int cur, int k, li dist)
{
  For(i,cur+1,pos.size())
  {
    li dis=findDis(cur,i);
    if(k==K-1)
      tim=min(dist+dis,tim);
    else
      recurse(i,k+1,dist+dis);
  }
}
int main()
{
  gl(R);gl(C);gi(K);gi(D);
  pos.pb(make_pair(0,0));
  For(i,0,D)
  {
    li a,b;
    gl(a);gl(b);
    pos.pb(make_pair(a,b));
  }
  sort(pos.begin(),pos.end());
  recurse(0,0,0);
  printf("%lld\n",tim);
  return 0;
}
