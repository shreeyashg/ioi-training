#include<iostream>
#include<cmath>
using namespace std;
long long minimum(long long r, long long c, int k, int d, long long x[], long long y[])
{
  if(k==0)
  {
    int i=0;
    while(x[i]!=r)
    {
      i++;
    }
    return x[i]+y[i];
  }
  if(k>d)
    return 301000000000;
  if(k==1)
    {
      long long justmin=301000000000;
      for(int u=0; u<d; u++)
	  if(justmin>x[u]+y[u])
	    justmin=x[u]+y[u];
	  return justmin;
    }
  long long min=301000000000;
    for(int i=0; i<d-1; i++)
      {
	long long remmin=301000000000;
	for(int j=i+1; j<d; j++)
	    if(remmin>abs(x[j]-x[i])+abs(y[j]-y[i]))
	       remmin=abs(x[j]-x[i])+abs(y[j]-y[i]);
	long long actmin=minimum(x[i], c, k-2, i+1, x, y);
	       if(min>actmin+remmin)
		 min=actmin+remmin;
      }
	return min;
}

int main()
{
  long long c, r;
  int k, d;
  cin>>r>>c>>k>>d;
  long long x[d], y[d];
  for(int i=0; i<d; i++)
    cin>>x[i]>>y[i];
  cout<<minimum(r, c, k, d, x, y)<<endl;
  return 0;
}
