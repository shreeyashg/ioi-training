#include <iostream>
#include <algorithm>
#define INFINITY 2147483647
using namespace std;

struct _dragon{
	int r,c;
} dragons[310];

long long T[310][310]; // best dist to kill i dragons stopping at j'th dragon
int R,C,K,D; // rows,columns,toKill,totalDragons

bool lesserDragon (_dragon a,_dragon b) { return (a.r < b.r); }

void init();
void doStuff();
long long dist(int,int);

int main() {
	init();
	doStuff();
}

void init() {
	cin >> R >> C >> K >> D;
	for ( int i = 1 ; i <= D ; i++ ) {
		cin >> dragons[i].r >> dragons[i].c;
	}
	sort(&dragons[1],&dragons[D+1],lesserDragon); // Time -> D*log(D)

/*	//DEBUG
	cerr << "Dragons\n-------\n";
	for ( int i = 1 ; i <= D ; i++ ) {
		cerr << dragons[i].r << "\t" << dragons[i].c << endl;
	}
*/
}

void doStuff() {
	long long currMin,tempVal;
	// populate T in K*(D^2) time
	for ( int j = 1 ; j <= D ; j++ ) {
		T[1][j] = dragons[j].r + dragons[j].c;
	}
	for ( int i = 2 ; i <= K ; i++ ) {
		for ( int j = 1 ; j <= D ; j++ ) {
			currMin = INFINITY;
			for ( int h = 1 ; h < j ; h++ ) {
				tempVal = T[i-1][h]+dist(h,j);
				currMin = min(currMin,tempVal);
			}
			T[i][j] = currMin;
//			cerr << "\t" << currMin;
		}
//		cerr << endl;
	}

	// find answer in (D) time
	currMin = INFINITY;
	for ( int h = 1 ; h <= D ; h++ ) {
		currMin = min(currMin,T[K][h]);
	}

	cout << currMin << endl;
}

long long dist(int a,int b) {
	return abs(dragons[b].r-dragons[a].r)+abs(dragons[b].c-dragons[a].c);
}
