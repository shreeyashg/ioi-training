#include<iostream>
#include<cstdio>
#include<algorithm>
#include<vector>
#include<set>
#include<queue>
#include<cmath>
#include<cassert>

using namespace std;

#define FOR(i,a,b) for(long long i=(a);i<(b);++i)
#define MAX 310
#define INF 1000000000000000LL
#define S(x) scanf("%lld", &x)

struct point{
  long long x, y;
} drgn[MAX];

bool operator < (const point &a, const point &b){
  return a.x<b.x;
}

long long R, C, K, D, dp[MAX][MAX], ans = INF;

int main(){

  S(R);	S(C);	S(K);	S(D);
  FOR(i,0,D){
    S(drgn[i].x);
    S(drgn[i].y);
  }
  sort(drgn, drgn+D);
  FOR(i,0,D){
    dp[i][0] = drgn[i].x+drgn[i].y;
    FOR(j,1,min(i+1, K)){
      dp[i][j] = INF;
      FOR(k,j-1,i)
	dp[i][j] = min(dp[i][j], (dp[k][j-1]+abs(drgn[i].x-drgn[k].x)+abs(drgn[i].y-drgn[k].y)));
    }
  }
  FOR(i,K-1,D)
    ans = min(ans, dp[i][K-1]);
  cout<<ans<<endl;

  return 0;
  
}