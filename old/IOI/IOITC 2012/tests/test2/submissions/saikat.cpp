#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
struct pos
{
	int r;
	int c;
};
bool operator<(const pos& p1,const pos& p2)
{
	return p1.r<p2.r;
}
int main()
{
	int R,C,K,D,i,j,pt=0,min[300],ptr[300];
	long long int count=0;
	pos dragn[300];
	cin>>R>>C>>K>>D;
	dragn[0].r=0;
	dragn[0].c=0;
	min[0]=0;
	for(i=1;i<=D;i++)
	{
		cin>>dragn[i].r>>dragn[i].c;
		min[i]=0;
	}
	sort(dragn,dragn+D+1);
	for(i=0;i<D-1;i++)
	{
		for(j=i+1;j<D;j++)
		{
			if(min[i]==0)
			{
				min[i]=(abs(dragn[j].r-dragn[i].r))+(abs(dragn[i].c-dragn[j].c));
				ptr[i]=j;
			}
			else if(min[i]>((abs(dragn[j].r-dragn[i].r))+(abs(dragn[i].c-dragn[j].c))))
			{
				min[i]=((abs(dragn[j].r-dragn[i].r))+(abs(dragn[i].c-dragn[j].c)));
				ptr[i]=j;
			}
		}
	}
	i=0;
	do
	{
		count+=min[i];
		i=ptr[i];
		pt++;
	}while(pt<K && i<D);
	cout<<count;
	return 0;
}
