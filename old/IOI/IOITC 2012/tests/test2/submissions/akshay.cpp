#include<iostream>
#include<queue>
#include<vector>
#include<algorithm>
using namespace std;
struct pos
{long long row,col;
};
bool operator < (pos p1,pos p2)
{return p1.row<p2.row;
}
struct state
{long long x,y;
long long killed;
long long steps;
long pos;
};
bool operator < (state s1,state s2)
{return s1.steps>s2.steps;
}
int main()
{vector<pos> dragons;
long long r,c,k,d;
cin>>r>>c>>k>>d;
dragons.resize(d);
for(long long i=0;i<d;++i)
cin>>dragons[i].row>>dragons[i].col;
sort(dragons.begin(),dragons.end());
if(k==d)
{state init={0,0,0,0,0};
init.x+=dragons[0].row;
init.steps+=init.x;
while(init.pos!=d-1)
{

long long q=dragons[init.pos].col;
if(q<init.y)
{long long inc=init.y-q;
init.y-=inc;
init.steps+=inc;
}
else
{long long inc=q-init.y;
init.y+=inc;
init.steps+=inc;
}

long long inc=dragons[init.pos+1].row-init.x;
init.x+=inc;
init.steps+=inc;
init.pos++;

}
long long q=dragons[init.pos].col;
if(q<init.y)
{long long inc=init.y-q;
init.y-=inc;
init.steps+=inc;
}
else
{long long inc=q-init.y;
init.y+=inc;
init.steps+=inc;
}

cout<<init.steps;
}
else
{//long long finalans=0;
//int found=0;
state init={0,0,0,0,0};
init.x+=dragons[0].row;
init.steps+=init.x;
priority_queue<state> mypq;
mypq.push(init);
while(!mypq.empty())
{
state x;
x=mypq.top();
mypq.pop();
//cerr<<endl<<x.x<<" "<<x.y<<" "<<x.killed;
state p;
if(x.killed+d-x.pos+1<k)continue;
if (dragons[x.pos].col==x.y)
{p=x;
p.killed++;
if(p.killed==k){
/*
if(found==0){finalans=p.steps;found=1;}
if(p.steps<finalans)finalans=p.steps;
continue;*/
cout<<p.steps;break;

}
long long inc=0;
if(x.pos==d-1)continue;
inc=dragons[x.pos+1].row-p.x;
p.x+=inc;
p.steps+=inc;
p.pos++;
mypq.push(p);
}
else
{
//option 1
{p=x;
if(x.pos==d-1)continue;
long long inc=dragons[x.pos+1].row-p.x;
p.x+=inc;
p.steps+=inc;
p.pos++;
mypq.push(p);
}
//option 2
{p=x;
long long q=dragons[x.pos].col;
if(q<p.y)
{long long inc=p.y-q;
p.y-=inc;
p.steps+=inc;
mypq.push(p);
}
else
{p=x;
long long inc=q-p.y;
p.y+=inc;
p.steps+=inc;
mypq.push(p);
}
}
}
}
 //cout<<finalans;
}
return 0;
}
