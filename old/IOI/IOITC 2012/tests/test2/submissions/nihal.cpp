#include<cstdlib>
#include<cstdio>
#include<algorithm>
using namespace std;
#define FOR(i, a, b) for(i=a;i<=b;i++)

struct drgn{
	int ic, jc;
};

bool operator < (drgn x, drgn y){
	return x.ic < y.ic;
};

int main(){
	drgn dr[305];
	int r, c, k, d, i, j, l, m;
	long long int temp, min, best[305][305];
	scanf("%d %d %d %d", &r, &c, &k, &d);
	FOR(i, 1, d) scanf("%d %d", &dr[i].ic, &dr[i].jc);
	sort(dr + 1, dr + d + 1);
	FOR(i, 1, d) best[i][1] = dr[i].ic + dr[i].jc;
	FOR(j, 2, k){
		FOR(i, j, d){
			min = best[j-1][j-1] + abs(dr[j-1].ic - dr[i].ic) + abs(dr[j-1].jc - dr[i].jc);
			FOR(l, j, i-1){
				temp = best[l][j-1] + abs(dr[l].ic - dr[i].ic) + abs(dr[l].jc - dr[i].jc);
				if(temp < min) temp = min;
			}
			best[i][j] = min;
		}
	}
	min = best[k][k];
	FOR(m, k+1, d) if(best[m][k] < min) min = best[m][k];
	printf("%lld", min);
	return 0;
}
