#include<cstdio>
#include<cstdlib>
#define MAXINT 2000000000000

using namespace std;

long long weights[301][301];
long long points[301][2];
long long fromsource[300];

long long r, c, k, d;

long long spath(int target, int len)
{
  if(len == 1)
    return weights[0][target];

  long long minimum = MAXINT, temp;
  bool flag = 1;
  for(int i = 1; i <= d; i++)
    {
      if(weights[i][target] != MAXINT)
	flag = 0;
      temp = spath(i, len - 1) + weights[i][target];
      if(temp < minimum && i != target)
	minimum = temp;
    }
  if(flag)
    return MAXINT;
  return minimum;
}

long long findshortest()
{
  long long min = MAXINT, temp;
  for(int j = 1; j <= d; j++)
    {
      temp = spath(j, k);
      if(temp < min)
	min = temp;
    }
  return min;
}

int main()
{
  scanf("%lld %lld %lld %lld", &r, &c, &k ,&d);
  for(int i = 1; i <= d; i++)
    scanf("%lld %lld", &points[i][1], &points[i][0]);

  for(int i = 0; i <= d; i++)
    for(int j = 0; j <= d; j++)
      weights[i][j] = MAXINT;
  weights[0][0] = 0;
  for(int i = 1; i <= d; i++)
    {
      weights[0][i] = points[i][0] + points[i][1];
      for(int j = i; j <= d; j++)
	{
	  if(points[i][1] <= points[j][1])
	    {
	      weights[i][j] = points[j][1] - points[i][1] + abs(points[i][0] - points[j][0]);
	      weights[j][i] = MAXINT;
	    }
	  else
	    {
	      weights[j][i] = points[i][1] - points[j][1] + abs(points[i][0] - points[j][0]);
	      weights[i][j] = MAXINT;
	    }
	}
      weights[i][i] = 0;
    }
  printf("%lld", findshortest());
}
