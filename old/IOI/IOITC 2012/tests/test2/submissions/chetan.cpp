#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

class dragon
{
public:
	long long x, y;
};

bool operator<(const dragon& a, const dragon& b)
{
	return a.x < b.x;
}

vector<dragon> dragons;

long long cost(int i, int j)
{
	return llabs(dragons[i].x - dragons[j].x) + llabs(dragons[i].y - dragons[j].y);
}

long long table[301][301];

int main()
{
	int k, d;
	long long r, c;
	cin >> r >> c >> k >> d;
	/*dragon drag;
	dragons.push_back(drag);*/
	dragons.resize(d + 1);
	for(int i = 1; i <= d; ++i)
	{
		//cin >> drag.x >> drag.y;
		//dragons.push_back(drag);
		//cout << dragons[i].x << ' ' << dragons[i].y;
		cin >> dragons[i].x >> dragons[i].y;
	}
	sort(dragons.begin(), dragons.end());
	for(int i = 1; i <= d; ++i)
		table[i][1] = dragons[i].x + dragons[i].y;
	for(int r1 = 2; r1 <= k; ++r1)
	{
		for(int i = r1; i <= d; ++i)
		{
			long long min = table[r1 - 1][r1 - 1] + cost(i, r1 - 1);
			for(int j = r1; j < i; ++j)
			{
				long long m = table[j][r1 - 1] + cost(i, j);
				min = m < min? m : min;
			}
			table[i][r1] = min;
		}
	}
	long long min = table[k][k];
	/*for(int i = 1; i <= d; ++i)
	{
		for(int r1 = 1; r1 <= i && r1 <= k; ++r1)
			cout << table[i][r1] << ' ';
		cout << endl;
	}*/
	for(int i = k + 1; i <= d; ++i)
		if(min > table[i][k])
			min = table[i][k];
	cout << min << endl;
	return 0;
}
