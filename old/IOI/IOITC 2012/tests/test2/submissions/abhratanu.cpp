#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;

#define MAXIM 1000000000000000

struct pnt
{
  long long r,c;
};

long long dist(pnt a,pnt b)
{
  return abs(a.r-b.r)+abs(a.c-b.c);
}

bool operator < (pnt a,pnt b)
{
  return a.r<b.r;
}

pnt a[310];
long long memo[310][310];
bool mark[310][310];
int k,d;

long long dp(pnt p,int num, int left)
{
  if(mark[num][left])
    return memo[num][left];
  if(left==0)
    return 0;
  if(left>d-num)
    return MAXIM;
  int i;
  for(i=num+1;i<=d;i++)
    memo[num][left]=min(memo[num][left],dist(p,a[i])+dp(a[i],i,left-1));
  mark[num][left]=1;
  return memo[num][left];
}

int main()
{
  int i,j,row,col;
  pnt p;p.r=0;p.c=0;
  cin >> row >> col >> k >> d;
  for(i=1;i<=d;i++)
    cin >> a[i].r >> a[i].c;
  for(i=0;i<=301;i++)
    for(j=0;j<=301;j++)
    {
      memo[i][j]=MAXIM;
      mark[i][j]=0;
    }
  sort(a+1,a+d+1);
  cout << dp(p,0,k) << endl;
  return 0;
}
  