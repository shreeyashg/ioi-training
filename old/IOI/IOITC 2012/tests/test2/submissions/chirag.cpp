#include<iostream>
#include<algorithm>
using namespace std;

#define MAX_D 2000000000

struct coord
{
  int x;
  int y;
}dragons[305];

struct list_elem
{
  int d_key;
  int min_l;
}num_dragon[305][305];       //first index stands for the number of dragons after a particular dragon in this list. second index is an iterator.

bool operator < (coord a, coord b)
{
  if(a.x<b.x) return 1;
  else return 0;
}

int list_size[305];

int r, c, tot_d, d;

int max (int a, int b)
{
  if (a>b) return a;
  else return b;
}

int min (int a, int b)
{
  if (a<b) return a;
  else return b;
}

int dist(int d1, int d2)
{
  return (max((dragons[d1].x - dragons[d2].x) , (dragons[d2].x - dragons[d1].x)) + max((dragons[d1].y - dragons[d2].y) , (dragons[d2].y - dragons[d1].y)));
}

int main()
{
  cin>>r>>c>>tot_d>>d;
  
  int i;
  
  for(i=0;i<d;++i)
  {
    cin>>dragons[i].x>>dragons[i].y;
    list_size[i] = 0;
  }
  
  sort (dragons , dragons + d);
  
  for(i=(d-1);i>=0;--i)
  {
    int j,k;
    
    for(j=(d-(i+1));j>=1;--j)
    {
      int min_dist = MAX_D;
      for(k=0;k<list_size[j];++k)
      {
	if( (num_dragon[j][k].min_l + dist (num_dragon[j][k].d_key , i)) < min_dist ) min_dist = num_dragon[j][k].min_l + dist (num_dragon[j][k].d_key , i);
      }
      num_dragon[j+1][list_size[j+1]].d_key = i;
      num_dragon[j+1][list_size[j+1]].min_l = min_dist;
      list_size[j+1] ++;
    }
    num_dragon[1][list_size[1]].d_key = i;
    num_dragon[1][list_size[1]].min_l = 0;
    list_size[1] ++;
  }
  
  long long min_d = 400000000000;
  for(i=0;i<list_size[tot_d];++i)
  {
    if(  (num_dragon[tot_d][i].min_l + (dragons[num_dragon[tot_d][i].d_key].x + dragons[num_dragon[tot_d][i].d_key].y)) < min_d) min_d = (num_dragon[tot_d][i].min_l + (dragons[num_dragon[tot_d][i].d_key].x + dragons[num_dragon[tot_d][i].d_key].y));
  }
  cout<<min_d;
  
  
//   for(i=1;i<=d;++i)
//   {
//     cout<<i<<" :: ";
//     int j;
//     for(j=0;j<list_size[i];++j)
//     {
//       cout<<"("<<num_dragon[i][j].d_key<<","<<num_dragon[i][j].min_l<<") -> ";
//     }
//     cout<<"\n";
//   }
  
}
  