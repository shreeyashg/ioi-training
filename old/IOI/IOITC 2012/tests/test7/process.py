#!/usr/bin/python3

import sys

lines = sys.stdin.readlines()

assert(len(lines)%2 == 0)

lines = [[int(y) for y in (x.replace('{',' ').replace('}', ' ').split(','))] for x in lines]

small=0
large=0

for i in range(len(lines)//2):
	assert(len(lines[2*i]) == len(lines[2*i+1]))
	N = len(lines[2*i])
	if(N <= 18):
		f = open('small'+str(small)+'.in', 'w')
		small+=1
	else:
		f = open('large'+str(large)+'.in', 'w')
		large+=1
	print(N, file=f)
	for j in range(N):
		print(lines[2*i][j], lines[2*i+1][j], file=f)
