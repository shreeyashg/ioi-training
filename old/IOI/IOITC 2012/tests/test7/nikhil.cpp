#include<vector>
#include<stack>
#include<set>
#include<map>
#include<queue>
#include<deque>
#include<string>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<cassert>
#include<cstdlib>
#include<cstdio>
#include<cmath>

using namespace std;

// Input macros
#define s(n)                        scanf("%d",&n)
#define sc(n)                       scanf("%c",&n)
#define sl(n)                       scanf("%lld",&n)
#define sf(n)                       scanf("%lf",&n)
#define ss(n)                       scanf("%s",n)

// Useful constants
#define INF                         (int)1e9
#define EPS                         1e-9

// Useful hardware instructions
#define bitcount                    __builtin_popcount
#define gcd                         __gcd

// Useful container manipulation / traversal macros
#define forall(i,a,b)               for(int i=a;i<b;i++)
#define foreach(v, c)               for( typeof( (c).begin()) v = (c).begin();  v != (c).end(); ++v)
#define all(a)                      a.begin(), a.end()
#define in(a,b)                     ( (b).find(a) != (b).end())
#define pb                          push_back
#define fill(a,v)                   memset(a, v, sizeof a)
#define sz(a)                       ((int)(a.size()))
#define mp                          make_pair

// Some common useful functions
#define maX(a,b)                    ( (a) > (b) ? (a) : (b))
#define miN(a,b)                    ( (a) < (b) ? (a) : (b))
#define checkbit(n,b)               ( (n >> b) & 1)
#define DREP(a)			    sort(all(a)); a.erase(unique(all(a)),a.end())
#define INDEX(arr,ind)		    (lower_bound(all(arr),ind)-arr.begin())

using namespace std;

#if DEBUG && !ONLINE_JUDGE

    #define debug(args...)     (Debugger()) , args

    class Debugger
    {
        public:
        Debugger(const std::string& _separator = ", ") :
        first(true), separator(_separator){}

        template<typename ObjectType>
        Debugger& operator , (const ObjectType& v)
        {
            if(!first)
                std:cerr << separator;
            std::cerr << v;
            first = false;
            return *this;
        }
        ~Debugger() {  std:cerr << endl;}

        private:
        bool first;
        std::string separator;
    };

    template <typename T1, typename T2>
    inline std::ostream& operator << (std::ostream& os, const std::pair<T1, T2>& p)
    {
        return os << "(" << p.first << ", " << p.second << ")";
    }

    template<typename T>
    inline std::ostream &operator << (std::ostream & os,const std::vector<T>& v)
    {
        bool first = true;
        os << "[";
        for(unsigned int i = 0; i < v.size(); i++)
        {
            if(!first)
                os << ", ";
            os << v[i];
            first = false;
        }
        return os << "]";
    }

    template<typename T>
    inline std::ostream &operator << (std::ostream & os,const std::set<T>& v)
    {
        bool first = true;
        os << "[";
        for (typename std::set<T>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
        {
            if(!first)
                os << ", ";
            os << *ii;
            first = false;
        }
        return os << "]";
    }

    template<typename T1, typename T2>
    inline std::ostream &operator << (std::ostream & os,const std::map<T1, T2>& v)
    {
        bool first = true;
        os << "[";
        for (typename std::map<T1, T2>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
        {
            if(!first)
                os << ", ";
            os << *ii ;
            first = false;
        }
        return os << "]";
    }

#else
    #define debug(args...)                  // Just strip off all debug tokens
#endif

typedef long long LL;

typedef pair<int, int> PII;
typedef pair<int, LL> PIL;
typedef pair<LL, int> PLI;
typedef pair<LL, LL> PLL;

typedef vector<int> VI;
typedef vector<LL> VL;
typedef vector<vector<int> > VVI;
typedef vector<VL> VVL;


int ni(int lo, int hi)
{
    int _num; 
    assert( s(_num) == 1 && lo <= _num && _num <= hi);
    return _num;
}

/*-------------------------Main code begins now ------------------------------*/
int testnum;

struct Point
{
  int x,y;
  
  Point(int _x = 0, int _y = 0)
  {
    x = _x; y = _y;
  }
  
  Point operator -(Point other)
  {
    return Point(x - other.x, y - other.y);
  }
  
  int operator ^ (Point other)
  {
    return x * other.y - y * other.x;
  }
  
  int operator * (Point other)
  {
    return x * other.x + y * other.y;
  }
};
bool operator < (const Point &A, const Point& B)
  {
    return A.x < B.x || ( A.x == B.x && A.y < B.y);
  }

vector<Point> findHull(vector<Point>& poly)
{
  int N = poly.size();
  vector<Point> out;
  out.resize(N+N);
  
  sort(poly.begin(), poly.end());
  int start = 0, k = 0;
  for(int i = 0; i < N; i++)
  {
    Point now = poly[i];
    while( k - start >= 2 && ( (now-out[k-1]) ^ (now- out[k-2]) ) > 0)
      k--;
    out[k++] = now;
  }
  
  k--;
  start = k;
  for(int i = N-1; i >= 0; i--)
  {
    Point now = poly[i];
    while( k - start >= 2 && ( (now-out[k-1]) ^ (now- out[k-2]) ) > 0)
      k--;
    out[k++] = now;
  }
  
  k--;
  out.resize(k);
  return out;
}

int findArea(vector<Point> &poly)
{
  if(poly.size() < 3) return 0;
  poly = findHull(poly);
  int ans = 0, N = poly.size();
  for(int i = 0; i < N; i++)
  {
    int j = (i+1) % N;
    ans += poly[i].x * poly[j].y - poly[i].y * poly[j].x;
  }
  return abs(ans);
}

vector<Point> all;

void preprocess()
{
    
}

void solve()
{
    int N = all.size();
    if( N <= 3)		// trivial case, no?
    {
      cout << 0 << endl;
      return;
    }
    
    int best = INF;
    for(int i = 0; i < N; i++)
      for(int j = 0; j < N; j++)
      {
	if(i == j) continue;
	vector<Point> left, right;
	
	bool bad = false;
	for(int k = 0; k < N; k++)
	{
	  if(k == i) right.pb(all[k]);
	  else if(k == j) left.pb(all[k]);
	  else
	  {
	    int det = (all[k] - all[i]) ^ (all[j] - all[i]);
	    if(det > 0)
	      right.pb(all[k]);
	    else if(det < 0)
	      left.pb(all[k]);
	    else
	    {
	     if( (all[k] - all[i]) * (all[j] - all[i]) < 0)
	       right.pb(all[k]);
	     else if( (all[k] - all[j]) * (all[i] - all[j]) < 0)
	       left.pb(all[k]);
	     else
	       bad = true;
	    }
	  }
	}
	
	if(bad) continue;
	best = min( best, max(findArea(left), findArea(right)));
      }
      cout << best << endl;
}

bool input()
{
  int N = ni(1, 50);
  
  debug(a, b , "hi", test, 30 * 56);
  for(int i = 0; i < N; i++
  {
    int x = ni(-1000, 1000), y = ni(-1000, 1000);
    all.pb( Point(x,y));
  }
  return true;
}


int main()
{
    preprocess();
    int T = 1;
    for(testnum=1;testnum<=T;testnum++)
    {
        if(!input()) break;
        solve();
    }
}
