import static java.lang.Math.*;
import static java.math.BigInteger.*;
import static java.util.Arrays.*;
import static java.util.Collections.*;
import java.math.*;
import java.util.*;
import java.io.*;	

public class wata {
  
  public long minArea(P[] ps) {
    int n = ps.length;
    sort(ps);
    long res = area(convexHull(ps));
    for (int i = 0; i < n; i++) {
      loop : for (int j = 0; j < n; j++) if (i != j) {
        ArrayList<P> left = new ArrayList<P>();
        ArrayList<P> right = new ArrayList<P>();
        for (int k = 0; k < n; k++) {
          if (k == i) right.add(ps[k]);
          else if (k == j) left.add(ps[k]);
          else {
            long det = ps[j].sub(ps[i]).det(ps[k].sub(ps[i]));
            if (det > 0) {
              left.add(ps[k]);
            } else if (det < 0) {
              right.add(ps[k]);
            } else {
              long dot = ps[j].sub(ps[i]).dot(ps[k].sub(ps[i]));
              if (dot < 0) {
                right.add(ps[k]);
              } else {
                dot = ps[i].sub(ps[j]).dot(ps[k].sub(ps[j]));
                if (dot < 0) {
                  left.add(ps[k]);
                } else {
                  continue loop;
                }
              }
            }
          }
        }
        long area1 = area(convexHull(left.toArray(new P[0])));
        long area2 = area(convexHull(right.toArray(new P[0])));
        res = min(res, max(area1, area2));
      }
    }
    return res;
  }

  public static void main(String [] args) throws IOException
  {
    (new wata()).run();
  }
  
  public void run() throws IOException
  {
    Scanner in = new Scanner(System.in);
    int N = in.nextInt();
    P [] all = new P[N];
    for(int i = 0; i < N; i++)
      all[i] = new P(in.nextInt(), in.nextInt());
    System.out.println(minArea(all));
  } 
  
  
  class P implements Comparable<P> {
    long x, y;
    P(long x, long y) {
      this.x = x;
      this.y = y;
    }
    P add(P p) {
      return new P(x + p.x, y + p.y);
    }
    P sub(P p) {
      return new P(x - p.x, y - p.y);
    }
    long dot(P p) {
      return x * p.x + y * p.y;
    }
    long det(P p) {
      return x * p.y - y * p.x;
    }
    public int compareTo(P o) {
      int comp = Long.signum(x - o.x);
      if (comp != 0) return comp;
      return Long.signum(y - o.y);
    }
    public String toString() {
      return String.format("(%d, %d)", x, y);
    }
  }
  
  P[] convexHull(P[] ps) {
    int n = ps.length, k = 0;
    if (n <= 1) return ps;
    P[] qs = new P[n * 2];
    for (int i = 0; i < n; qs[k++] = ps[i++]) {
      while (k > 1 && qs[k - 1].sub(qs[k - 2]).det(ps[i].sub(qs[k - 1])) <= 0) k--;
    }
    for (int i = n - 2, t = k; i >= 0; qs[k++] = ps[i--]) {
      while (k > t && qs[k - 1].sub(qs[k - 2]).det(ps[i].sub(qs[k - 1])) <= 0) k--;
    }
    P[] res = new P[k - 1];
    System.arraycopy(qs, 0, res, 0, k - 1);
    return res;
  }
  
  long area(P[] ps) {
    int n = ps.length;
    long res = 0;
    for (int i = 0; i < n; i++) {
      res += ps[i].det(ps[(i + 1) % n]);
    }
    return res;
  }
  
  void debug(Object...os) {
    System.err.println(deepToString(os));
  }
}