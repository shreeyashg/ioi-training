#include <iostream>
#include <algorithm>
#include <cmath>
#include <cassert>
using namespace std;

#define MAXIM 2000000000000000

struct point
{
  long long x,y;
  point(){x=0,y=0;}
  point (long long p,long long q)
  {
    x=p;y=q;
  }
};

bool operator < (point a,point b)
{
  return a.x<b.x||(a.x==b.x&&a.y<b.y);
}

point operator - (point a, point b)
{
  point h(a.x-b.x,a.y-b.y);
  return h;
}

long long mod(point a)
{
  return a.x*a.x+a.y*a.y;
}

long long dist(point a,point b)
{
  return mod(b-a);
}

long long cross(point a,point b,point c)
{
  point h=b-a,k=c-a;
  return h.y*k.x-h.x*k.y;
};

point arr[200];
point stck[200];
point c[2][200];

long long area(int n)
{
  int i;
  long long sum=0;
  for(i=2;i<n;i++)
    sum+=(cross(stck[1],stck[i],stck[i+1]));
  return abs(sum);
}

long long convexHull(int num,int n)
{
  if(n==0||n==1||n==2)
    return 0;
  int i,p=1,p1,d;
      
  sort(c[num]+1,c[num]+n+1);
  stck[1]=c[num][1];
  for(i=2;i<=n;i++)
  {
    while(p>1&&cross(stck[p-1],stck[p],c[num][i])>0)
      p--;
    p++;
    stck[p]=c[num][i];
  }
  //p--;
  p1=p;
  for(i=n-1;i>=1;i--)
  {
    while(p1>p&&cross(stck[p1-1],stck[p1],c[num][i])>0)
      p1--;
    p1++;
    stck[p1]=c[num][i];
  }
  return area(p1-1);
}

int main()
{
  int n,i,j,k,n1,n2;
  long long mn=MAXIM,t,d1,d2;cin >> n;
  for(i=1;i<=n;i++)
    cin >> arr[i].x >> arr[i].y;
  for(i=1;i<=n;i++)
  {
    for(j=1;j<=n;j++)
    {
      if(i==j)
	continue;
      n1=0;n2=0;
      for(k=1;k<=n;k++)
      {
	t=cross(arr[i],arr[j],arr[k]);
// 	if(t==0&&((arr[k].x<arr[i].x&&arr[k].x>arr[j].x)||(arr[k].x<arr[j].x&&arr[k].x>arr[i].x)))
// 	  goto last;
	//if(t == 0)
	  //assert(dist( arr[i], arr[k]) != dist( arr[j], arr[k]));
	if(t>0||(t==0&&dist(arr[i],arr[k])<dist(arr[j],arr[k])))
	{
	  n1++;
	  c[0][n1]=arr[k];
	}
	else
	{
	  n2++;
	  c[1][n2]=arr[k];
	}
      }
      d1=convexHull(0,n1);
      d2=convexHull(1,n2);
      mn=min(mn,max(d1,d2));
    }
    last : {}
  }
  cout << mn << endl;
  return 0;
} 