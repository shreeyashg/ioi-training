# include <cstdio>
# include <iostream>
# include <algorithm>
# include <vector>
# include <cstring>
# include <cctype>
# include <set>
# include <map>
# include <cmath>
# include <queue>
# include <string>

using namespace std;

const int MMAX=100000,NMAX=5000;
const long long INF=1ll<<60;

struct edge
{
	int u,v;
	int quality,cost;
}edges[MMAX];

bool operator<(const edge& e1,const edge& e2)
{
	return e1.cost<e2.cost;
}

int N,M;

int parent[NMAX];
int findparent(int u)
{
	if(parent[u]==u)return u;
	return parent[u]=findparent(parent[u]);
}

void merge(int u,int v)
{
	parent[findparent(u)]=findparent(v);
}

long long bestcost(int limit)
{
	long long ret=0;
	for(int i=0;i<N;i++)
		parent[i]=i;
	
	for(int i=0,j=0;i<M;i++)
	{
		if(edges[i].quality<limit)continue;
		if(findparent(edges[i].u)==findparent(edges[i].v))continue;
		
		j++;
		ret+=edges[i].cost;
		merge(edges[i].u,edges[i].v);
		
		if(j==N-1)return ret;
	}
	return INF;
}

int main()
{
	scanf("%d%d",&N,&M);
	
	int start=1e9,end=0,mid;
	for(int i=0;i<M;i++)
	{
		scanf("%d%d%d%d",&edges[i].u,&edges[i].v,&edges[i].quality,&edges[i].cost);
		start=min(start,edges[i].quality);
		end=max(end,edges[i].quality);
	}
	sort(edges,edges+M);
	
	if(bestcost(start)==INF)
	{
		printf("-1\n");
		return 0;
	}
	
	while(start<end)
	{
		mid=(start+end+1)/2;
		if(bestcost(mid)==INF)end=mid-1;
		else start=mid;
	}
	
	printf("%d %Ld\n",start,bestcost(start));
	
	return 0;
}
