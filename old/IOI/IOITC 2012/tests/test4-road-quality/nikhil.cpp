#include<vector>
#include<stack>
#include<set>
#include<map>
#include<queue>
#include<deque>
#include<string>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<cassert>
#include<cstdlib>
#include<cstdio>
#include<cmath>

using namespace std;

// Input macros
#define s(n)                        scanf("%d",&n)
#define sc(n)                       scanf("%c",&n)
#define sl(n)                       scanf("%lld",&n)
#define sf(n)                       scanf("%lf",&n)
#define ss(n)                       scanf("%s",n)

// Useful constants
#define INF                         (int)1e9
#define EPS                         1e-9

// Useful hardware instructions
#define bitcount                    __builtin_popcount
#define gcd                         __gcd

// Useful container manipulation / traversal macros
#define forall(i,a,b)               for(int i=a;i<b;i++)
#define foreach(v, c)               for( typeof( (c).begin()) v = (c).begin();  v != (c).end(); ++v)
#define whole(a)                    a.begin(), a.end()
#define in(a,b)                     ( (b).find(a) != (b).end())
#define pb                          push_back
#define fill(a,v)                   memset(a, v, sizeof a)
#define sz(a)                       ((int)(a.size()))
#define mp                          make_pair

// Some common useful functions
#define maX(a,b)                    ( (a) > (b) ? (a) : (b))
#define miN(a,b)                    ( (a) < (b) ? (a) : (b))
#define checkbit(n,b)               ( (n >> b) & 1)
#define DREP(a)			    sort(all(a)); a.erase(unique(all(a)),a.end())
#define INDEX(arr,ind)		    (lower_bound(all(arr),ind)-arr.begin())

using namespace std;

#if DEBUG && !ONLINE_JUDGE

    #define debug(args...)     (Debugger()) , args

    class Debugger
    {
        public:
        Debugger(const std::string& _separator = ", ") :
        first(true), separator(_separator){}

        template<typename ObjectType>
        Debugger& operator , (const ObjectType& v)
        {
            if(!first)
                std:cerr << separator;
            std::cerr << v;
            first = false;
            return *this;
        }
        ~Debugger() {  std:cerr << endl;}

        private:
        bool first;
        std::string separator;
    };

    template <typename T1, typename T2>
    inline std::ostream& operator << (std::ostream& os, const std::pair<T1, T2>& p)
    {
        return os << "(" << p.first << ", " << p.second << ")";
    }

    template<typename T>
    inline std::ostream &operator << (std::ostream & os,const std::vector<T>& v)
    {
        bool first = true;
        os << "[";
        for(unsigned int i = 0; i < v.size(); i++)
        {
            if(!first)
                os << ", ";
            os << v[i];
            first = false;
        }
        return os << "]";
    }

    template<typename T>
    inline std::ostream &operator << (std::ostream & os,const std::set<T>& v)
    {
        bool first = true;
        os << "[";
        for (typename std::set<T>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
        {
            if(!first)
                os << ", ";
            os << *ii;
            first = false;
        }
        return os << "]";
    }

    template<typename T1, typename T2>
    inline std::ostream &operator << (std::ostream & os,const std::map<T1, T2>& v)
    {
        bool first = true;
        os << "[";
        for (typename std::map<T1, T2>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
        {
            if(!first)
                os << ", ";
            os << *ii ;
            first = false;
        }
        return os << "]";
    }

#else
    #define debug(args...)                  // Just strip off all debug tokens
#endif

typedef long long LL;

typedef pair<int, int> PII;
typedef pair<int, LL> PIL;
typedef pair<LL, int> PLI;
typedef pair<LL, LL> PLL;

typedef vector<int> VI;
typedef vector<LL> VL;
typedef vector<vector<int> > VVI;
typedef vector<VL> VVL;


int ni()
{
    int _num; s(_num);
    return _num;
}

/*-------------------------Main code begins now ------------------------------*/
int testnum;

struct UF
{
  int N, comp;
  VI dad, mass;
  UF(int _N)
  {
    comp = N = _N;
    dad.resize(N); mass.resize(N);
    forall(i, 0, N)
      dad[i] = i, mass[i] = 1;
  }
  
  int find(int u)
  {
    if(dad[u] == u) return u;
    return dad[u] = find(dad[u]);
  }
  
  bool merge(int u, int v)
  {
    int c1 = find(u), c2 = find(v);
    if(c1 != c2)
    {
      comp --;
      if(mass[c1] < mass[c2]) swap(c1, c2);
      mass[c1] += mass[c2];
      dad[c2] = c1;
      return true;
    }
    return false;
  }
  
  int compCount()
  {
    return comp;
  }
};

void preprocess()
{
    
}


int N,M;
struct edge
{
  int u, v, qual, len;
};

bool qualDec(edge e1, edge e2)
{
  return e1.qual > e2.qual;
}

bool lenInc(edge e1, edge e2)
{
  return e1.len < e2.len;
}

edge all[1000005];

void solve()
{
    int leastQual = -1;
    sort(all, all + M, qualDec);
    UF one(N);
    forall(i, 0, M)
    {
      one.merge(all[i].u, all[i].v);
      if(one.compCount() == 1)
      {
	leastQual = all[i].qual;
	break;
      }
    }
    
    if(leastQual == -1)
    {
      cout << -1 << endl;
      return;
    }
    
    int index = 0;
    forall(i, 0, M)
      if(all[i].qual >= leastQual) index ++; 
      else break;
    
    sort(all, all + index, lenInc);
    LL cost = 0;
    UF two(N);
    for(int i = 0; i < index; i++)
    {
      if(two.merge(all[i].u, all[i].v))
	cost += all[i].len;
    }
    cout << leastQual << " " << cost << endl;
}

bool input()
{
  s(N); s(M);
  forall(i, 0, M)
  {
    s(all[i].u); s(all[i].v); 
    s(all[i].qual); s(all[i].len);
  }
  return true;
}


int main()
{
    preprocess();
    int T = 1;
    for(testnum=1;testnum<=T;testnum++)
    {
        if(!input()) break;
        solve();
    }
}
