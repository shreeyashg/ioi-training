For those of you who find it hard to indent your code manually (whether because you don't quite agree with length-8 tabs, or whether you have to press 3-4 tabs after every line), try doing the following:

For Kate:
Goto Tools -> Indentation -> C Style
Also, you could Goto Settings -> Configure Kate… Under Editor Component -> Editing: play around with whatever you need (and have a good look at the Indentation Tab - for specifying Tab-width etc).

For gedit:
Goto Edit -> Preferences…
Then Play around with whatever you need (Highlight matching brackets, highlight current line, display line numbers), but please visit Editor tab, and select "Enable automatic indentation". Other things also can be customized to suit your taste.

Also, in both Kate as well as gedit, you could re-indent entire lines by first just selecting the lines you need and pressing TAB. This does Not erase what you've written by putting one tab, but rather shifts all the selected lines by one tab.

Now, you have no reason to skip indentation - its easy, its useful, and everyone's happy.


So now, lets look at the solution to this problem, which had one or two boundary cases (as in, intuitive algorithms that were in fact wrong), so that maybe you'll be able to spot your issue here itself.
The problem asked you to find a connected spanning subgraph whose minimum-quality was maximum possible (among connected spanning subgraphs) and whose total length was minimum possible (among all connected spanning subgraphs having this maximum possible minimum-quality).
The first boundary case that a lot of you forgot, was to check if the graph is connected first of all. If it was not connected, you had to just output -1.
Now, assuming that the graph Was connected, there were two possible approaches you could have used.
(1) Binary search on the maximum possible minimum-quality.
For this approach, you had to fix a possible quality-threshhold, and then find a minimum spanning tree considering only those edges whose quality was at least this threshold. Finally just check if you have ensured connectivity after constructing this Minimum Spanning "Tree".
(2) Do 2 MST's. One to find the maximum possible minimum-quality, one to find the MST using the above as a threshold.
In the first MST, you have to ensure that you're taking min(quality, cur_edge_quality) instead of quality+cur_edge_quality. There was one possible bug that you could have made here: if you do your first MST and consider only the edges that were used till the point where you got connectivity, then you may leave out some edges of the same min-quality that can have been used.

Some people tried using this intuitive algorithm: Sort based on quality, and then break ties based on length. Finally do a single Kruskal's on this ordering of edges. This is wrong since you may take a high-length road just because it has a high quality. Consider the following example (Quality, Length):

--------0-------
|              |
|(5,1)         | (5,1)
|              |
1--------------2
      (10,10)

If you used the algorithm of first sorting by quality and then by length, you would definitely choose the (10,10) edge. Whereas optimally, you would just choose the two (5,1) edges.


Having said that, one of the first submissions we received was 100% correct; but also 100% unindented. Two things I'd like to point out in that code:
The Kruskal's used was clean. It used path-compression but no concept of ranks/sizes. In this problem O(N^2) Kruskal's would have passed, and this clearly is in practice fairly faster than O(N^2) [I'm not sure of the analysis as compared with Union-Find WITH ranking, which is guaranteed to have O(N alpha(N) ) complexity].
Another point to be noted was, the 2 Kruskals used had virtually copy-pasted code, and it was all written in the main() function. Having non-modular code like this may work in a simple problem such as this, but when things get harder, it would be best that you modularize as far as possible. Write two functions and call them from the main, instead of cluttering up your main() function so much.


Having a look at the overall code of people, I must say that you seem to have adapted to STL fairly enthusiastically :). It was interesting to see "#define pb(x) push_back(x)" in some people's codes :P Here are a few more well-used macros that you would like to use:
#define s(n)	scanf("%d", &n)
#define sl(n)	scanf("%lld", &n)
#define all(x)	x.begin(), x.end()
	//you could use the above well with sort(all(v))
#define pb(x)	push_back(x)
#define mp(x,y)	make_pair(x, y)
	//imagine being able to use mp(mp(a, b), mp(c, d))


But there were some flaws as well. I remember mentioning that the STL set uses only '<' to compare elements. So, it tests whether a and b are equivalent by checking  if !(a<b) && !(b<a). If you insert elements into a set without making sure that for a != b, either one of those two is true, then you would get into trouble: it would potentially ignore certain valid data of yours.

We had a submission where the "<" operator was overloaded in one place as follows:
return e1.qual > e2.qual || (e1.qual == e2.qual && e1.length < e2.length);

Now, if you use this along with a set<edge> in a problem whose test-data is like:

-------0-------
|             |
|             |
|(5,5)        |(5,5)
|             |
1-------------2
      (5,5)
ALL three edges will be indistinguishable to the set. And it will insert only one of the three treating the other two as duplicates. Your algorithm will then claim that the graph is not connected when in fact it is!


Also, don't create large arrays inside functions. Do this globally. There is a much larger array-limit for global arrays than for those within functions. Anything larger than 10^4 elements should be declared globally.



What is wrong with this pseudocode of Kruskal's? Assume that edges have been sorted appropriately.
Kruskal()
edges = 0;
for(each edge (u, v), in sorted order Until edges = N-1)
	if(comp[u] != comp[v])
		comp[u] = comp[v];
		edges++;
		total_length += length of (u,v)
output total_length



So instead of pointing out what is good coding practice, I'll take one moment to tell you how to improve a particular kind of Bad coding. It probably doesn't make a difference in terms of correctness, but it makes a world of difference in clarity.
So, imagine that you are performing Prim's algorithm. Also, assume you have (a) a means to compare edge weights properly (your own overloaded function) and (b) a set of edges for each vertex (the adjacency list, stored in the form of edges, like pair<int, pair<int, int> >, where the first element is the weight and the second element is the edge (u, v), assuming you are currently at v, and the edge corresponds to outgoing to u.

Now, what you must NOT do, is this:
PIII temp;			//pair int int int, this is okay
while(!q.empty())		//okay
{
	temp=*q.begin();q.erase(q.begin());		//okay
	if(visited[temp.second.first]==1)continue;	//starts getting bad here
	visited[temp.second.first]++;			//...
	total_length += temp.first;

	for(int i=0;i<arr[temp.second.first].size();i++)	//here it comes. . .
	{
		if(visited[arr[temp.second.first][i].second.first]==1)continue;	//Aaaargh, looong indices in the if!! :(
		q.insert(arr[temp.second.first][i]);
	}
}


INSTEAD, use this.

PIII temp;
while(!q.empty())
{
	temp=*q.begin();q.erase(q.begin());
	int l = temp.first, u = temp.second.first, v = temp.second.second;
	if(visited[u]==1)continue;
	visited[u]++;
	total_length += l;
	
	for(int i=0;i<arr[u].size();i++)
	{
		int w = arr[u][i].second.first;
		if(visited[w]==1)continue;
		q.insert(arr[u][i]);	
	}
}

What's the difference? I've put in meaningful variables in-between all the firsts and the seconds and also in the for loop, I don't have to worry much about what I need to do. So, WHENEVER you traverse the adjacency list of u using a forloop, ENSURE that you have an "int v = adjl[u][i];" right at the beginning! You don't want to have to do arbitrary checks on adjl[u][i] later on in the loop.



Overloading a bool operator returns a boolean. It does not return a value in {0, -1, 1} depending on less-than, equal-to, or greater-than.


Now here's another cool bug to avoid. If you make it, trust me, its gonna be hard to spot. It can be avoided by simply using a few more meaningful variables (like in the adjacency-list traversal).
So comp[u] gives you the component of vertex u.
Whats the error in this pseudocode?
merge(u, v)
	//assume u and v have difference comps, you've checked before calling the function.
	for(int i = 0; i < N; i++)
		if(comp[i] == comp[u]) comp[i] = comp[v];

It simply goes through all the nodes, and if it is of u's component, it makes it go to v's component.

So where's the bug?


<blank space left in case you want to search for it yourself. Spoiler alert :P>


<I've decided to tell you where it is, cuz its probably the only way I can explain how easily it can be avoided>


:
.


The bug is that at some point, i will be equal to u, and at that point, comp[u] will ITSELF get modified. For example, if your comp array (0-based) looks like
1 1 1 4 4
and you call merge (1, 4), you would get
4 4 1 4 4
instead of getting
4 4 4 4 4.

What you Should have done is
merge(u, v)
	c = comp[u];
	for(int i = 0; i < N; i++)
		if(comp[i] == c) comp[i] = comp[v];

There's a LOT that just DECLARING an extra variable can be used for you know!! 
I call this kind of bug the "substitution-bug", and I know from experience, this is one HARD-to-spot bug; and when you DO spot this bug, its one of those curse-yourself-to-hell-for-making-it moments.

A lot of you are using built-in functions and STL, but I did see one user-defined qsort() function too. Spare yourself the trouble; your sort is NOT faster than the built-in sort.


I hope that after this everyone gets themselves comfortable with Union-Find etc. Its about time people!

Good Luck!
Pradeep.