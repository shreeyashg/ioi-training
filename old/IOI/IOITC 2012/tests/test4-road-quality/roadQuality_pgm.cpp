/*
 *  roadQuality_pgm.cpp
 */

#include <vector>
#include <algorithm>
#include <cstdio>
#include <cstring>
using namespace std;

#define s(T) scanf("%d", &T)

const int max_n = 5005;
const int max_m = 1000005;

struct road 
{
	int from;
	int to;
	int id;
	long long len;
	int qual;
	
	road()
	{
		from = to = id = len = qual = 0;
	}
	
	road(int f, int t, int i, long long l, int q)
	{
		from = f;
		to = t;
		id = i;
		len = l;
		qual = q;
	}
};

bool compL(road r1, road r2)
{
	return r1.len < r2.len ||
	(r1.len == r2.len && r1.id < r2.id);
}

bool compQ(road r1, road r2)
{
	return r1.qual < r2.qual ||
	(r1.qual == r2.qual && r1.id < r2.id);
}

vector <road> R;
int N, M;

int comps[max_n];
int sizes[max_n];

int findComponent(int u)
{
	if(comps[u] == u) return u;
	return comps[u] = findComponent(comps[u]);
}

void merge(int u, int v)
{
	int uComp = findComponent(u);
	int vComp = findComponent(v);
	if(sizes[vComp] < sizes[uComp])
	{
		comps[vComp] = uComp;
		sizes[uComp] += sizes[vComp];
	}
	else 
	{
		comps[uComp] = vComp;
		sizes[vComp] += sizes[uComp];
	}
}

void input()
{
	s(N);
	s(M);
	
	for(int i = 0; i < M; i++)
	{
		int f, t, c, q;
		s(f); s(t); s(q); s(c); 
		R.push_back(road(f, t, i, c, q));
	}
}

int mQ;
long long mC;

void initKruskal()
{
	for(int i = 0; i < N; i++)
		comps[i] = i, sizes[i] = 0;
}

void doQualityKruskal()
{
	initKruskal();
	sort(R.rbegin(), R.rend(), compQ);
	mQ = 1000000007;			//INF

	int edgesAdded = 0;
	for(int i = 0; i < M && edgesAdded < N-1; i++)
	{
		road cur = R[i];
		if(findComponent(cur.from) != findComponent(cur.to))
		{
			mQ = min(mQ, cur.qual);
			merge(cur.from, cur.to);
			edgesAdded++;
		}
	}
	
	for(int i = 1; i < N; i++)
		if(findComponent(i) != findComponent(0))
			mQ = -1;
}

void doLengthKruskal()
{
	initKruskal();
	sort(R.begin(), R.end(), compL);
	mC = 0;
	
	for(int i = 0; i < M; i++)
	{
		road cur = R[i];
		if(cur. qual < mQ)
			continue;
		if(findComponent(cur.from) != findComponent(cur.to))
		{
			mC += cur.len;
			merge(cur.from, cur.to);
		}
	}
}

void solve()
{
	doQualityKruskal();
	if(mQ < 0)
	{	
		printf("-1\n");
		return;
	}
	doLengthKruskal();
	printf("%d %lld\n", mQ, mC);
}

int main()
{
	input();
	solve();
}
