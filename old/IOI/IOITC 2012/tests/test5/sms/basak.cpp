#include<vector>
#include<stack>
#include<set>
#include<map>
#include<queue>
#include<deque>
#include<string>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<cassert>
#include<cstdlib>
#include<cstdio>
#include<cmath>
#include<string>
//#include<debugger>

using namespace std;

#define s(n)					scanf("%d",&n)
#define sl(n) 					scanf("%lld",&n)
#define sf(n) 					scanf("%lf",&n)
#define ss(n) 					scanf("%s",n)
#define INF						(int)1e9
#define LINF					(long long)1e18
#define EPS						1e-9
#define maX(a,b)				((a)>(b)?(a):(b))
#define miN(a,b)				((a)<(b)?(a):(b))
#define abS(x)					((x)<0?-(x):(x))
#define FOR(i,a,b)				for(int i=a;i<b;i++)
#define REP(i,n)				FOR(i,0,n)
#define foreach(v,c)            for( typeof((c).begin()) v = (c).begin();  v != (c).end(); ++v)
#define mp						make_pair
#define FF						first
#define SS						second
#define tri(a,b,c)				mp(a,mp(b,c))
#define XX						first
#define YY						second.first
#define ZZ						second.second
#define pb						push_back
#define fill(a,v) 				memset(a,v,sizeof a)
#define all(x)					x.begin(),x.end()
#define SZ(v)					((int)(v.size()))
#define DREP(a)					sort(all(a)); a.erase(unique(all(a)),a.end())
#define INDEX(arr,ind)			(lower_bound(all(arr),ind)-arr.begin())
#define debug(args...)			{dbg,args; cerr<<endl;}
#define dline					cerr<<endl	

void sc(char &c){
	char temp[4];	ss(temp);	
	c=temp[0];
}

struct debugger
{
	template<typename T> debugger& operator , (const T& v)
	{	
		cerr<<v<<" ";	
		return *this;	
	}
} dbg;




typedef long long LL;
typedef pair<int,int> PII;
typedef pair<LL,LL> PLL;
typedef pair<int,PII> TRI;

typedef vector<int> VI;
typedef vector<LL> VL;
typedef vector<PII> VII;
typedef vector<PLL> VLL;
typedef vector<TRI> VT;

typedef vector<VI> VVI;
typedef vector<VL> VVL;
typedef vector<VII> VVII;
typedef vector<VLL> VVLL;
typedef vector<VT> VVT;


/*Main code begins now */

int testnum;
const LL mod = 100000007;
LL a[300005];
LL b[400005];

int key[] = {2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,7,7,7,7,8,8,8,9,9,9,9};
int rep[] = {1,2,3,1,2,3,1,2,3,1,2,3,1,2,3,1,2,3,4,1,2,3,1,2,3,4};
int len[] = {-1,-1,3,3,3,3,3,4,3,4};

void preprocess()
{
	a[0]=0;
	a[1]=1;
	a[2]=2;
	for(int i=3;i<300005;i++)
		a[i]=(a[i-1]+a[i-2]+a[i-3]+1)%mod;
		
	b[0]=0;
	b[1]=1;
	b[2]=2;
	b[3]=4;
	for(int i=4;i<400005;i++)
		b[i]=(b[i-1]+b[i-2]+b[i-3]+b[i-4]+1)%mod;
}

char temp[100005];

void solve()
{
	int n=strlen(temp);
	LL ans=1;
	int prev=-1,cnt=1;
	
	for(int i=0;i<=n;i++)
	{
		int z,cur;
		if(i==n)
		{
			z=-2;
			cur=0;
		}
		else
		{
			z=key[temp[i]-'a'];
			cur=rep[temp[i]-'a'];
		}
		
		if(z==prev)
			cnt+=cur;
		else
		{
			//debug(prev,cnt,b[cnt],a[cnt]);
			ans = (ans * (len[prev]==4 ? b[cnt] : a[cnt])) % mod;
			prev=z;
			cnt=cur;
		}
	}
	
	printf("%lld\n",ans);
		
		
}



bool input()
{
	ss(temp);
	return true;
}


int main()
{
	preprocess();
	int T; s(T);
	for(testnum=1;testnum<=T;testnum++)
	{
		if(!input()) break;
		solve();
	}
}

