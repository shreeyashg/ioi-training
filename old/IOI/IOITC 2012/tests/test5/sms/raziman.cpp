# include <cstdio>
# define BIGPRIME 100000007
# define LIMIT 1000000

int f3[LIMIT],g3[LIMIT];
int f4[LIMIT],g4[LIMIT];

char mapping[]="22233344455566677778889999";
char counter[]="12312312312312312341231234";

void initialise()
{
	f3[1]=g3[1]=f4[1]=g4[1]=1,f3[2]=g3[2]=f4[2]=g4[2]=2,f3[3]=g3[3]=f4[3]=g4[3]=4,f3[4]=7,g3[4]=f4[4]=g4[4]=8;
	for(int i=5;i<LIMIT;i++)
	{
		f3[i]=(f3[i-1]+f3[i-2]+f3[i-3])%BIGPRIME;
		f4[i]=(f4[i-1]+f4[i-2]+f4[i-3]+f4[i-4])%BIGPRIME;
		g3[i]=(g3[i-3]+f3[i])%BIGPRIME;
		g4[i]=(g4[i-4]+f4[i])%BIGPRIME;
	}
}

int main()
{
	initialise();
	
	int T;
	scanf("%d",&T);
	
	for(int t=0;t<T;t++)
	{
		char inp[LIMIT];
		scanf("%s",inp);
		
		long long ret=1ll;
		for(int i=0,j;inp[i];i=j)
		{
			int k=0;
			for(j=i;inp[j];j++)
				if(mapping[inp[i]-'a']!=mapping[inp[j]-'a'])
					break;
				else
					k+=counter[inp[j]-'a']-'0';
				ret=(ret*((mapping[inp[i]-'a']=='7'||mapping[inp[i]-'a']=='9')?g4[k]:g3[k]))%BIGPRIME;
		}
		printf("%Ld\n",ret);
	}
	return 0;
}
