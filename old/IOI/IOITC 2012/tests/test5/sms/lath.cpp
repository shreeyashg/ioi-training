#include"stdio.h"
#define N 400137
#define MOD 100000007
int f[2][N],g[2][N],cl[200];
int main()
{
	for(int i=3;i<5;i++)
	{
		f[i-3][0]=g[i-3][0]=1;
		for(int l=1;l<N;l++){	for(int j=1;j<=i&&j<=l;j++)	f[i-3][l]+=f[i-3][l-j];		f[i-3][l]%=MOD;		}
		for(int l=0;l<=i;l++)	g[i-3][l]= f[i-3][l];
		for(int l=i+1;l<N;l++)	g[i-3][l]=(f[i-3][l]+g[i-3][l-i])%MOD;
	}
	cl['a']=cl['b']=cl['c']='a'-1;
	cl['d']=cl['e']=cl['f']='c';
	cl['g']=cl['h']=cl['i']='f';
	cl['j']=cl['k']=cl['l']='i';
	cl['m']=cl['n']=cl['o']='l';
	cl['p']=cl['q']=cl['r']=cl['s']='o';
	cl['t']=cl['u']=cl['v']='s';
	cl['w']=cl['x']=cl['y']=cl['z']='v';
	int T;
	scanf("%d",&T);
	while(T--)
	{
		char inp[N+1];
		scanf("%s",inp);
		long long ans=1,sum=0,lastcl=9;
		for(int i=0;inp[i];){
			while(cl[inp[i]]==lastcl)
				sum+=inp[i]-lastcl,i++;
			ans=(ans*g[lastcl=='o'||lastcl=='v'][sum])%MOD,sum=0,lastcl=cl[inp[i]];
		}
		printf("%Ld\n",ans);
	}
}
