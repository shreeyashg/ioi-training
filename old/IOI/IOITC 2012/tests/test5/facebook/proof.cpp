#include<vector>
#include<stack>
#include<set>
#include<map>
#include<queue>
#include<deque>
#include<string>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<cassert>
#include<cstdlib>
#include<cstdio>
#include<cmath>
#include<string>
//#include<debugger>
 
using namespace std;
 
#define s(n) scanf("%d",&n)
#define sl(n) scanf("%lld",&n)
#define sf(n) scanf("%lf",&n)
#define ss(n) scanf("%s",n)
#define INF (int)1e9
#define LINF (long long)1e18
#define EPS 1e-9
#define maX(a,b) ((a)>(b)?(a):(b))
#define miN(a,b) ((a)<(b)?(a):(b))
#define abS(x) ((x)<0?-(x):(x))
#define FOR(i,a,b) for(int i=a;i<b;i++)
#define REP(i,n) FOR(i,0,n)
#define foreach(v,c) for( typeof((c).begin()) v = (c).begin(); v != (c).end(); ++v)
#define mp make_pair
#define FF first
#define SS second
#define tri(a,b,c) mp(a,mp(b,c))
#define XX first
#define YY second.first
#define ZZ second.second
#define pb push_back
#define fill(a,v) memset(a,v,sizeof a)
#define all(x) x.begin(),x.end()
#define SZ(v) ((int)(v.size()))
#define DREP(a) sort(all(a)); a.erase(unique(all(a)),a.end())
#define INDEX(arr,ind) (lower_bound(all(arr),ind)-arr.begin())
#define debug(args...) {dbg,args; cerr<<endl;}
#define dline cerr<<endl
 
void sc(char &c){
char temp[4]; ss(temp);
c=temp[0];
}
 
struct debugger
{
template<typename T> debugger& operator , (const T& v)
{	
cerr<<v<<" ";
return *this;
}
} dbg;
 
 
 
 
typedef long long LL;
typedef pair<int,int> PII;
typedef pair<LL,LL> PLL;
typedef pair<int,PII> TRI;
 
typedef vector<int> VI;
typedef vector<LL> VL;
typedef vector<PII> VII;
typedef vector<PLL> VLL;
typedef vector<TRI> VT;
 
typedef vector<VI> VVI;
typedef vector<VL> VVL;
typedef vector<VII> VVII;
typedef vector<VLL> VVLL;
typedef vector<VT> VVT;
 
 
/*Main code begins now */
 
int testnum;
int N,M,Q;
VI adjl[7005];
VI adjd[7005];
int ebeg[50005];
int eend[50005];
 
int dist[2][7005];
int par[2][7005];
 
void djikstra(int mode)
{
int src = mode==0 ? 0 : N-1;
set<PII> q;
dist[mode][src]=0;
par[mode][src]=-1;
q.insert(mp(0,src));
while(!q.empty())
{
PII temp = *(q.begin());
q.erase(q.begin());
int d=temp.FF;
int u=temp.SS;
if(d>dist[mode][u]) continue;
for(int i=0;i<adjl[u].size();i++)
{
int v=adjl[u][i];
int len = d+adjd[u][i];
if(dist[mode][v] > len)
{
dist[mode][v]=len;
par[mode][v]=u;
q.insert(mp(len,v));
}
}
}
}
int posmap[7005];
int revmap[7005];
int top;
 
int meet[2][7005];
const int BASE=1<<17;
int seg[BASE+BASE];
int ans[7005];
 
int lo,hi;
void update(int chaathukkutty,int beg,int end,int val)
{
if(beg>hi || end<lo) return;
if(lo<=beg && end<=hi)
{
seg[chaathukkutty] = min(seg[chaathukkutty],val);
return;
}
int twice=chaathukkutty<<1;
int mid = (beg+end)>>1;
update(twice,beg,mid,val);
update(twice+1,mid+1,end,val);
}
 
void update(int _lo,int _hi,int val)
{
lo=_lo;
hi=_hi;
if(lo>hi) return;
//debug("update",lo,hi,val);
update(1,0,BASE-1,val);
}
 
int query(int u)
{
u+=BASE;
int ans = seg[u];
for(u>>=1; u; u>>=1)
ans = min(ans, seg[u]);
return ans;
}
 
void preprocess()
{
 
}
 
void solve()
{
for(int i=0;i<2;i++)
for(int j=0;j<N;j++)
dist[i][j]=INF;
fill(par,-1);
djikstra(0);
djikstra(1);
//for(int i=0;i<N;i++)
// debug("dists and parents",i,":",dist[0][i],par[0][i],dist[1][i],par[1][i]);
fill(meet,-1);
fill(revmap,-1);
int cur=0;
top=0;
while(cur!=N-1)
{
posmap[top]=cur;
revmap[cur]=top;
meet[0][cur]=top;
meet[1][cur]=top;
top++;
cur=par[1][cur];
}
posmap[top]=cur;
revmap[cur]=top;
meet[0][cur]=top;
meet[1][cur]=top;
top++;
//debug("outside");
//for(int i=0;i<top;i++)
// debug("posmap",i,posmap[i]);
//for(int i=0;i<N;i++)
// debug("meets",i,":",meet[0][i],meet[1][i]);
for(int mode=0;mode<2;mode++)
for(int i=0;i<N;i++)
{
if(meet[mode][i]>=0) continue;
int cur=i;
while(meet[mode][cur]<0)
cur = par[mode][cur];
meet[mode][i]=meet[mode][cur];
}
//for(int i=0;i<N;i++)
// debug("meets",i,":",meet[0][i],meet[1][i]);
for(int i=0;i<BASE+BASE;i++)
seg[i]=INF;
for(int i=0;i<N;i++)
for(int j=0;j<adjl[i].size();j++)
{
int v=adjl[i][j];
int d=adjd[i][j] + dist[0][i] + dist[1][v];
if(revmap[v]<0 || revmap[i]<0 || revmap[v] != revmap[i]+1)
{
//debug("edge",i,v,d,meet[0][i],meet[1][v]-1);
update(meet[0][i],meet[1][v]-1,d);
}
}
//for(int i=0;i<top-1;i++)
// debug(i,query(i));
for(int i=0;i<top-1;i++)
{
ans[i]=query(i);
for(int j=0;j<N;j++)
if(meet[0][j]<=i && meet[1][j]>=i+1)
ans[i] = min(ans[i], dist[0][j]+dist[1][j]);
}
for(int i=0;i<Q;i++)
{
int chaathukkutty; s(chaathukkutty);
int x = revmap[ebeg[chaathukkutty]];
int y = revmap[eend[chaathukkutty]];
if(y<x) swap(x,y);
if(x<0 || y<0 || y!=x+1)
{
printf("%d\n",dist[1][0]);
continue;
}
int z = ans[x];
if(z>=INF)
printf("-1\n");
else
printf("%d\n",query(x));
}
}
 
 
 
bool input()
{
s(N); s(M); s(Q);
for(int i=0;i<N;i++)
{
adjl[i].clear();
adjd[i].clear();
}
for(int i=0;i<M;i++)
{
int a,b,d;
s(a); s(b); s(d);
adjl[a].pb(b);
adjd[a].pb(d);
adjl[b].pb(a);
adjd[b].pb(d);
ebeg[i]=a;
eend[i]=b;
}
return true;
}
 
 
int main()
{
preprocess();
input();
solve();
}
