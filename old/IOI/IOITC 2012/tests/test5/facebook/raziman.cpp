# include <cstdio>
# include <cstring>
# include <set>
# include <vector>
# include <algorithm>
# define NMAX 100000
# define MMAX 100000
# define INF 2000000000
using namespace std;

struct two
{
	int v,w;
	two(int a,int b){v=a;w=b;}
};
vector<two>adjlist[NMAX];

struct three
{
	int u,v,w;
	three(int a=0,int b=0,int c=0){u=a;v=b;w=c;}
	bool operator<(const three t)const
	{
		return w!=t.w?w<t.w:u!=t.u?u<t.u:v<t.v;
	}
}edges[MMAX];
set<three>edgeset;

int dj[NMAX];
struct djcomp
{
	bool operator()(const int& i,const int& j)
	{
		return dj[i]!=dj[j]?dj[i]<dj[j]:i<j;
	}
};
set<int,djcomp>djset;

int N,M,Q;
char seen[NMAX];
int dist1[NMAX],dist2[NMAX];
int parent[NMAX],depth[NMAX];
vector<int>depthlist[NMAX];
int result[NMAX];
void dijkstra(int start)
{
	fill(dj,dj+N,INF);
	fill(parent,parent+N,-1);
	dj[start]=0;
	djset.clear();
	djset.insert(start);
	while(!djset.empty())
	{
		int u=*(djset.begin());
		if(dj[u]>=INF)break;
		djset.erase(djset.begin());
		for(int i=0;i<adjlist[u].size();i++)
		{
			int v=adjlist[u][i].v,w=adjlist[u][i].w;
			if(dj[v]>dj[u]+w)
			{
				djset.erase(v);
				dj[v]=dj[parent[v]=u]+w;
				djset.insert(v);
			}
		}
	}
}

int getdepth(int v)
{
	return(depth[v]==-1)?depth[v]=getdepth(parent[v]):depth[v];
}

int main()
{
	int T;
	T=1;//scanf("%d",&T);
	for(int t=0;t<T;t++)
	{
		scanf("%d%d%d",&N,&M,&Q);
		for(int i=0;i<N;i++)adjlist[i].clear();
		for(int i=0;i<M;i++)
		{
			scanf("%d%d%d",&edges[i].u,&edges[i].v,&edges[i].w);
			adjlist[edges[i].u].push_back(two(edges[i].v,edges[i].w));
			adjlist[edges[i].v].push_back(two(edges[i].u,edges[i].w));
		}
		dijkstra(N-1);
		for(int i=0;i<N;i++)
			dist2[i]=dj[i];
		dijkstra(0);
		for(int i=0;i<N;i++)
			dist1[i]=dj[i];
		fill(depth,depth+N,-1);
		for(int i=N-1,j=NMAX-1;i>=0;depth[i]=j--,i=parent[i])depthlist[j].clear();
		for(int i=0;i<N;i++)depthlist[getdepth(i)].push_back(i);
		edgeset.clear();
		fill(result,result+NMAX,-1);
		for(int d=depth[0];d<NMAX;d++)
		{
			for(int i=0;i<depthlist[d].size();i++)
			{
				int u=depthlist[d][i];
				for(int j=0;j<adjlist[u].size();j++)
				{
					two e=adjlist[u][j];int v=e.v;
					if(parent[u]==v||parent[v]==u||depth[u]==depth[v])continue;
					if(depth[v]>depth[u])edgeset.insert(three(u,v,dist1[u]+dist2[v]+e.w));
					else edgeset.erase(edgeset.find(three(v,u,dist1[v]+dist2[u]+e.w)));
				}
			}
			if(!edgeset.empty())result[d]=edgeset.begin()->w;
		}
		for(int q=0;q<Q;q++)
		{
			int torem,ret;
			scanf("%d",&torem);
			int u=edges[torem].u,v=edges[torem].v;
			if((u==parent[v]||v==parent[u])&&depth[u]!=depth[v])ret=result[min(depth[u],depth[v])];
			else ret=dist1[N-1];
			if(ret==-1)printf("-1\n");
			else printf("%d\n",ret);
		}
	}
}