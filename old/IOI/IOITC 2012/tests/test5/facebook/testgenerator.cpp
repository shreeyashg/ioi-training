/**
 * Code to generate the test data for the facebook problem.
 *
 * Input: N M Q C seed
 * where
 * - N = Number of people
 * - M = Number of entries
 * - Q = Number of queries
 * - C = Number of components
 * - seed = seed for the random number generator. 
 */

#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<set>
#include<utility>
#include<algorithm>

using namespace std;

#define MAXVERT 7001
#define MAXEDGE 50001
#define MAXQ 10000
#define MAXVAL 20000
#define MAXC 100

struct db_entry {
	pair <int,int> points;
	int val;
};

struct db_entry entries[MAXEDGE];

int components[MAXC][MAXVERT];
int compindices[MAXC];
int queries[MAXQ];

set <pair <int, int> > S;

void swap (int *a, int *b)
{
	int c;
	c = *a;
	*a = *b;
	*b  = c;
}

set <int> QS;

void add_entry(int j, int k, int *nr_entries)
{
	int temp;
	struct db_entry entry;
	/* Make sure that we have not added this already */
	if (j > k)
		swap(&j, &k);

	if (S.find(make_pair(j,k)) != S.end()) {
		return;
	}

	/* What should be the value on the edge */
	temp = ((rand() >> 12) % MAXVAL) + 1;

	/* Add this value into our set of entries */
	entry.points = make_pair(j,k);
	entry.val = temp;
	entries[*nr_entries] = entry;
	*nr_entries = *nr_entries+1;

	/* Record that we have added this value into our entry */
	S.insert(make_pair(j,k));

	//printf("entry[%d] = %d %d %d\n", *nr_entries-1, j,k, temp);
}

int main()
{
	bool done;
	unsigned int seed;
	int N, M, Q, C, comp = 0, nr_entries = 0, temp;
	int disc_query, disc_query_entry, mul, offset;
	struct db_entry entry;
	int i, j, k, l;

	cin >> N >> M >> Q >> C >> seed;

	srand(seed);

	/* Assign vertices to components */

	for (i = 0; i < N; i++) {
		if (i == 0) 
			comp = 0;
		else if (i == N - 1) 
			comp = C-1;
		else
			comp = (rand() >> 10) % C;
		
		components[comp][compindices[comp]++] = i;
		
	}

	/* Print the component distribution
	for (i = 0; i < C; i++) {
		cout << "Component " << i << ": ";
		for (j = 0; j < compindices[i]; j++)
			cout << components[i][j] << " ";
		cout << "\n";
	}
	*/


	/* Add edges for vertices within components */
	while (nr_entries < (9*M)/10) {
	for (i = 0; i < C; i++){
		if (nr_entries >= (9*M)/10)
			break;
	   for (j = 0; j < compindices[i]; j++) {
		   if (nr_entries >= (9*M)/10)
		   	break;
		   for (k = j+1; k < compindices[i]; k++) {
			   if (nr_entries >= (9*M)/10)
				   break;
			   /* Should there be an edge between j and k*/
			   temp = (rand() >> 20) %2;
			   if (temp == 0)
				   continue;

			   add_entry(j,k, &nr_entries);
		   }
	   }
	}
	}

	/* Add a edges connecting components C/2 and (C/2 + 1) */
	i = C/2;
	l = C/2+1;
	j = components[i][(rand() >> 10) % compindices[i]];
	k = components[l][(rand() >> 10) % compindices[l]];
	add_entry(j,k,&nr_entries);

	/* We want this entry to be present in the query since removing this entry is
	 * going to make the (s,t) pair disconnected. */
	disc_query = ((rand() >> 12) % Q);
	queries[disc_query] = nr_entries - 1;
	disc_query_entry = nr_entries - 1;
	QS.insert(nr_entries - 1);

	/* Add edges to ensure that there is at least one path from 0 to N -1 */
	add_entry(0,j,&nr_entries);
	add_entry(k,N-1, &nr_entries);


	if ((C%2) == 0) 
		offset = C/2;
	else
		offset = C/2+1;
	/* Add the edges for vertices across components */
	while (nr_entries < M) {

		/* We further partition the set of components into two. One containing components from 0 to C/2.
		 * The other containing components from C/2 + 1 to C - 1. We add edges for vertices belonging to 
		 * components in these partitions.
		 *
		 * We pick the partition where the edge is to be added.
		 */
		mul = (rand() >> 20) % 2;

		i = (rand() >> 20) % (C/2);
		l = i;
		while (i == l)
			l = (rand() >> 20) % (C/2);

		/* Pick vertices from the correct partition */
		i = i + mul*offset;
		l = l + mul*offset;
		j = components[i][(rand() >> 10) % compindices[i]];
		k = components[l][(rand() >> 10) % compindices[l]];
		add_entry(j, k, &nr_entries);
	}

	/* Generate Queries */
	for (i = 0; i < Q; i++) {
		if (i == disc_query)
			continue;

		done = false;

		while(!done) {
			temp = (rand() >> 12) % M;

			if (QS.find(temp) != QS.end())
				continue;

			queries[i] = temp;
			done = true;
		}
	}

	random_shuffle(entries, entries+M);
	random_shuffle(queries, queries+Q);
	printf("%d %d %d\n", N, M ,Q);
	for ( i = 0; i < M; i++) {
//		if (i == disc_query_entry)
//			printf("============= %d ===================\n", i);
		printf("%d %d %d\n", entries[i].points.first, entries[i].points.second, entries[i].val);
//		if (i == disc_query_entry)
//			printf("===================================\n");
	}

	for (i = 0; i < Q; i++){
//		if (i == disc_query)
//			printf("===================================\n");
		cout << queries[i] <<"\n";
//		if (i == disc_query)
//			printf("===================================\n");
	}

	return 0;
}
