//#pragma comment(linker, "/STACK:16777216")
#include <iostream>
#include <cstdio>
#include <cmath>
#include <set>
#include <vector>
#include <map>
#include <cstring>
#include <sstream>
#include <algorithm>
#include <string>
#include <queue>
#include <fstream>

#define FOR(i,a,b) for(int i = (a); i <= (b); i++)
#define FR(i,a) for(int i = 0; i < (a); i++)
#define DR(i,a) for(int i = (a)-1; i >=0; i--)
#define DOWN(i,a,b) for(int i = (a); i >= (b); i--)
#define FORD(i,a,b) for(int i = (a), _b = (b); i >= _b; i--)
#define REPD(i,n) for(int i = (n) - 1; i >= 0; i--)
#define PB push_back
#define MP make_pair
#define F first
#define S second
#define RESET(c,x) memset(c,x,sizeof(c))
#define SIZE(c) (c).size()
#define ALL(c) (c).begin(), (c).end()

#define REP(i,a) for(int i = 0; i < (a); i++)

#define sqr(x) ((x)*(x))
#define oo 2000000009

using namespace std;
/*************************TEMPLATE**********************************/
long long convertToNum(string s)
{
    long long val = 0; FR(i,s.size()) val = val * 10 + s[i] - '0';
    return val;
}
char bu[50];
string convertToString(int a) {
    sprintf(bu,"%d",a);
    return string(bu);
}
long long GCD(long long x,long long y)  {
    if (!x) return y; if (!y) return x;
    if (x == y) return x; if (x < y) return GCD(x,y%x); else return GCD(x%y,y);
}
long long POW(long long x,long long y,long long Base){
    if (!y) return 1; long long u = POW(x,y/2,Base);
    u = (u * u) % Base;
    if (y & 1) return (u * x) % Base; else return u;
}

//newstate = (newstate-1) & oldstate
/**************************CODE HERE*****************************/

void OPEN() {
    freopen("test.in","r",stdin);
    freopen("test.out","w",stdout);
}
#define maxn 60003
int n,Q,g[maxn];
int x[maxn],y[maxn], Trace[maxn], prev[maxn],d[maxn];
bool dd[maxn], onRoad[maxn], roadNode[maxn];
priority_queue< pair<int,int> > Heap;
vector<int> list;
vector< pair<int,int> > a[maxn];
vector< int> cs[maxn];
int D[maxn],f[maxn];
int m,q;

set< pair<int,int> > se;
void init() {
    FR(i,n) g[i] = oo, dd[i] = true;
    g[n-1] = 0;
    while (!Heap.empty()) Heap.pop();
    Heap.push( make_pair(-g[n-1],n-1));
    while (!Heap.empty()) {
        pair<int,int> tmp = Heap.top();
        Heap.pop();
        if (!dd[tmp.second]) continue;
        int u = tmp.second;
        dd[u] = false;
        FR(i,a[u].size()) {
            int v = a[u][i].first;
            if (!dd[v]) continue;
            if (g[v] > g[u] + a[u][i].second) {
                g[v] = g[u] + a[u][i].second;
                Heap.push( make_pair(-g[v],v) );
            }
        }
    }
    
    FR(i,n) d[i] = oo, dd[i] = true, Trace[i] = -1;
    d[0] = 0;
    while (!Heap.empty()) Heap.pop();
    Heap.push( make_pair(-d[0],0));
    while (!Heap.empty()) {
        pair<int,int> tmp = Heap.top();
        Heap.pop();
        if (!dd[tmp.second]) continue;
        int u = tmp.second;
        dd[u] = false;
        FR(i,a[u].size()) {
            int v = a[u][i].first;
            if (!dd[v]) continue;
            if (d[v] > d[u] + a[u][i].second) {
                d[v] = d[u] + a[u][i].second;
                Trace[v] = cs[u][i];
                Heap.push( make_pair(-d[v],v) );
            }
        }
    }
    FR(i,m) onRoad[i] = false;
    FR(i,n) roadNode[i] = false;
    int i = n-1,j;
    list.clear();
    list.push_back(i);
    roadNode[n-1] = true;
    while (i != 0) {
        j = Trace[i];
        onRoad[j] = true;
        if (y[j] == i) prev[i] = x[j], i = x[j];
        else prev[i] = y[j], i = y[j];
        list.push_back(i);
        roadNode[i] = true;
    }
    
    reverse(list.begin(),list.end());
    FR(i,n) dd[i] = true;
    while (!Heap.empty()) Heap.pop();
    
    FR(i,n) D[i] = oo;
    int save = oo;
    se.clear();
    for(int i = list.size() - 1; i >= 1; i--) {
        int v = list[i];
        int u = prev[v];
        int roadNo = Trace[v];
        f[roadNo] = oo;
        dd[v] = false;
//        if (D[v] > g[v]) 
        {
            D[v] = g[v];
            Heap.push(make_pair(-g[v],v));
            while (!Heap.empty()) {
                pair<int,int> tmp = Heap.top();
                Heap.pop();
                int u = tmp.second;
                FR(i,a[u].size()) {
                    if (onRoad[cs[u][i]]) continue;
                    int v = a[u][i].first;
                    if (D[v] > D[u] + a[u][i].second) {
                        D[v] = D[u] + a[u][i].second;
                        if (!roadNode[v])
                        Heap.push(make_pair(-D[v],v) );
                        if (dd[v] && roadNode[v] && f[roadNo] > D[v] + d[v]) {
                            se.insert( make_pair(D[v] + d[v], v) );
                        }
                    }
                }   
            }
        }
        while (!se.empty()) {
            set< pair<int,int> > ::iterator tmp = se.begin();
            if (dd[(*tmp).second]) {
                f[roadNo] = (*tmp).first;
                break;
            }
            se.erase(tmp);
        }
    }
}
int main() {
//    OPEN();
    int ntest;
    //scanf("%d",&ntest);
    //while (ntest--) {
        scanf("%d%d%d",&n,&m,&q);
        FR(i,n) a[i].clear(), cs[i].clear();
        int u,v,len;
        FR(i,m) {
            scanf("%d%d%d",&x[i],&y[i],&len);
            u = x[i], v = y[i];
            a[u].push_back( make_pair(v,len) );
            a[v].push_back( make_pair(u,len) );
            cs[u].push_back(i);
            cs[v].push_back(i);
        }
        init();
        int index;
        FR(i,q) {
            scanf("%d",&index);
            if (n == 1) {
                printf("%d\n",0);
                continue;
            }
            if (!onRoad[index]) {
                printf("%d\n",d[n-1]);
            }
            else {
                if (f[index] == oo) printf("-1\n");
                else printf("%d\n",f[index]);
            }
        }
   // }
    return 0;
}

