#include <cstdio>
   #include <queue>
   #include <memory.h>
   #include <vector>
   #include <set>
   #include <map>
   #include <iostream>
   #include <string>
   #include <cstring>
   #include <algorithm>
   #include <cmath>
   #include <ctime>
   using namespace std;

   #define forn( i,n ) for ( int i=0; i<(int)(n); i++ )
   #define pb push_back
   typedef long long ll;
   typedef long double ld;
   typedef pair<int,int> pii;

   const int inf = (int)1e9 + 7;

   struct Edge {
   int dst, wgt, ind;
   Edge( int a, int b, int c ) {
   dst = a;
   wgt = b;
   ind = c;
   }
   Edge() { dst=wgt=ind = 0; }
   };

   priority_queue<pii, vector<pii>, greater<pii> > q;
   int d1[7010], p1[7010], d2[7010], p2[7010];
   vector<Edge> g[7010];
   int ans[50010];
   int n, m, Q, x, y, z;
   int a[50010], b[50010], c[50010];
   bool inmin[50010], isbridge[50010];
   vector<int> mpe;
   int ite, ue[50010];
   int used[7010], it, tin[7010], fup[7010], timer;

   void Dijkstra(int d[], int p[]) {
   while ( !q.empty() ) {
   pii z = q.top();
   q.pop();
   if ( d[z.second] < z.first ) continue;
   int v = z.second;
   forn( i, g[v].size() ) {
   int j = g[v][i].dst;
   if ( d[j] > d[v] + g[v][i].wgt ) {
   d[j] = d[v] + g[v][i].wgt;
   p[j] = g[v][i].ind;
   q.push( pii( d[j], j ) );
   }
   }
   }
   }

   int get( int id ) {
   if ( inmin[id] ) return ans[id];
   else return d1[0];
   }

   vector<pii> edges;

   void go() {
   int mx = inf;
   edges.clear();
   forn( i, m )
   if ( !inmin[i] ) {
   if ( d2[ a[i] ] + c[i] + d1[ b[i] ] >
   d2[ b[i] ] + c[i] + d1[ a[i] ] )
   swap( a[i], b[i] );
   int len = d2[ a[i] ] + c[i] + d1[ b[i] ];
   edges.pb( pii( len, i ) );
   }
   sort( edges.begin(), edges.end() );
   /*
   p * r*intf( "mpe:" );
   forn( i, mpe.size() )
   printf( " (%d-%d)", a[ mpe[i] ], b[ mpe[i] ] );
   printf( "\n" );
   */
   forn( q, edges.size() ) {
   int len = edges[q].first;
   int i = edges[q].second;
   if ( len >= mx ) continue;
   // printf( "now look at: %d %d\n", a[i], b[i] );
   ite++;
   int j = a[i], e;
   while ( j != 0 ) {
   e = p2[j];
   ue[e] = ite;
   j = a[e] == j ? b[e] : a[e];
   }
   j = b[i];
   while ( j != n-1 ) {
   e = p1[j];
   ue[e] = ite;
   j = a[e] == j ? b[e] : a[e];
   }
   mx = -1;
   forn( q, mpe.size() ) {
   e = mpe[q];
   if ( ue[e] != ite && len < ans[e] ) ans[e] = len;
   if ( ans[e] > mx ) mx = ans[e];
   }
   }
   }

   void dfs( int v, int pe ) {
   used[v] = it;
   tin[v] = fup[v] = timer++;
   forn( i, g[v].size() ) {
   int to = g[v][i].dst;
   if ( g[v][i].ind == pe ) continue;
   if ( used[to] == it )
   fup[v] = min( fup[v], tin[to] );
   else {
   dfs( to, g[v][i].ind );
   fup[v] = min( fup[v], fup[to] );
   if ( fup[to] > tin[v] ) {
   isbridge[ g[v][i].ind ] = true;
   // printf( "set as bridge: %d - %d %d %d\n", i, a[ g[v][i].ind ],
//b[ g[v][i].ind ], c[ g[v][i].ind ] );
   }
   }
   }
   }

   void bridges() {
   it++;
   timer = 0;
   dfs( 0, -1 );
   }

   void solve() {
   scanf( "%d %d %d", &n, &m, &Q );
   forn( i, n ) g[i].clear(); //, h[i] = 0;

   forn( i, m ) {
   scanf( "%d %d %d", &x, &y, &z );
   g[x].pb( Edge( y, z, i ) );
   g[y].pb( Edge( x, z, i ) );
   inmin[i] = false;
   isbridge[i] = false;
   ans[i] = inf;
   a[i] = x, b[i] = y, c[i] = z;
   }

   forn( i, n ) d1[i] = inf;
   d1[n-1] = 0;
   while ( !q.empty() ) q.pop();
   q.push( pii( 0, n-1 ) );

   Dijkstra(d1, p1);

   forn( i, n ) d2[i] = inf;
   d2[0] = 0;
   while ( !q.empty() ) q.pop();
   q.push( pii( 0, 0 ) );

   Dijkstra(d2, p2);

   bridges();

   int cur = 0, id;
   if ( d1[0] != inf ) {
   mpe.clear();
   while ( cur != n-1 ) {
   int e = p1[cur];
   inmin[e] = true;
   if ( !isbridge[e] ) mpe.pb( e );
   cur = a[e] == cur ? b[e] : a[e];
   }
   }

   if ( d1[0] != inf ) go();

   forn( w, Q ) {
   scanf( "%d", &id );
   if ( d1[0] == inf ) {
   printf( "-1\n" );
   continue;
   }
   int z = get( id );
   if ( z == -1 || z == inf ) printf( "-1\n" );
   else printf( "%d\n", z );
   }
   }

   int main() {
   int tc;
   tc=1; //scanf( "%d", &tc );
   while ( tc-- ) solve();
   return 0;
   } 
