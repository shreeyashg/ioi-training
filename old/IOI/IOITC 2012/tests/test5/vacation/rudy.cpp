#include<vector>
#include<stack>
#include<set>
#include<map>
#include<queue>
#include<deque>
#include<string>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<cassert>
#include<cstdlib>
#include<cstdio>
#include<cmath>
#include<string>

using namespace std;

#define s(n)					scanf("%d",&n)
#define mp						make_pair
#define tri(a,b,c)				mp(a,mp(b,c))
#define XX						first
#define YY						second.first
#define ZZ						second.second
#define pb						push_back
#define fill(a,v) 				memset(a,v,sizeof a)
#define all(x)					x.begin(),x.end()

typedef pair<int,pair<int,int> > TRI;
/*Main code begins now */

int testnum;
int N,Q;

const int BASE = 1<<15;
int arr[BASE];
int last[1<<20];

const int MAXQ = 200000;
TRI queries[MAXQ];
int ans[MAXQ];

int stree[BASE+BASE];
int total;

void update(int u,int val)
{
	total += val;
	for(u+=BASE; u; u>>=1)
		stree[u] += val;
}

int getsum(int u)
{
	if(u<0) return 0;
	int sum = stree[u+BASE];
	for(u+=BASE; u; u>>=1)
		if(u&1)
			sum += stree[u-1];
	return sum;
}

void solve()
{
	sort(queries, queries+Q);
	fill(last,-1);
	
	int qnum = 0;
	for(int i=0;i<N;i++)
	{
		int num = arr[i];
		int pos = last[num];;
		if(pos>=0)
			update(pos,-1);
		last[num]=i;
		update(i,+1);
		
		while(qnum<Q && queries[qnum].XX == i)
		{
			int ind = queries[qnum].YY;
			int beg = queries[qnum].ZZ;
			ans[ind] = total - getsum(beg-1);
			qnum++;
		}
	}
	
	for(int i=0;i<Q;i++)
		printf("%d\n",ans[i]);
}

bool input()
{
	s(N);
	assert(N<=30000);
	for(int i=0;i<N;i++)
	{
		s(arr[i]);
		assert(arr[i]>=1);
		assert(arr[i]<=1000000);
	}
		
	s(Q);
	assert(Q<=200000);
	for(int i=0;i<Q;i++)
	{
		int a; s(a); 
		int b; s(b);
		assert(1<=a && a<=b && b<=N);
		a--;
		b--;
		queries[i] = tri(b,i,a);
	}
	
	return true;
}


int main()
{
	input();
	solve();
}
