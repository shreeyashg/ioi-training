/*
* Author : Nikhil Garg
* Date   : 2010-12-26
* Team Proof's C++ template
*/

#include<iostream>
#include<sstream>
#include<vector>
#include<stack>
#include<set>
#include<map>
#include<queue>
#include<deque>
#include<bitset>
#include<algorithm>
#include<cstring>
#include<cassert>
#include<cstdlib>
#include<cstdio>
#include<cmath>
#include<cctype>
using namespace std;

#define pb								push_back
#define s(n)								scanf("%d",&n)
#define sl(n) 								scanf("%lld",&n)
#define sf(n) 								scanf("%lf",&n)
#define fill(a,v) 							memset(a, v, sizeof a)
#define INF								(int)1e9
#define EPS								1e-9

typedef long long LL;
typedef pair<int, int > PII;

int testCases, testNum;
struct event
{
	bool pair;
	int key;
	int other;
	int queryNum;
	event(bool p = false, int k = 0, int o = 0, int q = 0)
	{
		pair = p;
		key = k;
		other = o;
		queryNum = q;
	}
	friend bool operator < (event a , event b)
	{
		return (a.key < b.key) || ( a.key == b.key && !a.pair && b.pair) ;
	}
} all[300000];

int N, Q,total;
map<int,int> last;
int bit [30005];
int ans [200005];

void update(int i , int val )
{
	while( i <= 30000 )
	{
		bit[i] += val;
		i += i & -i;
	}
}

int query(int i )
{
	int ans = 0;
	while( i > 0)
	{
		ans += bit[i];
		i -= i & -i;
	}
	return ans;
}

void solve()
{
	sort( all, all + total );
	for(int i = 0; i < total; i++)
	{
		event e = all[i];
		if( e.pair )
			ans[e.queryNum] = query(e.key) - query(e.other - 1);
		
		else
		{
			int val = e.other;
			if( last[val] )
				update( last[val], -1);
			last[val] = e.key;
			update(e.key, 1 );
		}
	}
	for(int i = 0; i < Q; i++)
		printf("%d\n", ans[i]);
}

bool input()
{
	s(N);
	total = 0;
	for(int i = 1; i <= N; i++)
	{
		int a ; s(a);
		all[total++] = event( false, i, a);
	}
	s(Q);
	for(int i = 0; i < Q; i++)
	{
		int a, b; s(a); s(b);
		all[total++] = event( true, b, a,i);
	}
	return true;
}

int main()
{
	testCases = 1;
	for(testNum = 1; testNum <= testCases; testNum ++)
	{
		if(!input()) break;
		solve();
	}
}

