# include <cstdio>
# include <iostream>
# include <algorithm>
# include <vector>
# include <cstring>
# include <cctype>
# include <set>
# include <map>
# include <cmath>
# include <queue>
# include <string>

using namespace std;

const int NMAX=30000,QMAX=200000;

int ar[NMAX];

struct three
{
	int l,r,i;
}query[QMAX];

bool operator<(const three& t1,const three& t2)
{
	return t1.r<t2.r;
}

int segtree[65536];
const int basestart=32768;
int solution[QMAX];
map<int,int>lastpos;

void inc(int node,int delta)
{
	while(node>0)
	{
		segtree[node]+=delta;
		node>>=1;
	}
}

void add(int pos)
{
	int val=ar[pos];
	if(lastpos.count(val)!=0)inc(basestart+lastpos[val],-1);
	lastpos[val]=pos;
	inc(basestart+pos,1);
}

int getsum(int node,int l,int r,int L,int R)
{
	if(l==L&&r==R)return segtree[node];
	int M=(L+R)>>1;
	if(r<=M)return getsum(node<<1,l,r,L,M);
	else if(l>M)return getsum((node<<1)|1,l,r,M+1,R);
	else return getsum((node<<1),l,M,L,M)+getsum((node<<1)|1,M+1,r,M+1,R);
}

int main()
{
	int N,Q;
	
	scanf("%d",&N);
	for(int i=0;i<N;i++)
		scanf("%d",ar+i);
	
	scanf("%d",&Q);
	for(int i=0;i<Q;i++)
	{
		scanf("%d%d",&query[i].l,&query[i].r);
		query[i].l--,query[i].r--;
		query[i].i=i;
	}
	
	sort(query,query+Q);
	
	int done=0;
	for(int i=0;i<Q;i++)
	{
		while(done<=query[i].r)add(done++);
		solution[query[i].i]=getsum(1,query[i].l,query[i].r,0,32767);
	}
	
	for(int i=0;i<Q;i++)
		printf("%d\n",solution[i]);
	return 0;
}
