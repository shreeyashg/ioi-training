/*
 * Exit Status
 * 0 : All fine
 * 1 : Exceeds queries
 * 2 : Library user made an error
 * 8+0+0+0 = Error while opening file
 * 8+0+0+1 = Input format error
 * 8+0+2+0 = Some other input error
 * 8+4+0+0 = Error in opening output or log file
 * 8+4+2+0 = Some other error durig write
 */

//Originally written by Kshitij and Prateek in 2007 in C \cap C++

#include <cstdio>
#include <cstdlib>

#define FOR(i, N) for(i = 0; i < N; ++i )

#define INFILE stdin
#define OUTFILE stdout
//#define LOGFILE "wonder.log"

//typedef enum { False, True } Boolean;


static const int MAXN=2012;

static const int MAXQ = 50000;

// Main variable
static const int N = 2007;
static int Q = 0;
static int G[MAXN][MAXN]; // -1 : not fixed yet;
static bool used[MAXN] = {false};

// To ensure that the user is not useless 
static bool initDone = false;
static const int TOO_MANY_QUERIES = 1;
static const int USER_ERROR = 2;
static const int INPUT_FORMAT_ERROR = 9;

static FILE *fin, *fout/*,  *flog */;

int init()
{
  int i, j;
/*   if( initDone ) fprintf(flog, "init() called again.\n"); */
  
  if( initDone ) {
    fprintf(stderr, "init(): init() has already been called.\n");
    exit(USER_ERROR);
  }
  fin = stdin;
  fout = stdout;
  int seed;
  fscanf(fin, "%d", &seed);
  srand(seed);
  for(int i=0; i<N; ++i)
	 for(int j=0; j<N; ++j)
		 G[i][j] = -1;
  initDone=true;
  return N;
}

bool prefer(int i, int j) /* prefer may be changed */
{
  ++Q;
  
  if(!initDone) {
    fprintf(stderr, "prefer(%d, %d): init() hasn't been called yet.\n", i, j);
    exit(USER_ERROR);
  }
  
  if(i<1 || i>N || j<1 || j>N) {
    fprintf(stderr, "prefer(%d, %d): [Query #%d] Wonder not in range 1..%d.\n", i, j, Q, N);  /* prefer may be changed */
    exit(USER_ERROR);
  }
  
  if( i == j ) {
    fprintf(stderr, "prefer(%d, %d): [Query #%d] Both wonders should not be the same.\n", i, j, Q); /* prefer may be changed */
    exit(USER_ERROR);
  }
  
  if( Q > MAXQ ) {
    fprintf(stderr, "prefer(%d, %d): [Query #%d] You have exceeded the maximum number of queries allowed , %d.\n", i, j, Q, MAXQ);
    exit(TOO_MANY_QUERIES);
  }
  --i;
  --j;
  if(G[i][j] != -1)
	  return G[i][j];
  for(int z=0; z<N; ++z)
  {
	  if(z != i && z != j && G[i][z] == 1 && G[z][j] == 1)
	  {
		  G[i][j] = 0;
		  break;
	  }
	  else if (z != i && z != j && G[i][z] == 0 && G[z][j] == 0)
	  {
		  G[i][j] = 1;
		  break;
	  }
  }
  if (G[i][j] == -1)
  	 G[i][j] = (rand()>>16) %2;
  G[j][i] = 1 - G[i][j];

   return G[i][j];
}

int solution(const int *A)
{
  int i;
  
/*   fprintf(flog, "solution called with the following array:\n[");  fflush(flog); */
/*   FOR(i, N) fprintf(flog, " % 5d%s", A[i], i == N-1 ? "]\n" : (i%10 == 9 ? ",\n " : ", ") );  fflush(flog); */
  
  if(!initDone) {
    fprintf(stderr, "solution(...): init() hasn't been called yet.\n"); /* change solution */
    exit(USER_ERROR);
  }
  
  FOR(i, N) used[i] = false;
  FOR(i, N)
    if( A[i] < 1 || A[i] > N ) {
      fprintf(fout, "WRONG!\n");
      fprintf(fout, "%d is not in range 1..%d.\n", A[i], N);
      exit(0);
    }
    else if(used[A[i]-1]) {
      fprintf(fout, "WRONG!\n");
      fprintf(fout, "%d occurs more than once.\n", A[i]);
      exit(0);
    }
    else
      used[A[i]-1] = true;
  
  FOR(i, N-1)
    if(G[A[i]-1][A[i+1]-1] != 1) {
      fprintf(fout, "WRONG!\n");
      fprintf(fout, "%d in not preferred to %d, as reported by your program.\n", A[i], A[i+1]);
      exit(0);
    }
  fprintf(stderr, "queries used = %d\n", Q);
  fprintf(fout, "CORRECT!\n");
  
  exit(0);
}
