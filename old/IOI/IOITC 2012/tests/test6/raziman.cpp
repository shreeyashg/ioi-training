/*

Raziman T V

Final 1 : Question 2
Wonders of the World

Complexity : O ( N^2 )
Makes O ( N * Log(N) ) calls

*/

///////////////////////////////////////
//HEADERS

# include "wonderlib.h"

///////////////////////////////////////
//GLOBAL ARRAYS

int rank[2012];

///////////////////////////////////////
//MAIN FUNCTION

int main()
{
	int N=init();
	rank[0]=1;
	int start,end;
	for(int i=2;i<=N;i++)
	{
		bool pref;
		start=0;
		end=i-2;
		while(start<=end)
		{
			int mid=(start+end)/2;
			pref=prefer(i,rank[mid]);
			if(pref)
				end=mid-1;
			else
				start=mid+1;
		}
		for(int j=i-1;j>end+1;j--)
			rank[j]=rank[j-1];
		rank[end+1]=i;
	}
	solution(rank);
	return 0;
}

///////////////////////////////////////
