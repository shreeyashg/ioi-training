/*
 * Exit Status
 * 0 : All fine
 * 1 : Exceeds queries
 * 2 : Library user made an error
 * 8+0+0+0 = Error while opening file
 * 8+0+0+1 = Input format error
 * 8+0+2+0 = Some other input error
 * 8+4+0+0 = Error in opening output or log file
 * 8+4+2+0 = Some other error durig write
 */

//Originally written by Kshitij and Prateek in 2007 in C \cap C++

#include <cstdio>
#include <cstdlib>

#define FOR(i, N) for(i = 0; i < N; ++i )

#define INFILE stdin
#define OUTFILE stdout
//#define LOGFILE "wonder.log"

//typedef enum { False, True } Boolean;


static const int MAXN=2012;

static const int MAXQ = 50000;

// Main variable
static int N;
static int Q = 0;
static bool G[MAXN][MAXN] = {{false}};
static bool used[MAXN] = {false};

// To ensure that the user is not useless 
static bool initDone = false;
static const int TOO_MANY_QUERIES = 1;
static const int USER_ERROR = 2;
static const int INPUT_FORMAT_ERROR = 9;

static FILE *fin, *fout/*,  *flog */;

int init()
{
  int i, j;
/*   if( initDone ) fprintf(flog, "init() called again.\n"); */
  
  if( initDone ) {
    fprintf(stderr, "init(): init() has already been called.\n");
    exit(USER_ERROR);
  }
  initDone=true;
  
/*   if(!(flog = fopen( LOGFILE, "w"))) { */
/*     fprintf(stderr, "init(): Error opening log file for writing.\n"); */
/*     exit(8+4); */
/*   } */
/*   fprintf(flog, "Log file created.\n"); fflush(flog); */
  
  // Using standard input/output
  /*if(!(fin = fopen( INFILE, "r" ))) {
    fprintf(stderr, "init(): Error opening input file for reading.\n");
    exit(8+0);
  }
  
  if(!(fout = fopen( OUTFILE, "w"))) {
    fprintf(stderr, "init(): Error opening output file for writing.\n");
    exit(8+4);
  }*/
  
  fin = stdin;
  fout = stdout;
  if( fscanf(fin, "%d", &N) == EOF ) {
    fprintf(stderr, "init(): Input file not formatted correctly (Trying to read N).\n");
    exit(INPUT_FORMAT_ERROR);
  }
  
  if(N>MAXN || N<1) {
    fprintf(stderr, "N = %d. N is not in the range [1...%d]. ", N, MAXN);
    exit(USER_ERROR);
  }
  
  FOR(i, N) FOR(j, N) {
    int tmp;
    if( fscanf(fin, "%d", &tmp) == EOF) {
      fprintf(stderr, "init(): Input file not formatted correctly (Trying to read the matrix's (%d,%d)th element).\n", i+1, j+1);
      exit(INPUT_FORMAT_ERROR);
    }
    
    if(tmp != 0 && tmp != 1) {
      fprintf(stderr, "init(): Matrix's (%d,%d)th element is neither 0 nor 1.\n", i+1, j+1);
      exit(INPUT_FORMAT_ERROR);
    }
    
    if( i>j && tmp==G[j][i] ) {
      fprintf(stderr, "init(): Matrix's (i,j)th and (j,i)th value can't be the same. (%d,%d)th and (%d,%d)th element are both %d\n ",j+1,i+1,i+1,j+1,tmp);
      exit(INPUT_FORMAT_ERROR);
    }
    
    G[i][j] = tmp;
  }

/*   fprintf(flog, "init(): Return value %d.\n", N); fflush(flog); */
  return N;
}

bool prefer(int i, int j) /* prefer may be changed */
{
  ++Q;
  
  if(!initDone) {
    fprintf(stderr, "prefer(%d, %d): init() hasn't been called yet.\n", i, j);
    exit(USER_ERROR);
  }
  
  if(i<1 || i>N || j<1 || j>N) {
    fprintf(stderr, "prefer(%d, %d): [Query #%d] Wonder not in range 1..%d.\n", i, j, Q, N);  /* prefer may be changed */
    exit(USER_ERROR);
  }
  
  if( i == j ) {
    fprintf(stderr, "prefer(%d, %d): [Query #%d] Both wonders should not be the same.\n", i, j, Q); /* prefer may be changed */
    exit(USER_ERROR);
  }
  
  if( Q > MAXQ ) {
    fprintf(stderr, "prefer(%d, %d): [Query #%d] You have exceeded the maximum number of queries allowed , %d.\n", i, j, Q, MAXQ);
    exit(TOO_MANY_QUERIES);
  }
  
/*   fprintf(flog, "prefer(%d, %d): [Query #%d] Return value %d.\n", i, j, Q, G[i-1][j-1]); fflush(flog); */
  return G[i-1][j-1];
}

int solution(const int *A)
{
  int i;
  
/*   fprintf(flog, "solution called with the following array:\n[");  fflush(flog); */
/*   FOR(i, N) fprintf(flog, " % 5d%s", A[i], i == N-1 ? "]\n" : (i%10 == 9 ? ",\n " : ", ") );  fflush(flog); */
  
  if(!initDone) {
    fprintf(stderr, "solution(...): init() hasn't been called yet.\n"); /* change solution */
    exit(USER_ERROR);
  }
  
  FOR(i, N) used[i] = false;
  FOR(i, N)
    if( A[i] < 1 || A[i] > N ) {
      fprintf(fout, "WRONG!\n");
      fprintf(fout, "%d is not in range 1..%d.\n", A[i], N);
      exit(0);
    }
    else if(used[A[i]-1]) {
      fprintf(fout, "WRONG!\n");
      fprintf(fout, "%d occurs more than once.\n", A[i]);
      exit(0);
    }
    else
      used[A[i]-1] = true;
  
  FOR(i, N-1)
    if(!G[A[i]-1][A[i+1]-1]) {
      fprintf(fout, "WRONG!\n");
      fprintf(fout, "%d in not preferred to %d, as reported by your program.\n", A[i], A[i+1]);
      exit(0);
    }
  fprintf(stderr, "queries used = %d\n", Q);
  fprintf(fout, "CORRECT!\n");
  
  exit(0);
}
