int init();
/* Initializes, and returns N.  Should be called at the beginning of
 * your program.
 */

bool prefer(int i, int j);
/* Return true if i is preferred over j 
 */

void solution(const int *a);
/* Checks if the solution is correct or wrong and exits the program
 * gracefully.
 */
