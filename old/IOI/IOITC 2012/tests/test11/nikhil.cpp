#include<iostream>
#include<cstdio>
#include<cassert>
#include<algorithm>
#include<cstring>
#include<vector>
#include<cmath>

#define square(x) x*x
#define maxSteps 16000
#define maxDiff  200
#define maxRefine 10

using namespace std;

int N,M;
int maxRefineSteps=6500;

int delays[200][200];
double sortedDelays[200][200];
int importance[200];
int indices[200];
double sorters[200];
int times[200][200];
int tempTimes[200];
long long perform[200];


bool mysort(const int a, const int b)
{
	double comp=sorters[a]-sorters[b];
	if(comp) return comp<0;
	return importance[a]<importance[b];
}

void initLocalSearch()
{
	long long total=0;
	int time=0;
	memset(times,0,sizeof(times));
	
	for(int l=0;l<N;l++)
	{
		int index=indices[l];
		int completionTime=0;
		int *array=&delays[index][0];
		
		for(int k=0;k<M;k++,array++)
		{
			time=times[l][k]=(*array)+(l>0?times[l-1][k]:0);
			if(time>completionTime)
				completionTime=time;
		}
		perform[l]=(long long)completionTime*(long long)importance[index];
	}
}


bool localSearch(int start,int end)
{
	long long total=0,time=0,prevtotal=0;
	
	for(int k=0;k<M;k++)
		tempTimes[k]= (start>0 ? times[start-1][k]:0);
	
	
	for(int l=start;l<=end;l++)
	{
		int index=indices[l];
		int completionTime=0;
		int *array=&delays[index][0];
		
		for(int k=0;k<M;k++,array++)
		{
			time=tempTimes[k]=tempTimes[k]+(*array);
			if(time>completionTime)
				completionTime=time;
		}
		total+=(long long)completionTime*(long long)importance[index];
		prevtotal+=perform[l];
	}
	
	if(prevtotal<=total) return false;
	if(start==0)
	{
		int index=indices[0];
		int *array=&delays[index][0];
		int *timeArray=&times[0][0];
		int completionTime=0;
		for(int k=0;k<M;k++,array++)
		{
			time=times[0][k]=*array;
			if(time>completionTime)
				completionTime=time;
		}
		perform[0]=(long long)completionTime*(long long)importance[index];
		start++;
	}
	for(int l=start;l<=end;l++)
	{
		int index=indices[l];
		int *array=&delays[index][0];
		int *timeArray=&times[l][0];
		int *prevTime=timeArray-200;
		int completionTime=0;
		
		for(int k=0;k<M;k++,array++,timeArray++,prevTime++)
		{
			time=(*timeArray)=(*array)+ (*prevTime);
			if(time>completionTime)
				completionTime=time;
		}
		perform[l]=(long long)completionTime*(long long)importance[index];
	}
	return true;
}




int main()
{
	int T,temp;
	T = 1;
	while(T--)
	{
		scanf("%d%d",&N,&M);
		
		for(int i=0;i<N;i++)
		{
			indices[i]=i;
			scanf("%d",&importance[i]);
			int maxDelay=0;
			for(int j=0;j<M;j++)
			{
				scanf("%d",&delays[i][j]);
				maxDelay=max(maxDelay,delays[i][j]);
			}
			sorters[i]=(double)maxDelay/importance[i];
		}
		
		
		sort(indices,indices+N,mysort);
		
		initLocalSearch();

		int count=0;
		while(count<=2)
		{
			
			int steps=0;
			for(int i=1;i<N && steps<=maxRefineSteps;i++)
			{
				for(int j=0,k=i;k<N && steps<=maxRefineSteps;j++,k++,steps++)
				{

					temp=indices[k];
					indices[k]=indices[j];
					indices[j]=temp;


					if(localSearch(j,k))
					{
						if(j>=i) { j-=i-1;k-=i-1; i=1;}
					}
					else
					{
						temp=indices[k];
						indices[k]=indices[j];
						indices[j]=temp;
					}
				}
			}
			count++;
		}

		
		
		
		for(int i=0;i<M;i++)
		{
			for(int j=0;j<N;j++)
				printf("%d ",indices[j]+1);
			printf("\n");
		}
	}
}



