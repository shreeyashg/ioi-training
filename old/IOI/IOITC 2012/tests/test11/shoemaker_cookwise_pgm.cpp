/*
 	Team Proof
	IIT Delhi
 
	C++ Template
 */


#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <cassert>
#include <string>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <stack>
#include <queue>
#include <cstdlib>
using namespace std;

#define s(T) scanf("%d", &T)
#define sl(T) scanf("%lld", &T)
#define fill(a, val) memset(a, val, sizeof(a))
#define mp make_pair

int totalCases, testNum;
int N, M;

vector <int> shoeWt;
vector <vector <int> > timeTaken;

vector <int> bestperm;
long long bestTime;
vector <int> maxTime;

int control;

bool comp(int i, int j)
{
	int wti = timeTaken[i][control] * shoeWt[j];
	int wtj = timeTaken[j][control] * shoeWt[i];
	return wti < wtj
	|| (wti == wtj && i < j);						 
}

bool compmax(int i, int j)
{
	int wti = maxTime[i] * shoeWt[j];
	int wtj = maxTime[j] * shoeWt[i];
	return wti < wtj || (wti == wtj && i < j);
}

void preprocess()
{
	
}

bool input()
{
	shoeWt.clear();
	timeTaken.clear();
	
	s(N);
	s(M);
	shoeWt.resize(N);
	timeTaken.resize(N);
	for(int i = 0; i < N; i++)
	{
		timeTaken[i].resize(M);
		s(shoeWt[i]);
		for(int j = 0;j < M; j++)
			s(timeTaken[i][j]);
	}
	return true;
}

long long simulate(const vector <int> &curperm)
{
	vector <long long> finishtimes(M);
	long long ret = 0;
	for(int i = 0; i < N; i++)
	{
		int curshoe = curperm[i];
		for(int j = 0; j < M; j++)
			finishtimes[j] += timeTaken[curshoe][j];
		long long curfinish = 0;
		for(int j = 0; j < M; j++)
			curfinish = max(curfinish, finishtimes[j]);
		ret += curfinish * shoeWt[curshoe];
	}
	return ret;
}

void solve()
{
	bestTime = (long long)1e13;
	vector <int> curperm;
	for(int i = 0; i < N; i++) curperm.push_back(i);
	
	for(control = 0; control < M; control++)
	{
		sort(curperm.begin(), curperm.end(), comp);
		long long cons = simulate(curperm);
		if(cons < bestTime)
		{
			bestTime = cons;
			bestperm = curperm;
		}
	}
	
	maxTime.resize(N, 0);
	for(int i = 0; i < N; i++)
		for(int j = 0; j < M; j++)
			maxTime[i] = max(maxTime[i], timeTaken[i][j]);
	
	sort(curperm.begin(), curperm.end(), compmax);
	long long cons = simulate(curperm);
	if(cons < bestTime)
	{
		bestTime = cons;
		bestperm = curperm;
	}
	
	for(int i = 0; i < M; i++)
	{
		for(int j = 0; j < N; j++)
			printf("%d ", bestperm[j]+1);
		printf("\n");
	}
}

int main()
{
	preprocess();
	totalCases = 1;// s(totalCases);
	//s(totalCases);
	for(testNum = 1; testNum <= totalCases; testNum++)
	{
		if( !input())
			break;
		solve();
	}
}
