#include <stdio.h>
#include <stdlib.h>

struct point {
	int x;
	int y;
};

struct window {
	struct point topleft;
	struct point botright;
};

int nr_rows, nr_columns, nr_win_rows, nr_win_columns, nr_cells;
#define MAXLEN 10000000
struct point obs_cells[MAXLEN];

/*
 *   9| 1 |5
 *  ---------
 *   8| 0 |4
 *  --------- 
 *  10| 2 |6
 *
 *  encoding x:y
 *  Left = 1000 = 8
 *  Right= 0100 = 4
 *  Top  = 0001 = 1
 *  Bot =  0010 = 2
 *  Stay = 0000 = 0
 *  XCOR = 1100 = 12
 *  YCOR = 0011 =3
 */

#define STAY 0
#define TOP 1
#define BOT 2
#define YCOR 3
#define RIGHT 4
#define TOPRIGHT 5
#define BOTRIGHT 6
#define LEFT 8
#define TOPLEFT 9
#define BOTLEFT 10
#define XCOR 12

int get_direction(struct window w,  struct point p)
{
     int ret = 0;

     if (p.x < w.topleft.x) 
	     ret = ret | LEFT;
     else if (p.x > w.botright.x)
	     ret = ret | RIGHT;
     else 
	     ret = ret | STAY;

     if (p.y < w.topleft.y)
	     ret = ret | TOP;
     else if (p.y > w.botright.y)
	     ret = ret | BOT;
     else
	     ret = ret | STAY;

     return ret;
}

struct point getshifts(struct window window, struct point point, int dir)
{
	struct point retpoint;

	int xshift = 0, yshift = 0;
	
	if ((dir & XCOR) == LEFT)
		xshift = point.x - window.topleft.x;
	else if ((dir & XCOR) == RIGHT)
		xshift = point.x - window.botright.x;
	else 
		xshift = 0;

	if ((dir & YCOR) == TOP)
		yshift = point.y - window.topleft.y;
	else if ((dir & YCOR) == BOT)
		yshift = point.y - window.botright.y;
	else 
		yshift = 0;

	retpoint.x = xshift;
	retpoint.y = yshift;

	return retpoint;
}

struct window move(struct window w, int xshift, int yshift)
{
	struct window retwindow;

	retwindow.topleft.x = w.topleft.x + xshift;
	retwindow.botright.x = w.botright.x + xshift;
	retwindow.topleft.y = w.topleft.y + yshift;
	retwindow.botright.y = w.botright.y + yshift;
  
	return retwindow;
}

int main() 
{
  struct window scope;
  struct point shiftpoint;
  int dir;
  int i;
  long long int count = 0;

  scanf("%d%d", &nr_rows, &nr_columns);
  scanf("%d%d", &nr_win_rows, &nr_win_columns);
  scanf("%d", &nr_cells);

  for (i = 0; i < nr_cells; i++) {
	  scanf("%d%d", &(obs_cells[i].x), &(obs_cells[i].y));
  }

/*  
  for (i = 0; i < nr_cells; i++) {
	  printf("(%d,%d)\n", obs_cells[i].x, obs_cells[i].y);
  }
*/

  scope.topleft.x = 0;
  scope.topleft.y = 0;
  scope.botright.x = nr_win_rows - 1;
  scope.botright.y = nr_win_columns - 1;

  for (i = 0; i < nr_cells; i++) {
	dir = get_direction(scope, obs_cells[i]);
	shiftpoint = getshifts(scope, obs_cells[i], dir);
	count += abs(shiftpoint.x) + abs(shiftpoint.y);
	scope = move(scope, shiftpoint.x, shiftpoint.y);
  }
  printf("%lld\n", count);

  return 0;
}
