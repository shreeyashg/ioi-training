#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef pair<ll, ll> pii;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18;

ll m, n, r, c, k;
pii br, tl;
vector<pii> v;



ll upd(pii x)
{
	ll ans = 0;
	if(x.first >= tl.first && x.first <= br.first)
	{
		if(x.second >= tl.second && x.second <= br.second)
			return 0;
		if(x.second < tl.second)
		{
			ans += abs(tl.second - x.second);
			tl.second = x.second;
			br.second = x.second + r;
		}
		else if(x.second > br.second)
		{
			ans += abs(x.second - br.second);
			br.second = x.second;
			tl.second = x.second - r;
		}
		return ans;
	}
	if(x.second >= tl.second && x.second <= br.second)
	{
		if(x.first < tl.first)
		{
			ans += abs(x.first - tl.first);
			tl.first = x.first;
			br.first = x.first + c;
		}
		else if(x.first > br.first)
		{
			ans += abs(x.first - br.first);
			br.first = x.first;
			tl.first = x.first - c;
		}
		return ans;
	}

	// Fix x and then y
	if(x.first > br.first)
	{
		//cout << "####" << x.first <<" " << x.second << endl;
		ans += abs(x.first - br.first);
		br.first = x.first;
		tl.first = x.first - r;
	}
	else if(x.first < tl.first)
	{


		ans += abs(x.first - tl.first);
		tl.first = x.first;
		br.first = x.first + r;
	}

	if(x.second < tl.second)
	{

		ans += abs(x.second - tl.second);
		tl.second = x.second;
		br.second = x.second + c;
	}
	else if(x.second > br.second)
	{
		//cout << "####" << x.first <<" " << x.second << endl;
		ans += abs(br.second - x.second);
		br.second = x.second;
		tl.second = br.second - c;
	}


	return ans;
}

int main()
{	
	cin>>m>>n>>r>>c;
	r--, c--;
	cin>>k;
	br = {r, c};
	tl = {0, 0};
	REP(i, k)
	{
		ll x, y;
		cin>>x>>y;
		v.push_back({x,y});
	}

	ll ans=0;

	REP(i, v.size())
	{
		ans += upd(v[i]);
	/*	cout << ans << endl;
		cout << v[i].first << " " << v[i].second << endl;
		cout << "Current : " << tl.first << " " << tl.second << " --- " << br.first << " " << br.second << endl;
		cout << endl;  */
	}

	cout<<ans<<endl;
}