#include <cstdio>
#include <sstream>
#include <cassert>
#include <vector>
#include <utility>

using namespace std;


#include "MersenneTwister.h"


int main(int argc, char **argv)
{
	if(argc!=7)
	{
		fprintf(stderr, "Usage: %s {M} {N} {R} {C} {K} {seed/policy}\n policy 0 results in an input with the largest possible answer.\n", argv[0]);
		return 1;
	}
	istringstream is(string(argv[1]) + " " + string(argv[2]) + " " + string(argv[3])+ " " + string(argv[4]) + " " + string(argv[5]) + " " + string(argv[6]));
	int M, N, R, C, K, seed;
	is >> M >> N >> R >> C >> K >> seed;
	assert(1 <= R && R <= M);
	assert(1 <= C && C <= N);
	printf("%d %d\n%d %d\n%d\n", M, N, R, C, K);
	//cerr<<" seed "<<seed<<'\n';
	if(seed == 0)
	{
		for(int i=0; i< K/2; ++i)
			printf("%d %d\n0 0\n", M-1, N-1);
		if(K%2)
			printf("%d %d\n", M-1, N-1);
		return 0;
	}
	MTRand Rnd(seed);

	vector<pair<int, int> > p(K);
	for(int i=0; i<K; ++i)
	{
		if(i > 0 && Rnd.randInt(9) == 0) //add a point close to some previous point
		{
			pair<int, int> x = p[Rnd.randInt(i-1)];
			x.first += Rnd.randInt(2) - 1; // -1, 0, 1
			x.second += Rnd.randInt(2) - 1; // -1, 0, 1
			if(x.first < 0) x.first = 0;
			if(x.first > M-1) x.first = M-1;
			if(x.second < 0) x.second = 0;
			if(x.second > N-1) x.second = N-1;
			p[i] = x;
			printf("%d %d\n", x.first, x.second);
		}
		int x = Rnd.randInt(M-1);
		int y = Rnd.randInt(N-1);
		printf("%d %d\n", x, y);
		p[i] = make_pair(x,y);
	}
}
