/*
Solution for test 1 - Under the microscope.
Code by Prateek and Pradeep.

Input format:

[#rows] [#cols]
[#rows of window] [#cols of window]
[#cells to visit]
[several lines, each with row coordinate, column coordinate of cell]
*/

#include<cstdio>
#include<cstdlib>				// used for function abs()

using namespace std;

const int max_N=1000000;

int a1[max_N], a2[max_N];

long long solve(int N, int C, int a[], int K)
{
	//domain : [0,N)
	//window size: C
	//initial window position: [0,C)
	long long ans=0;
	int curpos=0, nxtpos;
	for(int i=0; i<K; ++i)
	{
		//first find where to go minimally
		if(a[i] < curpos) //move window left
			nxtpos = a[i];
		else if(a[i] >= curpos + C) //move window right
			nxtpos = a[i] - (C-1);
		else	//do nothing
			nxtpos = curpos;
		
		//add the distance and update the position
		ans += abs(curpos - nxtpos);
		curpos = nxtpos;
	}
	return ans;
}

int main()
{
	int M,N;
	scanf("%d %d", &M, &N);
	int R, C;
	scanf("%d %d", &R, &C);
	int K;
	scanf("%d", &K);
	for(int i=0; i<K; ++i)
		scanf("%d %d", &a1[i], &a2[i]);
	printf("%lld\n", solve(M, R, a1, K) + solve(N, C, a2, K));
}
