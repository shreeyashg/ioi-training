# include <cstdio>
# include <iostream>
# include <algorithm>
# include <vector>
# include <cstring>
# include <cctype>
# include <set>
# include <map>
# include <cmath>
# include <queue>
# include <string>
# include <cassert>

using namespace std;

int fwd[100000],bwd[100000];
int segtree[262144];
const int basestart=131072;
int N,Q;

struct two
{
	int size,index;
}queues[100000];

bool operator<(const two& t1,const two& t2)
{
	return t1.size<t2.size;
}

int findsize(int node)
{
	int ret=segtree[node];
	while(node>1)
	{
		if(node&1)ret+=segtree[node-1];
		node>>=1;
	}
	return ret;
}

int findpos(int val)
{
	if(val>segtree[1])return N;
	
	int node=1;
	while(node<basestart)
	{
		if(segtree[node<<1]>=val)node<<=1;
		else val-=segtree[node<<1],node=(node<<1)|1;
	}
	return node-basestart;
}

void inc(int node,int delta)
{
	while(node>0)
		segtree[node]+=delta,node>>=1;
}

int main()
{
	assert( scanf("%d%d",&N,&Q) == 2);
	
	for(int i=0;i<N;i++)
	{
		assert( scanf("%d",&queues[i].size) == 1);
		assert( queues[i].size >= 0 && queues[i].size <= 100000000);
		queues[i].index=i;
	}
	
	sort(queues,queues+N);
	
	segtree[basestart]=queues[0].size;
	fwd[queues[0].index]=0;
	bwd[0]=queues[0].index;
	
	for(int i=1;i<N;i++)
	{
		segtree[basestart+i]=queues[i].size-queues[i-1].size;
		fwd[queues[i].index]=i;
		bwd[i]=queues[i].index;
	}
	
	for(int i=basestart-1;i>0;i--)
		segtree[i]=segtree[i<<1]+segtree[(i<<1)|1];
	
	for(int q=0;q<Q;q++)
	{
		int qtype;
		assert( scanf("%d",&qtype) == 1);
		assert( qtype >= 1 && qtype <= 3);
		
		if(qtype==1)
		{
			int qnum;
			assert( scanf("%d",&qnum) == 1);
			assert( qnum >= 0 && qnum < N);
			
			int qsize=findsize(fwd[qnum]+basestart);
			int changepos=findpos(qsize+1)-1;
			
			if(changepos!=fwd[qnum])
			{
				int u=fwd[qnum],v=changepos;
				int a=qnum,b=bwd[changepos];
				
				fwd[a]=v;fwd[b]=u;
				bwd[u]=b;bwd[v]=a;
			}
			
			inc(changepos+basestart,1);
			if(changepos<N-1)inc(changepos+basestart+1,-1);
		}
		else if(qtype==2)
		{
			int qsize;
			assert( scanf("%d",&qsize) == 1);
			assert( qsize >= 0 && qsize <= 1000000000);
			printf("%d\n",N-findpos(qsize));
		}
		else if(qtype==3)
		{
			int qsize;
			assert( scanf("%d",&qsize) == 1);
			assert( qsize >= 1 && qsize <= 1000000000);

			int changepos=findpos(qsize);
			if(changepos<N)inc(changepos+basestart,-1);
		}
	}
	return 0;
}
