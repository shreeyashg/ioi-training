import java.io.*;
import java.util.*;

class TestGen_pgm
{
	Random r = new Random();
	BufferedReader keyin= new BufferedReader(new InputStreamReader(System.in));
	
	int N, Q;
	int [] arr;
	int [] pivots;
	int parts;
	int type;
	
	void exec() throws IOException
	{
		arr = new int[100005];
		pivots = new int[10005];
		type = Integer.parseInt(keyin.readLine());
		if(type == 0)
			generateTiny();
		else if(type == 1)
			generateSmall();
		else 
			generateLarge();
		
	}
	
	public static void main(String [] args) throws IOException
	{
		new TestGen_pgm().exec();
	}
	
	void generateTiny() throws IOException
	{
		N = 1000; Q = 5000;
		parts = Integer.parseInt(keyin.readLine());
		generateGeneral();
	}
	
	void generateSmall() throws IOException
	{
		N = 100000; Q = 500000;
		System.out.println(N + " " + Q);
		for(int i = 0; i < N; i++)
			System.out.print(String.format("0%c", (i == N-1? '\n': ' ')));
		
		for(int i = 0; i < Q; i++)
		{
			int c, d = 1;
			c = r.nextInt(2) + 1;
			if(c == 1)
				d = r.nextInt(N);
			else if(r.nextInt(5) < 4) 
				d = r.nextInt(Math.max(1, i/2+1));
				else 
				d = r.nextInt(1000000001);
			System.out.println(c + " " + d);
		}
	}
	
	void generateLarge() throws IOException
	{
		N = 100000; Q = 500000;
		parts = Integer.parseInt(keyin.readLine());
		generateGeneral();
	}
	
	void generateGeneral()
	{
		System.out.println(N + " " + Q);
		for(int i = 0; i < N/parts; i++)
			pivots[i] = r.nextInt(100000000);
		
		for(int i = 0; i < N; i++)
		{
		//	arr[i] = pivots[i%(N/parts)] + r.nextInt(parts);
			arr[i] = i % parts;
			System.out.print(String.format("%d%c", arr[i], (i == N-1? '\n': ' ')));
		}
		
		for(int i = 0; i < Q; i++)
		{
			int c, d=1;
			c = r.nextInt(3) + 1;		
			switch (c) {
				case 1:
					d = r.nextInt(N);
					arr[d]++;
					break;
				case 2:
					//d = r.nextInt(2) * 1000000000;				//0 or 10^9 inputs
					//d = r.nextInt(parts);
					d = Math.max(0, arr[r.nextInt(N)]-1 + r.nextInt(3));
					break;
				case 3:
					//d = Math.max(1, arr[r.nextInt(N)]-1 + r.nextInt(3));
					d = 1+ r.nextInt(parts);
					if(type == 1)
					for(int j = 0; j < N; j++)
						if(arr[j] >= d)
							arr[j]--;
					break;
			}
			System.out.println(c + " " + d);
		}
		
	}
}
