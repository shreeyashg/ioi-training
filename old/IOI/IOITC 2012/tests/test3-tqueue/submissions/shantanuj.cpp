#define intl long long;
#include <cstdio>
#include <vector>

using namespace std;

int N, Q;
vector<int> reg;

void fi(int j) {

	reg[j]++;
}

void fx(int j) {
	
	int ans = 0;
	for(int i = 0; i < N; i++)
		if(reg[i] >= j)
			ans++;
	printf("%d\n", ans);
}

void fy(int j) {
	
	int ans = 0;
	for(int i = 0; i < N; i++)
		if(reg[i] >= j)
			reg[i]--;
}

int main() {
	
	scanf("%d %d", &N, &Q);
	reg.resize(N);
	for(int i = 0; i < N; i++)
		scanf("%d", &reg[i]);
		
	for(int i = 0; i < Q; i++) {
		int a, b;
		scanf("%d %d", &a, &b);
		if(a == 1)
			fi(b);
		else if(a == 2)
			fx(b);
		else if(a == 3)
			fy(b);
	}
		
}
