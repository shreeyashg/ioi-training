#include <algorithm>
#include <iostream>
#include <cmath>
using namespace std;

struct dragonLocation {
	int x, y;
}dragon[301];

long long dist(int i, int j) {
	return((long long)(abs(dragon[i].x-dragon[j].x)+abs(dragon[i].y-dragon[j].y)));
}

bool row(dragonLocation a, dragonLocation b) {
	if(a.y < b.y)
		return true;
	else
		return false;
}

int main() {
	int r, c, k, d;
	cin >> r >> c >> k >> d;
	dragon[0].x=0;
	dragon[0].y=0;
	for(int i=1;i<=d;++i)
		cin >> dragon[i].y >> dragon[i].x;
	
	sort(dragon+1, dragon+d+1, row);
	
	long long killed[d+1][k+1];
	for(int i=1;i<=d;++i)
		killed[i][1]=dist(0,i);
	
	for(int j=2;j<=k;++j) {
		for(int i=j;i<=d;++i) {
			killed[i][j] = killed[j-1][j-1]+dist(j-1,i);
			for(int l=j;l<=i-1;++l) {
				long long curr = killed[l][j-1]+dist(l,i);
				if(killed[i][j]>curr)
					killed[i][j]=curr;
			}
		}
	}
	
	long long best=killed[k][k];
	for(int i=k+1;i<=d;i++)
		if(best>killed[i][k])
			best=killed[i][k];
	
	cout << best;
	
	return 0;
}
