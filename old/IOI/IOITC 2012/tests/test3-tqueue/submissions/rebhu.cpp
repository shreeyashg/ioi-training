#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#include<stack>
#include<set>
#define gi(a) gar=scanf("%d",&a)
#define gl(a) gar=scanf("%lld",&a)
#define For(i,lb,ub) for(int i=lb;i<ub;i++)
#define Rev(i,ub,lb) for(int i=ub;i>lb;i--)
#define pi(a) printf("%d ",a)
#define mod(a) ((a<0)?-a:a)
#define lli long long int
using namespace std;
int gar; //garbage value returned by scanf
int main()
{
  int N,Q;
  gi(N);gi(Q);
  if(N<=1000 && Q<=5000) //subtask 1
  {
    vector<lli> arr(N);
    For(i,0,N)
      gl(arr[i]);
    
    For(i,0,Q)
    {
      int code,e,count=0;
      lli ins;
      gi(code);
      switch(code)
      {
	case 1: gi(e);
		arr[e]++;
		break;
		
	case 2: gl(ins);
		count=0;
		For(j,0,N){
		  if(arr[j] >= ins)
		    count++;	  
		}
		printf("%d\n",count);
		break;
		
	case 3: gl(ins);
		For(j,0,N){
		  if(arr[j] >= ins)
		    arr[j]--;	  
		}
		break;
		
	default: cout<<"Illegal Entry!";
		  
      }
    }
  }
  else
  {//subtask 2 & 3
    vector<lli> arr(N);
    multiset<lli> arr2;
    For(i,0,N){
      gl(arr[i]);
      arr2.insert(arr[i]);
    }  
    For(i,0,Q)
    {
      multiset<lli>::iterator iar;
      int code,e;
      lli ins;
      gi(code);
      switch(code)
      {
	case 1: gi(e);
		arr[e]++;
		iar=arr2.find(arr[e]-1);
		arr2.erase(iar);
		arr2.insert(arr[e]);
		break;
		
	case 2: gl(ins);
		iar=arr2.lower_bound(ins);
		printf("%d\n",distance(iar,arr2.end()));
		break;
		
	/*case 3: gl(ins);
		For(j,0,N){
		  if(arr[j] >= ins)
		    arr[j]--;	  
		}
		break;*/
		
	default: cout<<"Illegal Entry!";
		 break; 
		  
      }
    }
  }
  return 0;
}
