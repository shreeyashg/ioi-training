#include<cstdlib>
#include<cstdio>

using namespace std;

#define FOR(i, a, b) for(i=a;i<=b;i++)
#define maxn 100005


int olp[maxn], nep[maxn], n, q;
long long int a[maxn], sora[maxn];

int f2(int p, int q, long long int x);
void f3(long long int y);
void quicksort(int a, int b);
void swap(int x, int y);

int main(){

	int i, t1, temp;
	long long int t2;
	
	scanf("%d %d", &n, &q);
	
	FOR(i, 0, n-1){
		scanf("%lld", &a[i]);
		sora[i] = a[i];
	}
	
	FOR(i, 0, n-1){
		olp[i] = nep[i] = i;
	}
	
	quicksort(0, n-1);
	
	FOR(i, 0, q-1){
		scanf("%d %lld", &t1, &t2);
		
		if(t1 == 1){
			a[t2]++;
			sora[nep[t2]]++;
		}
		
		else if(t1 == 2){
			temp = f2(0, n-1, t2);
			printf("%d\n", n - temp);
		}
		
		else f3(t2);
	}
	
	return 0;

}

void quicksort(int a, int b){

	int sep, i;
	
	if(b <= a) return;
	
	sep = a;
	
	FOR(i, a+1, b){
	
		if(sora[i] <= sora[a]){
			swap(sep + 1, i);
			sep++;			
		}
	
	}
	
	swap(sep, a);
	
	quicksort(a, sep-1);
	quicksort(sep+1, b);

}

void swap(int x, int y){

int t = sora[x];
sora[x] = sora[y];
sora[y] = t;

int ox = olp[x];
int oy = olp[y];

t = olp[x];
olp[x] = olp[y];
olp[y] = t;

nep[ox] = y;
nep[oy] = x;

}

int f2(int p, int q, long long int x){

	if(p == q){
		if(sora[p] >= x) return p;
		else return n;
	}
	
	if(q == p+1){
		if(sora[q] >= x){
			if(sora[p] >= x) return p;
			else return q;
		}
		else return n;
	}
	
	int mid = (p+q)/2;
	
	if(sora[mid] >= x){
		return f2(p, mid, x);
	}
	else return f2(mid+1, q, x);

}

void f3(long long int y){

	int i;
	int t = f2(0, n-1, y);
	
	FOR(i, t, n-1){
	
		sora[i]--;
		a[olp[i]]--;
	
	}

}
