#include<iostream>
#include<cstdio>

using namespace std;

long long arr[100000];
long long sortarray[100000];
int n;

int quicksort(int l, int r)
{
  if(r - l <= 1)
    return 0;

  int b = l + 1;
  for(int i = l + 1; i < r; i++)
    {
      if(sortarray[i] <= sortarray[l])
	{
	  swap(sortarray[i], sortarray[b]);
	  b++;
	}
    }
  swap(sortarray[l], sortarray[b - 1]);

  quicksort(l, b - 1);
  quicksort(b , r);
}

void f1(int index)
{
  //update arr val, binary search in sortarr, update, check to right and move
  //log n (n?), nlog n overall
  arr[index]++;
  int right = n - 1, left = 0, temp, pos, pos1;
  while(right >= left)
    {
      temp = (right + left) / 2;
      if(sortarray[temp] == arr[index] - 1)
	{
	  pos = temp;
	  break;
	}
      else if(sortarray[temp] < arr[index] - 1)
	left = temp;
      else
	right = temp;
    }
  pos1 = pos;
  sortarray[pos]++;
  for(int i = pos; sortarray[i] == arr[index] - 1 && i < n; i++)
    pos1 = i;
  swap(sortarray[pos1], sortarray[pos]);
}

void f2(int val)
{
  if(val > sortarray[n - 1])
    {
      printf("0\n");
      return;
    }
  if(val < sortarray[0])
    {
      printf("%d\n", n);
      return;
    }
  //binary search for val, find the pos. print n - pos
  //log n (n?), overall nlog n
  int right = n - 1, left = 0, temp, pos, pos1;
  while(right >= left)
    {
      temp = (right + left) / 2;
      if(right == left + 1)
	{
	  if(sortarray[left] >= val)
	    pos = left;
	  else
	    pos = right;
	  break;
	}

      if(sortarray[temp] == val || right == left)
	{
	  pos = temp;
	  break;
	}
      else if(sortarray[temp] < val)
	left = temp;
      else
	right = temp;
    }
  pos1 = pos;
  if(sortarray[pos] == val)
    for(int i = pos; i >= 0 && sortarray[i] == val; i--)
      pos1 = i;

  printf("%d\n", n - pos1);
}

void f3(int val)
{
  //n^2, qn^2 overall! qnlogn at least
  int right = n - 1, left = 0, temp, pos, pos1;
  while(right >= left)
    {
      temp = (right + left) / 2;
      if(right == left + 1)
	{
	  if(sortarray[left] >= val)
	    pos = left;
	  else
	    pos = right;
	  break;
	}

      if(sortarray[temp] == val || right == left)
	{
	  pos = temp;
	  break;
	}
      else if(sortarray[temp] < val)
	left = temp;
      else
	right = temp;
    }
  pos1 = pos;
  if(sortarray[pos] == val)
    for(int i = pos; i >= 0 && sortarray[i] == val; i--)
      pos1 = i;

  for(int i = pos1; i < n; i++)
    sortarray[i]--;

  for(int i = 0; i < n; i++)
    if(arr[i] >= val)
      arr[i]--;

  quicksort(0, n); //almost n^2 since it's nearly sorted
  //binary search for val, decrement all later ones by 1
}

int main()
{
  int q, type, input; scanf("%d %d", &n, &q);
  for(int i = 0; i < n; i++)
    {
      scanf("%lld", &arr[i]);
      sortarray[i] = arr[i];
    }
  quicksort(0, n);
  for(int i = 0; i < q; i++)
    {
      scanf("%d %d", &type, &input);
      switch(type)
	{
	case 1: f1(input); break;
	case 2: f2(input); break;
	case 3: f3(input); break;
	}
    }
}
