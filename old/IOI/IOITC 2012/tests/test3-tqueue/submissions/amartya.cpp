#include<iostream>
#include<cstdio>
#include<algorithm>
#include<vector>
#include<set>
#include<queue>
#include<cmath>
#include<cassert>

using namespace std;

#define FOR(i,a,b) for(long long i=(a);i<(b);++i)
#define MAX (1<<18)
#define INF 1000000000
#define S(x) scanf("%d", &x)

int N, Q, arr[MAX], a[MAX];

int main(){
  
  int t, q, h;
  
  cin>>N>>Q;
  FOR(i,0,N)
    S(arr[i]);
  FOR(i,0,N)
    a[i] = arr[i];
  sort(arr, arr+N);
  FOR(i,0,Q){
    S(t);	S(h);
    if(t == 1){
      q = a[h];
      int t1 = 0, t2 = N;
      while(t2-t1 > 1){
	t = (t1+t2)/2;
	if(arr[t] <= q)
	  t1 = t;
	if(arr[t] > q)
	  t2 = t;
      }
      assert(arr[t1] == q);
      ++arr[t1];
      ++a[h];
    }
    if(t == 2){
      int t1 = 0, t2 = N;
      while(t2-t1 > 1){
	t = (t1+t2)/2;
	if(arr[t] < h)
	  t1 = t;
	if(arr[t] >= h)
	  t2 = t;
      }
      cout<<N-t2<<endl;
    }
  }

  return 0;
  
}
//262144 131072