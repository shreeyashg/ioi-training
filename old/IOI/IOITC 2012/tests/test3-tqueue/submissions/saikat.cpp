#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
struct reg_d
{
	int a,ref;
};
reg_d s_r[100000];

bool operator<(const reg_d& r1,const reg_d& r2)
{
	return r1.a<r2.a;
}

void incr(int d,int n1)
{
	int p;
	for(p=0;p<n1;p++)
		if(s_r[p].ref==d)
		{
			s_r[p].a=s_r[p].a+1;
			sort(s_r, s_r+n1);
			break;
		}
}

int b_search(int beg,int end,int ele)
{
	int mid,k;
	mid=(beg+end)/2;
	if(s_r[mid].a>ele)
		k=b_search(beg,mid-1,ele);
	else if(s_r[mid].a<ele)
		k=b_search(mid+1,end,ele);
	else
		k=mid;
	return k;
}

void n_i(int d,int n)
{
	int i;
	i=b_search(0,n,d);
	cout<<(n-i+1)<<"\n";
}

void decr(int ele,int end)
{
	int e;
	e=b_search(0,end,ele);
	for(;e<=end;e++)
	s_r[e].a=s_r[e].a-1;
	sort(s_r ,s_r+(end+1));
}
			
int main()
{
	int n,q,c[500000],d[500000],i,j;
	cin>>n>>q;
	for(i=0;i<n;i++)
	{
		cin>>s_r[i].a;
		s_r[i].ref=i;
	}
	sort(s_r ,s_r+n);
	for(j=0;j<q;j++)
	{
		cin>>c[j]>>d[j];
	}
	for(j=0;j<q;j++)
	{
		if(c[j]==1)
			incr(d[j],n);
		else if(c[j]==2)
			n_i(d[j],n-1);
		else
			decr(d[j],n-1);
	}
}
