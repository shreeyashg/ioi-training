#include <iostream>
#include <algorithm>

using namespace std;

int n, q;

int nodes[100000];

void op1(int i)
{
	++nodes[i];
}

long long op2(int i, int f = 0, int ch = 0)
{
	int x = 0;
	for(int j = 0; j < n; ++j)
		if(nodes[j] >= i)
			++x;
	return x;
}

void op3(int i)
{
	int x = 0;
	for(int j = 0; j < n; ++j)
		if(nodes[j] >= i)
			--nodes[j];
}

int main(int, char**)
{
	cin >> n >> q;
	for(int i = 0; i < n; ++i)
	{
		cin >> nodes[i];
	}
	for(int i = 0; i < q; ++i)
	{
		int x;
		cin >> x;
		if(x == 1)
		{
			cin >> x;
			op1(x);
		}
		else if(x == 2)
		{
			cin >> x;
			cout << op2(x) << endl;
		}
		else
		{
			cin >> x;
			op3(x);
		}
	}
	return 0;
}
