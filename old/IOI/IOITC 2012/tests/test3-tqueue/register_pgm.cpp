
#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <cassert>
using namespace std;

#define s(T) scanf("%d", &T)

/** In this problem, I am using 1-based indexing since it is more compatible with BITs. Essentially there is a y++ queries of the form 1 y */


const int max_n = 100005;

long long BIT[max_n];

void incr(int pos, long long val)
{
	while(pos < max_n)
	{
		BIT[pos] += val;
		pos += pos & (-pos);
	}
}

long long query(int pos)
{
	long long sum = 0;
	while(pos > 0)
	{
		sum += BIT[pos];
		pos -= pos & (-pos);
	}
	return sum;
}


int ind(long long val)			//finds first pos in BIT where query(pos) >= val
{
	int lo = 0, hi = max_n-1, mid;
	while(lo < hi)
	{
		int mid = (lo + hi)/2;
		if(query(mid) >= val)
			hi = mid;
		else 
			lo = mid+1;
	}
	return lo;
}

int arr[max_n];
int fwd[max_n];
int bwd[max_n];
int N, Q;
pair <int, int> vals[max_n];

void input()
{
	s(N); s(Q);
	assert(N <= 100000 && N >= 1 && Q <= 500000 && Q >= 1);
	for(int i= 1; i <= N; i++)
	{	
		s(arr[i]);
		assert(arr[i] >= 0 && arr[i] <= 100000000);
	}
}

void solve()
{
	for(int i = 1; i <= N; i++)
		vals[i] = make_pair(arr[i], i);
	sort(vals+1, vals+1 + N);
	for(int i = 1; i <= N; i++)
		bwd[i] = vals[i].second;
	for(int i = 1; i <= N; i++)
		fwd[bwd[i]] = i;
	
	for(int i = 0; i < max_n; i++)
		BIT[i] = 0;
	for(int i = 1, prev = 0; i <= N; i++)
	{
		incr(i, vals[i].first-prev);
		prev = vals[i].first;
	}
	incr(N+1, 1000000000);
		
	for(int i = 0; i < Q; i++)
	{
		int x, y;
		s(x); s(y);
		assert(x >= 1 && x <= 3);
		int val, idx, u, v;
		switch (x) {
			case 1:
				assert(y < N && y >= 0);
				y++;
				val = query(fwd[y]) + 1;
				idx = ind(val);
				u = bwd[idx-1], v = fwd[y];
				swap(fwd[y], fwd[u]);
				swap(bwd[idx-1], bwd[v]);

				incr(idx, -1);
				incr(idx-1, 1);
				break;
			case 2:
				assert(y >= 0 && y <= 1000000000);
				if(y == 0)
					printf("%d\n", N);
				else
				{
					idx = ind(y);
					printf("%d\n", N-idx+1);
				}
				break;
			case 3:
				assert(y >= 1 && y <= 1000000000);
				idx = ind(y);
				incr(idx, -1);
				break;
		}
	}
	
}

int main()
{
	input();
	solve();
}
