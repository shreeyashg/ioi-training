The test 3 Register Report was definitely the hardest task so far. The first submission was after a whole forty five minutes, but it did correctly handle the subtask1. Surprisingly, there were a number of WAs on subtask 1 too. I suppose a lot of people were trying to kill three birds with one stone and missing all 3.

Since it had such few solutions, I will probably try to explain the solution rather than just a having a commentary on the code.

Subtask 1 having O(QN) complexity for brute force could pass. The kind of errors today again varied a lot, good luck debugging … errors in binary searches, errors in using the wrong index etc. Also, "no submission" for this task makes no sense. Its not as if the brute force was particularly hard to code!

Subtask 2 since you have no decrementing operations, is much simpler than the others. Once a register has a value at least k, it will always have a value at least k. I noticed a correct solution in the middle of the contest timing out. The solution was even O(N + Q). How is this possible? He was using cin, cout. Shifting to scant and printf would have saved him quite some debugging time.
Another solution was to somehow have a multi-set (set which allows for duplicates) and then doing a logN time search to find how many elements are there >= x (the query). One person used this approach using stl <multiset>. Can also be done using BIT/SegTrees.

Subtask 3. This one was the bad one. And the worst thing about it was that you needed to perform both individual random-access updates as well as seemingly arbitrary "range" updates.

[SPOILER ALERT: For an (somewhat) intuitive understanding of the problem's solution, continue. For a what-to-do understanding, look up "solution", and then work out why and how it works].

If you look at the elements in sorted order, most of the problem would get solved: you will (with a little effort of maintaining pointers) be able to tell which element to increase, be able to find the position of the element after which the "number" of elements would be known, and also be able to find out from what element onwards would you need to modify the values.

The downside of this is, for the 3rd kind of update, you would have to update O(N) elements. This can be severely severely costly. Can you do it in O(1), once you've found out at what "position" in the sorted array you need to make the change from?
And so, instead of storing the original array, store the differences of consecutive elements in the (sorted) array. Now, the i'th element becomes the prefix sum of the first i elements. So with a few careful changes - required for handling rearranging of elements - type1 query can be accomplished (indeed, type1 query was the most tricky of the 3 after this differences-reduction). Type 2 query is binary searching on the element that has prefix sum at least x. Type 3 query is after finding the position where the prefix sum is at least y, decrement the value of the "differences" array by 1.

Since you need dynamic updation and querying of a prefix-sum array, use a BIT / segment tree.

Time Complexity: O(logN ^ 2) per query. Can also be done in O(logN) if you're particular about the extra logN complexity (like Raziman).

It was interesting to see everyone start discussing so animatedly after the contest. There's nothing like a good post-exam brainstorming session to help spot yours and others' tricks and flaws.

Good luck!

Pradeep.