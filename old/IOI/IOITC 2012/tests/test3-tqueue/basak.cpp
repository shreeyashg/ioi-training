#include<vector>
#include<stack>
#include<set>
#include<map>
#include<queue>
#include<deque>
#include<string>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<cassert>
#include<cstdlib>
#include<cstdio>
#include<cmath>
#include<string>

using namespace std;

#define s(n)					scanf("%d",&n)
#define sl(n) 					scanf("%lld",&n)
#define sf(n) 					scanf("%lf",&n)
#define ss(n) 					scanf("%s",n)
#define INF						(int)1e9
#define LINF					(long long)1e18
#define EPS						1e-9
#define maX(a,b)				((a)>(b)?(a):(b))
#define miN(a,b)				((a)<(b)?(a):(b))
#define abS(x)					((x)<0?-(x):(x))
#define FOR(i,a,b)				for(int i=a;i<b;i++)
#define REP(i,n)				FOR(i,0,n)
#define foreach(v,c)            for( typeof((c).begin()) v = (c).begin();  v != (c).end(); ++v)
#define mp						make_pair
#define FF						first
#define SS						second
#define tri(a,b,c)				mp(a,mp(b,c))
#define XX						first
#define YY						second.first
#define ZZ						second.second
#define pb						push_back
#define fill(a,v) 				memset(a,v,sizeof a)
#define all(x)					x.begin(),x.end()
#define SZ(v)					((int)(v.size()))
#define DREP(a)					sort(all(a)); a.erase(unique(all(a)),a.end())
#define INDEX(arr,ind)			(lower_bound(all(arr),ind)-arr.begin())
#define debug(args...)			{dbg,args; cerr<<endl;}
#define dline					cerr<<endl	

void sc(char &c){
	char temp[4];	ss(temp);	
	c=temp[0];
}

struct debugger
{
	template<typename T> debugger& operator , (const T& v)
	{	
		cerr<<v<<" ";	
		return *this;	
	}
} dbg;

void debugarr(int * arr,int n)
{
	cout<<"[";
	for(int i=0;i<n;i++)
		cout<<arr[i]<<" ";
	cout<<"]"<<endl;
}





typedef long long LL;
typedef pair<int,int> PII;
typedef pair<LL,LL> PLL;
typedef pair<int,PII> TRI;

typedef vector<int> VI;
typedef vector<LL> VL;
typedef vector<PII> VII;
typedef vector<PLL> VLL;
typedef vector<TRI> VT;

typedef vector<VI> VVI;
typedef vector<VL> VVL;
typedef vector<VII> VVII;
typedef vector<VLL> VVLL;
typedef vector<VT> VVT;


/*Main code begins now */

int testnum;

const int BASE = (1<<17);
int stree[BASE+BASE];
int N,Q;
int fwd[BASE];
int bkd[BASE];

void inc(int u,int val)
{
	for(u+=BASE ; u; u>>=1)
		stree[u] += val;
}

int getPos(int val)
{
	int nval=val;
	int u=1;
	while(u<BASE)
	{
		u *= 2;
		if(val > stree[u])
			val -= stree[u++];
	}
	return u-BASE;
}
			
int sum(int ind)
{
	ind+=BASE;
	int sum=stree[ind];
	for(; ind; ind>>=1)
		if(ind&1) sum+=stree[ind-1];
	return sum;
}


void op1(int ind)
{
	int pos = fwd[ind];
	int val = sum(pos);
	int posend = getPos(val+1)-1;
	if(posend != pos)
	{
		int indend = bkd[posend];
		swap(fwd[ind],fwd[indend]);
		swap(bkd[pos],bkd[posend]);
	}
	inc(posend,+1);
	inc(posend+1,-1);
}

void op2(int val)
{
	int pos = getPos(val);
	printf("%d\n",N-pos+(val ? 1 : 0));
}

void op3(int val)
{
	int pos = getPos(val);
	inc(pos,-1);
}


int main()
{
	s(N); s(Q);
	VII nums;
	for(int i=0;i<N;i++)
	{
		int z; s(z);
		nums.pb(mp(z,i));
	}
	nums.pb(mp(0,-1));
	nums.pb(mp(2000000000,N));
	sort(all(nums));
	
	for(int i=1;i<=N+1;i++)
	{
		inc(i,nums[i].FF-nums[i-1].FF);
		fwd[nums[i].SS]=i;
		bkd[i]=nums[i].SS;
	}
	
	while(Q--)
	{
		int mode,val;
		s(mode); s(val);
		if(mode==1) op1(val);
		if(mode==2) op2(val);
		if(mode==3) op3(val);
	}
}

