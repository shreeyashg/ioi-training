#include<iostream>
#include<cmath>
#include<algorithm>
#include<string>
#include<string.h>
#include<vector>
#include<set>
#include<map>
#include<cstdio>
#include<queue>
#include<sstream>
#include<list>
#include<bitset>
#include<ctime>
using namespace std;
 
typedef long long Int;
#define FOR(i,a,b) for(int i=(a); i<=(b);++i)
#define mp make_pair
#define pb push_back
#define sz(s) (int)((s).size())
const int inf = 1000000000;
const int MOD = 1000000007;
const double pi=acos(-1.0);

struct item{
	int cnt, priority, key;
	item*left, *right;
	item(int _key) {
		cnt=1;
		priority=rand();
		key=_key;
		left=NULL;
		right=NULL;
	}
};
typedef item* pitem;
int getcnt(pitem t) {
	return t ? t->cnt : 0;
}
void upd_cnt(pitem t) {
	if(t) t->cnt = getcnt(t->left)+getcnt(t->right)+1;
}
 
void merge(pitem &t, pitem t1, pitem t2) {
	if(!t1 || !t2) {
		t=(t1 ? t1 : t2);
		return;
	}
	if(t1->priority > t2->priority)
		merge(t1->right, t1->right, t2), t=t1;else
		merge(t2->left, t1, t2->left), t=t2;
 
	upd_cnt(t);
}
 
void split(pitem t, pitem &t1, pitem &t2, int cnt) {
	if(!t) {
		t1=t2=NULL;
		upd_cnt(t1);
		upd_cnt(t2);
		return;
	}
	int C = getcnt(t->left);
	if(cnt<=C) split(t->left, t1, t->left, cnt), t2=t;else
		split(t->right, t->right, t2, cnt-1-C), t1=t;
 
	upd_cnt(t);
	//upd_cnt(t2);
}
 
 
void print(pitem t) {
	if(!t) return;
	print(t->left);
	printf("%c",t->key);
	print(t->right);
}
 
bool used[1000009];
int ans[1000009];
 
int main() {
	//freopen("input.txt","r",stdin);freopen("output.txt","w",stdout);
	int n,m;
	cin>>n>>m;
	pitem t = NULL;
	FOR(i,1,n) {
		pitem temp = new item(i);
		merge(t, t, temp);
	}

	memset(ans, -1, sizeof(ans));

	FOR(i,1,m) {
		int x,y;
		scanf("%d %d",&x,&y);
		pitem t1, t2, t3;
		split(t, t1, t2, y-1);
		split(t2, t2, t3, 1);
		if(ans[t2->key]!=-1 && ans[t2->key]!=x) {
			cout<<"-1"<<endl;
			return 0;
		}
		if(used[x] && ans[t2->key]!=x) {
			cout<<"-1"<<endl;
			return 0;
		}
		ans[t2->key]=x;
		merge(t, t2, t1);
		merge(t, t, t3);
		used[x]=true;
	}
	vector<int> na;
	FOR(i,1,n) if(!used[i]) na.pb(i);
	int pos=0;
	FOR(i,1,n) if(ans[i]>0) printf("%d ",ans[i]);else
		printf("%d ",na[pos++]);
	cout<<endl;
} 
