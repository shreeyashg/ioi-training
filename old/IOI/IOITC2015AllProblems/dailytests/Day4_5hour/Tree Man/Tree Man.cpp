#include <bits/stdc++.h>
#define rf freopen("in.in", "r", stdin);
#define wf freopen("out.out", "w", stdout);
#define rep(i, s, n) for(int i=int(s); i<=int(n); ++i)
using namespace std;
const int mx = 2e5+10, m = 1e9+7;

static int n, ch[mx], chm[mx], ans[mx], par[mx];
static int pr1[mx], pr2[mx], sf1[mx], sf2[mx];
vector<int> g[mx];

void dfs(int u, int l)
{
	ch[u] = 1;
	int  t1 = 0, t2 = 0;
	
	rep(i, 0, g[u].size()-1)
	{
		int v = g[u][i];
		if(v==par[u]) continue;
		
		dfs(v, l+1), ch[u] += ch[v], chm[u] = (chm[u]+chm[v])%m;
		pr1[v] = t1, pr2[v] = t2;
		
		int tmp = (1ll*t2*ch[v])%m;
		t1 = (t1+tmp)%m;
		t2 = (t2+ch[v])%m;
	}
	chm[u] = (chm[u]+t1)%m;
	
	t1 = 0, t2 = 0;
	for(int i = g[u].size()-1; i+1; --i)
	{
		int v = g[u][i];
		if(v==par[u]) continue;
		
		sf1[v] = t1, sf2[v] = t2;
		
		int tmp = (1ll*t2*ch[v])%m;
		t1 = (t1+tmp)%m;
		t2 = (t2+ch[v])%m;
	}
	
	rep(i, 0, g[u].size()-1)
	{
		int v = g[u][i];
		if(v==par[u]) continue;
		
		int tmp = (1ll*pr2[v]*sf2[v])%m;
		tmp = (tmp+pr1[v]+sf1[v])%m;
		tmp = (1ll*chm[v]*tmp)%m;
		tmp = (1ll*tmp*l)%m;
		
		ans[u] = (ans[u]+tmp)%m;
	}
}

int solve(vector <int> parent)
{
	n = parent.size();
	rep(i, 0, n-1)
	{
		int u = parent[i], v = i+1;
		g[u].push_back(v);
	}
	rep(i, 0, n-1)
		g[i+1].push_back(parent[i]), par[i+1]=parent[i];
	
	dfs(0, 0);
	
	int ret = 0;
	rep(i, 1, n)
		ret = (ret+ans[i])%m;
	return ret;
}

int main()
{
	rf;// wf;

	int sz, in;
	vector<int> parent;
	
	scanf("%d", &sz);
	rep(i, 1, sz)
		scanf("%d", &in), parent.push_back(in);
	printf("%d\n", solve(parent));
		
	return 0;
}