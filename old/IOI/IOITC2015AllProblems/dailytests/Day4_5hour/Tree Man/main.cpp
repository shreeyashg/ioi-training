#include <map>
#include <set>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cctype>
#include <cstdio>
#include <vector>
#include <cassert>
#include <complex>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;
#define int long long

const int mod = 1e9 + 7;
const int N = 2e5 + 23;
vector <int> child[N];
int s[N], subfc[N], ans;

inline void add(int& a, int b) { a += b; if (a >= mod) a -= mod; }

void dfs(int u, int dep)
{
	s[u] = 1;
	for (int v : child[u])
	{
		dfs(v, dep + 1);
		add(s[u], s[v]);
		add(subfc[u], subfc[v]);
	}

	int tot = s[u] - 1;
	int sum = tot * tot;
	for (int v : child[u])
		sum -= s[v] * s[v];
	sum /= 2;
	sum %= mod;

	add(subfc[u], sum);

	for (int v : child[u])
	{
		int cur = subfc[v];
		cur = cur * (sum - s[v] * (tot - s[v]) % mod) % mod;;
		if (cur < 0) cur += mod;
		add(ans, cur * dep % mod);
	}
}

#undef int
int main()
{
#define int long long
	int n; cin >> n;
	n++;
	for (int i = 1; i < n; i++)
	{
		int x; cin >> x;
		if (x >= 0) child[x].push_back(i);
	}
	dfs(0, 0);
	cout << ans << "\n";
	return 0;
}
