#include <bits/stdc++.h>
#include "testlib.h"
#define SZ(x) ((int)(x).size())
#define FOR(it, c) for(__typeof((c).begin()) it = (c).begin(); it != (c).end(); ++it)
#define pb push_back
#define F first
#define S second
#define MAXN 200000
#define mp make_pair
using namespace std;
typedef long long LL;
int p[MAXN+100];
int par[MAXN+100];
int height;
void pre()
{
    for(int i=0; i<MAXN+100; i++)
        p[i]=i+1;
}
vector <  int > G[MAXN+10];
int vis[MAXN+10];
int tin[MAXN+10];
int tim=0;
bool dfs(int node, int p, vector < pair < int , int > >& ar)
{
    tin[node]=tim;
    tim++;
    if(p!=-1)
        ar.push_back(make_pair(tin[p],tin[node]));
    int q=G[node].size();
    vis[node]=1;
    for(int i=0; i<q; i++)
        if(G[node][i]!=p)
        {
            if(vis[G[node][i]])return false;
            if(dfs(G[node][i],node,ar)==false)return false;
        }
    return true;
}
int dfs1(int node, int p)
{
    int q=G[node].size(),ret=1;
    for(int i=0; i<q; i++)
        if(G[node][i]!=p)
            ret += dfs1(G[node][i],node);
    return ret;
}
void bfs(int n)
{
    memset(vis,0,sizeof(vis));
    queue < int > myq;
    myq.push(1);
    vis[1]=0;
    while(not myq.empty())
    {
        int p=myq.front();
        myq.pop();
        for(int i=0; i<G[p].size(); i++)
            if(vis[G[p][i]]==0)
                myq.push(G[p][i]),vis[G[p][i]]=vis[p]+1;
    }
}
int check(vector < pair < int , int > > ar, vector < pair < int , int > >& ret)
{
    int n=ar.size()+1;
    for(int i=0; i<=n; i++)
        G[i].clear();
    memset(vis,0,sizeof(vis));
    for(int i=0; i<n-1; i++)
    {
        G[ar[i].F].pb(ar[i].S);
        G[ar[i].S].pb(ar[i].F);
        if(ar[i].F==ar[i].S)return false;
    }
    memset(vis,0,sizeof(vis));
    bfs(n);
    dfs1(1,-1);
    memset(vis,0,sizeof(vis));
    tim=1;
    if(dfs(1,-1,ret))return 1;
    else return -1;
}
vector<pair<int, int> > treeexpand(int n , int d) {
    int t = 2;
    vector<pair<int, int> > E;
    for(int i=1;i<=n;i++) {
        int prob = rnd.next(1,p[d-1]), nchild = 0;
        nchild=prob;
        for(int j = 0; j < nchild && t <= n; j++, t++)
            E.push_back(make_pair(i, t));
    }
    vector < pair < int, int > > ret;
    int p=check(E,ret);
    if (SZ(E) < n-1 || p==-1)return treeexpand(n,d);
    return ret;
}

vector<pair<int, int> > treeparents(int n) {
    vector<pair<int, int> > E;
    for(int i=2;i<=n;i++) {
        int p = rnd.next(1,i-1);
        E.push_back(make_pair(i, p));
    }
    vector < pair < int, int > > ret;
    int p=check(E,ret);
    if (SZ(E) < n-1 || p==-1)return treeparents(n);
    return ret;
}

vector<pair<int, int> > treechain(int n, int chain) {
    memset(vis,0,sizeof(vis));
    vector<pair<int, int> > E;
    vector < int  > choose;
    choose.resize(chain+1);
    for(int i=1; i<=chain; i++)
        choose[i]=(i);
    for(int i=chain+1; i<=n; i++)
    {
        int j=rnd.next(1,i);
        if(j<=chain)choose[j]=i;
    }
    shuffle(choose.begin()+1,choose.end());
    for(int i=1; i<=chain; i++)
    {
        vis[choose[i]]=1;
        if(i!=1)E.pb(make_pair(choose[i],choose[i-1]));
    }
    for(int i=1; i<=n; i++)
        if(vis[i]==0)E.pb(make_pair(choose[(rnd.next(1,chain))],i));

    vector < pair < int, int > > ret;
    int p=check(E,ret);
    if (SZ(E) < n-1 || p==-1)return treechain(n,chain);
    return ret;
}
vector<pair<int, int> > singlechain(int n){
    memset(vis,0,sizeof(vis));
    vector<pair<int, int> > E;
    for(int i=1; i<n; i++)
        E.push_back(make_pair(i,i+1));
    vector < pair < int, int > > ret;
    int p=check(E,ret);
    return ret;
}
vector<pair<int, int> > btree(int n){
    memset(vis,0,sizeof(vis));
    vector<pair<int, int> > E;
    for(int i=1; i<=n; i++){
        if(2*i<=n)
            E.pb(make_pair(i,2*i));
        if(2*i+1<=n)
            E.pb(make_pair(i,2*i+1));
    }
    vector < pair < int, int > > ret;
    int p=check(E,ret);
    return ret;
}
int ftst=0;
void single(int n,int flag){
    char name[11];
    sprintf(name,"input%d.in",ftst);
    ftst++;
    freopen(name,"wb",stdout);
    FILE* out=fopen(name,"wb");
    vector< pair<int,int> > tr;
    if(flag==0)tr=singlechain(n);
    else if(flag==1)tr=btree(n);
    else if(flag==2)tr=treeparents(n);
    printf("%d\n",n);
    memset(par,0,sizeof(par));
    for(int i=0; i<n-1; i++)
        par[tr[i].second]=tr[i].first;
    for(int i=1; i<=n; i++){
        printf("%d",par[i]);
        if(i==n)printf("\n");
        else printf(" ");
    }
}
void single1(int n, int flag, int factor){
    char name[11];
    sprintf(name,"input%d.in",ftst);
    ftst++;
    freopen(name,"wb",stdout);
    FILE* out=fopen(name,"wb");
    vector< pair<int,int> > tr;
    if(flag==0)tr=treechain(n,factor);
    else if(flag==1)tr=treeexpand(n,factor);
    printf("%d\n",n);
    memset(par,0,sizeof(par));
    for(int i=0; i<n-1; i++)
        par[tr[i].second]=tr[i].first;
    for(int i=1; i<=n; i++){
        printf("%d",par[i]);
        if(i==n)printf("\n");
        else printf(" ");
    }
}
void single2(int n){
    char name[11];
    sprintf(name,"input%d.in",ftst);
    ftst++;
    freopen(name,"wb",stdout);
    FILE* out=fopen(name,"wb");
    for(int i=0; i<n/2; i++)
        printf("%d ",i);
    for(int i=n/2; i<n; i++)
        printf("%d ",n/2);
}
//legend
//void single(int n, int flag) flag=0 : singlechain, flag=1:btree, flag=2: treeparents
//void single1(int n, int flag, int factor) flag=0: treechain(n,factor), flag=1: treeexpand(n,factor)
int main(void) {
    pre();
    printf("input00.txt to input06.txt are subtask1\n");
    printf("input07.txt to input17.txt are subtask2\n");
    //input00.txt to input06.txt are subtask1
    single(rnd.next(1,10000),1);
    single(rnd.next(1,10000),2);
    single(10000,1);
    single(10000,0);
    single(10000,0);
    single(10000,1);
    single(10000,2);
    single2(10000);

    //input07.txt to input17.txt are subtask2
    single(rnd.next(MAXN-MAXN/100,MAXN),1);
    single(rnd.next(MAXN-MAXN/10,MAXN),2);
    single(MAXN,1);
    single(MAXN,0);
    single1(MAXN,0,MAXN/10);
    single1(MAXN,1,30);
    single1(MAXN,1,2);
    single1(MAXN,1,4);
    single1(MAXN,1,10);
    single1(MAXN,1,20);
    single2(MAXN);
    return 0;
}
