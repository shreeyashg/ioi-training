#include<bits/stdc++.h>
#define max(a,b) (a>b ? a : b)
#define min(a,b) (a<b ? a : b)
#define LLD long long int
#define PB push_back
#define PII pair<int,int>
#define MAXN1 100
#define MAXN2 1000
#define MAXN3 10000
#define MAXQ 100
using namespace std;


//========================================================================================

// ######### Test Generation ###########

int rndA = 168726318;
int rndB = 768978978;
void gen0()  // small: random
{
	srand(rndA*0+rndB);
	freopen("input0.in","w",stdout);

    int n,q;
	n = MAXN1;
	q = MAXQ;
	cout<<n<<endl;
	string s;
	for(int i=0;i<n;i++)
	    s += (char)('a'+rand()%26);
	cout<<s<<endl;
	cout<<q<<endl;
	for(int i=1;i<=q;i++)
	    cout<<i<<" ";
	cout<<endl;
}
void gen1()  // small: multiple contiguos occurences
{
	srand(rndA*1+rndB);
	freopen("input1.in","w",stdout);

    int n,q;
	n = MAXN1;
	q = MAXQ;
	cout<<n<<endl;
	string s;
	s += 'a';
	for(int i=1;i<n;i++)
    {
        if(rand()%4)
            s += s[i-1];
        else
	        s += (char)('a'+rand()%26);
    }
	cout<<s<<endl;
	cout<<q<<endl;
	for(int i=1;i<=q;i++)
	    cout<<i<<" ";
	cout<<endl;
}
void gen2()  // medium: random
{
	srand(rndA*2+rndB);
	freopen("input2.in","w",stdout);

    int n,q;
	n = MAXN2;
	q = 0;
	cout<<n<<endl;
	string s;
	for(int i=0;i<n;i++)
	    s += (char)('a'+rand()%26);
	cout<<s<<endl;
	vector<int> Q;
	for(int i=1;i<=n;i++) {
	    if(rand()%10==0 && q!=MAXQ)
        {
            Q.PB(i);
            q++;
        }
    }
    cout<<q<<endl;
    for(int i=0;i<q;i++)
        cout<<Q[i]<<" ";
	cout<<endl;
}
void gen3()  // medium: contiguous
{
	srand(rndA*3+rndB);
	freopen("input3.in","w",stdout);

    int n,q;
	n = MAXN2;
	q = 0;
	cout<<n<<endl;
	string s = "a";
	for(int i=1;i<n;i++)
    {
        if(rand()%4)
            s += s[i-1];
        else
	        s += (char)('a'+rand()%26);
    }
	cout<<s<<endl;
	vector<int> Q;
	for(int i=1;i<=n;i++) {
	    if(rand()%10==0 && q!=MAXQ)
        {
            Q.PB(i);
            q++;
        }
    }
    cout<<q<<endl;
    for(int i=0;i<q;i++)
        cout<<Q[i]<<" ";
	cout<<endl;
}
void gen4()  // medium: same
{
	srand(rndA*4+rndB);
	freopen("input4.in","w",stdout);

    int n,q;
	n = MAXN2;
	q = 0;
	cout<<n<<endl;
	string s;
	for(int i=0;i<n;i++)
        s+='g';
	cout<<s<<endl;
	vector<int> Q;
	for(int i=1;i<=n;i++) {
	    if(rand()%10==0 && q!=MAXQ)
        {
            Q.PB(i);
            q++;
        }
    }
    cout<<q<<endl;
    for(int i=0;i<q;i++)
        cout<<Q[i]<<" ";
	cout<<endl;
}
void gen5()  // large: random
{
	srand(rndA*5+rndB);
	freopen("input5.in","w",stdout);

    int n,q;
	n = MAXN3;
	q = 0;
	cout<<n<<endl;
	string s;
	for(int i=0;i<n;i++)
	    s += (char)('a'+rand()%26);
	cout<<s<<endl;
	vector<int> Q;
	for(int i=1;i<=n;i++) {
	    if(rand()%50==0 && q!=MAXQ)
        {
            Q.PB(i);
            q++;
        }
    }
    cout<<q<<endl;
    for(int i=0;i<q;i++)
        cout<<Q[i]<<" ";
	cout<<endl;
}
void gen6()  // large: contiguous
{
	srand(rndA*6+rndB);
	freopen("input6.in","w",stdout);

    int n,q;
	n = MAXN3;
	q = 0;
	cout<<n<<endl;
	string s = "a";
	for(int i=1;i<n;i++)
    {
        if(rand()%4)
            s += s[i-1];
        else
	        s += (char)('a'+rand()%26);
    }
	cout<<s<<endl;
	vector<int> Q;
	for(int i=1;i<=n;i++) {
	    if(rand()%50==0 && q!=MAXQ)
        {
            Q.PB(i);
            q++;
        }
    }
    cout<<q<<endl;
    for(int i=0;i<q;i++)
        cout<<Q[i]<<" ";
	cout<<endl;
}
void gen7()  // large: same
{
	srand(rndA*7+rndB);
	freopen("input7.in","w",stdout);

    int n,q;
	n = MAXN3;
	q = 0;
	cout<<n<<endl;
	string s;
	for(int i=0;i<n;i++)
        s+='v';
	cout<<s<<endl;
	vector<int> Q;
	for(int i=1;i<=n;i++) {
	    if(rand()%50==0 && q!=MAXQ)
        {
            Q.PB(i);
            q++;
        }
    }
    cout<<q<<endl;
    for(int i=0;i<q;i++)
        cout<<Q[i]<<" ";
	cout<<endl;
}

int main()
{
	gen0();
	gen1();
	gen2();
	gen3();
	gen4();
	gen5();
	gen6();
	gen7();
	return 0;
}
