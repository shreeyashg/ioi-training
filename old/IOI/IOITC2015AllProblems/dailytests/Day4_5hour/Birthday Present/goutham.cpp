#include <map>
#include <set>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cctype>
#include <cstdio>
#include <vector>
#include <cassert>
#include <complex>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;

const int N = 1e5 + 23;
int a[N];
string S;
int nxt[N][26];
vector <int> occ[26];

void get_nxt(int t, int n)
{
	for (int i = 0; i < 26; i++)
	{
		vector <int>& cur = occ[i];
		int sz = cur.size();
		int pre = 0;
		for (int j = t - 1; j < sz; j++)
			for (; pre <= cur[j - (t - 1)]; pre++)
				nxt[pre][i] = cur[j];
		for (; pre < n; pre++) nxt[pre][i] = n;
	}
}

int main()
{
	int n, q; cin >> n >> S >> q;
	for (int i = 0; i < n; i++) a[i] = S[i] - 'a';
	for (int i = 0; i < n; i++) occ[a[i]].push_back(i);

	while (q--)
	{
		int t; cin >> t;
		long long ans[27] = {};
		get_nxt(t, n);
		for (int p = 0; p < n; p++)
		{
			sort(nxt[p], nxt[p] + 26);
			int pre = p;
			for (int i = 0; i < 26; i++)
				ans[i] += nxt[p][i] - pre, pre = nxt[p][i];
			ans[26] += n - pre;
		}
		for (int i = 0; i <= 26; i++)
			cout << ans[i] << (i == 26 ? '\n' : ' ');
	}
	return 0;
}