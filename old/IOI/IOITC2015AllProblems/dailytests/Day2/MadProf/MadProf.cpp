#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <utility>
#include <map>
#include <queue>
#include <stack>
#include <cassert>
#include <set>
#include <climits>
#include <string>
#include <cstring>
#define rep(i, s, n) for(int i=s; i<=n; ++i)
#define rf freopen("in.in", "r", stdin)
#define ll long long
using namespace std;
 
struct node
{
	node *l, *r; ll v, f;
	node(){l=r=NULL; v=f=0;}
};
int n, q, qs, qe, ch;
ll val;
 
void up(int x, int y, node* &cur)
{
	if(y<qs || qe<x) return;
 
	if(qs<=x && y<=qe)
	{
		cur->f += val;
		ll tmp=y-x+1;
		cur->v += tmp*val;
		return;
	}
 
	int m=(x+y)>>1;
	if(qs<=m && cur->l==NULL) cur->l=new node;
	if(qe>m && cur->r==NULL) cur->r=new node;
	up(x, m, cur->l); up(m+1, y, cur->r);
 
	cur->v = (cur->l!=NULL?cur->l->v:0) + (cur->r!=NULL?cur->r->v:0);
	ll tmp=y-x+1;
	cur->v += tmp*cur->f;
}
 
ll qu(int x, int y, node* &cur, ll f)
{
	if(y<qs || qe<x) return 0;
	if(qs<=x && y<=qe)
	{
		ll tmp=y-x+1;
		return cur->v + tmp*f;
	}
 
	int m=(x+y)>>1;
	if(qs<=m && cur->l==NULL) cur->l=new node;
	if(qe>m && cur->r==NULL) cur->r=new node;
	f += cur->f;
	
	return qu(x, m, cur->l, f)+qu(m+1, y, cur->r, f);
}
 
int main()
{
	//rf;
	node* root = new node;
	scanf("%d %d", &n, &q);

	while(q--)
	{
		scanf("%d %d %d", &ch, &qs, &qe);
		if(!ch)
		{
			scanf("%lld", &val);
			up(1, n, root);
		}
		else
			printf("%lld\n", qu(1, n, root, 0));
	}
	
	return 0;
}