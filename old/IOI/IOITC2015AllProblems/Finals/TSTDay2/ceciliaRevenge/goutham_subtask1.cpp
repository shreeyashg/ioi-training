#include <map>
#include <set>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cctype>
#include <cstdio>
#include <vector>
#include <cassert>
#include <complex>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;

const int N = 123, mod = 1e9 + 7;
int f[2][N];

inline void add(int& a, int b) { a += b; if (a >= mod) a -= mod; }

int main()
{
    int q; scanf("%d", &q);
    while (q--)
    {
        int n, m; scanf("%d%d", &n, &m);
        memset(f, 0, sizeof(f));
        for (int i = 1; i <= m; i++) f[1][i] = 1;
        for (int i = 2, now = 0; i <= n; i++, now ^= 1)
            for (int j = m; j >= 1; j--)
            {
                f[now][j] = f[now][j + 1];
                add(f[now][j], f[now ^ 1][m - j]);
            }
        int ret = 0;
        for (int j = 1; j <= m; j++) add(ret, f[n & 1][j]);
        printf("%d\n", ret);
    }
    return 0;
}
