#include <map>
#include <set>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cctype>
#include <cstdio>
#include <vector>
#include <cassert>
#include <complex>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;
#define int long long

const int N = 5e4 + 23, inf = (int)1e12;
int a[N], nxt[N];
int n;
int f[1 << 8][N];
bool done[1 << 8][N];

inline bool prime(int ct)
{
	return (ct == 2 or ct == 3 or ct == 5 or ct == 7);
}

inline int fval(int a, int b)
{
	if (a == 0) return 0;
	if (b % a == 0) return 0;
	return a % (b % a);
}

inline bool adj(int i, int j)
{
	if (i > j) swap(i, j);
	if (i == j) return false;
	return j < nxt[i];
}

inline bool cont(int mid, int pos, int msk)
{
	return (msk >> (pos - mid) & 1) != 0;
}

int go(int msk, int pos)
{
	int& ret = f[msk][pos];
	if (done[msk][pos]) return ret;
	done[msk][pos] = true;

	if (pos == 7)
		for (int mid = 0; mid <= 3; mid++)
		{
			if (!cont(mid, pos, msk)) continue;
			int ct = 0;
			for (int v = 0; v <= 7; v++)
				if (cont(v, pos, msk))
					if (adj(mid, v))
						ct++;
			if (!prime(ct)) return ret = -inf;
		}

	if (pos == n - 1)
	{
		for (int mid = pos - 3; mid <= n - 1; mid++)
		{
			if (!cont(mid, pos, msk)) continue;
			int ct = 0;
			for (int v = pos - 7; v <= n - 1; v++)
				if (cont(v, pos, msk))
					if (adj(mid, v))
						ct++;
			if (!prime(ct)) return ret = -inf;
		}
		return ret = 0;
	}

	int mid = pos - 3;
	int ct = 0;
	for (int v = pos - 7; v <= pos; v++)
		if (cont(v, pos, msk))
			if (adj(mid, v))
				ct++;

	ret = -inf;
	if (!cont(mid, pos, msk) or prime(ct))
	{
		int nsk = msk;
		nsk &= (1 << 7) - 1;
		nsk <<= 1;
		ret = max(ret, go(nsk, pos + 1));
	}
	if (adj(mid, pos + 1)) ct++;
	if (!cont(mid, pos, msk) or prime(ct))
	{
		int nsk = msk;
		nsk &= (1 << 7) - 1;
		nsk <<= 1;
		nsk++;
		ret = max(ret, 1 + go(nsk, pos + 1));
	}
	return ret;
}

int brute()
{
	int ans = 0;
	for (int msk = 1; msk < 1 << n; msk++)
	{
		bool can = true;
		for (int i = 0; i < n; i++)
			if (msk >> i & 1)
			{
				int ct = 0;
				for (int j = 0; j < n; j++)
					if (msk >> j & 1)
						if (adj(i, j))
							ct++;
				if (!prime(ct))
					can = false;
				if (!can) break;
			}
		if (can) ans = max(ans, 0ll + __builtin_popcountll(msk));
	}
	printf("%lld\n", ans);
	return 0;
}

#undef int
int main()
{
#define int long long
	scanf("%lld", &n);
	for (int i = 0; i < n; i++) scanf("%lld", a + i);
	for (int i = 0; i < n; i++)
	{
		int cur = a[i];
		int j = i + 1;
		for (; j < n; j++)
		{
			cur = fval(cur, a[j]);
			if (cur == 0) break;
		}
		nxt[i] = j;
	}
	if (n <= 7) return brute();

	int ans = 0;
	for (int msk = 0; msk < 1 << 8; msk++)
	{
		int ct = 0;
		for (int i = 0; i < 8; i++)
			if (msk >> i & 1)
				ct++;
		ans = max(ans, ct + go(msk, 7));
	}
	printf("%lld\n", ans);
	return 0;
}