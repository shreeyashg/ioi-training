#include<bits/stdc++.h>
using namespace std;

#define LET(x, a)  __typeof(a) x(a)
#define TR(v, it) for(LET(it, v.begin()); it != v.end(); it++)
#define si(x) scanf("%d",&x)
#define F first
#define S second
#define PB push_back
#define MP make_pair
#define INF 1000000000
#define MOD 1000000007
#define SET(x,y) memset(x,y,sizeof(x));
#define LL long long int
#define ULL unsigned LL
#define PII pair<int, int>

#define MAXN 100004
int a[MAXN];
int old[MAXN];
int used[MAXN];
map<int, int> mp, mp2;
int n;
int main() {
    int q, i, j, k;
    int loc, nh;
    cin >> n >> q;
    for (i = 0; i < n; i++) {
        scanf("%d", &a[i]);
    }
    int cs = 1;
    map<int, int> ::iterator it;
    while (q--) {
        int l1, l2, ans = 0;
        scanf("%d %d", &l1, &l2);
        l1--;
        l2--;
        scanf("%d", &k);
        mp.clear();
        for (i = 0; i < k; i++) {
            scanf("%d %d", &loc, &nh);
            loc --;
            mp[loc] = nh;
            old[loc] = a[loc];
            a[loc] = nh;
        }
        int tp = 0;
        int cnt = 0;
        cs ++;
        used[l1] = cs;
        for (i = l1 + 1; i < n ; i++) {
            if (tp == 0 && a[i] > a[l1]) {
                l1 = i;
                tp = 1 - tp;
            } else if (tp == 1 && a[i] < a[l1]) {
                l1 = i;
                tp = 1 - tp;
            }
            used[l1] = cs;
        }
        tp = 0;
        if (used[l2] == cs) {
            used[l2] = -1;
            cnt++;
        }
        for (i = l2 + 1; i < n; i++) {
            if (tp == 0 && a[i] > a[l2]) {
                l2 = i;
                tp = 1 - tp;
            } else if (tp == 1 && a[i] < a[l2]) {
                l2 = i;
                tp = 1 - tp;
            }
            if (used[l2] == cs) {
                used[l2] = -1;
                cnt++;
            }
        }
        cout << cnt << endl;
        for (it = mp.begin(); it != mp.end(); it++) {
            a[it->first] = old[it->first];
        }
    }
    return 0;
}
