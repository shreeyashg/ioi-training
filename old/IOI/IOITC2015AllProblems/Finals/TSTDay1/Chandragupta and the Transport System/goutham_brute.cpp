#include <map>
#include <set>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cctype>
#include <cstdio>
#include <vector>
#include <cassert>
#include <complex>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;
#define int long long

const int N = 1e5 + 23;
vector <int> adj[N];
int a[N], par[N];

void dfs(int u, int p)
{
	par[u] = p;
	vector <int> :: iterator it = find(adj[u].begin(), adj[u].end(), p);
	if (it != adj[u].end()) adj[u].erase(it);
	for (int v : adj[u]) dfs(v, u);
}

void upd(int u, int x, int d)
{
	a[u] += x;
	for (int v : adj[u]) upd(v, x + d, d);
}

#undef int
int main()
{
#define int long long
	int n, q; cin >> n >> q;
	for (int i = 0; i < n - 1; i++)
	{
		int x, y; cin >> x >> y;
		adj[x].push_back(y);
		adj[y].push_back(x);
	}
	for (int i = 1; i <= n; i++) cin >> a[i];
	dfs(1, 0);
	while (q--)
	{
		int ch; cin >> ch;
		if (ch == 1)
		{
			int v, x, d;
			cin >> v >> x >> d;
			upd(v, x, d);
		}
		else
		{
			int v, ans = 0; cin >> v;
			while (v) ans += a[v], v = par[v];
			cout << ans << "\n";
		}
	}
	return 0;
}