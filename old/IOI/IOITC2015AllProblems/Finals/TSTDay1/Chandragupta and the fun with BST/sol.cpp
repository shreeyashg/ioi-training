#include<bits/stdc++.h>
#define assn(n,a,b) assert(n<=b && n>=a)
using namespace std;
#define pb push_back
#define mp make_pair
#define clr(x) x.clear()
#define sz(x) ((int)(x).size())
#define F first
#define S second
#define REP(i,a,b) for(i=a;i<b;i++)
#define rep(i,b) for(i=0;i<b;i++)
#define rep1(i,b) for(i=1;i<=b;i++)
#define pdn(n) printf("%d\n",n)
#define sl(n) scanf("%lld",&n)
#define sd(n) scanf("%d",&n)
#define pn printf("\n")
typedef pair<int,int> PII;
typedef vector<PII> VPII;
typedef vector<int> VI;
typedef vector<VI> VVI;
typedef long long LL;
#define MOD 1000000007ll
LL mpow(LL a, LL n) 
{LL ret=1;LL b=a;while(n) {if(n&1) 
    ret=(ret*b)%MOD;b=(b*b)%MOD;n>>=1;}
return (LL)ret;}
int main()
{
    map <pair<int,int> , LL > mymap;
    map <pair<int,int> , LL >::iterator itt;
    set<int> myset;
    set<int> myn;
    set<int>::iterator it;
    int n;

    mymap[mp(INT_MIN,INT_MAX)]=1;
    sd(n);
    for(int i=0; i<n; i++){
        int x,l,r;
        sd(x);
        myset.insert(x);
        it=myset.find(x);
        if(it==myset.begin())l=INT_MIN;
        else{
            it--;
            l=*it;
            it++;
        }
        it++;
        if(it==myset.end())r=INT_MAX;
        else r=*it;
        itt=mymap.find(mp(l,r));
        assert(itt!=mymap.end());
        mymap[mp(l,x)]=(2ll*itt->S)%MOD;
        mymap[mp(x,r)]=(2ll*itt->S+1ll)%MOD;
        cout << itt->S;
        if(i!=n-1)cout << " ";
        else cout << endl;
        mymap.erase(itt);
    }
    return 0;
}
