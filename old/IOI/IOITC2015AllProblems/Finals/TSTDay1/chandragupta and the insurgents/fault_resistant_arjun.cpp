#include <iostream>
#include <sstream>
#include <algorithm>
#include <vector>
#include <string>
#include <set>
#include <queue>
#include <stack>
#include <map>
#include <cstdio>
#include <cstring>
#include <cassert>
using namespace std;

#define FOR(i, n) for(int i=0;i<int(n);i++)
#define FOR1(i, n) for(int i=1;i<=int(n);i++)
#define FORA(i, a, n) for(int i=a;i<int(n);i++)
#define FORR(i, n) for(int i=n-1;i>=0;i--)
#define foreach(it, c) for(typeof(c.begin()) it = c.begin(); it != c.end(); it++)
#define all(c) c.begin(), c.end()
#define clear(c,v) memset(c,v,sizeof(c))

typedef long long int lli;
typedef pair<int,int> ii;

int n,e1,q,totale,u,v,A[1000010],c,cnt;

// interval updates, interval queries (lazy propagation)
const int SN = 1048576;  // must be a power of 2
int PassingOver[2*SN], NonZerosWithin[2*SN];


struct SegmentTree {

    // T[x] is the (properly updated) sum of indices represented by node x
    // U[x] is pending increment for _each_ node in the subtree rooted at x 

    SegmentTree() { clear(PassingOver,0);
      clear(NonZerosWithin,0); }

    // increment every index in [ia,ib) by incr 
    // the current node is x which represents the interval [a,b)
    void update(int incr, int ia, int ib, int x = 1, int a = 0, int b = SN) { // [a,b)
        ia = max(ia,a), ib = min(ib,b); // intersect [ia,ib) with [a,b)
        if(ia >= ib) return;            // [ia,ib) is empty 
        if(ia == a and ib == b) {       // We push the increment to 'pending increments'
            PassingOver[x] += incr;               // And stop recursing
            return; 
        }
        //T[x] += incr * (ib - ia);          // Update the current node
        update(incr,ia,ib,2*x,a,(a+b)/2);  // And push the increment to its children
        update(incr,ia,ib,2*x+1,(a+b)/2, b);
	if(PassingOver[2*x]!=0)
	  NonZerosWithin[x] = (a+b)/2 - a;
	else
	  NonZerosWithin[x] = NonZerosWithin[2*x];
	if(PassingOver[2*x+1]!=0)
	  NonZerosWithin[x] += (b - ((a+b)/2));
	else
	  NonZerosWithin[x] += NonZerosWithin[2*x + 1];
    }

    int query(int ia, int ib, int x = 1, int a = 0, int b = SN) {
        ia = max(ia,a), ib = min(ib,b); //  intersect [ia,ib) with [a,b)
        if(ia >= ib) return 0;          // [ia,ib) is empty 
        
        if(PassingOver[x]!=0)
	  return (ib-ia);
        if(ia == a and ib == b) 
	    return NonZerosWithin[x];
        
//         T[x] += (b - a) * U[x];           // Carry out the pending increments
//         U[2*x] += U[x], U[2*x+1] += U[x]; // Push to the childrens' pending increments
//         U[x] = 0;
	int q1,q2;
        q1 = query(ia,ib,2*x,a,(a+b)/2);
	q2 = query(ia,ib,2*x+1,(a+b)/2,b);
	return q1+q2; 
	  
    }
};

int main()
{
    ios::sync_with_stdio(false);
    SegmentTree T;

    cin>>n>>e1>>q;
    totale=e1+n-1;
    
    for(int i=1;i<=e1;i++)
    {
      cin>>u>>v;
      if(u>v)
	swap(u,v);
      T.update(1,u,v);
    }
    for(int i=1;i<=q;i++)
    {
      cin>>c>>u>>v;
      if(u>v)
	swap(u,v);
      if(c==0)
      {
	totale++;
	T.update(1,u,v);
      }
      
      if(c==1)
      {
	totale--;
	T.update(-1,u,v);
      }
      
      if(c==2)
      {
	cout<<v-u-T.query(u,v)<<"\n";
      }
    }
}
      
	




    
    
    
    
    
    
//     srand(time(0));
//     int Q = 1000000;

//     while(Q--){
//         a = (rand() % n) + 1, b = (rand() % n) + 1;
//         if(a > b) swap(a,b);
// 
//         x = rand() % (n*n);
//         printf("update : [%d,%d) +%d\n", a,b,x);
//         T.update(x,a,b);
//         FORA(i,a,b) v[i] += x;
// 
//         a = (rand() % n) + 1, b = (rand() % n) + 1;
//         if(a > b) swap(a,b);
//         printf("query : [%d,%d) \n",a,b);
// 
//         int sum = 0;
//         FORA(i,a,b) sum += v[i];
// 
//         FOR1(i,n) cout << v[i] << ' '; cout << endl;
//         cout << T.query(a,b) << ' ' << sum << endl;
// 
//         assert(T.query(a,b) == sum);
//     }
    


