#include<bits/stdc++.h>
using namespace std;

#define LET(x, a)  __typeof(a) x(a)
#define TR(v, it) for(LET(it, v.begin()); it != v.end(); it++)
#define si(x) scanf("%d",&x)
#define F first
#define S second
#define PB push_back
#define MP make_pair
#define INF 1000000000
#define MOD 1000000007
#define SET(x,y) memset(x,y,sizeof(x));
#define LL long long int
#define ULL unsigned LL
#define PII pair<int, int>

class Seg {
    static PII merge(PII det1, PII det2) {
        if (det1.first == det2.first) {
            return MP(det1.first, det1.second + det2.second);
        } else if (det1.first < det2.first) {
            return det1;
        } else {
            return det2;
        }
    }
    struct node {
        node *l, *r;
        PII min_det;
        int st, en;
        int add;
        node(int st, int en) {
            l = r = NULL;
            this->st = st;
            this->en = en;
            min_det = MP(0, en - st + 1);
            add = 0;
        }
        void recal() {
            if (!l && !r)
                return;
            int mid = (st + en) / 2;
            PII l_det = MP(0, mid - st + 1), r_det = MP(0, en - mid);
            if (l) 
                l_det = l->min_det;
            if (r) 
                r_det = r->min_det;
            min_det = merge(l_det, r_det);
        }
        void push() {
            if (add) {
                int mid = (st + en) / 2;
                if (!l) 
                    l = new node(st, mid);
                if (!r)
                    r = new node(mid + 1, en);
                r->min_det.first += add;
                l->add += add;
                l->min_det.first += add;
                r->add += add;
                add = 0;
            }
        }
    } *top;

    void update(node *&cur, int cl, int cr, int ql, int qr, int del) {
        if (cl > qr || ql > cr)
            return;
        if (!cur)
            cur = new node(cl, cr);
        if (cl >= ql && cr <= qr) {
            cur->min_det.first += del;
            cur->add += del;
            return;
        }
        int mid = (cl + cr) / 2;
        cur->push();
        update(cur->l, cl, mid, ql, qr, del);
        update(cur->r, mid + 1, cr, ql, qr, del);
        cur->recal();
    }
    PII query(node *&cur, int cl, int cr, int ql, int qr) {
        if (cl > qr || ql > cr)
            return MP(INF, 0);
        if (cur == NULL) 
            return MP(0, max(0, min(qr, cr) - max(ql, cl) + 1));
        if (cl >= ql && cr <= qr) {
            return cur->min_det;
        }
        int mid = (cl + cr) / 2;
        cur->push();
        return merge(
            query(cur->l, cl, mid, ql, qr),
            query(cur->r, mid + 1, cr, ql, qr)
        );
    }
    public:
    int N;
    Seg(int N) {
        this->N = N;
        top = NULL;
    }
    void U(int st, int en, int val) {
        update(top, 0, N, st, en, val);
    }
    PII Q(int st, int en) {
        return query(top, 0, N, st, en);
    }
};
int main() {
    int n, m, q;
    int u, v;
    cin >> n >> m >> q;
    Seg *st = new Seg(n);
    while (m--) {
        scanf("%d %d", &u, &v);
	if(u>v)
	  swap(u,v);
        v--;
        st->U(u, v, 1);
    }
    int tp;
    while (q--) {
        scanf("%d %d %d", &tp, &u, &v);
	if(u>v)
	  swap(u,v);
        v--;
        if (tp == 0) {
            st->U(u, v, 1);
        } else if (tp == 1) {
            st->U(u, v, -1);
        } else {
            PII ret = st->Q(u, v);
            int ans = 0;
            if (ret.first == 0) {
                ans = ret.second;
            } 
            printf("%d\n", ans);
        }
    }
    return 0;
}

