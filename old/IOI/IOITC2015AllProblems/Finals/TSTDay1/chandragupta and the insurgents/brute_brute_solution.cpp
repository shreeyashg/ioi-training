#include<bits/stdc++.h>
using namespace std;

#define LET(x, a)  __typeof(a) x(a)
#define TR(v, it) for(LET(it, v.begin()); it != v.end(); it++)
#define si(x) scanf("%d",&x)
#define F first
#define S second
#define PB push_back
#define MP make_pair
#define INF 1000000000
#define MOD 1000000007
#define SET(x,y) memset(x,y,sizeof(x));
#define LL long long int
#define ULL unsigned LL
#define PII pair<int, int>

multiset<int> s[200000];
int vis[200000];
int ct = 4;
bool reach(int u, int v) {
    if (u == v)
        return true;
    if (vis[u] == ct)
        return false;
    vis[u] = ct;
    multiset<int> ::iterator it;
    for (it = s[u].begin(); it != s[u].end(); it++) {
        if (reach(*it, v)) 
            return true;
    }
    return false;
}
int main() {
    int n, m, q;
    int u, v;
    cin >> n >> m >> q;
    while (m--) {
        scanf("%d %d", &u, &v);
        assert(u != v);
        if (u > v)
            swap(u, v);
        s[u].insert(v);
        s[v].insert(u);
    }
    for (int i = 2; i <= n; i++) {
        s[i].insert(i - 1);
        s[i - 1].insert(i);
    }
    int tp;
    while (q--) {
        scanf("%d %d %d", &tp, &u, &v);
        assert(u != v);
        if (u > v)
            swap(u, v);
        if (tp == 0) {
            s[u].insert(v);
            s[v].insert(u);
        } else if (tp == 1) {
            s[u].erase(s[u].find(v));
            s[v].erase(s[v].find(u));
        } else {
            int cnt = 0;
            for (int i = u + 1; i <= v; i++) {
                s[i].erase(s[i].find(i - 1));
                s[i - 1].erase(s[i - 1].find(i));
                ct++;
                if (!reach(u, v)) {
                    cnt ++;
                }
                s[i].insert(i - 1);
                s[i - 1].insert(i);
            }
            printf("%d\n", cnt);
        }
    }
    return 0;
}

