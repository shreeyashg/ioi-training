Given a graph G=(V,E1 \union E2). V = {v1,v2,..,vN} is the set of vertices and E1 \union E2 is the set of bidirectional edges. E1 = {(v1,v2) , (v2,v3), ... , (v(N-1),vN)}.
A pair of vertices {u,v} is said to be connected if you can reach one from the other through some sequence of edges from E1 and E2. ie. you can use any of the edges in the graph.
A pair of connected vertices {u,v} is said to be fault-resistant, if for all edges (x,y) which belong to (E1 \union E2), deleting (x,y) still keeps {u,v} connected. ie. {u,v} is connected in G' = (V, E1 \union E2 - (x,y)), for all (x,y) \in E1 \union E2.

E1 are 'permanent edges'. E2 forms a 'dynamic set'. Updates are of the form, "Delete (u,v) from E2" or "Add (u,v) to E2". Queries are of the form "Is the pair {u,v} fault-resistant?".
|V| < 10^5 , Initial number of edges < 5 * 10^5. Number of updates < 3 * 10^5. Number of queries < 3 * 10^5. (Something like this..)

Variant: For query {u,v}, find the number of edges (x,y) in E1 \union E2, such that {u,v} is connected in G' = (V, E1 \union E2 - (x,y)).
(note: If the answer to this is N, the answer to the first variant is '{u,v} is fault-resistant'. If the answer is < M, then the answer to the first variant is 'not fault-resistant'.
