#include <bits/stdc++.h>
#include "testlib.h"
#define SZ(x) ((int)(x).size())
#define FOR(it, c) for(__typeof((c).begin()) it = (c).begin(); it != (c).end(); ++it)
#define pb push_back
#define F first
#define S second
#define MAXN 200000
#define mp make_pair
using namespace std;
typedef long long LL;
typedef vector< pair<int,int> > VPII;
int p[MAXN+100];
pair<int,int> limits[MAXN+10];
int weight[MAXN+10];
void pre()
{
    for(int i=0; i<MAXN+100; i++)
        p[i]=i+1;
}
vector <  int > G[MAXN+10];
int vis[MAXN+10];
bool dfs(int node, int p, vector < pair < int , int > >& ar)
{
    if(p!=-1)
        ar.push_back(make_pair(p,node));
    int q=G[node].size();
    vis[node]=1;
    for(int i=0; i<q; i++)
        if(G[node][i]!=p)
        {
            if(vis[G[node][i]])return false;
            if(dfs(G[node][i],node,ar)==false)return false;
        }
    return true;
}
int dfs1(int node, int p)
{
    int q=G[node].size(),ret=1;
    for(int i=0; i<q; i++)
        if(G[node][i]!=p)
            ret += dfs1(G[node][i],node);
    if(ret!=1)ret++;
    weight[node]=ret;
    return ret;
}
void dfs2(int node, int p, int l, int r)
{
    limits[node]=mp(l,r);
    int q=G[node].size(),ll=l+1;
    for(int i=0; i<q; i++)
        if(G[node][i]!=p){
            dfs2(G[node][i],node,ll,ll+weight[G[node][i]]-1);
            ll+=weight[G[node][i]];
        }
}
int check(vector < pair < int , int > > ar, vector < pair < int , int > >& ret)
{
    int n=ar.size()+1;
    for(int i=0; i<=n; i++)
        G[i].clear();
    for(int i=0; i<n-1; i++)
    {
        G[ar[i].F].pb(ar[i].S);
        G[ar[i].S].pb(ar[i].F);
        if(ar[i].F==ar[i].S)return false;
    }
    dfs1(1,-1);
    dfs2(1,-1,1,weight[1]);
    memset(vis,0,sizeof(vis));
    if(dfs(1,-1,ret))return 1;
    else return -1;
}
vector<pair<int, int> > treeexpand(int n , int d) {
    int t = 2;
    vector<pair<int, int> > E;
    for(int i=1;i<=n;i++) {
        int prob = rnd.next(1,p[d-1]), nchild = 0;
        nchild=prob;
        for(int j = 0; j < nchild && t <= n; j++, t++)
            E.push_back(make_pair(i, t));
    }
    vector < pair < int, int > > ret;
    int p=check(E,ret);
    if (SZ(E) < n-1 || p==-1)return treeexpand(n,d);
    return ret;
}

vector<pair<int, int> > treeparents(int n) {
    vector<pair<int, int> > E;
    for(int i=2;i<=n;i++) {
        int p = rnd.next(1,i-1);
        E.push_back(make_pair(i, p));
    }
    vector < pair < int, int > > ret;
    int p=check(E,ret);
    if (SZ(E) < n-1 || p==-1)return treeparents(n);
    return ret;
}

vector<pair<int, int> > treechain(int n, int chain) {
    memset(vis,0,sizeof(vis));
    vector<pair<int, int> > E;
    vector < int  > choose;
    choose.resize(chain+1);
    for(int i=1; i<=chain; i++)
        choose[i]=(i);
    for(int i=chain+1; i<=n; i++)
    {
        int j=rnd.next(1,i);
        if(j<=chain)choose[j]=i;
    }
    shuffle(choose.begin()+1,choose.end());
    for(int i=1; i<=chain; i++)
    {
        vis[choose[i]]=1;
        if(i!=1)E.pb(make_pair(choose[i],choose[i-1]));
    }
    for(int i=1; i<=n; i++)
        if(vis[i]==0)E.pb(make_pair(choose[(rnd.next(1,chain))],i));

    vector < pair < int, int > > ret;
    int p=check(E,ret);
    if (SZ(E) < n-1 || p==-1)return treechain(n,chain);
    return ret;
}
vector<pair<int, int> > singlechain(int n){
    memset(vis,0,sizeof(vis));
    vector<pair<int, int> > E;
    for(int i=1; i<n; i++)
        E.push_back(make_pair(i,i+1));
    vector < pair < int, int > > ret;
    int p=check(E,ret);
    return ret;
}
vector<pair<int, int> > btree(int n){
    memset(vis,0,sizeof(vis));
    vector<pair<int, int> > E;
    for(int i=1; i<=n; i++){
        if(2*i<=n)
            E.pb(make_pair(i,2*i));
        if(2*i+1<=n)
            E.pb(make_pair(i,2*i+1));
    }
    vector < pair < int, int > > ret;
    int p=check(E,ret);
    return ret;
}
int ftst=0;
void single(int n,int flag){
    char name[11];
    sprintf(name,"input%d.in",ftst);
    ftst++;
    freopen(name,"wb",stdout);
    FILE* out=fopen(name,"wb");
    vector< pair<int,int> > tr;
    int nn=n/2;
    if(flag==0)tr=singlechain(nn);
    else if(flag==1)tr=btree(nn);
    else if(flag==2)tr=treeparents(nn);
    VPII upd,quer;
    assert(weight[1]<=n);
    for(int i=1; i<=nn; i++){
        if(rnd.next(1,2)==1)
            upd.push_back(limits[i]);
        else quer.push_back(limits[i]);
    }
    printf("%d %d %d\n",n,(int)upd.size(),(int)quer.size());
    shuffle(upd.begin(),upd.end());
    shuffle(quer.begin(),quer.end());
    for(int i=0; i<upd.size(); i++){
        int y1,y2;
        if(rnd.next(1,100)<=30){
            y1=rnd.next(1,n);
            y2=rnd.next(y1,n);
        }
        else{
            y1=rnd.next(1,n/4);
            y2=rnd.next(n-n/4,n);
        }
        printf("%d %d %d %d\n",upd[i].first,y1,upd[i].second,y2);
    }
    for(int i=0; i<quer.size(); i++){
        int y1,y2;
        if(rnd.next(1,100)<=30){
            y1=rnd.next(1,n);
            y2=rnd.next(y1,n);
        }
        else{
            y1=rnd.next(1,n/4);
            y2=rnd.next(n-n/4,n);
        }
        printf("%d %d %d %d\n",quer[i].first,y1,quer[i].second,y2);
    }
}
void single1(int n, int flag, int factor){
    char name[11];
    sprintf(name,"input%d.in",ftst);
    ftst++;
    freopen(name,"wb",stdout);
    FILE* out=fopen(name,"wb");
    vector< pair<int,int> > tr;
    int nn=n/2;
    if(flag==0)tr=treechain(nn,factor);
    else if(flag==1)tr=treeexpand(nn,factor);
    VPII upd,quer;
    assert(weight[1]<=n);
    for(int i=1; i<=nn; i++){
        if(rnd.next(1,2)==1)
            upd.push_back(limits[i]);
        else quer.push_back(limits[i]);
    }
    printf("%d %d %d\n",n,(int)upd.size(),(int)quer.size());
    shuffle(upd.begin(),upd.end());
    shuffle(quer.begin(),quer.end());
    for(int i=0; i<upd.size(); i++){
        int y1,y2;
        if(rnd.next(1,100)<=30){
            y1=rnd.next(1,n);
            y2=rnd.next(y1,n);
        }
        else{
            y1=rnd.next(1,n/4);
            y2=rnd.next(n-n/4,n);
        }
        printf("%d %d %d %d\n",upd[i].first,y1,upd[i].second,y2);
    }
    for(int i=0; i<quer.size(); i++){
        int y1,y2;
        if(rnd.next(1,100)<=30){
            y1=rnd.next(1,n);
            y2=rnd.next(y1,n);
        }
        else{
            y1=rnd.next(1,n/4);
            y2=rnd.next(n-n/4,n);
        }
        printf("%d %d %d %d\n",quer[i].first,y1,quer[i].second,y2);
    }
}
//legend
//void single(int n, int flag) flag=0 : singlechain, flag=1:btree, flag=2: treeparents
//void single1(int n, int flag, int factor) flag=0: treechain(n,factor), flag=1: treeexpand(n,factor)
int main(void) {
    pre();
    //0 to 6 are subtask1
    single(1000,0);
    single(1000,1);
    single(1000,2);
    single1(1000,1,2);
    single1(1000,1,5);
    single1(1000,1,10);
    single1(1000,0,500);

    //7 to 14 are subtask2
    single(MAXN,0);
    single(MAXN,1);
    single(MAXN,2);
    single1(MAXN,1,2);
    single1(MAXN,1,5);
    single1(MAXN,1,10);
    single1(MAXN,1,20);
    single1(MAXN,0,MAXN/2);
    return 0;
}
