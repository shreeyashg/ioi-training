#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <queue>
#include <set>
#include <cstdio>
#include <cstdlib>
#include <stack>
#include <cstring>
#include <iomanip>
#include <cctype>
#include <map>

using namespace std;
const int N = 1005;
int a[N][N];
int p[N][N];

int main() {
    int n,u,q; cin>>n>>u>>q;
    memset(a,0,sizeof(a));
    for(int i = 0;i < u;i++) {
        int x1,x2,y1,y2;
        cin>>x1>>y1>>x2>>y2;
        a[x1][y1]++;
        a[x1][y2 + 1]--;
        a[x2 + 1][y1]--;
        a[x2 + 1][y2 + 1]++;
    }
    for(int i = 1;i <= n;i++) {
        for(int j = 1;j <= n;j++) {
            if(i > 1) a[i][j]+=a[i - 1][j];
            if(j > 1) a[i][j]+=a[i][j - 1];
            if(i > 1 && j > 1) a[i][j]-=a[i - 1][j - 1];
        }
    }
    for(int i = 1;i <= n;i++) {
        p[i][0] = 0;
        for(int j = 1;j <= n;j++) {
            if(a[i][j]) a[i][j] = 1;
            p[i][j] = (a[i][j] == 0 ? j : p[i][j - 1]);
        }
    }
    for(int i = 0;i < q;i++) {
        int x1,x2,y1,y2; cin>>x1>>y1>>x2>>y2;
        int cnt = 0;
        for(int x = x1;x <= x2;x++) {
            if(p[x][y2] < y1) cnt++;
        }
        cout<<cnt<<endl;
    }
}