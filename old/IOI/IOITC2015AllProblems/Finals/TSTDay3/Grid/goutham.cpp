#include <map>
#include <set>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cctype>
#include <cstdio>
#include <vector>
#include <cassert>
#include <complex>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;
#define int long long

const int N = 2e5 + 23;
int seg[4 * N], lazy[4 * N], b[4 * N], e[4 * N];

void calc(int n)
{
	if (b[n] == e[n]) seg[n] = 1;
	else
	{
		seg[n] = 0;
		if (!lazy[n + n]) seg[n] += seg[n + n];
		if (!lazy[n + n + 1]) seg[n] += seg[n + n + 1];
	}
}

void init(int n, int l, int r)
{
	b[n] = l, e[n] = r;
	if (l != r)
	{
		int m = (l + r) / 2;
		init(n + n, l, m);
		init(n + n + 1, m + 1, r);
	}
	calc(n);
}

void upd(int n, int l, int r, int v)
{
	if (l > e[n] or r < b[n]) return;
	if (l <= b[n] and e[n] <= r)
	{
		lazy[n] += v;
		n /= 2;
		while (n > 0) calc(n), n /= 2;
		return;
	}
	upd(n + n, l, r, v);
	upd(n + n + 1, l, r, v);
}

int que(int n, int l, int r)
{
	if (l > e[n] or r < b[n]) return 0;
	if (lazy[n]) return 0;
	if (l <= b[n] and e[n] <= r) return seg[n];
	return que(n + n, l, r) + que(n + n + 1, l, r);
}

int u[N][4], q[N][4], ans[N];
vector <int> evs;
map <int, pair <int, int> > ma;
map <int, int> st;

#undef int
int main()
{
#define int long long
	int n, q1, q2;
	scanf("%lld%lld%lld", &n, &q1, &q2);
	for (int i = 0; i < q1; i++)
	{
		scanf("%lld%lld%lld%lld", &u[i][0], &u[i][1], &u[i][2], &u[i][3]);
		evs.push_back(u[i][0]);
		evs.push_back(u[i][2]);
		ma[u[i][0]] = make_pair(i, 0);
		ma[u[i][2]] = make_pair(i, 0);
	}
	for (int i = 0; i < q2; i++)
	{
		scanf("%lld%lld%lld%lld", &q[i][0], &q[i][1], &q[i][2], &q[i][3]);
		evs.push_back(q[i][0]);
		evs.push_back(q[i][2]);
		ma[q[i][0]] = make_pair(i, 1);
		ma[q[i][2]] = make_pair(i, 1);
		st[q[i][2]] = i;
	}

	sort(evs.begin(), evs.end());
	init(1, 1, n);

	int sz = evs.size();
	for (int i = 0; i < sz; i++)
	{
		int x = evs[i];
		int id = ma[x].first;
		if (ma[x].second == 0)
		{
			if (u[id][0] == u[id][2]) continue;
			if (u[id][0] == x)
				upd(1, u[id][1], u[id][3], 1);
			else
				upd(1, u[id][1], u[id][3], -1);
		}
		else if (q[id][2] == x)
				ans[st[x]] = que(1, q[id][1], q[id][3]);
	}

	for (int i = 0; i < q2; i++)
		printf("%lld\n", q[i][3] - q[i][1] + 1 - ans[i]);
	return 0;
}