#include<bits/stdc++.h>
using namespace std;

#define LET(x, a)  __typeof(a) x(a)
#define TR(v, it) for(LET(it, v.begin()); it != v.end(); it++)
#define si(x) scanf("%d",&x)
#define F first
#define S second
#define PB push_back
#define MP make_pair
#define INF 1000000000
#define MOD 1000000007
#define SET(x,y) memset(x,y,sizeof(x));
#define LL long long int
#define ULL unsigned LL
#define PII pair<int, int>
class Seg {
    static PII merge(PII ele1, PII ele2) {
        if (ele1.first > ele2.first)
            swap(ele1, ele2);
        if (ele1.first == ele2.first)
            ele1.second += ele2.second;
        return ele1;
    }
    struct node {
        node *l, *r;
        int st, en;
        int add;
        PII mins;
        node(int st, int en) {
            l = r = NULL;
            mins = MP(0, en - st + 1);
            this->st = st;
            this->en = en;
            add = 0;
        }
        void incr(int by) {
            add += by;
            mins.first += by;
        }
        void push() {
            if (add) {
                int mid = (st + en) / 2;
                if (!l) l = new node(st, mid);
                if (!r) r = new node(mid + 1, en);
                l->incr(add);
                r->incr(add);
                add = 0;
            }
        }
        void recal() {
            push();
            int mid = (st + en) / 2;
            PII minsl, minsr;
            if (l) {
                minsl = l->mins;
            } else {
                minsl = MP(0, mid - st + 1);
            }
            if (r) {
                minsr = r->mins;
            } else {
                minsr = MP(0, en - mid);
            }
            mins = merge(minsl, minsr);
        }
    };
    PII query(node *&cur, int cl, int cr, int ql, int qr) {
        if (ql > cr || cl > qr) 
            return MP(INF, 0);
        if (cur == NULL)
            return MP(0, min(qr, cr) - max(ql, cl) + 1);
        if (cl >= ql && cr <= qr) {
            return cur->mins;
        }
        cur->push();
        int mid = (cl + cr) / 2;
        return merge(
            query(cur->l, cl, mid, ql, qr), 
            query(cur->r, mid + 1, cr, ql, qr)
        );
    }
    void update(node *&cur, int cl, int cr, int ql, int qr, int by) { 
        if (ql > cr || cl > qr)
            return;
        if (cur == NULL)
            cur = new node(cl, cr);
        if (cl >= ql && cr <= qr) {
            cur->incr(by);
            return;
        }
        cur->push();
        int mid = (cl + cr) / 2;
        update(cur->l, cl, mid, ql, qr, by);
        update(cur->r, mid + 1, cr, ql, qr, by);
        cur->recal();
    }

    public:
    int N;
    node *top;
    Seg(int N) {
        this->N = N;
        top = NULL;
    }
    PII Q(int ql, int qr) {
        return query(top, 0, N, ql, qr);
    }
    void U(int ql, int qr, int val) {
        return update(top, 0, N, ql, qr, val);
    }
};
Seg *s;

struct Range {
    int tp;
    int stx, enx;
    int sty, eny;
    Range(int tp, int stx, int enx, int sty = 0, int eny = 0) {
        this->tp = tp;
        this->stx = stx;
        this->enx = enx;
        this->sty = sty;
        this->eny = eny;
    }
    bool operator < (Range b) {
        return stx == b.stx ? tp < b.tp : stx < b.stx;
    }
};

vector<int> v[500000];
int qx1[500000], qy1[500000], qx2[500000], qy2[500000];
vector<Range> nodes;
int ans[500000];
int used[500000];
int main() {
    int dummy, N, Q;
    cin >> dummy >> N >> Q;
    s = new Seg(INF);
    int x1, y1, x2, y2, i;
    for (i = 0; i < N; i++) {
        scanf("%d %d %d %d", &x1, &y1, &x2, &y2);
        assert(x1 <= x2);
        assert(y1 <= y2);
        if (x1 > x2) swap(x1, x2);
        if (y1 > y2) swap(y1, y2);
        assert(y1 >= 0 && y2 <= dummy);
        assert(!used[x1]);
        assert(!used[x2]);
        used[x1] = 1;
        used[x2] = 1;
        nodes.PB(*new Range(0, x1, x1, y1, y2));
        nodes.PB(*new Range(1, x2, x2, y1, y2));
    }
    for (i = 0; i < Q; i++) {
        scanf("%d %d %d %d", &qx1[i], &qy1[i], &qx2[i], &qy2[i]);
        assert(qx1[i] <= qx2[i]);
        assert(qy1[i] <= qy2[i]);
        assert(!used[qx1[i]]);
        assert(!used[qx2[i]]);
        assert(qy1[i] >= 0 && qy2[i] <= dummy);
        used[qx1[i]] = 1;
        used[qx2[i]] = 1;
        if (qx1[i] > qx2[i]) swap(qx1[i], qx2[i]);
        if (qy1[i] > qy2[i]) swap(qy1[i], qy2[i]);
        nodes.PB(*new Range(2, qx1[i], qx1[i], i, i));
    }
    sort(nodes.begin(), nodes.end());
    for (int i = 0; i < (int)nodes.size(); i++) {
        if (nodes[i].tp == 0) {
            s->U(nodes[i].sty, nodes[i].eny, 1);
        } else if (nodes[i].tp == 1) {
            s->U(nodes[i].sty, nodes[i].eny, -1);
        } else {
            int cq = nodes[i].sty;
            PII ret = s->Q(qy1[cq], qy2[cq]);
            if (ret.first != 0) 
                ret.second = 0;
            ans[cq] = qy2[cq] - qy1[cq] + 1 - ret.second;
        }
    }
    for (int i = 0; i < Q; i++) {
        printf("%d\n", ans[i]);
    }
    return 0;
}

