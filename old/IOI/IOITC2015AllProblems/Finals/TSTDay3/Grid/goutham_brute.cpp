#include <map>
#include <set>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cctype>
#include <cstdio>
#include <vector>
#include <cassert>
#include <complex>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;
#define int long long

const int N = 1000 + 23;
int a[N][N];

#undef int
int main()
{
#define int long long
	int n, q1, q2;
	cin >> n >> q1 >> q2;
	while (q1--)
	{
		int x1, y1, x2, y2;
		cin >> x1 >> y1 >> x2 >> y2;
		for (int i = x1; i <= x2; i++)
			for (int j = y1; j <= y2; j++)
				a[i][j] = 1;
	}
	while (q2--)
	{
		int x1, y1, x2, y2;
		cin >> x1 >> y1 >> x2 >> y2;
		int ans = 0;
		for (int i = y1; i <= y2; i++)
		{
			bool can = true;
			for (int j = x1; j <= x2; j++)
				if (a[j][i] == 0)
					can = false;
			if (can) ans++;
		}
		cout << ans << "\n";
	}
	return 0;
}