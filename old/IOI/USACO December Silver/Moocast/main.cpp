#include <iostream>
#include <vector>
#include <cstring>
#include <queue>
#include <map>
#include <deque>
#include <fstream>
#include <algorithm>
#include <cmath>
#include <set>

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i=a; i<=b; i++)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

using namespace std;

ll dp[205], vis[205];

ll n;
ll k;
struct cow
{
        
        ll x, y, p;
};

vector<cow> v;



void dfs(ll idx)
{
        if(vis[idx])
                return;
        
        vis[idx]=1;
        
        REP(i, n)
        {
                if(sqrt((v[idx].x-v[i].x)*(v[idx].x-v[i].x) + (v[idx].y-v[i].y)*(v[idx].y-v[i].y))<=v[idx].p)
                {
                        dfs(i);
                }
        }
}


int main()
{
        ifstream cin("moocast.in");
        ofstream cout("moocast.out");
        cin>>n;
        REP(i, n)
        {
                cow a;
                ll x, y, z;
                cin>>a.x>>a.y>>a.p;
                v.push_back(a);
        }
        ll ans=0;
        ll mans=0;
        REP(i, n)
        {
                memset(vis, 0, sizeof(vis));
                dfs(i);
                REP(j, n)
                {
                        if(vis[j]) ans++;
                }
                mans = max(mans, ans);
                ans=0;
        }
        
        cout<<mans<<endl;
}