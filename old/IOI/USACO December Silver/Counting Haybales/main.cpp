#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <deque>
#include <fstream>
#include <algorithm>
#include <set>

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i=a; i<=b; i++)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

using namespace std;

const ll MX = 1e5 + 11;

ll n, q;
vector<ll> v;

int main()
{
        ifstream cin("haybales.in");
        ofstream cout("haybales.out");
        cin>>n>>q;
        REP(i, n)
        {
                ll x;
                cin>>x;
                v.push_back(x);
        }
        sort(v.begin(), v.end());
        
        
        REP(i, q)
        {
                ll a, b;
                cin>>a>>b;
                auto it1 = lower_bound(v.begin(), v.end(), a);
                auto it2 = lower_bound(v.begin(), v.end(), b);
                
                //cout<<*it2<<" "<<*it1<<endl;
                if(it2==v.end() && it1==v.end())
                {
                        cout<<0<<endl;
                        continue;
                }
                if(*it2>b || it2==v.end())
                {
                        it2--;
                }
                
                
                cout<<it2-it1+1<<endl;
        }
}