#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <deque>
#include <fstream>
#include <algorithm>
#include <set>

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i=a; i<=b; i++)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

using namespace std;

ll n;

vector<pair<string, string> >v;

ll bsdown(string x)
{
        string chk = x.substr(0,2);
        
        ll lo = 0;
        ll hi = v.size()-1;
        
        REP(i, 50)
        {
                ll mid = (lo+hi)/2;
                string abc = v[mid].first.substr(0, 2);
                if(abc > chk)
                {
                        hi = mid-1;
                }
                else if(abc < chk)
                {
                        lo = mid+1;
                }
                else if(lo != mid)
                {
                        hi = mid;
                }
                else
                        return mid;
        }
        
        return -1;
}

ll bsup(string x)
{
        string chk = x.substr(0,2);
        
        ll lo = 0;
        ll hi = v.size()-1;
        ll found = -1;
        
        REP(i, 50)
        {
                ll mid = (lo+hi)/2;
                string abc = v[mid].first.substr(0, 2);
                if(abc > chk)
                {
                        hi = mid-1;
                }
                else if(abc < chk)
                {
                        lo = mid+1;
                }
                else if(abc == chk)
                {
                        found = mid;
                        lo = mid+1;
                }
        }
        
        return found;
}

int main()
{
       // ifstream cin("citystate.in");
       // ofstream cout("citystate.out");
        map<string, multiset<string> >mp;
        cin>>n;
        REP(i, n)
        {
                string a, b;
                cin>>a>>b;
                mp[b].insert(a);
                mp[a.substr(0,2)].insert(b);
        }
        ll ans=0;
        FOR(i, 'A', 'Z')
        {
                FOR(j, 'A', 'Z')
                {
                        FOR(k, 'A', 'Z')
                        {
                                FOR(l, 'A', 'Z')
                                {
                                        char a = i;
                                        char b = j;
                                        char c = k;
                                        char d = l;
                                        string aa,bb;
                                        aa+=a;
                                        aa+=b;
                                        bb+=c;
                                        bb+=d;
                                        ans+=mp[aa].count(bb);
                                        ans+=mp[bb].count(aa);
                                }
                        }
                }
        }
        
        /*
        REP(i, v.size())
        {
                REP(j, v.size())
                {
                        if(j!=i && v[j].second==v[i].first.substr(0, 2) && v[i].second==v[j].first.substr(0, 2))
                        {
                                ans++;
                        }
                }
        } */
        
        cout<<ans<<endl;
}