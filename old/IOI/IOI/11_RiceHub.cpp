#include <bits/stdc++.h>
using namespace std;

typedef int  ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, MX = 5e6 + 4;

ll n, R[MX];

int main()
{
	ios::sync_with_stdio(false);
	cin.tie(0);
	cin>>n;
	REP(i, n)
	{
		cin>>R[i];
	}
	sort(R, R+n);
	cout << "Done" << endl;
}