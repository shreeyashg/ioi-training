#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <tuple>
#include <unordered_map>
#include <iomanip>
#include <functional>
using namespace std;

typedef long long ll;
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
//#define MX 30001
#define MOD 1000000007
#define INF 1000009*1e9

const ll MX = 1e5 + 4;
const ll V = 1e3 + 6;

ll choice[V];

ll n, m;
ll adj[V][V];
vector<ll> v[MX];
/*
bool valid(ll from, ll to)
{
        bool a=0, b=0;
        for(ll i=max(from, to); i!=min(from, to); i=i%n + 1)
        {
                if(!vis[i] && i!=from && i!=to)
                        a=1;
        }
        for(ll i=min(from,to); i!=max(from,to); i++)
        {
                if(!vis[i] && i!=from && i!=to)
                        b=1;
        }
        
        if(a&&b)
                return 0;
        else
                return 1;
}*/

ll left(ll x)
{
        if(x==1) return n;
        return x-1;
}

ll right(ll x)
{
        if(x==n) return 1;
        return x+1;
}

bool DFS(ll x, ll le, ll ri, set<ll> vis)
{
        
        if(vis.count(x)==0)
                vis.insert(x);
        else
                return 0;
        
        if(vis.size()==n) {;
                return 1;}
        
        if(adj[x][le] && DFS(le, left(le), ri, vis)) {
                choice[x] = le;
                return 1;
        }
        else if(adj[x][ri] && DFS(ri, le, right(ri), vis)) {
                choice[x] = ri;
                return 1;
        }
        else if(0 && adj[x][ri] && adj[x][le] && DFS(le, left(le), ri, vis))
                return 1;
        else if(0 && adj[x][ri] && adj[x][le] && DFS(ri, le, right(ri), vis))
                return 1;
                
        else
                return 0;
        
        //return max(DFS(le, left(le), ri), DFS(ri, le, right(ri)));
        return 0;
}

void printpath(ll x)
{
        ll f = x;
        REP(i, n)
        {
                cout<<f<<endl;
                f = choice[f];
        }
}

int main()
{
        cin>>n;
        cin>>m;
        REP(i, m)
        {
                ll a, b;
                cin>>a>>b;
                adj[a][b] = adj[b][a] = 1;
                v[a].push_back(b);
                v[b].push_back(a);
        }
        
        ll ans=0;
        FOR(i, 1, n)
        {
                set<ll> ab;
                //cout<<i<<" "<<left(i)<<" "<<right(i)<<endl;
                if(DFS(i, left(i), right(i), ab))
                {
                        printpath(i);
                        exit(0);
                }
                //DEBUG(DFS(i, n));
        }
        cout<<-1<<endl;
}