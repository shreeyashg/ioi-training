#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <unordered_map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 100006 //10^5 + 6
#define MOD 1000000007 // 10^9 + 7
#define INF 1e18+5 // 10^18 + 5

int main()
{
        ios::sync_with_stdio(false);
        cin.tie(0);
        ll t;
        cin>>t;
        while(t--)
        {
                vector<ll> v;
                unordered_map<ll, ll> mp;
                ll n;
                cin>>n;
                REP(i, n)
                {
                        ll k;
                        cin>>k;
                        v.push_back(k);
                        mp[k]++;
                }
                
                ll m1= -INF;
                ll m2= -INF;
                ll m1p=0, m2p=0;
                
                REP(i, v.size())
                {
                        if(v[i]>m1)
                        {
                                m1= v[i];
                                m1p  =i;
                        }
                }
                
                REP(i, v.size())
                {
                        if(v[i]>m2 && i!=m1p)
                        {
                                m2 = v[i];
                                m2p =i;
                        }
                }
                
                if(mp[m2*m1])
                {
                        cout<<"yes"<<'\n';
                }
                else
                {
                        cout<<"no"<<'\n';
                }
                /*
                ll sum=0;
                FOR(i, 0, v.size()-1)
                {
                        FOR(j, i+1, v.size()-1)
                        {
                                ll p = v[i]*v[j];
                                if(mp[p])
                                {
                                        continue;
                                }
                                else
                                {
                                        cout<<"no"<<'\n';
                                        exit(0);
                                }
                        }
                }
                
                cout<<"yes"<<'\n';
                */
        }
}