#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <unordered_map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 100006 //10^5 + 6
#define MOD 1000000007 // 10^9 + 7
#define INF 1e18+5 // 10^18 + 5

int main()
{
        ll t;
        cin>>t;
        while(t--)
        {
                vector<ll> v;
                ll l, o;
                cin>>l>>o;
                REP(i, l)
                {
                        v.push_back(i+1);
                }
                
                ll ans=0;
                REP(i, v.size())
                {
                        REP(j, v.size())
                        {
                                if(i!=j && (v[i]+v[j])%o==0)
                                        ans++;
                        }
                }
                cout<<ans<<endl;
        }
        
}