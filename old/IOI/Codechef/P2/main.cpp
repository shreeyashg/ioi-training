#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 100006 //10^5 + 6
#define MOD 1000000007 // 10^9 + 7
#define INF 1e18+5 // 10^18 + 5

ll s(ll x)
{
        ll sum=0;
        string str = to_string(x);
        REP(i, str.size())
        {
                sum+=(str[i]-'0');
        }
        return sum;
}

int main()
{
        ll t;
        cin>>t;
        while(t--)
        {
                ll n;
                cin>>n;
                ll sum = s(n);
                string str="";
                ll num=0;
                ll p = 0;
                while(sum>9)
                {
                        num+=(pow(10, p)*9);
                        sum-=9;
                        p++;
                }
                num+=(pow(10, p)*sum);
                cout<<s(num+1)<<endl;
        }
}