#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 5;

vector<pair<ll, ll> > v[MX];
set<ll> s[MX];
ll dist[MX], vis[MX];

void dij(ll s)
{
        memset(vis, 0, sizeof(vis));
        REP(I, MX) dist[I] = INF;
        dist[s] = 0;
        priority_queue<pair<ll, ll>, vector<pair<ll, ll> >, greater<pair<ll, ll> > > q;
        q.push({0, s});
        while(!q.empty())
        {
                auto t = q.top();
                q.pop();
                
               // cout<<" > "<<t.second<<endl;
                ll id = t.second;
                ll di = t.first;
                
                if(!vis[id])
                        vis[id] = 1;
                else
                        continue;
                
                REP(i, v[id].size())
                {
                        ll nid = v[id][i].first;
                        ll ndist = di + v[id][i].second;
                        if(!vis[nid] && ndist<dist[nid])
                        {
                                dist[nid] = ndist;
                                q.push({ndist, nid});
                                //if(!vis[nid])vis[nid] = 1;
                        }
                }
        }
}


int main()
{
	ll t;
	cin>>t;
	while(t--)
	{

		ll n, k, x, m, ss;
		FOR(i, 0, MX-1) v[i].clear(), s[i].clear();
		cin >> n >> k >> x >> m >> ss;
		REP(i, m)
		{
			ll a, b, c;
			cin >> a >> b >> c;
			v[a].push_back({b, c}), s[a].insert(b),v[b].push_back({a, c}), s[b].insert(a);
		}
		
		if(ss <= k)
		{
			FOR(i, 1, k)
			{
				if(i!=ss)v[ss].push_back({i, x}), v[i].push_back({ss, x}), s[ss].insert(i), s[i].insert(ss);
			}
		}
	/*	FOR(i, 1, n)
		{
			REP(j, v[i].size())
			{
				cout << i << " " << v[i][j].first << " " << v[i][j].second << endl;
			}
		} */

		dij(ss);
		FOR(i, 1, n)
		{
			cout << dist[i] << " ";
		}
		cout << endl;
	}
}