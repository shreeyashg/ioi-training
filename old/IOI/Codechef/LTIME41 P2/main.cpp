#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <unordered_map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 100006 //10^5 + 6
#define MOD 1000000007 // 10^9 + 7
#define INF 1e18+5 // 10^18 + 5


ll n;
vector<ll> v;
string str;

bool div(ll a, ll b, ll c)
{
        
        cout<<"1 "<<a+1<<" "<<b+1<<" "<<c<<endl;
        string str;
        cin>>str;
        return str=="Yes";
}

bool isless(ll a, ll b)
{
        string str;
        cout<<"2 "<<a+1<<" "<<b+1<<endl;
        cin>>str;
        return str=="Yes";
}

// vec has indices
// solve returns order of elements with indices in vec with index of min val in indices
vector<ll>  solve(vector<ll> vec, ll d)
{
        //first is the order and second is the min val's index
        vector<ll> v1;
        vector<ll> v2;
        
        if(vec.size()==1)
        {
                return vec;
        }
        if(vec.size()==2)
        {
                if(isless(vec[0], vec[1]))
                {
                        return {vec[0], vec[1]};
                }
                else
                        return {vec[1], vec[0]};
        }
        v1.push_back(vec[0]);
        
        FOR(i, 1, vec.size()-1)
        {
                if(div(vec[0], vec[i], d))
                {
                        v1.push_back(vec[i]);
                }
                else
                {
                        v2.push_back(vec[i]);
                }
        }
        
        auto x = solve(v1, 2*d);
        auto y = solve(v2, 2*d);
        
        vector<ll> res;
        ll cnt=0;
        if(isless(x[0], y[0]))
        {
                REP(i, x.size())
                {
                        res.push_back(x[i]);
                        cnt++;
                        if(i<vec.size()) res.push_back(y[i]);
                }
        }
        else
        {
                REP(i, y.size())
                {
                        res.push_back(y[i]);
                        if(i<vec.size()) i++,res.push_back(x[i]);
                }
        }
        
        return res;
}

int main()
{
        ios::sync_with_stdio(false);
        cin.tie(0);
        
        ll t;
        cin>>t;
        while(t--)
        {
                v.clear();
                cin>>n;
                v.resize(n+11);
                vector<ll> indices;
                REP(i, n)
                {
                        indices.push_back(i);
                }
             
                vector<ll> ans = solve(indices, 2);
                cout<<"3 ";
                REP(i, n)
                {
                        cout<<ans[i]+1<<" ";
                }
                cout<<endl;
        }
        
        
        
}