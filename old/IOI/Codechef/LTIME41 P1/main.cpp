#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <unordered_map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 100006 //10^5 + 6
#define MOD 1000000007 // 10^9 + 7
#define INF 1e18+5 // 10^18 + 5

ll n=3;

int main()
{
        ios::sync_with_stdio(false);
        cin.tie(0);
        
        ll t;
        cin>>t;
        while(t--)
        {
                ll arr[3][3];
                REP(i, 3)
                {
                        REP(j, 3)
                        {
                                cin>>arr[i][j];
                        }
                }
                
                ll sumf=0;
                REP(i, n)
                {
                        ll sum=0;
                        REP(j, n)
                        {
                                sum+=arr[i][j];
                                if(sum%2)
                                        sumf = max(sumf, sum);
                                else
                                        sumf = max(sumf, sum-1);
                        }
                        if(sum%2)
                                sumf = max(sumf, sum);
                        else
                                sumf = max(sumf, sum-1);
                }
                
                REP(i, n)
                {
                        ll sum=0;
                        REP(j, n)
                        {
                                sum+=arr[j][i];
                                if(sum%2)
                                        sumf = max(sumf, sum);
                                else
                                        sumf = max(sumf, sum-1);
                        }
                        if(sum%2)
                                sumf = max(sumf, sum);
                        else
                                sumf = max(sumf, sum-1);
                }
                
                cout<<sumf<<endl;
        }
        
        
}