#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define ll long long int
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 100006 //10^5 + 6
//#define MOD 1000000007 // 10^9 + 7
#define INF 1e18+5 // 10^18 + 5

const ll MOD = 1e9 + 7;

//Function from https://discuss.codechef.com/questions/20451/a-tutorial-on-fast-modulo-multiplication-exponential-squaring

long long int fast_exp(ll base,ll exp)
{
        if(exp==1)
                return base%MOD;
        if(exp==0)
                return 1;
        else
        {
                if(exp%2 == 0)
                {
                        long long int base1 = (ll)pow(fast_exp(base, exp/2),2)%MOD;
                        if(base1 >= 1000000007)
                                return base1%1000000007;
                        else
                                return base1%MOD;
                }
                else
                {
                        long long int ans = ((base%MOD * (ll)pow(fast_exp(base,(exp-1)/2),2)))%MOD;
                        if(ans >= 1000000007)
                                return ans%1000000007;
                        else
                                return ans%MOD;
                }
        }
}

int main()
{
        ll t;
        cin>>t;
        while(t--)
        {
                //(fast_exp(2, n-2) * (fast_exp(2, n-1)-n))%MOD
                //ll n;
                //cin>>n;
                ll n;
                cin>>n;
                if(n==1 || n==2)
                {
                        cout<<0<<endl;
                        continue;
                }
                cout<<(((ll)pow(2, n-2)%MOD)*((ll)pow(2, n-1)-n)%MOD + MOD)%MOD<<endl;//(fast_exp(2, n-2)%MOD * (fast_exp(2, n-1)%MOD-n%MOD)+MOD*10000000)%MOD<<endl;
              /*  for(ll n=2; n<=MOD; n++)
                {
                        if((fast_exp(2, n-2) * (fast_exp(2, n-1)-n))%MOD>MOD || (fast_exp(2, n-2) * (fast_exp(2, n-1)-n))%MOD<0)
                        {
                                DEBUG(n);
                                DEBUG((fast_exp(2, n-2)%MOD * (fast_exp(2, n-1)%MOD-n%MOD)+MOD*10000000)%MOD);
                        }
                }*/
        }
}