#include <iostream>
#include <cstdio>
#include <string>
#include <sstream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <stack>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <ctime>
#include <cassert>
using namespace std;
#define pb push_back
#define mp make_pair
#define pii pair<int,int>
#define vi vector<int>
#define vpii vector<pii>
#define SZ(x) ((int)(x.size()))
#define fi first
#define se second
#define FOR(i,n) for(int (i)=0;(i)<(n);++(i))
#define FORI(i,n) for(int (i)=1;(i)<=(n);++(i))
#define IN(x,y) ((y).find((x))!=(y).end())
#define ALL(t) t.begin(),t.end()
#define FOREACH(i,t) for (typeof(t.begin()) i=t.begin(); i!=t.end(); i++)
#define REPS(i,a,b) for(int (i)=(a);(i)<=(b);++i)
#define REP(i, n) for(ll i=0; i<n; i++)
#define REPD(i,a,b) for(int (i)=(a); (i)>=(b);--i)
#define REMAX(a,b) (a)=max((a),(b));
#define REMIN(a,b) (a)=min((a),(b));
#define DBG cerr << "debug here" << endl;
#define DBGV(vari) cerr << #vari<< " = "<< (vari) <<endl;

typedef long long ll;
const int INF = 1e9;

typedef pair<vector<int>, int> Result;

bool is_less(int i, int j)
{
        cout << 2 << " " << i+1 << " " << j+1 << endl;
        flush(cout);
        string res;
        cin >> res;
        flush(cout);
        return res == "Yes";
}

bool is_abs_dif_divisible(int i, int j, int d)
{
        cout << 1 << " " << i+1 << " " << j+1 << " " << d << endl;
        flush(cout);
        string res;
        cin >> res;
        flush(cout);
        return res == "Yes";
}

Result solve(vi indices, int divisor)
{
        int n = indices.size();
        Result res;
        if(n == 1)
        {
                res = {{1}, indices[0]};
        }
        else if(n == 2)
        {
                if(is_less(indices[0], indices[1]))
                {
                        res = {{1, 2}, indices[0]};
                }
                else
                {
                        res = {{2, 1}, indices[1]};
                }
        }
        else
        {
                vector<int> left_zip = {indices[0]};
                vector<int> right_zip;
                vector<int> left_indices = {0};
                vector<int> right_indices;
                for(int i = 1; i < n; ++i)
                {
                        if(is_abs_dif_divisible(indices[0], indices[i], divisor))
                        {
                                left_zip.pb(indices[i]);
                                left_indices.pb(i);
                        }
                        else
                        {
                                right_zip.pb(indices[i]);
                                right_indices.pb(i);
                        }
                }
                
                Result res_left = solve(left_zip, 2*divisor);
                Result res_right = solve(right_zip, 2*divisor);
                res.fi = vector<int>(n);
                res.se = res_left.se;
                if(is_less(res_right.se, res_left.se))
                {
                        res.se = res_right.se;
                }
                for(int i = 0; i < SZ(left_indices); ++i)
                {
                        int val = (res.se == res_left.se) ? (2 * res_left.fi[i] - 1) : (2 * res_left.fi[i]);
                        res.fi[left_indices[i]] = val;
                }
                for(int i = 0; i < SZ(right_indices); ++i)
                {
                        int val = (res.se == res_right.se) ? (2 * res_right.fi[i] - 1) : (2 * res_right.fi[i]);
                        res.fi[right_indices[i]] = val;
                }
        }
        return res;
}

int main()
{
        ios_base::sync_with_stdio(0);
        int t;
        cin >> t;
        flush(cout);
        while(t--)
        {
                int n;
                cin >> n;
                flush(cout);
                vi init;
                for(ll i=0; i<n; i++) init.pb(i);
                Result res = solve(init, 2);
                cout << "3";
                FOR(i, n) cout << " " << res.fi[i];
                cout << endl << flush;
                flush(cout);
        }
        return 0;
}