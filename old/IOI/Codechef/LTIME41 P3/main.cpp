#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <unordered_map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 4001 //10^5 + 6
#define MOD 1000000007 // 10^9 + 7
#define INF 1e18+5 // 10^18 + 5


ll n, m, q, s, e;
ll county[MX];
vector<ll> v[MX];
vector<ll> cot[MX];
ll target;

ll dp[MX][MX];
ll visited[MX];

ll rec(ll x)
{
        ll ans=INF;
        if(county[x]==target)
                return 0;
        if(!visited[x])
                visited[x] = 1;
        if(dp[s][x]) return dp[s][x];
        else
        {
                REP(i, v[x].size())
                {
                        if(!visited[v[x][i]])ans = min(ans,1+rec(v[x][i]));
                }
        }
        return dp[s][x] = ans;
}

int main()
{
        ios::sync_with_stdio(false);
        cin.tie(0);
        
        
        cin>>n>>m;
        REP(i, n)
        {
                cin>>county[i+1];
                cot[county[i+1]].push_back(i+1);
        }
        
        REP(i, n-1)
        {
                ll a, b;
                cin>>a>>b;
                v[a].push_back(b);
                v[b].push_back(a);
        }
        
        cin>>q;
        REP(i, q)
        {
                memset(visited, 0 ,sizeof(visited));
                cin>>s>>e;
                target = y;
                ll fk = rec(x);
                cout<<fk<<'\n';
        }
}