#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <unordered_map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 100006 //10^5 + 6
#define MOD 1000000007 // 10^9 + 7
#define INF 1e18+5 // 10^18 + 5

priority_queue<ll> pq1;
priority_queue<ll, vector<ll>, greater<ll> > pq2;
//set<ll> s;

int main()
{
        ios::sync_with_stdio(false);
        cin.tie(0);
        ll t;
        cin>>t;
        while(t--)
        {
                vector<ll> v;
                unordered_map<ll, ll> mp;
                ll n;
                cin>>n;
                REP(i, n)
                {
                        ll k;
                        cin>>k;
                        v.push_back(k);
                        pq1.push(k);
                        pq2.push(k);
                        mp[k]++;
                }
                
                /*
                
                ll m1= -INF;
                ll m2= -INF;
                ll m1p=0, m2p=0;
                
                REP(i, v.size())
                {
                        if(v[i]>m1)
                        {
                                m1= v[i];
                                m1p  =i;
                        }
                }
                
                REP(i, v.size())
                {
                        if(v[i]>m2 && i!=m1p)
                        {
                                m2 = v[i];
                                m2p =i;
                        }
                }
                 */
                
                //DEBUG(s.size());
                
                if(v.size()<=1e3)
                {
                        bool y = 1;
                        //vector<ll> vec;
                        
                        
                        
                        REP(i, v.size())
                        {
                                FOR(j, i+1, v.size()-1)
                                {
                                        if(mp[v[i]*v[j]])
                                        {
                                                continue;
                                        }
                                        else
                                        {
                                                cout<<"no"<<endl;
                                                y=0;
                                                break;
                                        }
                                }
                        }
                        if(y)
                        {
                                cout<<"yes"<<endl;
                        }
                        continue;
                }
                
                ll m1 = pq1.top();
                pq1.pop();
                ll m2 = pq1.top();
                ll n1 =pq2.top();
                pq2.pop();
                ll n2 = pq2.top();
                
                if(mp[m1*m2] && mp[m1*n1] && mp[m1*n2] && mp[m2*n2] && mp[n1*n2] && mp[m2*n1])
                {
                        cout<<"yes"<<'\n';
                }
                else
                {
                        cout<<"no"<<'\n';
                }
                /*
                 ll sum=0;
                 FOR(i, 0, v.size()-1)
                 {
                 FOR(j, i+1, v.size()-1)
                 {
                 ll p = v[i]*v[j];
                 if(mp[p])
                 {
                 continue;
                 }
                 else
                 {
                 cout<<"no"<<'\n';
                 exit(0);
                 }
                 }
                 }
                 
                 cout<<"yes"<<'\n';
                 */
        }
}