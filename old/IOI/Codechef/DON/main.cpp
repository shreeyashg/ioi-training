#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <unordered_map>
#include <cstring>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define MX 100006 //10^5 + 6
#define MOD 1000000007 // 10^9 + 7
#define INF 1e18+5 // 10^18 + 5

ll nexti[MX], nexte[MX], nextm[MX];
string str;


ll rec(ll x, char ch)
{
        if(x>str.size()-1 && ch=='M')
        {
                return 0;
        }
        
        ll a1=0, a2=0, a3=0;
        
        if(ch=='I')a1 = max(a1,1 + rec(nexti[x], 'I'));
        if(ch=='I')a2 = max(a2,1 + rec(nexte[x], 'E'));
        if(ch=='E')a3 = max(a3,1 + rec(nextm[x], 'M'));
        
        
        
}

int main()
{
        cin>>str;
        ll firsti=-1, firste=-1, firstm=-1;
        ll curr=-1;
        REP(i, str.size())
        {
                if(str[i]=='I')
                {
                        curr = i;
                        firsti = i;
                        break;
                }
        }
        
        FOR(i, 0, curr-1)
        {
                nexti[curr] = firsti;
        }

        
        FOR(i, curr+1, str.size()-1)
        {
                if(str[i] == 'I')
                {
                        FOR(j, curr, i-1)
                        {
                                nexti[j] = i;
                        }
                        nexti[curr] = i;
                        curr = i;
                }
        }
        
        
        
        
        REP(i, str.size())
        {
                if(str[i]=='E')
                {
                        curr = i;
                        firste = i;
                        break;
                }
        }
        
        FOR(i, 0, curr-1)
        {
                nexte[curr] = firste;
        }
        
        FOR(i, curr+1, str.size()-1)
        {
                if(str[i] == 'E')
                {
                        FOR(j, curr, i-1)
                        {
                                nexte[j] = i;
                        }
                        nexte[curr] = i;
                        curr = i;
                        
                }
        }
        
        
        
        
        
        REP(i, str.size())
        {
                if(str[i]=='M')
                {
                        curr = i;
                        firstm = i;
                        break;
                }
        }
        
        FOR(i, 0, curr-1)
        {
                nextm[curr] = firstm;
        }
        
        FOR(i, curr+1, str.size()-1)
        {
                if(str[i] == 'I')
                {
                        FOR(j, curr, i-1)
                        {
                                nextm[j] = i;
                        }
                        nextm[curr] = i;
                        curr = i;
                }
        }
        
        //ll rec(ll x, char ch)
        cout<<rec(firsti, 'I')<<endl;
}