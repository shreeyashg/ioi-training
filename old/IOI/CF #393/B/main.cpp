#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <fstream>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

typedef long long ll;

ll n;
ll arr[103], cnt=0;

int main()
{
        cin>>n;
        REP(i, n) cin>>arr[i];
        REP(i, n)
        {
                if(arr[i])
                        cnt++;
        }
        ll ff=0, f=0;
        REP(i, n)
        {
                if(arr[i]) {
                        ff=i;
                        break;
                }
        }
        REP(i, n)
        {
                if(arr[i])
                {
                        f=i;
                }
        }
        if(cnt==1)
                cout<<1<<endl, exit(0);
        if(cnt==0)
                cout<<0<<endl, exit(0);
        
        ll ans = cnt-1;
        ll prod=1, curr=1;
        FOR(i, ff, f)
        {
                if(arr[i])
                {
                        prod*=curr;
                        curr=1;
                }
                else
                        curr++;
        }
        prod*=curr;
        cout<<prod<<endl;
}