#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <fstream>
#include <set>
#include <cmath>
#include <algorithm>
#include <deque>
#include <bitset>
#include <tuple>
#include <iomanip>
#include <functional>
using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

typedef long long ll;

const ll MX = 2e3 + 3;

ll n;
pair<ll, ll> A; pair<ll, ll> B;
vector<pair<ll, ll> > v;

inline ll dist(pair<ll, ll> x, pair<ll, ll> y)
{
        return ((x.first-y.first)*(x.first-y.first)) + ((x.second-y.second)*(x.second-y.second));
}

int main()
{
        cin>>n>>A.first>>A.second>>B.first>>B.second;
        v.resize(n);
        ll md=0, me=0;
        REP(i, n)
        {
                cin>>v[i].first>>v[i].second;
                if(dist(v[i], A)<dist(v[i], B))
                {
                        md = max(md, dist(v[i], A));
                }
                else
                        me = max(me, dist(v[i], B));
        }
        cout<<md+me<<endl;
}