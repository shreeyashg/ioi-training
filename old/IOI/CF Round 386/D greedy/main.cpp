#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstdio>
#include <cstring>
#include <set>
#include <algorithm>
#include <cmath>
#include <bitset>

using namespace std;

typedef long long ll;

#define FOR(i, a, b) for(ll i=a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define REP(i, n) for(ll i=0; i<n; i++)
#define DEBUG(x) cout<<#x<<" : "<<x<<endl
#define lb(x, s) lower_bound(x.begin(), x.end(), s)
#define pi 3.1415926536

const ll INF = 1e13+ 5;
const ll MX = 1e5 + 3;

ll n, k, a, b;

int main()
{
        cin>>n>>k>>a>>b;
        ll ak=0, bk=0;
        string str;
        REP(i, n)
        {
                if(a<0 || b<0 || ak>k || bk>k)
                        cout<<"NO"<<endl, exit(0);
                if(ak==k)
                {
                        b--;
                        str+='B';
                        bk++;
                        ak=0;
                }
                else if(bk==k)
                {
                        a--;
                        str+='G';
                        ak++;
                        bk=0;
                }
                else if(a>b)
                {
                        a--;
                        str+='G';
                        ak++;
                        bk=0;
                }
                else
                {
                        b--;
                        str+='B';
                        bk++;
                        ak=0;
                }
        }
        if(str.size()<n || a<0 || b<0)
                cout<<"NO"<<endl;
        else
                cout<<str<<endl;
}