#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstdio>
#include <cstring>
#include <set>
#include <algorithm>
#include <cmath>
#include <bitset>

using namespace std;

typedef long long ll;

#define FOR(i, a, b) for(ll i=a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define REP(i, n) for(ll i=0; i<n; i++)
#define DEBUG(x) cout<<#x<<" : "<<x<<endl
#define lb(x, s) lower_bound(x.begin(), x.end(), s)
#define pi 3.1415926536

const ll INF = 1e18+ 5;
const ll MX = 1e5 + 3;

ll n, m;
vector<ll> v;
set<ll> even, odd;
set<ll> dups;
map<ll, ll> mp;

int main()
{
        cin>>n>>m;
        REP(i, n)
        {
                ll k;
                cin>>k;
                v.push_back(k);
                mp[k]++;
                if(mp[k]>1)
                        dups.insert(k);
                if(k%2)odd.insert(k);
                else even.insert(k);
        }
        
        cout<<dups+abs(odd-even)/2<<endl;
}