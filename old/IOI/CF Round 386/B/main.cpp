#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstdio>
#include <cstring>
#include <set>
#include <algorithm>
#include <cmath>
#include <bitset>

using namespace std;

typedef long long ll;

#define FOR(i, a, b) for(ll i=a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define REP(i, n) for(ll i=0; i<n; i++)
#define DEBUG(x) cout<<#x<<" : "<<x<<endl
#define lb(x, s) lower_bound(x.begin(), x.end(), s)
#define pi 3.1415926536

const ll INF = 1e13+ 5;

ll n;
string str;
string newstr;

int main()
{
        cin>>n;
        cin>>str;
        
        
        
        newstr = str;
        ll up = str.size()-2;
        ll down = 0;
        ll k = str.size()-2;
        if(str.size()%2)
        {
                while(k>=0)
                {
                        if(k%2==0)
                        {
                                newstr[up] = str[k];
                                up--;
                        }
                        else
                                newstr[down] = str[k], down++;
                        k--;
                }
        }
        else
        {
                ll up = str.size()-2;
                ll down = 0;
                ll k = str.size()-2;
                while(k>=0)
                {
                        if(k%2)
                        {
                                newstr[up] = str[k];
                                up--;
                        }
                        else
                                newstr[down] = str[k], down++;
                        k--;
                }
                
        }
        cout<<newstr<<endl;
}