#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstdio>
#include <cstring>
#include <set>
#include <algorithm>
#include <cmath>
#include <bitset>

using namespace std;

typedef long long ll;

#define FOR(i, a, b) for(ll i=a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define REP(i, n) for(ll i=0; i<n; i++)
#define DEBUG(x) cout<<#x<<" : "<<x<<endl
#define lb(x, s) lower_bound(x.begin(), x.end(), s)
#define pi 3.1415926536

const ll INF = 1e13+ 5;
const ll MX = 1e5 + 3;

ll n, k, a, b;
ll choice[MX];

bool rec(ll A, ll B, ll cont, bool type)
{
        if(A==0 && B==0 && cont<=k)
                return 1;
        if(cont>k || A<0 || B<0)
                return 0;
        bool ans=0;
        if(type)
        {
                if(rec(A, B-1, cont+1, type))
                        choice[n-A-B] = 1;
                else if(rec(A-1, B, 1, !type))
                        choice[n-A-B] = 0;
                else
                        return 0;
                ans = max(rec(A, B-1, cont+1, type),rec(A-1, B, 1, !type));
        }
        else
        {
                if(rec(A-1, B, cont+1, type))
                {
                        choice[n-A-B] = 0;
                }
                else if(rec(A, B-1, 1, !type))
                {
                        choice[n-A-B] = 1;
                }
                else
                        return 0;
                ans = max(rec(A-1, B, cont+1, type), rec(A, B-1, 1, !type));
        }
        return ans;
}

int main()
{
        memset(choice, -1, sizeof(choice));
        cin>>n>>k>>a>>b;
        if(rec(a, b, 0, 0) || rec(a, b, 0, 1))
        {
                REP(i, n)
                {
                        if(choice[i])
                                cout<<'B';
                        else
                                cout<<'G';
                }
                cout<<endl;
        }
        else
                cout<<"NO"<<endl;
}