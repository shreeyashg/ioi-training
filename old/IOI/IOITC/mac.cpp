#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, MX = 1e4 + 4, INF = 1e18;

ll n, k, m[MX], p[MX], dp[MX][104][2];

ll rec(ll idx, ll cons, bool mac)
{
	if(idx == n+1)
		return 0;
	if(cons > k)
		return INF;
	if(dp[idx][cons][mac]!=-1)
		return dp[idx][cons][mac];
	ll ans = INF;

	ll dk = 0;
	if(mac)
		dk = m[idx];
	else
		dk = p[idx];
	ans = min(ans, dk + rec(idx + 1, cons + 1, mac));
	ans = min(ans, dk + rec(idx + 1, 1, !mac));
	return dp[idx][cons][mac] = ans;
}

int main()
{
	memset(dp, -1, sizeof(dp));
	cin >> n >> k;
	REP(i, n) cin >> m[i+1];
	REP(i, n) cin >> p[i+1];

	cout << min(rec(1, 1, 1), rec(1, 1, 0)) << endl;
}