#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

struct edge
{
        ll q, l, x, y;
};


const ll MX = 5e3 + 4, INF = 1e18 + 4;
ll arr[MX], sz[MX];

vector<edge> v;

bool cmp(edge a, edge b)
{
        if(a.q==b.q)
                return a.l<b.l;
        return a.q>b.q;
}


inline ll root(ll x)
{
        while(x!=arr[x])x=arr[arr[x]];
        return x;
}

inline bool find(ll x, ll y)
{
        return root(x)==root(y);
}

void uni(ll x, ll y)
{
        x = root(x);
        y = root(y);
        if(x==y)
                return;
        if(sz[x]<sz[y]) arr[x] = arr[y], sz[y]+=sz[x];
        else arr[y] = arr[x], sz[x]+=sz[y];
}



ll n, m;

int main()
{
        ll done=0;
        cin>>n>>m;
        FOR(i, 1, n) arr[i]=i;
        REP(i, m)
        {
                edge x;
                cin>>x.x>>x.y>>x.q>>x.l;
                v.push_back(x);
        }
        ll qq=INF, len=0;
        sort(v.begin(), v.end(), cmp);
        REP(i, v.size())
        {
               // cout<<v[i].q<<" "<<v[i].l<<" "<<v[i].x<<" "<<v[i].y<<endl;
                if(!find(v[i].x, v[i].y))
                {
                        done++;
                        uni(v[i].x, v[i].y);
                        qq=min(qq, v[i].q);
                        len+=v[i].l;
                }
        }
        if(done==n-1)cout<<qq<<" "<<len<<endl;
        else
                cout<<-1<<endl;
}