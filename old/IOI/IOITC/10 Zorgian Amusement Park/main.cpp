#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;
vector<ll> v[MX];
ll n, h[MX], t[MX], l[MX], curr, P[MX][19], maxdp[MX][19], mdpidx[MX][19], ans, len[MX];



void setpar(ll idx, ll par)
{

        REP(i, v[idx].size())
        {
                if(v[idx][i]==par)
                        continue;
                //DEBUG(v[idx][i]);
                t[v[idx][i]] = idx;
                l[v[idx][i]] = l[idx] + 1;
                setpar(v[idx][i], idx);
        }
}

void init()
{
        memset(P, -1, sizeof(P));
        memset(maxdp, -1, sizeof(maxdp));
        FOR(i, 1, n)
        {
                if(i!=1)P[i][0] = t[i];
                if(i!=1)maxdp[i][0] = max(0ll, h[t[i]]);
                if(i!=1)
                {
                        mdpidx[i][0] = t[i];
                }
        }
        
        for(ll j=1; (1<<j)<n; j++)
        {
                FOR(i, 1, n)
                {
                        if(P[i][j-1]!=-1)P[i][j]  = P[P[i][j-1]][j-1];
                        if(P[i][j]!=-1)
                        {
                                maxdp[i][j] =  max(maxdp[P[i][j-1]][j-1], maxdp[i][j-1]);
                                if(maxdp[P[i][j-1]][j-1] > maxdp[i][j-1])
                                {
                                        mdpidx[i][j] = mdpidx[P[i][j-1]][j-1];
                                }
                                else
                                        mdpidx[i][j] = mdpidx[i][j-1];
                        }
                
                }
        }
}

/*
void precompute()
{
        for (int j = 1; j < ln_N; ++j)
                for (int i = 1; i <= N; ++i)
                        dp[j][i] = {0, -1};
        
        for (int j = 1; (1 << j) < N; ++j)
                for (int i = 1; i <= N; ++i)
                        if (dp[j - 1][i].next_val != -1)
                        {
                                int nv = dp[j - 1][i].next_val;
                                if (dp[j - 1][nv].next_val == -1)
                                {
                                        dp[j][i] = dp[j - 1][nv];
                                }
                                else
                                {
                                        dp[j][i] =
                                        {
                                                max(dp[j - 1][i].max, dp[j - 1][nv].max),
                                                dp[j - 1][nv].next_val
                                        };
                                }
                        }
}


void solve(int root)
{
        vis[root] = 1;
        int j = 0;
        for (; (1 << j) < N; ++j)
                ;
        --j;
        
        int curr = root;
        for (int i = j; i >= 0; --i)
                if (dp[i][curr].next_val != -1 && dp[i][curr].max < H[root])
                        curr = dp[i][curr].next_val;
        
        curr = dp[0][curr].next_val;
        
        int par = dp[0][root].next_val;
        if (par == -1)
        {
                len[root] = 0;
        }
        else if (curr == -1)
        {
                len[root] = 0;
                ans += (len[par] + 1);
        }
        else
        {
                len[root] = len[curr] + (H[curr] > H[root]);
                ans += (len[par] + 1 - len[curr]);
        }
        
        for (auto &v : E[root])
                if (!vis[v])
                        solve(v);
}
 */

void DFS(ll idx, ll p)
{
        ll curr = idx;
        ll first = t[idx];
        ll j=0;
        for(; maxdp[curr][j]!=-1; j++)
                ;
        j--;
        
        for(ll i=j; i>=0; i--)
        {
                if(mdpidx[curr][i]==first)
                        curr = P[curr][i];
                
        }
        //if(mdpidx[curr][i])
        
        REP(i, v[idx].size())
        {
                if(v[idx][i]==p)
                        continue;
                DFS(v[idx][i], idx);
        }
        
        
}

int main()
{
       // cout<<223653<<endl;
        ios::sync_with_stdio(false);
        cin.tie(0);
        cout.tie(0);
        l[1]=1;
        cin>>n;
        REP(i, n-1)
        {
                ll a, b;
                cin>>a>>b;
                v[a].push_back(b);
                v[b].push_back(a);
        }

        REP(i, n)
        {
                cin>>h[i+1];
        }

        setpar(1, 0);
        init();
        
        /*
        
         First ancestor whose number is = mdpidx.
         Start with the farthest
         If max > first
         answer b/w curr to 2^k th ancestor of curr.
         else
         answer is 0.
         */
        
        ll idx = 4;
        ll curr = idx;
        ll first = idx;
        ll j=0;
        for(; maxdp[curr][j]!=-1; j++)
                ;
        j--;
        
        for(ll i=j; i>=0; i--)
        {
                if(mdpidx[curr][i]==-1)
                        continue;
                if(mdpidx[curr][i]==t[idx])
                {
                        curr = P[curr][i];
                }
        }
        
        cout<<curr<<endl;
        //DFS(1, 0);
        /*
        ll ans=0;
        FOR(i, 1, n)
        {
                FOR(j, 0, 10)
                {
                        if(P[i][j]!=-1)
                                cout<<i<<" "<<j<<" "<<P[i][j]<<endl;
                }
              //  ans += len[i];
        } */
        //cout<<ans<<endl;
}

/*
 
 5
 1 2
 3 4
 2 3
 2 5
 5
 1
 3
 2
 9

 */
