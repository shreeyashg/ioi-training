#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

ll curr=1, m = 1e8 + 7;

vector<string>words = {"abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};

pair<ll, ll> calc(char ch)
{
        REP(i, words.size())
        {
                REP(j, words[i].size())
                {
                        if(words[i][j] == ch)
                        {
                                return {i+1, j+1};
                        }
                }
        }
        
        return {0,0};
}


ll modulo(ll a, ll b)
{
        if(b==0)
                return 1;
        if(b==1)
                return a%m;
        else
        {
                if(b%2==0)
                {
                        ll ans=((modulo(a, b/2)%m) * (modulo(a, b/2)%m))%m;
                        return ans;
                }
                else
                {
                        ll ans=((modulo(a, (b-1)/2)%m)*(modulo(a, (b-1)/2)%m))%m;
                        ans*=a;
                        ans%=m;
                        return ans;
                }
        }
}


int main()
{
        ll t;
        cin>>t;
        while(t--)
        {
                curr=1;
                string str;
                cin>>str;
                ll past=-1;
                ll curr=1;
                pair<ll, ll> gg=calc(str[0]);
                gg.second=0;
                REP(i, str.size())
                {
                      //  cout<<gg.first<<" "<<gg.second<<endl;
                        auto x = calc(str[i]);
                        if(x.first==gg.first)
                        {
                                gg.second += x.second;
                        }
                        else
                        {
                                gg.first = x.first;
                                curr *= modulo(2, gg.second-1);
                                curr %= m;
                                gg.second = x.second;
                        }
                }
                if(gg.second)
                        curr*= modulo(2, gg.second-1), curr%=m;
                cout<<curr<<endl;
        }
}