#pragma GCC optimize("O3")
#include <iostream>
#include <vector>
using namespace std;

typedef int ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

typedef vector<vector<ll> > mx;

const ll MOD = 1e9 + 7;
ll K, q, n, m;

mx mul(mx a, mx b)
{
        mx c(m+1, vector<ll> (m+1));
        FOR(i, 1, m) FOR(j, 1, i) FOR(k, 1, m)
        {
                c[i][j] = (c[i][j] + (a[i][k] * b[k][j])%MOD) % MOD;
        }
        FOR(i, 1, m)
        {
                FOR(j, 1, i-1)
                {
                        c[j][i] = c[i][j];
                }
        }
        return c;
}


mx pow(mx a, ll x)
{
        if(x==1)
                return a;
        if(x%2)
                return mul(a, pow(a, x-1));
        mx X = pow(a, x/2);
        return mul(X, X);
}

int main()
{
        ios::sync_with_stdio(false);
        cin.tie(0);
        cout.tie(0);
        cin>>q;
        REP(i, q)
        {
                ll ans=0;
                cin>>n>>m;
                mx odd(m+1, vector<ll>(m+1));
                mx even(m+1, vector<ll>(m+1));
                FOR(i, 1, m)
                {
                        FOR(j, i+1, m)
                        {
                                odd[i][j] = 1;
                        }
                        FORD(j, i-1, 1)
                        {
                                even[i][j] = 1;
                        }
                }
                n--;
                mx T = mul(odd,even);
                mx X;
                if(n==0)
                {
                        cout<<m<<endl;
                        continue;
                }
                if(n==1)
                {
                        ll s=0;
                        FOR(i, 1, m)
                        {
                                FOR(j, 1, m)
                                {
                                        s+=odd[i][j];
                                        s%=MOD;
                                }
                        }
                        cout<<s<<endl;
                        continue;
                }
                else
                {
                        X = pow(T, (n)/2);
                }
                
                FOR(i, 1, m)
                {
                    //    cout<<n<<" "<<i<<" = "<<f(n,i)<<endl;
                        FOR(j, 1, m)
                        {
                                ans = (ans + X[i][j]) %MOD;
                        }
                }
                cout<<ans%MOD<<endl;
        }
}