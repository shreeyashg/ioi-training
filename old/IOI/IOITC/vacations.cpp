#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 3e4 + 5;

set<ll> s[MX*4];
ll n, q, arr[MX];
set<ll> E;

void build(ll x, ll y, ll idx)
{
	if(x == y)
	{
		s[idx].insert(arr[x]);
		return;
	}

	build(x, (x+y)/2, 2*idx);
	build((x+y)/2 + 1, y, 2*idx + 1);

	s[idx].insert(s[2*idx].begin(), s[2*idx].end());
	s[idx].insert(s[2*idx+1].begin(), s[2*idx+1].end());
}

set<ll> query(ll x, ll y, ll X, ll Y, ll idx)
{
	if(X>y || Y<x)
		return E;
	if(x>=X && y<=Y)
		return s[idx];
	set<ll> r;
	if(!(X>(x+y)/2 || Y<x))r.insert(query(x, (x+y)/2, X, Y, 2*idx).begin(), query(x, (x+y)/2, X, Y, 2*idx).end());
	if(!(X>y || Y<(x+y)/2+1))r.insert(query((x+y)/2 + 1, y, X, Y, 2*idx+1).begin(), query((x+y)/2 + 1, y, X, Y, 2*idx+1).end());

	return r;
}

int main()
{
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout.tie(0);
	/*set<ll> A = {1, 2, 4, 5};
	set<ll> B = {2, 4, 1, 3};
	set<ll> C;
	C.insert(A.begin(), A.end());
	C.insert(B.begin(), B.end());*/

	//cout << C.size() << endl;

	cin >> n;
	REP(i, n) cin >> arr[i+1];
	build(1, n, 1);
	cin >> q;
	REP(i, q)
	{
		ll x, y;
		cin >> x >> y;
		set<ll> m = query(1, n, x, y, 1);
		cout << m.size() << endl;
	}
}