#!/bin/bash
cd /Users/shree/desktop/IOI/IOITC
g++ -std=c++11 -O3 152\ Monster.cpp

#two styles, comment out the one you don't need.

#use this if files are like
#in0.txt, out0.txt, etc.
#change 0/10 to whatever lower/upper bound is
#ac = 0 wa = 0
x = 26

for i in {0..$x}
do
    #echo "turnir.in.${i}"
    
    ./a.out < "/Users/shree/Desktop/IOI/IOITC2015AllProblems/Finals/TSTDay2/monsterChallenge/testdata/MonsterChallenge_data/input${i}.in" > temp.txt ;
    
    if diff -B "/Users/shree/Desktop/IOI/IOITC2015AllProblems/Finals/TSTDay2/monsterChallenge/testdata/MonsterChallenge_data/output${i}.out" "temp.txt" > ch.txt; then
    	 echo "Case ${i}: Correct Answer"
    	 ((ac++))
	else
 		 echo "Case ${i}: Wrong Answer"
 		 ((wa++))
         echo "Input :"
         cat "/Users/shree/Desktop/IOI/IOITC2015AllProblems/Finals/TSTDay2/monsterChallenge/testdata/MonsterChallenge_data/input${i}.in"
         exit 1
    cat ch.txt
	rm temp.txt
	#echo "ac = $ac | wa = $wa"
	fi
done

if [($x -eq $ac)]; then
    echo "All test cases correct."
else
    echo "$wa cases incorrect."

#echo "ac = $ac | wa = $wa"

#use this if files are like
#x.in, x.out (where x is any string)
#you can even change .in/.out to any file
#extension, as long as they are different.
#for i in *.in
#do
#       time ./a.out < $i > temp.txt
#        diff temp.txt ${i%.in}.out
#        rm temp.txt
#done

