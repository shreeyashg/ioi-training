#!/bin/bash
cd /Users/shree/desktop/IOI/IOITC
g++ -std=c++11 -O3 Microscope.cpp

#two styles, comment out the one you don't need.

#use this if files are like
#in0.txt, out0.txt, etc.
#change 0/10 to whatever lower/upper bound is
#ac = 0 wa = 0
echo ""

./a.out < 'sample.in' > temp.txt ;
if diff -B -b -w --strip-trailing-cr "sample.out" "temp.txt" > ch.txt; then
    echo "Sample Correct."
else
    echo "Wrong Answer on sample test case 1."
    echo "Sample input:"
    cat 'sample.in'
    cat ch.txt

    exit 1
fi

rm ch.txt
rm temp.txt

for i in {0..19}
do
    #echo "turnir.in.${i}"
    
    ./a.out < "/Users/shree/Desktop/IOI/IOITC 2012/tests/test1-bio/data/${i}.in" > temp.txt ;
    
    if diff -B -b -w --strip-trailing-cr "/Users/shree/Desktop/IOI/IOITC 2012/tests/test1-bio/data/${i}.out" "temp.txt" > ch.txt; then
    	 echo "Case ${i}: Correct Answer"
    	 ((ac++))
	else
 		 echo "Case ${i}: Wrong Answer"
 		 ((wa++))
         #echo "Input :"
         #cat "/Users/shree/Desktop/IOI/IOITC 2012/tests/test1-bio/data/${i}.in"
         echo ""
         exit 1
    cat ch.txt
	rm temp.txt
    rm ch.txt
	#echo "ac = $ac | wa = $wa"
	fi
    echo ""
done


#echo "ac = $ac | wa = $wa"

#use this if files are like
#x.in, x.out (where x is any string)
#you can even change .in/.out to any file
#extension, as long as they are different.
#for i in *.in
#do
#       time ./a.out < $i > temp.txt
#        diff temp.txt ${i%.in}.out
#        rm temp.txt
#done

