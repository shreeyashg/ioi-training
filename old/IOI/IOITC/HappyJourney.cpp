#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 5e5;

ll n, m, x, y;
vector<pair<ll, char> >v[MX];
vector<string> vec;
string infi(MX, 'z');
bool on[MX];
string dp[MX];

string ls(string A, string B)
{
	if(A.size() < B.size())
		return A;
	else if(A.size() > B.size())
		return B;
	return (A<B)?A:B;
}

string go(ll xx)
{
	if(xx == y || on[xx])
		return "";
	string x = infi;
	REP(i, v[xx].size())
	{
		x = ls(x, go(v[xx][i].first) + v[xx][i].second);
	}
	on[xx] = 1;
	return dp[xx] = x;
}


int main()
{
	//cout << infi << endl;
	//cout << ls("Helz", "Hell") << endl;
	vec.resize(MX);
	cin >> n >> m;
	cin >> x >> y;

	REP(i, m)
	{
		ll a, b;
		char x;
		cin >> a >> x >> b;
		v[a].push_back({b,x});
		v[b].push_back({a,x});
	}

	cout << go(x) << endl;
}