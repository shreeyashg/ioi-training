#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i=a; i<=b; i++)
#define FORD(i, a, b) for(ll i=a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl

typedef long long ll;

const ll MX = 1e5;
const ll MY = 5*1e2;
const ll INF = 1e15;

vector<pair<ll, ll> >v;
ll before[MX+1], nnext[MX+1];
vector<ll> vv[MX+1];

int main()
{
        ll n;
        cin>>n;
        REP(i, n)
        {
                ll k, f;
                cin>>k>>f;
                if(f==0 || f==500 || k==0 || k==MX)
                        continue;
                v.push_back({k,f});
                vv[k].push_back(f);
        }
        v.push_back({MX, 500});
        v.push_back({0, 0});
        
        vv[MX].push_back(500);
        vv[0].push_back(0);
        
        sort(v.begin(), v.end());
        ll area=0;
        
        ll firstx = v[0].first;
        ll lastx = v[v.size()-1].first;
        //DEBUG(firstx);
        FOR(i, 0, firstx-1)
        {
                before[i] = 0;
        }
        
        FOR(i, firstx, MX)
        {
                if(!vv[i].empty() && vv[i][0])firstx = i,before[i] = firstx;
                else before[i] = firstx;
        }
        
       // DEBUG(lastx);
       // DEBUG(vv[0][0]);
        nnext[lastx] = (ll)1e5;
        FORD(i, lastx, 0)
        {
                
        }
        FORD(i, lastx-1, 0)
        {
                if(lastx==v[1].first)
                        break;
                if(!vv[i].empty() && vv[i][0])
                        lastx = i, nnext[i] = lastx;
                else
                        nnext[i] = lastx;
        }
        nnext[MX] = MX;
        before[0] = 0;
        ll ans=0;
        FOR(i, 0, MX)
        {
                if(!vv[i].empty())
                {
                        ans = max(ans, vv[i][0]*(nnext[i]-before[i]));
                }
                else
                {
                        ans = max(ans, 500*(nnext[i]-before[i]));
                }
        }
        
        cout<<ans<<endl;
}