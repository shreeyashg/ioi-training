#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

typedef long long ll;
#define REP(i, n) for(ll i=0; i<n; i++)

vector<ll> v;
unordered_map<ll, ll> mp;

int main()
{
        ll n;
        cin>>n;
        if(n<=2)
        {
                cout<<n<<endl;
                exit(0);
        }
        REP(i, n)
        {
                ll k;
                cin>>k;
                v.push_back(k);
                mp[k]++;
        }
        sort(v.begin(), v.end());
        ll ans = 2;
        for(ll i=0; i<n; i++)
        {
                for(ll j=i+1; j<n; j++)
                {
                        ll curr=2;
                        ll a=v[i];
                        ll d=v[j]-v[i];
                        ll k=2;
                        ll x = a+(k*d);
                        while(mp[x])
                        {
                                curr++;
                                k++;
                                x = a+(k*d);
                        }
                        ans = max(ans, curr);
                        curr=0;
                }
        }
        
        cout<<ans<<endl;
}