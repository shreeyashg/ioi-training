#include <iostream>
#include <vector>
#include <queue>
#include <algorithm>
#include <cstring>

using namespace std;
typedef long long ll;

const ll MX = 511, INF = 1e15;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i=a; i<=b; i++)

ll arr[MX][MX], good[MX][MX], vis[MX][MX], dp[MX][MX];
ll n, k, x, y, c;
/*
void populate(ll a, ll b)
{
        //cout<<a<<" "<<b<<endl;
        if(a>n-1 || b>n-1 || a<0 || b<0)
                return;
        if(vis[a][b])
                return;
        
        vis[a][b]=1;
        
        if((abs(x-a)+abs(y-b))<=c)
                good[a][b]=1;
        else
                return;
        
        populate(a, b+1);
        populate(a+1, b);
        populate(a-1, b);
        populate(a, b-1);
}

void populate2(ll a, ll b)
{
        queue<pair<ll, ll>> q;
        q.push({x, y});
        while(!q.empty())
        {
                ll a = q.front().first;
                ll b = q.front().second;
                q.pop();
                if(a>n-1 || b>n-1 || a<0 || b<0)
                        continue;
                if(vis[a][b] || good[a][b])
                        continue;
                
                vis[a][b]=1;
                
                if(abs(x-a)+abs(y-b) <= c)
                        good[a][b]=1;
                q.push({a, b+1});
                q.push({a+1, b});
                q.push({a-1, b});
                q.push({a, b-1});
        }
}

void print()
{
        REP(i, n)
        {
                REP(j, n)
                {
                        if(good[i][j]) cout<<'X';
                        else cout<<'.';
                }
                cout<<'\n';
        }
        
        cout<<'\n';
}
*/
ll rec(ll a, ll b)
{
        if(dp[a][b]!=-INF)
                return dp[a][b];
        //cout<<a<<" "<<b<<'\n';
        if(a==n-1 && b==n-1)
                return dp[a][b] = arr[n-1][n-1];
        if(a>n-1 || b>n-1 || !good[a][b])
        {
                if(!good[a][b])return dp[a][b] = -INF;
                else return -INF;
        }
        
        return dp[a][b] = arr[a][b] + max(rec(a, b+1), rec(a+1, b));
}

int main()
{
        ios::sync_with_stdio(false);
        cin.tie(0);
        memset(good, 0, sizeof(good));
        
        cin>>n>>k;
        REP(i, n)
        {
                REP(j, n)
                {
                        cin>>arr[i][j];
                }
        }
        
        REP(i, k)
        {
                //ll x,y;
                //memset(vis, 0, sizeof(vis));
                cin>>x>>y>>c;
                x-=1;
                y-=1;
                //populate2(x,y);
                FOR(i, x-c, x+c)
                {
                        FOR(j, y-c, y+c)
                        {
                                if(abs(x-i)+abs(y-j)<=c && i>=0 && i<n & j>=0 && j<n)
                                {
                                        good[i][j]=1;
                                }
                        }
                }
        }
        
    /*    REP(i, n)
        {
                REP(j, n)
                {
                        if(good[i][j])
                                cout<<arr[i][j]<<" ";
                        else
                                cout<<'.'<<" ";
                }
                cout<<'\n';
        }
        
     */
      
        REP(i, MX)
        {
                REP(j, MX)
                {
                        dp[i][j] = -INF;
                }
        }
        
        //cout<<"WA"<<'\n';
      
        if(rec(0, 0)<=-INF)
        {
                cout<<"NO"<<'\n';
                return 0;
        }
        else
        {
                cout<<"YES"<<'\n';
                cout<<rec(0,0)<<'\n';
        }

       
              /*  REP(i, n)
                {
                        REP(j, n)
                        {
                                cout<<dp[i][j]<<" ";
                        }
                        cout<<'\n';
                }*/
                
        
        
        /*REP(i, n)
        {
                REP(j, n)
                {
                        if(good[i][j])
                        {
                                cout<<'X';
                        }
                        else
                                cout<<'.';
                }
                cout<<'\n';
        }*/
}