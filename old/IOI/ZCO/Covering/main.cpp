#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include <map>

typedef long long ll;
using namespace std;

#define FOR(i, a, b) for(ll i=a; i<=b; i++)

ll n, ans;
vector<pair<ll, ll> > v;
map<pair<ll, ll>, bool> mp;

bool overlap(pair<ll, ll> x, pair<ll, ll> y)
{
        if(x.first>=y.first && x.second<=y.second)
        {
                return true;
        }
        if(y.first>=x.first && y.second<=x.second)
        {
                return true;
        }
        if(x.second>=y.first && x.second<=y.second)
        {
                return true;
        }
        if(x.first<=y.second && x.first>=y.first)
        {
                return true;
        }
        return false;
}

bool cmp(pair<ll, ll> a, pair<ll, ll> b)
{
        return a.second<b.second;
}

int main()
{
        cin>>n;
        ans = n;
        FOR(i, 0, n-1)
        {
                ll a, b;
                cin>>a>>b;
                v.push_back({a,b});
        }
        
        sort(v.begin(), v.end(), cmp);
        vector<ll> picked;
        ll ans=1;
        picked.push_back(v[0].second);
        FOR(i, 1, v.size()-1)
        {
                //FOR(j, 0, i-1)
                {
                        if(picked[picked.size()-1]<v[i].first)
                        {
                                ans++;
                                picked.push_back(v[i].second);
                        }
                }
        }
        cout<<ans<<endl;
}