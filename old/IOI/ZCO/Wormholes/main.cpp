#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;;
typedef long long ll;
#define REP(i, n) for(ll i=0; i<n; i++)

ll n, x, y;
vector<pair<ll, ll> >v;
vector<ll> X,Y;

int main()
{
        cin>>n>>x>>y;
        REP(i, n)
        {
                ll a, b;
                cin>>a>>b;
                v.push_back({a,b});
        }
        
        sort(v.begin(), v.end());
        REP(i, x)
        {
                ll f;
                cin>>f;
                X.push_back(f);
        }
        REP(i, y)
        {
                ll f;
                cin>>f;
                Y.push_back(f);
        }
        
        sort(X.begin(), X.end());
        sort(Y.begin(), Y.end());
        
        ll ans=99999999999;
        
        REP(i, v.size())
        {
                ll a = v[i].first, b = v[i].second;
                auto it1 = upper_bound(X.begin(), X.end(), a)-1;
                auto it2 = lower_bound(Y.begin(), Y.end(), b);
                if(*it2>=*it1)ans = min(ans, *it2-*it1+1);
        }
        
        cout<<ans<<endl;
}