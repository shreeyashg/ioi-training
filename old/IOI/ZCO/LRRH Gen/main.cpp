//
//  main.cpp
//  LRRH Gen
//
//  Created by Shreeyash Gotmare on 19/10/16.
//  Copyright © 2016 Shreeyash Gotmare. All rights reserved.
//

#include <iostream>
typedef long long ll;
using namespace std;
#define REP(i, n) for(ll  i= 0; i<n; i++)
ll arr[501][501];
int main(int argc, const char * argv[])
{
        ll n = 500, c = 10000, k=11;
        REP(i, n)
        {
                REP(j, n)
                {
                        arr[i][j] = rand()%2000;
                        if(i>1 && j>1)arr[i][j] -= 1000;
                }
        }
        
        cout<<n<<" "<<c<<endl;
        REP(i, n)
        {
                REP(j, n)
                {
                        cout<<arr[i][j]<<" ";
                }
                cout<<'\n';
        }
        
        REP(i, c)
        {
                cout<<rand()%500<<" "<<rand()%500<<" "<<(k%10) + 1<<'\n';
        }
}
