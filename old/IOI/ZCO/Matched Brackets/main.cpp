#include <iostream>
#include  <vector>
#include <stack>
using namespace std;

typedef long long ll;
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define REP(i, n) for(ll i=0; i<n; i++)

ll n;
vector<ll> v;

int main()
{
        cin>>n;
        FOR(i, 0, n-1)
        {
                ll k;
                cin>>k;
                v.push_back(k);
        }
        
        stack<ll> s;
        ll maxdepth=0, mdp=0;
        REP(i, v.size())
        {
                if(v[i]==1)
                {
                        s.push(v[i]);
                        if(s.size()>maxdepth)
                        {
                                maxdepth = s.size();
                                mdp=i+1;
                        }
                        
                }
                else
                {
                        s.pop();
                }
        }
        ll t=0;
        stack<pair<ll, ll> >st;
        pair<ll, ll> f;
        ll ans=0;
        ll a, b;
        REP(i, v.size())
        {
                t++;
                if(v[i]==1)
                {
                        st.push(make_pair(i+1, t));
                }
                else
                {
                        auto x = st.top();
                        st.pop();
                        if(t-x.second>ans)
                        {
                                ans = t-x.second;
                                b = x.first;
                        }
                }
        }
        
        cout<<maxdepth<<" "<<mdp<<" "<<ans+1<<" "<<b<<endl;
}