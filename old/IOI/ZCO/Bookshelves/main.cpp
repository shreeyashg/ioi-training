#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <cstring>
#include <set>
#include <algorithm>
#include <deque>
using namespace std;

#define ll long long
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define NMAX 1000009
#define INF 1000009*1e9

ll a1=INF, a2=INF;

ll solve(vector<ll> a, vector<ll> b, ll k)
{
        priority_queue<ll> f;//Max
        priority_queue<ll, vector<ll> , greater<ll> > s;//Min
        
        REP(i, a.size()) f.push(a[i]);
        REP(i, b.size()) s.push(b[i]);
        
        while(k--)
        {
                ll p = f.top();
                ll q = s.top();
                f.pop(), s.pop();
                f.push(q);
                s.push(p);
        }
        ll ax=s.top();
        while(!s.empty())
        {
                ax=s.top();
                s.pop();
        }
        
        return ax + f.top();
}

int main()
{
        vector<ll> v1, v2;
        ll n, k;
        cin>>n>>k;
        REP(i, n)
        {
                ll k;
                cin>>k;
                v1.push_back(k);
        }
        REP(i, n)
        {
                ll k;
                cin>>k;
                v2.push_back(k);
        }
        a1 = solve(v1, v2, k);
        a2 = solve(v2, v1, k);
        cout<<min(a1, a2)<<endl;
}