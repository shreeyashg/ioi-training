#include <iostream>
#include <vector>
#include <stack>
using namespace std;

typedef long long  ll;
#define REP(i, n) for(ll i=0 ; i<n; i++)

vector<ll> v;
stack<char> s;

int main()
{
        ll n;
        cin>>n;
        REP(i, n)
        {
                ll k;
                cin>>k;
                v.push_back(k);
        }
        
        ll curly=0, circ=0;
        ll alt=0;
        
        ll t=0, t1=0, t2=0, t01=0, t02=0;
        
        ll num=0, num2=0;
        REP(i, v.size())
        {
                t++;
                if(v[i]==1 || v[i]==3)
                {
                        if(v[i]==1){s.push('(');if(num==0)t1=t;num++;}
                        else {s.push('{');if(num2==0)t01=t;num2++;}
                }
                else
                {
                        if(v[i]==2)
                        {
                                num--;
                                if(num==0)
                                        circ = max(circ, t-t1+1);
                                s.pop();
                        }
                        if(v[i]==4)
                        {
                                num2--;
                                if(num2==0)
                                        curly = max(curly, t-t01+1);
                                s.pop();
                        }
                }
        }
        
        stack<ll> st;
        ll cd=0, ct=0, a, b;
        REP(i, v.size())
        {
                if(v[i]==1)
                {
                        if(s.empty() || s.top()!=1)
                                ct++;
                        a++;
                        s.push(1);
                }
                if(v[i]==3)
                {
                        if(s.empty()||s.top()!=2)
                                ct++;
                        b++;
                        s.push(2);
                }
                if(v[i]==2 || v[i]==4)
                {
                        if(v[i]==2)
                                a--;
                        else
                                b--;
                        s.pop();
                        if(s.empty()||s.top()!=(v[i]/2))
                                ct--;
                }
                alt = max(alt, ct);
        }
        
        cout<<alt<<" "<<circ<<" "<<curly<<endl;
}