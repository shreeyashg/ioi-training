//
//  main.cpp
//  Tournament
//
//  Created by Shreeyash Gotmare on 22/10/16.
//  Copyright © 2016 Shreeyash Gotmare. All rights reserved.
//

#include <iostream>
#include <map>

using namespace std;

map<int, int> mp;

int main()
{
        long long n;
        cin>>n;
        for(long long i=0; i<n; i++)
        {
                long long k;
                cin>>k;
                mp[k]++;
        }
        long long ans=0;
        for(int i=1; i<=1000; i++)
        {
                for(int j=i+1; j<=1000; j++)
                {
                        ans+=mp[i]*(j-i)*mp[j];
                }
        }
        
        cout<<ans<<endl;
}