#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

ll n;
string getter;

int main()
{
	cin>>n;
	vector<string> v;

	for(int i = 0; i <= n; i++)
	{
		vector<string> v;
		getline(cin, getter);
		if(i==0)continue;
		string buf = "";
		for(int i = 0; i<getter.size(); i++)
		{
			if(getter[i]==' ')v.push_back(buf),buf="";
			else buf+=getter[i];
			if(i==getter.size()-1)
				v.push_back(buf);

		}
		reverse(v.begin(),v.end());
		cout<<"Case #"<<i<<": ";
		for(int i =0 ; i<v.size(); i++)
		{
			cout<<v[i]<<" ";
		}
		cout<<endl;
	}
	
}