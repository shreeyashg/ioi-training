#include <iostream>
#include <vector>
#include <queue>
#include <deque>
#include <fstream>
#include <algorithm>

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i=a; i<=b; i++)

const ll MX = 1e6;

using namespace std;

vector<ll> x, y;

int main()
{
        ifstream cin("square.in");
        ofstream cout("square.out");
        
        REP(i, 2)
        {
                REP(j, 2)
                {
                        ll a, b;
                        cin>>a>>b;
                        x.push_back(a);
                        y.push_back(b);
                }
        }
        sort(x.begin(), x.end());
        sort(y.begin(), y.end());
        
        cout<<max(x[x.size()-1]-x[0], y[y.size()-1]-y[0])*max(x[x.size()-1]-x[0], y[y.size()-1]-y[0])<<endl;
}