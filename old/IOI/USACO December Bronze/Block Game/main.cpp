#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <deque>
#include <fstream>
#include <algorithm>
#include <set>

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i=a; i<=b; i++)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;


const ll MX = 1e6;

using namespace std;

ll n;
map<char,ll> mp;

int main()
{
        ifstream cin("blocks.in");
        ofstream cout("blocks.out");
        
        cin>>n;
        REP(i, n)
        {
                set<char> s;
                string a, b;
                cin>>a>>b;
                map<char,ll> aa, bb;
                REP(i, a.size())
                {
                        s.insert(a[i]);
                        aa[a[i]]++;
                }
                REP(i, b.size())
                {
                        s.insert(b[i]);
                        bb[b[i]]++;
                }
                
                for(auto it = s.begin(); it!=s.end(); it++)
                {
                        mp[*it] += max(aa[*it], bb[*it]);
                }
                
        }
        FOR(i, 'a', 'z')
        {
                cout<<mp[i]<<endl;
        }
}