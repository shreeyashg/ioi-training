#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n, m;
ll arr[103][103];
ll rm[103], cm[103];
ll ans = 0;
vector<string> v;

int main()
{
	cin >> n >> m;
	REP(i, n)
	{
		REP(j, m)
		{
			cin >> arr[i][j];
		}
	}

	if(n > m)
	{
		REP(i, m)
		{
			ll cnt = 0;
			bool b = 0;
			while(true)
			{
				cnt = 0;
				REP(j, n)
				{
					if(arr[j][i])
						cnt ++;
					else
						b = 1;
				}
				if(b == 1)
					break;

				if(cnt == n)
				{
					ans ++;
					v.push_back("col " + to_string(i+1));
					REP(j, n)
					{
						arr[j][i]--;
						if(arr[j][i] < 0) cout << -1 << endl, exit(0);
					}
				}
			}
		}

		REP(i, n)
		{
			ll cnt = 0;
			bool b = 0;
			while(true)
			{
				cnt = 0;
				REP(j, m)
				{
					if(arr[i][j])
						cnt ++;
					else b = 1;
				}

				if(b == 1)
					break;

				if(cnt == m)
				{
					ans ++;
					v.push_back("row " + to_string(i+1));
					REP(j, m)
					{
						arr[i][j]--;
						if(arr[i][j] < 0) cout << -1 << endl, exit(0);
					}
				}
			}
		}

		REP(i, n)
		{
			REP(j, m)
			{
				if(arr[i][j])
					cout << -1 << endl, exit(0);
			}
		}
	}
	else
	{
		REP(i, n)
		{
			ll cnt = 0;
			bool b = 0;
			while(true)
			{
				cnt = 0;
			/*	REP(i, n)
				{
					REP(j, m)
					{
						cout << arr[i][j] <<" ";
					}
					cout << endl;
				}
				cout << endl;
			*/
				REP(j, m)
				{
					if(arr[i][j])
						cnt ++;
					else 
						b = 1;
				}

				if(b == 1)
					break;

				if(cnt == m)
				{
					ans ++;
					v.push_back("row " + to_string(i+1));
					REP(j, m)
					{
						arr[i][j]--;
						if(arr[i][j] < 0) cout << -1 << endl, exit(0);
					}
				}
				else
					break;
			}
		}

		REP(i, m)
		{
			ll cnt = 0;
			bool b = 0;
			while(true)
			{
				cnt = 0;
				REP(j, n)
				{
					if(arr[j][i])
						cnt ++;
					else
						b = 1;
				}
				if(b == 1)
					break;

				if(cnt == n)
				{
					ans ++;
					v.push_back("col " + to_string(i+1));
					REP(j, n)
					{
						arr[j][i]--;
						if(arr[j][i] < 0) cout << -1 << endl, exit(0);
					}
				}
			}
		}

		REP(i, n)
		{
			REP(j, m)
			{
				if(arr[i][j])
					cout << -1 << endl, exit(0);
			}
		}
	}

	cout << ans << endl;
	REP(i, v.size()) cout << v[i] << endl;
}