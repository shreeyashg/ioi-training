#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n, k;
vector<ll> v, ps; 
ll bit[2 * MX];

ll query(ll idx)
{
	ll ans = 0;
	while(idx > 0)
		ans += bit[idx], idx -= (idx & -idx);
	return ans;
}

void update(ll idx, ll val)
{
	while(idx < 2 * MX)
	{
	//	cout << idx << endl;
		bit[idx] += val, idx += (idx & -idx);
	}
}

int main()
{
	cin >> n >> k;
	v.resize(n), ps.resize(n + 2);
	REP(i, n)
	{
		cin >> v[i];
		if(v[i] < k)
			v[i] = -1;
		else
			v[i] = 1;
	}
	ps[1] = 0;
	ps[0] = 0;
	FOR(i, 2, n + 1) ps[i] = ps[i-1] + v[i - 2];

	ll ans = 0;
	update(ps[1] + MX, 1);
	FOR(i, 2, n + 1)
	{
	//	DEBUG(i);
		update(ps[i] + MX, 1);
		ans += i  - query(ps[i] + MX);
	}

	cout << (n * (n + 1))/2 - ans << endl;
}