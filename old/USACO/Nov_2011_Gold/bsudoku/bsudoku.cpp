#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 10;

bool b[MX][MX];
ll dp[MX][MX][512][8][2];

ll rec(ll r, ll c, ll mc, ll mb, bool p)
{
//	cout << r << " " << c << " " << mc << " " << mb <<"  "<< p << endl;
	if(r == 9)
	{
		if(mc)
			return INF;
		return 0;
	}
	if(c == 9)
	{
		if(p)
			return INF;
		if(r % 3 == 2 && mb)
			return INF;
		return rec(r + 1, 0, mc, mb, p);
	}

	if(dp[r][c][mc][mb][p] != -1)
		return dp[r][c][mc][mb][p];

	// set 1
	ll ans = INF;
	ans = min(ans, !b[r][c] + rec(r, c + 1, mc ^ 1 << c, mb ^ 1 << c / 3, !p));

	// set 0
	ans = min(ans, b[r][c] + rec(r, c + 1, mc, mb, p));
	return dp[r][c][mc][mb][p] = ans;
}

int main()
{
	memset(dp, -1, sizeof(dp));
	REP(i, 9)
	{
		string S;
		cin >> S;
		REP(j, 9)
		{
			b[i][j] = (S[j] == '1');
		}
	}

	cout << rec(0, 0, 0, 0, 0) << endl;
}