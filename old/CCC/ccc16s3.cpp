#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n, m;
vector<ll> phos;
vector<ll> v[MX];
ll vis[MX], sub[MX], ispho[MX];
ll md = 0, mv = -1;

void DFS(ll cur, ll dist)
{
	if(vis[cur]) return;
	vis[cur] = 1;

	if(ispho[cur] && dist > md)
		md = dist, mv = cur;

	REP(i, v[cur].size())
	{
		if(!vis[v[cur][i]])
			DFS(v[cur][i], 1 + dist);
	}

}

ll init(ll cur)
{
	if(vis[cur]) return 0;
	vis[cur] = 1;

	ll ans = 0;
	REP(i, v[cur].size())
	{
		if(!vis[v[cur][i]]) ans += init(v[cur][i]);
	}

	return sub[cur] = ispho[cur] + ans;
}

ll rec(ll cur)
{
//	cout << cur << endl;
	if(vis[cur]) return 0;
	vis[cur] = 1;

	ll ans = 0;
	REP(i, v[cur].size())
	{
		if(sub[v[cur][i]] && !vis[v[cur][i]]) ans += (1 + rec(v[cur][i]));
	}

	return ans;
}

int main()
{
	cin >> n >> m;
	phos.resize(m);
	REP(i, m) cin >> phos[i], ispho[phos[i]] = 1;

	REP(i, n -1 )
	{
		ll x, y;
		cin >> x >> y;
		v[x].push_back(y);
		v[y].push_back(x);
	}

	DFS(phos[0], 0);
	ll start = mv;
	memset(vis, 0, sizeof(vis));
	init(mv);

/*	FOR(i, 0, n - 1)
	{
		cout << i << " > " << sub[i] << endl;
	}
*/
//	cout << endl;
	memset(vis, 0, sizeof(vis));

	ll dist = 2 * rec(mv);

	memset(vis ,0, sizeof(vis));
	DFS(mv, 0);
	cout << dist - md << endl;
}