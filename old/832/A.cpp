#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

int main()
{
	ll n, k;
	cin >> n >> k;

	ll num = n/k;
	ll left = (n % k);

	if(num == 0)
	{
		cout << "NO" << endl;
	}

	if(num % 2 == 0)
	{
		if(left != 0)cout << "YES" << endl;
		else cout << "NO" << endl;
	}
	else
	{
		cout << ""
	}
}