#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll N, D;
ll lefts[MX];
vector< pair<ll, pair<ll, ll> > > v;

bool cmp(pair<ll, pair<ll, ll> > a, pair<ll, pair<ll, ll> > b)
{
	if(a.first == b.first)
	{
		if(a.second.first == b.second.first)
		{
			return a.second.second >  b.second.second;
		}
		else
			return a.second.first > b.second.first;
	}
	return a.first < b.first;
}

int main()
{
	ll t;
	cin >> t;
	while(t--)
	{
		memset(lefts, 0, sizeof(lefts));
		ll total = 0;
		cin >> N >> D;
		REP(i, N)
		{
			ll x, y, z;
			cin >> x >> y >> z;
			v.push_back({x, {y, z}});
			total += z * y;
		}

		sort(v.begin(), v.end(), cmp);
		REP(i, v.size())
		{
			lefts[i] = v[i].second.first;
		}

		ll ans = 0;
		ll idx = 0;
		set<pair<ll, ll> > s;

		ll used = 0;

		FOR(i, 1, D)
		{
			ll day = 0;
			while(v[idx].first <= i && idx < v.size())
			{
		//		cout << "o " << idx << endl;
				s.insert({-v[idx].second.second, idx});
				idx ++;
			}



			if(!s.empty())
			{
				auto it = s.begin();
				auto xp = *it;
				ll idx = xp.second;
				ll cost = -xp.first;
				if(lefts[idx] == 1)
				{
					used += cost;
					day += cost;
					lefts[idx] = 0;
					s.erase(it);
				}
				else
				{
				//	cout << cost << endl;
					used += cost;
					day += cost;
					lefts[idx]--;
				}
			}

		//	cout << i << " used today = " << day << endl;
		}

		cout << total - used << endl;	
		v.clear();
	//	cout << "EOT" << endl;
	}
}