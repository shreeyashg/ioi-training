#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

int main()
{
	ll t;
	cin >> t;
	cin.ignore();
	while(t--)
	{
		string str;
		getline(cin, str);
	// /	cout << str << endl << endl;
		transform(str.begin(), str.end(), str.begin(), ::tolower);
		ll cnt = 0;
		REP(i, str.size())
		{
			if(str[i] == ' ')
				cnt ++;
		}
		cnt --;
		if(cnt == -1)
		{
			REP(i, str.size())
			{
				if(i == 0)
					cout << (char)(str[i] - 32);
				else
					cout << str[i];
			}
			cout << endl;
		}
		else
		{
			REP(i, str.size())
			{
				if(i == 0)
					cout << (char)toupper(str[i]) << ". ";// << endl;
				if(str[i] == ' ')
				{
					if(cnt)
					{
						cout << (char)toupper(str[i+1]) << ". ";
						cnt --;
					}
					else
					{
						FOR(j, i + 1, str.size() - 1)
						{
							if(j == i + 1)
								cout << (char)toupper(str[j]);
							else
								cout << str[j]; 
						}
						cout << endl;
						break;
					}
				}
			}

		}
	}
}