#include <bits/stdc++.h>
using namespace std;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

typedef long long ll;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;
ll N, B;

ll func(ll x)
{
	return (x * ((N - x) / B));
}

ll ts(ll start, ll end)
{
    ll l = start, r = end;
    ll ans = 0;

    for(int i=0; i<1000; i++) {

      ll l1 = (l*2+r)/3;
      ll l2 = (l+2*r)/3;
          	       // cout << l1 << " " << l2 << endl;
      ans = max(ans, func(N - B*l1));
      ans = max(ans, func(N - B*l2));

      //cout<<l1<<" "<<l2<<endl;
      if(func(N - B * l1) > func(N - B * l2)) r = l2; else l = l1;
      
      }

     return ans;
}

int main()
{
	ll t;
	cin >> t;
	while(t--)
	{
		cin >> N >> B;
		ll ans = 0;
		cout << ts(1, ceil((double)N/(double)B)) << endl;
	}

}
