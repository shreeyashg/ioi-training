#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;
string s;


int main()
{
	ll t;
	cin >> t;
	while(t--)
	{

		string str;
		cin >> str;
		REP(i ,str.size())
		{
			if(str[i] != '=')s += str[i];
		}

	/*	FORD(i, str.size() - 1, 0)
		{
			if(str[i] == '>')
				up[i] = up[i + 1];
			if(str[i] == '<')
				down[i] = down[]

		}
		REP(i, str.size())
		{
			if(str[i] == '>')
			{
				s++;
				mx = max(mx, s);
				mn = min(mn, s);
			}
			if(str[i] == '<')
			{
				s--;
				mx = max(mx, s);
				mn = min(mn, s);
			}
		}

		cout << mx - mn + 1 << endl;
		up.clear();
		down.clear();
	*/
	//	cout << s << endl;
		if(s.empty())
		{
			cout << 1 << endl;
			continue;
		}
		ll curr = s[0];
		ll cntc = 1, mx = 1;
		FOR(i, 1, s.size() - 1)
		{
			if(s[i] == curr)
				cntc ++;
			else
				mx = max(mx, cntc), cntc = 1, curr = s[i];
		}
		mx = max(mx, cntc);
		cout << mx + 1 << endl;

		s.clear();
	}
}