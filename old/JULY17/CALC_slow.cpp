#include <bits/stdc++.h>
using namespace std;
 
typedef long long ll;
 
#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
 
const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;
double N, B;

double calc(double x)
{
	return (x*N/B) - ((x*x)/B); 
}

int main()
{
	ll t;
	cin >> t;
	while(t--)
	{
		cin >> N >> B;
		cout << floor(calc(N / 2.0)) << endl;
		continue;
	/*	ll ans = 0;
		FOR(i, 1, N)
		{
			ll left = N - i;
			ans = max(ans, i * (left/B));
		}
		cout << ans << endl;
 */
	}
 
} 
