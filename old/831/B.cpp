#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

string a, b, c, A, B, C;

ll find(char ch)
{
	REP(i, a.size())
	{
		if(a[i] == ch || A[i] == ch)
			return i;
	}
	return -1;
}

int main()
{
	cin >> a >> b >> c;
	REP(i, a.size()) 
	{
		if(a[i] >= 97 && a[i] <= 122) A.push_back((char)(a[i] - 32));
		if(b[i] >= 97 && b[i] <= 122) B.push_back((char)(b[i] - 32));
	}

	REP(i, c.size())
	{
		if(c[i]>=97 && c[i] <= 122) C.push_back(b[find(c[i])]);
		else if(find(c[i]) == -1)
			C.push_back(c[i]);
		else C.push_back(B[find(c[i])]);
	}

	cout << C << endl;
}