#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

int main()
{
	ll n;
	cin >> n;
	vector<ll> v;
	REP(i, n)
	{
		ll x;
		cin >> x;
		v.push_back(x);
	}

	bool a = 0, b = 0, c = 0;
	vector<ll> vb;
	REP(i, v.size()-1)
	{
		if(v[i + 1] > v[i])
		{
			vb.push_back(-1);
		}
		else if(v[i + 1] < v[i])
			vb.push_back(1);
		else
			vb.push_back(0);
	}

	ll csum = 0, presum = 0;
	if(is_sorted(vb.begin(), vb.end()) || v.size() == 1)
		cout << "YES" << endl;
	else
		cout << "NO" << endl;
}