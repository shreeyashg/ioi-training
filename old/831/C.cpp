#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 2e3 + 4;

ll ks[MX], ps[MX];
//unordered_map<ll, ll> mp;+
vector<ll> v;
set<ll> s, global, temp;

int main()
{
	ios::sync_with_stdio(false);
	cin.tie(0);
	ll n, k;
	cin >> k >> n;

	REP(i, k)
	{
		cin >> ks[i];
		ps[i + 1] = ps[i] + ks[i];
	}

	vector<ll> b;

	REP(i, n)
	{
		ll f;
		cin >> f;
		b.push_back(f);
		s.insert(f);
	}

	REP(i, b.size())
	{
	//	map<ll, ll> mps;
		FOR(j, 1, k)
		{
			global.insert(b[i] - ps[j]);
		}
		break;
	}

	/*

	for(auto it = global.begin() ; it != global.end(); it ++)
	{
		cout << *it << endl;
	}

	DEBUG("2312");

	*/

	ll ans = 0;
	temp = s;

	for(auto it = global.begin() ; it  != global.end(); it++)
	{
		FOR(i, 1, k)
		{
			temp.erase(*it + ps[i]);
		}
		if(temp.empty())
			ans ++;
		temp = s;
	}

	cout << ans << endl;
}