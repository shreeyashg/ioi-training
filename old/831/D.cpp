#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n, k, p;
vector<ll> v, ks;
ll dp[1005][2005];

ll rec(ll peeps, ll keys)
{
	if(dp[peeps][keys] != -1)
		return dp[peeps][keys];

	if(peeps > n)
		return 0;
	else if (keys > k)
		return INF;

	ll ans = INF;
	// Use the key
	ans = min(ans, max(abs(v[peeps] - ks[keys]) + abs(ks[keys] - p), rec(peeps + 1, keys + 1)));

	// Don't use the key
	ans = min(ans, rec(peeps, keys + 1));

	return dp[peeps][keys] = ans;
}

int main()
{
	ios::sync_with_stdio(false);
	cin.tie(0);
	memset(dp, -1, sizeof(dp));

	cin >> n >> k >> p;
	REP(i, n)
	{
		ll t;
		cin >> t;
		v.push_back(t);
	}
	sort(v.begin(), v.end());
	v.insert(v.begin(), 0);

//	REP(i, v.size()) cout << v[i] << " ";
//	cout << endl;

	REP(i, k)
	{
		ll x;
		cin >> x;
		ks.push_back(x);
	}
	ks.insert(ks.begin(), 0);
	sort(ks.begin(), ks.end());

//	REP(i, ks.size()) cout << ks[i] <<" ";
//	cout << endl;	

/*

	dp[0][0] = 0;
	FOR(i, 1, n)
	{
		FOR(j, 1, k)
		{
			FOR(x, 1, j)
			{
				dp[i][j] = min(dp[i][j], dp[i - 1][j - x] + use(i, x))
			}
		}
	}

*/
	cout << rec(1, 1) << endl;
}