#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll bit[4 * MX];
vector<ll> v, pos[MX];
set<ll> s;

ll query(ll aa) // Returns sum from 0 to i
{
        ll ans=0;
        while(aa>0)
        {
                ans+=bit[aa];
                aa-= (aa&-aa);
        }
        return ans;
}

void update(ll aa, ll x) // adds x to ith element
{
        while(aa<MX)
        {
        //	cout << aa << endl;
                bit[aa]+=x;
                aa+= (aa&-aa);
        }
}

ll getsum(ll l, ll r)
{
	return(query(r) - query(l - 1));
}

int main()
{


	ll n;
	cin >> n;

	REP(i, n) update(i + 1, 1);

	REP(i, n)
	{
		ll k;
		cin >> k;
		pos[k].push_back(i + 1);
		s.insert(k);
	}

//	cout << "_" << endl;

	ll ans = 0;
	for(auto it = s.begin() ; it != s.end() ; it++)
	{
		v.push_back(*it);
	}

//	cout << "_" << endl;

	REP(i, v.size())
	{
		sort(pos[v[i]].begin(), pos[v[i]].end());
	}

	ll curr = 1;
	REP(i, v.size())
	{
		ll s = lower_bound(pos[v[i]].begin(), pos[v[i]].end(), curr) - pos[v[i]].begin();
		FOR(j, s, pos[v[i]].size() - 1)
		{
			if(pos[v[i]][j] >= curr) ans += getsum(curr, pos[v[i]][j]);
			else ans += getsum(curr, n) + getsum(1, pos[v[i]][j]);
			curr = pos[v[i]][j];
			update(pos[v[i]][j], -1);
		}
		FOR(j, 0, s - 1)
		{
			if(pos[v[i]][j] > curr)ans += getsum(curr, pos[v[i]][j]);
			else ans += getsum(curr, n) + getsum(1, pos[v[i]][j]); 
			curr = pos[v[i]][j];
			update(pos[v[i]][j], -1);
		}

	//	cout << ans << endl;

	//	cout << "_" << endl;
	}

	cout << ans << endl;
}

