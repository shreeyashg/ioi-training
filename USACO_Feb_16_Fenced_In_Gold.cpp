#pragma GCC optimize "-O3"
#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 2e9, MX = 2e3 + 4;

ll A, B, n, m, a[MX], b[MX];
vector<pair<ll, pair<ll, ll> > > edges; 
vector<ll> arr;

ll finds(ll x)
{
	if(x != arr[x])
		return arr[x] = finds(arr[x]);
	return arr[x];
}

void uni(ll x, ll y)
{
	x = finds(x);
	y = finds(y);
	if(x == y)
		return;
	arr[y] = arr[x];
}

void print()
{
	FOR(i, 1, (n + 1) * (m + 1))
	{
		cout << "par[" << i << "] = " << arr[i] << endl;
	}
}

int main()
{
	ifstream cin("fencedin.in");
	ofstream cout("fencedin.out");
	cin >> A >> B >> n >> m;
	arr.resize((n+1)*(m+1)+1);
	REP(i, n)
	{
		cin >> a[i + 1];
	}
	REP(i, m)
	{
		cin >> b[i + 1];
	}
	FOR(i, 1, (n+1) * (m+1))
	{
		arr[i] = i;
	}

	sort(a + 1, a + n + 1);
	sort(b + 1, b + m + 1);
	b[m + 1] = A;
	a[n + 1] = B;
	ll gcnt = 1;
	ll left = 0, top = 0;
	

	FOR(i, 1, m + 1)
	{
		ll curr = b[i] - top;
		FOR(j, 1, n)
		{
			edges.push_back({curr, {gcnt, gcnt + 1}});
		//	edges.push_back({curr, {gcnt + 1, gcnt}});
			gcnt++;
		}
		gcnt++;
		top = b[i];
	}

	ll k = edges.size();
//	cout << endl;
	gcnt = 1;
	FOR(i, 1, n + 1)
	{
		ll curr = a[i] - left;
		gcnt = i;
		FOR(j, 1, m)
		{
			edges.push_back({curr, {gcnt, gcnt + n + 1}});
		//	edges.push_back({curr, {gcnt + n + 1, gcnt}});
			gcnt += n + 1;
		}
		gcnt++;
		left = a[i];
	}

	sort(edges.begin(), edges.end());
//	reverse(edges.begin(), edges.end());

	ll ans = 0, cnt = 0;
	REP(i, edges.size())
	{
		ll x = edges[i].second.first, y = edges[i].second.second;
		ll cost = edges[i].first;
	// 	cout << x << " " << y << 

		if(finds(x) != finds(y))
			ans += cost, uni(x, y), cnt ++;
	}

	cout << ans << endl;
}