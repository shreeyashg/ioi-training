#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;
ll arr[256];
pair<ll, ll> memo[250][250];

//memo[l][r] = {ans, good};

pair<ll, ll> f(ll l, ll r)
{
//	cout << l << " " << r << endl;
	if(l == r)
		return {arr[l], 1};

	if(memo[l][r].second != -1)
		return memo[l][r];

	pair<ll, ll> ans;
	ll a1 = 0, a2 = 0;
	for(ll i = l; i < r; i++)
	{
		if(f(l, i) == f(i + 1, r) && f(i + 1, r).second && f(l, i).second)
			a1 = max(a1, (f(l, i).first + 1));
		else
			a2 = max(a2, max(f(l, i).first, f(i + 1, r).first));
	}
	if(a1 >= a2)
		return memo[l][r] = {a1, 1};
	else
		return memo[l][r] = {a2, 0};
}

int main()
{
	REP(i, 250) REP(j, 250) memo[i][j] = {-1, -1};

//	ifstream cin("248.in");
//	ofstream cout("248.out");
	memset(memo, -1, sizeof(memo));
	ll n;
	cin >> n;
	REP(i, n) cin >> arr[i + 1];
	cout << f(1, n).first << endl;
}