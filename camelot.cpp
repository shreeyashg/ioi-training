#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;
template <typename T,typename U>                                                   
std::pair<T,U> operator+(const std::pair<T,U> & l,const std::pair<T,U> & r) {   
    return {l.first+r.first,l.second+r.second};                                    
}                                                                                  

ll grid[9][9], apsp[9][9][9][9];
ll vis[9][9], dist[9][9], allk[9][9];
vector<pair<ll, ll> > knights;
pair<ll, ll> king;
string str;
vector<pair<ll, ll>> mov = {{+2, +1}, {+2, -1}, {-2, +1}, {-2, -1}, {+1, +2}, {+1, -2}, {-1, +2}, {-1, -2}}; 
void go(pair<ll, ll>);

inline ll kingdist(ll a, ll b, ll x, ll y)
{
	return max(abs(x - a), abs(y - b));
}

void genmat()
{
	FOR(i, 1, 8)
	{
		FOR(j, 1, 8)
		{
			go({i, j});
		}
	}
}

void gallk()
{
	FOR(i,1,8)
	{
		FOR(j,1,8)
		{
			ll ans = 0;
			REP(ii, knights.size())
			{
			//	DEBUG(apsp[i][j][knights[ii].first][knights[ii].second]); 	
				ans += apsp[i][j][knights[ii].first][knights[ii].second];
			}
			allk[i][j] = ans;
		}
	}
}

ll getking()
{
	ll ans = INF;
	FOR(i,1,8)
	{
		FOR(j,1,8)
		{
			ll finalx = i, finaly = j;
			REP(iii, knights.size())
			{
				auto chosen = knights[iii];
				FOR(x, 1, 8)
				{
					FOR(y, 1, 8)
					{
						ans = min(ans, kingdist(x, y, king.first, king.second) + apsp[chosen.first][chosen.second][x][y] + apsp[x][y][finalx][finaly] + allk[finalx][finaly] - apsp[finalx][finaly][chosen.first][chosen.second]);
					}
				}
			}
		}
	}

	return ans;
}

void go(pair<ll, ll> p)
{
	memset(dist, 0, sizeof(dist));
	memset(vis, 0, sizeof(vis));
	queue<pair<pair<ll, ll>, ll> > q;
	q.push({p, 0});

	while(!q.empty())
	{
		auto t = q.front();
		q.pop();

		if(t.first.first < 1 || t.first.first > 8 || t.first.second < 1 || t.first.second > 8)
			continue;

		if(vis[t.first.first][t.first.second])
			continue;
		vis[t.first.first][t.first.second] = 1;
		dist[t.first.first][t.first.second] = t.second;

		REP(i, mov.size())
		{
			q.push({{t.first + mov[i]}, t.second + 1});
		}
	}

	FOR(i, 1, 8)
	{
		FOR(j, 1, 8)
		{
			apsp[p.first][p.second][i][j] = dist[i][j];
		}
	}
}

int main()
{
	cin >> str;
	REP(i, str.size())
	{
		if(i%2 == 0)
		{
			ll second = str[i] - 'A' + 1;
			ll first = str[i + 1] - '0';
			if(i == 0)grid[second][first] = 5, king = {second, first};
			else grid[second][first] = 1, knights.push_back({second, first});
		}
	}

	genmat();
	gallk();
/*	FOR(i,1,8)
	{
		FOR(j,1,8)
		{
			cout << grid[i][j]<< " ";
		}
		cout << endl;
	}
	cout << endl;
	FOR(i,1,8)
	{
		FOR(j,1,8)
		{
			cout << apsp[1][1][i][j]<< " ";
		}
		cout << endl;
	}
	cout << endl;
	FOR(i,1,8)
	{
		FOR(j,1,8)
		{
			cout << allk[i][j]<< " ";
		}
		cout << endl;
	}

	REP(i, knights.size())
	{
		cout << knights[i].first << " " << knights[i].second << endl;
	} */

	cout << getking() << endl;
}