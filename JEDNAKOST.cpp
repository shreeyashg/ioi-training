#include <bits/stdc++.h>
using namespace std;

typedef short ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const short INF = 30000;

ll S, n;
pair<ll, ll> choice[1000][5001];
ll memo[1000][5001], nxt[1001];
bool chk[1000][5001];
string str, s;

ll getnum(ll l, ll r)
{
//	cout << l << " " << r << endl;
	if(l > r)
		return 0;
	return stoll(str.substr(l, r - l + 1));
}

ll rec(ll start, ll sum)
{
//	cout << start << " " << sum << endl;

	if(start > n - 1)
		return INF;
	if(sum < 0 || sum > S)
		return INF;

	if(n - nxt[start] <= 5 && getnum(nxt[start], n - 1) == sum)
	{
		choice[start][sum] = {-1, -1};
		return 0;
	}
	if(n - 1 == start && getnum(start, n - 1) != sum)
		return INF;

	if(chk[start][sum])
		return memo[start][sum];

	ll ans = INF;
	FOR(i, nxt[start], min(nxt[start] + 3, n - 1))
	{
	//	cout << nxt[i] << " " << i << endl;
		ll num = getnum(nxt[start], i);
	//	cout << "start = " << start <<" , num = " << num << " range = [ " << i + 1 << " " << n - 1 << " ] " << endl;
		
		if(1 + rec(i + 1, sum - num) < ans)
		{
			choice[start][sum] = {i + 1, sum - num};
			ans = 1 + rec(i + 1, sum - num);
		}
	}
	chk[start][sum] = 1;
	return memo[start][sum] = ans;
}

int main()
{
	ios::sync_with_stdio(false);
	cin.tie(0);

	cin >> str;
	REP(i, str.size())
	{
		if(str[i] == '=')
			n = i, s = str.substr(i + 1, str.size() - 1 - i);
	}

	S = stoll(s);

	REP(i, n)
	{
		FOR(ii, i + 1, n - 1)
		{
			if(str[i] != '0')
			{
				nxt[i] = i;
			}
			else if(str[ii] != '0')
			{
				nxt[i] = ii;
				break;
			}
		}
		if(nxt[i] == 0 && i > 0)
			nxt[i] = n - 1;
	}

//	cout << S << endl;
//	cout << getnum(3, 5) << endl;
//	cout << "Here" << endl;
//	cout << str << endl;
	rec(0, S); // << endl; // << endl;
//	cout << endl;
	ll aa = choice[0][S].first, bb = choice[0][S].second;
	ll cnt = 0;

	while(true)
	{
	//	cout << aa << " " << bb << endl;

		if(aa == -1 && bb == -1)
			break;
		str.insert(aa + cnt, "+"), cnt++;
		ll naa = choice[aa][bb].first;
		ll nbb = choice[aa][bb].second;
		aa = naa, bb = nbb;
	}
	cout << str << endl;
}