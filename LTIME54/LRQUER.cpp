#pragma GCC optimize ("O5")
#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll arr[MX], n, q;
multiset<ll> s[4*MX];

ll g_result = INF, g_val = -1;

void build_tree(ll node, ll a, ll b) {
  	if(a > b) return; // Out of range
  	
  	if(a == b) 
  	{ // Leaf node
    		s[node].insert(arr[a]); // Init value
		return;
	}
	
	build_tree(node*2, a, (a+b)/2); // Init left child
	build_tree(node*2+1, 1+(a+b)/2, b); // Init right child
	
	ll idx = node;
	if(s[2*idx].size() > s[2*idx+1].size())
	{
		s[idx] = s[2*idx];
		for(auto it = s[2*idx+1].begin(); it != s[2*idx+1].end(); it++)
		{
			s[idx].insert(*it);
		}
	}
	else
	{
		s[idx] = s[2*idx+1];
		for(auto it = s[2*idx].begin(); it != s[2*idx].end(); it++)
		{
			s[idx].insert(*it);
		}
	}
	// Init root value
}

void update_tree(ll node, ll a, ll b, ll pos, ll val) 
{
	if(!(a <= b) || !(pos >= a && pos <= b)) // Current segment is not within range [i, j]
		return;
    
  	if(a == pos && b == pos) 
  	{	
  		// Segment is fully within range
  	//	cout << pos << " " << val << endl;
 		if(s[node].count(arr[pos]))
 			s[node].erase(arr[pos]);
 		s[node].insert(val);
 		return;
 	}

	update_tree(node*2, a, (a+b)/2, pos, val); // Updating left child
	update_tree(1+node*2, 1+(a+b)/2, b, pos, val); // Updating right child

	ll idx = node;
	if(s[node].count(arr[pos]))
		s[node].erase(arr[pos]);
	s[node].insert(val);
}

ll query_tree(ll node, ll a, ll b, ll i, ll j, ll k) {
	
	if(a > b || a > j || b < i) return 0; // Out of range

	if(a >= i && b <= j) // Current segment is totally within range [i, j]
	{
		auto it = s[node].lower_bound(k);
		ll ans = INF;
		if(it != s[node].end() && *it <= k)
		{
			ans = min(ans, abs(*it-k));
			if(abs(*it-k) < g_result)
				g_val = *it, g_result = abs(*it-k);
		}

		if(it != s[node].begin())
		{
			it--;
			ans = min(ans, abs(*it-k));
			if(abs(*it-k) < g_result)
				g_val = *it, g_result = abs(*it-k);
		}
		return ans;
	}

	ll q1 = query_tree(node*2, a, (a+b)/2, i, j,k); // Query left child
	ll q2 = query_tree(1+node*2, 1+(a+b)/2, b, i, j,k); // Query right child

	ll res = min(q1, q2); // Return final result
	
	return res;
}



int main()
{



//	ios::sync_with_stdio(false);
//	cin.tie(0);
//	cout.tie(0);

//	DEBUG("built");

	ll t;
	cin >> t;
	while(t--)
	{

		memset(arr, 0, sizeof(arr));
		REP(i, 4*MX)
		{
		    if(!s[i].empty()) s[i].clear();
		}


		scanf("%lld%lld", &n, &q);

		REP(i, n) scanf("%lld", &arr[i+1]);

		build_tree(1, 1, n);

		REP(i, q)
		{
			ll x, y, k;
			scanf("%lld%lld%lld", &x, &y, &k);
			if(x == 1)
			{
				ll sum = arr[y] + arr[k];
				ll prod = arr[y] * arr[k];

			//	cout << endl << endl;
			//	DEBUG(sum);
			//	DEBUG(prod);

				g_result = INF, g_val=-1;

				ll res1 = query_tree(1, 1, n, y, k, (sum/2));
			//	DEBUG(res1);
			//	DEBUG(g_result);
			//	DEBUG(g_val);
			//	DEBUG("t2");

				ll r1 = g_val;

				g_result = INF, g_val=-1;

				ll res2 = query_tree(1, 1, n, y, k, (sum/2) + (sum%2));
			//	DEBUG(res2);

			//	DEBUG(g_result);
			//	DEBUG(g_val);
				g_result = g_val;
			//	DE
			//	cout << endl;
				printf("%lld\n", max((g_result * (sum) - (g_result*g_result) - prod), ((r1 * (sum) - (r1*r1) - prod))));// << endl;// << endl;
			}
			else
			{
			//	DEBUG("start");
				update_tree(1, 1, n, y, k);
			//	DEBUG("done");
				arr[y] = k;
			}
		}
	}
}