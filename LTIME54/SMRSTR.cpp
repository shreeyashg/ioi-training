#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

int main()
{
	ll t;
	cin >> t;
	while(t--)
	{
		ll n, q;
		cin >> n >> q;
		vector<ll> d;
		REP(i, n)
		{
			ll x;
			cin >> x;
			d.push_back(x);
		}
		sort(d.begin(), d.end());
		reverse(d.begin(), d.end());

		REP(f, q)
		{
			ll x;
			cin >> x;
			REP(i, d.size())
			{
				x /= d[i];
				if(d[i] == 1 || x == 0)
					break;
			}
			cout << x << " ";
		}
		cout << endl;
	}
}