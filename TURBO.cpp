#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

struct node {
  ll from, to;
  long long value, lazy;
  node *left, *right;
  node() {
    from=1;
    to=1e9;
    value=0;
    lazy=0;
    left=NULL;
    right=NULL;
  }
  void extend() {
    if(left==NULL) {
      left=new node();
      right=new node();
      left->from=from;
      left->to=(from+to)>>1;
      right->from=((from+to)>>1)+1;
      right->to=to;
    }
  }
};

node *root;

void update(node *curr, ll left, ll right, long long value) {
  if(curr->lazy) {
    curr->value+=(curr->to-curr->from+1)*curr->lazy;
    if(curr->from!=curr->to) {
      curr->extend();
      curr->left->lazy+=curr->lazy;
      curr->right->lazy+=curr->lazy;
    }
    curr->lazy=0;
  }
  if((curr->from)>(curr->to) || (curr->from)>right || (curr->to)<left) return;
  if(curr->from>=left && curr->to<=right) {
    curr->value+=(curr->to-curr->from+1)*value;
    if(curr->from!=curr->to) {
      curr->extend();
      curr->left->lazy+=value;
      curr->right->lazy+=value;
    }
    return;
  }
  curr->extend();
  update(curr->left,left,right,value);
  update(curr->right,left,right,value);
  curr->value=curr->left->value + curr->right->value;
}

long long query(node *curr, ll left, ll right) {
  if((curr->from)>(curr->to) || (curr->from)>right || (curr->to)<left) return 0;
  if(curr->lazy) {
    curr->value+=(curr->to-curr->from+1)*curr->lazy;
    curr->extend();
    curr->left->lazy+=curr->lazy;
    curr->right->lazy+=curr->lazy;
    curr->lazy=0;
  }
  if(curr->from>=left && curr->to<=right) return curr->value;
  long long q1,q2;
  curr->extend();
  q1=query(curr->left,left,right);
  q2=query(curr->right,left,right);
  return q1+q2;
}

ll og[MX], arr[MX], n;

int main()
{
	cin >> n;
	FOR(i, 1 ,n) cin >> arr[i], og[arr[i]] = i;
	node *root = new node();

	ll t1 = 0, t2 = n + 1, target = 1, ans = 0;
	update(root, 1, n, 1);

//	FOR(i, 1, n) cout << query(root, i, i) << " " ;
//	cout << endl;

	FOR(i, 1, n)
	{

		if(i % 2)
			target = ++t1;
		else
			target = --t2;

		update(root, og[target], og[target], -1);

		if(i % 2)
		{
			// left
			cout << query(root, 1, og[target]) << endl;
		}
		else
		{
			cout << query(root, og[target], n) << endl;
			// right
		}
	}


}