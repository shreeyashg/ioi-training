#include <bits/stdc++.h>
using namespace std;

typedef int ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 2e9 + 8, MX = 1e6 + 4;

ll n, arr[MX], ans, k, dp[MX][3][2];
pair<ll, pair<ll, ll> > choice[MX][3][2];

ll rec(ll idx, ll last, bool ch)
{
//	cout << idx << " " << last <<" " << ch << endl;
	if(idx == n + 1)
		return 0;

	if(dp[idx][last][ch] != -1)
		return dp[idx][last][ch];
	

	// WE FIX THE CURRENT DIGIT. 
	// WE KNOW THE PREVIOUS DIGIT.

	assert(idx >= 1 && idx <= n);
	assert(last >= 0 && last <= 2);
	assert(ch >= 0 && ch <= 1);

	ll ans = INF;
	FOR(j, 0, 2)
	{
		if(j == ((ch)?arr[idx-1]:last))
			continue;
	//	ans = min(ans, rec(idx + 1, j, 0) + (arr[idx + 1] != j));

		if(rec(idx + 1, j, 0) + (arr[idx] != j) < ans)
		{
			choice[idx][last][ch] = {idx+1, {j, 0}};
			ans = rec(idx+1, j, 0) + (arr[idx] != j);
		}
	}

	if(arr[idx] != ((ch)?arr[idx-1]:last))
	{
		if(rec(idx + 1, last, 1) < ans)
		{
			choice[idx][last][ch] = {idx + 1, {last, 1}};
			ans = rec(idx + 1, last, 1);
		}
	//	ans = min(ans, rec(idx + 1, last, 1));
	}

	return dp[idx][last][ch] = ans;
}

int main()
{
	memset(dp, -1, sizeof(dp));
	cin >> n >> k;
	arr[0] = -1;
//	cout << n << " " << k << endl;
	REP(i, n) cin >> arr[i+1];//, cout << arr[i+1] << " " ;
//	cout << endl;
//	cout << endl;

	pair<ll, pair<ll, ll> > p = {0, {0, 0}};
	REP(i, MX)
	{
		REP(j, 3)
		{
			REP(k, 2)
			{
				choice[i][j][k] = p;
			}
		}
	}

	cout << rec(1, 0, 1) << endl;
//	cout << endl;
//	DEBUG(rec(6, 1, 0)); // Min moves if last is 0 and starting from 4
	ll idx = 1, last = 0, ch = 1;
	while(true)
	{
	//	cout << idx << " " << last << " " << ch << endl;;

		assert(idx >= 1 && idx <= n + 1);
		assert(last >= 0 && last <= 2);
		assert(ch >= 0 && ch <= 1);

		if(idx >= 2)
			cout << ((ch) ? arr[idx - 1] : last) << " ";
		if(idx == n + 1)
			break;


		ll nidx = choice[idx][last][ch].first;
		ll nlast = choice[idx][last][ch].second.first;
		ll nch = choice[idx][last][ch].second.second;

	//	cout << ((nch) ? arr[nidx] : nlast) << " "; 

		idx = nidx, last = nlast, ch = nch;
	}
	cout << endl;
}