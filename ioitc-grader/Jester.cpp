#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 200 + 4;

ll m, n, dp[MX][MX], dp2[MX][MX][2];

ll calc()
{
	memset(dp, 0, sizeof(dp));
	memset(dp2, 0, sizeof(dp2));

	FOR(i, 1, m)
	{
		dp[1][i] = 1;
		dp2[1][i][0] = 1;
	}

	FOR(i, 2, n)
	{
		FOR(cval, 1, m)
		{
			FORD(j, cval, 1)
			{
				dp[i][cval] += (dp[i-1][j])%MOD;
				dp[i][cval] %= MOD;
			}
		}
	}

	FOR(i, 2, n)
	{
		FOR(cval, 1, m)
		{
			FOR(j, cval, m)
			{
				if(j == cval)
				{
					dp2[i][cval][1] += dp2[i-1][j][1];
					dp2[i][cval][1] %= MOD;
					dp2[i][cval][0] += dp2[i-1][j][0];
					dp2[i][cval][0] %= MOD;
				}
				else
				{
					dp2[i][cval][1] += (dp2[i-1][j][1] + dp2[i-1][j][0]) % MOD;
					dp2[i][cval][1] %= MOD;
				}
			}
		}
	}

	FOR(i, 1, n)
	{
		FOR(j, 1, m)
		{
		//	cout << "dp[" << i << "][" << j << "] = " << dp[i][j] << endl;
		//	cout << "dp2[" << i << "][" << j << "][1] = " << dp2[i][j][1] << endl;
		//	cout << "dp2[" << i << "][" << j << "][0] = " << dp2[i][j][0] << endl;
		//	cout << "dp3[" << i << "][" << j << "][1] = " << dp3[i][j][1] << endl;
		//	cout << "dp3[" << i << "][" << j << "][0] = " << dp3[i][j][0] << endl;
 		}
 	//	cout << endl;
	}

	ll ans = 0, inc = 0, dec = 0;
	FOR(i, 1, m)
	{
		ans += dp[n][i];
		ans %= MOD;
		inc += dp[n][i];
		ans += dp2[n][i][1];
		ans %= MOD;
		dec += dp2[n][i][1];
	}

//	cout << "Number of ~~~increasing with length n and first " << endl;
	FOR(i, 1, m)
	{
//		cout << i << " = " << dp2[n][i][1] << endl;
	}



//	cout << "Before Mult" << endl; 
//	DEBUG(ans);

	FOR(i, 1, n-1)
	{
		FOR(j, 1, m)
		{
			// decrease then increase
			ans += (dp2[i][j][1] * dp2[n-i+1][j][1]) % MOD;
			
			ans %= MOD;
		}
	}

	cout << ans%MOD << endl;

}

int main()
{
	// dp[i][j] = Number of i length non-decreasing arrays with i-th element j
	// dp2[i][j][1] = Number of i length ~decreasing arrays that have at least one decrement with last j

	FOR(i, 1, 5)
	{
		FOR(j, 1, 5)
		{
			m = i, n = j;
		//	cout << i << " " << j << endl;
		//	calc();
		//	cout << endl;
		}
	}


	ll q;
	cin >> q;
	REP(x, q)
	{
		cin >> m >> n;
		calc();
		// SUBTRACT m from answer
	}
}