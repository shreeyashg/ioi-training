#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;
ll dp[20][20][20], n, k, total;

ll rec(ll lo, ll hi, ll root) // Number of BSTs with "left" nodes having root [lo, hi]
{
//	cout << lo << " " << hi << " " << left << endl;
	if(lo == hi)
	{
		return 1;
	}

	if(lo > hi)
		return 1;

	if(dp[lo][hi][root] != -1)
		return dp[lo][hi][root];
	
	ll ans = 0;

	FOR(i, lo, root-1)
	{
		FOR(j, root+1, hi)
		{
			// left root is i, right root is j;
			// now distribute
		//	FOR(k, 0, distrib)
		//	{
			//	ans += rec(lo, root-1, k, i) * rec(root+1, hi, distrib-k, j);
		//	}
			ans += rec(lo, root-1, i) * rec(root+1, hi, j);
		}
	}

	if(root == lo)
	{
		ll i = root;
		FOR(j, root+1, hi)
		{
			// left root is i, right root is j;
			// now distribute
			ans += rec(lo, root-1, i) * rec(root+1, hi, j);
		/*	FOR(k, 0, distrib)
			{
				ans += rec(lo, root-1, k, i) * rec(root+1, hi, distrib-k, j);
			}
		*/
		}
	}

	if(root == hi)
	{
		ll j = root;
		FOR(i, lo, root-1)
		{
			// left root is i, right root is j;
			// now distribute
			ans += rec(lo, root-1, i) * rec(root+1, hi, j);
		/*	FOR(k, 0, distrib)
			{
				ans += rec(lo, root-1, k, i) * rec(root+1, hi, distrib-k, j);
			}
		*/
		}
	
	}

	return dp[lo][hi][root] = ans;
}

string str;

void getstring(ll cl, ll cr)
{
	if(cl > cr)
		return;
	if(cl == cr)
	{
		str += (char)(cl-1+'a'); 
		return;
	}

//	DEBUG(tot);
//	DEBUG(k);
	
//	cout << cl << " " << cr << " ";// << k << " ";	

	char cans = (char)('a'+cl-1);
	ll tot = 0, croot = cl;
	FOR(i, cl, cr)
	{
		if(rec(cl, cr, i) < k)
			k-=rec(cl, cr, i);
		else
		{
			cans = (char)((i-1)+'a'), croot = i;
			break;
		}
	}

//	cout << k << " " <<  cans << endl;
	if(cans == 'a')
	{
	/*	DEBUG("----");
		DEBUG(cl);
		DEBUG(cr);
		DEBUG(croot);
		DEBUG(cans);
		DEBUG(k);
		DEBUG("----");
	*/}

//	DEBUG(cans);
	str += cans;
	if(cl <= croot-1) getstring(cl, croot-1);
	if(croot+1 <= cr) getstring(croot+1, cr);
}

int main()
{
//	rec(1, 19, "");	
//	cout << v.size() << endl;
//	for(auto it = s.begin(); it != s.end(); it++) cout << *it << endl;
	memset(dp, -1, sizeof(dp));

	ll sum = 0;
	while(true)
	{
		ll n, root;
		ll cl,cr,root2;
		cin >> cl >> cr;// >> root2;
		FOR(i, cl, cr)
		{
			sum += rec(cl, cr, i);
			cout << cl << " " << cr << " " << i << " " << sum << endl;
		}
	}
	ll tot = 0;

	FOR(r, 1, n)
	{
		tot += rec(1, n, r);
	}	

//	exit(0);

	if(tot < k)
		cout << -1 << endl;

	// search!
	total = tot;
//	DEBUG(total);

	getstring(1, n);
	cout << str << endl;
/*	ll cl = 1, cr = n, croot = -1;
	string str = "";
	while(true)
	{
		ll tot = 0; char cans = ' ';
		FOR(i, 1, n)
		{
		//	DEBUG(tot);
			if(tot + rec(cl, cr, i) > k)
				break;
			if(tot <= k)
				cans = (char)((i-1)+'a'), croot = i;
			tot+=rec(cl, cr, i);
		}
		str += cans;
		cout << cans << endl;
		exit(0);
	}
*/
}