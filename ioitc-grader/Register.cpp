#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e9 + 5, MX = 5e5 + 4;

vector<pair<ll, ll> > v;
ll n, q, a[MX], sortedpos[MX], ogpos[MX], diff[MX], bit[MX];

void update(ll idx, ll val) // Add val to idx
{
	if(idx == 0)
		return;
	while(idx < MX)
		bit[idx]+=val, idx+=(idx & -idx);
}

ll query(ll idx)
{
	ll ans = 0;
	while(idx > 0)
	{
		ans += bit[idx];
		idx -= (idx & -idx);
	}
	return ans;
}

inline ll sum(ll L, ll R) // sum(1, X) returns number of integers <= X
{
	return query(R) - query(L-1);
}

ll findval(ll x)
{
//	DEBUG(pos);
	ll lo = 1, hi = n, ans = -1;
	REP(i, 35)
	{
		if(hi < lo)
			break;
		ll mid = (hi + lo) / 2;
	//	cout << " *** " << mid << " " << sum(1, mid) << endl;
		if(sum(1, mid) >= x)
			ans = mid, hi = mid-1;
		else
			lo = mid + 1;
	}
	return ans;
}

int main()
{
	ios::sync_with_stdio(false);
	cin.tie(0);
	cin >> n >> q;
	REP(i, n)
	{
		ll x, y;
		cin >> x;
		a[i + 1] = x;
		y = i + 1;
		v.push_back({x, y});
	}
//	FOR(i, 0, 55)
//		cout << i << " " << sum(1, i) << endl;
	v.push_back({-124214, 0});
	sort(v.begin(), v.end());
	
	ll prev = 0;
	REP(i, v.size())
	{
		diff[i + 1] = v[i+1].first - prev;
		prev = v[i+1].first;
	}

	FOR(i, 1, n)
	{
		update(i, diff[i]);
	//	update(i+1, -diff[i]);
	//	cout << i << " " << diff[i] << endl;
	}


	FOR(i, 1, n)
	{
		sortedpos[v[i].second] = i;
		ogpos[i] = v[i].second;
	}



	REP(i, q)
	{

	/*	cout << i << endl;
		FOR(i, 1, n)
		{
			cout << sum(i, i) << " ";
		}
		cout << endl;
		FOR(i, 1, n)
		{
			cout << sum(1, i) << " ";
		}
		cout << endl << endl;
	*/
		ll type, val;
		cin >> type >> val;
		if(type == 1)
		{
			
		}
		if(type == 2)
		{
			ll x = findval(val);
			if(x==-1)
				cout << 0 << endl;
			else
				cout << n-x+1 << endl;
		//	cout << n-sum(1, val-1) << endl;
			
		}
		if(type == 3)
		{
			ll x = findval(val);
			if(x == -1)
				continue;
			update(x, -1);
		//	cout << 3 << endl;
		}
	}



}