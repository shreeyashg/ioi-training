#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n, m;
ll s, t, dists[MX], distt[MX];
bool vis[MX];

vector<pair<char, ll> > v[MX];

int main()
{
	cin >> n >> m >> s >> t;
	REP(i, m)
	{
		ll x, y;
		char ch;
		cin >> x >> ch >> y;
		v[x].push_back({ch, y});
		v[y].push_back({ch, x});
	}

	FOR(i, 1, n)
	{
		sort(v[i].begin(), v[i].end());
	}
	queue<ll> q;
	q.push(s);
	memset(dists, -1, sizeof(dists));

	dists[s] = 0;

	while(!q.empty())
	{
		auto tt = q.front();
		q.pop();
	//	DEBUG(tt);
		if(!vis[tt])
			vis[tt]=1;
		else
			continue;

		REP(i, v[tt].size())
		{
			if(dists[v[tt][i].second] == -1)
				dists[v[tt][i].second] = dists[tt] + 1;
			if(!vis[v[tt][i].second])
				q.push(v[tt][i].second);
		}
	}

	FOR(i, 1, n)
	{
	//	cout << i << " " << dists[i] << endl;
	}

	memset(vis, 0, sizeof(vis));

	q.push(t);
	memset(distt, -1, sizeof(distt));

	distt[t] = 0;
	while(!q.empty())
	{
		auto tt = q.front();
		
	//	DEBUG(tt);

		q.pop();
		if(!vis[tt])
			vis[tt]=1;
		else
			continue;

		REP(i, v[tt].size())
		{
			if(distt[v[tt][i].second] == -1)
				distt[v[tt][i].second] = distt[tt] + 1;
			if(!vis[v[tt][i].second])
				q.push(v[tt][i].second);
		}

	}


	memset(vis, 0, sizeof(vis));
	q.push(s);
//	dists[s] = 0;
	string str = "" ;
	ll total = dists[t];
// /	DEBUG(total);
	while(!q.empty())
	{
		vector<ll> curr;
		vector<pair<char,ll> > edges;
		auto tt = q.front();
		ll cv = distt[tt];
		if(tt == t)
			break;
		if(vis[tt])
			continue;

		vis[tt] = 1;
	///	cout << tt << endl;
		q.pop();
//		if(vis[tt])
//			continue;
		curr.push_back(tt);
		while(true)
		{
			if(q.empty() || distt[q.front()] != distt[curr[0]])
				break;
			if(!vis[q.front()])
				curr.push_back(q.front());
	//		cout << q.front() << endl;
			vis[q.front()] = 1;
			q.pop();
		}

	//	cout << "-\nEdges:" << endl;
	//	DEBUG(str);
		REP(i, curr.size())
		{
	//		DEBUG(curr[i]);
			REP(j, v[curr[i]].size())
			{
				ll nxt = v[curr[i]][j].second;
				char val = v[curr[i]][j].first;
	//			DEBUG(nxt);
	//			DEBUG(val);
				if(distt[nxt] == cv - 1 && !vis[nxt])
					edges.push_back(v[curr[i]][j]);
			}
		}

//		cout << "-\nEdges:" << endl;

		if(!edges.empty())sort(edges.begin(), edges.end());
		if(!edges.empty())str += edges[0].first;

		REP(i, edges.size())
		{
//			cout << edges[i].second << " " << edges[i].first << endl;
			if(edges[i].first != edges[0].first)
				break;
			if(!vis[edges[i].second])q.push(edges[i].second);
		}

//		cout << "-----------------------" << endl;
	}

	cout << str << endl;

//	memset(vis, 0, sizeof(vis));

//	memset(vis, 0, sizeof(vis));

//	f(s, 0, ' ');
}