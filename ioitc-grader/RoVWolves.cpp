#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) assert(#x == #x);
#define DB(x) cout << #x << " = " << x << endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll lb;

ll findk(ll tt)
{
	ll lo = 2, hi = 2e9, ans = -1;
	REP(i, 40)
	{
		ll mid = (lo + hi)/2;
		ll curr = mid/2; // pos in list = mid/2
		curr *= (curr + 1);
	//	DEBUG(curr);
		curr -= (mid%2)?(0):mid/2;

	//	DEBUG(mid);
	//	DEBUG(curr);
	//	cout << endl;

		if(curr <= tt)
			ans = mid, lb = curr, lo = mid+1;
		else
			hi = mid-1;
	}
	return ans;
}

inline ll xterms(ll fk)
{
	ll rk = fk, lk = fk;
	DEBUG(rk);
	DEBUG(lk);
	rk/=2, lk/=2;
	DEBUG(rk);
	DEBUG(lk);
	if(rk%2)
	{
		rk++;
		DEBUG(rk);
	}
	if(lk%2)
	{
		lk--;
		DEBUG(lk);
	}
	rk/=2;
	lk/=2;
	DEBUG(rk * rk);
	DEBUG(lk * (lk + 1));
	return (rk * rk) - (lk * (lk + 1));
}

inline ll yterms(ll fk)
{
	ll uk = fk, dk = fk;
	uk /= 2;
	ll ans = 0;
	ll mult = (uk%2)?(uk+1)/2:uk/2;
	if(fk % 2)
	{
		// can add current u
		if(uk % 2)
		{
			ans += ((uk+1)/2) * ((uk+1)/2);
			ans -= (uk/2) * ((uk/2)+1);
		}
		else
		{
			ans += (uk/2) * (uk/2);
			ans -= (uk/2) * ((uk/2)+1);
		}
	}
	else
	{
		// cannot add current u
		if(uk % 2)
		{
			ans += (uk/2) * (uk/2);
			ans -= (uk/2) * ((uk/2) + 1);
		}
		else
		{
			ans += (uk/2) * (uk/2);
			uk--;
			ans -= (uk/2) * ((uk/2) + 1);
		}
	}
	return ans;
}


pair<ll, ll> calc(ll t)
{
	ll fk = findk(t);

	ll nxt = fk % 4;
	t -= lb;

	ll x = xterms(fk), y = yterms(fk);

	if(nxt == 0)
	{
		y -= t;
	}
	if(nxt == 1)
	{
		x += t;
	}
	if(nxt == 2)
	{
		y += t;
	}
	if(nxt == 3)
	{
		x -= t;
	}

	return {x, y};
}

ll calc2(ll x, ll y)
{
//	cout << abs(x) << " " << abs(y) << endl;
	ll gans = 0;
	if((abs(x) > abs(y)) || (abs(x) == abs(y) && x < 0))
	{
		// Find T that gives us the particular x value.
		// i.e find k for which xterms(k) gives abs(x)
	//	DB(x);
	//	DB(y);
		ll lo = 2, hi = 2e9, ans = -1;
		REP(i, 40)
		{
			ll mid = (lo + hi)/2;
			ll val = xterms(mid);
			//double cval = xterms(mid);
			if(abs(val) < abs(x))
			{
				lo = mid + 1;
			}
			else if(abs(val) > abs(x))
			{
				hi = mid - 1;
			}
			else
			{
				// first positive then negative
				if(val == x)
				{
					ans = mid;
					hi = mid - 1;
				}
				else
				{
					if(x > 0)
						hi = mid-1;
					else
						lo = mid+1;
				}
			}
		}

		// if x > 0 then starting y is -abs(x-1)

		// get value of T and get final result.
		ll tval = (ans % 2 == 0) ? (ans/2) * (ans/2) : ((ans+1)/2 * (ans+1/2)) - (ans+1)/2;
	//	DB(tval);  


		if(x > 0)
		{
			ll cval = -x;
			cval++;

			gans = tval + abs(y - cval);
		}
		else
		{
			ll cval = abs(x);

			gans = tval + abs(cval - y);
		}

	//	DB(gans);
		return gans;
	}
	else // abs(y) > abs(x) || (abs(x) == abs(y) && x > 0)
	{
	//	DB("case 2");
	//	DB(x);
	////	DB(y);
		ll lo = 2, hi = 2e9, ans = -1;
		REP(i, 40)
		{
			ll mid = (lo + hi)/2;
			ll val = yterms(mid);

			if(abs(val) < abs(y))
			{
				lo = mid + 1;
			}
			else if(abs(val) > abs(y))
			{
				hi = mid - 1;
			}
			else
			{
				// first positive then negative
				if(val == y)
				{
					ans = mid;
					hi = mid - 1;
				}
				else
				{
					if(y > 0)
						hi = mid-1;
					else
						lo = mid+1;
				}
			}
		}
		

		// get value of T!!
		ll tval = (ans % 2 == 0) ? (ans/2) * (ans/2) : (((ans+1)/2) * ((ans+1)/2)) - ((ans+1)/2);
	//	DB(tval);

		if(y > 0)
		{
			ll cval = abs(y);
			gans = tval + (cval - x);
		}
		else
		{
			ll cval = y;
			gans = (tval + (x - cval));
		}

	//	DB(gans);
		return gans;
	}
}

// next move modulo 4:
// 0 = Down
// 1 = R
// 2 = U
// 3 = L

int main()
{
 	// Upper_bound

//	calc2(0, 0);

 	FOR(i, 1, 110)
 	{
 	//	cout << i << " " << yterms(i) << endl;
 	//	ll x = i;
 	//	cout << xterms(x) << endl;
 	//	cout << x << " == " << (calc(x).first) << " " << (calc(x).second) << " MAX = " << ((max(abs(calc(x).second), abs(calc(x).first)) == abs(calc(x).first)) ? calc(x).first : calc(x).second) << endl;
 	//	cout << i << " " << calc(i).first << " " << calc(i).second << endl;
 	}

 	ll q;
 	cin >> q;

	REP(i, q)
	{
		ll type;
		cin >> type;
		if(!type)
		{
			ll x;
			cin >> x;
			cout << calc(x).first << " " << calc(x).second << endl;
		}
		else
		{
			ll x, y;
			cin >> x >> y;
		//	pair<ll, ll> qf = {x, y};

			// take care of t <= 10 and stuff

			cout << calc2(x, y) << endl;
		}
	}

}