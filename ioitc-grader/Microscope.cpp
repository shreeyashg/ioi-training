#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll gr, gc, r, c, k;
vector<pair<ll, ll> > v(MX);
ll cr, cc;
pair<ll, ll> nexts, topleft;

inline bool inblock()
{
	if(nexts.first >= topleft.first && nexts.first <= cr)
	{
		if(nexts.second >= topleft.second && nexts.second <= cc)
			return true;
	}
	return 0;
}

ll fixrow()
{
	if(nexts.first < topleft.first)
	{
		ll diff = topleft.first - nexts.first;				
		topleft.first -= diff;
		cr -= diff;
		return diff;
	}
	else
	{
		ll diff = nexts.first - cr;
		topleft.first += diff;
		cr += diff;
		return diff;
	}
}

ll fixcol()
{
	if(nexts.second > cc)
	{
		ll diff = nexts.second - cc;
		cc += diff;
		topleft.second += diff;
		return diff;
	}
	if(nexts.second < topleft.second)
	{
		ll diff = topleft.second - nexts.second;
		topleft.second -= diff;
		cc -= diff;
		return diff;
	}
}
 
ll move()
{
	if(inblock())
		return 0;

	ll ans = 0;

	if(!(nexts.first >= topleft.first && nexts.first <= cr)) // Not in those rows.
		ans += fixrow();
	

	if(!(nexts.second >= topleft.second && nexts.second <= cc)) // Not in those cols
		ans += fixcol();

	return ans;
}


int main()
{
	cin >> gr >> gc >> r >> c >> k;
	REP(i, k)
	{
		cin >> v[i].first >> v[i].second;
	}
	gr--; gc--; r--; c--;
	cr = r, cc = c;
	topleft = {0, 0};
	ll ans = 0;
	REP(i, k)
	{
	//	cout << i + 1 << endl;
	//	cout << v[i].first <<" "  << v[i].second << endl << endl;
	//	cout << topleft.first << " " << topleft.second << endl;
	//	cout << cr << " " << cc << endl;
		nexts = v[i];
		ll x = move();
		ans += x;
	//	cout << x << endl << endl << endl;
	}

	cout << ans << endl;
}