#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 3e4 + 4, CMX = 3e6 + 4, kx = 2e5 + 4, sz = 173;

// Squareroot decomposition
// Store frequency table for each block in query range

ll n, q, arr[MX], f[CMX], anss[kx], ans;
vector<pair<pair<ll, ll>, ll> > queries;

bool cmp(pair<pair<ll, ll>, ll>  x, pair<pair<ll, ll>, ll>  y)
{
	if(x.first.first/sz == y.first.first/sz)
		return x.first.second < y.first.second;
	else
		return x.first.first/sz < y.first.first/sz;
}

void add(ll idx)
{
//	cout << "add " << idx << endl;
	if(!f[arr[idx]])
		ans++;
	f[arr[idx]]++;
}

void del(ll idx)
{
//	cout << "del " << idx << endl;
	if(f[arr[idx]]==1)
		ans--;
	f[arr[idx]]--;
}

int main()
{
	ios::sync_with_stdio(false);
	cin.tie(0);
	cin >> n;
	REP(o, n) cin >> arr[o];
	cin >> q;
	REP(ff, q)
	{
		ll x, y;
		cin >> x >> y;
		queries.push_back({{--x, --y}, ff});
	}

	sort(queries.begin(), queries.end(), cmp);
//	REP(i, queries.size()) cout << queries[i].first.first << " " << queries[i].first.second << endl;

	ll cl = 0, cr = -1;
	REP(x, queries.size())
	{
	//	cout << endl;
		ll l = queries[x].first.first, r = queries[x].first.second;
	//	DEBUG(l);
	//	DEBUG(r);
	//	DEBUG(cl);
	//	DEBUG(cr);
		while(cr < r)
		{
			add(cr+1);
			cr++;
		}
		while(cl > l)
		{
			add(cl-1);
			cl--;
		}

		while(cr > r)
		{
			del(cr);
			cr--;
		}

		while(cl < l)
		{
			del(cl);
			cl++;
		}
	//	DEBUG(ans);
	//	cout << endl;
		anss[queries[x].second] = ans;
	}
//	DEBUG(cl);
//	DEBUG(cr);

	REP(i, queries.size()) cout << anss[i] << endl;
}