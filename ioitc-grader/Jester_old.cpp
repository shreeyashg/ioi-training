#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 5e3 + 4;

ll m, n, dp[MX][MX][3][2];

int main()
{
	m = 2, n = 2;

//	dp[n][m][f][g] = No. of arrays of length n with last number m. 
// 	f = 0 signifies decreasing, f = 1 signifies increasing, f = 2 signifies same
//	g = 0 global decreasing, g = 1 global increasing

//	TO GO FROM G = 0 TO G = 1, MUST ADD 1
//	CAN GO FROM F = 0, 1 TO F = 2 AND BACK (BY USING G).


	FOR(i, 1, m)
	{
		dp[1][i][0][0] = 1;
	//	dp[1][i][][1] = 1;
		dp[1][i][2][0] = 1;

		dp[1][i][1][1] = 1;
		dp[1][i][2][1] = 1;
	}

	FOR(len, 2, n)
	{
		FOR(val, 1, m)
		{
			DEBUG(len);
			DEBUG(val);

			// f signifies future val

			FORD(poss, val-1, 1)
			{
				dp[len][val][1][1] += (dp[len-1][poss][1][1]);
				cout << len << " " << val << " 1 " << 1 << " gets " << dp[len-1][poss][1][1]  << " from " << len - 1 << " " << poss << " 1 " << 1 << endl;
			//	cout << len << " " << val << " 1 " << 1 << " gets " << dp[len-1][poss][2][1] << " from " << len - 1 << " " << poss << " 2 " << 1 << endl;
			}

			dp[len][val][2][1] += (dp[len-1][val][2][1] + dp[len-1][val][1][1]);
			cout << len << " " << val << " 2 " << 1 << " gets " << dp[len-1][val][2][1] << " from " << len - 1 << " " << val << " 2 " << 1 << endl;
			cout << len << " " << val << " 2 " << 1 << " gets " << dp[len-1][val][1][1] << " from " << len - 1 << " " << val << " 1 " << 1 << endl;
			dp[len][val][2][0] += (dp[len-1][val][0][0] + dp[len-1][val][2][0]);
			cout << len << " " << val << " 2 " << 0 << " gets " << dp[len-1][val][0][0] << " from " << len - 1 << " " << val << " 0 " << 0 << endl;
			cout << len << " " << val << " 2 " << 0 << " gets " << dp[len-1][val][2][0] << " from " << len - 1 << " " << val << " 2 " << 0 << endl;

			FOR(poss, val+1, m)
			{
				dp[len][val][0][0] += (dp[len-1][poss][0][0]);
				cout << len << " " << val << " 0 " << 0 << " gets " << dp[len-1][poss][0][0] << " from " << len - 1 << " " << poss << " 0 " << 0 << endl;
				cout << len << " " << val << " 0 " << 0 << " gets " << dp[len-1][poss][2][0] << " from " << len - 1 << " " << poss << " 2 " << 0 << endl;
			}

			cout << "dp[len][val][0][0] is " << dp[len][val][0][0] << endl;
			cout << "dp[len][val][0][1] is " << dp[len][val][0][1] << endl;
			cout << "dp[len][val][1][0] is " << dp[len][val][1][0] << endl;
			cout << "dp[len][val][1][1] is " << dp[len][val][1][1] << endl;	
			cout << "dp[len][val][2][0] is " << dp[len][val][2][0] << endl;
			cout << "dp[len][val][2][1] is " << dp[len][val][2][1] << endl;
			cout << " --  --  --  -- " << endl;

		}
	}

	cout << " ' ' ' ' ' ' " << endl;

	ll ans = 0;
//	FOR(i, 1, m) ans += (dp[n][i][0] + dp[n][i][1]);

//	cout << ans << endl;

	ll q;
	cin >> q;
	REP(x, q)
	{
		ll m, n;
		cin >> m >> n;

		// SUBTRACT m from answer
	}
}