#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e8 + 7, INF = 1e18, MX = 5e5 + 4;

bool th = 0; // 1 if char {pqrs, wxyz}
string s;
ll n, len, sz, dp[MX], dp_f[MX];

map<ll, string> mp;
map<char, ll> button, dist;  

string sh(string str)
{
	string ans = "";
	REP(i, str.size())
	{
		char but = (char)('0' + button[str[i]]);
		ll dis = dist[str[i]];
		REP(ii, dis) ans += but;
	}
	return ans;
}

void init()
{
	dp[0] = 0;
	dp[1] = 1;
	dp[2] = 2;
	FOR(i, 3, MX-1) dp[i] = (1 + dp[i-1] + dp[i-2] + dp[i-3]) % MOD;

	dp_f[0] = 0;
	dp_f[1] = 1;
	dp_f[2] = 2;
	dp_f[3] = 4;
	FOR(i, 4, MX-1) dp_f[i] = (1 + dp_f[i-1] + dp_f[i-2] + dp_f[i-3] + dp_f[i-4]) % MOD;
}

int main()
{
	mp[2] = "abc";
	mp[3] = "def";
	mp[4] = "ghi";
	mp[5] = "jkl";
	mp[6] = "mno";
	mp[7] = "pqrs";
	mp[8] = "tuv";
	mp[9] = "wxyz";

	FOR(i, 2, 9)
	{
		REP(ii, mp[i].size()) dist[mp[i][ii]] = ii + 1, button[mp[i][ii]] = i;
	}

	/*
	FOR(i, 1, 15)
	{
		len = i;
		f(0, "");
		cout << i << " " << ss.size() << " " << rec(len) << endl;
		ss.clear();
	}
	*/
	//FOR(i ,1, 10) cout << i << " " << rec(i) << endl;
//	exit(0);
	init();
	cin >> n;
	REP(f, n)
	{
	//	string s;
		cin >> s;
		string sfa = sh(s);
		s = sfa;
	//	cout << s << endl;
		char ch = s[0];
		ll ans = 1;
		len = 0;
		REP(i, s.size())
		{
			if(s[i] != ch)
			{
				if((ch == '7' || ch == '9'))
					th = 1;
				else
					th = 0;
			//	
			//	DEBUG(s[i]);
			//	cout << len << endl;
		//		assert(len >= 0 && len <= MX-1);
				ll x = (th) ? dp_f[len] : dp[len];
				ans = (ans%MOD * x%MOD)%MOD;
				ch = s[i], len = 0;
			}
			len++;
		}
		ch = s[s.size()-1];
		if((ch == '7' || ch == '9'))
			th = 1;
		else
			th = 0;

	//	DEBUG(len);
	//	cout << len << endl;
		ans = (ans%MOD * ((th) ? dp_f[len] : dp[len])%MOD)%MOD;

		cout << ans%MOD << endl;
	}

}