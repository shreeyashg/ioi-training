#!/bin/bash

#final1-06
#final2-07
#final3-08

# Use 1-2-3- followed by problem name

cp Jester.cpp /home/shreeyash/IOI/IOITC\ 2013\ Solutions/testdata/final1-06may2013/2-Jester/testdata/
cd /home/shreeyash/IOI/IOITC\ 2013\ Solutions/testdata/final1-06may2013/2-Jester/testdata/

g++ -std=c++11 Jester.cpp

for i in *.in
do
    ./a.out < $i > temp.txt
    if diff "${i%.in}.out" "temp.txt" > /dev/null ; then
    	 	echo "$i Correct Answer"
    	 	((ac++))
	else
 		echo "$i Wrong Answer"
 		((wa++))
	rm temp.txt
	#echo "ac = $ac | wa = $wa"
	fi
done
#two styles, comment out the one you don't need.

#use this if files are like
#in0.txt, out0.txt, etc.
#change 0/10 to whatever lower/upper bound is

#echo "ac = $ac | wa = $wa"

#use this if files are like
#x.in, x.out (where x is any string)
#you can even change .in/.out to any file
#extension, as long as they are different.
#for i in *.in
#do
#       time ./a.out < $i > temp.txt
#        diff temp.txt ${i%.in}.out
#        rm temp.txt
#done