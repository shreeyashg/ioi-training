#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 5e4 + 4;

ll val[150], dp[MX][11][5];
ll m, k, r;
string str;

ll f(ll idx, ll form, ll last)
{
//	cout << idx << " " << form << " " << last << endl;
	if(form <= 0)
		return 0;
	if(idx == m && form >= 1)
		return 1;
	else if(idx == m)
		return 0;
 
	if(dp[idx][form][last] != -1)
		return dp[idx][form][last];

	ll ans = 0;
	if(str[idx] != 'N')
	{
//		DEBUG("!=N");
		if(val[str[idx]] >= last)
		{
			ans += f(idx + 1, form, val[str[idx]]);
		}
		else
		{
			ans += f(idx + 1, form-1, val[str[idx]]);
		}
	}
	else
	{
		FOR(i, 1, 4)
		{
			if(i >= last)
			{
				ans += f(idx + 1, form, i);
			}
			else
			{
				ans += f(idx + 1, form-1, i);
			}
		}
	}
//	DEBUG(ans);
	return dp[idx][form][last] = ans;
}

int main()
{
	memset(dp, -1, sizeof(dp));

	val['A'] = 1;
	val['C'] = 2;
	val['G'] = 3;
	val['T'] = 4;

	cin >> m >> k >> r;
	cin >> str;

	assert(f(0, k, 0) >= r);

//	cout << f(0, k, 0) << endl;
}