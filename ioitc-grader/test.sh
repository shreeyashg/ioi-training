#!/bin/bash

cp Register.cpp ~/IOI/IOITC\ 2012/tests/test3-tqueue/data/
cd ~/IOI/IOITC\ 2012/tests/test3-tqueue/data/

g++ -std=c++11 -O2 Register.cpp

#two styles, comment out the one you don't need.

#use this if files are like
#in0.txt, out0.txt, etc.
#change 0/10 to whatever lower/upper bound is

for i in *.in
do
    ./a.out < $i > temp.txt
    if diff "${i%.in}.out" "temp.txt" > /dev/null ; then
    	 	echo "$i Correct Answer"
    	 	((ac++))
	else
 		echo "$i Wrong Answer"
 		((wa++))
	rm temp.txt
	#echo "ac = $ac | wa = $wa"
	fi
done

#echo "ac = $ac | wa = $wa"

#use this if files are like
#x.in, x.out (where x is any string)
#you can even change .in/.out to any file
#extension, as long as they are different.
#for i in *.in
#do
#       time ./a.out < $i > temp.txt
#        diff temp.txt ${i%.in}.out
#        rm temp.txt
#done