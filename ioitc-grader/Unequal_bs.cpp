#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e6 + 4;

ll n, arr[MX], ans, k;

int main()
{
	cin >> n >> k;
	REP(i, n) cin >> arr[i+1];

	arr[n+1] = 1e8;
	arr[0] = 1e8;

	if(k == 1)
	{

	}

	if(k == 2)
	{

	}

	FOR(i, 2, n)
	{
		if(arr[i-1] == arr[i])
		{

			// can either change i-1 or i
			
			// TRY CHANGING I-1
		//	DEBUG(i);
			bool f = 1;

			if(i == n)
			{
				FOR(j, 0, 2)
				{
					if(arr[i-2] != j && arr[i]!=j)
					{
						arr[i-1] = j;
						ans++;
						f = 0;
						break;
					}
				}
			}


			if(!f)
				continue;

			FOR(j, 0, 2)
			{
				FOR(k, 0, 2)
				{
					// j is arr[i-1], k is arr[i]

					if(j != k)
					{
						if(arr[i+1]!=k)
						{
							if(arr[i-2]!=j)
							{
								ans += ((j!=arr[i-1]) + (k!=arr[i]));

								arr[i-1]=j;
								arr[i]=k;
								f=0;
								break;
							}
						}
					}
				}
				if(!f)
					break;
			}
		}
	}

	cout << ans << endl;

	FOR(i, 1, n)
	{
		cout << arr[i] << " " ;
	}

	cout << endl;
}