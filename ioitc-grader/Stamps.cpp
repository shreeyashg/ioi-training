#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4, NX = 1e4 + 4;

vector<string> stamps;
string target;
vector<ll> strpos[11][27];
ll temp[27], dp[MX];

ll cnt = 0;

ll f(ll pos)
{
//	cout << pos << endl;

//	cnt++;
	if(pos >= target.size())
		return 0;

	if(dp[pos] != -1)
		return dp[pos];

	ll ans = INF;
	REP(i, stamps.size())
	{
	//	FOR(j, 0, 25) temp[j] = 0;
		ll nxt = pos, curr = -1;
		while(true)
		{
//			cnt++;

		//	assert(nxt <= target.size());
			// Try to get as much as possible from current string
			if(nxt == target.size() || curr + 1 == stamps[i].size())
				break;

			// find next valid element in array
			auto it = upper_bound(strpos[i][target[nxt]-'a'].begin(), strpos[i][target[nxt]-'a'].end(), curr);
			if(it == strpos[i][target[nxt]-'a'].end())
				break;
			curr = *it;
		//	DEBUG(i);
		//	DEBUG(curr);
		//	temp[target[nxt]-'a']++;
			nxt++;
		}

		if(nxt != pos)
			ans = min(ans, 1 + f(nxt));
	}
	return dp[pos] = ans;

}

int main()
{
	memset(dp, -1, sizeof(dp));
	ios::sync_with_stdio(false);
	cin.tie(0);

	ll x;
	cin >> x;
	ll y;
	cin >> y >> target;

	REP(i, x)
	{
		cin >> y;
		string s;
		cin >> s;
		stamps.push_back(s);
	
		cnt++;
	}

	REP(i, stamps.size())
	{
		REP(j, stamps[i].size())
		{
			strpos[i][stamps[i][j]-'a'].push_back(j);
		}
		cnt++;
	}

	ll pos = 0;
	REP(i, stamps.size())
	{
		while(true)
		{
			char ch = target[pos];
			if()
		}
	}

	cout << ((f(0) >= INF) ? -1 : f(0)) << endl;
	assert(cnt <= 1e7);
}