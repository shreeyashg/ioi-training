#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 5e3 + 4;

ll n, m, k, arr[MX];
bool vis[MX];
vector<ll> v[MX];
vector<pair<pair<ll, ll>, pair<ll, ll> > >edges;
map<pair<ll, ll>, ll> mp, exists;

void f(ll idx)
{
	if(vis[idx])
		return;
	vis[idx] = 1;
	REP(i, v[idx].size())
	{
		if(!vis[v[idx][i]] && mp[{idx, v[idx][i]}] >= k)
		{
			f(v[idx][i]);
		}
	}
}

bool check()
{
	REP(i, n) vis[i] = 0;
	f(0);
//	bool chk = 0;
	REP(i, n)
	{
		if(!vis[i])
			return 0;
	}
	return 1;
}

ll bs()
{
	ll lo = 1, hi = 1e9, ans = 1e9;
	REP(i, 35)
	{
		k = (lo + hi)/2;
	//	DEBUG(k);
		if(check())
			lo = k + 1, ans = k;
		else
			hi = k - 1;
	}
	return ans;
}

ll find(ll x) { return arr[x] = (arr[x] != x) ? find(arr[x]) : x; }
void uni(ll x, ll y) {((x == y) ? : (rand() % 2) ? arr[find(x)] = find(y) : arr[find(y)] = find(x));}

int main()
{
	cin >> n >> m;

	// 1. Binary search on answer.
	// 2. Find MST on the new graph
	REP(i, n) arr[i] = i;
//	ll f =0;
	REP(x, m)
	{
		ll i, j, q, l;
		cin >> i >> j >> q >> l;

	//	f = q;
		v[i].push_back(j);
		v[j].push_back(i);
		edges.push_back({{l, q}, {i, j}});
		edges.push_back({{l, q}, {j, i}});
		if(exists[{i, j}])
			mp[{i, j}] = max(mp[{i, j}], q), mp[{j, i}] = max(mp[{i, j}], q);
		else
			mp[{i, j}] = q, mp[{j, i}] = q, exists[{i, j}] = 1;
	}

	sort(edges.begin(), edges.end());
//	reverse(edges.begin(), edges.end());

	k = bs();
//	k = 7;
	// Construct MST
	ll ans = 0;
	REP(i, edges.size())
	{
		if(edges[i].first.second < k || edges[i].second.first == edges[i].second.second || find(edges[i].second.first) == find(edges[i].second.second))
			continue;
		else
			uni(edges[i].second.first, edges[i].second.second), ans += edges[i].first.first;
	}

	ll ff = find(0);
	REP(i, n)
	{
		if(find(i) != ff)
			cout << -1 << endl, exit(0);
	}

	cout << k << " " << ans << endl;
}