#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 5e3 + 4;

ll n;
vector<ll> arr;
ll mins[MX][MX], maxes[MX][MX], dp[MX];
bool sorted[MX][MX];

/*

ll rec(ll l, ll r)
{
	if(l == r)
		return 0;
	
	if(sorted[l][r])
	{
		return 0;
	}

	if(dp[l][r] != -1)
		return dp[l][r];

	ll ans = INF;
	ll c1 = 0, c2 = 0;

	c1 = (arr[l] > mins[l+1][r]) ? arr[l] : 0; 
	c2 = (arr[r] < maxes[l][r-1]) ? arr[r] : 0;

	ans = min(rec(l+1, r) + c1, rec(l, r-1) + c2);
	
	return dp[l][r] = ans;
}

*/

ll f(ll idx)
{
	if(dp[idx] != -1)
		return dp[idx];

	ll ans = 0;
	FOR(i, idx+1, n-1)
	{
		if(arr[i] >= arr[idx])
			ans = max(ans, arr[i] + f(i));
	}

	return dp[idx] = ans;
}

int main()
{
	memset(dp, -1, sizeof(dp));
	cin >> n;
	ll sum = 0;
	arr.push_back(0);
	REP(i, n)
	{
		ll x;
		cin >> x;
		sum += x;
		arr.push_back(x);
	}
	
	cout << sum-f(0) << endl;

	exit(0);

/*

	for(ll jump = 0; jump <= n; jump++)
	{
		for(ll i = 1; i + jump <= n; i++)
		{
			if(jump == 0)
			{
				mins[i][i] = arr[i], maxes[i][i] = arr[i], sorted[i][i] = 1;
				continue;
			}

			mins[i][i+jump] = min(arr[i+jump], mins[i][i+jump-1]);
			maxes[i][i+jump] = max(arr[i+jump], maxes[i][i+jump-1]);
			if(arr[i+jump] >= arr[i+jump-1])
				sorted[i][i+jump] |= sorted[i][i+jump-1];
			else
				sorted[i][i+jump] = 0;

		//	cout << i << " " << i + jump << endl;
		//	DEBUG(mins[i][i+jump]);
		//	DEBUG(maxes[i][i+jump]);
		//	DEBUG(sorted[i][i+jump]);
		}
	}

	cout << rec(1, n) << endl;
*/}