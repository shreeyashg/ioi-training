#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e6 + 4;

ll n, m;
short vis[MX], col[MX];
vector<int> v[MX];
vector<int> ele;
bool f = 1; 

inline ll calc(ll x)
{
	return x * (x - 1);
}

void dfs(int idx, short last)
{
	if(vis[idx])
		return;
	vis[idx] = 1, col[idx] = (last == 1) ? 2 : 1;
	ele.push_back(idx);
	REP(i, v[idx].size())
	{
		if(col[idx] == col[v[idx][i]])
			f = 0;
		else
			dfs(v[idx][i], col[idx]);
	}
}

void ff(int idx, short last)
{
	stack<pair<ll, ll> >s;
	s.push({idx, last});
	while(!s.empty())
	{
		auto x = s.top();
		s.pop();

		ll idx = x.first, last = x.second;
		if(vis[idx])
			continue;
		vis[idx] = 1, col[idx] = (last == 1) ? 2 : 1;
		ele.push_back(idx);
		REP(i, v[idx].size())
		{
			if(col[idx] == col[v[idx][i]])
				f = 0;
			else
				s.push({v[idx][i], col[idx]});
		}
	}
}

int main()
{

	cin >> n >> m;
	REP(i, m)
	{
		ll x, y;
		cin >> x >> y;
		v[x].push_back(y);
		v[y].push_back(x);
	}

	ll ans = 0;

//	dfs(0, 0);
//	REP(i, n) cout << i << " " << col[i] << " " << vis[i] << endl;
//	DEBUG(f);
//	DEBUG(ele.size());
	FOR(i, 0, n-1)
	{
		if(!vis[i])
		{
			f = 1;
			if(ele.size()) ele.clear();
			ff(i, 0);
		//	ll x = ele.size();
		//	if(f) ans += (2*(calc(x/2)));
		//	else ans += (calc(x));
		

			// check bipartite, do calc

			ll e = 0, o = 0;
			REP(i, ele.size())
			{
				if(col[ele[i]] % 2)
					o++;
				else
					e++;
			}

			ll x = ele.size();
			if(f) ans += ((calc(e)) + (calc(o)));
			else ans += (calc(x));
		}
	}

	cout << ans + n << endl;
}