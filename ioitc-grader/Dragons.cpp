#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll r, c, k, d;
vector<pair<ll, ll> > v;
ll dp[305][305];


ll rec(ll idx, ll num)
{
//	cout << idx << " " << num << endl;
	if(dp[idx][num] != -1)
		return dp[idx][num];
	
	if(idx == d)
		return (num != k) * INF;

	if(num == k)
		return 0;

	ll ans = INF;
	FOR(i, idx + 1, d)
	{
	//	cout << endl;
	//	cout << i << " " << idx << endl;
	//	DEBUG(abs(v[i].second - v[idx].second));
	//	DEBUG(abs(v[i].first - v[idx].first));
	//	DEBUG(rec(i, num + 1));
	//	cout << endl;
		ans = min(ans, abs(v[i].second - v[idx].second) + abs(v[i].first - v[idx].first) + rec(i, num + 1));
	}

	return dp[idx][num] = ans;
}

int main()
{
	memset(dp, -1, sizeof(dp));
	cin >> r >> c >> k >> d;
	v.push_back({0, 0});
	REP(i, d)
	{
		ll x, y;
		cin >> x >> y;
		v.push_back({x, y});
	}

	sort(v.begin(), v.end());

	cout << rec(0, 0) << endl;
}