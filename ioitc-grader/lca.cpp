#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) assert(#x == #x);
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4, LN = 18;

ll pa[MX][LN], depth[MX], n, cnt, m;
vector<ll> v[MX];

void build_LCA()
{
	REP(i, LN)
	{
		FOR(j, 1, n)
		{
			if(pa[j][i] != -1)
				pa[j][i+1] = pa[pa[j][i]][i];
		}
	}
}

void print()
{
	FOR(i, 1, n)
	{
		FOR(j, 0, 3)
		{
			if(pa[i][j] != -1)cout << "pa[" << i << "][" << j << "] = " << pa[i][j] << endl;
		}
	}
}

ll LCA(ll u, ll v)
{
	if(depth[u] < depth[v])
		swap(u, v);

	ll diff = depth[u] - depth[v];
	DEBUG(diff);
	FORD(c, LN-1, 0)
	{
		if(pa[u][c] == -1 || (1 << c) > diff)
			continue;
		u = pa[u][c], diff -= (1 << c);
	}

//	cout << u << " " << v << endl;

	if(u == v)
		return u;
	
	FORD(c, LN-1, 0)
	{
		if(pa[u][c] != -1 && pa[v][c] != -1 && pa[u][c] != pa[v][c])
			u = pa[u][c], v = pa[v][c];
	}

	return (u == v) ? u : pa[u][0];
}

void init(ll idx, ll prev)
{
//	cout << idx << " " << prev << endl;
	pa[idx][0] = prev;
	depth[idx] = (prev != -1) ? depth[prev] + 1 : 0;

	REP(i, v[idx].size())
	{
		if(v[idx][i] != prev)
			init(v[idx][i], idx);
	}
}

int main()
{
	ll t;
	cin >> t;
	while(t--)
	{
		cout << "Case " << ++cnt << ":" << endl;
		memset(pa, -1, sizeof(pa));
		memset(depth, 0, sizeof(depth));
		cin >> n;
		FOR(i, 1, n)
		{
			v[i].clear();
			ll sz;
			cin >> sz;

			REP(ii, sz)
			{
				ll k; cin >> k;
				v[i].push_back(k);
				v[k].push_back(i);
			}
		}

		init(1, -1);
		build_LCA();

		cin >> m;
		REP(i, m)
		{
			ll x, y;
			cin >> x >> y;
			DEBUG(x);
			DEBUG(depth[x]);
			DEBUG(y);
			DEBUG(depth[y]);
			cout << LCA(x, y) << endl;
		}
	}
}