#pragma GCC optimize("-O5")
#include <bits/stdc++.h>
using namespace std;

typedef int ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) assert(#x == #x)
#define DEBUGs(x) cout << #x <<" = " << x << endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e9, MX = 1e5 + 4, LN = 20, B = 315;

// Use Kruskal's to get MST.
// Do jump pointers on MST to answer max LCA queries.

ll n, gd, m, q, s, t, arr[MX], pa[MX][21], val[MX][21], depth[MX];
bool vis[MX];
vector<pair<ll, ll> > v[MX], mst[MX], queries;
vector<pair<ll, pair<ll, ll> > > edges;

ll find(ll x) {return arr[x] = (arr[x] == x) ? x : find(arr[x]);}
void uni(ll x, ll y) { (x == y) ? : (rand() % 2) ? arr[find(x)] = find(y) : arr[find(y)] = find(x);}

void build_LCA()
{
	REP(i, LN)
	{
		FOR(j, 1, n)
		{
			if(pa[j][i] != -1)
				pa[j][i + 1] = pa[pa[j][i]][i], val[j][i + 1] = max(val[j][i], val[pa[j][i]][i]);
		}
	}
}


void print()
{
	FOR(i, 1, n)
	{
		FOR(j, 0, 3)
		{
			if(pa[i][j] != -1)cout << "pa[" << i << "][" << j << "] = " << pa[i][j] << endl;
		}
	}

	FOR(i, 1, n)
	{
		FOR(j, 0, 3)
		{
			if(pa[i][j] != -1)cout << "val[" << i << "][" << j << "] = " << val[i][j] << endl;
		}
	}	
}


ll LCA(ll u, ll v)
{
	if(depth[u] < depth[v])
		swap(u, v);

	ll diff = depth[u] - depth[v];
	FORD(c, LN-1, 0)
	{
		if(pa[u][c] == -1 || (1 << c) > diff)
			continue;
		u = pa[u][c], diff -= (1 << c);
	}

//	cout << u << " " << v << endl;

	if(u == v)
		return u;
	
	FORD(c, LN-1, 0)
	{
		if(pa[u][c] != -1 && pa[v][c] != -1 && pa[u][c] != pa[v][c])
			u = pa[u][c], v = pa[v][c];
	}

	return (u == v) ? u : pa[u][0];
}


void init(ll idx, ll prev, ll cost)
{
	if(vis[idx])
		return;
	vis[idx] = 1;
	pa[idx][0] = prev;
	if(cost != -1)
		val[idx][0] = cost;
	depth[idx] = (prev != -1) ? depth[prev] + 1 : 0;

	REP(i, mst[idx].size())
	{
		if(mst[idx][i].first != idx)
			init(mst[idx][i].first, idx, mst[idx][i].second);
	}
}

ll calc(ll node, ll anc)
{
	if(node == anc)
		return 0;

	ll ans = val[node][0];
	ll diff = depth[node] - depth[anc];

	FORD(i, LN-1, 0)
	{
	//	cout << i << " node = " << node << " ans = " << ans << " diff = " << diff << endl;
		if(pa[node][i] != -1 && (1 << i) <= diff)
		{
			ans = max(ans, val[node][i]);
			node = pa[node][i];
			diff -= (1 << i);
		}
	}

	return ans;
}

int main()
{
	ios::sync_with_stdio(false);
	cin.tie(0);
	memset(pa, -1, sizeof(pa));
	memset(val, -1, sizeof(val));
	cin >> n >> m;
//	cout << n << " " << m << endl;
	FOR(i, 1, n) arr[i] = i;
	REP(i, m)
	{
		ll x, y, c;
		cin >> x >> y >> c;
		v[x].push_back({y, c});
		v[y].push_back({x, c});
		edges.push_back({c, {x, y}});
	}

	sort(edges.begin(), edges.end());

	REP(i, edges.size())
	{
		ll c = edges[i].first, x = edges[i].second.first, y = edges[i].second.second;

		if(find(x) == find(y))
			continue;
		else
			uni(x, y), mst[x].push_back({y, c}), mst[y].push_back({x, c});
	}

	init(1, -1, -1);


	build_LCA();
//	print();
//	cout << "DOEN" << endl;

//	DEBUGs(val[7][1]);
//	DEBUGs(calc(7, 1));// << endl;
//	return 0;
	cin >> q;
	REP(i, q)
	{
		ll x, y;
		cin >> x >> y;
	//	cout << x << " " << y << endl;
		DEBUG(x);
		DEBUG(y);
		ll lca = LCA(x, y);
		DEBUG(lca);
		DEBUG(calc(x, lca));
		DEBUG(calc(y, lca));

		cout << max(calc(x, lca), calc(y, lca)) << endl;
	}

//	cout << "DOEN" << endl;
	// Calculate LCA

}