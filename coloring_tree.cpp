#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n, m, r, val[MX], sz[MX], loc[MX];
vector<ll> v[MX];
set<ll> s[MX];
bool vis[MX];

void calc(ll idx)
{
	if(vis[idx])
		return;
	vis[idx] = 1;

	if(v[idx].size() == 1 && (idx != r || n == 1))
	{
		s[idx].insert(val[idx]);
		sz[idx] = 1;
		return;
	}

	REP(i, v[idx].size())
	{
		if(!vis[v[idx][i]])
			calc(v[idx][i]);
	}

	ll cmax = 0, cidx = 0;
	REP(i, v[idx].size())
	{
		if(s[loc[v[idx][i]]].size() > cmax)
			cmax = s[loc[v[idx][i]]].size(), cidx = loc[v[idx][i]];
	}

	REP(i, v[idx].size())
	{
		if(loc[v[idx][i]] != cidx)
		{
			for(auto it = s[loc[v[idx][i]]].begin() ; it != s[loc[v[idx][i]]].end(); it++)
			{
				s[cidx].insert(*it);
			}
			s[loc[v[idx][i]]].clear();
		}
	}


	loc[idx] = cidx;
	s[cidx].insert(val[idx]);
	sz[idx] = s[cidx].size();
}

int main()
{

	memset(vis, 0, sizeof(vis));
	cin >> n >> m >> r;
	FOR(i, 1, n) loc[i] = i;
	REP(i, n - 1)
	{
		ll x, y;
		cin >> x >> y;
		v[x].push_back(y);
		v[y].push_back(x);
	}

	REP(i, n)
	{
		cin >> val[i + 1];
	}
	calc(r);
//	FOR(i, 1, n) cout << i << "  " << sz[i] << endl;
//	cout << endl;
	REP(i, m)
	{
		ll x;
		cin >> x;
		cout << sz[x] << endl;
	}
}