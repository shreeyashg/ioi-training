#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n, k;
vector<pair<ll, ll> > v;
set<ll> good;
int main()
{
	cin >> n >> k;
	REP(i, n)
	{
		ll x;
		cin >> x;
		v.push_back({x, i+1});
	}
	sort(v.begin(), v.end());

	ll ans = 0;
	FORD(i, n-1, n-k)
	{
	//	cout << v[i].first << " " << v[i].second << endl;
		ans += v[i].first;
		good.insert(v[i].second);
	}

	cout << ans << endl;

	ll curr = 0;
	for(auto it = good.begin(); it != good.end(); it++)
	{
		auto x = it;
		if(++x != good.end())
			cout << *it-curr << " ";
		else
			cout << n-curr << endl;
		curr = *it;
	}
}