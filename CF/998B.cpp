#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll arr[105], b, n;
vector<ll> cuts;

int main()
{
	cin >> n >> b;
	REP(i, n)
	{
		cin >> arr[i + 1];
	}

	ll odd = 0; //, eve = 0;
	FOR(i, 1, n-1)
	{
		if(arr[i] % 2) odd ++;
		else odd --;
		if(!odd)
			cuts.push_back(abs(arr[i] - arr[i + 1]));
	}
	sort(cuts.begin(), cuts.end());

	ll cnt = 0;
	REP(i, cuts.size())
	{
		b -= cuts[i];
		if(b >= 0)
			cnt++;
	}

	cout << cnt << endl;
}