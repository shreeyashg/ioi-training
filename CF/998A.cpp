#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll a[11];

int main()
{
	ll n;
	cin >> n;
	ll x = (1 << n) - 1;
	REP(i, n) cin >> a[i];
	FOR(i, 0, x)
	{
	//	cout << "i = " << i << ", ";
		ll gg = 0, aa = 0;
		bitset<11> b(i);
	//	cout << b << endl;
		FOR(j, 0, n - 1)
		{
			// j + 1 bit
			ll curr = (1 << j);
			if((i & curr) == curr)
				aa += a[j];
			else
				gg += a[j];
		}
	//	DEBUG(gg);
	//	DEBUG(aa);
		if(gg)
		{
			if(aa)
			{
				if(gg != aa)
				{
					ll cnt = 0;
					vector<ll> ans;
					FOR(j, 0, n - 1)
					{
						ll curr = (1 << j);
						if((i & curr) == curr)
							cnt++, ans.push_back(j + 1);
					}
					cout << cnt << endl;
					REP(i, ans.size())
					{
						cout << ans[i] << " " ;
					}
					cout << endl;
					exit(0);

				}
			}
		}
	}
	cout << -1 << endl;
}