#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

vector<ll> v = {1, 5, 10, 50};
// 6 10 15 21 28 36 45 55 66 78 91

ll t;
int main()
{

	cin >> t;
	if(t > 12)
	{
		cout << 341 + ((t - 12) * 49) << endl, exit(0);
	}
	ll n = 1;
	while(n <= 12)
	{
		set<ll> s;
		FOR(i, 0, n)
		{
			for(ll j = 0; i + j <= n; j++)
			{
				for(ll k = 0; i + j + k <= n; k++)
				{
					for(ll l = 0; i + j + k + l <= n; l++)
					{
					//	cout << i << " " << j << " " << k << " "  << l << endl;
						if(i + j + k + l == 0)
							continue;
						if(i + j + k + l == n)
							s.insert((v[0] * i) + (v[1] * j) + (v[2] * k) + (v[3] * l));
					}
				}
			}
		}
		if(n == t)
			cout << s.size() << endl, exit(0);
		n ++;
	}
}