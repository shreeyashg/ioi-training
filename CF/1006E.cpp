#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 2e5 + 4;

ll n, q, st[MX], en[MX];
bool vis[MX];
vector<ll> dfsv, v[MX];
ll cnt = 0;
void f(ll idx)
{
//	cout << "TOP " << idx << endl;
	st[idx] = ++cnt;
	if(vis[idx])
		return;
	vis[idx] = 1;
//	DEBUG(idx);
	dfsv.push_back(idx);
	REP(i, v[idx].size())
	{
//		DEBUG(v[idx][i]);
		if(!vis[v[idx][i]])
			f(v[idx][i]);
	}
	en[idx] = cnt;
}

int main()
{	
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout.tie(0);
	dfsv.push_back(0);
	memset(vis, 0, sizeof(vis));
	cin >> n >> q;
	REP(i, n)
	{
		if(!i) continue;
		ll x, y;
		cin >> x;
		v[x].push_back(i + 1);
		v[i + 1].push_back(x);
	}
	f(1);
//	REP(i, dfsv.size()) cout << dfsv[i] << " ";
//	cout << endl;

/*	FOR(i, 1, n)
	{
		DEBUG(i);
		DEBUG(st[i]);
		DEBUG(en[i]);
		cout << endl;
	}
*/
	REP(i, q)
	{
		ll x, y;
		cin >> x >> y;
		if(en[x]-st[x]+1<y)
			cout << -1 << endl;
		else
			cout << dfsv[st[x]+y-1] << endl;
	}
}