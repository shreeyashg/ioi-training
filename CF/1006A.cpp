#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll arr[MX];

int main()
{
	ll n;
	cin >> n;

	REP(i, n) cin >> arr[i + 1];

	FOR(i, 1, n)
	{
		if(arr[i] % 2 == 0)
			arr[i]--;
	}

	REP(i, n) cout << arr[i + 1] << " ";
}