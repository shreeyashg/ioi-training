#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n, x, y;
string str;

int main()
{
	cin >> n >> x >> y >> str;
	bool chk = 0;
	REP(i, str.size())
	{
		if(str[i] == '0')
			chk = 1;
	}
	if(!chk)
		cout << 0 << endl, exit(0);


	char last = str[0];
	ll cnt = 0;
	FOR(i, 1, str.size() - 1)
	{
		if(str[i] == '1' && last == '0')
			cnt ++, last = '1';
		if(str[i] == '0' && last == '1')
			last = '0';
	}
	if(last == '0')
		cnt ++;

	ll ans = INF;
	FOR(i, 1, cnt)
	{
		ans = min((i * y) + ((cnt - i) * x), ans);
	}
	cout << ans << endl;
}