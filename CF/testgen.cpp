#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

int main()
{
	ll n = 2000, k = 1000;
	cout << n << " " << k << endl;
	srand(time(NULL));
	REP(i, n)
	{
		ll x = rand() % 2000;
		cout << x + 1 << " " << " ";
	}
	cout << endl;
}