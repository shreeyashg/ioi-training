#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n, arr[2*MX];
set<ll> st, en;
map<ll, ll> stm, enm;

int main()
{
	cin >> n;
	REP(i, n) cin >> arr[i + 1];

	ll sum = 0;
	FOR(i, 1, n)
	{
		sum += arr[i];
		st.insert(sum);
		if(!stm[sum]) stm[sum] = i;
	}
	sum = 0;
	FORD(i, n, 1)
	{
		sum += arr[i];
		en.insert(sum);
		if(!enm[sum]) enm[sum] = i;
	}

	set<ll> intersect;
	set_intersection(st.begin(),st.end(),en.begin(),en.end(),
                  std::inserter(intersect,intersect.begin()));

	ll ans = 0;
	for(auto it = intersect.begin(); it != intersect.end(); it++)
	{
		if(stm[*it] < enm[*it])
			ans = *it;
	}
	cout << ans << endl;
}