#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cerr<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n, ans=0;
string a, b;

int main()
{
	cin >> n >> a >> b;
	REP(i, a.size())
	{
		DEBUG("!");
		if(i >= (n/2))
			break;
	//	DEBUG(i);
		bool f = 0;
		DEBUG("FF");
		if((a[i] == a[n-1-i]) && (b[i] == b[n-i-1]))
			continue;
		DEBUG("FFF");
		if((a[i] != b[i]) && (a[n-i-1] != b[i]))
			ans++, f=1;
		DEBUG("SAF");
		if((a[i] != b[n-i-1]) && (a[n-i-1] != b[n-i-1]) && (b[i] != b[n-i-1]))
			ans++, f=1;
		if(a[i] != a[n-i-1] && b[i] == b[n-i-1] && !f)
			ans++;
	//	DEBUG(ans);
	}
	if(a.size()%2)
	{
		if(a[(n-1)/2] != b[(n-1)/2])
			ans++;
	}
			
	cout << ans << endl;
}
