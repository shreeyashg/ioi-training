#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 4000 + 4;

ll n, m;
bool know[MX][MX];
vector<pair<ll, ll> > v(MX);
ll num[MX];

int main()
{
	cin >> n >> m;
	REP(i, m)
	{
		ll x, y;
		cin >> x >> y;
		v[i] = {x, y};
		num[x] ++, num[y] ++;
		know[x][y] = know[y][x] = 1;
	}

	bool f = 0;
	ll ans = INF;
	REP(i, m)
	{
		REP(j, m)
		{
			if(i != j)
			{
				if(v[i].first == v[j].first)
				{
					if(know[v[i].second][v[j].second])
						f = 1, ans = min(ans, num[v[i].first] + num[v[j].second] + num[v[i].second]);
				}
				else if(v[i].first == v[j].second)
				{
					if(know[v[i].second][v[j].first])
						f = 1, ans = min(ans, num[v[i].first] + num[v[j].first] + num[v[i].second]);
				}
				else if(v[i].second == v[j].first)
				{
					if(know[v[i].first][v[j].second])
						f = 1, ans = min(ans, num[v[i].first] + num[v[j].second] + num[v[i].second]);
				}
				else if(v[i].second == v[j].second)
				{
					if(know[v[i].first][v[j].first])
						f = 1, ans = min(ans, num[v[i].first] + num[v[j].first] + num[v[i].second]);
				}
			}
		}
	}

	if(!f)
		cout << -1 << endl;
	else
		cout << ans - 6 << endl;
}