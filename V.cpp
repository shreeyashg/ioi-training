#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4, X = 1e5;

ll x, a, b;
string ss, s;
ll memo[13][3][X + 1];
bool pres[10];

ll f(ll idx, ll less, ll sum)
{
	// less = 0 implies that the number will be <= good regardless of length
	// less = 1 implies number will be <= good only if digits are <= str[idx]
	// less = 2 implies number will be < good only if its length is < str.length()
//	cout << ". " << idx << " " << less << " " << sum << " " << last << endl;

//	cout << idx <<" " << less <<" " << sum << endl;

	ll gc = (sum%x == 0 && idx != 0);

	if(memo[idx][less][sum] != -1)
		return memo[idx][less][sum]; 

	if(idx == 0)
	{
		REP(xx, ss.size())
		{
			ll i = ss[xx] - '0';
			if(i == 0) continue;
			ll nv = i % x;
			if(i < (s[idx] - '0')) gc += f(idx + 1, 0, nv);
			if(i == (s[idx] - '0')) gc += f(idx + 1, 1, nv);
			if(i > (s[idx] - '0') && s.size() - 1 > idx) gc += f(idx + 1, 2, nv);
		}
		return gc;
	}

	if(idx == s.size() - 1 && less == 2)
		return gc;

	else if(idx == s.size())
		return gc;

	REP(xx, ss.size())
	{
		ll i = ss[xx] - '0';
		ll nv = (((sum%x)*(10%x))%x + i%x)%x;
		if(less == 1 && (i) == (s[idx] - '0'))
			gc += f(idx + 1, 1, nv);

		else if(less == 1 && i < (s[idx] - '0'))
			gc += f(idx + 1, 0, nv);

		else if(less == 1 && i > (s[idx] - '0') && idx + 1 <= s.size() - 1)
			gc += f(idx + 1, 2, nv);

		else if(less == 2)
			gc += f(idx + 1, 2, nv);

		else if(less == 0)
			gc += f(idx + 1, 0, nv);

	}

	return memo[idx][less][sum] = gc;
}

bool check(string str)
{
	REP(i, str.size())
	{
		if(!pres[str[i]-'0'])
			return 0;
	}
	return 1;
}

int main()
{
	memset(memo, -1, sizeof(memo));
	cin >> x >> a >> b >> ss;
	sort(ss.begin(), ss.end());
	// Multiples of X
	if(x <= X)
	{
		s = to_string(b);
		ll ff = f(0, 0, 0);
		s = to_string(a - 1);
		memset(memo, -1, sizeof(memo));
		ll fff = f(0, 0, 0);
		cout << ff - fff << endl;
	}
	else
	{
		ll cnt =  0;
		REP(i, ss.size()) pres[ss[i]-'0'] = 1;
		ll d = a/x;
		ll ad = d * x; 
		if(ad < a)
			ad += x;
		for(ll i = (d < 0) ? x : ad; i <= b; i += x)
		{
			ll curr = i;
			if(check(to_string(i)))
				cnt ++;
		}
		cout << cnt << endl;
	}

}