#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 2e5 + 4;

ll n;
ll arr[MX];
multiset<ll> s;

int main()
{
	cin >> n;
	FOR(i, 1, n) cin >> arr[i], arr[i] += i, s.insert(arr[i]);

	ll cnt = 0;
	ll bad = n;
	auto f = s.end();
	f--;
	cout << *f << " ";
	FOR(i, 1, n-1)
	{
		cnt++;

		auto x = s.lower_bound(arr[bad]);
		s.erase(x);
		s.insert(arr[bad]-n);
		auto it = s.end();
		it--;

		cout << *it + cnt << " ";
		bad--;
	}
	cout << endl;
}