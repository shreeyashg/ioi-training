#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n, a[MX], p[MX], calc[MX];
vector<ll> v[MX]; ll hojo;

bool vis[MX];

void dfs(ll idx)
{
	if(vis[idx])
		return;
	vis[idx] = 1;

	if(v[idx].size() == 1 && idx != hojo)
	{
		calc[idx] = a[idx];
		return;
	}

	ll cval = INF;
	REP(i, v[idx].size())
	{	
		if(!vis[v[idx][i]])
			dfs(v[idx][i]);
		cval = min(cval, calc[v[idx][i]]);
	}
	calc[idx] = min(cval, a[idx]);
}

int main()
{
	memset(vis, 0, sizeof(vis));
	memset(calc, MOD, sizeof(calc));
	cin >> n;
	REP(i, n)
	{
		cin >> a[i + 1];
	}

	REP(i, n) cin >> p[i + 1];

	FOR(i, 1, n) 
	{
		if(p[i] != -1)v[i].push_back(p[i]), v[p[i]].push_back(i);
		else hojo = i;
	}

	FOR(i, 1, n)
	{
	//	cout << i << ": " << endl;
	//	REP(j, v[i].size()) cout << v[i][j] << " ";
	//	cout << endl; 
	}

	dfs(hojo);
	ll ans = -MOD;
	FOR(i, 1, n)
	{
		if(v[i].size() == 1 && i != hojo)
			;
		else
			ans = max(ans, (a[i] - calc[i]));
	}
	cout << ans;
}