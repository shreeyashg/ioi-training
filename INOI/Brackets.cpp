#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 7e2 + 4;

ll n, k, b[MX], v[MX], memo[MX][MX];

ll rec(ll l, ll r) // Maximum sum from [l, r]
{
	if(l > r)
		return 0;
	if(l + 1 == r && b[r] - b[l] == k)
		return v[l] + v[r];
	if(l == r)
		return 0;

	if(memo[l][r] != -1)
		return memo[l][r];

	ll ans = 0;

	if(b[r]-b[l]==k)
	{
		ll gc = v[l] + v[r];
		FOR(i, l, r-1)
		{
			ans = max(ans, gc + rec(l+1, i) + rec(i + 1, r-1));
			ans = max(ans, rec(l, i) + rec(i + 1, r));
		}
	}
	else
	{
		FOR(i, l, r-1)
		{
			ans = max(ans, rec(l, i) + rec(i + 1, r));
		}
	}

	return memo[l][r] = ans;
}

int main()
{
	memset(memo, -1, sizeof(memo));
	cin >> n >> k;

	REP(i, n)
	{
		cin >> v[i + 1];
	}
	REP(i, n)
	{
		cin >> b[i + 1];
	}

	cout << rec(1, n) << endl;
}