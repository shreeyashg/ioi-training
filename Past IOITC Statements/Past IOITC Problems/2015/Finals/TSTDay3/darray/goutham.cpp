#include <map>
#include <set>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cctype>
#include <cstdio>
#include <vector>
#include <cassert>
#include <complex>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;

const int N = 1234, mod = 1e9 + 7;
int a[N], s[N], f[N][N];

inline void add(int& a, int b) { a += b; if (a >= mod) a -= mod; }

int main()
{
	int n, rnk = 1, ans = 0; cin >> n;
	for (int i = 1; i <= n; i++) cin >> a[i];
	for (int i = 0; i <= n + 1; i++) f[i][0] = 1;
	for (int j = 1; j <= n; j++)
		for (int i = 0; i <= n; i++)
		{
			f[i][j] = f[i + 1][j - 1];
			if (i > 0) add(f[i][j], f[i - 1][j]);
		}
	for (int i = 1, se = 1; i <= n; i++)
	{
		int ps = se;
		while (s[se - 1] > a[i]) add(rnk, f[se--][n - i]);
		s[se++] = a[i];
		cout << ps - se + 1 << (" \n"[i == n]);
		if (i < n)
			add(ans, f[0][i - 1] * 1ll * f[0][n - 1 - i] % mod);
	}
	
	int typ;
        cin>>typ;
 
            if(typ>=1)
            {
                cout<<rnk<<endl;
            }
            if(typ==2)
            {
                cout<<ans<<endl;

            }
	
// 	cout << rnk << "\n" << ans << "\n";
	return 0;
}