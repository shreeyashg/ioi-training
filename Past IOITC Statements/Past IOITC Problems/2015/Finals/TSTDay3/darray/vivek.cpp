
// Author : Vivek Hamirwasia (viv001)
#include<bits/stdc++.h>
using namespace std;

#define CLR(a,x) memset(a,x,sizeof(a))
#define PB push_back
#define INF 1000000000
#define MOD 1000000007
#define LET(x,a) __typeof(a) x(a)
#define MP make_pair
#define tr(container , it) for(LET(it,container.begin) ; it!=container.end() ; it++)
#define FOR(i,a,b) for(i=a;i<b;i++)
#define REP(i,a) FOR(i,0,a)
#define LLD long long
#define VI vector < int >
#define PII pair < int , int >
#define MAX 1000000000
#define gi(x) scanf("%d",&x)
//_____starts____here_______
int a[10002];
int d[10002];
int dp[1002][1002], dp2[1002][1002];
int n;
int func(int i, int j)
{
    if(i>n) return dp[i][j] = 0;
    if(j>=i) return dp[i][j] = 0;
    if(i==n) return dp[i][j] = 1;
    if(dp[i][j]!=-1)
        return dp[i][j];
    return dp[i][j] = (func(i+1, j)+func(i,j+1))%MOD;
}

LLD g(int i, int j)
{
    if(i<0 || j<0 ) return 1;
    return dp2[i][j];
}

int main()
{
    cin>>n;
    for(int i=1;i<=n;i++)
        cin>>a[i];
    memset(dp,-1,sizeof dp);
    memset(dp2,0,sizeof dp2);


    n++;

    dp2[0][0] = 1;
    for(int i=1;i<=n;i++)
        dp2[i][0] = 1;

    for(int i=1;i<=n;i++)
        for(int j=1;j<=i;j++)
            dp2[i][j] = (dp2[i-1][j]+dp2[i][j-1])%MOD;

    d[1] = 0;
    stack<int> st;
    st.push(1);
    for(int i=2;i<n;i++)
    {
        int cnt = 0;
        while(!st.empty() && a[st.top()]>a[i])
        {
            cnt++;
            st.pop();
        }
        st.push(i);
        d[i] = cnt;
    }

    int ans = 0;
    for(int i=1;i<n;i++)
        for(int j=0;j<=i-1;j++)
            dp[i][j] = (func(i+1,j) + func(i,j+1))%MOD;

    for(int i=0;i<=n;i++)
        for(int j=0;j<=n;j++)
            if(dp[i][j]==-1)
                dp[i][j]=0;


    int prev = 0;
    for(int i=1;i<n;i++)
    {
        for(int j=prev;j<prev+d[i];j++)
        {
            ans += dp[i+1][j];
            ans %= MOD;
        }
        prev += d[i];
    }
    ans++;

    LLD ans2=0;

    for(int i=2;i<n;i++)
    {
        ans2 += (g(i-2,i-2)*g(n-i-1,n-i-2))%MOD;
        ans2%=MOD;
    }
    ans2%=MOD;

    int typ;
    cin>>typ;
    if(typ>=0)
    {
        for(int i=1;i<n;i++)
            cout<<d[i]<<" ";
        cout<<endl;
    }
    if(typ>=1)
    {
        cout<<ans<<endl;
    }
    if(typ==2)
    {
        cout<<ans2<<endl;

    }

    return 0;
}


