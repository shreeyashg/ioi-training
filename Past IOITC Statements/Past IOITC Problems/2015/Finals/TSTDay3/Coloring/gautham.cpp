#include <map>
#include <set>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cctype>
#include <cstdio>
#include <vector>
#include <cassert>
#include <complex>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;
#define int long long

const int N = 1e6 + 23, mod = 1e9 + 7;
vector <int> adj[N];
int col[N], par[N], na[N], nb[N];
bool was[N];

int power2(int n)
{
	if (n < 60) return (1LL << n) % mod;
	int ret = power2(n / 2);
	ret = ret * ret % mod;
	if (n % 2 == 1) ret = ret << 1;
	if (ret >= mod) ret -= mod;
	return ret;
}

void dfs(int u, int rt1, int rt2)
{
	was[u] = true;
	par[u] = rt1;
	for (int v : adj[u])
		if (!was[v])
			dfs(v, rt2, rt1);
}

bool isuncol(int i)
{
	if (i % 2 == 0) i--;
	if (na[i] == 0 and nb[i] == 0 and na[i + 1] == 0 and nb[i + 1] == 0)
		return true;
	return false;
}

bool isbad(int i)
{
	if (i % 2 == 0) i--;
	if (na[i] > 0 and nb[i] > 0) return true;
	if (na[i] > 0 and na[i + 1] > 0) return true;
	if (na[i + 1] > 0 and nb[i + 1] > 0) return true;
	if (nb[i] > 0 and nb[i + 1] > 0) return true;
	return false;
}

int dad(int u)
{
	if (par[u] == u) return u;
	return par[u] = dad(par[u]);
}

#undef int
int main()
{
#define int long long
	int n, m, q, k1, k2;
	cin >> n >> m >> q >> k1 >> k2;

	memset(col, -1, sizeof(col));
	while (k1--)
	{
		int x; cin >> x;
		col[x] = 0;
	}
	while (k2--)
	{
		int x; cin >> x;
		col[x] = 1;
	}

	while (m--)
	{
		int u, v;
		cin >> u >> v;
		adj[u].push_back(v);
		adj[v].push_back(u);
	}

	int cs = n + 10;

	for (int i = 1; i <= n; i++)
		if (!was[i])
		{
			dfs(i, cs + 1, cs + 2);
			par[cs + 1] = cs + 1;
			par[cs + 2] = cs + 2;
			cs += 2;
		}

	for (int i = 1; i <= n; i++)
		if (col[i] == 0) na[dad(i)]++;
		else if (col[i] == 1) nb[dad(i)]++;

	int uncol = 0, bad = 0;
	for (int i = n + 11; i <= cs; i += 2)
	{
		if (isuncol(i)) uncol++;
		if (isbad(i)) bad++;
	}

	while (q--)
	{
		int ch; cin >> ch;
		if (ch == 0)
		{
			int u; char co;
			cin >> u >> co;

			uncol -= isuncol(dad(u));
			bad -= isbad(dad(u));

			if (col[u] == 0) na[dad(u)]--;
			if (col[u] == 1) nb[dad(u)]--;
			col[u] = -1;

			if (co == 'A') col[u] = 0;
			if (co == 'B') col[u] = 1;
			if (col[u] == 0) na[dad(u)]++;
			if (col[u] == 1) nb[dad(u)]++;

			uncol += isuncol(dad(u));
			bad += isbad(dad(u));
		}
		else if (ch == 1)
		{
			int u, v; cin >> u >> v;

			int fst = dad(u), snd = dad(v);
			if (snd % 2 == 0) snd--;
			else snd++;

			if (fst == snd) continue;

			uncol -= isuncol(dad(u)) + isuncol(dad(v));
			bad -= isbad(dad(u)) + isbad(dad(v));

			par[snd] = fst;
			na[fst] += na[snd];
			nb[fst] += nb[snd];

			if (fst % 2 == 0) fst--;
			else fst++;
			if (snd % 2 == 0) snd--;
			else snd++;

			par[snd] = fst;
			na[fst] += na[snd];
			nb[fst] += nb[snd];

			uncol += isuncol(dad(u));
			bad += isbad(dad(u));
		}
		else
		{
			if (bad) cout << "0\n";
			else cout << power2(uncol) << "\n";
		}
	}
	return 0;
}