
// Author : Vivek Hamirwasia (viv001)
#include<bits/stdc++.h>
using namespace std;

#define CLR(a,x) memset(a,x,sizeof(a))
#define PB push_back
#define INF 1000000000
#define MOD 1000000007
#define LET(x,a) __typeof(a) x(a)
#define MP make_pair
#define tr(container , it) for(LET(it,container.begin) ; it!=container.end() ; it++)
#define FOR(i,a,b) for(i=a;i<b;i++)
#define REP(i,a) FOR(i,0,a)
#define LLD long long
#define VI vector < int >
#define PII pair < int , int >
#define NMAX 100000
#define QMAX 100000
#define MMAX 100000
#define gi(x) scanf("%d",&x)
//_____starts____here_______
int comp[100002][2][2];
vector<int> V[100002];
int typ[1000002];
int par[1000002];
int ans[1000002];
int col[1000002];
int cnt[4];
void updval(int p1)
{
    cnt[ans[p1]]--;
    if(comp[p1][0][0]==0 && comp[p1][0][1]==0 && comp[p1][1][0]==0 && comp[p1][1][1]==0)
        ans[p1] = 2;
    else
    {
        if( (comp[p1][0][0]!=0&&comp[p1][1][0]!=0) || (comp[p1][0][1]!=0&&comp[p1][1][1]!=0)
          ||(comp[p1][0][0]!=0&&comp[p1][0][1]!=0) || (comp[p1][1][0]!=0&&comp[p1][1][1]!=0) )
            ans[p1] = 0;
        else
            ans[p1] = 1;
    }
    cnt[ans[p1]]++;
}
void merge(int a, int b)
{
    int p1 = par[a];
    int p2 = par[b];
    if(p1==p2) return;
    if(V[p1].size() < V[p2].size())
        return merge(b,a);

    // merge b into a.
    int t1 = typ[a];
    int t2 = typ[b];

    comp[p1][t1][0] += comp[p2][1-t2][0];
    comp[p1][t1][1] += comp[p2][1-t2][1];

    comp[p1][1-t1][0] += comp[p2][t2][0];
    comp[p1][1-t1][1] += comp[p2][t2][1];

    cnt[ans[p2]]--;

    int sz = V[p2].size();
    for(int i=0;i<sz;i++)
    {
        V[p1].PB(V[p2][i]);
        par[V[p2][i]]=p1;
        if(typ[V[p2][i]]==t2)
          typ[V[p2][i]] = 1 - t1;
        else
            typ[V[p2][i]] = t1;
    }
    V[p2].clear();
    updval(p1);
}
LLD two[1000002];
LLD getans()
{
    if(cnt[0]!=0)
        return 0;
    return two[cnt[2]];
}
int main()
{
    memset(comp,0,sizeof comp);

    two[0] = 1;
    for(int i=1;i<=100002;i++)
        two[i] = (2*two[i-1])%MOD;
    int n,m,q,k1,k2;
    gi(n);
    gi(m);
    gi(q);
    gi(k1);
    gi(k2);
//     assert(1<=n<=NMAX);
//     assert(1<=m<=MMAX);
//     assert(1<=q<=QMAX);
//     assert(0<=k1<=n);
//     assert(0<=k2<=n);
//     assert(0<=k1+k2<=n);
    for(int i=0;i<n;i++)
    {
        par[i] = i;
        V[i].PB(i);
        typ[i] = 0;
        col[i] = -1;
        ans[i] = 2;
        cnt[2]++;
    }
    for(int i=0;i<k1;i++)
    {
        int x;
        gi(x);
//         assert(1<=x<=n);
        x--;
//         assert(col[x]==-1);
        comp[x][0][0] = 1;
        col[x] = 0;
        updval(x);
    }

    for(int i=0;i<k2;i++)
    {
        int x;
        gi(x);
//         assert(1<=x<=n);
        x--;
//         assert(col[x]==-1);
        comp[x][0][1] = 1;
        col[x] = 1;
        updval(x);
    }

    for(int i=0;i<m;i++)
    {
        int u,v;
        gi(u);
        gi(v);
//         assert(1<=u<=n);
//         assert(1<=v<=n);
        u--,v--;
        merge(u,v);
    }

    for(int i=0;i<q;i++)
    {
        int a,u,v,c;
        char str[20];
        gi(a);
        if(a==0)
        {
            gi(u);
//             assert(1<=u<=n);
            u--;
            int p = par[u];
            int t = typ[u];
            scanf("%s",str);
            if(str[0]=='A') c = 0;
            else if(str[0]=='B') c = 1;
            else c = -1;
            if(c==col[u])
                continue;

            if(col[u]==-1)
                comp[p][t][c]++;
            else
            {
                comp[p][t][col[u]]--;
                if(c!=-1)
                  comp[p][t][c]++;
            }
            col[u] = c;
            updval(p);
        }
        else if(a==1)
        {
            gi(u);
            gi(v);
//             assert(1<=u<=n);
//             assert(1<=v<=n);
            u--,v--;
            merge(u,v);
        }
        else
           printf("%lld\n",getans()); 

    }

    return 0;
}


