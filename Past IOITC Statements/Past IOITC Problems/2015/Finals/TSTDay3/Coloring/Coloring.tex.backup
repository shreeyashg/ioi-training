\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{anysize}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{bm}
\usepackage{enumerate}
\usepackage{multirow}
\usepackage{subfig}
\usepackage{caption}
\usepackage{multicol}
\marginsize{1.5 cm}{1.5 cm}{1.5 cm}{1.5 cm}

\title{\textbf{IOITC 2015 Finals, Day 2}}
\date{\vspace{-7 mm}}
\begin{document}

\maketitle

\section*{Graph Coloring}

You are given a bipartite graph $G$ = $(V,E)$. $V$ = {1,2,..,N}.
A subset $S \subset V$ is initially colored with two colors, A and B. Updates are of the form "0 u A", "0 u B" or "0 u U", which refers to "Color the vertex u with A", "Color the vertex u with B" and "Un-color vertex u" respectively. You also get updates of the form "1 u v" which means that you should add the bidirectional edge (u,v) to the graph. You are guaranteed that even after adding the edges, the graph remains bipartite. You get queries of the form "2" for which you have to report the number of ways in which you can color all the currently uncolored vertices such that no edge in G has both end points with the same color. Output '0' if this partial coloring cannot be extended fully to satisfy the condition. Output the answer modulo 109 + 7.\\

Note: A graph is called bipartite, if it's vertex set can be partitioned into two subsets S1, S2 such that $S1 \union S2 = V, S1 \intersection S2 = \null$, and there are no edges with both end points in either subset. ie. all edges go between S1 and S2.

For inserting a number into an already existing BST, we use the following pseudo code:
\begin{verbatim}
insert(Node root, int data)
{
     if (root doesn't exist)
           root = new Node(data);
        
     else if (data < root->data)
           insert(root->left, data);
  	
     else if (data > root->data)
           insert(root->right, data);
}\end{verbatim}

Note that you can assume that Chandragupta only inserts distinct values into the BST. Now, Chandragupta defines a new numbering criteria of nodes in a BST. He gives number $1$ to the root node and for each node with number $i$, he gives numbers $2\cdot i$ and $2\cdot i+1$ to it's left and right child respectively, if they exist. For example, the following shows the numbering criteria of a complete binary search tree with $7$ nodes.

\begin{verbatim}

               1
              / \
             /   \
            /     \
           /       \
          2         3
         / \       / \
        /   \     /   \
       4     5   6     7
\end{verbatim}

Now, he defines a function $F$ where $F(x)$ returns the node number of the node where key $x$ is stored. This function's domain includes only those values which are present in the BST.\\

Arriving to challenging Chanakya on BSTs, Chandragupta gives him $N$ distinct integers $A_1, A_2, ..., A_N$, which he has to insert one by one into a BST(which is initially empty) in the order they arrive in the input. After all the insertions have been done, Chanakya has to report $N$ integers $F(A_1), F(A_2), ..., F(A_N)$. Since these numbers can be large, output each of them after modulo $10^9+7$.

\subsection*{Input}
The first line of input will contain integer $N$ \textit{i.e.}, the number of integers in array $A$.

\vspace{2mm}
\noindent
The second line contains $N$ space separated integers denoting array $A$.

\subsection*{Output}
Output $N$ space separated integers where $i$'th number denotes $F(A_i)$ modulo $10^9+7$.

\subsection*{Test Data}
In all the subtasks,\\
$1\le A_i \le 10^9$\\

\vspace{2 mm}
\noindent
\textbf{Subtask 1 (30 Points)}: $1 \le N \le 1000$.

\vspace{2 mm}
\noindent
\textbf{Subtask 2 (70 Points)}: $1 \le N \le 3*10^5$.

\begin{multicols}{2}
\subsubsection*{Sample Input1}
3\\
4 2 7\\
\columnbreak
\subsubsection*{Sample Output1}
1 2 3\\
\end{multicols}

\begin{multicols}{2}
\subsubsection*{Sample Input2}
3\\
4 6 5\\
\columnbreak
\subsubsection*{Sample Output2}
1 3 6\\
\end{multicols}

\subsection*{Limits}
Time: 1 second

\vspace{2 mm}
\noindent
Memory: 256 MB

\end{document}
