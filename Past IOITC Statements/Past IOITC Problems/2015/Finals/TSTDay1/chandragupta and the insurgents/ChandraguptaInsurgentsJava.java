import java.util.*;
import java.io.*;
import java.math.*;
class ChandraguptaInsurgentsJava {
    static int[] A = new int[1000010];

    // interval updates, interval queries (lazy propagation)
    static int SN = 1048576;  // must be a power of 2
    static int[] PassingOver = new int[2*SN];
    static int[] NonZerosWithin = new int[2*SN];

    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static StringTokenizer st = null;
    static String ns() throws Exception {
        while (st == null || !st.hasMoreTokens()) {
            st = new StringTokenizer(br.readLine());
        }
        return st.nextToken();
    }
    static int ni() throws Exception {
        return Integer.parseInt(ns());
    }
    static class SegmentTree {

        // T[x] is the (properly updated) sum of indices represented by node x
        // U[x] is pending increment for _each_ node in the subtree rooted at x 

        SegmentTree() { 
            // clear(PassingOver,0);
            // clear(NonZerosWithin,0); 
        }

        // increment every index in [ia,ib) by incr 
        // the current node is x which represents the interval [a,b)
        void update(int incr, int ia, int ib, int x, int a, int b) { // [a,b)
            ia = Math.max(ia,a); ib = Math.min(ib,b); // intersect [ia,ib) with [a,b)
            if(ia >= ib) return;            // [ia,ib) is empty 
            if(ia == a && ib == b) {       // We push the increment to 'pending increments'
                PassingOver[x] += incr;               // And stop recursing
                return; 
            }
            //T[x] += incr * (ib - ia);          // Update the current node
            update(incr,ia,ib,2*x,a,(a+b)/2);  // And push the increment to its children
            update(incr,ia,ib,2*x+1,(a+b)/2, b);
            if(PassingOver[2*x]!=0)
                NonZerosWithin[x] = (a+b)/2 - a;
            else
                NonZerosWithin[x] = NonZerosWithin[2*x];
            if(PassingOver[2*x+1]!=0)
                NonZerosWithin[x] += (b - ((a+b)/2));
            else
                NonZerosWithin[x] += NonZerosWithin[2*x + 1];
        }
        
        public void update(int incr, int ia, int ib) {
            update(incr, ia, ib, 1, 0, SN);
        }

        int query(int ia, int ib, int x, int a, int b) {
            ia = Math.max(ia,a);ib = Math.min(ib,b); //  intersect [ia,ib) with [a,b)
            if(ia >= ib) return 0;          // [ia,ib) is empty 

            if(PassingOver[x]!=0)
                return (ib-ia);
            if(ia == a && ib == b) 
                return NonZerosWithin[x];

            //         T[x] += (b - a) * U[x];           // Carry out the pending increments
            //         U[2*x] += U[x], U[2*x+1] += U[x]; // Push to the childrens' pending increments
            //         U[x] = 0;
            int q1,q2;
            q1 = query(ia,ib,2*x,a,(a+b)/2);
            q2 = query(ia,ib,2*x+1,(a+b)/2,b);
            return q1+q2; 
        }
        public int query(int ia, int ib) {
            return query(ia, ib, 1, 0, SN);
        }
    };

    public static void main(String[] args) throws Exception {
        SegmentTree T = new SegmentTree();
        

        int u, v, totale;
        int n,e1,q,c,cnt;
        // cin>>n>>e1>>q;
        n = ni();
        e1 = ni();
        q = ni();
        totale=e1+n-1;
        int tmp;
        for(int i=1;i<=e1;i++)
        {
            u = ni();
            v = ni();
            if(u>v) {
                tmp = u;
                u = v;
                v = tmp;
            }
            T.update(1,u,v);
        }
        for(int i=1;i<=q;i++)
        {
            c = ni();
            u = ni();
            v = ni();
            if(u>v) {
                tmp = u;
                u = v;
                v = tmp;
            }
            if(c==0)
            {
                totale++;
                T.update(1,u,v);
            }

            if(c==1)
            {
                totale--;
                T.update(-1,u,v);
            }

            if(c==2)
            {
                System.out.println(v-u-T.query(u,v));
            }
        }
    }
}
