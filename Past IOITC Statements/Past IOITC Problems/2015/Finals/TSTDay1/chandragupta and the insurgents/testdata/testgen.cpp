#include<bits/stdc++.h>
#define max(a,b) (a>b ? a : b)
#define min(a,b) (a<b ? a : b)
#define LLD long long int
#define PB push_back
#define PII pair<int,int>
#define MAXN1 100
#define MAXM1 10
#define MAXQ1 100

#define MAXN2 1000
#define MAXM2 100
#define MAXQ2 1000

#define MAXN3 100000
#define MAXM3 1000
#define MAXQ3 100000

using namespace std;


//========================================================================================

// ######### Test Generation ###########

int rndA = 168726318;
int rndB = 768978978;
map<PII, bool> mp;
vector<int> V[60];
void clr()
{
    mp.clear();
    for(int i=0;i<60;i++)
        V[i].clear();
}
bool insert(int x, int y)
{
    if(x==y) return false;
    if(x>y) swap(x,y);
    PII z = PII(x,y);
    if(mp.count(z)!=0)
        return false;
    mp[z]=true;
    return true;
}

bool sel[1000002];
void gen_clusters(int k, int len, int n)
{
    memset(sel,false,sizeof sel);
    for(int i=0;i<k;)
    {
        int idx = rand()%n + 1;
        bool f = false;
        for(int j=0;j<len;j++)
            if(idx+j>n || sel[idx])
            {
                f=true;
                break;
            }
        if(f)
            continue;
        for(int j=0;j<len;j++)
        {
            V[i].PB(idx+j);
            sel[idx+j]=true;
        }
        i++;
    }
}

void gen0()  // small: random
{
    srand(rndA*0+rndB);
    int n,m,q;
    freopen("input0.in","w",stdout);
    clr();
    n = MAXN1;
    m = MAXM1;
    q = MAXQ1;
    cout<<n<<" "<<m<<" "<<q<<endl;

    int k = 2;
    int len = 8;
    gen_clusters(k,len,n);
    for(int i=0;i<m;i++)
    {
        int x,y;
        while(1)
        {
            int idx = rand()%k;
            x = V[idx][rand()%len];
            y = V[idx][rand()%len];
            if(insert(x,y))
                break;
        }
        cout<<x<<" "<<y<<endl;
    }

    for(int i=0;i<q;i++)
    {
        int typ = rand()%3;
        cout<<typ<<" ";
        if(typ==0) // add
        {
            int x,y;
            while(1)
            {
                int idx = rand()%k;
                x = V[idx][rand()%len];
                y = V[idx][rand()%len];
                if(insert(x,y))
                    break;
            }
            cout<<x<<" "<<y<<endl;
        }
        else if(typ==1) // rem
        {
            auto it = mp.begin();
            advance(it, rand()%mp.size());
            PII p = it->first;
            cout<<p.first<<" "<<p.second<<endl;
            mp.erase(it);
        }
        else // query
        {
            int x,y;
            if(rand()%4)  // random
            {
                while(1)
                {
                    x = rand()%n+1;
                    y = rand()%n+1;
                    if(x!=y)
                        break;
                }
            }
            else  //biased : clusters
            {
                while(1)
                {
                    int idx = rand()%k;
                    x = V[idx][rand()%len];
                    y = V[idx][rand()%len];
                    if(x!=y) break;
                }
            }
            cout<<x<<" "<<y<<endl;
        }
    }
}
void gen1()  // small: random
{
    srand(rndA*1+rndB);
    int n,m,q;
    freopen("input1.in","w",stdout);
    clr();
    n = MAXN1;
    m = MAXM1;
    q = MAXQ1;
    cout<<n<<" "<<m<<" "<<q<<endl;

    int k = 3;
    int len = 6;
    gen_clusters(k,len,n);
    for(int i=0;i<m;i++)
    {
        int x,y;
        while(1)
        {
            int idx = rand()%k;
            x = V[idx][rand()%len];
            y = V[idx][rand()%len];
            if(insert(x,y))
                break;
        }
        cout<<x<<" "<<y<<endl;
    }

    for(int i=0;i<q;i++)
    {
        int typ = rand()%3;
        cout<<typ<<" ";
        if(typ==0) // add
        {
            int x,y;
            while(1)
            {
                int idx = rand()%k;
                x = V[idx][rand()%len];
                y = V[idx][rand()%len];
                if(insert(x,y))
                    break;
            }
            cout<<x<<" "<<y<<endl;
        }
        else if(typ==1) // rem
        {
            auto it = mp.begin();
            advance(it, rand()%mp.size());
            PII p = it->first;
            cout<<p.first<<" "<<p.second<<endl;
            mp.erase(it);
        }
        else // query
        {
            int x,y;
            if(rand()%4)  // random
            {
                while(1)
                {
                    x = rand()%n+1;
                    y = rand()%n+1;
                    if(x!=y)
                        break;
                }
            }
            else  //biased : clusters
            {
                while(1)
                {
                    int idx = rand()%k;
                    x = V[idx][rand()%len];
                    y = V[idx][rand()%len];
                    if(x!=y) break;
                }
            }
            cout<<x<<" "<<y<<endl;
        }
    }
}

void gen2()  // small: random
{
    srand(rndA*2+rndB);
    int n,m,q;
    freopen("input2.in","w",stdout);
    clr();
    n = MAXN1;
    m = MAXM1;
    q = MAXQ1;
    cout<<n<<" "<<m<<" "<<q<<endl;

    int k = 2;
    int len = 8;
    gen_clusters(k,len,n);
    for(int i=0;i<m;i++)
    {
        int x,y;
        while(1)
        {
            int idx = rand()%k;
            x = V[idx][rand()%len];
            y = V[idx][rand()%len];
            if(insert(x,y))
                break;
        }
        cout<<x<<" "<<y<<endl;
    }

    for(int i=0;i<q;i++)
    {
        int typ = rand()%3;
        cout<<typ<<" ";
        if(typ==0) // add
        {
            int x,y;
            while(1)
            {
                int idx = rand()%k;
                x = V[idx][rand()%len];
                y = V[idx][rand()%len];
                if(insert(x,y))
                    break;
            }
            cout<<x<<" "<<y<<endl;
        }
        else if(typ==1) // rem
        {
            auto it = mp.begin();
            advance(it, rand()%mp.size());
            PII p = it->first;
            cout<<p.first<<" "<<p.second<<endl;
            mp.erase(it);
        }
        else // query
        {
            int x,y;
            if(rand()%4)  // random
            {
                while(1)
                {
                    x = rand()%n+1;
                    y = rand()%n+1;
                    if(x!=y)
                        break;
                }
            }
            else  //biased : clusters
            {
                while(1)
                {
                    int idx = rand()%k;
                    x = V[idx][rand()%len];
                    y = V[idx][rand()%len];
                    if(x!=y) break;
                }
            }
            cout<<x<<" "<<y<<endl;
        }
    }
}

void gen3()  // medium: random
{
    srand(rndA*3+rndB);
    int n,m,q;
    freopen("input3.in","w",stdout);
    clr();
    n = MAXN2;
    m = MAXM2;
    q = MAXQ2;
    cout<<n<<" "<<m<<" "<<q<<endl;

    int k = 5;
    int len = 10;
    gen_clusters(k,len,n);
    for(int i=0;i<m;i++)
    {
        int x,y;
        while(1)
        {
            int idx = rand()%k;
            x = V[idx][rand()%len];
            y = V[idx][rand()%len];
            if(insert(x,y))
                break;
        }
        cout<<x<<" "<<y<<endl;
    }

    for(int i=0;i<q;i++)
    {
        int typ = rand()%3;
        cout<<typ<<" ";
        if(typ==0) // add
        {
            int x,y;
            while(1)
            {
                int idx = rand()%k;
                x = V[idx][rand()%len];
                y = V[idx][rand()%len];
                if(insert(x,y))
                    break;
            }
            cout<<x<<" "<<y<<endl;
        }
        else if(typ==1) // rem
        {
            auto it = mp.begin();
            advance(it, rand()%mp.size());
            PII p = it->first;
            cout<<p.first<<" "<<p.second<<endl;
            mp.erase(it);
        }
        else // query
        {
            int x,y;
            if(rand()%4)  // random
            {
                while(1)
                {
                    x = rand()%n+1;
                    y = rand()%n+1;
                    if(x!=y)
                        break;
                }
            }
            else  //biased : clusters
            {
                while(1)
                {
                    int idx = rand()%k;
                    x = V[idx][rand()%len];
                    y = V[idx][rand()%len];
                    if(x!=y) break;
                }
            }
            cout<<x<<" "<<y<<endl;
        }
    }
}
void gen4()  // medium: random
{
    srand(rndA*4+rndB);
    int n,m,q;
    freopen("input4.in","w",stdout);
    clr();
    n = MAXN2;
    m = MAXM2;
    q = MAXQ2;
    cout<<n<<" "<<m<<" "<<q<<endl;

    int k = 8;
    int len = 10;
    gen_clusters(k,len,n);
    for(int i=0;i<m;i++)
    {
        int x,y;
        while(1)
        {
            int idx = rand()%k;
            x = V[idx][rand()%len];
            y = V[idx][rand()%len];
            if(insert(x,y))
                break;
        }
        cout<<x<<" "<<y<<endl;
    }

    for(int i=0;i<q;i++)
    {
        int typ = rand()%3;
        cout<<typ<<" ";
        if(typ==0) // add
        {
            int x,y;
            while(1)
            {
                int idx = rand()%k;
                x = V[idx][rand()%len];
                y = V[idx][rand()%len];
                if(insert(x,y))
                    break;
            }
            cout<<x<<" "<<y<<endl;
        }
        else if(typ==1) // rem
        {
            auto it = mp.begin();
            advance(it, rand()%mp.size());
            PII p = it->first;
            cout<<p.first<<" "<<p.second<<endl;
            mp.erase(it);
        }
        else // query
        {
            int x,y;
            if(rand()%4)  // random
            {
                while(1)
                {
                    x = rand()%n+1;
                    y = rand()%n+1;
                    if(x!=y)
                        break;
                }
            }
            else  //biased : clusters
            {
                while(1)
                {
                    int idx = rand()%k;
                    x = V[idx][rand()%len];
                    y = V[idx][rand()%len];
                    if(x!=y) break;
                }
            }
            cout<<x<<" "<<y<<endl;
        }
    }
}

void gen5()  // medium: random
{
    srand(rndA*5+rndB);
    int n,m,q;
    freopen("input5.in","w",stdout);
    clr();
    n = MAXN2;
    m = MAXM2;
    q = MAXQ2;
    cout<<n<<" "<<m<<" "<<q<<endl;

    int k = 8;
    int len = 11;
    gen_clusters(k,len,n);
    for(int i=0;i<m;i++)
    {
        int x,y;
        while(1)
        {
            int idx = rand()%k;
            x = V[idx][rand()%len];
            y = V[idx][rand()%len];
            if(insert(x,y))
                break;
        }
        cout<<x<<" "<<y<<endl;
    }

    for(int i=0;i<q;i++)
    {
        int typ = rand()%3;
        cout<<typ<<" ";
        if(typ==0) // add
        {
            int x,y;
            while(1)
            {
                int idx = rand()%k;
                x = V[idx][rand()%len];
                y = V[idx][rand()%len];
                if(insert(x,y))
                    break;
            }
            cout<<x<<" "<<y<<endl;
        }
        else if(typ==1) // rem
        {
            auto it = mp.begin();
            advance(it, rand()%mp.size());
            PII p = it->first;
            cout<<p.first<<" "<<p.second<<endl;
            mp.erase(it);
        }
        else // query
        {
            int x,y;
            if(rand()%4)  // random
            {
                while(1)
                {
                    x = rand()%n+1;
                    y = rand()%n+1;
                    if(x!=y)
                        break;
                }
            }
            else  //biased : clusters
            {
                while(1)
                {
                    int idx = rand()%k;
                    x = V[idx][rand()%len];
                    y = V[idx][rand()%len];
                    if(x!=y) break;
                }
            }
            cout<<x<<" "<<y<<endl;
        }
    }
}
void gen6()  // large: random
{
    srand(rndA*6+rndB);
    int n,m,q;
    freopen("input6.in","w",stdout);
    clr();
    n = MAXN3;
    m = MAXM3;
    q = MAXQ3;
    cout<<n<<" "<<m<<" "<<q<<endl;

    int k = 20;
    int len = 20;
    gen_clusters(k,len,n);
    for(int i=0;i<m;i++)
    {
        int x,y;
        while(1)
        {
            int idx = rand()%k;
            x = V[idx][rand()%len];
            y = V[idx][rand()%len];
            if(insert(x,y))
                break;
        }
        cout<<x<<" "<<y<<endl;
    }

    for(int i=0;i<q;i++)
    {
        int typ = rand()%3;
        cout<<typ<<" ";
        if(typ==0) // add
        {
            int x,y;
            while(1)
            {
                int idx = rand()%k;
                x = V[idx][rand()%len];
                y = V[idx][rand()%len];
                if(insert(x,y))
                    break;
            }
            cout<<x<<" "<<y<<endl;
        }
        else if(typ==1) // rem
        {
            auto it = mp.begin();
            advance(it, rand()%mp.size());
            PII p = it->first;
            cout<<p.first<<" "<<p.second<<endl;
            mp.erase(it);
        }
        else // query
        {
            int x,y;
            if(rand()%4)  // random
            {
                while(1)
                {
                    x = rand()%n+1;
                    y = rand()%n+1;
                    if(x!=y)
                        break;
                }
            }
            else  //biased : clusters
            {
                while(1)
                {
                    int idx = rand()%k;
                    x = V[idx][rand()%len];
                    y = V[idx][rand()%len];
                    if(x!=y) break;
                }
            }
            cout<<x<<" "<<y<<endl;
        }
    }
}
void gen7()  // large: random
{
    srand(rndA*7+rndB);
    int n,m,q;
    freopen("input7.in","w",stdout);
    clr();
    n = MAXN3;
    m = MAXM3;
    q = MAXQ3;
    cout<<n<<" "<<m<<" "<<q<<endl;

    int k = 20;
    int len = 20;
    gen_clusters(k,len,n);
    for(int i=0;i<m;i++)
    {
        int x,y;
        while(1)
        {
            int idx = rand()%k;
            x = V[idx][rand()%len];
            y = V[idx][rand()%len];
            if(insert(x,y))
                break;
        }
        cout<<x<<" "<<y<<endl;
    }

    for(int i=0;i<q;i++)
    {
        int typ = rand()%3;
        cout<<typ<<" ";
        if(typ==0) // add
        {
            int x,y;
            while(1)
            {
                int idx = rand()%k;
                x = V[idx][rand()%len];
                y = V[idx][rand()%len];
                if(insert(x,y))
                    break;
            }
            cout<<x<<" "<<y<<endl;
        }
        else if(typ==1) // rem
        {
            auto it = mp.begin();
            advance(it, rand()%mp.size());
            PII p = it->first;
            cout<<p.first<<" "<<p.second<<endl;
            mp.erase(it);
        }
        else // query
        {
            int x,y;
            if(rand()%4)  // random
            {
                while(1)
                {
                    x = rand()%n+1;
                    y = rand()%n+1;
                    if(x!=y)
                        break;
                }
            }
            else  //biased : clusters
            {
                while(1)
                {
                    int idx = rand()%k;
                    x = V[idx][rand()%len];
                    y = V[idx][rand()%len];
                    if(x!=y) break;
                }
            }
            cout<<x<<" "<<y<<endl;
        }
    }
}


void gen8()  // large: random
{
    srand(rndA*8+rndB);
    int n,m,q;
    freopen("input8.in","w",stdout);
    clr();
    n = MAXN3;
    m = MAXM3;
    q = MAXQ3;
    cout<<n<<" "<<m<<" "<<q<<endl;

    int k = 30;
    int len = 30;
    gen_clusters(k,len,n);
    for(int i=0;i<m;i++)
    {
        int x,y;
        while(1)
        {
            int idx = rand()%k;
            x = V[idx][rand()%len];
            y = V[idx][rand()%len];
            if(insert(x,y))
                break;
        }
        cout<<x<<" "<<y<<endl;
    }

    for(int i=0;i<q;i++)
    {
        int typ = rand()%3;
        cout<<typ<<" ";
        if(typ==0) // add
        {
            int x,y;
            while(1)
            {
                int idx = rand()%k;
                x = V[idx][rand()%len];
                y = V[idx][rand()%len];
                if(insert(x,y))
                    break;
            }
            cout<<x<<" "<<y<<endl;
        }
        else if(typ==1) // rem
        {
            auto it = mp.begin();
            advance(it, rand()%mp.size());
            PII p = it->first;
            cout<<p.first<<" "<<p.second<<endl;
            mp.erase(it);
        }
        else // query
        {
            int x,y;
            if(rand()%4)  // random
            {
                while(1)
                {
                    x = rand()%n+1;
                    y = rand()%n+1;
                    if(x!=y)
                        break;
                }
            }
            else  //biased : clusters
            {
                while(1)
                {
                    int idx = rand()%k;
                    x = V[idx][rand()%len];
                    y = V[idx][rand()%len];
                    if(x!=y) break;
                }
            }
            cout<<x<<" "<<y<<endl;
        }
    }
}




int main()
{
       gen0();
       gen1();
       gen2();
       gen3();
       gen4();
       gen5();
       gen6();
       gen7();
       gen8();
    return 0;
}
