#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll arr[MX];

void add(ll l, ll r)
{
	FOR(i, l, r)
	{
		arr[i] += 1;
	}
}

void del(ll l, ll r)
{
	FOR(i, l, r)
	{
		arr[i] -= 1;
	}
}

ll qq(ll l, ll r)
{
	ll cnt = 0;
	FOR(i, l, r)
	{
		if(!arr[i])
			cnt++;
	}
	return cnt;
}

int main()
{
	ll n, m, q;
	cin >> n >> m >> q;
	REP(i, m)
	{
		ll x, y;
		cin >> x >> y;
		add(x, y);
	}

	REP(i, q)
	{
		ll a, b, c;
		cin >> a >> b >> c;
		if(a == 0)
		{
			add(min(b, c), max(b, c) - 1);
		}
		else if(a == 1)
		{
			del(min(b, c), max(b, c) - 1);
		}
		else
		{
			cout << qq(min(b, c), max(b, c) - 1) << endl;
		}
	}
}