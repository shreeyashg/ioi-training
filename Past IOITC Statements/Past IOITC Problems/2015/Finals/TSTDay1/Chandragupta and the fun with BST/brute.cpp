#include<bits/stdc++.h>
#define assn(n,a,b) assert(n<=b && n>=a)
using namespace std;
#define pb push_back
#define mp make_pair
#define clr(x) x.clear()
#define sz(x) ((int)(x).size())
#define F first
#define S second
#define REP(i,a,b) for(i=a;i<b;i++)
#define rep(i,b) for(i=0;i<b;i++)
#define rep1(i,b) for(i=1;i<=b;i++)
#define pdn(n) printf("%d\n",n)
#define sl(n) scanf("%lld",&n)
#define sd(n) scanf("%d",&n)
#define pn printf("\n")
typedef pair<int,int> PII;
typedef vector<PII> VPII;
typedef vector<int> VI;
typedef vector<VI> VVI;
typedef long long LL;
#define MOD 1000000007ll
LL mpow(LL a, LL n) 
{LL ret=1;LL b=a;while(n) {if(n&1) 
    ret=(ret*b)%MOD;b=(b*b)%MOD;n>>=1;}
return (LL)ret;}
map<int, LL> mymap;
struct node{
    int val;
    LL ans;
    struct node * l;
    struct node * r;
};
node * insert(node * root, int val, LL ans){
    if(root==NULL){
        root=new node;
        root->val=val;
        root->ans=ans;
        root->l=root->r=NULL;
    }
    else if(root->val<val)root->r=insert(root->r,val,(ans*2ll+1ll)%MOD);
    else root->l=insert(root->l,val,(ans*2ll)%MOD);
    return root;
}
void print(node * root){
    if(root==NULL)return;
    print(root->l);
    mymap[root->val]=root->ans;
    print(root->r);
}
int ar[100009];
int main()
{
    int t=1;
    while(t--){
        mymap.clear();
        node * root=NULL;
        int n,x;
        sd(n);
        for(int i=0; i<n; i++){
            sd(ar[i]);
            mymap[ar[i]]=i;
            root=insert(root,ar[i],1);
        }
        print(root);
        for(int i=0; i<n-1; i++)
            cout << mymap[ar[i]] << " ";
        cout << mymap[ar[n-1]] << "\n";
    }
    return 0;
}

