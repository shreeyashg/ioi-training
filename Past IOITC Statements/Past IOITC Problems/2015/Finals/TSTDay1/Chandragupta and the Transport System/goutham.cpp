#include <map>
#include <set>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cctype>
#include <cstdio>
#include <vector>
#include <cassert>
#include <complex>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;
#define int long long

const int N = 1e5 + 23, magic = 333;
int ss[N + N], ff[N + N], pred[N + N], last[N], a[N];
int tin[N], tout[N], dep[N], lazy1[N], lazy2[N];
int tim, orgA, sumA, orgD, curD, sumD;

void dfs1(int u, int p = -1)
{
	tin[u] = ++tim;
	if (~p) a[u] += a[p], dep[u] = dep[p] + 1;
	for (int e = last[u]; e >= 0; e = pred[e])
		if (ff[e] != p)
			dfs1(ff[e], u);
	tout[u] = ++tim;
}

bool anc(int a, int b) { return tin[a] <= tin[b] and tout[a] >= tout[b]; }

void dfs2(int u, int p = -1)
{
	a[u] += sumA + sumD + lazy1[u];
	for (int e = last[u]; e >= 0; e = pred[e])
		if (ff[e] != p)
		{
			orgA += lazy1[u], sumA += orgA + lazy1[u];
			orgD += lazy2[u], curD += orgD, sumD += curD;
			dfs2(ff[e], u);
			sumD -= curD, curD -= orgD, orgD -= lazy2[u];
			sumA -= orgA + lazy1[u], orgA -= lazy1[u];
		}
}


#undef int
int main()
{
#define int long long
	int n, q;
	scanf("%lld%lld", &n, &q);
	for (int i = 0; i < n - 1; i++)
	{
		scanf("%lld%lld", ss + i, ff + i);
		ss[i + n - 1] = --ff[i];
		ff[i + n - 1] = --ss[i];
	}
	for (int i = 0; i < n; i++) last[i] = -1;
	for (int i = 0; i < n + n - 2; i++)
		pred[i] = last[ss[i]], last[ss[i]] = i;

	for (int i = 0; i < n; i++) scanf("%lld", a + i);
	dfs1(0);

	set <int> que;
	while (q--)
	{
		if ((int)que.size() == magic)
		{
			dfs2(0, -1);
			que.clear();
			for (int i = 0; i < n; i++) lazy1[i] = lazy2[i] = 0;
		}

		int ch; scanf("%lld", &ch);
		if (ch == 1)
		{
			int v, fst, snd;
			scanf("%lld%lld%lld", &v, &fst, &snd);
			v--;
			lazy1[v] += fst, lazy2[v] += snd;
			que.insert(v);
		}
		else
		{
			int v; scanf("%lld", &v);
			v--;
			int ans = a[v];
			for (int u : que)
				if (anc(u, v))
				{
					int fst = lazy1[u], snd = lazy2[u], l = dep[v] - dep[u];
					ans += (l + 1) * fst + (l * (l + 1) / 2) * snd;
				}
			printf("%lld\n", ans);
		}
	}
	return 0;
}