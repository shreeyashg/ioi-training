#include<bits/stdc++.h>
#define assn(n,a,b) assert(n<=b && n>=a)
using namespace std;
#define pb push_back
#define mp make_pair
#define clr(x) x.clear()
#define sz(x) ((int)(x).size())
#define F first
#define S second
#define REP(i,a,b) for(i=a;i<b;i++)
#define rep(i,b) for(i=0;i<b;i++)
#define rep1(i,b) for(i=1;i<=b;i++)
#define pdn(n) printf("%d\n",n)
#define sl(n) scanf("%lld",&n)
#define sd(n) scanf("%d",&n)
#define pn printf("\n")
typedef pair<int,int> PII;
typedef vector<PII> VPII;
typedef vector<int> VI;
typedef vector<VI> VVI;
typedef long long LL;
#define MOD 1000000007ll
LL mpow(LL a, LL n) 
{LL ret=1;LL b=a;while(n) {if(n&1) 
    ret=(ret*b)%MOD;b=(b*b)%MOD;n>>=1;}
return (LL)ret;}

#define TRACE

//FILE *fin = freopen("in","r",stdin);
//FILE *fout = freopen("out","w",stdout);


#ifdef TRACE
#define trace1(x)                cerr << #x << ": " << x << endl;
#define tracea(a, n)             for(int i=0; i<n; i++){cerr << ": " << a[i] << " ";if(i==n-1)cout << endl;}
#define trace2(x, y)             cerr << #x << ": " << x << " | " << #y << ": " << y << endl;
#define trace3(x, y, z)          cerr << #x << ": " << x << " | " << #y << ": " << y << " | " << #z << ": " << z << endl;
#define trace4(a, b, c, d)       cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << endl;
#define trace5(a, b, c, d, e)    cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << " | " << #e << ": " << e << endl;
#define trace6(a, b, c, d, e, f) cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << " | " << #e << ": " << e << " | " << #f << ": " << f << endl;

#else

#define trace1(x)
#define trace2(x, y)
#define trace3(x, y, z)
#define trace4(a, b, c, d)
#define trace5(a, b, c, d, e)
#define trace6(a, b, c, d, e, f)

#endif
#define MAXN 100000
#define MAXQ 100000
VI deg[MAXN+10];
LL val[MAXN+10];
int tin[MAXN+10];
int tout[MAXN+10];
int L[MAXN+10];
int tim=1;
LL pathsum[MAXN+10];
LL upda[MAXN+10],updd[MAXN+10];
LL param1,param2,cura,curd,param=0;
struct node{
    int v,a,d;
    node(int vv, int aa, int dd){
        v=vv;a=aa;d=dd;
    }
};
void dfs(int node, int p, int h){
    param += val[node];
    pathsum[node]=param;
    L[node]=h;
    tin[node]=tim;
    tim++;
    for(VI::iterator it=deg[node].begin(); it!=deg[node].end(); it++)
        if((*it)!=p)dfs(*it,node,h+1);
    param -= val[node];
    tout[node]=tim-1;
}
void dfsupdate(int node, int p){
    LL temp=curd;
    param1 += upda[node] + curd;
    param2 += param1;
    pathsum[node]+=param2;
    curd+=updd[node];
    for(VI::iterator it=deg[node].begin(); it!=deg[node].end(); it++)
        if((*it)!=p)dfsupdate(*it,node);
    param2 -= param1;
    param1 -= (upda[node]+temp);
    curd-=updd[node];
}

int main()
{
    int n,q,sqrtq=0,u,v;
    sd(n),sd(q);
    while(sqrtq*sqrtq<q)sqrtq++;
    assn(n,1,MAXN);
    assn(q,1,MAXQ);
    for(int i=1; i<n; i++){
        sd(u),sd(v);
        deg[u].pb(v);
        deg[v].pb(u);
    }
    for(int i=1; i<=n; i++){
        sl(val[i]);
        assn(val[i],0,100000);
    }
    dfs(1,-1,0);
    vector<node> queries;
    while(q--){
        int fl,v,a,d;
        sd(fl),sd(v);
        assn(fl,1,2);
        assn(v,1,n);
        if(fl==2){
            LL ans=pathsum[v];
            int p=queries.size();
            for(int i=0; i<p; i++){
                int vv=queries[i].v,a=queries[i].a,d=queries[i].d;
                if(tin[v]<=tout[vv] and tin[v]>=tin[vv]){
                    LL dell=L[v]-L[vv];
                    ans += ((((LL)(2*a) + (dell*(LL)d))*(dell+1))/2ll);
                }
            }
            printf("%lld\n",ans);
        }
        else{
            sd(a),sd(d);
            assn(a,0,100000);
            assn(d,0,100000);
            queries.pb(node(v,a,d));
        }
        int p=queries.size();
        if(p>sqrtq){
            memset(updd,0,sizeof(updd));
            memset(upda,0,sizeof(upda));
            param1=param2=cura=curd=0;
            for(int i=0; i<p; i++){
                updd[queries[i].v]+=(LL)queries[i].d;
                upda[queries[i].v]+=(LL)queries[i].a;
            }
            dfsupdate(1,-1);
            queries.clear();
        }
    }
    return 0;
}
