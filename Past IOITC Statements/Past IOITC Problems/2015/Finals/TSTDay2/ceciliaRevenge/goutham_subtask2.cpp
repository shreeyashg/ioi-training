#include <map>
#include <set>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cctype>
#include <cstdio>
#include <vector>
#include <cassert>
#include <complex>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;
#define int long long

const int mod = 1e9 + 7;

inline void add(int& a, int b) { a += b; if (a >= mod) a -= mod; }

struct matrix
{
	vector <vector <int> > mat;
	matrix(int n = 0, bool id = false)
	{
		mat.resize(n);
		for (int i = 0; i < n; i++) mat[i].resize(n);
		if (id) for (int i = 0; i < n; i++) mat[i][i] = 1;
	}
	inline int size() { return (int)mat.size(); }
	vector <int>& operator[] (int i) { return mat[i]; }
};

matrix operator * (matrix &a, matrix &b)
{
	int n = a.size();
	matrix ret(n);
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			for (int k = 0; k < n; k++)
			{
				ret[i][j] += 1ll * a[i][k] * b[k][j] % mod;
				if (ret[i][j] >= mod) ret[i][j] -= mod;
			}
	return ret;
}

matrix pow(matrix &a, int p)
{
	int n = a.size();
	matrix ret(n, true);
	while (p > 0)
	{
		if (p % 2 == 1) ret = ret * a;
		p /= 2, a = a * a;
	}
	return ret;
}

#undef int
int main()
{
#define int long long
	int q; scanf("%lld", &q);
	while (q--)
	{
		int n, m; scanf("%lld%lld", &n, &m);

		matrix a(m);
		for (int i = 0; i < m; i++)
			for (int j = 0; j < m; j++)
				a[i][j] = m - max(i + 1, j + 1);
		a = pow(a, (n - 1) / 2);

		int ans = 0;
		for (int i = 0; i < m; i++)
		{
			int cur = 0;
			for (int j = 0; j < m; j++)
				add(cur, a[i][j]);
			if (n % 2 == 0) add(ans, cur * (m - i - 1) % mod);
			else add(ans, cur % mod);
		}
		printf("%lld\n", ans);
	}
	return 0;
}