#include <map>
#include <set>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cctype>
#include <cstdio>
#include <vector>
#include <cassert>
#include <complex>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;
#define int long long

const int mod = 1e9 + 7;

inline void add(int& a, int b) { a += b; if (a >= mod) a -= mod; }

struct matrix
{
	vector <vector <int> > mat;
	void init(int n = 0, bool id = false)
	{
		mat.resize(n);
		for (int i = 0; i < n; i++) mat[i].resize(n);
		if (id) for (int i = 0; i < n; i++) mat[i][i] = 1;
	}
	inline int size() { return (int)mat.size(); }
	vector <int>& operator[] (int i) { return mat[i]; }
};

matrix operator * (matrix &a, matrix &b)
{
	int n = a.size();
	matrix ret;
	ret.init(n);
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			for (int k = 0; k < n; k++)
				add(ret[i][j], a[i][k] * b[k][j] % mod);
	return ret;
}

matrix a[41][20];

void pre()
{
	for (int m = 1; m < 41; m++)
	{
		a[m][0].init(m, false);
		for (int i = 0; i < m; i++)
			for (int j = 0; j < m; j++)
				a[m][0][i][j] = m - max(i + 1, j + 1);
		for (int j = 1; j < 20; j++)
			a[m][j] = a[m][j - 1] * a[m][j - 1];
	}
}

#undef int
int main()
{
#define int long long
	pre();
	int q; scanf("%lld", &q);
	while (q--)
	{
		int n, m; scanf("%lld%lld", &n, &m);

		int cur[m];
		for (int i = 0; i < m; i++) cur[i] = 1;
		for (int i = 0; i < 20; i++)
			if ((n - 1) >> (i + 1) & 1)
			{
				int nxt[m];
				for (int j = 0; j < m; j++) nxt[j] = 0;
				for (int j = 0; j < m; j++)
					for (int k = 0; k < m; k++)
						add(nxt[j], a[m][i][j][k] * cur[k] % mod);
				for (int j = 0; j < m; j++) cur[j] = nxt[j];
			}

		int ans = 0;
		for (int i = 0; i < m; i++)
			if (n % 2 == 0) add(ans, cur[i] * (m - i - 1) % mod);
			else add(ans, cur[i]);
		printf("%lld\n", ans);
	}
	return 0;
}