#include<bits/stdc++.h>
#define assn(n,a,b) assert(n<=b && n>=a)
using namespace std;
#define pb push_back
#define mp make_pair
#define clr(x) x.clear()
#define sz(x) ((int)(x).size())
#define F first
#define S second
#define REP(i,a,b) for(i=a;i<b;i++)
#define rep(i,b) for(int i=0;i<b;i++)
#define rep1(i,b) for(i=1;i<=b;i++)
#define pdn(n) printf("%d\n",n)
#define sl(n) scanf("%lld",&n)
#define sd(n) scanf("%d",&n)
#define pn printf("\n")
typedef pair<int,int> PII;
typedef vector<PII> VPII;
typedef vector<int> VI;
typedef vector<VI> VVI;
typedef long long LL;
#define MOD 1000000007ll
#define MAXM 40
#define MAXN 1000000
#define LOGMAXN 20
LL even[MAXM+10][MAXM+10];
LL odd[MAXM+10][MAXM+10];
LL T[LOGMAXN+10][MAXM+10][MAXM+10];
LL temp[LOGMAXN+10][MAXM+10][MAXM+10];
void pre(int n){
    memset(T,0,sizeof(T));
    memset(even,0,sizeof(even));
    memset(odd,0,sizeof(odd));
    memset(temp,0,sizeof(temp));
    for(int i=0; i<n; i++)
        for(int j=0; j<n; j++){
            if(j<i)even[i][j]=1;
            else if(j>i)odd[i][j]=1;
        }
    rep(i,n)rep(j,n)rep(k,n){
        temp[0][i][j]+=(even[i][k]*odd[k][j])%MOD;
        if(temp[0][i][j]>=MOD)temp[0][i][j]-=MOD;
    }
    rep(i,n)T[0][i][i]=1;
}
int main()
{
    int t;
    cin >> t;
    while(t--)
    {
        LL N,n;
        sl(N),sl(n);
        N-=1;
        LL tmp=N/2;
        pre(n);
        int cnt1=1,cnt2=1;
        while(tmp){
            if(tmp&1){
                rep(i,n)rep(j,n)rep(k,n){
                    T[cnt1][i][j]+=(T[cnt1-1][i][k]*temp[cnt2-1][k][j])%MOD;
                    if(T[cnt1][i][j]>=MOD)T[cnt1][i][j]-=MOD;
                }
                cnt1++;
            }
            rep(i,n)rep(j,n)rep(k,n){
                temp[cnt2][i][j]+=(temp[cnt2-1][i][k]*temp[cnt2-1][k][j])%MOD;
                if(temp[cnt2][i][j]>=MOD)temp[cnt2][i][j]-=MOD;
            }
            cnt2++;
            tmp>>=1;
        }
        if(N%2){
            rep(i,n)rep(j,n)rep(k,n){
                T[cnt1][i][j]+=(T[cnt1-1][i][k]*even[k][j])%MOD;
                if(T[cnt1][i][j]>=MOD)T[cnt1][i][j]-=MOD;
            }
            cnt1++;
        }
        LL ret=0;
        rep(i,n)rep(j,n){
            ret += T[cnt1-1][i][j];
            if(ret>=MOD)ret-=MOD;
        }
        printf("%lld\n",ret);
    }
    return 0;
}
