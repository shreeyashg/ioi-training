#include<bits/stdc++.h>
#define assn(n,a,b) assert(n<=b && n>=a)
using namespace std;
#define pb push_back
#define mp make_pair
#define clr(x) x.clear()
#define sz(x) ((int)(x).size())
#define F first
#define S second
#define REP(i,a,b) for(i=a;i<b;i++)
#define rep(i,b) for(int i=0;i<b;i++)
#define rep1(i,b) for(i=1;i<=b;i++)
#define pdn(n) printf("%d\n",n)
#define sl(n) scanf("%lld",&n)
#define sd(n) scanf("%d",&n)
#define pn printf("\n")
typedef pair<int,int> PII;
typedef vector<PII> VPII;
typedef vector<int> VI;
typedef vector<VI> VVI;
typedef long long LL;
#define MOD 1000000007ll
#define MAXM 50
#define MAXN 1000000000
#define LOGMAXN 30
LL I[MAXM+10][MAXM+10];
LL store[MAXM+10][LOGMAXN+10][MAXM+10][MAXM+10];
LL EVEN[MAXM+10][MAXM+10][MAXM+10];
LL even[MAXM+10][MAXM+10];
LL odd[MAXM+10][MAXM+10];
LL T[MAXM+10][MAXM+10];
LL ans[LOGMAXN+10][MAXM+10][1];
void prepre(){
    int n=MAXM+2;
    for(int i=0; i<n; i++)
        I[i][i]=1;
    for(int i=0; i<n; i++)
        for(int j=0; j<n; j++){
            if(j<i)even[i][j]=1;
            else if(j>i){
                odd[i][j]=1;
                for(int k=0; k<n; k++)
                    EVEN[k][i][j]=1;
            }
        }
}
void pre(int n){
    rep(i,n)rep(j,n)rep(k,n){
        store[n][0][i][j]+=(even[i][k]*odd[k][j])%MOD;
        if(store[n][0][i][j]>=MOD)store[n][0][i][j]-=MOD;
    }
    LL cnt=1;
    while(cnt<=LOGMAXN){
        rep(i,n)rep(j,n)rep(k,n){
            store[n][cnt][i][j]+=(store[n][cnt-1][i][k]*store[n][cnt-1][k][j])%MOD;
            if(store[n][cnt][i][j]>=MOD)store[n][cnt][i][j]-=MOD;
        }
        cnt++;
    }
}
int main()
{
    prepre();
    for(int i=1; i<=MAXM; i++)
        pre(i);
    int t;
    cin >> t;
    while(t--)
    {
        LL N,n;
        sl(N),sl(n);
        N-=1;
        memset(ans,0,sizeof(ans));
        for(int i=0; i<n; i++)
            ans[0][i][0]=1;
        int cnt=1;
        for(int i=0; i<LOGMAXN; i++)
            if((N/2)&(1<<i)){
                rep(ii,n)rep(jj,1)rep(kk,n){
                    ans[cnt][ii][jj]+=(store[n][i][ii][kk]*ans[cnt-1][kk][jj])%MOD;
                    if(ans[cnt][ii][jj]>=MOD)ans[cnt][ii][jj]-=MOD;
                }
                cnt++;
            }
        LL ret=0;
        if(N%2){
            rep(ii,n)rep(jj,1)rep(kk,n){
                ans[cnt][ii][jj]+=(EVEN[n][ii][kk]*ans[cnt-1][kk][jj])%MOD;
                if(ans[cnt][ii][jj]>=MOD)ans[cnt][ii][jj]-=MOD;
            }
            cnt++;
        }
        rep(i,n)
        {
            ret += ans[cnt-1][i][0];
            if(ret>=MOD)ret-=MOD;
        }
        printf("%lld\n",ret);
    }
    return 0;
}
