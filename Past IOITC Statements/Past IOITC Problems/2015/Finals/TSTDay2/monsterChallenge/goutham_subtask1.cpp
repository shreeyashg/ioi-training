#include <map>
#include <set>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cctype>
#include <cstdio>
#include <vector>
#include <cassert>
#include <complex>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;
#define int long long

const int N = 23;
int a[N];
bool adj[N][N];

int f(int a, int b)
{
	if (a == 0) return 0;
	if (b % a == 0) return 0;
	return a % (b % a);
}

#undef int
int main()
{
#define int long long
	int n; cin >> n;
	for (int i = 0; i < n; i++) cin >> a[i];
	for (int i = 0; i < n; i++)
		for (int j = i + 1; j < n; j++)
		{
			int cur = a[i];
			for (int k = i + 1; k <= j; k++)
				cur = f(cur, a[k]);
			if (cur != 0) adj[i][j] = adj[j][i] = true;
		}
	int ans = 0;
	for (int msk = 1; msk < 1 << n; msk++)
	{
		bool can = true;
		for (int i = 0; i < n; i++)
			if (msk >> i & 1)
			{
				int ct = 0;
				for (int j = 0; j < n; j++)
					if (msk >> j & 1)
						if (adj[i][j])
							ct++;
				if (!(ct == 2 or ct == 3 or ct == 5 or ct == 7))
					can = false;
			}
		if (can) ans = max(ans, 0ll + __builtin_popcountll(msk));
	}
	cout << ans << "\n";
	return 0;
}