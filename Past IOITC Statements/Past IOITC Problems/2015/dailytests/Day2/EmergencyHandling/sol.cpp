#include<iostream>
#include<stdio.h>
#include<queue>
#include<string.h>
#include<algorithm>
#include<math.h>
#include<bits/stdc++.h>
#define assn(n,a,b) assert(n<=b and n>=a)
#define inff 0x3ffffff
typedef long long LL;
using namespace std;
int n;
priority_queue<int>que[110];
void init()
{
    int i;
    for(i=0;i<=100;i++)
    {
        while(que[i].size())
            que[i].pop();
    }
}
int main()
{
    int t,i,j;
    char s[5];
    scanf("%d",&t);
    assn(t,1,5);
    LL ix,fc;
    int id;
    int cas=1;
    while(t--)
    {
        init();
        scanf("%d",&n);
        assn(n,1,100000);
        int t0,st0,r;
        int prev=-1;
        for(i=1;i<=n;i++)
        {
            scanf("%s",s);
            if(s[0]=='P')
            {
                scanf("%d%d%d",&t0,&st0,&r);
                assert(t0>=prev);
                prev=t0;
                assn(t0,0,1000000);
                assn(st0,0,100000000);
                assn(r,0,100);
                que[r].push(st0-r*t0);
            }
            else
            {
                scanf("%d",&t0);
                assert(t0>=prev);
                prev=t0;
                ix=-inff;
                id=-1;
                for(j=100;j>=0;j--)
                {
                    if(que[j].size()==0)
                        continue;
                    fc=que[j].top();
                    fc=fc+(LL)j*t0;
                    if(fc>ix)
                    {
                        ix=fc;
                        id=j;
                    }
                }
                que[id].pop();
                printf("%lld %d\n",ix,id);
            }
        }
    }
    return 0;
}
