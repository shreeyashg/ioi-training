#include <bits/stdc++.h>
#define rf freopen("in.in", "r", stdin);
#define wf freopen("out.out", "w", stdout);
#define rep(i, s, n) for(int i=int(s); i<=int(n); ++i)
using namespace std;
const int mx = 1e6+10;

vector< pair<int, int> > g[mx];
int n, vis[mx];
long long ans = 0;

void dfs(int u, int ed)
{
	vis[u] = 1;
	rep(i, 0, g[u].size()-1)
	{
		int v = g[u][i].first, d = g[u][i].second;
		if(vis[v]) continue;

		int tmp = min(ed, d); ans += tmp;
		dfs(v, tmp);
	}
}

int main()
{
	rf;// wf;
	
	scanf("%d", &n);
	rep(i, 0, n-1)
	{
		int u, v, d;
		scanf("%d %d %d", &u, &v, &d);
		g[u].push_back(make_pair(v, d));
		g[v].push_back(make_pair(u, d));
	}

	rep(i, 1, n)
	{
		memset(vis, 0, sizeof vis);
		dfs(i, mx);
	}
	ans/=2;

	printf("%lld\n", ans);
	return 0;
}