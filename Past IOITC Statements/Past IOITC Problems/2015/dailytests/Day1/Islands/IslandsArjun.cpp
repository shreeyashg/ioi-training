#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

int parent[1000010], rank[1000010], n,a,b,w,c;
long long int tot;
pair < int, pair<int,int> > p;
vector< pair < int, pair<int,int> > > V;


int find(int i)
{
  if(parent[i] == i) 
    return i;
  parent[i] = find(parent[i]);
  return parent[i];
}

int join(int i, int j)
{
  if(rank[i] < rank[j])
    swap(i,j);
  parent[j] = i;
  rank[i] += rank[j];
}


int main()
{
  cin>>n;
  
  for(int i=1;i<=n;i++)
  {
    parent[i] = i;
    rank[i] = 1;
  }
  
  for(int i=1;i<=n-1;i++)
  {
    cin>>a>>b>>w;
    p.first = w;
    p.second.first = a;
    p.second.second = b;
    V.push_back(p);
  }
  
  sort(V.begin(),V.end());
  
  tot=0;
  
  for(int i= n-2;i>=0;i--)
  {
    a=V[i].second.first;
    b=V[i].second.second;
    a = find(a);
    b = find(b);
    
    c=V[i].first;
    tot += (rank[a] * rank[b] * c);
    join(a,b);
  }
  
  cout<<tot<<"\n";
}