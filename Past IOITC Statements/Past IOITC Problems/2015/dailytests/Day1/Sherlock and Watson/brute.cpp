#include<bits/stdc++.h>
#define assn(n,a,b) assert(n<=b and n>=a)
using namespace std;
int ar[109]={},dp[109][109]={};
int rec(int i, int j){
    if(i<1)return 0;
    int &ret=dp[i][j];
    if(ret!=-1)return ret;
    ret=INT_MIN;
    for(int it=1; it<=ar[i-1]; it++)
        ret=max(ret,abs(j-it)+rec(i-1,it));
    return ret;
}
int main()
{
    int t;
    cin >> t;
    assn(t,1,20);
    while(t--)
    {
        memset(dp,-1,sizeof(dp));
        int n,i,j,ans=INT_MIN;
        cin >> n;
        assn(n,1,100);
        for(i=0; i<n; i++){
            cin >> ar[i];
            assn(ar[i],1,100);
        }
        for(int i=1; i<=ar[n-1]; i++)
            ans=max(ans,rec(n-1,i));
        cout << ans << endl;
    }
    return 0;
}
