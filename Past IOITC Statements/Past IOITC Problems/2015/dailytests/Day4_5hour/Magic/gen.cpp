#include<bits/stdc++.h>
using namespace std;

#define LET(x, a)  __typeof(a) x(a)
#define TR(v, it) for(LET(it, v.begin()); it != v.end(); it++)
#define si(x) scanf("%d",&x)
#define F first
#define S second
#define PB push_back
#define MP make_pair
#define INF 1000000000
#define MOD 1000000007
#define SET(x,y) memset(x,y,sizeof(x));
#define LL long long int
#define ULL unsigned LL
#define PII pair<int, int>
#define MAX 1000300
int cs = 0;
int loc[MAX];
int BIT[MAX];
typedef struct _Node {
    int x, y, cnt, orig_val;
    int cadd;
    int acnt;
    _Node *l, *r, *par;
    _Node(int loc, int val) : x(loc), y((rand() << 16) ^ rand()), cnt(1), l(NULL), r(NULL) {
        acnt = (val > 0);
        orig_val = val;
        par = NULL;
    }
    ~_Node() { delete l; delete r; }
    void recalc() {
        push();
        cnt = 1;
        acnt = (orig_val > 0);
        if (l) {
            cnt += l->cnt;
            acnt += l->acnt;
        }
        if (r) {
            cnt += r->cnt;
            acnt += r->acnt;
        }
    }
    void add(int v) {
        x += v;
        cadd += v;
    }
    void push() {
        if (l) {
            l->add(cadd);
        } 
        if (r) {
            r->add(cadd);
        }
        cadd = 0;
    }
    void inorder() {
        push();
        if (l)
            l->inorder();
        cout << orig_val << " ";
        if (r)
            r->inorder();
    }
} *Node;


class cart_tree {
    Node merge(Node l, Node r) {
        if (!l || !r) return l ? l : r;
        l->push();
        r->push();
        if (l->y < r->y) {
            l->r = merge(l->r, r);
            l->r->par = l;
            l->recalc();
            return l;
        } else {
            r->l = merge(l, r->l);
            r->l->par = r;
            r->recalc();
            return r;
        }
    }

    void split(Node v, int x, Node &l, Node &r) {
        l = r = NULL;
        if (!v) return;
        v->push();
        if (v->x < x) {
            split(v->r, x, v->r, r);
            l = v;
            if (v->r)
                v->r->par = v;
        } else {
            split(v->l, x, l, v->l);
            r = v;
            if (v->l)
                v->l->par = v;
        }
        v->recalc();
    }
    Node root;
    Node getkth(Node cnode, int k) {
        cnode->push();
        if (cnode->l != NULL) {
            if (cnode->l->acnt >= k)
                return getkth(cnode->l, k);
            k -= cnode->l->acnt;
        }
        if (cnode->orig_val > 0) {
            if (k == 1) 
                return cnode;
            k -= 1;
        }
        return getkth(cnode->r, k);
    }
    public:
    void traverse() {
        root->inorder();
        cout << endl;
    }
    cart_tree() : root(NULL) {}
    ~cart_tree() { delete root; }
    Node insert(int loc, int val) {
        Node l, r;
        split(root, loc, l, r);
        Node nnode = new _Node(loc, val);
        root = merge(merge(l, nnode), r);
        return nnode;
    }
    void erase(int x) {
        Node l, m, r;
        split(root, x, l, m);
        split(m, x + 1, m, r);
        assert(m && m->cnt == 1 && m->x == x);
        delete m;
        root = merge(l, r);
    }
    int getKth(int x) {
        return getkth(root, x)->orig_val;
    }
    void move(int A) {
            Node AandBefore, beforeA, onlyA, afterA;
            split(root, A + 1, AandBefore, afterA);
            split(AandBefore, A, beforeA, onlyA);
            if (beforeA)
                beforeA->add(1);
            if (onlyA && beforeA)
                onlyA->add(-beforeA->cnt);
            root = merge(
                merge(onlyA, beforeA),
                afterA
            );
    }
    int size() const { return root ? root->cnt : 0; }
} *ct;


void gen(int n, int m) {
    printf("Generating case %d(n = %d, m = %d)\n", cs, n, m);
    vector<int> v;
    for (int i = 0; i < n; i++) {
        v.PB(i + 1);
    }
    random_shuffle(v.begin(), v.end());
    ct = new cart_tree();
    for (int i = 0; i < n; i++) {
        ct->insert(i, v[i]);
    }
    char fname[20];
    sprintf(fname, "input%d.in", cs++);
    FILE* f = fopen(fname, "w");
    // vector<int> v2 = v;
    fprintf(f, "%d %d\n", n, m);
    for (int i = 0; i < m; i++) {
        int x = rand() % n;
        int ele = ct->getKth(x + 1);
        // assert(ele == v2[x]);
        fprintf(f, "%d %d\n", ele, x + 1);
        ct->move(x);
        // v2.erase(v2.begin() + x);
        // v2.insert(v2.begin(), ele);
    }
    fclose(f);
}

int main() {
    srand(1234);
    gen(10000, 0);
    gen(10000, 1);
    gen(10000, 10);
    gen(10000, 100);
    gen(10000, 1000);
    gen(10000, 10000);
    gen(1000, 1000);
    gen(100, 10000);
    gen(10, 10000);

    gen(1000000, 0);
    gen(1000000, 1);
    gen(1000000, 10);
    gen(1000000, 100);
    gen(1000000, 1000);
    gen(10000, 1000000);
    gen(1000, 1000000);
    gen(100, 1000000);
    gen(10, 1000000);
    gen(1000000, 1000000);
    return 0;
}

