#include<iostream>
#include<vector>
#include<queue>
#include<algorithm>
#include<cassert>

using namespace std;

priority_queue < pair< long long int, int> , vector<pair< long long int, int> > , greater< pair< long long int, int> > > Q;

int n,m,a, temp1, AA[1000010], u, v, burnt[1000010];
long long int w, D[1000010], INF = 1000000000000000000,d, c;
vector<int>A;
pair<long long int, int> p;
vector < pair<long long int, int> > V[1000010];


int main()
{
  cin>>n>>m>>a;
  assert(n>1);
  assert(n<=100000);
  //assert(n<=1000);
  //assert(m>0);
  assert(m<=100000);
  //assert(m<=1000);
  assert(a>=1 && a<= n);
  
  for(int i=1;i<=a;i++)
  {
    cin>>temp1;
    assert(temp1 <= n && temp1 >= 1);
    A.push_back(temp1);
    AA[temp1] = 1;
  }
  
  for(int i=1;i<=m;i++)
  {
    cin>>u>>v>>w;
    assert( u>=1 && u<=n && v>=1);
    assert(v<=n);
    assert(u!=v);
    assert(w>=1 && w<=10000000000);
    p.first = w;
    p.second = v;
    V[u].push_back(p);
    p.second = u;
    V[v].push_back(p);
  }
  
  for(int i=1;i<=a;i++)
  {
    p.first = 0;
    p.second = n+1;
    V[A[i-1]].push_back(p);
    p.second = A[i-1];
    V[n+1].push_back(p);
  }
  
  for(int i=1;i<=n+1;i++)
  {
    D[i] = INF;
    burnt[i] = 0;
  }
  
  p.first = 0;
  p.second = n+1;
  Q.push(p);
  D[n+1] = 0;
  
  while(!Q.empty())
  {
    d = Q.top().first;
    u = Q.top().second;
    Q.pop();
    
    if(burnt[u])
      continue;
    
    if(d > D[u])
      continue;
    
    for(int i=0;i<V[u].size();i++)
    {
      v = V[u][i].second;
      c = V[u][i].first;
      if(burnt[v])
	continue;
      if( (d+c) < D[v])
      {
	D[v] = d+c;
	p.first = d+c;
	p.second = v;
	Q.push(p);
      }
    }
  }
  
  for(int i=1;i<=n;i++)
  {
    if(AA[i])
      continue;
    cout<<i<<" ";
    if(D[i]==INF)
      cout<<-1<<"\n";
    else
      cout<<D[i]<<"\n";
  }
}
