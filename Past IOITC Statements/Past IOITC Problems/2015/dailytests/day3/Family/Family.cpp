#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <set>
#include<bits/stdc++.h>
using namespace std;
const int M = 100 * 10000 +5;
vector <int> G[M], ls[M], la[M], ans;
int s[M], d[M], f[M], curpos = 0;

void dfs(int v,int ch)
{
    la[ch].push_back(v);
    d[v] = ch;
    s[v] = curpos++;
    ls[ch].push_back(s[v]);
    for(int i = 0; i < G[v].size(); i++)
        dfs(G[v][i], ch + 1);
    f[v] = curpos;
}

int main()
{
    ios::sync_with_stdio(false);
    int n, m;
    cin >> n;
    for(int i = 1; i<n+1; i++)
    {
        int x;
        cin >> x;
        assert(x>=0 and x<=n);
        G[x].push_back(i);
    }
    dfs(0,0);   
    cin >> m;
    for (int i = 0; i<m; i++)
    {
        int v,p;
        cin >> v >> p;
        if (d[v] <= p)
        {
            cout << 0 << endl;
            continue;
        }
        int u = lower_bound(ls[d[v] - p].begin(), ls[d[v] - p].end(),s[v]) - ls[d[v]-p].begin() - 1;
        int low = lower_bound(ls[d[v]].begin(), ls[d[v]].end(), s[la[d[v]-p][u]]) - ls[d[v]].begin();
        int up = lower_bound(ls[d[v]].begin(), ls[d[v]].end(), f[la[d[v]-p][u]]) - ls[d[v]].begin();
        cout << up - low - 1 << '\n';
    }
    return 0;
}
