#include<bits/stdc++.h>
using namespace std;

#define LET(x, a)  __typeof(a) x(a)
#define TR(v, it) for(LET(it, v.begin()); it != v.end(); it++)
#define si(x) scanf("%d",&x)
#define F first
#define S second
#define PB push_back
#define MP make_pair
#define INF 1000000000
#define MOD 1000000007
#define SET(x,y) memset(x,y,sizeof(x));
#define LL long long int
#define ULL unsigned LL
#define PII pair<int, int>
#define MAX 1000003
int par[24][MAX];
int tms[MAX][2];
int curtime = 3;
int L[MAX];
vector<int> v[MAX];
vector<int> sorted_vals[MAX];
int getpar(int v, int x) {
    int cur = 0;
    while (x) {
        if ((x & 1)) {
            v = par[cur][v];
        }
        cur++;
        x >>= 1;
    }
    return v;
}
int maxh=-1;
void dfs(int cnode, int clev = 0) {
    maxh=max(maxh,clev);
    tms[cnode][0] = curtime++;
    L[cnode] = clev;
    sorted_vals[clev].PB(tms[cnode][0]);
    for (int i = 0; i < (int)v[cnode].size(); i++) {
        dfs(v[cnode][i], clev + 1);
    }
    tms[cnode][1] = curtime++;
}
int main() {
    int i, n, j;
    cin >> n;
    par[0][0] = 0;
    for (i = 1; i <= n; i++) {
        cin >> par[0][i];
        v[par[0][i]].PB(i);
    }
    for (i = 1; i < 24; i++) {
        for (j = 1; j <= n; j++) {
            par[i][j] = par[i - 1][par[i - 1][j]];
        }
    }
    for (i = 1; i <= n; i++) {
        if (par[0][i] == 0)
            dfs(i);
    }
    int q, qv, qp;
    cin >> q;
    while (q--) {
        scanf("%d %d", &qv, &qp);
        assert(qp>=1 and qp<=maxh);
        int req_node = getpar(qv, qp);
        int req_lev = L[qv];
        int ans;
        if (req_node == 0) 
            ans = 0;
        else 
            ans = (
                upper_bound(sorted_vals[req_lev].begin(), sorted_vals[req_lev].end(), tms[req_node][1]) - 
                  upper_bound(sorted_vals[req_lev].begin(), sorted_vals[req_lev].end(), tms[req_node][0]) - 1
            );

        printf("%d\n", ans);
    }
    return 0;
}

