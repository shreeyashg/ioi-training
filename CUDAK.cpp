#include <bits/stdc++.h>
#pragma GCC optimize("O8")
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll a, b, s;
string ss;
ll dp[17][2][140];

ll f(ll idx, bool b, ll cs)
{

	if(cs > s)
		return 0;


	if(idx == ss.size() && cs == s)
		return 1;
	else if(idx == ss.size())
		return 0;

	if(dp[idx][b][cs] != -1)
		return dp[idx][b][cs];


	ll ans = 0;

	if(b == 0)
	{
		FOR(i, 0, 9)
		{
			if(i < ss[idx] - '0')
			{
				ans += f(idx + 1, 1, cs + i);
			}
			
			else if(i == ss[idx] - '0')
			{
				ans += f(idx + 1, 0, cs + i);
			}
		}
	}
	else
	{
		FOR(i, 0, 9)
		{
			ans += f(idx + 1, 1, cs + i);
		}
	}

	return dp[idx][b][cs] = ans;
}

ll calc(ll x)
{
	memset(dp, -1, sizeof(dp));
	ss = to_string(x);
	return f(0, 0, 0);
}

int main()
{
	memset(dp, -1, sizeof(dp));
	cin >> a >> b >> s;
	ll ab = calc(b);
	ll aa = calc(a);
	ll x = ab - calc(a);// << endl;
	cout << x << endl;
	ll lo = a, hi = b, ans = -1;
	REP(i, 55)
	{
		ll mid = (lo + hi)/2;
		if(calc(mid) - aa == 1)
		{
			ans = mid;
			hi = mid - 1;
		}
		else if(calc(mid) - aa > 1)
		{
			hi = mid - 1;
		}
		else if(calc(mid) == aa)
			lo = mid + 1;
	}

	cout << ans << endl;
}