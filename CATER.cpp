#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 10243, INF = 1e18, MX = 2.5e3 + 4;

ll n, k, vis[MX], dp[MX][MX], sz[MX];
vector<ll> v[MX], child[MX];

// sz[x] includes idx;

void dfs(ll idx)
{
	REP(i, child[idx].size())
		dfs(child[idx][i]);

	vector< vector<ll> > c(child[idx].size() + 1, vector<ll>(k + 1));

//	REP(i, c.size()) REP(j, c[i].size()) c[i][j] = 1;
//	c[0][0] = 1;
	ll cnt = 0;


//	FOR(i, 0, child[idx].size()) c[i][0] = 1;
	c[0][0] = 1;

	REP(i, child[idx].size())
	{	
		FOR(j, 0, min(sz[idx]-1, k - 1))
		{
			FOR(p, 0, j)
			{
				if(j - p <= sz[child[idx][i]])
					c[i+1][j] += (c[i][p]%MOD * dp[child[idx][i]][j-p])%MOD, c[i+1][j]%=MOD;

			}
		//	cout << "c[" << i + 1 << "][" << j << "] = " << c[i + 1][j] << endl;
		}
	}
//	cout << endl;
	dp[idx][0] = 1;
	FOR(i, 1, k)
	{
		dp[idx][i] = c[child[idx].size()][i-1];
	//	cout << "dp[" << idx << "][" << i << "] = " << c[child[idx].size()][i-1] << endl;
	}

//	cout << endl << endl;
}


void init(ll idx)
{
//	cout << idx << ", ";
	vis[idx] = 1;

	if(v[idx].size() == 1 && idx != 1)
		sz[idx] = 1;

	sz[idx] = 1;

	REP(i, v[idx].size())
	{
		if(!vis[v[idx][i]])
		{
			init(v[idx][i]);
			child[idx].push_back(v[idx][i]);
			sz[idx] += sz[v[idx][i]];
		}
	}
}

int main()
{
	cin >> n >> k;
	REP(i, n - 1)
	{
		ll a, b;
		cin >> a >> b;
		v[a].push_back(b);
		v[b].push_back(a);
	}

	init(1);
//	cout << endl;
	dfs(1);

	ll ans = 0;
	FOR(i, 1, n)
	{
		ans += dp[i][k]%MOD;
		ans %= MOD;
	}

	cout << ans << endl;
//	cout << endl;
	
/*	cout << "END" << endl;
	FOR(i, 1, n)
	{
		cout << i << " " << sz[i] << endl;
	}
	cout << endl;


	FOR(i, 1, n)
	{
		cout << i << " : " << endl;
		REP(k, child[i].size())
		{
			cout << child[i][k] << ", "; 
		}		
		cout << "*" << endl;
	}
*/


}