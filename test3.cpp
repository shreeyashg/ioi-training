#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll ta[505][505];

void init()
{
	FOR(i, 1, 500)
	{
		ta[i][0] = 1;
		ta[i][i] = 1;
		FOR(j, 1, i - 1)
		{
			ta[i][j] = (ta[i - 1][j] % MOD) + (ta[i - 1][j - 1] % MOD);
			ta[i][j] %= MOD;
		}
	}
}

ll pow(ll x, ll k)
{
	ll res = x;
	FOR(i, 2, k)
	{
		res = res * x;
		res %= MOD;
	}
	return res;
}

int main()
{
	init();
	ll n, x, k;
	cin >> n >> x >> k;

	
}