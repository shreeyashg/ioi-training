#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, MX = 3e4 + 4;

struct node
{
	ll from, to, val, lazy;
	node *left, *right;
	node()
	{
		from = 1;
		to = 1e9;
		val = 0, lazy = 0, left = NULL, right = NULL;
	}

	void extend()
	{
		if(left == NULL)
		{
			left = new node(), right = new node();
			left->from = from;
			left->to = (from + to) >> 1;
			right->from = ((from + to) >> 1) + 1;
			right->to = to;
		}
	}
};

node *root;

/*
	1 
	5
	1 4
	2 6
	8 10
	3 4
	7 10
*/

void update(node *curr, ll l, ll r, ll val)
{
	if((curr->from) > r || (curr->to) < l || (curr->from) > (curr->to))
		return;


	if(curr->from >= l && curr->to <= r)
	{
		curr->val = val;
		if(curr->left == NULL && curr->right == NULL && curr->from != curr->to)curr->extend();
		return;
	}

	curr->extend();
	if(curr->val)curr->left->val = curr->val, curr->right->val = curr->val;
	if(curr->from != curr->to)curr->val = 0;
	update(curr->left, l, r, val);
	update(curr->right, l, r, val);
}

set<ll> s;

void cnt(node *curr)
{
//	cout << curr->from << " " << curr->to << " " << curr->val << endl;;
	if(curr->val)
	{
		s.insert(curr->val);
		return;
	}

	if(curr->left == NULL || curr->right == NULL)
		return;

	cnt(curr->left);
	cnt(curr->right);
}

int main()
{
	ll t;
	cin >> t;
	while(t--)
	{
		node *root = new node();

		ll n;
		cin >> n;

		ll x, y;
		vector<pair<ll, ll> > upds;
		REP(i, n)
		{
			cin >> x >> y;
			upds.push_back({x, y});
		}

		REP(i, upds.size())
		{

			x = upds[i].first, y = upds[i].second;
			update(root, x, y, i + 1);
		//	cout << x << " &&&&&&&&& &&&&&&&& " << y << endl;
	//		cnt(root);
	//		cout << endl;
		//	cout << endl << endl << endl;
		}
	 	cnt(root);
	//	cnt(root);
		cout << s.size() << endl;

		delete root;
		s.clear();
	}
}