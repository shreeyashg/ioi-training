#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cerr<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll a, b;
string s;
ll dp[20][2][11][11][4];

ll f(ll idx, bool b, ll pree, ll pre, ll x)
{
//	cout << idx << " " << b << " " << pree << " " << pre << endl;
//	DEBUG("x");	
	if(x >= 2)
	{
		if(pree == pre)
			return 0;
	}

	if(idx == s.size())
		return 1;

//	DEBUG("x");
	if(dp[idx][b][pree][pre][x] != -1)
		return dp[idx][b][pree][pre][x];
//	DEBUG("x");
	ll ans = 0;
	FOR(i, 0, 9)
	{
		if(b == 0 && i < s[idx] - '0')
		{
			if(i == pree && x >= 2)
				continue;

			if(i == pre && x)
				continue;

			ans += f(idx + 1, 1, pre, i, (!x ? i > 0 : min(x + 1, 2ll)));
		}

		if(b == 0 && i == s[idx] - '0')
		{
		//	cout << pree << " " << pre << " " << i << " " << x << endl;

			if(i == pree && x >= 2)
				continue;

			if(i == pre && x)
				continue;

			ans += f(idx + 1, 0, pre, i, (!x ? i > 0 : min(x + 1, 2ll)));
		}

		if(b == 1)
		{
			if(i == pree && x >= 2)
				continue;
	
			if(i == pre && x)
				continue;

			ans += f(idx + 1, 1, pre, i, (!x ? i > 0 : min(x + 1, 2ll)));
		}
	}

	return dp[idx][b][pree][pre][x] = ans;
}

ll calc(ll x) // Palindrome free <= x
{
	memset(dp, -1, sizeof(dp));
	s = to_string(x);
	return f(0, 0, 10, 10, 0);
}

int main()
{	
//	memset(dp, -1, sizeof(dp));
	cin >> a >> b;
/*	a--;
	s = to_string(a);
	ll ff = f(0, 0, 10, 10);
	s = to_string(b);
	memset(dp, -1, sizeof(dp));
	ll fff = f(0, 0, 10, 10);
	cout << fff - ff << endl;
*/	

//	cout << calc(123) << endl;
//	return 0;

//	calc(101);
	//return 0;
	cout << calc(b) - calc(a-1) << endl;

}