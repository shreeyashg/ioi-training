#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll t, n, vis[MX], dp[MX][2];
vector<ll> v[MX], child[MX];

void init(ll idx)
{ 
	if(vis[idx])
		return;
	vis[idx] = 1;
	REP(i, v[idx].size())
	{
		if(!vis[v[idx][i]])
		{
			init(v[idx][i]);
			child[idx].push_back(v[idx][i]);
		}
	}
}

void dfs(ll idx)
{
	REP(i, child[idx].size())
	{
		dfs(child[idx][i]);
	}

	if(child[idx].size() == 0)
	{
		dp[idx][0] = 1;
		dp[idx][1] = 1;
		return;
	}

	ll ans = INF, ans_0 = INF, minsum;
	REP(p, 2)
	{
		REP(child[idx].size())
		{
						
		}
	}

}

int main()
{
	cin >> t;
	while(t--)
	{
		memset(dp, 0, sizeof(dp));
		REP(i, MX) v[i].clear(), child[i].clear();
		memset(vis, 0, sizeof(vis));
	//	ll n; 
		cin >> n;

		REP(i, n - 1)
		{
			ll x, y;
			cin >> x>> y;
			v[x].push_back(y);
			v[y].push_back(x);
		}

		if(n == 1)
		{
			cout << "-1" << endl;
			continue;
		}

		if(n == 2)
		{
			cout << "2" << endl;
			continue;
		}

		init(1);

		dfs(1);

		cout << dp[1][1] << endl;

		/*

		FOR(i, 1, n)
		{
			cout << i << endl;
			REP(ii, child[i].size())
			{
				cout << child[i][ii] << " ";
			}
			cout << endl;
			cout << endl;
		}

		cout << endl << "End of test" << endl << endl;;

		*/
	}
}