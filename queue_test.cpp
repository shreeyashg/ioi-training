#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

int main()
{
	deque<ll> q;
	ll n = 4e7;
	srand(time(NULL));
	FOR(i, 1, n)
	{
		q.push_back(rand() % 1012101);
	}

	ll pre = 0;
	REP(i, q.size())
	{
		if(rand()%2)
			q.pop_front();
		else
			q.pop_back();

		if(rand() % 4 >= 2)
			q.push_back(rand()%233222);
		else
			q.push_back(rand()%22311);
		
		if(i/100000 != pre)
			cout << i << endl, pre = i/100000; 
	}

	ll cnt = 0;
	REP(i, q.size()) cnt += (q[i] % 3313);

	cout << cnt << endl;
}