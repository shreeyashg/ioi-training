#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll x, y, t;
string a, b;
map<ll, ll> ma, mb;
int main()
{
	cin >> x >> y >> a >> b >> t;

	if(t == 0)
	{
		reverse(a.begin(), a.end());
		cout << a << b << endl;
		exit(0);
	}

	reverse(a.begin(), a.end());
	string c = a + b;

	REP(i, a.size()) ma[a[i]]=1;
	REP(i, b.size()) mb[b[i]]=1;

	while(t--)
	{
		
		REP(i, c.size() - 1)
		{
			if(ma[c[i]] && mb[c[i + 1]])
			{
				swap(c[i], c[i + 1]);
				i++;
			}
		}

	//	cout << c << endl;
//		cout << xpos << " " << ypos << endl;


//		cout << endl;
	}

	/*
	
	4 4
	AAAA
	BBBB


	*/

	cout << c << endl;
}