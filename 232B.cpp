#include<iostream>
using namespace std;

const int MAXN = 110 + 5;

const long long MOD = 1e9 + 7;

long long dp[MAXN][MAXN * MAXN];
long long P[MAXN * MAXN] , C[MAXN][MAXN];

long long powlog(long long a , long long p)
{
    if(p == 0ll)
        return 1ll;

    long long ans = powlog(a , p / 2ll);
    ans = (ans * ans) % MOD;
    if(p % 2ll)
        ans = (ans * a) % MOD;
    return ans;
}
int main()
{
    long long n , m , k;
    cin >> n >> m >> k;

    for(int i = 0;i < MAXN;i ++)
        for(int j = 0;j <= i;j ++)
            if(i == j || j == 0)
                C[i][j] = 1;
            else
                C[i][j] = (C[i - 1][j] + C[i - 1][j - 1]) % MOD;

    dp[0][0] = 1;
    for(int i = 1;i <= min(n , m);i ++)
    {
        long long cnt = (m - i) / n + 1;

        for(int j = 0;j <= n;j ++)
            P[j] = powlog(C[n][j] , cnt);

        for(int j = 0;j <= k;j ++)
            for(int t = 0;(t <= j) && (t <= n);t ++)
                dp[i][j] = ((dp[i - 1][j - t] * P[t]) % MOD + dp[i][j]) % MOD;

    }
    cout << dp[min(n ,m)][k] << endl;
    return 0;
}