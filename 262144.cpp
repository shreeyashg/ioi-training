#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 262144 + 5, Mans = 65 + 3;

ll dp[Mans][MX], arr[MX], n;

int main()
{
	ifstream cin("262144.in");
	ofstream cout("262144.out");
	memset(dp, -1, sizeof(dp));

	cin >> n;
	REP(i, n) cin >> arr[i + 1];
	
	FOR(i, 1, n) dp[arr[i]][i] = i;

	// dp[i][j] = Minimum end index to make answer i starting from j.

	FOR(i, 1, Mans)
	{
		FOR(j, 1, n)
		{
			if(dp[i-1][j] != -1)
				dp[i][j] = dp[i-1][dp[i-1][j] + 1];
		}
	}

	ll f = 0;
	FOR(i, 1, 65)
	{
		FOR(j, 1, n)
		{
			if(dp[i][j] != -1)
				f = max(f, i);		}
	}

	cout << f << endl;
}