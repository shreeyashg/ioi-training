#include <bits/stdc++.h>
using namespace std;

typedef int ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, MX = 5e6 + 4, INF = 2e9 + 5;

ll n;
vector<ll> m, arr;
/*
bool f(ll x)
{
//	cout << x << endl;
	arr.push_back(0);

	arr.push_back(x); 
	arr.push_back(2 * m[1] - x);

	bool fl = 1;

	FOR(i, 1, n)
	{
		if(i != n) arr.push_back(arr[i] + 2 * (m[i + 1] - m[i]));
		if((arr[i] + arr[i + 1])/2 != m[i] || arr[i] > arr[i + 1] || (arr[i] + arr[i + 1])%2)
		{
			fl = 0;
			break;
		}
	}

	arr.clear();

	if(fl)
		return 1;
	return 0;
}
*/
int main()
{
	cin >> n;
	m.push_back(0);
	REP(i, 2)
	{
		ll x;
		cin >> x;
		m.push_back(x);
		//cin >> m[i + 1];
	}
//	cout << y -x + 1 << endl;
//	exit(0);

	if(n == 2)
		cout << m[2] - m[1] + 1 << endl, exit(0);


//	cout << cmin << " " << cmax << endl << endl << endl;
	ll mi = m[2], mim = m[2];
	ll cmax = m[2];
	ll cmin = m[1];
	FOR(i, 3, n)
	{
		cin >> mi;
		ll kmax = cmax;
		cmax = 2 * mim - cmin;
		cmin = 2 * mim - kmax;

	//	cout <<  i << endl;
	//	cout << cmin << " " << cmax << endl;
	//	cout << "-----------------------" << endl;


		cmax = min(cmax, mi);
		cmin = max(cmin, mim);

	//	cout <<  i << endl;
	//	cout << cmin << " " << cmax << endl;
	//	cout << "-----------------------" << endl;
	//	cout << endl << endl << endl;
		mim = mi;
	}

	cout << max(cmax - cmin + 1, 0) << endl;

}