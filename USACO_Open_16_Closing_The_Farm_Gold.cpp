#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 2e5 + 4;
ll n, m, arr[MX], sz[MX];
vector<ll> v[MX];
vector<ll> order;

ll finds(ll x)
{
	while(x != arr[x])
		x = arr[arr[x]];
	return x;
}

void uni(ll x, ll y)
{
	x = finds(x), y = finds(y);
//	cout << x << " " << y << endl;
	if(x == y)
		return;
	if(sz[y] < sz[x]) swap(y, x);
	arr[x] = arr[y], sz[y] += sz[x];
}

void print()
{
	return;
}

int main()
{
	ios::sync_with_stdio(false);
	cin.tie(0);
	cout.tie(0);
	ifstream cin("closing.in");
	ofstream cout("closing.out");
	cin >> n >> m;
	REP(i, m)
	{
		ll x, y;
		cin >> x >> y;
		v[x].push_back(y);
		v[y].push_back(x);
	}

	REP(i, n)
	{
		ll x; cin >> x;
		arr[i + 1] = i + 1;
		sz[i + 1] = 1;
		order.push_back(x);
	}

	reverse(order.begin(), order.end());
	deque<string> answers;
	set<ll> present;

	present.insert(order[0]);
	answers.push_front("YES");
	REP(i, order.size() - 1)
	{
		ll x = order[i + 1];
	//	DEBUG(x);
	//	print();
	//	ll x = 1;
		REP(i, v[x].size())
		{
			if(present.count(v[x][i]))
				uni(x, v[x][i]);
		}	
		present.insert(x);
		if(present.size() == sz[finds(x)])
			answers.push_front("YES");
		else
			answers.push_front("NO");
		print();
	}

	REP(i, answers.size())
	{
		cout << answers[i] << endl;
	}
}