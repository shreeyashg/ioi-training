#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n, k;
vector<ll> rown[MX], coln[MX], val;
vector<pair<ll, ll> > v;
map<pair<ll, ll>, ll> mp;
ll choice[MX], dp[MX][2];

ll rec(ll idx, bool x)
{
	// x == 1 implies last was to go down
//	cout << idx << " " << x << endl;

	if(idx == 1)
		return val[idx];

	if(dp[idx][x] != -1)
		return dp[idx][x];

	ll a = v[idx].first, b = v[idx].second;
	vector<ll> cpsuml;

	if(x == 1)
	{
		// go up
		auto it = lower_bound(coln[b].begin(), coln[b].end(), a);
		if(it == coln[b].begin())
		{
		// 	DEBUG("-INF");
			return -INF;
		}

		ll ans = -INF, cpsum = 0, ck = -1;
		FORD(i, it-coln[b].begin()-1, 0)
		{
			pair<ll, ll> cur = {coln[b][i], b};
			ll nidx = mp[cur];	
		//	cout << nidx << " " << idx << endl;
			

			ll pre = rec(nidx, !x);

			if(val[idx] + pre + ((idx == n) * k) < 0)
				continue;

			if(val[idx] + pre + cpsum >= ans)
			{
				ans = (pre + val[idx] + cpsum);
			}

			if(val[nidx] >= 0)
				cpsum += val[nidx];//, cpsuml.push_back(nidx);

		}

		return dp[idx][x] = ans;
	}

	if(x == 0)
	{
		// go left
		auto it = lower_bound(rown[a].begin(), rown[a].end(), b);
		if(it == rown[a].begin())
		{
			return -INF;
		}

		ll ans = -INF, cpsum = 0, ck = -1;
		FORD(i, it - rown[a].begin() - 1, 0)
		{
			pair<ll, ll> cur = {a, rown[a][i]};
			ll nidx = mp[cur];
		//	cout << nidx << " " << idx << endl;
			ll pre = rec(nidx, !x);
		//	cout << pre << endl;

			if((val[idx] + pre + ((idx == n) * k) < 0))
				continue;	

			if(val[idx] + pre + cpsum > ans)
			{
				ans = (pre + val[idx] + cpsum);
				ck = nidx;
			}

			if(val[nidx] >= 0)
				cpsum += val[nidx];

		}

		
		return dp[idx][x] = ans;	
	}
}

int main()
{
	memset(dp, -1, sizeof(dp));
	cin >> n >> k;
	v.push_back({0, 0});
	val.push_back(0);
	FOR(i, 1, n)
	{
		ll a, b, x;
		cin >> a >> b >> x;
		mp[{a, b}] = i;
		rown[a].push_back(b);
		coln[b].push_back(a);
		v.push_back({a, b});
		val.push_back(x - k);
	}

	FOR(i, 1, 1e5)
	{
		sort(rown[i].begin(), rown[i].end());
		sort(coln[i].begin(), coln[i].end());
	}

//	cout << rec(3, 0) << " " << rec(3, 1) << endl;
	cout << max(rec(n, 1), rec(n, 0)) + k << endl;
}