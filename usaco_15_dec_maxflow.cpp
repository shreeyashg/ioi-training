#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4, LN = 18;

ll pa[MX][LN], depth[MX], n, cnt, k, val[MX], sb[MX], mi[MX];
vector<ll> v[MX];

void build_LCA()
{
	REP(i, LN)
	{
		FOR(j, 1, n)
		{
			if(pa[j][i] != -1)
				pa[j][i + 1] = pa[pa[j][i]][i];
		}
	}
}

ll LCA(ll u, ll v)
{
	if(depth[u] < depth[v])
		swap(u, v);

	ll diff = depth[u] - depth[v];
//	DEBUG("diffed");
	REP(i, LN)
	{
	//	cout << u << " . " << (diff >> i) << " " << 1 << endl;
		if((diff >> i) & 1)
			u = pa[u][i];
	}	

	if(u != v)
	{
		FORD(i, LN - 1, 0)
		{
			if(pa[u][i] != pa[v][i])
			{
				u = pa[u][i];
				v = pa[v][i];
			}
		}
		u = pa[u][0];
	}

	return u;
}

void init(ll idx, ll prev)
{
// /	cout << idx << " " << prev << endl;
	pa[idx][0] = prev;
	depth[idx] = (prev != -1) ? depth[prev] + 1 : 0;
	REP(i, v[idx].size())
	{
		if(v[idx][i] != prev)
			init(v[idx][i], idx);
	}
}

void init2(ll idx ,ll par)
{
	sb[idx] = val[idx];

	REP(i, v[idx].size())
	{
		if(v[idx][i] != par) init2(v[idx][i], idx), sb[idx] += sb[v[idx][i]];
	}
}

int main()
{
	ifstream cin("maxflow.in");
	ofstream cout("maxflow.out");

	memset(pa, -1, sizeof(pa));
	memset(depth, 0, sizeof(depth));

	cin >> n >> k;
	REP(i, n - 1)
	{
		ll x, y;
		cin >> x >> y;
		v[x].push_back(y);
		v[y].push_back(x);
	}

	init(1, -1);
	build_LCA();
	REP(i, k)
	{
		ll x, y;
		cin >> x >> y;

		ll lca = LCA(x, y);


		val[x]++, val[y]++;
		val[lca]--;
		if(pa[lca][0] != -1)
			val[pa[lca][0]]--;
	}

	init2(1, 0);
	ll ans = 0;
	FOR(i, 1, n)
	{
	//	cout << i << " " << sb[i] << endl;
		ans = max(ans, sb[i]);
	}

	cout << ans << endl;
}