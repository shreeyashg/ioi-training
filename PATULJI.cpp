#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

vector<ll> v;
bool t =0;
ll up = 511;



void check(ll x)
{
//	cout << __builtin_popcount(x) << endl;
	if(x > up)
		return;
	if(__builtin_popcount(x) == 7)
	{
		ll ans = 0;
		bitset<9> f(x);
		REP(i, f.size())
		{
			if(f[i])ans += v[i];
		}

		if(ans == 100)
		{
			REP(i, f.size())
			{
				if(f[i])
					cout << v[i] << endl;
			}
			exit(0);
		}	
			
	}
	check(x + 1);
	
}

int main()
{
	REP(i,9)
	{
		ll k;
		cin >> k;
		v.push_back(k);
	}

	check(10);

}