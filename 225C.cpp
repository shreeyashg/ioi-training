#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e3 + 4;

char grid[MX][MX];
ll cost[2][MX], memo[MX][2][MX];
ll n, m, x, y;


ll rec(ll xp, ll color, ll cnt)
{
//	cout << xp << " " << color << " " << cnt << endl;
	if(xp == m)
	{
		if(cnt >= x && cnt <= y)
		{
			return cost[color][xp];
		}
		else
			return INF;
	}

	if(memo[xp][color][cnt] != -1)
		return memo[xp][color][cnt];

	if(cnt < x)
		return memo[xp][color][cnt] = cost[color][xp] + rec(xp + 1, color, cnt + 1);
	else if(cnt > y)
		return memo[xp][color][cnt] = INF;
	else
	{
		ll ans = INF;
		ans = min(ans, cost[color][xp] + rec(xp + 1, !color, 1));
		ans = min(ans, cost[color][xp] + rec(xp + 1, color, cnt + 1));

		return memo[xp][color][cnt] = ans;
	}

}

int main()
{
	cin >> n >> m >> x >> y;
	memset(memo, -1, sizeof(memo));
	REP(i, n)
	{
		REP(j, m)
		{
			char ch;
			cin >> grid[i + 1][j + 1];
		}
	}

	FOR(i, 1, m)
	{
		FOR(j, 1, n)
		{
			if(grid[j][i] != '.')
				cost[0][i]++;
			else
				cost[1][i]++;
		}
	}

	cout << min(rec(1, 0, 1), rec(1, 1, 1)) << endl;
}