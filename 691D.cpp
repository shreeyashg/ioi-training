#pragma GCC optimize("O3")
#include <bits/stdc++.h>
using namespace std;

typedef int ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, MX = 1e6 + 4;

vector<ll> v[MX];
vector<ll> ex;
ll n, arr[MX], m,  p[MX], cnt[MX];
bool ch[MX];

inline ll find(ll x) { return arr[x] = (x == arr[x]) ? x : find(arr[arr[x]]); }
inline void uni(ll x, ll y) { (x == y) ? : (rand() % 2) ? (arr[find(x)] = find(arr[y])) : (arr[find(y)] = find(arr[x])); }

int main()
{
	scanf("%d%d", &n, &m);
	FOR(i, 1, n) arr[i] = i;
	REP(i, n)
	{
		scanf("%d", &p[i + 1]);
	}

	REP(i, m)
	{
		ll x ,y;
		scanf("%d%d", &x, &y);
		uni(x, y);
	}

	FOR(i, 1, n)
	{
	    	ll g = find(i);
		if(!ch[g])
			ex.push_back(g);
		ch[g] = 1;
		v[g].push_back(p[i]);
	}

	REP(i, ex.size())
	{
		sort(v[ex[i]].begin(), v[ex[i]].end(), greater<int>());
	}


	FOR(i, 1, n)
	{
		ll g = arr[i];
		p[i]  = v[g][cnt[g]];
		cnt[g]++;
	}

	FOR(i, 1 ,n) printf("%d ", p[i]);
	printf("\n");
}
