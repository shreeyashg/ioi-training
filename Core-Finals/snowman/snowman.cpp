#include<bits/stdc++.h>
#define ii pair<int,int>
using namespace std;

map <int, int> mmap;
priority_queue <ii, vector<ii > > pq;
//vector <pair<int, ii > > v;

main() {

    freopen("input9.in", "r", stdin);
    freopen("output9.out", "w", stdout);

    int n;
    cin>>n;
    for (int i=1; i<=n; i++) {
        int x;
        cin>>x;
        if (mmap.find(x)==mmap.end()) mmap.insert(make_pair(x, 1));
        else mmap[x]++;
    }

    for (map<int, int>::iterator it=mmap.begin(); it!=mmap.end(); ++it) {
        //cout<<it->second<<" "<<it->first<<endl;
        pq.push(make_pair(it->second, it->first));
    }

    int ans=0;

    while (1) {
        if (pq.empty()) break;
        ii top = pq.top();
        //cout<<top.second<<endl;
        pq.pop();
        if (pq.empty()) break;
        ii top2 = pq.top();
        pq.pop();
        if (pq.empty()) break;
        ii top3 = pq.top();
        pq.pop();

        /*int x = top.second;
        int y = top2.second;
        int z = top3.second;

        if (y<z) swap(y,z);
        if (x<y) swap(x,y);
        if (y<z) swap(y,z);

        v.push_back(make_pair(x, make_pair(y,z)));*/

        ans++;

        if (top.first>1) pq.push(make_pair(top.first-1, top.second));
        if (top2.first>1) pq.push(make_pair(top2.first-1, top2.second));
        if (top3.first>1) pq.push(make_pair(top3.first-1, top3.second));
    }

    cout<<ans<<endl;

    //for (int i=0; i<ans; i++) cout<<v[i].first<<" "<<v[i].second.first<<" "<<v[i].second.second<<endl;

}
