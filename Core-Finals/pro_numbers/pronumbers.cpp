#include<bits/stdc++.h>
#define mod 1000000007
using namespace std;

main() {

    char input[20][20] = {"input1.in","input2.in","input3.in"};
    char output[20][20] = {"output1.out","output2.out","output3.out"};

    int inp=3;

    while (inp--) {

        freopen(input[inp], "r", stdin);
        freopen(output[inp], "w", stdout);
        int t;
        cin>>t;
        while (t--) {

            int n, x, y;
            cin>>n>>x>>y;
            if (n%6==1) cout<< (x + mod) % mod;
            if (n%6==2) cout<< (y + mod) % mod;
            if (n%6==3) cout<< (y-x + mod) % mod;
            if (n%6==4) cout<< (-x + mod) % mod;
            if (n%6==5) cout<< (-y + mod) % mod;
            if (n%6==0) cout<< (x-y + mod) % mod;
            cout<<endl;
        }

        fclose(stdin);
        fclose(stdout);
    }
}
