#include "bits/stdc++.h"
using namespace std;
const int N = 1e5 + 5;
int n , q;
int x , y , val;
int x1 , x2;
int ql , qr;
vector < pair < int , int > > v[N];
struct node{
    int sum;
    node* left;
    node* right;
    node(int _sum = 0 , node* _left = NULL , node* _right = NULL){
        sum = _sum;
        left = _left;
        right = _right;
    }
    node* insert(int l , int r , int idx , int val);
};
node* dummy;
node* root[N];
node* node::insert(int l , int r , int idx , int val){
    if(l == r){
        return new node(sum + val , dummy , dummy);
    }
    int mid = l + r >> 1;
    if(idx <= mid){
        return new node(sum + val , left -> insert(l , mid , idx , val) , right);
    }
    return new node(sum + val , left , right -> insert(mid + 1 , r , idx , val));
}
int query(node* lft , node* rgt , int l , int r , int ql , int qr){
    if(l > qr || r < ql){
        return 0;
    }
    if(l >= ql && r <= qr){
        return rgt -> sum - lft -> sum;
    }
    int mid = l + r >> 1;
    return query(lft -> left , rgt -> left , l , mid , ql , qr) + query(lft -> right , rgt -> right , mid + 1 , r , ql , qr);
}
int main(){
    scanf("%d %d" , &n , &q);
    for(int i = 1 ; i <= n ; ++i){
        scanf("%d %d %d" , &x , &y , &val);
        v[x].emplace_back(make_pair(y , val));
    }
    dummy = new node();
    dummy -> left = dummy -> right = dummy;
    root[0] = dummy;
    for(int i = 1 ; i < N ; ++i){
        root[i] = root[i - 1];
        for(auto it : v[i]){
            root[i] = root[i] -> insert(1 , N - 1 , it.first , it.second);
        }
    }
    while(q--){
        scanf("%d %d %d %d" , &x1 , &ql , &x2 , &qr);
        if(query(root[x1 - 1] , root[x2] , 1 , N - 1 , ql , qr) % 3 == 0){
            printf("YES\n");
        }
        else{
            printf("NO\n");
        }
    }
}