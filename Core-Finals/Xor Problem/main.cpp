#include "bits/stdc++.h"
using namespace std;
const int N = 1e5 + 5;
const int mod = 1e9 + 7;
int n;
int x = 0;
int inp;
int add(int a , int b){
    int res = a + b;
    if(res >= mod){
        return res - mod;
    }
    return res;
}
int mult(int a , int b){
    long long res = a;
    res *= b;
    if(res >= mod){
        return res % mod;
    }
    return res;
}
int power(int a , int b){
    int res = 1;
    while(b){
        if(b & 1){
            res = mult(res , a);
        }
        a = mult(a , a);
        b >>= 1;
    }
    return res;
}
int main(){
    cin >> n;
    for(int i = 1 ; i <= n ; ++i){
        cin >> inp;
        x ^= inp;
    }
    if(x == 0){
        cout << add(power(2 , n - 1) , mod - 1);
    }
    else{
        cout << 0;
    }
}