#include<bits/stdc++.h>
using namespace std;

int edges[100010];

main() {

    freopen("input6.in", "r", stdin);
    freopen("output6.out", "w", stdout);

    int n, m;
    cin>>n>>m;

    for (int i=1; i<=m; i++) {
        int x, y;
        cin>>x>>y;
        edges[x]++;
        edges[y]++;
    }

    int odd=0;
    for (int i=1; i<=n; i++) {
        if (edges[i]%2==1) odd++;
    }

    if (odd<=2) cout<<"YES"<<endl;
    else cout<<"NO"<<endl;
}
