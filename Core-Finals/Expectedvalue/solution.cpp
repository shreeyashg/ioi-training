#include "bits/stdc++.h"
using namespace std;
const int N = 1005;
const int K = 1005;
const int T = 105;
int t;
int n , k;
double mem[K][N];
double dp(int k , int d){
    if(d > k){
        return 0.0;
    }
    if(d < 1){
        return 0.0;
    }
    if(k == 1){
        return 1.0;
    }
    if(mem[k][d] > -1.0){
        return mem[k][d];
    }
    double res = 0.0;
    res += dp(k - 1 , d - 1) * (n - d + 1.0);
    res += dp(k - 1 , d) * d;
    res /= n;
    return mem[k][d] = res;
}
double solve(int n , int k){
    double res = 0;
    for(int i = 0 ; i <= k ; ++i){
        for(int j = 0 ; j <= n ; ++j){
            mem[i][j] = -2.0;
        }
    }
    for(int i = 1 ; i <= n ; ++i){
        res += dp(k , i) * i;
    }
    return res;
}
int main(){
    cin >> t;
    while(t--){
        cin >> n >> k;
        printf("%.2lf\n" , solve(n , k));
    }
}
