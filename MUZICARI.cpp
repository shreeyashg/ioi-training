#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 5e2 + 4;

ll t, n, dp[5004][MX], ans[MX];
vector<pair<ll, ll> >v, vv;


int main()
{
	cin >> t >> n;
	ll sum = 0;
	REP(i, n)
	{
		ll a, b;
		cin >> b;
		sum += b;
		a = i + 1;
		v.push_back({b, a});
	}

	vv = v;

	v.push_back({-1000, 0});
	sort(v.begin(), v.end());
	
	FOR(i, 0, n)
	{
		dp[0][i] = 1;
	}	

	FOR(i, 1, n)
	{
		FOR(j, 1, t)
		{
			if(j - v[i].first >= 0)
			{
				if(dp[j-v[i].first][i-1])
				{

					dp[j][i] = 1;
					continue;
				}

				else if(dp[j][i-1])
				{
					dp[j][i] = 1;
					continue;
				}

				else
					dp[j][i] = 0;
			}
		}
	}

	ll tar = 0;
	FORD(i, t, 1)
	{
		if(dp[i][n])
		{
			tar = i;
			break;
		}
	}

	ll currn = n;
	vector<ll> chosen, not_chosen;
	while(true)
	{
		if(currn == 0)
			break;
		if(tar-v[currn].first >= 0 && dp[tar-v[currn].first][currn-1])
			chosen.push_back(v[currn].second), tar-=v[currn].first;
		else
			not_chosen.push_back(v[currn].second);
		currn--;
	}
	ll c = 0, nc = 0;
	REP(i, chosen.size())
	{
		ans[chosen[i]] = c;
		c += vv[chosen[i]-1].first;
	}
	cout << endl;

	REP(i, not_chosen.size())
	{
		ans[not_chosen[i]] = nc;
		nc += vv[not_chosen[i]-1].first;
	}

	FOR(i, 1, n) cout << ans[i] << " ";
	cout << endl;
}