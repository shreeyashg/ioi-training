#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n, m, k, x, vis[MX], dp[MX][11][3];
vector<ll> v[MX], child[MX];

void dfs(ll idx)
{

	REP(i, child[idx].size())
	{
		dfs(child[idx][i]);
	}

	if(child[idx].size() == 0)
	{
		
		
		dp[idx][1][1] = 1;
		dp[idx][0][2] = m-k;
		dp[idx][0][0] = k-1;
		

		dp[idx][0][1] = 0;
		
		// Look good? Ya
	}

	vector<vector<vector<ll> > > c(child[idx].size()+1, vector< vector<ll> >(x+4, vector<ll>(3))); // Equivalent to c[child[idx].size()][x+1][3];
    

	FOR(i, 0, x)
	{
	    FOR(p, 0, 2)
	    {
	        if(p == 0)
	        {
	            c[0][0][0] = (k-1);
	        }
	        
	        if(p == 1)
	        {
	            c[0][1][1] = 1;
	        }
	        
	        if(p == 2)
	        {
	            c[0][0][2] = (m-k);
	        }
	        
	        
	    }
	}
	
	REP(i, child[idx].size())
	{
		FOR(j, 0, x)
		{
			FOR(k, 0, j)
			{
				FOR(p, 0, 2)
				{
				    
				    if(p == 0)
				    {
				        c[i+1][j][0] += (c[i][j-k][0]%MOD * (dp[child[idx][i]][k][1]%MOD + dp[child[idx][i]][k][2]%MOD + dp[child[idx][i]][k][0]%MOD))%MOD;
				    }
				    if(p == 1)
				    {
				        c[i+1][j][1] += (c[i][j-k][1]%MOD * (dp[child[idx][i]][k][0])%MOD)%MOD;
				    }
				    if(p == 2)
				    {
				        c[i+1][j][2] += (c[i][j-k][2]%MOD * (dp[child[idx][i]][k][0]%MOD + dp[child[idx][i]][k][2]%MOD))%MOD;
				    }
				}
			}
		}
	}


	if(child[idx].size() != 0)
	{
		FOR(i, 0, x)
		{
			FOR(p, 0, 2)
			{
				dp[idx][i][p] = c[child[idx].size()][i][p];
			}
		}
	}

}


void init(ll idx)
{
	if(vis[idx])
		return;
	vis[idx] = 1;

	REP(i, v[idx].size())
	{
		if(!vis[v[idx][i]])
		{
			init(v[idx][i]);
			child[idx].push_back(v[idx][i]);
		}
	}
}

int main()
{
	cin >> n >> m;
	REP(i, n-1)
	{
		ll x, y;
		cin >> x >> y;
		v[x].push_back(y);
		v[y].push_back(x);
	}

	cin >> k >> x;
	init(1);
	dfs(1);

    ll ans = 0;

	FOR(i, 0, x)
	{
	    FOR(p, 0, 2)
	    {
	        ans = (ans%MOD + dp[1][i][p]%MOD)%MOD;
	    }
	}
	
	cout << ans%MOD << endl;
}