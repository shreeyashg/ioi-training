#include<iostream>
#include<cstdio>
#include<vector>
#include<utility>
#include<algorithm>
using namespace std;
int A,B;
vector<pair<int,int> > prv[1001][1001];
int sol[1001][1001];
char SA[1001],SB[1001];
char minnext[1001][1001];
vector<pair<int,int> > minn[1001][1001];
bool visited[1001][1001];
vector<pair<int,int> > valids[2001];
bool added[1001][1001];
void setn(int i,int j)
{
	if(visited[i][j])
	{
		return;
	}
	visited[i][j]=1;
	for(pair<int,int> k:prv[i][j])
	{
		if(k.first==i-1)
		{
			if(SA[i]<=minnext[k.first][k.second])
			{
				if(SA[i]<minnext[k.first][k.second])
				{
					minnext[k.first][k.second]=SA[i];
					minn[k.first][k.second].clear();
				}
				minn[k.first][k.second].push_back(make_pair(i,j));
			}
		}
		else
		{
			if(SB[j]<=minnext[k.first][k.second])
			{
				if(SB[j]<minnext[k.first][k.second])
				{
					minnext[k.first][k.second]=SB[j];
					minn[k.first][k.second].clear();
				}
				minn[k.first][k.second].push_back(make_pair(i,j));
			}
		}
		setn(k.first,k.second);
	}
}
int main()
{
	cin>>A>>B;
	for(int i=1;i<=A;i++)
	{
		scanf(" %c",&SA[i]);
	}
	for(int i=1;i<=B;i++)
	{
		scanf(" %c",&SB[i]);
	}
	for(int i=0;i<=A;i++)
	{
		for(int j=0;j<=B;j++)
		{
			if(i==0&&j==0)
			{
				continue;
			}
			sol[i][j]=1000000;
			if(j>0)
			{
				sol[i][j]=sol[i][j-1]+1;
				prv[i][j].push_back(make_pair(i,j-1));			
			}
			if(i>0&&sol[i-1][j]+1<=sol[i][j])
			{
				if(sol[i-1][j]+1<sol[i][j])
				{
					prv[i][j].clear();
				}
				sol[i][j]=sol[i-1][j]+1;
				prv[i][j].push_back(make_pair(i-1,j));
			}
			if(i>0&&j>0&&SA[i]==SB[j]&&sol[i][j]>=sol[i-1][j-1]+1)
			{
				if(sol[i-1][j-1]+1<sol[i][j])
				{
					prv[i][j].clear();
				}
				sol[i][j]=sol[i-1][j-1]+1;
				prv[i][j].push_back(make_pair(i-1,j-1));
			}
		}
	}
	for(int i=0;i<=A;i++)
	{
		for(int j=0;j<=B;j++)
		{
			minnext[i][j]='Z';
		}
	}
	setn(A,B);
	valids[0].push_back(make_pair(0,0));
	for(int i=1;i<=sol[A][B];i++)
	{
		char curmin='Z';
		for(pair<int,int> j:valids[i-1])
		{
			curmin=min(curmin,minnext[j.first][j.second]);
		}
		cout<<curmin;
		for(pair<int,int> j:valids[i-1])
		{
			if(minnext[j.first][j.second]==curmin)
			{
				for(pair<int,int> k:minn[j.first][j.second])
				{
					if(!added[k.first][k.second])
					{
						added[k.first][k.second]=1;
						valids[i].push_back(k);
					}
				}
			}
		}		
	}
	cout<<endl;
	return 0;
}
