#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll sa, sb;
string a, b;

pair<ll, ll> choice[1001][1001];
char chs[1001][1001];

ll dp[1001][1001];

ll f(ll x, ll y)
{
	if(x == sa && y == sb)
		return 0;
	
	if(dp[x][y] != -1)
		return dp[x][y];

	ll ans = INF, ch = 'Z';
//	choice[x][y] = {-1, -1};
	if(x == sa)
	{
		// increment y
		if(1+f(x, y+1) < ans || (1+f(x, y+1) == ans && b[y] < ch))
			ans = 1+f(x, y+1), ch = b[y], choice[x][y] = {x, y+1};

	}
	else if(y == sb)
	{
		// increment x
		if(1+f(x+1, y) < ans || (1+f(x+1, y) == ans && a[x] < ch))
			ans = 1+f(x+1, y), ch = a[x], choice[x][y] = {x+1, y};

	}
	else
	{
		// try both
		if(a[x] == b[y])
		{	
			if(1+f(x+1, y+1) < ans || (1+f(x+1, y+1) == ans && b[y] < ch))
				ans = 1+f(x+1, y+1), ch = b[y], choice[x][y] = {x+1, y+1};
		}
		
		if(1+f(x, y+1) < ans || (1+f(x, y+1) == ans && b[y] < ch))
			ans = 1+f(x, y+1), ch = b[y], choice[x][y] = {x, y+1};

		if(1+f(x+1, y) < ans || (1+f(x+1, y) == ans && a[x] < ch))
			ans = 1+f(x+1, y), ch = a[x], choice[x][y] = {x+1, y};

	}

	chs[x][y] = ch;
	return dp[x][y] = ans;
}

int main()
{
	memset(dp, -1, sizeof(dp));
	cin >> sa >> sb;
	cin >> a >> b;

	f(0, 0);// << endl;

	ll x = 0, y = 0;
	while(true)
	{
		if(x == -1)
			assert(false);
		if(x == sa && y == sb)
			break;
		cout << chs[x][y];
		ll nx = choice[x][y].first, ny = choice[x][y].second;
		x = nx, y = ny;
	}
	cout << endl;
	//DEBUG()		
}