#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e4 + 4;

ll n, k;
set<ll> s;
vector<ll> v;
ll dp[MX][55];
map<ll, ll> mp;

struct node
{
	ll from, to, val, lazy;
	node *left, *right;
	node()
	{
		from = 1;
		to = 1e9 + 7;
		val = 0, lazy = 0, left = NULL, right = NULL;
	}

	void extend()
	{
		if(left == NULL)
		{
			left = new node(), right = new node();
			left->from = from;
			left->to = (from + to) >> 1;
			right->from = ((from + to) >> 1) + 1;
			right->to = to;
		}
	}
};

void update(node *curr, ll l, ll r, ll val)
{
	if(l > r)
		return;
	if(curr->lazy)
	{
		curr->val = (curr->to - curr->from + 1) * curr->lazy;
		if(curr->from != curr->to)
		{
			curr->extend();
			curr->left->lazy += curr->lazy;
			curr->right->lazy += curr->lazy;
		}
		curr->lazy = 0;
	}

	if((curr->from) > r || (curr->to) < l || (curr->from) > (curr->to))
		return;
	if(curr->from >= l && curr->to <= r)
	{
		curr->val += (curr->to - curr->from + 1) * val;
		if(curr->from != curr->to)
		{
			curr->extend();
			curr->left->val += val;
			curr->right->val += val;
		}
		return;
	}
	curr->extend();
	update(curr->left, l, r, val);
	update(curr->right, l, r, val);
	curr->val = curr->left->val + curr->right->val;
}

ll query(node *curr, ll l, ll r)
{
	if(curr->from > curr->to || curr->from > r || curr->to < l)
		return 0;
	if(curr->lazy)
	{
		curr->val += (curr->to - curr->from + 1)*curr->lazy;
		curr->extend();
		curr->left->lazy += curr->lazy;
		curr->right->lazy += curr->lazy;
		curr->lazy = 0;
	}

	if(curr->from >= l && curr->to <= r)
		return curr->val;

	ll q1, q2;
	q1 = query(curr->left, l, r), q2 = query(curr->right, l, r);
	return q1 + q2;
}
node *root = new node();

int main()
{
	cin >> n >> k;
	v.push_back(0);
	REP(i, n)
	{
		ll x;
		cin >> x;
		if(!s.count(x))	
			v.push_back(x), s.insert(x);
	}

	FOR(i, 1, n)
	{
		dp[i][1] = 1;
	}

	FOR(i, 1, v.size())
	{
		mp[v[i]] = i;
	}

//	cout << v.size() << endl;
	// DP[i][j] = Number of increasing subsequences of length j that end at element i.



	FOR(kk, 2, k)
	{
		cout << kk << endl;
		FORD(i, n, n - kk + 1)
		{
			update(root, v[i], v[i], 1);
		}
		DEBUG("UPDATED");
		FORD(i, n - kk, 1)
		{
			dp[i][kk] = dp[i][kk - 1] + query(root, v[i] + 1, 1e9);
			update(root, v[i], v[i], 1);
		}
		DEBUG("QUERIED");
	}

	ll ans = 0;
	FOR(i, 1, n)
	{
		ans += dp[i][k];
	}

	cout << ans << endl;
}