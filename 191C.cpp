#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll pa[MX][LN], depth[MX], n, cnt, m;
vector<ll> v[MX];

void build_LCA()
{
	REP(i, LN)
	{
		FOR(j, 1, n)
		{
			if(pa[j][i] != -1)
				pa[j][i + 1] = pa[pa[j][i]][i];
		}
	}
}

ll LCA(ll u, ll v)
{
	if(depth[u] < depth[v])
		swap(u, v);

	ll diff = depth[u] - depth[v];
//	DEBUG("diffed");
	REP(i, LN)
	{
	//	cout << u << " . " << (diff >> i) << " " << 1 << endl;
		if((diff >> i) & 1)
			u = pa[u][i];
	}	

//	cout << depth[u] << " " << depth[v] << endl;
//	DEBUG("same leveled");
	if(u != v)
	{
		FORD(i, LN - 1, 0)
		{
			if(pa[u][i] != pa[v][i])
			{
				u = pa[u][i];
				v = pa[v][i];
			}
		}
		u = pa[u][0];
	}
	return u;
}

void init(ll idx, ll prev)
{
	pa[idx][0] = prev;
	depth[idx] = (prev != -1) ? depth[prev] + 1 : 0;

	REP(i, v[idx].size())
	{
		if(v[idx][i] != idx)
			init(v[idx][i], idx);
	}
}

int main()
{
	cin >> n;
	REP(i, n - 1)
	{
		ll x, y;
		cin >> x >> y;
		v[x].push_back(y);
		v[y].push_back(x);
	}

	init(1, -1);
	build_LCA();

	cin >> m;
	REP(i, m)
	{
		ll x, y;
		cin >> x >> y;

		
	}
}