#pragma GCC optimize("O3")
#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4, VX = 1e6 + 4;

struct node
{
	ll from, to, val, lazy;
	node *left, *right;
	node()
	{
		from = 1;
		to = 1e9;
		val = 0, lazy = 0, left = NULL, right = NULL;
	}

	void extend()
	{
		if(left == NULL)
		{
			left = new node(), right = new node();
			left->from = from;
			left->to = (from + to) >> 1;
			right->from = ((from + to) >> 1) + 1;
			right->to = to;
		}
	}
};

node *root;

void update(node *curr, ll l, ll r, ll val)
{
	if(curr->lazy)
	{
		curr->val = (curr->to - curr->from + 1) * curr->lazy;
		if(curr->from != curr->to)
		{
			curr->extend();
			curr->left->lazy += curr->lazy;
			curr->right->lazy += curr->lazy;
		}
		curr->lazy = 0;
	}

	if((curr->from) > r || (curr->to) < l || (curr->from) > (curr->to))
		return;
	if(curr->from >= l && curr->to <= r)
	{
		curr->val += (curr->to - curr->from + 1) * val;
		if(curr->from != curr->to)
		{
			curr->extend();
			curr->left->val += val;
			curr->right->val += val;
		}
		return;
	}
	curr->extend();
	update(curr->left, l, r, val);
	update(curr->right, l, r, val);
	curr->val = curr->left->val + curr->right->val;
}

ll query(node *curr, ll l, ll r)
{
	if(curr->from > curr->to || curr->from > r || curr->to < l)
		return 0;
	if(curr->lazy)
	{
		curr->val += (curr->to - curr->from + 1)*curr->lazy;
		curr->extend();
		curr->left->lazy += curr->lazy;
		curr->right->lazy += curr->lazy;
		curr->lazy = 0;
	}

	if(curr->from >= l && curr->to <= r)
		return curr->val;

	ll q1, q2;
	q1 = query(curr->left, l, r), q2 = query(curr->right, l, r);
	return q1 + q2;
}

ll arr[MX], last[VX], res[VX];

int main() {
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);
  //freopen("test.txt","r",stdin);
  //freopen("taskname.in","r",stdin);
  //freopen("taskname.out","w",stdout);
  memset(last, -1, sizeof(last));
  ll type,p,q;
  long long v;

  root=new node();
  ll n; cin >> n;
  FOR(i, 1, n) cin >> arr[i];

  vector<pair<pair<ll, ll>, ll> > queries;
  ll qq; cin >> qq;
  REP(i ,qq)
  {

  	ll x, y;
  	cin >> x >> y;
  	queries.push_back({{y, x}, i});
  }

  sort(queries.begin(), queries.end());
  REP(i, queries.size()) swap(queries[i].first.first, queries[i].first.second);
  ll curr = 0, currcnt = 0;
  /*  while(curr != queries[0].first.second)
  {
  	curr++;
  }
  FORD(i, curr, 1)
  {
  	if(!mp[arr[i]])
  		update_tree(root, i, i, ++currcnt), mp[arr[i]] = 1;
  	else
  		update_tree(root, i, i, currcnt);

  }
  FOR(i, 1, n) cout << query_tree(root, i, i) << " ";
  cout << endl;
*/
  // Check if last occurrence of any number is between a and b for each query 

  FOR(i, 1, queries[0].first.second)
  {
  	if(last[arr[i]] == -1) last[arr[i]] = i, update(root, last[arr[i]], last[arr[i]], +1);
  	else update(root, last[arr[i]], last[arr[i]], -1), last[arr[i]] = i, update(root, last[arr[i]], last[arr[i]], +1);
  } 
  curr = queries[0].first.second;
  REP(i, queries.size())
  {
  // 	FOR(ii, 1, n) cout << query_tree(root, ii, ii) << " ";
  //	cout << "curr = " << curr << endl;
  	while(curr != queries[i].first.second)
  	{
  		curr++;
  		if(last[arr[curr]] == -1) last[arr[curr]] = curr, update(root, last[arr[curr]], last[arr[curr]], +1);
  		else update(root, last[arr[curr]], last[arr[curr]], -1), last[arr[curr]] = curr, update(root, last[arr[curr]], last[arr[curr]], +1);
  	//	FOR(ii, 1, n) cout << query_tree(root, ii, ii) << " ";
  	//	cout << "curr = " << curr << endl;
    	}

  	res[queries[i].second] = query(root, queries[i].first.first, queries[i].first.second);
  //	cout << queries[i].first.first << " " << queries[i].first.second <<" " << res[queries[i].second] << endl;
  	
  }

  REP(i, queries.size()) cout << res[i] << endl;
  /*
  while(tests--) {
    
    scanf("%lld %lld", &n, &qs);
    for(i=1;i<=qs;i++) {
      scanf("%lld", &type);
      if(type==0) {
        scanf("%lld %lld %lld", &p, &q, &v);
        if(p>q) swap(p,q);	
        update_tree(root,p,q,v);
      } 
      else {
        scanf("%lld %lld", &p, &q);
        if(p>q) swap(p,q);
        prllf("%lld\n", query_tree(root,p,q));
      }
    }
  }
  
  return 0;*/
}