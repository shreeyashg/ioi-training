#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll r, c;
vector<string> grid, tex;
bool vis[52][52]; 

inline bool good(ll xx , ll yy)
{
	char x = grid[xx][yy];
	if((x == '.' || x == 'D') && !vis[xx][yy])
		return true;
	return false;
}

inline bool good2(char x)
{
	if(x != 'D' && x != 'X')
		return true;
//	char x = grid[xx][yy];
	return false;
}

int main()
{
	cin >> r >> c;
	grid.resize(r + 1);
	string cc = "";
	REP(i, c) cc += '.';
	grid[0] = cc;
	REP(i, r)
	{
		string x = "", y = ".";
		cin >> x;
		y += x;
		grid[i + 1] = y;
	}

	pair<ll, ll> start = {-1, -1}, end = {-1, -1};
	FOR(i, 1, r)
	{
		FOR(j, 1, c)
		{
			if(grid[i][j] == 'S')
			{
				start = {i, j};
				grid[i][j] = '.';
			}
			if(grid[i][j] == 'D')
				end = {i, j};
		}
	}

	queue<pair<pair<ll, ll>, ll> > q;
	ll curr = 0;
	tex = grid;
	q.push({{start.first, start.second}, 0});
	set<ll> s;
	while(!q.empty())
	{
		auto t = q.front();
		q.pop();
	//	cout << t.first.first << " " << t.first.second << " " << t.second << endl;

		if(vis[t.first.first][t.first.second]) continue;
		vis[t.first.first][t.first.second] = 1;
		
		ll found = 0;
		if(t.second != 0 && !s.count(t.second))
		{
			s.insert(t.second);
			FOR(i, 1, r)
			{
				FOR(j, 1, c)
				{
					if(grid[i][j] == '*')
					{
						if(i + 1 <= r && good2(grid[i + 1][j])) tex[i + 1][j] = '*';
						if(i - 1 >= 1 && good2(grid[i - 1][j])) tex[i-1][j] = '*';
						if(j+1 <= c && good2(grid[i][j+1])) tex[i][j + 1] = '*';
						if(j-1>=1&&good2(grid[i][j-1]))tex[i][j-1] = '*';

				/*		if(i + 1 <= r && grid[i + 1][j] == 'D') found = 1;
						if(i - 1 >= 1 && grid[i-1][j] == 'D') found = 1;
						if(j+1 <= c && grid[i][j+1] == 'D') found = 1;
						if(j-1>=1&&grid[i][j-1]=='D') found = 1; */
					}
				}
			}
		}

		if(tex[t.first.first][t.first.second] == '*')
			continue;

		if(t.first == end)
			cout << t.second << endl, exit(0);
		grid = tex;

	

		


		if(!found)
		{
			if(t.first.second + 1 <= c && good(t.first.first, t.first.second + 1))q.push({{t.first.first, t.first.second + 1}, t.second + 1});
			if(t.first.second - 1 >= 1 && good(t.first.first, t.first.second - 1))q.push({{t.first.first, t.first.second - 1}, t.second + 1});
			if(t.first.first + 1 <= r && good(t.first.first + 1, t.first.second))q.push({{t.first.first + 1, t.first.second}, t.second + 1});
			if(t.first.first - 1 >= 1 && good(t.first.first - 1, t.first.second))q.push({{t.first.first - 1, t.first.second}, t.second + 1});
		}
		
	}
	cout << "KAKTUS" << endl;
}