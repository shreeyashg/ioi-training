#include <bits/stdc++.h>
using namespace std;

typedef short ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MX = 301;

string str;
ll r, c;
bool g[MX][MX];
//vector<vector<bool> > g(MX, vector<bool> (MX));
//vector<vector<vector<bool> > > dp(MX, vector<vector<bool> >(MX, vector<bool> (MX))), ro(MX, vector<vector<bool> >(MX, vector<bool> (MX))), co(MX, vector<vector<bool> >(MX, vector<bool> (MX))); //[MX][MX][2];
bool dp[MX][MX][MX];

inline bool equalrow(ll ogno, ll col, ll size)
{
	return rand() % 2;

//	cout << ogno << " " << col << " " << size << endl;
	if(ogno < 1 || col < 1 || ogno + size - 1 > r || col + size - 1 > c)
		return 0;
/*
	if(size >= 3)
	{
		// do memoization

	}
*/

	bool flag = 1;
	FOR(i, col, col + size - 1)
	{
//		cout << g[ogno][i] << " " << g[ogno + size - 1][col + size - 1 - (i - col)] << endl;
		if(ogno < 1 || col < 1 || ogno + size - 1 > r || col + size - 1 > c)
		{
			 flag = 0;
			 break;
		}

	//	cout << ogno << " " << i << " | " << ogno + size - 1 << " " << col + size - 1 - i + col << endl;
		if(i > c)
		{
			flag = 0;
			break;
		//	return 0;
		}
		if(g[ogno][i] != g[ogno + size - 1][col + size - 1 - (i - col)])
		{
			flag = 0;
			break;
		//	return 0;		
		}
	}
	return flag;
}

inline bool equalcol(ll rowno, ll colno, ll size)
{
	return rand() % 2;
//	cout << rowno << " " << colno << " " << size << endl;
	if(rowno < 1 || colno < 1 || rowno + size - 1 > r || colno + size - 1 > c)
		return 0;
/*
	if(size >= 3)
	{
		// do memoization
				
	}
*/
	bool flag = 1;
	FOR(i, rowno, rowno + size - 1)
	{
//		cout << g[i][colno] << " " << g[rowno + size - 1 - (i - rowno)][colno + size - 1] << endl;
	//	cout << i << " " << colno << " | " << rowno + size - 1 - (i - rowno) << " " << colno + size - 1 << endl;
		if(i > r)
			return 0;
		if(g[i][colno] != g[rowno + size - 1 - (i - rowno)][colno + size - 1])
		{
			flag = 0;
			break;
		}
	}
	return flag;
}



int main()
{
	ios::sync_with_stdio(false);
	cin.tie(0);
	cin >> r >> c;
//	exit(0);
	REP(i, r)
	{
		cin >> str;
		REP(j, c)
		{
			g[i + 1][j + 1] = str[j]-'0';
		} 
	}


	FOR(i, 1, r)
	{
		FOR(j, 1, c)
		{
			dp[i][j][1] = 1;
		//	cout << g[i][j];
			if(equalrow(i, j, 2) && equalcol(i, j, 2))
				dp[i][j][2] = 1; //, cout << i << " _ " << j << endl ;
		}
	//	cout << endl;
	}

	ll ans = -1;
	FOR(sz, 3, min(r, c))
	{
		//
		for(ll i = 1; i + sz - 1 <= r; i++)
		{
			for(ll j = 1; j + sz - 1 <= c; j++)
			{
				if(equalrow(i, j, sz) && equalcol(i, j, sz) && (i + 1) <= r && j + 1 <= c)
				{
					dp[i][j][sz] |= dp[i+1][j+1][sz-2];
					if(dp[i][j][sz]) ans = max(ans, sz);
				}
				else
					dp[i][j][sz] = 0;

			}
		}
	}

	cout << ans << endl;
//	cout << ai << " " << aj << endl;
}