#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n, m, fx, fy, bx, by;
string f, b;
vector<pair<ll, ll> > farmer, boi;
ll memo[1005][1005];

pair<ll, ll> ts(char ch)
{
	switch(ch)
	{
		case 'W': return {-1, 0};
		case 'N': return {0, 1};
		case 'E': return {1, 0};
		default: return {0, -1};
	}
	return {0, 0};
}

ll dist(pair<ll, ll> a, pair<ll, ll> b)
{
	return (a.first - b.first) * (a.first - b.first) + (a.second - b.second) * (a.second - b.second);
}

pair<ll, ll> add(pair<ll, ll> a, pair<ll, ll> b)
{
	return {a.first + b.first, a.second + b.second};
}

ll rec(ll x, ll y)
{
	if(x == n && y == m)
		return 0;

	if(memo[x][y] != -1)
		return memo[x][y];

	ll ans = INF;


	if(x < n && y < m)ans = min(ans, dist(farmer[x + 1], boi[y + 1]) + rec(x + 1, y + 1));
	if(y < m)ans = min(ans, dist(farmer[x], boi[y + 1]) + rec(x, y + 1));
	if(x < n)ans = min(ans, dist(farmer[x + 1], boi[y]) + rec(x + 1, y));

	return memo[x][y] = ans;
}

int main()
{
	ifstream cin("radio.in");
	ofstream cout("radio.out");

	REP(i, 1005) REP(j, 1005) memo[i][j] = -1;
	cin >> n >> m;
	cin >> fx >> fy >> bx >> by;
	cin >> f >> b;

	pair<ll, ll> F = {fx, fy}; 
	pair<ll, ll> B = {bx, by};
	farmer.push_back(F);
	REP(i, f.size() + 1)
	{
		if(i == 0)
			continue;
		farmer.push_back(add(ts(f[i - 1]), farmer[farmer.size() - 1]));

	//	cout << farmer[i].first << " " << farmer[i].second << endl;
	}

	boi.push_back(B);
	REP(i, b.size() + 1)
	{
		if(i == 0)
			continue;
		boi.push_back(add(ts(b[i - 1]), boi[boi.size() - 1]));
	//	cout << boi[i].first << " " << boi[i].second << endl;
	}

	cout << rec(0, 0) << endl;
}