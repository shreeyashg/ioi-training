#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9, INF = 1e11, MX = 1e4 + 4;

ll m, n, start, k, cc, vis[MX], cost[MX], spc[MX], comp[MX], memo[MX], bad[MX], prob, flag;
vector<ll> v[MX], rev[MX], adj[MX];
stack<ll> s;
vector<ll> vec;
vector<vector<ll> > components;
set<ll> ss[MX];
bool sp[MX];

void DFS(ll idx)
{
	if(vis[idx])
		return;
	vis[idx] = 1;
	REP(i, v[idx].size())
	{
		DFS(v[idx][i]);
	}
	s.push(idx);
}

void DFS2(ll idx)
{
	//cout << idx << endl;
	if(vis[idx])
		return;
	vis[idx] = 1;
	vec.push_back(idx);
	REP(i, rev[idx].size())
	{
		DFS2(rev[idx][i]);
	}
}

ll dp(ll x)
{
	if(x == comp[2])
		return memo[comp[2]] = 1;
	if(memo[x] != -1)
		return memo[x] % MOD;

	ll ans = 0;
	REP(i, adj[x].size())
	{
		if(ans + dp(adj[x][i]) >= MOD) flag = 1;
		ans = (ans + dp(adj[x][i])) % MOD;
	}

	return memo[x] = ans % MOD;
}

int main()
{
	cin >> n >> m;
	REP(i, m)
	{
		ll x, y;
		cin >> x >> y;
		v[x].push_back(y);
		rev[y].push_back(x);
	}

	for(ll i = 1; i <= n; i ++)
	{
		if(!vis[i])
			DFS(i);
	//	cout << s.size() << endl;
	}

	memset(vis, 0, sizeof(vis));
	while(!s.empty())
	{
		cc ++;
		ll x = s.top();
		s.pop();
		if(!vis[x])
		{
		//	memset(vis, 0, sizeof(vis));
			DFS2(x);
			components.push_back(vec);
			REP(i, vec.size())
			{
				comp[vec[i]] = components.size() - 1;
			}
		}
		vec.clear();

	}


	FOR(i, 1, n)
	{
		REP(j, v[i].size())
		{
			if(comp[i] != comp[v[i][j]] )
				adj[comp[i]].push_back(comp[v[i][j]]);
		}
	}


	REP(i, components.size())
	{
		if(components[i].size() > 1)
			bad[i] = 1;
	}

	memset(vis, 0, sizeof(vis));
/*
	REP(i, components.size())
	{
		cout << "inside" << endl;
		REP(j, components[i].size())
		{
			cout << components[i][j] <<" " ;
		}
		cout << endl;
		cout << "i = " << i << endl;
		REP(j, adj[i].size())
		{
			cout << adj[i][j] << " ";
		}
		cout << endl;
		cout << endl;
	}
*/

	memset(memo, -1, sizeof(memo));

	ll k = dp(comp[1]);
//	cout << k % MOD << endl;
	if(flag)
	{
		string str = to_string(k % MOD);
		while(str.size() < 9)
		{
			str.insert(0, "0");
		}
		cout << str << endl;
	}
	else cout << k << endl;
}