#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll sum(ll x)
{
	ll dig = 0;
	string str = to_string(x);
	REP(i, str.size())
	{
		dig += str[i] - '0';
	}
	return dig;
}

int main()
{
	ll x;
	cin >> x;
	ll asf = 0;
	FOR(i, 1, x) cout << i << " " << sum(i) << endl, asf += sum(i);
	cout << asf << endl; 
}