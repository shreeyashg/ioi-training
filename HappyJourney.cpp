#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n, m;
ll s, t, dists[MX], distt[MX];
bool vis[MX];

vector<pair<char, ll> > v[MX];

int main()
{
	cin >> n >> m >> s >> t;
	REP(i, m)
	{
		ll x, y;
		char ch;
		cin >> x >> ch >> y;
		v[x].push_back({ch, y});
		v[y].push_back({ch, x});
	}

	FOR(i, 1, n)
	{
		sort(v[i].begin(), v[i].end());
	}

	queue<ll> q;
	q.push(s);
	dists[s] = 0;
	while(!q.empty())
	{
		auto tt = q.front();
		

		q.pop();
		if(!vis[tt])
			vis[tt]=1;
		else
			continue;

		REP(i, v[tt].size())
		{
			if(!vis[v[tt][i].second])
				dists[v[tt][i].second] = dists[tt]+1, q.push(v[tt][i].second);
		}
	}


	memset(vis, 0, sizeof(vis));

	q.push(t);
	distt[t] = 0;
	while(!q.empty())
	{
		auto tt = q.front();
		
	//	DEBUG(tt);

		q.pop();
		if(!vis[tt])
			vis[tt]=1;
		else
			continue;

		REP(i, v[tt].size())
		{
			if(!vis[v[tt][i].second])
				distt[v[tt][i].second] = distt[tt]+1, q.push(v[tt][i].second);
		}

	}


	memset(vis, 0, sizeof(vis));
	q.push(s);
//	dists[s] = 0;
	string str = "" ;
	ll total = dists[t];
//	DEBUG(total);
	while(!q.empty())
	{
		vector<ll> curr;
		vector<pair<char,ll> > edges;
		auto tt = q.front();
		vis[tt] = 1;
		if(tt == t)
			break;
		q.pop();
		curr.push_back(tt);
		while(true)
		{
			if(q.empty() || distt[q.front()] != distt[curr[0]])
				break;
			curr.push_back(q.front());
			vis[q.front()] = 1;
			q.pop();
		}
		
		REP(i, curr.size())
		{
			REP(j, v[curr[i]].size())
			{
				ll nxt = v[curr[i]][j].second;
				char val = v[curr[i]][j].first;

				if(distt[nxt] == distt[curr[i]] - 1 && !vis[nxt])
					edges.push_back(v[curr[i]][j]);
			}
		}

		if(!edges.empty())sort(edges.begin(), edges.end());
		if(!edges.empty())str += edges[0].first;

		REP(i, edges.size())
		{
			if(edges[i].first != edges[0].first)
				break;
			q.push(edges[i].second);
		}
	}

	cout << str << endl;

//	memset(vis, 0, sizeof(vis));

//	memset(vis, 0, sizeof(vis));

//	f(s, 0, ' ');
}