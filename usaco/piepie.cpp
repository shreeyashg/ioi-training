#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n;
vector<pair<ll, ll> >A, B;

int main()
{
	cin >> n;
	REP(i, n)
	{
		ll x, y;
		cin >> x >> y;
		A.push_back({x,y});
	}

	REP(i, n)
	{
		ll x, y;
		cin >> x >> y;
		B.push_back({x,y});
	}
	
	
}