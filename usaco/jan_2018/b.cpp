#include <bits/stdc++.h>
#include <fstream>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll dist1[MX], dist2[MX], n, k;
vector<ll> v[MX];
vector<pair<ll, ll> > leafs;
bool vis[MX], leaf[MX], nm[MX];

ll dfs(ll idx, ll par, ll dist)
{
	if(v[idx].size()==1)
	{
	//	if(k == idx)
	//		cout << 1 << endl, exit(0);
		leafs.push_back({dist, idx});
		leaf[idx] = 1;
	}

	dist1[idx] = dist;

	REP(i, v[idx].size())
	{
		if(v[idx][i] != par)
			dfs(v[idx][i], idx, dist + 1);
	}


}

bool bfs(ll od) // 0 means not reachable. 1 means reachable.
{
	queue<ll> q;
	memset(vis, 0, sizeof(vis));
	memset(dist2, 0, sizeof(dist2));
	memset(nm, 0, sizeof(nm));
	vector<ll> nm;
//	cout << "Pushed: " << endl;
	REP(i, leafs.size())
	{
		if(i < od) q.push(leafs[i].second);//, cout << leafs[i].second << " ";
		else nm.push_back(leafs[i].second);
	}
//	cout << " . " << endl;

	while(!q.empty())
	{
		ll x = q.front();
		q.pop();
	//	cout  << " curr =  "<< x << " dist = " << dist2[x] << endl;

		if(!vis[x])
			vis[x] = 1;
		else
			continue;

		REP(i, v[x].size())
		{
			if(!vis[v[x][i]])
			{
				dist2[v[x][i]] = dist2[x] + 1;
				q.push(v[x][i]);
			}
		}
	}

//	DEBUG(od);
//	FOR(i, 1, n) cout << i << " " << dist2[i] << endl;
//	cout << endl << endl;

	REP(i, nm.size())
	{
		if(dist1[nm[i]] <= dist2[nm[i]])
			return 1;
	}
	return 0;
}

ll bs()
{
	ll lo = 1, hi = leafs.size();
	ll ans = hi;

//	FOR(i, lo, hi) cout << bfs(i) << endl;

	REP(i, 20)
	{
		ll mid = (lo + hi) / 2;
		if(bfs(mid))
		{
			lo = mid + 1;
		}
		else
		{
			ans = mid;
			hi = mid - 1;
		}
	}

	return ans;
}

int main()
{
	ifstream cin("atlarge.in");
	ofstream cout("atlarge.out");

	cin >> n >> k;
	REP(i, n - 1)
	{
		ll x, y;
		cin >> x >> y;
		v[x].push_back(y);
		v[y].push_back(x);
	}

	dfs(k, 0, 0);

//	cout << leafs.size() << endl;

//	exit(0);
	sort(leafs.begin(), leafs.end());
	reverse(leafs.begin(), leafs.end());


//	DEBUG("Distances from k:");
//	FOR(i, 1, n) cout << i << " " << dist1[i] << endl;

	cout << bs() << endl;
}