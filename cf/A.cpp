#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n = 6;
ll arr[10];

int main()
{
	ll sum = 0;
	REP(i, n)
	{
		cin >> arr[i + 1];
		sum += arr[i + 1];
	}

	if(sum % 2)
		cout << "NO" << endl, exit(0);

	FOR(i, 1, n)
	{
		FOR(j, i + 1, n)
		{
			FOR(k, j + 1, n)
			{
				if(arr[i] + arr[j] + arr[k] == sum/2)
				{
					cout << "YES" << endl;
					exit(0);
				}
			}
		}
	}

	cout << "NO" << endl;
	exit(0);
}