#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 2e5 + 4;

ll last[MX], n, arr[MX];

int main()
{
	cin >> n;
	REP(i, n) cin >> arr[i + 1];

	memset(last, -1, sizeof(last));

	FORD(i, n, 1)
	{
		if(last[arr[i]] == -1)
			last[arr[i]] = i;
	}

	ll ans = INF, pos = 0;
	FOR(i, 1, n)
	{
		if(true)
		{
			if(last[arr[i]] < ans)
				ans = last[arr[i]], pos = arr[i];
		}
	}

	cout << pos << endl;
}