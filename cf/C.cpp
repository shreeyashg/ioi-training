#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 2e5 + 4;

ll n, t[MX], last[MX];
set<ll> s[MX];

int main()
{
	cin >> n;
	FOR(i, 1, n)
	{
		cin >> t[i];
	}

	memset(last, -1, sizeof(last));

	ll num = 1;
	s[0].insert(num);
	FOR(i, 1, n)
	{
		ll x = t[i];
		if(s[x].empty())
		{
			s[i].insert(++num);
		}
		else
		{
			ll a = *(s[x].begin());
			s[x].clear();
			s[i].insert(a);	
		//	last[x] = -1;
		}
	}

	cout << num << endl;
}