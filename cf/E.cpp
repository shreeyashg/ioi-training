#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e6 + 4;

ll fact[MX], n, k;

// Next 3 functions from "https://stackoverflow.com/questions/10118137/fast-n-choose-k-mod-p-for-large-n"

long long degree(long long a, long long k, long long p) {
  long long res = 1;
  long long cur = a;

  while (k) {
    if (k % 2) {
      res = (res * cur) % p;
    }
    k /= 2;
    cur = (cur * cur) % p;
  }
  return res;
}

int get_degree(long long n, long long p) { // returns the degree with which p is in n!
  int degree_num = 0;
  long long u = p;
  long long temp = n;

  while (u <= temp) {
    degree_num += temp / u;
    u *= p;
  }
  return degree_num;
}

long long combinations(int n, int k, long long p) {
  int num_degree = get_degree(n, p) - get_degree(n - k, p);
  int den_degree = get_degree(k, p);

  if (num_degree > den_degree) {
    return 0;
  }
  long long res = 1;
  for (long long i = n; i > n - k; --i) {
    long long ti = i;
    while(ti % p == 0) {
      ti /= p;
    }
    res = (res * ti) % p;
  }
  for (long long i = 1; i <= k; ++i) {
    long long ti = i;
    while(ti % p == 0) {
      ti /= p;
    }
    res = (res * degree(ti, p-2, p)) % p;
  }
  return res;
}


int main()
{
	fact[0] = 1;
	FOR(i, 1, 1e6) fact[i] = (i%MOD * fact[i-1]%MOD)%MOD;

	cin >> n >> k;

	if(k >= n)
		cout << 0 << endl, exit(0);


	ll outside = (n - (k + 1));
//	cout << fact[5] << " " << midperms << endl;
	ll ans = 0;	
	FOR(i, 1, n)
	{
		ll first = i;
		ll midperms = fact[first-1] / fact[first - 1 - k];
		ll out = fact[outside];
		ans += midperms * out;
	}
	cout << ans << endl;
//	DEBUG("WTF");
}