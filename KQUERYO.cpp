#pragma GCC optimize("O3")
#include <bits/stdc++.h>
using namespace std;

typedef int ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const int MOD = 1e9 + 7, MX = 3e4 + 4, VX = 1e6 + 4;

ll arr[MX], n, q;
vector<ll> v[4 * MX];

inline void merge(ll node, vector<ll> &a, vector<ll> &b)
{
	ll i = 0, j = 0;
	REP(xyz, a.size() + b.size())
	{
		if(i < a.size() && j < b.size() && a[i] < b[j]) v[node].push_back(a[i]), i++;
		else if(i < a.size() && j < b.size()) v[node].push_back(b[j]), j++;
		else if(i < a.size()) v[node].push_back(a[i]), i++;
		else v[node].push_back(b[j]), j++;
	}
}

void build_tree(ll node, ll a, ll b) 
{
  	if(a > b) return; // Out of range
  	
  	if(a == b) 
  	{ // Leaf node
    		v[node].push_back(arr[a]); // Init value
		return;
	}
	
	build_tree(node*2, a, (a+b)/2); // Init left child
	build_tree(node*2+1, 1+(a+b)/2, b); // Init right child
	
	merge(node, v[node*2], v[node*2 + 1]);
}

ll query_tree(ll node, ll a, ll b, ll i, ll j, ll k) 
{
	if(a > b || a > j || b < i) return 0;

	if(a >= i && b <= j) // Current segment is totally within range [i, j]
	{
		return v[node].end() - upper_bound(v[node].begin(), v[node].end(), k);
	}

	ll q1 = query_tree(node*2, a, (a+b)/2, i, j, k); // Query left child
	ll q2 = query_tree(1+node*2, 1+(a+b)/2, b, i, j, k); // Query right child

	ll res = (q1 + q2); // Return final result
	
	return res;
}


int main()
{
	//{1, 4, 6, 2, 52, 11, 34}
	ios::sync_with_stdio(false);
	cin.tie(0);

	cin >> n;// >> q;
	REP(i, n) cin >> arr[i + 1];
	build_tree(1, 1, n);
	cin >> q;
	ll a = 0, b = 0, c = 0, last_ans = 0;
	REP(i, q)
	{
		ll x, y, k;
		cin >> a >> b >> c;
		last_ans = query_tree(1, 1, n, a, b, c);
		cout << last_ans << endl;

	//	REP(i, 10) cout << tree[1][i] << " ";
	//	cout << endl;
	}

}