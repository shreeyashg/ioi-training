#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll c, n, k, t;
vector<pair<ll, ll> > v;
map<ll, vector<ll> > s;
/*
ll bs(ll pos)
{
	ll cnt = 0;
	ll x = v[pos].first, y = v[pos].second;
	ll lo = 1, hi = pos - 1;
	REP(i, 20)
	{
		ll mid = (lo + hi)/2;
		if((x >= v[mid].first && x <= v[mid].second) || (y >= v[mid].first && y <= v[mid].second))
		{
			cnt += (pos - mid);
			hi = mid-1;
		}
		else
		{

		}
	}

	ll cnt = 0;
	ll x = v[pos].first, y = v[pos].second;
	ll lo = 1, hi = pos - 1;
	REP(i, 20)
	{
		ll mid = (lo + hi)/2;
		if((x >= v[mid].first && x <= v[mid].second) || (y >= v[mid].first && y <= v[mid].second))
		{

		}
	}

	return cnt;
}
*/

ll numi[MX];

bool poss(ll x, ll y)
{
	FOR(i, 1, n)
	{
		auto a = v[i];
		if(a == make_pair(x, y))
		{
			v.erase(v.begin() + i);
			break;
		}
	}

	bool f = 0;

	FOR(i, 0, n)
	{
		if(v[i+1].first - v[i].second >= y - x + 1)
		{
			DEBUG(v[i+1].first);
			DEBUG(v[i].second);
			f = 1;
			break;
		}
	}

	FOR(i, 0, n)
	{
		if(v[i+1].first <= v[i].second && (i+1 != v.size()-1))
		{
			DEBUG("oop");
			DEBUG(i);
			DEBUG(v[i+1].first);
			DEBUG(v[i].second);

			f = 0;
		}
	}

	v.push_back({x, y});
	sort(v.begin(), v.end());

	return f;
}

int main()
{
	cin >> t;
	while(t--)
	{
		v.clear();
		memset(numi, 0, sizeof(numi));
		vector<ll> starts, ends;
		cin >> c >> n >> k;
		v.push_back({1,1});
		REP(i, n)
		{
			ll x, y;
			cin >> x >> y;
			starts.push_back(x);
			ends.push_back(y);
			v.push_back({x, y});
			s[x].clear();
		}
		FOR(i, 1, n)
		{
			ll x = v[i].first, y = v[i].second;
			s[x].push_back(y);
		}

		REP(i, starts.size())
		{
			sort(s[starts[i]].begin(), s[starts[i]].end());
		}

		v.push_back({c, INF});
	
		sort(v.begin(), v.end());
		sort(starts.begin(), starts.end());
		sort(ends.begin(), ends.end());
		ll cnt = 0;

		FOR(i, 1, n)
		{
			ll x = v[i].first, y = v[i].second;
			auto it = lower_bound(starts.begin(), starts.end(), x);
			auto it2 = upper_bound(starts.begin(), starts.end(), y);;
		//	auto it2 = starts.upper_bound(x);
			if((*it >= x && *it <= y))
			{
			//	DEBUG(x); DEBUG(y);
			//	DEBUG(*it);

				ll curr1 = (it2-it)-1;
				numi[i] += curr1;
				cnt+=curr1;
			}
		}

		if(!k)
		{
			if(cnt)
				cout << "Bad" << endl;
			else
				cout << "Good" << endl;
		}
		else
		{
			ll ka = 0;
			FOR(i, 1, n)
			{
				ll x = v[i].first, y = v[i].second;
				auto it = lower_bound(starts.begin(), starts.end(), x);
				it++;

				if((*it >= v[i].first) || (*it <= v[i].second))
				{
					ll nx = *it, ny = (*it == v[i].first) ? s[*it][1] : s[*it][0];
					ll x = v[i].first, y = v[i].second;
				
					DEBUG(nx);
					DEBUG(ny);
					DEBUG(x);
					DEBUG(y);
					if(poss(x, y) && poss(nx, ny))
					{
						cout << "Good" << endl;
						break;
					}
					else
					{
						cout << "Bad" << endl;
						break;
					}

				}


			}			
			// check for space		
		}



	}
	

}