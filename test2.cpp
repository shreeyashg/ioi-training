#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

vector<ll> v[MX];
ll n, depth[MX];

void init(ll idx, ll par)
{
	REP(i, v[idx].size())
	{
		if(v[idx][i] != par)
		{
			depth[v[idx][i]] = depth[idx] + 1;
			init(v[idx][i], idx);
		}
	}
}

int main()
{
	cin >> n;
	REP(i, n - 1)
	{
		ll x, y;
		cin >> x >> y;
		v[x].push_back(y);
		v[y].push_back(x);
	}

	init(1, 0);

	ll k;
	cin >> k;
	REP(i, k)
	{
		ll x ,y;
		cin >> x >> y;

		if(abs(depth[x] - depth[y]) % 2 == 0)
			cout << "Even" << endl;
		else
			cout << "Odd" << endl;
	}
 }