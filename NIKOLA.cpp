#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 1e5 + 4;

ll n;
vector<ll> v;
ll memo[1005][1005];

ll rec(ll curr, ll last)
{
	if(curr == n)
		return 0;

	if(memo[curr][last] != -1)
		return memo[curr][last];

	ll ans = INF;
	ll dist = abs(curr - last);
	if(curr + dist + 1 <= n)ans = min(ans, v[curr + dist + 1] + rec(curr + dist + 1, curr));
	if(curr - dist >= 1)ans = min(ans, v[curr - dist] + rec(curr - dist, curr));

	return memo[curr][last] = ans;
}

int main()
{
	memset(memo, -1, sizeof(memo));
	cin >> n;
	v.push_back(INF);
	REP(i, n)
	{
		ll x;
		cin >> x;
		v.push_back(x);
	}

	cout << v[2] + rec(2, 1) << endl;
}