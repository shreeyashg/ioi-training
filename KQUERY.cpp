#include <bits/stdc++.h>
using namespace std;

typedef int ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;
#define endl '\n'

const ll MOD = 1e9 + 7, MX = 3e4 + 4;

struct node
{
	ll from, to, val, lazy;
	node *left, *right;
	node()
	{
		from = 1;
		to = 3e4;
		val = 0, lazy = 0, left = NULL, right = NULL;
	}

	void extend()
	{
		if(left == NULL)
		{
			left = new node(), right = new node();
			left->from = from;
			left->to = (from + to) >> 1;
			right->from = ((from + to) >> 1) + 1;
			right->to = to;
		}
	}
};

node *root;

void update(node *curr, ll l, ll r, ll val)
{
	if(curr->lazy)
	{
		curr->val = (curr->to - curr->from + 1) * curr->lazy;
		if(curr->from != curr->to)
		{
			curr->extend();
			curr->left->lazy += curr->lazy;
			curr->right->lazy += curr->lazy;
		}
		curr->lazy = 0;
	}

	if((curr->from) > r || (curr->to) < l || (curr->from) > (curr->to))
		return;
	if(curr->from >= l && curr->to <= r)
	{
		curr->val += (curr->to - curr->from + 1) * val;
		if(curr->from != curr->to)
		{
			curr->extend();
			curr->left->val += val;
			curr->right->val += val;
		}
		return;
	}
	curr->extend();
	update(curr->left, l, r, val);
	update(curr->right, l, r, val);
	curr->val = curr->left->val + curr->right->val;
}

ll query(node *curr, ll l, ll r)
{
	if(curr->from > curr->to || curr->from > r || curr->to < l)
		return 0;
	if(curr->lazy)
	{
		curr->val += (curr->to - curr->from + 1)*curr->lazy;
		curr->extend();
		curr->left->lazy += curr->lazy;
		curr->right->lazy += curr->lazy;
		curr->lazy = 0;
	}

	if(curr->from >= l && curr->to <= r)
		return curr->val;

	ll q1, q2;
	q1 = query(curr->left, l, r), q2 = query(curr->right, l, r);
	return q1 + q2;
}

ll n, arr[MX], q, res[7 * MX];
vector<pair<ll, ll> >v;
vector<pair<ll, pair<ll, pair<ll, ll> > > >queries;

int main()
{	
	ios::sync_with_stdio(false);
	cin.tie(0);
	cin >> n;
	v.push_back({0, 0});
	REP(i, n)
	{
		cin >> arr[i + 1];
		v.push_back({arr[i + 1], i + 1});
	}
	sort(v.begin(), v.end());
	node *root = new node();

	REP(i, n)
	{
		update(root, i + 1, i + 1, 1);
	}

	ll pos = 1;

	cin >> q;
	queries.push_back({-1, {0, {0, 0}}});

	REP(i, q)
	{
		ll x, y, z;
		cin >> x >> y >> z;
		queries.push_back({z, {i + 1, {x, y}}});
	}
	sort(queries.begin(), queries.end());

	REP(i, q)
	{
		while(pos <= n && v[pos].first <= queries[i + 1].first)
			update(root, v[pos].second, v[pos].second, -1), pos++;
		res[queries[i + 1].second.first] = query(root, queries[i + 1].second.second.first, queries[i + 1].second.second.second);
	}

	FOR(i, 1, q) cout << res[i] << endl;
}
