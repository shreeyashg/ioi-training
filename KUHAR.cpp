#include <bits/stdc++.h>
using namespace std;

typedef int ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e9, MX = 104;

ll n, money, X[104], Y[104], szs[MX], ps[MX], szb[MX], pb[MX];
vector<ll> cost;
vector<bool> chk;
ll rec(ll i, ll j)
{
//	cout << i << " " << j << endl;
	if(j <= 0)
		return 0;
	if(chk[j])
		return cost[j];

	if(j <= szs[i])
	{
		chk[j] = 1;
	//	DEBUG("-2");
		return cost[j] = ps[i];
	}
	if(j - szb[i] >= 0)
	{
		chk[j] = 1;
	//	DEBUG("-3");
		return cost[j] = min(rec(i, j - szb[i]) + pb[i], rec(i, j - szs[i]) + ps[i]);
	}
	else
	{
	//	DEBUG("-5");
		chk[j] = 1;
		return cost[j] = min(pb[i], rec(i, j - szs[i]) + ps[i]);
	}
}

bool check(ll servings)
{
	ll tc = 0;
	REP(i, n)
	{
		chk.resize(5e5 + 1);
		cost.resize(5e5 + 1);

		ll need = max((int)0, (servings * X[i]) - Y[i]);
		if(need >= 5e5 + 1)
			return 0;
		tc += rec(i, need);
		chk.clear(), cost.clear();
	}
//	cout << servings << " " << tc << endl;
	return tc <= money;
}


ll bs()
{
	ll lo = 0, hi = 5e5 + 4, mid = (lo + hi)/2, ans = 0;
	REP(i, 21)
	{
		mid = (lo + hi) / 2;
		if(check(mid))
			ans = mid, lo = mid + 1;
		else
			hi = mid - 1;
	}
	return ans;
}


int main()
{
	cin >> n >> money;
	REP(i, n)
	{
		cin >> X[i] >> Y[i] >> szs[i] >> ps[i] >> szb[i] >> pb[i];
	}
/*
	REP(i, n)
	{
		FOR(j, 1, 1e5 + 1)
		{
			if(j <= (szs[i]))
				mincost[i][j] = ps[i];
			else
			{
				if(j - szb[i] >= 0)
					mincost[i][j] = min(mincost[i][j - szb[i]] + pb[i], mincost[i][j - szs[i]] + ps[i]);
				else
					mincost[i][j] = min(pb[i], mincost[i][j - szs[i]] + ps[i]);
			}
		//	if(j <= 100) cout << i << " " << j << " , cost = " << mincost[i][j] << endl; 
		}
	}
*/
	cout << bs() << endl;
}