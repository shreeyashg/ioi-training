#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define REP(i, n) for(ll i=0; i<n; i++)
#define FOR(i, a, b) for(ll i = a; i<=b; i++)
#define FORD(i, a, b) for(ll i = a; i>=b; i--)
#define DEBUG(x) cout<<#x<<" > "<<x<<endl;

const ll MOD = 1e9 + 7, INF = 1e18, MX = 2e3 + 4;

ll n, k, x;
ll a, b, s, good;
string str = "";
ll memo[20][3][150];


ll f(ll idx, ll less, ll sum)
{
	// less = 0 implies that the number will be <= good regardless of length
	// less = 1 implies number will be <= good only if digits are <= str[idx]
	// less = 2 implies number will be < good only if its length is < str.length()
//	cout << ". " << idx << " " << less << " " << sum << " " << last << endl;


	ll gc = 0;

	if(memo[idx][less][sum] != -1)
		return memo[idx][less][sum];

	if(idx == 0)
	{
		FOR(i, 1, 9)
		{
			if(i < (str[idx] - '0')) gc += f(idx + 1, 0, sum + i);
			if(i == (str[idx] - '0')) gc += f(idx + 1, 1, sum + i);
			if(i > (str[idx] - '0') && str.size() - 1 > idx) gc += f(idx + 1, 2, sum + i);
		}
		return gc;
	}

	gc += (sum);

	if(idx == str.size() - 1 && less == 2)
		return gc;

	else if(idx == str.size())
		return gc;

	FOR(i, 0, 9)
	{

		if(less == 1 && (i) == (str[idx] - '0'))
			gc += f(idx + 1, 1, sum + i);

		else if(less == 1 && i < (str[idx] - '0'))
			gc += f(idx + 1, 0, sum + i);

		else if(less == 1 && i > (str[idx] - '0') && idx + 1 <= str.size() - 1)
			gc += f(idx + 1, 2, sum + i);

		else if(less == 2)
			gc += f(idx + 1, 2, sum + i);

		else if(less == 0)
			gc += f(idx + 1, 0, sum + i);

	}

	return memo[idx][less][sum] = gc;
}

ll ff(ll val)
{
	memset(memo, -1, sizeof(memo));

	good = val;
	str = to_string(good);
	return f(0, 0, 0);
}


ll sum(ll x)
{

	ll dig = 0;
	string str = to_string(x);
	REP(i, str.size())
	{
		dig += str[i] - '0';
	}
	return dig;
}

int main()
{
	while(true)
	{
		cin >> n >> k;// >> x;
		a = n, b = k;//, s = x;

		if(a == -1 && b == -1)
			break;
		// First count less than a;
	//	cout << ff(a - 1) << endl;
	//	FOR(j, 1, b) asf += sum(j);

		// Then less than b;
		cout << ff(b) - ff(a - 1) << endl;



		// Then diff
	//	cout << ff(b) - ff(a - 1) << endl;

	}
}